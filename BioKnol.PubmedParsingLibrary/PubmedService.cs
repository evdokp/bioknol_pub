﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BioKnol.PubmedParsingLibrary
{
    public class PubmedService
    {
        public PubmedParsingResult Parse(int id)
        {
            var url = $"http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id={id}&retmode=xml";
            var wc = new WebClient();
            var downloadedPumbed = wc.DownloadString(url);
            var result = new PubmedParsingResult();

            // parsing
            XElement root = XElement.Parse(downloadedPumbed);
            XElement medline = root
                .Descendants("PubmedArticle")
                .Descendants("MedlineCitation").FirstOrDefault();

            XElement articlexml = medline?.Descendants("Article").FirstOrDefault();
            result.ArticleTitle = articlexml?.Descendants("ArticleTitle").FirstOrDefault()?.Value;
            result.ArticleAbstract = articlexml?.Descendants("Abstract").FirstOrDefault()?.Descendants("AbstractText").FirstOrDefault()?.Value;

            List<XElement> datecreated = medline?.Descendants("DateCreated").FirstOrDefault()?.Descendants().ToList();
            if (datecreated?.Count > 0)
            {
                DateTime dtDateCreated = new DateTime(Convert.ToInt32(datecreated?[0].Value), Convert.ToInt32(datecreated?[1].Value), Convert.ToInt32(datecreated?[2].Value));
                result.DateCreated = dtDateCreated;
            }


            List<XElement> datecompleted = medline?.Descendants("DateCompleted").FirstOrDefault()?.Descendants().ToList();
            if (datecompleted?.Count > 0)
            {
                DateTime dtDateCompleted = new DateTime(Convert.ToInt32(datecompleted?[0].Value), Convert.ToInt32(datecompleted?[1].Value), Convert.ToInt32(datecompleted?[2].Value));
                result.DateCompleted = dtDateCompleted;
            }

            List<XElement> daterevised = medline?.Descendants("DateRevised").FirstOrDefault()?.Descendants().ToList();
            if (daterevised?.Count > 0)
            {
                DateTime dtDateRevised = new DateTime(Convert.ToInt32(daterevised?[0].Value), Convert.ToInt32(daterevised?[1].Value), Convert.ToInt32(daterevised?[2].Value));
                result.DateRevised = dtDateRevised;
            }

            XElement journalxml = articlexml?.Descendants("Journal").FirstOrDefault();
            result.Journal = new JournalParsingResult()
            {
                Title = journalxml?.Descendants("Title").FirstOrDefault()?.Value,
                ISOAbbreviation = journalxml?.Descendants("ISOAbbreviation").FirstOrDefault()?.Value,
                Country = medline?.Descendants("MedlineJournalInfo").FirstOrDefault()?.Descendants("Country").FirstOrDefault()?.Value
            };

            result.IssueYear = Convert.ToInt32(journalxml?
                .Descendants("JournalIssue").FirstOrDefault()?
                .Descendants("PubDate").FirstOrDefault()?
                .Descendants("Year").FirstOrDefault()?.Value);


            // authors
            IEnumerable<XElement> authorslistxml =
                articlexml?.Descendants("AuthorList").FirstOrDefault()?.Descendants("Author");
            foreach (var authorxml in authorslistxml)
            {
                result.AuthorsList.Add(new AuthorParsingResult()
                {
                    Last = authorxml.Descendants("LastName").FirstOrDefault().Value, 
                    Fore = authorxml.Descendants("ForeName").FirstOrDefault().Value,
                    Initials = authorxml.Descendants("Initials").FirstOrDefault().Value
                });
            }

            // mesh terms

            IEnumerable<XElement> meshlistxml = medline?.Descendants("MeshHeadingList").Descendants()
                .Where(p=>p.Name== "DescriptorName");
            foreach (var meshxml in meshlistxml)
            {
                result.MeshHeadingsList.Add(meshxml.Value);
            }

            // parsing finished


            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioKnol.PubmedParsingLibrary
{
    public class PubmedParsingResult
    {
        public string ArticleTitle { get; set; }
        public string ArticleAbstract { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateRevised { get; set; }
        public DateTime? DateCompleted { get; set; }
        public int IssueYear { get; set; }
        public JournalParsingResult Journal { get; set; }
        public List<AuthorParsingResult> AuthorsList { get; set; }
        public List<string> MeshHeadingsList { get; set; } 
        public PubmedParsingResult()
        {
            AuthorsList = new List<AuthorParsingResult>();
            MeshHeadingsList = new List<string>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioKnol.PubmedParsingLibrary
{
    public class AuthorParsingResult
    {
        public string Last { get; set; }
        public string Fore { get; set; }
        public string Initials { get; set; }
        public string Full => $"{Fore} {Last} {Initials}";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Text;
using System.Threading.Tasks;

namespace BioKnol.DataAccess
{
    public class ViewsRepository
    {
        private bio_dataEntities db = new bio_dataEntities();

        public bool TryGetPMIDbyUrl(string url,ref int? pmid)
        {
            var sequence = db.ViewsLogs.Where(p => p.PubMed_ID != null && p.URL == url);
            bool result = sequence.Any();
            if (result) pmid = sequence.Select(p => p.PubMed_ID).First();
            return result;
        }


        public bool HasDOIByURL(string url) 
        {
            return db.ViewsLogs.Where(p => p.DOI.Length > 2 && p.URL == url).Any();
        }

        public ViewsLog GetSingleViewByURLwithDOI(string url) 
        {
            return db.ViewsLogs.Where(p => p.DOI.Length > 2 && p.URL == url).FirstOrDefault();
        }

    }
}

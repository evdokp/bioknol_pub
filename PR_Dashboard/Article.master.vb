﻿Imports DevExpress.Web
Imports System.Drawing
Imports BioWS.BLL.Extensions

Public Class Article
    Inherits System.Web.UI.MasterPage

    Private _profilePersonID As Integer?
    Private ReadOnly Property ProfilePersonID As Integer
        Get
            If _profilePersonID Is Nothing Then
                _profilePersonID = Me.Page.GetProfilePersonID()
            End If
            Return _profilePersonID
        End Get
    End Property
    Private ReadOnly Property PersonID As Integer
        Get
            If Me.QS("PersonID").IsNullOrEmpty() Then
                Return ProfilePersonID
            Else
                Return Page.QSInt32("PersonID")
            End If
        End Get
    End Property

    Private ReadOnly Property PubmedID As Integer
        Get
            Return Me.QSInt32("Pubmed_ID")
        End Get
    End Property

    Private Function GetWidgetMenuItem(ByVal r As WidgetsDataSet.WidgetsRow) As ASPxMenu.MenuItem
        Dim item As New ASPxMenu.MenuItem
        item.Text = r.Title
        item.Name = r.Widget_ID
        item.NavigateUrl = String.Format("~\ViewInfoPMWidget.aspx?PersonID={0}&WidgetID={1}&Pubmed_ID={2}", PersonID, r.Widget_ID, PubmedID)
        If BLL.Widgets.IsAdditionalInfoAvailableByWidget(PubmedID, r.Widget_ID) > 0 Then
            item.Image.Url = "~\Images\lightbulb_on.png"
        Else
            item.Image.Url = "~\Images\lightbulb.png"
        End If
        Return item
    End Function
    Private Sub SetWidgetsMenu(ByVal widgetsTBL As WidgetsDataSet.WidgetsDataTable)
        For i = 0 To widgetsTBL.Count - 1
            Dim r = widgetsTBL.Item(i)
            Dim item As ASPxMenu.MenuItem = GetWidgetMenuItem(r)
            ASPxMenu2.Items.Add(item)
        Next
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.NotPostBack() Then
            Dim widgetsTA As New WidgetsDataSetTableAdapters.WidgetsTableAdapter


            ASPxMenu1.Items.FindByName("miOverview").NavigateUrl = "ViewInfoPM.aspx?Pubmed_ID=" & Me.QS("Pubmed_ID")

            'hide request if download available
            Dim miRequests As DevExpress.Web.ASPxMenu.MenuItem = ASPxMenu3.Items.FindByName("miRequest")
            miRequests.Visible = DownloadUnavailable
            If DownloadUnavailable Then
                miRequests.Text = IIf(HasNoRequests, "Request", "Already requested")
                miRequests.Enabled = HasNoRequests.Inverse()
            End If




            Dim widgetsTBL = widgetsTA.GetDataByPersonID_ON(ProfilePersonID)
            Dim HasWidgets As Boolean = widgetsTBL.Count > 0
            ASPxMenu2.Visible = HasWidgets
            divWidgets.Visible = HasWidgets

            If HasWidgets Then
                SetWidgetsMenu(widgetsTBL)
            End If

        End If


    End Sub

    Protected Sub ASPxMenu3_ItemClick(source As Object, e As DevExpress.Web.ASPxMenu.MenuItemEventArgs) Handles ASPxMenu3.ItemClick
        If e.Item.Name = "miRecommend" Then
            Response.Redirect(String.Format("~\RecommendArticle.aspx?PubmedID={0}&UserID={1}", PubmedID, ProfilePersonID), False)
        End If
    End Sub


    Private ReadOnly Property HasNoRequests As Boolean
        Get
            Dim reqta As New ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter
            Return reqta.GetCountByPersonIDPubmedID(PubmedID, ProfilePersonID) = 0
        End Get
    End Property
    
    Private ReadOnly Property DownloadUnavailable() As Boolean
        Get
            Dim ta As New ArticlesDataSetTableAdapters.ArticleOffersTableAdapter
            Dim count As Integer = ta.GetCountByPubmedIDwithFiles(PubmedID)
            Return count = 0
        End Get
    End Property

End Class
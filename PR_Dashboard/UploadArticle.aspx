﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Basic.Master"
    CodeBehind="UploadArticle.aspx.vb" Inherits="BioWS.UploadArticle"  ValidateRequest = "false" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Upload paper
    </h2>
    <fieldset class="register" style="width: 500px;">
        <legend>Upload information</legend>
        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="View1" runat="server">
                <p>
                    <asp:Label ID="lblTitle" runat="server" AssociatedControlID="txtTitle">Title:</asp:Label>
                    <asp:TextBox ID="txtTitle" runat="server" TextMode="MultiLine" Rows="4" Width="320px"
                        CssClass="textEntry"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="txtTitle"
                        CssClass="failureNotification" ErrorMessage="Title is required." ToolTip="Title is required."
                        ValidationGroup="Upload">*</asp:RequiredFieldValidator>
                </p>
                <p>
                    <asp:Label ID="lblPubMedID" runat="server" AssociatedControlID="txtTitle">PubMed identifier (PMID):</asp:Label>
                    <asp:TextBox ID="txtPubMedID" runat="server" CssClass="textEntry"></asp:TextBox>
                </p>
                <p>
                    <asp:Label ID="lblDOI" runat="server" AssociatedControlID="txtDOI">Digital object identifier (DOI):</asp:Label>
                    <asp:TextBox ID="txtDOI" runat="server" CssClass="textEntry"></asp:TextBox>
                </p>
                <p>
                    <asp:Label ID="lblFile" runat="server" AssociatedControlID="FileUpload1">File:</asp:Label>
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                </p>
                <p>
                    <asp:Button ID="Button1" runat="server" Text="Upload paper" />
                </p>
                <p>
                    <asp:Button ID="Button2" runat="server" Text="Go back to paper URL" />
                </p>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <p>
                    File has been uploaded. Thank you!
                </p>
                <p>
                    <asp:Button ID="btn_one_more" runat="server" Text="Upload one more" /><br />
                    <asp:Button ID="btn_goback" runat="server" Text="Go back to paper URL" />
                </p>
            </asp:View>
        </asp:MultiView>
    </fieldset>
</asp:Content>

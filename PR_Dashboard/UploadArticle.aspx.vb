﻿Imports System.IO
Imports BioWS.BLL
Imports BioWS.BLL.Extensions
Imports BioWS.BLL.Extensions.Web

Public Class UploadArticle
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Me.QS("Request_ID").IsNullOrEmpty() Then
            btn_goback.Text = "Go back to paper URL"
            Button2.Text = "Go back to paper URL"
        Else
            btn_goback.Text = "Go back other people requests"
            Button2.Text = "Go back other people requests"
        End If

        If Me.NotPostBack() Then
            Dim vUrl As String
            Dim vPmid As Integer
            Dim vTitle As String
            Dim vDoi As String

            If Request.QueryString("Request_ID") = String.Empty Then
                vUrl = HttpUtility.UrlDecode(Request.QueryString("URL"))
                vPmid = BLL.PubMed.GetPubMedIDfromURL(vUrl)
                If vPmid > 0 Then
                    Dim pubMedArticle As PubMed.PubMedArticle = New BLL.PubMed.PubMedArticle(vPmid)
                    vTitle = pubMedArticle.Title
                Else
                    Dim vHtml As String = vUrl.GetHTML()
                    vTitle = vHtml.GetTitleTagText()
                    vDoi = BLL.DOI.GetDOIbyURL_internal(vUrl)
                    If vDoi.IsNullOrEmpty() Then
                        vDoi = BLL.DOI.GetDOIFromHTML(vHtml)
                    End If
                End If
            Else
                Dim ta As New ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter
                Dim r = ta.GetDataByID(Request.QueryString("Request_ID")).Item(0)
                vUrl = r.URL
                If r.IsDOINull = True Then
                    vDoi = String.Empty
                Else
                    vDoi = r.DOI
                End If
                If r.IsPubMed_IDNull = True Then
                    vPmid = 0
                Else
                    vPmid = r.PubMed_ID
                End If

                If vPmid > 0 Then
                    vTitle = BLL.PubMed.GetTitleByID_internal(vPmid)
                Else
                    vTitle = r.Title
                End If

            End If

            Me.MultiView1.ActiveViewIndex = 0
            Me.txtPubMedID.Text = vPmid
            Me.txtDOI.Text = vDoi
            Me.txtTitle.Text = vTitle
        End If

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click

        Dim vPmid As Integer = 0
        Integer.TryParse(Me.txtPubMedID.Text, vPmid)
        Dim vTitle As String
        If vPmid > 0 Then
            vTitle = String.Empty
        Else
            vTitle = Me.txtTitle.Text
        End If

        Dim vPluginId As Integer
        Dim vPersonId As Integer
        Dim vUrl As String

        If Request.QueryString("Request_ID") = String.Empty Then
            vPluginId = Request.QueryString("PlugInID")
            vPersonId = Request.QueryString("UserID")
            vUrl = HttpUtility.UrlDecode(Request.QueryString("URL"))
            UploadFile(vUrl, vPmid, vPluginId, vPersonId, vTitle)
        Else
            vPluginId = 0
            Dim p As New ProfileCommon
            p = p.GetProfile(User.Identity.Name)
            vPersonId = p.Person_ID
            Dim requeststa As New ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter
            Dim requestrow = requeststa.GetDataByID(Request.QueryString("Request_ID")).Item(0)
            vUrl = requestrow.URL
            UploadFile(vUrl, vPmid, vPluginId, vPersonId, vTitle)

            'create link and send message
            Dim offersta As New ArticlesDataSetTableAdapters.ArticleOffersTableAdapter
            Dim offerst = offersta.GetDataByURL(vUrl)
            Dim linksta As New ArticlesDataSetTableAdapters.LinksArticleRequestsToOffersTableAdapter



            Dim body, reqtitle, dwnllink, pubmpages As String
            pubmpages = ""
            dwnllink = ""
            reqtitle = ""
            body = ""


            dwnllink = dwnllink & "Download link(s):<br/>"
            For i = 0 To offerst.Count - 1
                'insert link
                Dim offerrow As ArticlesDataSet.ArticleOffersRow = offerst.Item(i)
                linksta.Insert(Request.QueryString("Request_ID"), offerrow.ID)



                dwnllink = dwnllink & "<a href=""http://ws.bioknowledgecenter.ru/Download.ashx"
                dwnllink = dwnllink & "?vFileName=" & HttpUtility.UrlEncode(offerrow.FileName)
                dwnllink = dwnllink & "&vOriginalName=" & HttpUtility.UrlEncode(offerrow.OriginalName)
                dwnllink = dwnllink & """>"
                dwnllink = dwnllink & offerrow.OriginalName
                dwnllink = dwnllink & "</a></br></br>"

                Dim feedaction As BLL.Feed.FeedActionArticleOffer

                If offerrow.IsPubMed_IDNull() = False Then
                    If offerrow.PubMed_ID > 0 Then


                        feedaction = New BLL.Feed.FeedActionArticleOffer(offerrow.PubMed_ID, offerrow.ID)


                        pubmpages = pubmpages & "<a href=""http://ws.bioknowledgecenter.ru/ViewInfoPM.aspx?"
                        pubmpages = pubmpages & "Pubmed_ID=" & offerrow.PubMed_ID
                        pubmpages = pubmpages & """>"
                        pubmpages = pubmpages & BLL.PubMed.GetTitleByID_internal(offerrow.PubMed_ID)
                        pubmpages = pubmpages & "</a></br></br>"

                    Else
                        feedaction = New BLL.Feed.FeedActionArticleOffer(0, offerrow.ID)
                    End If
                Else
                    feedaction = New BLL.Feed.FeedActionArticleOffer(0, offerrow.ID)
                End If


                feedaction.Save(requestrow.Person_ID)


            Next
            If requestrow.IsPubMed_IDNull() = False Then
                reqtitle = BLL.PubMed.GetTitleByID_internal(requestrow.PubMed_ID)
            Else
                reqtitle = requestrow.Title
            End If
            If reqtitle.Length > 0 Then
                reqtitle = "You requested:</br><a href=""" & requestrow.URL & """>" & reqtitle & "</a></br></br>"
            End If
            If pubmpages.Length > 0 Then
                pubmpages = "Article page(s):<br/>" & pubmpages
            End If
            body = reqtitle & dwnllink & pubmpages
            'send message
            Try
                ' todo if person is 0 or nothing do not add
                Dim li As New List(Of Integer)
                li.Add(requestrow.Person_ID)
                BLL.MessageHelper.Send(Nothing, li, "Your request was processed", body, Page)
            Catch ex As Exception

            End Try

        End If

    End Sub
    Private Sub UploadFile(ByVal vURl As String, ByVal vPmid As Integer, ByVal vPluginId As Integer, ByVal vPersonId As Integer, ByVal vTitle As String)
        If Me.FileUpload1.HasFile = True Then
            '--Добавляем файл----------------------------------------------------------
            Dim fileData As Byte() = New Byte(FileUpload1.PostedFile.InputStream.Length) {}
            FileUpload1.PostedFile.InputStream.Read(fileData, 0, fileData.Length)
            Dim contentType As String = FileUpload1.PostedFile.ContentType
            Dim originalName As String = Path.GetFileName(FileUpload1.PostedFile.FileName)
            BLL.FileHelper.SaveFile.SaveArticleAzure(vURl,
                                           vPmid,
                                            Me.txtDOI.Text,
                                            vPluginId,
                                            vPersonId,
                                           fileData,
                                           contentType,
                                           originalName,
                                           vTitle, String.Empty)
            Me.MultiView1.ActiveViewIndex = 1
        End If
    End Sub

    Protected Sub btn_one_more_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_one_more.Click
        Me.MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub btn_goback_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_goback.Click
        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)

        If Request.QueryString("Request_ID") = String.Empty Then
            Dim vUrl As String
            vUrl = HttpUtility.UrlDecode(Request.QueryString("URL"))
            Me.Response.Redirect(vUrl)
        Else
            Me.Response.Redirect("~\Account\OtherPeopleRequests.aspx?Person_ID=" & p.Person_ID, False)
        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)
        If Request.QueryString("Request_ID") = String.Empty Then
            Dim vUrl As String
            vUrl = HttpUtility.UrlDecode(Request.QueryString("URL"))
            Me.Response.Redirect(vURL)
        Else
            Me.Response.Redirect("~\Account\OtherPeopleRequests.aspx?Person_ID=" & p.Person_ID, False)
        End If

    End Sub
End Class
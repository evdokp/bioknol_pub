﻿Imports DevExpress.Web
Imports System.IO
Imports BioWS.BLL.Extensions


Public Class ViewInfoPM
    Inherits System.Web.UI.Page
   
 
    Private Sub SetPageTitles(ByVal pma As BLL.PubMed.PubMedArticle)
        Dim ct1 As ContentPlaceHolder = CType(Master.Master.FindControl("MainContent"), ContentPlaceHolder)
        CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "Article information - Overview"
        CType(Master.Master.FindControl("page_header"), System.Web.UI.HtmlControls.HtmlGenericControl).Visible = False
        CType(ct1.FindControl("hl_pm"), HyperLink).Text = pma.Title
        CType(ct1.FindControl("hl_pm"), HyperLink).NavigateUrl = pma.PubmedURL
        Title = pma.Title
    End Sub
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
        Dim pma As New BLL.PubMed.PubMedArticle(Me.QSInt32("Pubmed_ID"))

        SetPageTitles(pma)

        lit_abstract.Text = pma.Abstract
        lit_authores.Text = pma.AuthorsString
        lit_journal.Text = pma.Journal


        Dim meshStr As String = pma.MeshString
        lit_mesh.Text = IIf(meshStr.IsNullOrEmpty(), "no MESH-headings detected", meshStr)



        Dim _pr As New ProfileCommon
        _pr = _pr.GetProfile(User.Identity.Name)



        If pma.OffersWithFilesCount > 0 Then
            Dim dwnldta As New ArticlesDataSetTableAdapters.ArticleOffersTableAdapter
            Dim dwnlt = dwnldta.GetDataByPubmedIDwithFiles(Request.QueryString("Pubmed_ID"))


            Dim downloads = From d In dwnlt
                            Select d.ContentType, FileName = GetFileName(d.OriginalName), Image = GetImageByType(d.ContentType), _
                            DownloadLink = "~\Download.ashx?vFileName=" & HttpUtility.UrlEncode(d.FileName) & "&vOriginalName=" & HttpUtility.UrlEncode(d.OriginalName)

            Dim dwnldlst = downloads.ToList()

            gvDownloads.DataSource = dwnldlst
            gvDownloads.DataBind()
        Else
            pnlDownloads.Visible = False
        End If

        hfPageOpenDateTime.Value = Now.ToString
        hfPubmed.Value = Me.QS("Pubmed_ID")

    End Sub

    Private Function GetImageByType(contenttype As String) As String
        Select Case contenttype
            Case "application/pdf"
                Return "~\Images\pdficon16.png"
            Case "text/plain"
                Return "~\Images\txticon16.png"
            Case Else
                Return "~\Images\unknownicon16.png"
        End Select
    End Function
    Private Function GetFileName(filename As String) As String
        Dim fnwe As String = Path.GetFileNameWithoutExtension(filename)
        Dim ext As String = Path.GetExtension(filename)
        If fnwe.Length > 70 Then
            Return fnwe.Substring(0, 70) & " ... " & ext
        Else
            Return filename
        End If
    End Function

   


    <System.Web.Services.WebMethod()> _
    Public Shared Function GetArtHist(ByVal PubmedID As Integer,
                                        ByVal EarlierThen As String,
                                        ByVal LastIndex As Integer) As String


        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)

        Dim dt As Date
        DateTime.TryParse(EarlierThen, dt)

        Dim db = New DataClassesFeedDataContext


        Dim tt = From c In db.GetArticleViewHistory(10, EarlierThen, LastIndex, p.Person_ID, PubmedID)
                Select vTimestamp = c.TimeStamp, vDateTime = c.TimeStamp.ToString("dd.MM.yyyy HH:mm:ss"), _
                        vInterval = GetIntervalFromSeconds(c.SecondsSinceLast), _
                        vEgo = GetIntervalFromSeconds((Now - c.TimeStamp).TotalSeconds) & " ago", _
                        isFirst = IIf(c.SecondsSinceLast = -1, 1, 0)



        Return tt.ToList().ToJSON

    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetAfter(ByVal ViewDT As DateTime,
                                        ByVal EarlierThen As String,
                                        ByVal LastIndex As Integer) As String

        ViewDT = ViewDT.ToLocalTime()

        Dim timesp As TimeSpan = ViewDT.TimeOfDay


        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)

        Dim dt As Date
        DateTime.TryParse(EarlierThen, dt)

        Dim db = New DataClassesFeedDataContext


        Dim tt = From c In db.GetArticleViewHistoryAfter(10, EarlierThen, LastIndex, p.Person_ID, ViewDT)
                Select vTimestamp = c.TimeStamp, vDateTime = c.TimeStamp.ToString("dd.MM.yyyy HH:mm:ss"), _
                        vInterval = GetIntervalFromSeconds(c.SecondsSinceLast), _
                        vEgo = GetIntervalFromSeconds((Now - c.TimeStamp).TotalSeconds) & " ago", _
                        isFirst = IIf(c.SecondsSinceLast = -1, 1, 0), _
                        vTitle = c.Title, vID = c.Pubmed_ID





        Return tt.ToList().ToJSON

    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetBefore(ByVal ViewDT As DateTime,
                                        ByVal EarlierThen As String,
                                        ByVal LastIndex As Integer) As String
        ViewDT = ViewDT.ToLocalTime()
        Dim timesp As TimeSpan = ViewDT.TimeOfDay


        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)

        Dim dt As Date
        DateTime.TryParse(EarlierThen, dt)

        Dim db = New DataClassesFeedDataContext


        Dim tt = From c In db.GetArticleViewHistoryBefore(10, EarlierThen, LastIndex, p.Person_ID, ViewDT)
                Select vTimestamp = c.TimeStamp, vDateTime = c.TimeStamp.ToString("dd.MM.yyyy HH:mm:ss"), _
                        vInterval = GetIntervalFromSeconds(c.SecondsSinceLast), _
                        vEgo = GetIntervalFromSeconds((Now - c.TimeStamp).TotalSeconds) & " ago", _
                        isFirst = IIf(c.SecondsSinceLast = -1, 1, 0), _
                        vTitle = c.Title, vID = c.Pubmed_ID





        Return tt.ToList().ToJSON
    End Function


    Public Shared Function GetIntervalFromSeconds(ByVal seconds As Integer) As String
        Dim s As String
        Dim timesp As TimeSpan
        timesp = TimeSpan.FromSeconds(seconds)

        If timesp.Days > 1 Then
            s = s & timesp.Days & "d "
        End If
        If timesp.TotalDays < 100 Then
            If timesp.TotalHours > 1 Then
                s = s & timesp.Hours & "h "
            End If
            If timesp.TotalDays < 15 Then
                If timesp.TotalMinutes > 1 Then
                    s = s & timesp.Minutes & "m "
                End If
                If timesp.TotalDays < 2 Then
                    If timesp.TotalSeconds > 1 Then
                        s = s & timesp.Seconds & "s "
                    End If
                End If
            End If
        End If
       
        
        Return s
    End Function


End Class
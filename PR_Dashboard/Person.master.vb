﻿Imports DevExpress.Web
Imports System.Drawing
Imports BioWS.BLL.Extensions



Public Class Person
    Inherits MasterPage
    Private _curprofile As ProfileCommon
    Private ReadOnly Property CurProfile As ProfileCommon
        Get
            If _curprofile Is Nothing Then
                _curprofile = Me.GetProfile()
            End If
            Return _curprofile
        End Get
    End Property
    Private ReadOnly Property IsOwn As Boolean
        Get
            Return CurProfile.Person_ID = Me.QSInt32("PersonID")
        End Get
    End Property
    Private ReadOnly Property ViewedPersonID As Integer
        Get
            Return Me.QSInt32("PersonID")
        End Get
    End Property
    Private ReadOnly Property ViewerPersonID As Integer
        Get
            Return CurProfile.Person_ID
        End Get
    End Property



    Private Sub SetMI(menu As DevExpress.Web.ASPxMenu.ASPxMenu, itemname As String, url As String, text As String)
        Dim item As DevExpress.Web.ASPxMenu.MenuItem = menu.Items.FindByName(itemname)
        If url.IsNotEmpty() Then
            item.NavigateUrl = String.Format("{0}?PersonID={1}", url, Me.QS("PersonID"))
        End If
        If text.IsNotEmpty() Then
            item.Text = text
        End If
        item.Visible = True
    End Sub
    Private Sub HideMI(menu As DevExpress.Web.ASPxMenu.ASPxMenu, itemname As String)
        Dim item As DevExpress.Web.ASPxMenu.MenuItem = menu.Items.FindByName(itemname)
        item.Visible = False
    End Sub


    Private Function cRecommendationsFromViewed() As Integer
        Dim artrecta As New ArticlesDataSetTableAdapters.ArticleRecommendationsTableAdapter
        Return artrecta.GetCountByPersonID(Request.QueryString("PersonID"))
    End Function
    Private Function GetRecommendationToTOTAL() As Integer
        Dim persArtRecTA As New ArticlesDataSetTableAdapters.ArticleRecommendationsToPersonsTableAdapter
        Return persArtRecTA.GetCountRecommendationsToTOTAL(Request.QueryString("PersonID"))
    End Function
    Private Function GetRecommendationToNEW() As Integer
        Dim persArtRecTA As New ArticlesDataSetTableAdapters.ArticleRecommendationsToPersonsTableAdapter
        Return persArtRecTA.GetCountRecommendationsToUNVIEWED(Request.QueryString("PersonID"))
    End Function
    Private Function IsFollowed() As Boolean
        Dim followta As New PersonsDataSetTableAdapters.FollowingsTableAdapter
        Dim followt = followta.GetDataByPersonFollowedPerson(ViewerPersonID, ViewedPersonID)
        If followt.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub DataBindWidgets()
        Dim ta As New WidgetsDataSetTableAdapters.WidgetsTableAdapter
        Dim pid As Integer = IIf(Me.QS("PersonID").IsNullOrEmpty(), ViewerPersonID, ViewedPersonID)


        Dim t = ta.GetDataByPersonID_ON_MULT(ViewerPersonID)
        If t.Count > 0 Then
            Me.ASPxMenu2.Visible = True
            For i = 0 To t.Count - 1
                Dim item As New ASPxMenu.MenuItem
                item.Text = t.Item(i).Title
                item.Name = t.Item(i).Widget_ID
                item.NavigateUrl = String.Format("~\Account\ViewsHistoryWidgets.aspx?PersonID={0}&WidgetID={1}", pid, t.Item(i).Widget_ID)
                Me.ASPxMenu2.Items.Add(item)
            Next
        End If
        Dim item1 As New ASPxMenu.MenuItem
        item1.Text = "Distinct Pudmed IDs"
        item1.Name = 0
        item1.NavigateUrl = String.Format("~\Account\ViewsHistoryWidgets.aspx?PersonID={0}&WidgetID={1}", pid, 0)
        Me.ASPxMenu2.Items.Add(item1)
    End Sub
    Private Sub SetRecommendationsMenu()

        Dim recommends As String = IIf(IsOwn, "I recommend", "Recommends")
        Dim recommendedbyothers As String = IIf(IsOwn, "Recommended to me", "Recommended by others")

        Dim RecommendationsFromViewed As Integer = cRecommendationsFromViewed()
        If RecommendationsFromViewed > 0 Then
            SetMI(menuArticles, "miRecommends", "~\Account\Articles\RecommendationsFrom.aspx", String.Format("{0} ({1})", recommends, RecommendationsFromViewed))
        Else
            HideMI(menuArticles, "miRecommends")
        End If

        Dim recToNew As Integer = GetRecommendationToNEW()
        Dim recToTotal As Integer = GetRecommendationToTOTAL()

        If recToTotal > 0 Then
            If recToNew > 0 And IsOwn = True Then
                SetMI(menuArticles, "miRecommended", "~\Account\Articles\RecommendationsTo.aspx", String.Format("{0} ({1})", recommendedbyothers, recToNew))
            Else
                SetMI(menuArticles, "miRecommended", "~\Account\Articles\RecommendationsTo.aspx", String.Format("{0}", recommendedbyothers))
            End If
        Else
            HideMI(menuArticles, "miRecommended")
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load


        '! profile menu items
        SetMI(menuProfile, "PersonalProfile", "~\Account\ViewPerson.aspx", String.Empty)
        SetMI(menuProfile, "Statistics", "~\Account\ViewsHistoryStatistics.aspx", String.Empty)
        SetMI(menuProfile, "ActivityFeed", "~\Account\ViewsHistory.aspx", String.Empty)
        SetMI(menuProfile, "FollowedFeed", "~\Account\FollowedFeed.aspx", String.Empty)


        SetRecommendationsMenu()





        If Me.Page.NotPostBack() Then

            Dim pers As New BLL.Person(CInt(Request.QueryString("PersonID")))
            If pers.HasImage Then
                imgAvatar.ImageUrl = pers.Image200
            Else
                imgAvatar.Visible = False
            End If



            If IsOwn Then
                '! this is persons own page
                HideMI(menuActions, "miFollow")
                HideMI(menuActions, "miMessage")
                SetMI(menuActions, "miEditProfile", "", String.Empty)
                SetMI(menuActions, "miManageWidgets", "~/Account/MyWidgets.aspx", String.Empty)
                SetMI(menuActions, "miManageSites", "~/Account/ManageSites.aspx", String.Empty)
                SetMI(menuActions, "miEditProfile", "~/Account/Profile/EditProfile.aspx", String.Empty)

                If Not pers.IsAdmin Then
                    HideMI(menuActions, "miManagePRD")
                End If

            Else
                HideMI(menuActions, "miEditProfile")
                HideMI(menuActions, "miManageWidgets")
                HideMI(menuActions, "miManageSites")
                Me.menuArticles.Items.FindByName("miMyRequests").Visible = False

                If IsFollowed() Then
                    SetMI(menuActions, "miFollow", String.Empty, "Unfollow")
                Else
                    SetMI(menuActions, "miFollow", String.Empty, "Follow")
                End If
            End If
            DataBindWidgets()

        End If


    End Sub

    Protected Sub menuActions_ItemClick(source As Object, e As DevExpress.Web.ASPxMenu.MenuItemEventArgs) Handles menuActions.ItemClick
        Select Case e.Item.Name
            Case "miFollow"
                Dim _profile As New ProfileCommon
                _profile = _profile.GetProfile(HttpContext.Current.User.Identity.Name)
                Dim followta As New PersonsDataSetTableAdapters.FollowingsTableAdapter
                Dim followt = followta.GetDataByPersonFollowedPerson(_profile.Person_ID, Request.QueryString("PersonID"))
                If followt.Count > 0 Then
                    followta.Delete(_profile.Person_ID, Request.QueryString("PersonID"), 0)
                    e.Item.Text = "Follow"

                    Dim feedaction As New BLL.Feed.FeedActionPersonUnFollow(Request.QueryString("PersonID"))
                    feedaction.Save(_profile.Person_ID)

                Else
                    followta.Insert(_profile.Person_ID, Request.QueryString("PersonID"), 0)
                    e.Item.Text = "Unfollow"

                    Dim feedaction As New BLL.Feed.FeedActionPersonFollow(Request.QueryString("PersonID"))
                    feedaction.Save(_profile.Person_ID)
                End If
            Case "miMessage"
                Dim path As String = String.Format("~\Account\Messages\ComposeMessage.aspx?PersonToID={0}", Request.QueryString("PersonID"))
                Response.Redirect(path, False)
        End Select
    End Sub
End Class
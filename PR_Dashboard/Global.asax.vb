﻿Imports System.Web.SessionState
Imports DevExpress.Xpo
Imports DevExpress.Xpo.Metadata



Public Class Global_asax
    Inherits System.Web.HttpApplication

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
        ' Code that runs on application startup

        RegularHelper.InitiateTick()

        Dim parsingHelper As New BLL.ParsingHelper()
        Dim savedparsingHelper As New BLL.DelayedParsing.SavedParserHelper()


        'Dim dict As XPDictionary = New DevExpress.Xpo.Metadata.ReflectionDictionary()
        '' Initialize the XPO dictionary. 
        'dict.GetDataStoreSchema(GetType(pmMeSH).Assembly)

        'Dim store As DevExpress.Xpo.DB.IDataStore = DevExpress.Xpo.XpoDefault.GetConnectionProvider("data source=195.128.125.101,1433;User ID=claimer_sql_user2;Password=hrc7hv8x3p!;database=bio_data", _
        '     DevExpress.Xpo.DB.AutoCreateOption.SchemaAlreadyExists)
        'DevExpress.Xpo.XpoDefault.DataLayer = New DevExpress.Xpo.ThreadSafeDataLayer(dict, store)
        'DevExpress.Xpo.XpoDefault.Session = Nothing




    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

End Class
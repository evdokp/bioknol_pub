﻿Public Class FinishRegistration
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Redirect(String.Format("~\LoginRedirect.aspx?Person_ID={0}&finreg=1", Request.QueryString("UserID")))
    End Sub

End Class
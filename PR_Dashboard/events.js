﻿{
'dateTimeFormat': 'iso8601',
'wikiURL': "http://simile.mit.edu/shelf/",
'wikiSection': "Simile Cubism Timeline",

'events' : [
        {'start': '1924',
        'title': 'Barfusserkirche',
        'description': 'by Lyonel Feininger, American/German Painter, 1871-1956',
        'image': 'http://images.allposters.com/images/AWI/NR096_b.jpg',
        'link': 'http://www.allposters.com/-sp/Barfusserkirche-1924-Posters_i1116895_.htm'
        },


        {'start': '1913',
        'end': '1929',
        'title': 'Three Figures',
        'description': 'by Kasimir Malevich, Ukrainian Painter, 1878-1935',
        'image': 'http://images.allposters.com/images/BRGPOD/75857_b.jpg',
        'link': 'http://www.allposters.com/-sp/Three-Figures-1913-28-Posters_i1349989_.htm'
        }

]
}
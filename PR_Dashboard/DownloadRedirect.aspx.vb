﻿Imports BioWS.BLL.Extensions


Public Class DownloadRedirect
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim offersTA As New ArticlesDataSetTableAdapters.ArticleOffersTableAdapter
        Dim offersTBL = offersTA.GetDataByPubmedIDwithFiles(Me.QSInt32("PubmedID"))
        If offersTBL.Count > 0 Then
            Dim offerRow = offersTBL.Item(0)
            Response.Redirect("~/Download.ashx?vFileName=" & HttpUtility.UrlEncode(offerRow.FileName) & "&vOriginalName=" & HttpUtility.UrlEncode(offerRow.OriginalName))
        Else
            Response.Redirect("~/Default.aspx")
        End If
    End Sub

End Class
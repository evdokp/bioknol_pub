﻿Imports BioWS.BLL.Extensions

Public Class MyActivity
    Inherits System.Web.UI.Page
    Private ReadOnly Property ViewedObjectType As String
        Get
            Return Me.QS("objtype")
        End Get
    End Property
    Private ReadOnly Property ViewedObjectID As Integer
        Get
            Return Me.QSInt32("objid")
        End Get
    End Property
    Private ReadOnly Property IsGroup As Boolean
        Get
            Return ViewedObjectType = "g"
        End Get
    End Property

    Private ReadOnly Property PageTitle As String
        Get
            If Me.QSInt32("followed") = 1 Then
                Return "Followed activity"
            Else
                Return "Activity"
            End If
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfIsFollowed.Value = Me.QS("followed")


        hfPageOpenDateTime.Value = Now.ToString
        Dim Content2 As ContentPlaceHolder = CType(Me.Master.Master.FindControl("cph1"), ContentPlaceHolder)
        Dim hfCurPage As HiddenField = CType(Content2.FindControl("hfCurPage"), HiddenField)
        hfCurPage.Value = PageTitle
        litCurPage.Text = PageTitle

        If IsGroup Then

        Else
            Dim pers As New BLL.Person(ViewedObjectID)

            Me.Page.Title = pers.PageHeader & " | " & PageTitle.ToLower()


            hlViewedPerson.Text = pers.PageHeader
            hlViewedPerson.NavigateUrl = pers.URL
            hfPersonID.Value = ViewedObjectID
        End If



    End Sub



    Public Class FeedItem
        Public Title As String
        Public ActionTime As String
        Public ViewType As String
        Public AuthorsString As String
        Public JournalTitle As String
        Public PubYear As Integer?
        Public PubmedID As Integer?
        Public DOI As String
        Public ViewerPersonID As Integer
        Public DoiUrlEnc As String
        Public HasDownload As Boolean
        Public RecommendationsCount As Integer
        Public CommentsCount As Integer
        Public LinkID As String
        Public ReadsCount As Integer
        Public IsFavorite As Boolean
        Public IsTrash As Boolean
        Public WhoViewedPersonID As Integer
        Public WhoViewedUserName As String
    End Class

    Private Shared Function GetFeedUsual(ByVal query As String, ByVal feedType As Integer, ByVal PersonID As Integer, ByVal EarlierThen As String, ByVal LastIndex As Integer, ByVal db As FeedDataClassesDataContext, ByVal p As ProfileCommon, ByVal recta As PersonsDataSetTableAdapters.v_PersonsTableAdapter) As IEnumerable(Of FeedItem)

        If query.Length > 0 Then
            Dim foundCount As Integer
            Dim historystring As String = ArticlesDA.GetDistinctHistoryPMIDS_byPerson(PersonID, New Date(2009, 1, 1), DateTime.Today.AddDays(1)).ListToString(",")
            Dim pmlist As List(Of Integer) = BLL.PubMed.GetPMIDlistByQuery(query, historystring, foundCount)
            Dim pmliststr As String = IIf(pmlist.Count = 0, "-1000", pmlist.ListToString(","))


            Dim res1 = (From feeditem In db.feed_GetFeedQuery(20, EarlierThen, LastIndex, PersonID, p.Person_ID, feedType, query, pmliststr)
                                  Select feeditem).ToList()

            Return From feeditem In res1
                               Select New FeedItem() With {.ActionTime = CType(feeditem.ActionTime, DateTime).ToString("HH:mm"), _
                                                                 .AuthorsString = feeditem.AuthorsString, _
                                                                 .Title = feeditem.Title, _
                                                                 .ViewType = feeditem.ViewType, _
                                                                 .JournalTitle = feeditem.JournalTitle, _
                                                                 .PubYear = feeditem.PubYear, _
                                                                 .PubmedID = feeditem.PubmedID, _
                                                                 .DOI = feeditem.DOI, _
                                                                 .ViewerPersonID = p.Person_ID,
                                                                 .DoiUrlEnc = HttpUtility.UrlEncode("http://dx.doi.org/" & feeditem.DOI), _
                                                                 .HasDownload = feeditem.HasDownload, _
                                                                 .LinkID = feeditem.LinkID, _
                                                                 .RecommendationsCount = feeditem.RecommendationsCount, _
                                                                 .CommentsCount = feeditem.CommentsCount, _
                                                                 .WhoViewedPersonID = 0}

        Else
            Dim res1 = (From feeditem In db.feed_GetFeed(20, EarlierThen, LastIndex, PersonID, p.Person_ID, feedType)
                                  Select feeditem).ToList()


            Return From feeditem In res1
                               Select New FeedItem() With {.ActionTime = CType(feeditem.ActionTime, DateTime).ToString("HH:mm"), _
                                                                 .AuthorsString = feeditem.AuthorsString, _
                                                                 .Title = feeditem.Title, _
                                                                 .ViewType = feeditem.ViewType, _
                                                                 .JournalTitle = feeditem.JournalTitle, _
                                                                 .PubYear = feeditem.PubYear, _
                                                                 .PubmedID = feeditem.PubmedID, _
                                                                 .DOI = feeditem.DOI, _
                                                                 .ViewerPersonID = p.Person_ID,
                                                                 .DoiUrlEnc = HttpUtility.UrlEncode("http://dx.doi.org/" & feeditem.DOI), _
                                                                 .HasDownload = feeditem.HasDownload, _
                                                                 .LinkID = feeditem.LinkID, _
                                                                 .RecommendationsCount = feeditem.RecommendationsCount, _
                                                                 .CommentsCount = feeditem.CommentsCount, _
                                                                 .WhoViewedPersonID = 0}
        End If




        'Dim res1 = (From feeditem In db.feed_GetFeed(20, EarlierThen, LastIndex, PersonID, p.Person_ID, feedType)
        '                              Select feeditem).ToList()








    End Function


    Private Shared Function GetFeedFollowed(ByVal query As String, ByVal feedType As Integer, ByVal PersonID As Integer, ByVal EarlierThen As String, ByVal LastIndex As Integer, ByVal db As FeedDataClassesDataContext, ByVal p As ProfileCommon, ByVal recta As PersonsDataSetTableAdapters.v_PersonsTableAdapter) As IEnumerable(Of FeedItem)


        If query.Length > 0 Then
            Dim foundCount As Integer
            Dim historystring As String = ArticlesDA.GetDistinctHistoryPMIDS_byPerson(PersonID, New Date(2009, 1, 1), DateTime.Today.AddDays(1)).ListToString(",")
            Dim pmlist As List(Of Integer) = BLL.PubMed.GetPMIDlistByQuery(query, historystring, foundCount)
            Dim pmliststr As String = IIf(pmlist.Count = 0, "-1000", pmlist.ListToString(","))

            Return From feeditem In db.feed_GetFeedFollowedQuery(20, EarlierThen, LastIndex, PersonID, p.Person_ID, feedType, query, pmliststr)
                      Select New FeedItem() With {.ActionTime = CType(feeditem.ActionTime, DateTime).ToString("HH:mm"), _
                                                        .AuthorsString = feeditem.AuthorsString, _
                                                        .Title = feeditem.Title, _
                                                        .ViewType = feeditem.ViewType, _
                                                        .JournalTitle = feeditem.JournalTitle, _
                                                        .PubYear = feeditem.PubYear, _
                                                        .PubmedID = feeditem.PubmedID, _
                                                        .DOI = feeditem.DOI, _
                                                        .ViewerPersonID = p.Person_ID,
                                                        .DoiUrlEnc = HttpUtility.UrlEncode("http://dx.doi.org/" & feeditem.DOI), _
                                                        .HasDownload = feeditem.HasDownload, _
                                                        .LinkID = feeditem.LinkID, _
                                                          .RecommendationsCount = feeditem.RecommendationsCount, _
                                                                 .CommentsCount = feeditem.CommentsCount, _
                                                  .WhoViewedPersonID = feeditem.WhoViewedPerson_ID, .WhoViewedUserName = feeditem.DisplayName}

        Else
            Return From feeditem In db.feed_GetFeedFollowed(20, EarlierThen, LastIndex, PersonID, p.Person_ID, feedType)
                        Select New FeedItem() With {.ActionTime = CType(feeditem.ActionTime, DateTime).ToString("HH:mm"), _
                                                          .AuthorsString = feeditem.AuthorsString, _
                                                          .Title = feeditem.Title, _
                                                          .ViewType = feeditem.ViewType, _
                                                          .JournalTitle = feeditem.JournalTitle, _
                                                          .PubYear = feeditem.PubYear, _
                                                          .PubmedID = feeditem.PubmedID, _
                                                          .DOI = feeditem.DOI, _
                                                          .ViewerPersonID = p.Person_ID,
                                                          .DoiUrlEnc = HttpUtility.UrlEncode("http://dx.doi.org/" & feeditem.DOI), _
                                                          .HasDownload = feeditem.HasDownload, _
                                                          .LinkID = feeditem.LinkID, _
                                                                              .RecommendationsCount = feeditem.RecommendationsCount, _
                                                                 .CommentsCount = feeditem.CommentsCount, _
                                                    .WhoViewedPersonID = feeditem.WhoViewedPerson_ID, .WhoViewedUserName = feeditem.DisplayName}

        End If






    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetFeed(ByVal isFollowed As Integer, ByVal feedType As Integer, ByVal PersonID As Integer,
                                   ByVal EarlierThen As String,
                                    ByVal LastIndex As Integer, ByVal query As String) As String


        Dim dt As Date
        DateTime.TryParse(EarlierThen, dt)

        Dim db = New FeedDataClassesDataContext

        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)

        Using recta As New PersonsDataSetTableAdapters.v_PersonsTableAdapter()

            Dim tt As IEnumerable(Of FeedItem)
            If isFollowed = 1 Then
                tt = GetFeedFollowed(query, feedType, PersonID, EarlierThen, LastIndex, db, p, recta)
            Else
                tt = GetFeedUsual(query, feedType, PersonID, EarlierThen, LastIndex, db, p, recta)
            End If





            Dim tt2 = tt.ToList()
            Dim linkids = (From items In tt2
                                                Select items.LinkID).Distinct().ToList()

            FillFeedCollectionWithCounts(tt2, db, linkids, p.Person_ID)
            FillFeedCollectionWithFavorite(tt2, db, linkids, p.Person_ID)
            FillFeedCollectionWithTrash(tt2, db, linkids, p.Person_ID)


            Dim s As String = tt2.ToJSON
            Return s
        End Using

    End Function
    Protected Shared Sub FillFeedCollectionWithCounts(ByRef input As List(Of FeedItem),
        ByRef db As FeedDataClassesDataContext,
        ByVal linkids As List(Of String), ByVal viewerPersonID As Integer)




        Dim viewscounts = (From viewsc In db.v_ViewsCountByLinkIDByPersonIDs
                                                    Where viewsc.PersonID = viewerPersonID And linkids.Contains(viewsc.LinkID)
                                                    Select viewsc).ToList()


        For Each inputitem In input
            Dim vc = (From viewsc In viewscounts Where viewsc.LinkID = inputitem.LinkID Select viewsc.ViewsCount).FirstOrDefault()
            If vc.HasValue Then
                inputitem.ReadsCount = vc
            End If
        Next
    End Sub
    Protected Shared Sub FillFeedCollectionWithFavorite(ByRef input As List(Of FeedItem),
    ByRef db As FeedDataClassesDataContext,
    ByVal linkids As List(Of String), ByVal viewerPersonID As Integer)

        Dim taglinks = (From taglink In db.LinksTagsToArticleIDs
                                                    Where taglink.PersonID = viewerPersonID And linkids.Contains(taglink.LinkID)
                                                    Select taglink).ToList()

        For Each inputitem In input
            Dim hasTag = (From taglink In taglinks Where taglink.LinkID = inputitem.LinkID And taglink.PersonID = viewerPersonID And taglink.TagID = 1 Select taglink).Count() > 0
            inputitem.IsFavorite = hasTag
        Next
    End Sub
    Protected Shared Sub FillFeedCollectionWithTrash(ByRef input As List(Of FeedItem),
    ByRef db As FeedDataClassesDataContext,
    ByVal linkids As List(Of String), ByVal viewerPersonID As Integer)

        Dim taglinks = (From taglink In db.LinksTagsToArticleIDs
                                                    Where taglink.PersonID = viewerPersonID And linkids.Contains(taglink.LinkID)
                                                    Select taglink).ToList()

        For Each inputitem In input
            Dim hasTag = (From taglink In taglinks Where taglink.LinkID = inputitem.LinkID And taglink.PersonID = viewerPersonID And taglink.TagID = 2 Select taglink).Count() > 0
            inputitem.IsTrash = hasTag
        Next
    End Sub

End Class
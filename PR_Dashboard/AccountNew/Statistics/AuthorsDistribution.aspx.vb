﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.Web.UI.DataVisualization.Charting.Utilities
Imports BioWS.BLL.Extensions


Public Class AuthorsDistribution
    Inherits System.Web.UI.Page

    ' Fields...

    Private ReadOnly Property ViewedObjectType As String
        Get
            Return Me.QS("objtype")
        End Get
    End Property
    Private ReadOnly Property ViewedObjectID As Integer
        Get
            Return Me.QSInt32("objid")
        End Get
    End Property
    Private ReadOnly Property IsGroup As Boolean
        Get
            Return ViewedObjectType = "g"
        End Get
    End Property
    Private ReadOnly Property StartDate As Date?
        Get
            Return CType(Me.QS("StartDate"), Date?)
        End Get
    End Property
    Private ReadOnly Property EndDate As Date?
        Get
            Return CType(Me.QS("EndDate"), Date?)
        End Get
    End Property


    Protected Sub DataBindChart()

        Dim s1 As Series = Me.Chart1.Series("Series1")
        s1.Points.Clear()

        Dim ta As New ChartDataSetTableAdapters.DataTable2TableAdapter
        Dim t As ChartDataSet.DataTable2DataTable

        If IsGroup Then
            If Me.ddl_type.SelectedValue = 1 Then
                t = ta.Get_Authors_ByGroupIDandTwoDates_ByDistinctPMIDs(ddl_topN.SelectedValue, StartDate, EndDate, ViewedObjectID)
            Else
                t = ta.Get_Authors_ByGroupIDandTwoDatesByViewsCount(Me.ddl_topN.SelectedValue, StartDate, EndDate, ViewedObjectID)
            End If
        Else
            If ddl_type.SelectedValue = 1 Then
                t = ta.Get_Authors_ByPersonIDandTwoDates_ByDistinctPMIDs(Me.ddl_topN.SelectedValue, ViewedObjectID, StartDate, EndDate)
            Else
                t = ta.Get_Authors_ByPersonIDandTwoDates_ByViewsCount(Me.ddl_topN.SelectedValue, ViewedObjectID, StartDate, EndDate)
            End If
        End If





        Dim rnd As New Random
        Dim k As Integer
        For i = 0 To t.Count - 1
            k = t.Count - 1 - i
            Dim r = t.Item(k)
            Dim dp1 As New DataPoint
            dp1.SetValueXY(r.vTitle, r.vCount)
            s1.Points.Add(dp1)
        Next
        Me.Chart1.Height = 80 + 14 * t.Count
    End Sub
    Protected Sub CreateChart()
        Dim cha As ChartArea = Me.Chart1.ChartAreas("ChartArea1")
        cha.BorderColor = Drawing.Color.LightGray
        cha.BorderWidth = 1
        cha.BorderDashStyle = ChartDashStyle.Solid
        cha.AxisX.MajorGrid.Enabled = False
        cha.AxisY.MajorGrid.LineColor = Drawing.Color.LightGray
        cha.AxisY.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha.AxisX.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha.AxisY.LineColor = Drawing.Color.LightGray
        cha.AxisX.LineColor = Drawing.Color.LightGray
        cha.AxisX.IntervalAutoMode = False
        cha.AxisX.Interval = 1
        cha.AxisX.LabelStyle.Interval = 1







        Dim s1 As Series = Me.Chart1.Series("Series1")
        s1.ChartType = SeriesChartType.Bar
        s1.CustomProperties = "DrawingStyle=LightToDark"
        s1.Color = Drawing.Color.Orange
        s1.IsValueShownAsLabel = True
        s1.Label = "#VAL"


        cha.Position.Auto = False

        cha.Position.Y = 0
        cha.Position.Height = 100
        cha.Position.X = 5
        cha.Position.Width = 90






    End Sub

    Private Sub Page_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataBinding
        DataBindChart()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CreateChart()
        DataBindChart()

        Me.Chart1.Width = 675

    End Sub



End Class
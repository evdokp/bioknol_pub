﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.Web.UI.DataVisualization.Charting.Utilities
Imports System.Drawing
Imports System.Data
Public Class ChildGroupsActivity
    Inherits System.Web.UI.Page

    Protected Sub DataBindChart()

        DatabindBarChart()
    End Sub
    Private Sub DatabindBarChart()
        Dim s1 As Series = Me.Chart1.Series("Series1")
        s1.Points.Clear()
        Dim type As String
        If Me.ddl_type.SelectedValue = 1 Then
            type = "pmdistinct"
        Else
            type = "totalviews"
        End If


        Dim query As String


        query = String.Format("EXEC	 [dbo].[GetGroupsViewsByTwoDatesParentGroupID] '{0}', " & ddl_topN.SelectedValue & ",'{1}','{2}',{3}",
                              type,
                    Request.QueryString("StartDate"),
                    Request.QueryString("EndDate"),
                    Request.QueryString("ObjectID"))


        Dim dt1 As DataTable = BLL.SQLHelper.GetDT_AllStrings(query)


        Dim rnd As New Random
        Dim k As Integer
        For i = 0 To dt1.Rows.Count - 1
            Dim dataRow = dt1.Rows(i)
            Dim dp1 As New DataPoint
            dp1.SetValueXY(dataRow(1), dataRow(2))
            s1.Points.Add(dp1)
        Next
        Me.Chart1.Height = 80 + 14 * dt1.Rows.Count


        DatabindTimeSeriesChart(dt1)
    End Sub
    Private Sub DatabindTimeSeriesChart(ByVal dt1 As DataTable)


        '! add series
        Dim datesTA As New ChartDataSetTableAdapters.DataTable1TableAdapter
        Dim datesT As New ChartDataSet.DataTable1DataTable



        Chart2.Series.Clear()
        For Each groupRow As DataRow In dt1.Rows
            Dim groupSerie As New Series
            groupSerie.LegendText = groupRow(1)
            groupSerie.ChartArea = "ChartArea1"
            Chart2.Series.Add(groupSerie)
            groupSerie.ChartType = SeriesChartType.StackedArea
            groupSerie.XValueType = ChartValueType.Date

            If Me.ddl_type.SelectedValue = 1 Then
                datesT = datesTA.Get_PMIDsDistinct_ByGroupIDByTwoDates(Request.QueryString("StartDate"), Request.QueryString("EndDate"), groupRow(0))
                Chart2.Titles(0).Text = "Distinct Pubmed IDs"

            Else
                datesT = datesTA.Get_TotalViews_ByGroupIDByTwoDates(Request.QueryString("StartDate"), Request.QueryString("EndDate"), groupRow(0))
                Chart2.Titles(0).Text = "Total views"
            End If


            Dim curDT As DateTime = Request.QueryString("StartDate")

            While curDT < Request.QueryString("EndDate")
                Dim dp1 As New DataPoint()
                Dim curDatesT = From d In datesT
                                Select d Where d.vDate = curDT

                If curDatesT.Count > 0 Then
                    dp1.SetValueXY(curDT, curDatesT.ToList()(0).vCount)
                Else
                    dp1.SetValueXY(curDT, 0)
                End If

                groupSerie.Points.Add(dp1)
                curDT = curDT.AddDays(1)
            End While

        Next




        ApplyGroupping()

    End Sub

    Private Sub ApplyGroupping()
        Dim it As IntervalType
        Dim int As Integer

        Select Case ddl_grint.SelectedValue
            Case "Day"
                it = IntervalType.Days
                int = 1
            Case "Week"
                it = IntervalType.Weeks
                int = 1
            Case "2 Weeks"
                it = IntervalType.Weeks
                int = 2
            Case "Month"
                it = IntervalType.Months
                int = 1
        End Select
        Me.Chart2.ChartAreas("ChartArea1").AxisX.IntervalType = it
        Me.Chart2.ChartAreas("ChartArea1").AxisX.LabelStyle.IntervalType = it
        For Each ser As Series In Chart2.Series
            Me.Chart2.DataManipulator.Group("SUM, X:LAST", int, it, ser)
        Next
    End Sub
    Private Sub ddl_grint_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_grint.SelectedIndexChanged
        ApplyGroupping()
    End Sub
    Protected Sub CreateChart()
        '!+ bar chart
        Dim cha As ChartArea = Me.Chart1.ChartAreas("ChartArea1")
        cha.BorderColor = Drawing.Color.LightGray
        cha.BorderWidth = 1
        cha.BorderDashStyle = ChartDashStyle.Solid
        cha.AxisX.MajorGrid.Enabled = False
        cha.AxisY.MajorGrid.LineColor = Drawing.Color.LightGray
        cha.AxisY.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha.AxisX.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha.AxisY.LineColor = Drawing.Color.LightGray
        cha.AxisX.LineColor = Drawing.Color.LightGray
        cha.AxisX.IntervalAutoMode = False
        cha.AxisX.Interval = 1
        cha.AxisX.LabelStyle.Interval = 1

        Dim s1 As Series = Me.Chart1.Series("Series1")
        s1.ChartType = SeriesChartType.Bar
        s1.CustomProperties = "DrawingStyle=LightToDark"
        s1.Color = Drawing.Color.Orange
        s1.IsValueShownAsLabel = True
        s1.Label = "#VAL"

        cha.Position.Auto = False
        cha.Position.Y = 0
        cha.Position.Height = 100
        cha.Position.X = 5
        cha.Position.Width = 90

        '!+ timeseries chart

        Dim cha2 As ChartArea = Me.Chart2.ChartAreas("ChartArea1")
        cha2.BorderColor = Drawing.Color.LightGray
        cha2.BorderWidth = 1
        cha2.BorderDashStyle = ChartDashStyle.Solid
        cha2.AxisY.MajorGrid.LineColor = Drawing.Color.LightGray
        cha2.AxisX.MajorGrid.LineColor = Drawing.Color.LightGray
        cha2.AxisY.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha2.AxisX.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha2.AxisY.LineColor = Drawing.Color.LightGray
        cha2.AxisX.LineColor = Drawing.Color.LightGray
        cha2.AxisX.IntervalType = DateTimeIntervalType.Days
        cha2.AxisX.IsMarginVisible = False

        Dim title As New Title
        Chart2.Titles.Add(title)

        Dim legend As New Legend
        legend.Docking = Docking.Bottom
        Chart2.Legends.Add(legend)


    End Sub




    Private Sub Page_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataBinding
        DataBindChart()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CreateChart()
        DataBindChart()

        Me.Chart1.Width = 675
        Me.Chart2.Width = 675
    End Sub



End Class
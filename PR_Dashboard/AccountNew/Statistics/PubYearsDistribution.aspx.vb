﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.Web.UI.DataVisualization.Charting.Utilities
Imports BioWS.BLL.Extensions

Public Class PubYearsDistribution
    Inherits System.Web.UI.Page
    Private ReadOnly Property ViewedObjectType As String
        Get
            Return Me.QS("objtype")
        End Get
    End Property
    Private ReadOnly Property ViewedObjectID As Integer
        Get
            Return Me.QSInt32("objid")
        End Get
    End Property
    Private ReadOnly Property IsGroup As Boolean
        Get
            Return ViewedObjectType = "g"
        End Get
    End Property
    Private ReadOnly Property StartDate As Date?
        Get
            Return CType(Me.QS("StartDate"), Date?)
        End Get
    End Property
    Private ReadOnly Property EndDate As Date?
        Get
            Return CType(Me.QS("EndDate"), Date?)
        End Get
    End Property

    Protected Sub DataBindChart()
        Dim pointsExtraDiff As Integer


        Dim s1 As Series = Me.Chart1.Series("Series1")
        Dim s2 As Series = Me.Chart1.Series("Series2")
        s1.Points.Clear()
        s2.Points.Clear()

        Dim ta As New ChartDataSetTableAdapters.DataTable2TableAdapter
        Dim t As ChartDataSet.DataTable2DataTable
        Dim t2 As ChartDataSet.DataTable2DataTable

        If IsGroup Then
            t = ta.Get_Years_ByGroupIDandTwoDates_ByDistinctPMIDs(StartDate, EndDate, ViewedObjectID)
            t2 = ta.Get_Years_ByGroupIDandTwoDates_ByViewsCount(StartDate, EndDate, ViewedObjectID)
        Else
            t = ta.Get_Years_ByPersonIDandTwoDates_ByDistinctPMIDs(ViewedObjectID, StartDate, EndDate)
            t2 = ta.Get_Years_ByPersonIDandTwoDates_ByViewsCount(ViewedObjectID, StartDate, EndDate)
        End If









        Dim LastDateTimeWithDataPoint As Date

        If t.Count > 0 Then


            If Not t.Item(0).IsvTitleNull = True Then
                If Not t.Item(0).vTitle = String.Empty Then
                    LastDateTimeWithDataPoint = New Date(t.Item(0).vTitle, 1, 1)
                Else
                    LastDateTimeWithDataPoint = New Date(t.Item(1).vTitle, 1, 1)
                End If
            Else
                LastDateTimeWithDataPoint = New Date(t.Item(1).vTitle, 1, 1)
            End If


            For i = 0 To t.Count - 1
                Dim r = t.Item(i)
                Dim dp1 As New DataPoint
                If r.IsvTitleNull Then
                Else
                    If r.vTitle = String.Empty Then
                    Else
                        pointsExtraDiff = DateDiff(DateInterval.Year, LastDateTimeWithDataPoint.Date, New Date(r.vTitle, 1, 1)) - 1
                        For k = 1 To pointsExtraDiff
                            Dim ep As New DataPoint
                            ep.SetValueXY(LastDateTimeWithDataPoint.AddYears(k).Date, 0)
                            ep.IsValueShownAsLabel = False
                            s1.Points.Add(ep)
                        Next
                        dp1.SetValueXY(New Date(r.vTitle, 1, 1), r.vCount)
                        dp1.IsValueShownAsLabel = True
                        dp1.Label = "#VAL"
                        s1.Points.Add(dp1)

                        LastDateTimeWithDataPoint = New Date(r.vTitle, 1, 1)
                    End If
                End If
            Next

        End If


        Dim ta2 As New ChartDataSetTableAdapters.DataTable2TableAdapter

        If t2.Count > 0 Then


            If Not t2.Item(0).IsvTitleNull = True Then
                If Not t2.Item(0).vTitle = String.Empty Then
                    LastDateTimeWithDataPoint = New Date(t2.Item(0).vTitle, 1, 1)
                Else
                    LastDateTimeWithDataPoint = New Date(t2.Item(1).vTitle, 1, 1)
                End If
            Else
                LastDateTimeWithDataPoint = New Date(t2.Item(1).vTitle, 1, 1)
            End If

            For i = 0 To t2.Count - 1
                Dim r = t2.Item(i)
                Dim dp2 As New DataPoint
                If r.IsvTitleNull Then
                Else
                    If r.vTitle = String.Empty Then
                    Else
                        pointsExtraDiff = DateDiff(DateInterval.Year, LastDateTimeWithDataPoint.Date, New Date(r.vTitle, 1, 1)) - 1
                        For k = 1 To pointsExtraDiff
                            Dim ep As New DataPoint
                            ep.SetValueXY(LastDateTimeWithDataPoint.AddYears(k).Date, 0)
                            ep.IsValueShownAsLabel = False
                            s2.Points.Add(ep)
                        Next
                        dp2.SetValueXY(New Date(r.vTitle, 1, 1), r.vCount)
                        dp2.IsValueShownAsLabel = True
                        dp2.Label = "#VAL"
                        s2.Points.Add(dp2)
                        LastDateTimeWithDataPoint = New Date(r.vTitle, 1, 1)
                    End If
                End If
            Next
        End If

    End Sub
    Protected Sub CreateChart()
        Dim cha As ChartArea = Me.Chart1.ChartAreas("ChartArea1")
        cha.BorderColor = Drawing.Color.LightGray
        cha.BorderWidth = 1
        cha.BorderDashStyle = ChartDashStyle.Solid
        cha.AxisX.MajorGrid.Enabled = False
        cha.AxisY.MajorGrid.LineColor = Drawing.Color.LightGray
        cha.AxisY.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha.AxisX.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha.AxisX.MajorTickMark.Enabled = True
        cha.AxisY.LineColor = Drawing.Color.LightGray
        cha.AxisX.LineColor = Drawing.Color.LightGray
        cha.AxisX.IntervalAutoMode = False
        cha.AxisX.IntervalType = DateTimeIntervalType.Years
        cha.AxisX.Interval = 1
        cha.AxisX.LabelStyle.Format = "{0:yyyy}"
        cha.AxisX.IsLabelAutoFit = True
        cha.AxisX.LabelAutoFitStyle = LabelAutoFitStyles.LabelsAngleStep30

        Dim s1 As Series = Me.Chart1.Series("Series1")
        s1.ChartType = SeriesChartType.Area
        s1.XValueType = ChartValueType.Date
        s1.LegendText = "Distinct PMIDs"
        Dim s2 As Series = Me.Chart1.Series("Series2")
        s2.ChartType = SeriesChartType.Area
        s2.LegendText = "Views count"
        s2.XValueType = ChartValueType.Date



        Dim l As New Legend
        l.Docking = Docking.Bottom
        l.IsDockedInsideChartArea = False
        Chart1.Legends.Add(l)







    End Sub

    Private Sub Page_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataBinding
        DataBindChart()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CreateChart()
        DataBindChart()
        Me.Chart1.Width = 675

    End Sub



End Class
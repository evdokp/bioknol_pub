﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.Web.UI.DataVisualization.Charting.Utilities
Imports System.Drawing
Imports BioWS.BLL.Extensions


Public Class DomainsDistribution
    Inherits System.Web.UI.Page


    Private ReadOnly Property ViewedObjectType As String
        Get
            Return Me.QS("objtype")
        End Get
    End Property
    Private ReadOnly Property ViewedObjectID As Integer
        Get
            Return Me.QSInt32("objid")
        End Get
    End Property
    Private ReadOnly Property IsGroup As Boolean
        Get
            Return ViewedObjectType = "g"
        End Get
    End Property
    Private ReadOnly Property StartDate As Date?
        Get
            Return CType(Me.QS("StartDate"), Date?)
        End Get
    End Property
    Private ReadOnly Property EndDate As Date?
        Get
            Return CType(Me.QS("EndDate"), Date?)
        End Get
    End Property

    Protected Sub DataBindChart()
        DatabindTimeSeriesChart()
        DatabindBarChart()
    End Sub
    Private Sub DatabindBarChart()
        Dim s1 As Series = Me.Chart1.Series("Series1")
        s1.Points.Clear()

        Dim ta As New ChartDataSetTableAdapters.DataTable2TableAdapter
        Dim t As ChartDataSet.DataTable2DataTable

        If IsGroup Then
            If ddl_type.SelectedValue = 1 Then
                t = ta.Get_Domains_ByGroupIDandTwoDates_ByDistinctPMIDs(ddl_topN.SelectedValue, StartDate, EndDate, ViewedObjectID)
            Else
                t = ta.Get_Domains_ByGroupIDandTwoDates_ByViewsCount(Me.ddl_topN.SelectedValue, StartDate, EndDate, ViewedObjectID)
            End If
        Else
            If ddl_type.SelectedValue = 1 Then
                t = ta.Get_Domains_ByPersonIDandTwoDates_ByDistinctPMIDs(Me.ddl_topN.SelectedValue, ViewedObjectID, StartDate, EndDate)
            Else
                t = ta.Get_Domains_ByPersonIDandTwoDates_ByViewsCount(Me.ddl_topN.SelectedValue, ViewedObjectID, StartDate, EndDate)
            End If
        End If





        Dim rnd As New Random
        Dim k As Integer
        For i = 0 To t.Count - 1
            k = t.Count - 1 - i
            Dim r = t.Item(k)
            Dim dp1 As New DataPoint
            dp1.SetValueXY(r.vTitle, r.vCount)
            s1.Points.Add(dp1)
        Next
        Me.Chart1.Height = 80 + 14 * t.Count
    End Sub
    Private Sub DatabindTimeSeriesChart()
        '! get domains list
        Dim domainsTA As New MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter
        Dim domainsT As New MonitoredSitesDataSet.MonitoredSitesDataTable

        If IsGroup Then
            domainsT = domainsTA.getTopNDomainsByGroupIDandTwoDates(5, ViewedObjectID, StartDate, EndDate)
        Else
            domainsT = domainsTA.getTopNDomainsByPersonIDandTwoDates(5, StartDate, EndDate, ViewedObjectID)
        End If



        '! add series
        Dim datesTA As New ChartDataSetTableAdapters.DataTable1TableAdapter
        Dim datesT As New ChartDataSet.DataTable1DataTable
        Chart2.Series.Clear()
        For Each domainRow As MonitoredSitesDataSet.MonitoredSitesRow In domainsT
            Dim domainSerie As New Series
            domainSerie.LegendText = domainRow.SiteURL
            domainSerie.ChartArea = "ChartArea1"
            Chart2.Series.Add(domainSerie)
            domainSerie.ChartType = SeriesChartType.StackedArea
            domainSerie.XValueType = ChartValueType.Date

            If IsGroup Then
                datesT = datesTA.GetViewsByGroupIDTwoDatesMonitoredSiteID(ViewedObjectID, StartDate, EndDate, domainRow.ID)
            Else
                datesT = datesTA.GetViewsByPersonIDTwoDatesMonitoredSiteID(StartDate, EndDate, domainRow.ID, ViewedObjectID)
            End If
            

            Dim curDT As DateTime = StartDate

            While curDT < EndDate
                Dim dp1 As New DataPoint()
                Dim curDatesT = From d In datesT
                                Select d Where d.vDate = curDT

                If curDatesT.Count > 0 Then
                    dp1.SetValueXY(curDT, curDatesT.ToList()(0).vCount)
                Else
                    dp1.SetValueXY(curDT, 0)
                End If

                domainSerie.Points.Add(dp1)
                curDT = curDT.AddDays(1)
            End While

        Next


    End Sub

    Private Sub ddl_grint_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_grint.SelectedIndexChanged
        Dim it As IntervalType
        Dim int As Integer

        Select Case ddl_grint.SelectedValue
            Case "Day"
                it = IntervalType.Days
                int = 1
            Case "Week"
                it = IntervalType.Weeks
                int = 1
            Case "2 Weeks"
                it = IntervalType.Weeks
                int = 2
            Case "Month"
                it = IntervalType.Months
                int = 1
        End Select
        Me.Chart2.ChartAreas("ChartArea1").AxisX.IntervalType = it
        Me.Chart2.ChartAreas("ChartArea1").AxisX.LabelStyle.IntervalType = it
        For Each ser As Series In Chart2.Series
            Me.Chart2.DataManipulator.Group("SUM, X:LAST", int, it, ser)
        Next
        

    End Sub
    Protected Sub CreateChart()
        '!+ bar chart
        Dim cha As ChartArea = Me.Chart1.ChartAreas("ChartArea1")
        cha.BorderColor = Drawing.Color.LightGray
        cha.BorderWidth = 1
        cha.BorderDashStyle = ChartDashStyle.Solid
        cha.AxisX.MajorGrid.Enabled = False
        cha.AxisY.MajorGrid.LineColor = Drawing.Color.LightGray
        cha.AxisY.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha.AxisX.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha.AxisY.LineColor = Drawing.Color.LightGray
        cha.AxisX.LineColor = Drawing.Color.LightGray
        cha.AxisX.IntervalAutoMode = False
        cha.AxisX.Interval = 1
        cha.AxisX.LabelStyle.Interval = 1

        Dim s1 As Series = Me.Chart1.Series("Series1")
        s1.ChartType = SeriesChartType.Bar
        s1.CustomProperties = "DrawingStyle=LightToDark"
        s1.Color = Drawing.Color.Orange
        s1.IsValueShownAsLabel = True
        s1.Label = "#VAL"

        cha.Position.Auto = False
        cha.Position.Y = 0
        cha.Position.Height = 100
        cha.Position.X = 5
        cha.Position.Width = 90

        '!+ timeseries chart

        Dim cha2 As ChartArea = Me.Chart2.ChartAreas("ChartArea1")
        cha2.BorderColor = Drawing.Color.LightGray
        cha2.BorderWidth = 1
        cha2.BorderDashStyle = ChartDashStyle.Solid
        cha2.AxisY.MajorGrid.LineColor = Drawing.Color.LightGray
        cha2.AxisX.MajorGrid.LineColor = Drawing.Color.LightGray
        cha2.AxisY.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha2.AxisX.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha2.AxisY.LineColor = Drawing.Color.LightGray
        cha2.AxisX.LineColor = Drawing.Color.LightGray
        cha2.AxisX.IntervalType = DateTimeIntervalType.Days
        cha2.AxisX.IsMarginVisible = False

        Dim title As New Title
        title.Text = "Views by top 5 domains"
        Chart2.Titles.Add(title)

        Dim legend As New Legend
        legend.Docking = Docking.Bottom
        Chart2.Legends.Add(legend)


    End Sub

   


    Private Sub Page_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataBinding
        DataBindChart()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CreateChart()
        DataBindChart()

        Me.Chart1.Width = 675
        Me.Chart2.Width = 675
    End Sub



End Class
﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.Web.UI.DataVisualization.Charting.Utilities

Public Class JournalsDistribution
    Inherits System.Web.UI.Page

    Protected Sub DataBindChart()

        Dim s1 As Series = Me.Chart1.Series("Series1")
        s1.Points.Clear()

        Dim ta As New ChartDataSetTableAdapters.DataTable2TableAdapter
        Dim t As ChartDataSet.DataTable2DataTable

        If Request.QueryString("Type") = "Person" = 0 Then
            If Me.ddl_type.SelectedValue = 1 Then
                t = ta.Get_Journals_ByPersonIDandTwoDates_ByDisctinctPMIDs(Me.ddl_topN.SelectedValue, Request.QueryString("ObjectID"), Request.QueryString("StartDate"), Request.QueryString("EndDate"))
            Else
                t = ta.Get_Journals_ByPersonIDandTwoDates_ByViewsCount(Me.ddl_topN.SelectedValue, Request.QueryString("ObjectID"), Request.QueryString("StartDate"), Request.QueryString("EndDate"))
            End If
        Else
            If Me.ddl_type.SelectedValue = 1 Then
                t = ta.Get_Journals_ByGroupIDandTwoDates_ByDistinctPMIDs(Me.ddl_topN.SelectedValue, Request.QueryString("StartDate"), Request.QueryString("EndDate"), Request.QueryString("ObjectID"))

            Else
                t = ta.Get_Journals_ByGroupIDandTwoDates_ByViewsCount(Me.ddl_topN.SelectedValue, Request.QueryString("StartDate"), Request.QueryString("EndDate"), Request.QueryString("ObjectID"))
            End If
        End If





        Dim rnd As New Random
        Dim k As Integer
        For i = 0 To t.Count - 1
            k = t.Count - 1 - i
            Dim r = t.Item(k)
            Dim dp1 As New DataPoint
            dp1.SetValueXY(r.vTitle, r.vCount)
            s1.Points.Add(dp1)
        Next
        Me.Chart1.Height = 80 + 14 * t.Count
    End Sub
    Protected Sub CreateChart()
        Dim cha As ChartArea = Me.Chart1.ChartAreas("ChartArea1")
        cha.BorderColor = Drawing.Color.LightGray
        cha.BorderWidth = 1
        cha.BorderDashStyle = ChartDashStyle.Solid
        cha.AxisX.MajorGrid.Enabled = False
        cha.AxisY.MajorGrid.LineColor = Drawing.Color.LightGray
        cha.AxisY.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha.AxisX.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha.AxisY.LineColor = Drawing.Color.LightGray
        cha.AxisX.LineColor = Drawing.Color.LightGray
        cha.AxisX.IntervalAutoMode = False
        cha.AxisX.Interval = 1
        cha.AxisX.LabelStyle.Interval = 1







        Dim s1 As Series = Me.Chart1.Series("Series1")
        s1.ChartType = SeriesChartType.Bar
        s1.CustomProperties = "DrawingStyle=LightToDark"
        s1.Color = Drawing.Color.Orange
        s1.IsValueShownAsLabel = True
        s1.Label = "#VAL"


        cha.Position.Auto = False

        cha.Position.Y = 0
        cha.Position.Height = 100
        cha.Position.X = 5
        cha.Position.Width = 90






    End Sub

    Private Sub Page_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataBinding
        DataBindChart()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CreateChart()
        DataBindChart()

        Me.Chart1.Width = 675

    End Sub



End Class
﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.Web.UI.DataVisualization.Charting.Utilities
Imports BioWS.BLL.Extensions

Public Class ViewsTimeSeries
    Inherits System.Web.UI.Page

    Private ReadOnly Property ViewedObjectType As String
        Get
            Return Me.QS("objtype")
        End Get
    End Property
    Private ReadOnly Property ViewedObjectID As Integer
        Get
            Return Me.QSInt32("objid")
        End Get
    End Property
    Private ReadOnly Property IsGroup As Boolean
        Get
            Return ViewedObjectType = "g"
        End Get
    End Property
    Private ReadOnly Property StartDate As Date?
        Get
            Return CType(Me.QS("StartDate"), Date?)
        End Get
    End Property
    Private ReadOnly Property EndDate As Date?
        Get
            Return CType(Me.QS("EndDate"), Date?)
        End Get
    End Property

    Protected Sub DataBindChart()
        Dim sPMIDs As Series = Chart1.Series("SeriesPMIDs")
        Dim sViews As Series = Me.Chart1.Series("SeriesViews")
        Dim sViewsPM As Series = Me.Chart1.Series("SeriesViewsPM")
        sPMIDs.Points.Clear()
        sViewsPM.Points.Clear()
        sViews.Points.Clear()

        Dim pointsExtraDiff As Integer
        Dim LastDateTimeWithDataPoint As Date = StartDate


        Dim ta As New ChartDataSetTableAdapters.DataTable1TableAdapter
        Dim tPMIDs As ChartDataSet.DataTable1DataTable
        Dim tViewsPM As ChartDataSet.DataTable1DataTable
        Dim tViews As ChartDataSet.DataTable1DataTable
        If IsGroup Then
            tPMIDs = ta.Get_PMIDsDistinct_ByGroupIDByTwoDates(StartDate, EndDate, ViewedObjectID)
            tViewsPM = ta.Get_PMViews_ByGroupIDByTwoDates(StartDate, EndDate, ViewedObjectID)
            tViews = ta.Get_TotalViews_ByGroupIDByTwoDates(StartDate, EndDate, ViewedObjectID)
        Else
            tPMIDs = ta.Get_PMIDsDistinct_ByPersonIDByTwoDates(ViewedObjectID, StartDate, EndDate)
            tViewsPM = ta.Get_PMViews_ByPersonIDByTwoDates(ViewedObjectID, StartDate, EndDate)
            tViews = ta.Get_TotalViews_ByPersonIDByTwoDates(ViewedObjectID, StartDate, EndDate)
        End If



        Dim cPMIDs, cViewsPM, cViews As Integer


        For i = 0 To tPMIDs.Count - 1
            Dim rPMIDs = tPMIDs.Item(i)
            pointsExtraDiff = DateDiff(DateInterval.Day, LastDateTimeWithDataPoint.Date, rPMIDs.vDate) - 1
            For k = 1 To pointsExtraDiff
                Dim ep As New DataPoint
                ep.SetValueXY(LastDateTimeWithDataPoint.AddDays(k).Date, 0)
                sPMIDs.Points.Add(ep)
            Next
            Dim dp1 As New DataPoint() With {.MarkerSize = 6, .IsEmpty = False}
            dp1.SetValueXY(rPMIDs.vDate, rPMIDs.vCount)
            cPMIDs = cPMIDs + rPMIDs.vCount
            sPMIDs.Points.Add(dp1)
            LastDateTimeWithDataPoint = rPMIDs.vDate
        Next

        pointsExtraDiff = DateDiff(DateInterval.Day, LastDateTimeWithDataPoint.Date, CDate(EndDate)) - 1
        If pointsExtraDiff > 0 Then
            For k = 1 To pointsExtraDiff
                Dim ep As New DataPoint
                ep.SetValueXY(LastDateTimeWithDataPoint.AddDays(k).Date, 0)
                sPMIDs.Points.Add(ep)
            Next
        End If



        LastDateTimeWithDataPoint = StartDate

        For i = 0 To tViewsPM.Count - 1
            Dim rViewsPM = tViewsPM.Item(i)
            pointsExtraDiff = DateDiff(DateInterval.Day, LastDateTimeWithDataPoint.Date, rViewsPM.vDate) - 1
            For k = 1 To pointsExtraDiff
                Dim ep As New DataPoint
                ep.SetValueXY(LastDateTimeWithDataPoint.AddDays(k).Date, 0)
                sViewsPM.Points.Add(ep)
            Next
            Dim dp1 As New DataPoint
            dp1.SetValueXY(rViewsPM.vDate, rViewsPM.vCount)
            cViewsPM = cViewsPM + rViewsPM.vCount
            dp1.MarkerSize = 6
            sViewsPM.Points.Add(dp1)
            LastDateTimeWithDataPoint = rViewsPM.vDate
        Next
        pointsExtraDiff = DateDiff(DateInterval.Day, LastDateTimeWithDataPoint.Date, CDate(EndDate)) - 1
        If pointsExtraDiff > 0 Then
            For k = 1 To pointsExtraDiff
                Dim ep As New DataPoint
                ep.SetValueXY(LastDateTimeWithDataPoint.AddDays(k).Date, 0)
                sViewsPM.Points.Add(ep)
            Next
        End If

        LastDateTimeWithDataPoint = StartDate

        For i = 0 To tViews.Count - 1
            Dim rViews = tViews.Item(i)
            pointsExtraDiff = DateDiff(DateInterval.Day, LastDateTimeWithDataPoint.Date, rViews.vDate) - 1
            For k = 1 To pointsExtraDiff
                Dim ep As New DataPoint
                ep.SetValueXY(LastDateTimeWithDataPoint.AddDays(k).Date, 0)
                sViews.Points.Add(ep)
            Next

            Dim dp1 As New DataPoint() With {.MarkerSize = 6}
            dp1.SetValueXY(rViews.vDate, rViews.vCount)
            cViews = cViews + rViews.vCount
            sViews.Points.Add(dp1)
            LastDateTimeWithDataPoint = rViews.vDate
        Next
        pointsExtraDiff = DateDiff(DateInterval.Day, LastDateTimeWithDataPoint.Date, CDate(EndDate)) - 1
        If pointsExtraDiff > 0 Then
            For k = 1 To pointsExtraDiff
                Dim ep As New DataPoint
                ep.SetValueXY(LastDateTimeWithDataPoint.AddDays(k).Date, 0)
                sViews.Points.Add(ep)
            Next
        End If


        sPMIDs.LegendText = String.Format("Distinct PMIDs views ({0})", cPMIDs)
        sViewsPM.LegendText = String.Format("PMIDs views ({0})", cViewsPM)
        sViews.LegendText = String.Format("Total views ({0})", cViews)


    End Sub
    Protected Sub CreateChart()
        Dim cha As ChartArea = Me.Chart1.ChartAreas("ChartArea1")
        cha.BorderColor = Drawing.Color.LightGray
        cha.BorderWidth = 1
        cha.BorderDashStyle = ChartDashStyle.Solid
        cha.AxisY.MajorGrid.LineColor = Drawing.Color.LightGray
        cha.AxisX.MajorGrid.LineColor = Drawing.Color.LightGray
        cha.AxisY.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha.AxisX.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha.AxisY.LineColor = Drawing.Color.LightGray
        cha.AxisX.LineColor = Drawing.Color.LightGray
        cha.AxisX.IntervalType = DateTimeIntervalType.Days
        cha.AxisX.IsMarginVisible = False



        Dim sPMIDs As Series = Me.Chart1.Series("SeriesPMIDs")
        sPMIDs.ChartArea = "ChartArea1"
        sPMIDs.ChartType = SeriesChartType.Area
        sPMIDs.XValueType = ChartValueType.Date



        Dim sViewsPM As Series = Me.Chart1.Series("SeriesViewsPM")
        sViewsPM.ChartArea = "ChartArea1"
        sViewsPM.ChartType = SeriesChartType.Area
        sViewsPM.XValueType = ChartValueType.Date



        Dim sViews As Series = Me.Chart1.Series("SeriesViews")
        sViews.ChartArea = "ChartArea1"
        sViews.ChartType = SeriesChartType.Area
        sViews.XValueType = ChartValueType.Date



        Dim l As New Legend() With {.Docking = Docking.Bottom, .Alignment = Drawing.StringAlignment.Far, .IsDockedInsideChartArea = False}




        Me.Chart1.Legends.Add(l)



    End Sub

    Private Sub Page_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataBinding
        DataBindChart()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.Chart1 IsNot Nothing Then
            CreateChart()
            DataBindChart()
            Me.Chart1.Width = 675
        End If
    End Sub



    Private Sub ddl_grint_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_grint.SelectedIndexChanged
        Dim it As IntervalType
        Dim int As Integer

        Select Case ddl_grint.SelectedValue
            Case "Day"
                it = IntervalType.Days
                int = 1
            Case "Week"
                it = IntervalType.Weeks
                int = 1
            Case "2 Weeks"
                it = IntervalType.Weeks
                int = 2
            Case "Month"
                it = IntervalType.Months
                int = 1
        End Select
        Me.Chart1.ChartAreas("ChartArea1").AxisX.IntervalType = it
        Me.Chart1.ChartAreas("ChartArea1").AxisX.LabelStyle.IntervalType = it
        Me.Chart1.DataManipulator.Group("SUM, X:LAST", int, it, Me.Chart1.Series("SeriesPMIDs"))
        Me.Chart1.DataManipulator.Group("SUM, X:LAST", int, it, Me.Chart1.Series("SeriesViewsPM"))
        Me.Chart1.DataManipulator.Group("SUM, X:LAST", int, it, Me.Chart1.Series("SeriesViews"))


    End Sub

End Class
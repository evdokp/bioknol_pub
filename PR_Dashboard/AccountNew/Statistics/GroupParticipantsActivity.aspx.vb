﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.Web.UI.DataVisualization.Charting.Utilities
Imports BioWS.BLL.Extensions

Public Class GroupParticipantsActivity
    Inherits System.Web.UI.Page

    Private ReadOnly Property ViewedObjectType As String
        Get
            Return Me.QS("objtype")
        End Get
    End Property
    Private ReadOnly Property ViewedObjectID As Integer
        Get
            Return Me.QSInt32("objid")
        End Get
    End Property
    Private ReadOnly Property IsGroup As Boolean
        Get
            Return ViewedObjectType = "g"
        End Get
    End Property
    Private ReadOnly Property StartDate As Date?
        Get
            Return CType(Me.QS("StartDate"), Date?)
        End Get
    End Property
    Private ReadOnly Property EndDate As Date?
        Get
            Return CType(Me.QS("EndDate"), Date?)
        End Get
    End Property

    Protected Sub DataBindChart()


        Dim query As String


        query = String.Format("EXEC [dbo].[GetGroupActivityDistribution]    {0}  ,'{1}'  ,'{2}'",
                           ViewedObjectID,
                   StartDate,
                    EndDate)


        Dim dt1 As DataTable = BLL.SQLHelper.GetDT_AllStrings(query)



        Dim sUpTo5 As Series = Chart1.Series("sUpTo5")
        Dim sUpTo10 As Series = Me.Chart1.Series("sUpTo10")
        Dim sAbove10 As Series = Me.Chart1.Series("sAbove10")

        sUpTo5.Points.Clear()
        sUpTo10.Points.Clear()
        sAbove10.Points.Clear()


        For i = 0 To dt1.Rows.Count - 1
            Dim dateRow As DataRow = dt1.Rows(i)

            Dim dp5 As New DataPoint
            dp5.SetValueXY(CDate(dateRow(0)), dateRow(1))
            sUpTo5.Points.Add(dp5)

            Dim dp10 As New DataPoint
            dp10.SetValueXY(CDate(dateRow(0)), dateRow(2))
            sUpTo10.Points.Add(dp10)

            Dim dpAbove10 As New DataPoint
            dpAbove10.SetValueXY(CDate(dateRow(0)), dateRow(3))
            sAbove10.Points.Add(dpAbove10)

        Next


        


    End Sub
    Protected Sub CreateChart()
        Dim cha As ChartArea = Me.Chart1.ChartAreas("ChartArea1")
        cha.BorderColor = Drawing.Color.LightGray
        cha.BorderWidth = 1
        cha.BorderDashStyle = ChartDashStyle.Solid
        cha.AxisY.MajorGrid.LineColor = Drawing.Color.LightGray
        cha.AxisX.MajorGrid.LineColor = Drawing.Color.LightGray
        cha.AxisY.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha.AxisX.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha.AxisY.LineColor = Drawing.Color.LightGray
        cha.AxisX.LineColor = Drawing.Color.LightGray
        cha.AxisX.IntervalType = DateTimeIntervalType.Days
        cha.AxisX.IsMarginVisible = False

        Chart1.Series.Clear()


        Dim sUpTo5 As New Series("sUpTo5")
        Chart1.Series.Add(sUpTo5)
        sUpTo5.ChartArea = "ChartArea1"
        sUpTo5.ChartType = SeriesChartType.Line
        sUpTo5.XValueType = ChartValueType.Date
        sUpTo5.BorderWidth = 2


        Dim sUpTo10 As New Series("sUpTo10")
        Chart1.Series.Add(sUpTo10)
        sUpTo10.ChartArea = "ChartArea1"
        sUpTo10.ChartType = SeriesChartType.Line
        sUpTo10.XValueType = ChartValueType.Date
        sUpTo10.BorderWidth = 2

        Dim sAbove10 As New Series("sAbove10")
        Chart1.Series.Add(sAbove10)
        sAbove10.ChartArea = "ChartArea1"
        sAbove10.ChartType = SeriesChartType.Line
        sAbove10.XValueType = ChartValueType.Date
        sAbove10.BorderWidth = 2


        sUpTo5.LegendText = "up to 5 active days"
        sUpTo10.LegendText = "from 5 to 10 active days"
        sAbove10.LegendText = "above 10 active days"

        Dim l As New Legend() With {.Docking = Docking.Bottom, .Alignment = Drawing.StringAlignment.Far, .IsDockedInsideChartArea = False}
        Me.Chart1.Legends.Clear()

        Me.Chart1.Legends.Add(l)



    End Sub



    Private Sub RecreateChart()
        CreateChart()
        DataBindChart()
        Me.Chart1.Width = 675
        Dim it As IntervalType
        Dim int As Integer

        Select Case ddl_grint.SelectedValue
            Case "Day"
                it = IntervalType.Days
                int = 1
            Case "Week"
                it = IntervalType.Weeks
                int = 1
            Case "2 Weeks"
                it = IntervalType.Weeks
                int = 2
            Case "Month"
                it = IntervalType.Months
                int = 1
        End Select
        Me.Chart1.ChartAreas("ChartArea1").AxisX.IntervalType = it
        Me.Chart1.ChartAreas("ChartArea1").AxisX.LabelStyle.IntervalType = it
        Me.Chart1.DataManipulator.Group("SUM, X:LAST", int, it, Me.Chart1.Series("sUpTo5"))
        Me.Chart1.DataManipulator.Group("SUM, X:LAST", int, it, Me.Chart1.Series("sUpTo10"))
        Me.Chart1.DataManipulator.Group("SUM, X:LAST", int, it, Me.Chart1.Series("sAbove10"))
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.Chart1 IsNot Nothing Then
            RecreateChart()
        End If
    End Sub



    Private Sub ddl_grint_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_grint.SelectedIndexChanged
        RecreateChart()


    End Sub

End Class
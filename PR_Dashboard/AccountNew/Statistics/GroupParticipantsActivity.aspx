﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GroupParticipantsActivity.aspx.vb" Inherits="BioWS.GroupParticipantsActivity" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="background: none; margin-left:10px; margin-bottom:10px;">
    <form id="form1" runat="server">
    <div>
    
   
    <asp:Chart ID="Chart1" runat="server">
      <series>
    </series>
    
        <chartareas>
        <asp:ChartArea Name="ChartArea1">
        </asp:ChartArea>
    </chartareas>
    
    </asp:Chart>
    <br />
    <div style=" text-align:center;">
    <table cellpadding="2" cellspacing="2">
        <tr>
            <td align="right">
              Grouping interval:
            </td>
            <td>
                <asp:DropDownList ID="ddl_grint" runat="server" AutoPostBack="true">
                    <asp:ListItem Text="Day" Value="Day" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Week" Value="Week"></asp:ListItem>
                    <asp:ListItem Text="2 Weeks" Value="2 Weeks"></asp:ListItem>
                    <asp:ListItem Text="Month" Value="Month"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        
    </table>


    </div>
  
    </div>
    </form>
</body>
</html>

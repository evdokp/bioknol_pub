﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="JournalsDistribution.aspx.vb" Inherits="BioWS.JournalsDistribution" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="background: none; margin-left:10px; margin-bottom:10px;">
    <form id="form1" runat="server">
    <div>
    
   
 <asp:Chart ID="Chart1" runat="server">
    <series>
        <asp:Series Name="Series1">
        </asp:Series>
    </series>
    <chartareas>
        <asp:ChartArea Name="ChartArea1">
        </asp:ChartArea>
    </chartareas>
</asp:Chart>
<br />
<div style="text-align:center">
<table cellpadding ="2" cellspacing ="2">
<tr>
<td align ="right" >Top items count:</td>
<td style="text-align:left"><asp:DropDownList ID="ddl_topN" runat="server" AutoPostBack ="true" >
<asp:ListItem Text ="10" Value ="10" Selected ="True" ></asp:ListItem>
<asp:ListItem Text ="20" Value ="20"></asp:ListItem>
<asp:ListItem Text ="50" Value ="50"></asp:ListItem>
<asp:ListItem Text ="100" Value ="100"></asp:ListItem>
</asp:DropDownList></td>
</tr>
<tr>
<td align ="right" >Chart type:</td>
<td style="text-align:left" >
 <asp:DropDownList ID="ddl_type" runat="server" AutoPostBack ="true" >
<asp:ListItem Text ="by count of disctinct PMIDs" Value ="1" Selected ="True" ></asp:ListItem>
<asp:ListItem Text ="by count of views" Value ="2"></asp:ListItem>
</asp:DropDownList></td>
</tr>
</table>
</div>



    </div>
    </form>
</body>
</html>

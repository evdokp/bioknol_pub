﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Masters/bsside.master" Theme="Theme2" CodeBehind="ViewMesh.aspx.vb" Inherits="BioWS.ViewMesh" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph1" runat="server">
    <script src="../Scripts/jsrender.js"></script>
            <script src="../Scripts/jquery.hovercard.min.js"></script>
    <script src="../Scripts/Code/PersonHoverCards.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('a.author').PersonHoverCards();
        });
    </script>


    <h1 style="margin-bottom: 20px;">
        <asp:Literal ID="litTitle" runat="server"></asp:Literal>
    </h1>

    <div class="row-fluid">
        <div class="span5">
            <h2>Top specialized people</h2>
            <div class="smallfade">The figure on the right represents specificity rank.</div>
            <asp:Repeater ID="repSpecificPeople" runat="server">
                <ItemTemplate>

                    <div class="feed_comment" style="padding:0px">
                        <div class="fleft" style="padding:4px">
                        <a class="fleft" href="ViewPerson.aspx?objid=<%# Eval("PersonID")%>&objtype=p" target="_blank"  style="margin-right: 4px;">
                            <img src="<%# Eval("AvatarURL")%>" alt="" />
                        </a>
                            <div class="fleft">
                        <a class="author" href="ViewPerson.aspx?objid=<%# Eval("PersonID")%>&objtype=p" target="_blank" personid="<%# Eval("PersonID")%>" style="margin-right: 4px;"><%# Eval("DisplayName")%></a>
                                </div>
                            <div class="clearfix"></div>
                            </div>
                        <div class="fright sidebid">
                            <%# Eval("SpecificityRank")%>
                        </div>

                        <div class="clearfix"></div>
                    </div>



                </ItemTemplate>
            </asp:Repeater>

        </div>
        <div class="span7">
  
        </div>
    </div>

</asp:Content>

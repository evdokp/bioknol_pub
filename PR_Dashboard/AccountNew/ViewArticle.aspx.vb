﻿Imports System.IO
Imports BioWS.BLL
Imports BioWS.BLL.Extensions
Public Class ViewArticle
    Inherits Page
    Private Function GetImageByType(contenttype As String) As String
        Select Case contenttype
            Case "application/pdf"
                Return "~\Images\pdficon16.png"
            Case "text/plain"
                Return "~\Images\txticon16.png"
            Case Else
                Return "~\Images\unknownicon16.png"
        End Select
    End Function
    Private Function GetFileName(filename As String) As String
        Dim fnwe As String = Path.GetFileNameWithoutExtension(filename)
        Dim ext As String = Path.GetExtension(filename)
        If fnwe.Length > 70 Then
            Return fnwe.Substring(0, 70) & " ... " & ext
        Else
            Return filename
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim pma As New BLL.PubMed.PubMedArticle(Me.QSInt32("id"))
        hfArticleID.Value = Me.QS("id")
        hfPageOpenDateTime.Value = Now.ToString
        litTitle.Text = pma.Title
        Me.Page.Title = pma.Title

        litAuthors.Text = pma.AuthorsString
        litJournal.Text = pma.Journal
        litPubYear.Text = pma.PubYear.ToString
        hlPubmed.NavigateUrl = pma.PubmedURL
        hlUpload.NavigateUrl = "~/UploadArticle.aspx?URL=" & pma.PubmedURL

        hfRecommend.Value = IIf(Me.QS("recommend").IsNullOrEmpty(), 0, 1)

        ToggleFulltextLinks(pma)
        ToggleMesh(pma)
        ToggleReadWithIt(pma)
        DataBindWidgets(pma)

        ToggleRequest()

    End Sub
    Private Sub ToggleReadWithIt(ByVal pma As PubMed.PubMedArticle)
        Using ta As New ArticlesPubmedDataSetTableAdapters.ArticlesPubmedTableAdapter()
            Dim tt = ta.GetViewedTogetherHINT_data(10, 2, pma.Pubmed_ID)
            Dim hasData = tt.Count > 0
            pnlWhatOthers.Visible = hasData
            If hasData Then
                Dim res As New List(Of BLL.PubMed.PubMedArticle)

                For i = 0 To tt.Count - 1
                    Dim art As New BLL.PubMed.PubMedArticle(tt.Item(i).Pubmed_ID)
                    res.Add(art)
                Next

                repWhatOthers.DataSource = res
                repWhatOthers.DataBind()

            End If
        End Using
    End Sub
    Private Sub ToggleRequest()
        Using reqta As New ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter()
            If reqta.GetCountByPersonIDPubmedID(Me.QSInt32("id"), Me.Page.GetProfilePersonID()) = 0 Then
                hlRequest.Text = "Request"
            Else
                hlRequest.Text = "Already requested"
                liRequest.Attributes.Add("class", "disabled")
            End If
        End Using
    End Sub
    Private Sub ToggleFulltextLinks(ByVal pma As BLL.PubMed.PubMedArticle)
        If pma.OffersWithFilesCount > 0 Then

            hlRequest.Visible = False
            hlUpload.Visible = False


            Using dwnldta As New ArticlesDataSetTableAdapters.ArticleOffersTableAdapter()
                Dim dwnlt = dwnldta.GetDataByPubmedIDwithFiles(Request.QueryString("id"))

                Dim downloads = From d In dwnlt
                                Select d.ContentType, FileName = GetFileName(d.OriginalName), Image = GetImageByType(d.ContentType), _
                                DownloadLink = "~\Download.ashx?vFileName=" & HttpUtility.UrlEncode(d.FileName) & "&vOriginalName=" & HttpUtility.UrlEncode(d.OriginalName)

                Dim dwnldlst = downloads.ToList()
                repDownloads.DataSource = dwnldlst
            End Using
            repDownloads.DataBind()
        Else
            pnlDownloads.Visible = False
        End If
    End Sub
    Private Sub ToggleMesh(ByVal pma As BLL.PubMed.PubMedArticle)
        Using ta As New ArticlesPubmedDataSetTableAdapters.pmMeSHTableAdapter()
            Dim t = ta.GetDataByPubmedID(pma.Pubmed_ID)
            If t.Count > 0 Then
                Dim res = From tt In t
                          Select tt.Heading, tt.MeSH_ID

                repMesh.DataSource = res.ToList()
                repMesh.DataBind()

            Else
                pnlMesh.Visible = False
            End If
        End Using

    End Sub

    Private Sub DataBindWidgets(ByVal pma As PubMed.PubMedArticle)
        Using widgetsTA As New WidgetsDataSetTableAdapters.WidgetsTableAdapter()
            Dim widgetsTBL = widgetsTA.GetDataByPersonID_ON(Me.Page.GetProfilePersonID())
            Dim res As New Dictionary(Of Integer, String)

            For i = 0 To widgetsTBL.Count - 1
                Dim r = widgetsTBL.Item(i)
                If r.Widget_ID > 1 Then
                    If BLL.Widgets.IsAdditionalInfoAvailableByWidget(pma.Pubmed_ID, r.Widget_ID) > 0 Then
                        res.Add(r.Widget_ID, r.Title)
                    End If
                End If
            Next

            Dim pid As Integer = Me.Page.GetProfilePersonID()

            Dim res2 = (From w In res
                        Select WidgetID = w.Key,
                        PersonID = pid, PubmedID = Me.QS("id"),
                              Title = w.Value).ToList()

            repWidgets.DataSource = res2
        End Using
        repWidgets.DataBind()




    End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function RecentlyReadBy(ByVal articleid As String) As String
        Using ta As New PersonsDataSetTableAdapters.v_PersonsTableAdapter()
            Dim tt = ta.GetListOfPersonsWhoReadLinkID(articleid)
            Dim res = From pers In tt
                      Select ID = pers.Person_ID,
                          Avatar = IIf(pers.Image50.Length < 12, BLL.Common.GetGravatar(pers.UserName, 50), "https://bioknolstorage.blob.core.windows.net/userimages/" & pers.AvatarGUID & "_50." & pers.AvatarExtension),
                          Name = pers.DisplayName

            Return (res.ToList().ToJSON())
        End Using
    End Function




    <System.Web.Services.WebMethod()> _
    Public Shared Function GetArtHist(ByVal PubmedID As Integer,
                                        ByVal EarlierThen As String,
                                        ByVal LastIndex As Integer) As String


        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)

        Dim dt As Date
        DateTime.TryParse(EarlierThen, dt)

        Dim db = New DataClassesFeedDataContext


        Dim tt = From c In db.GetArticleViewHistory(10, EarlierThen, LastIndex, p.Person_ID, PubmedID)
                Select vTimestamp = c.TimeStamp, vDateTime = c.TimeStamp.ToString("dd MMMM yyyy HH:mm:ss"), _
                        vInterval = GetIntervalFromSeconds(c.SecondsSinceLast), _
                        vEgo = GetIntervalFromSeconds((Now - c.TimeStamp).TotalSeconds) & " ago", _
                        isFirst = IIf(c.SecondsSinceLast = -1, 1, 0)



        Return tt.ToList().ToJSON

    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetAfter(ByVal ViewDT As DateTime,
                                        ByVal EarlierThen As String,
                                        ByVal LastIndex As Integer) As String

        ViewDT = ViewDT.ToLocalTime()

        Dim timesp As TimeSpan = ViewDT.TimeOfDay


        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)

        Dim dt As Date
        DateTime.TryParse(EarlierThen, dt)

        Dim db = New DataClassesFeedDataContext


        Dim tt = From c In db.GetArticleViewHistoryAfter(10, EarlierThen, LastIndex, p.Person_ID, ViewDT)
                Select vTimestamp = c.TimeStamp, vDateTime = c.TimeStamp.ToString("dd MMMM yyyy HH:mm:ss"), _
                        vInterval = GetIntervalFromSeconds(c.SecondsSinceLast), _
                        vEgo = GetIntervalFromSeconds((Now - c.TimeStamp).TotalSeconds) & " ago", _
                        isFirst = IIf(c.SecondsSinceLast = -1, 1, 0), _
                        vTitle = c.Title, vID = c.Pubmed_ID





        Return tt.ToList().ToJSON

    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetBefore(ByVal ViewDT As DateTime,
                                        ByVal EarlierThen As String,
                                        ByVal LastIndex As Integer) As String
        ViewDT = ViewDT.ToLocalTime()
        Dim timesp As TimeSpan = ViewDT.TimeOfDay


        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)

        Dim dt As Date
        DateTime.TryParse(EarlierThen, dt)

        Dim db = New DataClassesFeedDataContext


        Dim tt = From c In db.GetArticleViewHistoryBefore(10, EarlierThen, LastIndex, p.Person_ID, ViewDT)
                Select vTimestamp = c.TimeStamp, vDateTime = c.TimeStamp.ToString("dd MMMM yyyy HH:mm:ss"), _
                        vInterval = GetIntervalFromSeconds(c.SecondsSinceLast), _
                        vEgo = GetIntervalFromSeconds((Now - c.TimeStamp).TotalSeconds) & " ago", _
                        isFirst = IIf(c.SecondsSinceLast = -1, 1, 0), _
                        vTitle = c.Title, vID = c.Pubmed_ID





        Return tt.ToList().ToJSON
    End Function


    Public Shared Function GetIntervalFromSeconds(ByVal seconds As Integer) As String
        Dim s As String
        Dim timesp As TimeSpan
        timesp = TimeSpan.FromSeconds(seconds)

        If timesp.Days > 1 Then
            s = s & timesp.Days & "d "
        End If
        If timesp.TotalDays < 100 Then
            If timesp.TotalHours > 1 Then
                s = s & timesp.Hours & "h "
            End If
            If timesp.TotalDays < 15 Then
                If timesp.TotalMinutes > 1 Then
                    s = s & timesp.Minutes & "m "
                End If
                If timesp.TotalDays < 2 Then
                    If timesp.TotalSeconds > 1 Then
                        s = s & timesp.Seconds & "s "
                    End If
                End If
            End If
        End If


        Return s
    End Function

    Protected Sub Repeater2_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles repWidgets.ItemCommand

    End Sub
End Class
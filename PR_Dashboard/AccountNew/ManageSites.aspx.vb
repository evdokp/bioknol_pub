﻿Imports BioWS.BLL.Extensions

Public Class ManageSites1
    Inherits System.Web.UI.Page
    Private _curProfilePersonID As Integer?
    Public ReadOnly Property CurProfilePersonID As Integer
        Get
            If _curProfilePersonID Is Nothing Then
                _curProfilePersonID = Me.GetProfilePersonID()
            End If
            Return _curProfilePersonID
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim pers As New BLL.Person(Me.QSInt32("objid"))
        hlViewedPerson.Text = pers.PageHeader
        hlViewedPerson.NavigateUrl = pers.URL

        Dim Content2 As ContentPlaceHolder = CType(Me.Master.Master.FindControl("cph1"), ContentPlaceHolder)
        Dim hfCurPage As HiddenField = CType(Content2.FindControl("hfCurPage"), HiddenField)


        hfCurPage.Value = "Manage sites"
        Me.litCurPage.Text = "Manage sites"

    End Sub

    Protected Sub lnkb_action_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim vID As Integer = CType(CType(sender, LinkButton).CommandArgument, Integer)
        
        Using ta As New MonitoredSitesDataSetTableAdapters.MonitoredSitesUserExclusionsTableAdapter()
            Dim hasExclusion As Boolean = ta.GetCountByBothParams(vID, CurProfilePersonID) > 0
            If hasExclusion Then
                ta.DeleteQuery(vID, CurProfilePersonID)
            Else
                ta.Insert(vID, CurProfilePersonID)
            End If
        End Using
        gv_sites.DataBind()
    End Sub

    Protected Sub gv_sites_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gv_sites.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim drw = DirectCast(e.Row.DataItem, DataRowView)
            Dim datarow = drw.Row
            Dim ms_row = DirectCast(datarow, MonitoredSitesDataSet.MonitoredSitesRow)


            Using ta As New MonitoredSitesDataSetTableAdapters.MonitoredSitesUserExclusionsTableAdapter()
                Dim hasExclusion As Boolean = ta.GetCountByBothParams(ms_row.ID, CurProfilePersonID) > 0
                Dim lnkb_action As LinkButton = e.Row.Cells(2).FindControl("lnkb_action")
                lnkb_action.Font.Bold = hasExclusion
                lnkb_action.Text = IIf(hasExclusion, "Include", "Exclude")
            End Using
        End If
    End Sub
End Class
﻿Imports BioWS.BLL.Extensions

Public Class ViewStatistics
    Inherits System.Web.UI.Page

    Private ReadOnly Property ViewedObjectType As String
        Get
            Return Me.QS("objtype")
        End Get
    End Property
    Private ReadOnly Property ViewedObjectID As Integer
        Get
            Return Me.QSInt32("objid")
        End Get
    End Property
    Private ReadOnly Property IsGroup As Boolean
        Get
            Return ViewedObjectType = "g"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            SetObjectName()

            Dim Content2 As ContentPlaceHolder = CType(Me.Master.Master.FindControl("cph1"), ContentPlaceHolder)
            Dim hfCurPage As HiddenField = CType(Content2.FindControl("hfCurPage"), HiddenField)
            hfCurPage.Value = "Statistics"
            litCurPage.Text = "Statistics"

            Dim pr As New ProfileCommon
            pr = pr.GetProfile(HttpContext.Current.User.Identity.Name)

            BindStatisticsPage(Today.AddDays(-pr.DefaultPeriodDays), Today)

        End If
    End Sub
    Private Sub SetObjectName()
        If IsGroup Then
            Dim group As New BLL.Group(ViewedObjectID)
            hlViewedPerson.Text = group.Title
            hlViewedPerson.NavigateUrl = group.URL
        Else
            Dim pers As New BLL.Person(ViewedObjectID)
            hlViewedPerson.Text = pers.PageHeader
            hlViewedPerson.NavigateUrl = pers.URL
        End If
    End Sub

    Private Sub BindStatisticsPage(ByVal StartDate As DateTime, ByVal EndDate As DateTime)
        Dim qsb As New QueryStringBuilder()
        qsb.SetQSValue("objid") _
            .SetQSValue("objtype") _
            .SetValue("StartDate", StartDate) _
            .SetValue("EndDate", EndDate)

        ASPxSplitter1.Panes(0).ContentUrl = cmbStatPage.SelectedItem.Value & qsb.ToQueryString()

    End Sub



    Private Sub SetPeriod1_PeriodChanged(StartDate As Date, EndDate As Date) Handles SetPeriod1.PeriodChanged
        BindStatisticsPage(StartDate, EndDate)
    End Sub

    Private Sub cmbStatPage_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cmbStatPage.SelectedIndexChanged
        BindStatisticsPage(SetPeriod1.StartDate, SetPeriod1.EndDate)
    End Sub

End Class
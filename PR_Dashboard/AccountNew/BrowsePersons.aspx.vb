﻿Imports BioWS.BLL.Extensions

Public Class BrowsePersons
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfViewerID.Value = Me.GetProfilePersonID
    End Sub

    <System.Web.Services.WebMethod()> _
    Public Shared Function ClosestBySpec(ByVal personID As Integer) As String

        Dim ta As New PersonsDataSetTableAdapters.v_PersonsTableAdapter
        Using ta
            Dim tt = ta.GetClosestBySpecializationByPersonID(personID)
            Dim res = From pers In tt
                      Select ID = pers.Person_ID,
                           Avatar = IIf(pers.Image50.Length < 12, BLL.Common.GetGravatar(pers.UserName, 50), "https://bioknolstorage.blob.core.windows.net/userimages/" & pers.AvatarGUID & "_50." & pers.AvatarExtension),
                          Name = pers.DisplayName

            Return (res.ToList().ToJSON())

        End Using


    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function CommentTheSame(ByVal personID As Integer) As String
        Dim ta As New PersonsDataSetTableAdapters.v_PersonsTableAdapter
        Using ta
            Dim tt = ta.GetCommentTheSameByPersonID(personID)
            Dim res = From pers In tt
                      Select ID = pers.Person_ID,
                         Avatar = IIf(pers.Image50.Length < 12, BLL.Common.GetGravatar(pers.UserName, 50), "https://bioknolstorage.blob.core.windows.net/userimages/" & pers.AvatarGUID & "_50." & pers.AvatarExtension),
                          Name = pers.DisplayName

            Return (res.ToList().ToJSON())
        End Using

    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function Followers(ByVal personID As Integer) As String

        Using ta As New PersonsDataSetTableAdapters.v_PersonsTableAdapter()
            Dim tt = ta.GetFollowersByFollowedPersonID(personID)
            Dim res = From pers In tt
                      Select ID = pers.Person_ID,
                              Avatar = IIf(pers.Image50.Length < 12, BLL.Common.GetGravatar(pers.UserName, 50), "https://bioknolstorage.blob.core.windows.net/userimages/" & pers.AvatarGUID & "_50." & pers.AvatarExtension),
                          Name = pers.DisplayName

            Return (res.ToList().ToJSON())
        End Using
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function GetByQuery(ByVal personID As Integer, ByVal query As String) As String

        Using ta As New PersonsDataSetTableAdapters.v_PersonsTableAdapter()
            Dim tt = ta.GetDataByQueryMinusSelf("%" & query & "%", personID)

            Dim res = From pers In tt
                      Select ID = pers.Person_ID,
                                Avatar = IIf(pers.Image50.Length < 12, BLL.Common.GetGravatar(pers.UserName, 50), "https://bioknolstorage.blob.core.windows.net/userimages/" & pers.AvatarGUID & "_50." & pers.AvatarExtension),
                          Name = pers.DisplayName

            Return (res.ToList().ToJSON())
        End Using
    End Function
End Class
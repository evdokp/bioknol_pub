﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Masters/bsside.Master"
    CodeBehind="MyActivity.aspx.vb" Theme="Theme2" Inherits="BioWS.MyActivity" %>

<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cph1" runat="server">
    <script src="../Scripts/Code/FeedItemManager.js" type="text/javascript"></script>
    <script src="../Scripts/Code/FeedManager.js" type="text/javascript"></script>
    <script src="../Scripts/Code/RecommendationsManager.js" type="text/javascript"></script>
    <script src="../Scripts/Code/CommentsManager.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.js"></script>
        <script src="../Scripts/jquery.hovercard.min.js"></script>
    <script src="../Scripts/Code/PersonHoverCards.js"></script>
    <script>

    </script>


    <script type="text/javascript">



        var fm = new FeedManager();
        var rm = new RecommendationsManager();
        

        $(document).ready(function () {

            $('#txtRecEmails').watermark('Type e-mail on each line...');
            $('#recPersonQuery').watermark('Type user name to find...');
            $('#recAddRecommedComment').watermark('Add a comment to your recommendation...')


            $('a[rel="popover"]').popover({ delay: { show: 1200, hide: 100 }, placement: 'bottom' });
            $('input[rel="popover"]').popover({ delay: { show: 1200, hide: 100 }, placement: 'bottom' });
            fm.Init();
            rm.Init();
            
            fm.LoadMore();

            $('.recommend').live('click', function () {
                var tr = $(this).parents('tr');
                var arttitle = tr.find('.arttitle').text();
                rm.Show(tr.attr('articleid'), arttitle);
            });

        });


    </script>
    <asp:HiddenField ID="hfLastIndex" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfPageOpenDateTime" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfPersonID" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfIsFollowed" runat="server" ClientIDMode="Static" />
    <dx:ASPxLoadingPanel ID="ASPxLoadingPanel1" runat="server" ClientInstanceName="LoadingPanel"
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" ImageSpacing="5px"
        ContainerElementID="tblFeed" Modal="True">
        <Image Url="~/App_Themes/DevEx/Web/Loading.gif">
        </Image>
    </dx:ASPxLoadingPanel>
    <div id="newfeed">


    </div>
    <script id="feedTmpl" type="text/x-jsrender">
        {{:ViewType}}
    </script>



    <script id="tmplDate" type="text/x-jquery-tmpl">
    </script>
    <script id="tmplPubmed" type="text/x-jquery-tmpl">
    </script>
    <script id="tmplDoi" type="text/x-jquery-tmpl">
    </script>
    <script id="tmplRecomendators" type="text/x-jquery-tmpl">
    </script>
    
    <script id="tmplRecComInitialLoad" type="text/x-jquery-tmpl">
    </script>
    <script id="tmplRecommendation" type="text/x-jquery-tmpl">
    </script>
    <script id="tmplComment" type="text/x-jquery-tmpl">
    </script>


    <ul class="breadcrumb">
        <li>
            <asp:HyperLink ID="hlViewedPerson" runat="server" />
            <span class="divider">/</span> </li>
        <li>
            <asp:Literal ID="litCurPage" runat="server"></asp:Literal>
        </li>
        <div class="fright" style="position:relative;">
            <input type="text" id="txtQuery" class="fleft"  style="height:20px; font-size:11px;  line-height:11px; padding:1px; position:absolute; left:-220px; top:-2px;  " />
            <a class="btn  btn-mini fleft" id="btnFind" style="margin-right: 3px;">find</a>
            <a class="btn  btn-mini fleft" id="btnClearQuery">clear</a>
            <div class="btn-group fleft" data-toggle="buttons-radio" style="padding-left: 4px;">
                <a class="btn btn-info btn-mini feedtype active" feedtype="1" rel="popover" title="All"
                    data-content="Removes any filter applied to feed or list">all</a> <a class="btn btn-info btn-mini feedtype"
                        feedtype="2" rel="popover" title="Favorite" data-content="Applies filter to feed or list in order to show only favorite items">
                        <i class="icon-star icon-white"></i></a><a class="btn btn-info btn-mini feedtype"
                            feedtype="3" rel="popover" title="Recommended" data-content="Applies filter to feed or list in order to show only items that has been recommended">
                            <i class="icon-thumbs-up icon-white"></i></a><a class="btn btn-info btn-mini feedtype"
                                feedtype="4" rel="popover" title="Trash" data-content="Applies filter to feed or list in order to show only items that has been moved to trash">
                                <i class="icon-trash icon-white"></i></a>
            </div>
        </div>
    </ul>
    <input type="hidden" value="1" id="hfFeedtype" />
    <div class="clearfix">
    </div>
    <table id="tblFeed" class="table">
        
    </table>
    <a id="btnShowMore" class="btn">Show more</a>
    <%--recommendations html--%>
    <div class="modal hide" id="recModal" style="width: 600px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
                ×</button>
            <h3>Recommend article</h3>
            <div style="height: 38px; overflow: hidden; font-style: italic; padding: 7px;" id="recArticleTitle">
            </div>
            <textarea style="width:97%" rows="2" id="recAddRecommedComment"></textarea>
        </div>
        <div class="modal-body" style="height: 650px;">
            <div class="tabbable">
                <!-- Only required for left/right tabs -->
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1" data-toggle="tab">Find BioKnol users</a></li>
                    <li><a href="#tab2" data-toggle="tab">Send e-mails</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                        <div class="row-fluid">
                            <div class="span6">
                                
                                    <input  id="recPersonQuery" size="16" style="width:90%" type="text"/>
                                    
                                
                                <div style="height: 300px; border: 1px solid lightgray; overflow-y: scroll;">
                                    <table class="table" width="90%" id="recSearchResults" style="display: none;">
                                    </table>
                                </div>
                            </div>
                            <div class="span6">
                                <div style="padding: 7px;">
                                    <strong>Already recommeneded to:</strong>
                                </div>
                                <div style="height: 300px; border: 1px solid lightgray; overflow-y: scroll;">
                                    <table class="table" width="90%" id="recAlreadyRec">
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2">
                        <div class="row-fluid">
                            <div class="span6">
                                <textarea id="txtRecEmails" cols="20" rows="2" style="height: 230px; width: 90%; border: 1px solid lightgray;"></textarea>
                                <a class="btn btn-primary" id="recSendEmails" data-loading-text="Sending..">Send recommendations</a>
                            </div>
                            <div class="span6">
                                <div style="height: 230px; border: 1px solid lightgray; overflow-y: scroll;">
                                    <table class="table" width="90%" id="tblEmailResults" style="">
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
            <a href="#" class="btn btn-primary" id="recSaveUpdateClose" >Save comment and update recommendation dates</a>
        </div>
    </div>
    <script id="tmplPerson" type="text/x-jquery-tmpl">
        <tr style="cursor: pointer;" personid="${Person_ID}">
            <td>
                <b>${DisplayName}</b>
            </td>
        </tr>
    </script>
</asp:Content>

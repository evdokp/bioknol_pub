﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Masters/bsside.Master"
    CodeBehind="EditProfile.aspx.vb" Theme="Theme2" Inherits="BioWS.EditProfile1" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cph1" runat="server">
    <ul class="breadcrumb">
        <li>
            <asp:HyperLink ID="hlViewedPerson" runat="server" />
            <span class="divider">/</span> </li>
        <li>
            <asp:Literal ID="litCurPage" runat="server"></asp:Literal>
        </li>
    </ul>
    <asp:Panel ID="pnlFinReg" runat="server" Visible="false">
        <br />
        <div class="blockinfo">
            <h4>Welcome to BioKnol!</h4>
            Please, finish registration by filling fields below.
            <br />
            <br />
        </div>
        <br />
    </asp:Panel>


    <div class="row-fluid">
        <div class="span5">
            <ul class="breadcrumb">
                <li>Profile information</li>
            </ul>
            <asp:Panel ID="pnlSent" runat="server" Visible="false">
                <div class="alert alert-success">
                    <asp:Literal ID="litSent" runat="server"></asp:Literal>
                </div>
                <br />
            </asp:Panel>
            <table width="100%" cellpadding="4" cellspacing="4">
                <tr>
                    <td valign="top" style="width: 80px; text-align: right;">Last name:
                    </td>
                    <td valign="top" style="">

                        <asp:TextBox ID="txtLastName" runat="server" CssClass="input-medium"
                            Width="270px">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="width: 80px; text-align: right;">First name:
                    </td>
                    <td valign="top" style="">
                        <asp:TextBox
                            ID="txtFirstName"
                            runat="server"
                            CssClass="input-medium"
                            Width="270px" />

                    </td>
                </tr>

                <tr>
                    <td valign="top" style="width: 80px; text-align: right;">&nbsp;
                    </td>
                    <td valign="top" style="">
                        <asp:Button ID="btnSave" runat="server" class="btn btn-primary btn-large"
                            Text="Save"></asp:Button>
                    </td>
                </tr>
            </table>
        </div>
        <div class="span7">
         
        </div>
    </div>

</asp:Content>

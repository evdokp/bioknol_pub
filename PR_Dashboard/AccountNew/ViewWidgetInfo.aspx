﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Masters/bsside.master"
    Theme="Theme2" CodeBehind="ViewWidgetInfo.aspx.vb" Inherits="BioWS.ViewWidgetInfo" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph1" runat="server">


  <script type="text/javascript">
      $(document).ready(function () {
          ShowHideOnOffControls()
          $('#hlToggleWidget').click(function () {
              PageMethods.ToggleWidget($('#hfWidgetID').val(), function () {
                  if (parseInt($('#hfIsWidgetOn').val()) === 1) {
                      $('#hfIsWidgetOn').val(0);
                  }
                  else {
                      $('#hfIsWidgetOn').val(1);
                  }
                  ShowHideOnOffControls();
              });
          });
      });

      function ShowHideOnOffControls() {
          var ison = parseInt($('#hfIsWidgetOn').val());
          if (ison === 1) {
              $('#spanIsWidgetOn').show();
              $('#hlToggleWidget').text('Turn off');
          }
          else {
              $('#spanIsWidgetOn').hide();
              $('#hlToggleWidget').text('Turn on');
          }
      }
    </script>
        
    <asp:HiddenField ID="hfWidgetID" runat="server" ClientIDMode="Static" />
        
    <asp:HiddenField ID="hfIsWidgetOn" runat="server" ClientIDMode="Static" />
  <div style="margin-bottom: 4px;">
                    <a>&nbsp;</a>
                <div class="fright">
                    <span class="label label-info" style="display: none;" id="spanIsWidgetOn">Widget is on</span> 
                        <a id="hlToggleWidget"  class="btn btn-mini"></a>
                </div>
                <div class="clear">
                </div>
            </div>


<div>
    
</div>
    <ul class="breadcrumb">
        <li>
            <asp:HyperLink ID="hlViewedPerson" runat="server" />
            <span class="divider">/</span> </li>
            <li>
            <asp:HyperLink ID="hlManageWidgets" runat="server" Text="Manage widgets" />
            <span class="divider">/</span> </li>
        <li>
            <asp:Literal ID="litCurPage" runat="server"></asp:Literal>
        </li>
    </ul>
    <ul class="breadcrumb">
        <li>About the widget</li>
    </ul>
    <table width="100%" id="tblCrit" class="table table-striped table-bordered table-condensed">
        <tr>
            <td valign="top" style="text-align: right;" class="">
                Description:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litDesc" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="">
                Pubmed IDs processing mode:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litAllowMultiple" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="">
                Provided by:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litIsExternal" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="">
                Current version:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litVersion" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Last release date:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litAddedAt" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Added by:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:HyperLink ID="hlAddedBy" runat="server" />
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlUsedBy" runat="server">
        <ul class="breadcrumb">
            <li>This widget is used by</li>
        </ul>
        <table width="100%" id="Table1">
            <tr>
                <td valign="top" style="text-align: right;" class="td_label">
                </td>
                <td valign="top" style="" class="td_field">
                    <dx:ASPxGridView ID="gvUsedBy" runat="server" AutoGenerateColumns="False" Width="100%"
                        KeyFieldName="Person_ID" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                        <Columns>
                            <dx:GridViewDataDateColumn VisibleIndex="0" Caption="Participants:">
                                <PropertiesDateEdit Spacing="0">
                                </PropertiesDateEdit>
                                <DataItemTemplate>
                                    <asp:Image ID="Image5" runat="server" ImageUrl='<%# "~\UserImages\" & Eval("Image50")  %>'
                                        Style="float: left; margin-right: 10px;" />
                                    <asp:HyperLink ID="hlPerson" runat="server" NavigateUrl='<%# "~\Account\ViewPerson.aspx?PersonID=" & Eval("Person_ID") %>'
                                        Text='<%# Eval("UserName") %>' />
                                    <br />
                                    <span style="color: Gray">
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("LastName") & " " & Eval("FirstName")& " " & Eval("Patronimic") %>' />
                                    </span>
                                </DataItemTemplate>
                            </dx:GridViewDataDateColumn>
                        </Columns>
                        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                            <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                            </LoadingPanelOnStatusBar>
                            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                            </LoadingPanel>
                        </Images>
                        <ImagesFilterControl>
                            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                            </LoadingPanel>
                        </ImagesFilterControl>
                        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                            </Header>
                            <LoadingPanel ImageSpacing="5px">
                            </LoadingPanel>
                        </Styles>
                        <StylesEditors ButtonEditCellSpacing="0">
                            <ProgressBar Height="21px">
                            </ProgressBar>
                        </StylesEditors>
                    </dx:ASPxGridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Masters/bsside.Master"
    CodeBehind="MyActivity.aspx.vb" Theme="Theme2" Inherits="BioWS.MyActivity" %>

<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cph1" runat="server">
    <script src="../Scripts/Code/FeedItemManager.js" type="text/javascript"></script>
    <script src="../Scripts/Code/FeedManager.js" type="text/javascript"></script>
    <script src="../Scripts/Code/CommentsManager.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.js"></script>
    <script src="../Scripts/jquery.hovercard.min.js"></script>
    <script src="../Scripts/Code/PersonHoverCards.js"></script>
    <script src="../Scripts/TS/RecManager.js"></script>
    <script src="../Scripts/select2.min.js"></script>


    <script>

    </script>


    <script type="text/javascript">
        var fm = new FeedManager();
        $(document).ready(function () {

            $('#txtRecEmails').watermark('Type e-mail on each line...');
            $('#recPersonQuery').watermark('Type user name to find...');
            $('#recAddRecommedComment').watermark('Add a comment to your recommendation...')

            $('a[rel="popover"]').popover({ delay: { show: 1200, hide: 100 }, placement: 'bottom' });
            $('input[rel="popover"]').popover({ delay: { show: 1200, hide: 100 }, placement: 'bottom' });
            fm.Init();

            fm.LoadMore();

            $('.recommend').live('click', function () {
                var tr = $(this).parents('tr');
                var articlaTitle = tr.find('.arttitle').text();
                var articleId = tr.attr('articleid');
                var recmng = new RecManager({
                    articleid: articleId,
                    articletitle: articlaTitle,
                    newRecommendationCallback: fm.GetCommentsManager().IncreaseRecsCount,
                    commentIncreaseCallback: fm.GetCommentsManager().IncreaseCommentsCount,
                    beforeShowCallback: fm.GetCommentsManager().CollapseAllDivsByArticleID
                });
            });

        });


    </script>
    <asp:HiddenField ID="hfLastIndex" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfPageOpenDateTime" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfPersonID" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfIsFollowed" runat="server" ClientIDMode="Static" />
    <dx:ASPxLoadingPanel ID="ASPxLoadingPanel1" runat="server" ClientInstanceName="LoadingPanel"
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" ImageSpacing="5px"
        ContainerElementID="tblFeed" Modal="True">
        <Image Url="~/App_Themes/DevEx/Web/Loading.gif">
        </Image>
    </dx:ASPxLoadingPanel>
    <div id="newfeed">
    </div>
    <script id="feedTmpl" type="text/x-jsrender">
        {{:ViewType}}
    </script>



    <script id="tmplDate" type="text/x-jquery-tmpl">
    </script>
    <script id="tmplPubmed" type="text/x-jquery-tmpl">
    </script>
    <script id="tmplDoi" type="text/x-jquery-tmpl">
    </script>
    <script id="tmplRecomendators" type="text/x-jquery-tmpl">
    </script>

    <script id="tmplRecComInitialLoad" type="text/x-jquery-tmpl">
    </script>
    <script id="tmplRecommendation" type="text/x-jquery-tmpl">
    </script>
    <script id="tmplComment" type="text/x-jquery-tmpl">
    </script>


    <ul class="breadcrumb" style="margin-bottom:5px;">
        <li>
            <asp:HyperLink ID="hlViewedPerson" runat="server" />
            <span class="divider">/</span> </li>
        <li>
            <asp:Literal ID="litCurPage" runat="server"></asp:Literal>
        </li>
        <div class="fright" style="position: relative;">
            <input type="text" id="txtQuery" class="fleft" style="height: 20px; font-size: 11px; line-height: 11px; padding: 1px; position: absolute; left: -220px; top: -2px;" />
            <a class="btn  btn-mini fleft" id="btnFind" style="margin-right: 3px;">find</a>
            <a class="btn  btn-mini fleft" id="btnClearQuery">clear</a>
            <div class="btn-group fleft" data-toggle="buttons-radio" style="padding-left: 4px;">
                <a class="btn btn-info btn-mini feedtype active" feedtype="1" rel="popover" title="All"
                    data-content="Removes any filter applied to feed or list">all</a> <a class="btn btn-info btn-mini feedtype"
                        feedtype="2" rel="popover" title="Favorite" data-content="Applies filter to feed or list in order to show only favorite items">
                        <i class="icon-star icon-white"></i></a><a class="btn btn-info btn-mini feedtype"
                            feedtype="3" rel="popover" title="Recommended" data-content="Applies filter to feed or list in order to show only items that has been recommended">
                            <i class="icon-thumbs-up icon-white"></i></a><a class="btn btn-info btn-mini feedtype"
                                feedtype="4" rel="popover" title="Trash" data-content="Applies filter to feed or list in order to show only items that has been moved to trash">
                                <i class="icon-trash icon-white"></i></a>
            </div>
        </div>
    </ul>
    <input type="hidden" value="1" id="hfFeedtype" />
    
    
    <div id="divForwardToPubmedSearch" style="display:none; margin-bottom:7px; padding-left:555px;">
        <a target="_blank" href="" ><span style="padding-top:5px; padding-right:4px;">Search 'asdasd' in pubmed</span><img alt="" src="../Images/new_window_icon.gif" /></a>
        <div class="clearfix"></div>
    </div>
        
    <table id="tblFeed" class="table">
    </table>
    <a id="btnShowMore" class="btn">Show more</a>
    <%--new recs html--%>

    <div class="modal hide" id="recManager" style="width: 600px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <span style="color: gray">Recommend article: </span><strong>Modal header</strong>
        </div>
        <div class="modal-body">
            <div id="dRecSelect">
            <div style="margin-bottom:4px;"><strong>To:</strong></div>
            <input type="hidden" id="recSelector" style="width: 100%;" />
            <div style="margin-top:10px;" id="recPreselectedPers">
                <div><strong>or click one of following:</strong></div>
                <div id="dAlreadyRec" style="margin-top:5px;">
                    <div class="fleft" style="color: gray;width:170px;text-align:right; margin-right:5px;">Already recommended to:</div>
                    <div class="fleft" id="spAlreadyRec" style="width:380px;"></div>
                    <div class="clearfix"></div>
                </div>
                <div id="dRecentPersons" style="margin-top:5px;">
                    <div class="fleft" style="color: gray;width:170px; text-align:right;margin-right:5px;">Recent persons:</div>
                    <div class="fleft" id="spRecentPersons" style="width:380px;"></div>
                    <div class="clearfix"></div>
                </div>
                <div id="dFollowers" style="margin-top:5px;">
                    <div class="fleft" style="color: gray;width:170px; text-align:right;margin-right:5px;">Followers:</div>
                    <div class="fleft" id="spFollowers" style="width:380px;"></div>
                    <div class="clearfix"></div>
                </div>
                 <div id="dFollowed" style="margin-top:5px;">
                    <div class="fleft" style="color: gray;width:170px; text-align:right;margin-right:5px;">Followed:</div>
                    <div class="fleft" id="spFollowed" style="width:380px;"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
                <div style="margin-top:10px;">
                <div style="margin-bottom:4px;"><strong>add comment:</strong></div>
                      <textarea style="width: 98%" rows="2" id="textComment"></textarea>
                    </div>
                </div>
            <div id="dRecLoading" style="display:none; text-align:center; padding:50px;">
                <img alt="" src="../Images/ajax-loader2.gif" style="margin-right:15px;"/> Sending recommendations...
            </div>
            <div id="dRecDone"  style="display:none; text-align:center; padding:50px;">
                <h3>Done!</h3>
                </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" id="btnRecClose">Close</a>
            <a href="#" class="btn btn-primary" id="btnRecommend">Recommend</a>
        </div>
    </div>

</asp:Content>

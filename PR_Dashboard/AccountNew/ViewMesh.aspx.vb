﻿Imports BioWS.BLL.Extensions



Public Class ViewMesh
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim id = Me.QSInt32("id")
        Dim mesh As New BLL.Mesh(id)
        litTitle.Text = mesh.Title
        Me.Page.Title = mesh.Title


        Using ta As New DataSetMeshFreqTableAdapters.v_PersonsMeshFrequenciesTableAdapter()
            Dim tt = ta.GetAllbyMeshID(id)



            Dim res = From person In tt
                      Select person.PersonID,
                          person.DisplayName,
                          AvatarURL = IIf(person.Image50.Length < 12, BLL.Common.GetGravatar(person.UserName, 50), "https://bioknolstorage.blob.core.windows.net/userimages/" & person.AvatarGUID & "_50." & person.AvatarExtension),
                          person.SpecificityRank


            repSpecificPeople.DataSource = res.ToList()
        End Using
        repSpecificPeople.DataBind()

    End Sub

End Class
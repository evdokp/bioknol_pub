﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Masters/bsside.master" Theme="Theme2" CodeBehind="ViewArticle.aspx.vb" Inherits="BioWS.ViewArticle" %>

<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSplitter" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxNavBar" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>

<%@ Register Src="../UserControls/ViewWidget.ascx" TagName="ViewWidget" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cph1" runat="server">

    <script src="../Scripts/Code/ViewArticleManager.js"></script>
    <script src="../Scripts/jsrender.js"></script>
        <script src="../Scripts/TS/RecManager.js"></script>
    <script src="../Scripts/select2.min.js"></script>

    <script src="../Scripts/jquery.hovercard.min.js"></script>
    <script src="../Scripts/Code/PersonHoverCards.js"></script>

    <script type="text/javascript">
        var fm = undefined;
        $(document).ready(function () {
            $('#txtRecEmails').watermark('Type e-mail on each line...');
            $('#recPersonQuery').watermark('Type user name to find...');
            $('#recAddRecommedComment').watermark('Add a comment to your recommendation...')

            var mng = new ViewArticleManager($('#hfArticleID').val());

            $('#btnRecommendArt').click(function () {
                var recmng = new RecManager({
                    articleid: $('#hfArticleID').val(),
                    articletitle: $('h1').text(),
                    newRecommendationCallback: mng.LoadRecsAndComments,
                    commentIncreaseCallback: mng.LoadRecsAndComments
                });
            });


            PageMethods.RecentlyReadBy($("#hfArticleID").val(), function (response) {
                var data = JSON.parse(response);
                if (data.length > 0) {
                    $('#recentlyReadBy').html($('#personsTemplate').render(data))
                    $('#recentlyReadBy a.author').PersonHoverCards();
                } else {
                    $('#recentlyReadBy').prev().hide();
                    $('#recentlyReadBy').hide();
                }

            });

            // article history loading
            $("#divViewsHistLoad").html("");
            ShowArtHist();
            $("#btnArtHistShowMore").click(function () {
                ShowArtHist()
            });

            $('#btnBackToHistory').click(function () {
                //toggle divs
                $('#h2History').text('Your history of reading the article');
                $('#divArtHistLoad').toggle();
                if (showMoreHistHidden === false) {
                    $('#btnArtHistShowMore').toggle();
                }
                $('#divBeforeAndAfter').toggle();
                $('#btnBackToHistory').toggle();
                //----
            });
            //handle article click
            $('#divArtHistLoad a').live('click', function () {
                //toggle divs
                $('#h2History').text('Before and after the article view');
                $('#divArtHistLoad').toggle();
                if (showMoreHistHidden === false) {
                    $('#btnArtHistShowMore').toggle();
                }
                $('#divBeforeAndAfter').toggle();
                $('#btnBackToHistory').toggle();
                //----
                $('#hfSelectedDate').val($(this).attr('timestamp'));
                var dt = new Date(parseInt($(this).attr('timestamp').slice(6, 19)));

                $("#hfLastIndexAfter").val(0);
                $("#hfLastIndexBefore").val(0);
                $('#divArtHistUPDOWN').html('');
                $('#divArtHistUPDOWN').html('<div class="artview" id="divThisArticle"><b>This article view at ' + dt.format("dd.MM.yyyy HH:mm:ss") + '</b></div>');
                LoadAfter();
                LoadBefore();
            });

            $("#btnShowMoreAfter").hide();
            $("#btnShowMoreBefore").hide();
            LoadingPanel.Hide();
            LoadingPanelBeforeAfter.Hide();

            $("#btnShowMoreAfter").click(function () {
                LoadAfter();
            });
            $("#btnShowMoreBefore").click(function () {
                LoadBefore();
            });

            if (parseInt($('#hfRecommend').val())===1)
            {
                $('#btnRecommendArt').trigger('click')
            }

        });
        var showMoreHistHidden = false;
        function ShowArtHist() {

            $('#btnArtHistShowMore').attr('disabled', 'disabled');
            LoadingPanel.Show();




            PageMethods.GetArtHist(parseInt($("#hfArticleID").val()), $("#hfPageOpenDateTime").val(), $("#hfLastIndexArtHist").val(), function (response) {

                $('#btnArtHistShowMore').removeAttr('disabled');
                LoadingPanel.Hide();
                var obj = jQuery.parseJSON(response);
                $("#hfLastIndexArtHist").val(parseInt($("#hfLastIndexArtHist").val()) + obj.length);
                $("#tmplArtHist").tmpl(obj).appendTo("#divArtHistLoad");

                if (obj.length < 10) {
                    $("#btnArtHistShowMore").hide();
                    showMoreHistHidden = true;
                }

            });
        };

        function LoadAfter() {

            $('#btnShowMoreAfter').attr('disabled', 'disabled');
            LoadingPanelBeforeAfter.Show();
            var dt = new Date(parseInt($('#hfSelectedDate').val().slice(6, 19)));

            PageMethods.GetAfter(dt, $("#hfPageOpenDateTime").val(), $("#hfLastIndexAfter").val(), function (response) {
                $('#btnShowMoreAfter').removeAttr('disabled');
                LoadingPanelBeforeAfter.Hide();
                var obj = jQuery.parseJSON(response);
                $("#hfLastIndexAfter").val(parseInt($("#hfLastIndexAfter").val()) + obj.length);
                $("#tmplArtHistUP").tmpl(obj).prependTo("#divArtHistUPDOWN");
                if (obj.length < 10) {
                    $("#btnShowMoreAfter").hide();
                } else {
                    $("#btnShowMoreAfter").show();
                }
            });
        };

        function LoadBefore() {

            $('#btnShowMoreBefore').attr('disabled', 'disabled');
            LoadingPanelBeforeAfter.Show();
            var dt = new Date(parseInt($('#hfSelectedDate').val().slice(6, 19)));
            PageMethods.GetBefore(dt, $("#hfPageOpenDateTime").val(), $("#hfLastIndexBefore").val(), function (response) {
                $('#btnShowMoreBefore').removeAttr('disabled');
                LoadingPanelBeforeAfter.Hide();
                var obj = jQuery.parseJSON(response);
                $("#hfLastIndexBefore").val(parseInt($("#hfLastIndexBefore").val()) + obj.length);
                $("#tmplArtHistDown").tmpl(obj).appendTo("#divArtHistUPDOWN");
                if (obj.length < 10) {
                    $("#btnShowMoreBefore").hide();
                } else {
                    $("#btnShowMoreBefore").show();
                }
            });
        };
    </script>
    <script id="personsTemplate" type="text/x-jsrender">
        <div class="feed_comment">
            <a class="fleft" href="ViewPerson.aspx?objid={{:ID}}&objtype=p" target="_blank" style="margin-right:4px;">
                <img src="{{:Avatar}}" alt="" />
            </a>
        <div class="fleft">
            <a class="author" personid="{{:ID}}" href="ViewPerson.aspx?objid={{:ID}}&objtype=p" target="_blank" style="margin-right:4px;">{{:Name}}</a>
        </div>
            <div class="clearfix"></div>
        </div>
    </script>
    <asp:HiddenField ID="hfArticleID" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hfLastIndexArtHist" runat="server" ClientIDMode="Static" Value="0" />
    <asp:HiddenField ID="hfLastIndexAfter" runat="server" ClientIDMode="Static" Value="0" />
    <asp:HiddenField ID="hfLastIndexBefore" runat="server" ClientIDMode="Static" Value="0" />
    <asp:HiddenField ID="hfSelectedDate" runat="server" ClientIDMode="Static" Value="0" />
    <asp:HiddenField ID="hfPageOpenDateTime" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfRecommend" runat="server" ClientIDMode="Static" />
    <h1>
        <asp:Literal ID="litTitle" runat="server"></asp:Literal>
    </h1>

    <style>
        li.disabled a {
            color: gray;
        }

            li.disabled a:hover {
                cursor: default;
                background-color: transparent;
            }
    </style>



    <div style="margin-bottom: 20px;">
        <b>
            <asp:Literal ID="litAuthors" runat="server"></asp:Literal>

        </b>&nbsp;&nbsp;<asp:Literal ID="litJournal" runat="server"></asp:Literal><i>(<asp:Literal ID="litPubYear" runat="server"></asp:Literal>)</i>
    </div>

    <div class="row-fluid">
        <div class="span2">
            <h2>Actions</h2>

            <ul class="nav nav-pills  nav-stacked">
                <li>
                    <asp:HyperLink ID="hlPubmed" runat="server" Text="View Pubmed page" Target="_blank"></asp:HyperLink>
                </li>
                <li>
                    <asp:HyperLink ID="hlUpload" runat="server" Text="Upload" Target="_blank"></asp:HyperLink>
                </li>
                <li id="liRequest" runat="server">
                    <asp:HyperLink NavigateUrl="#" ID="hlRequest" ClientIDMode="Static" runat="server" Text="Request fulltext"></asp:HyperLink></li>
                <li><a href="#" id="btnRecommendArt">Recommend</a></li>

            </ul>
            <asp:Panel ID="pnlDownloads" runat="server">
                <h2>Downloads</h2>

                <asp:Repeater ID="repDownloads" runat="server">
                    <HeaderTemplate>
                        <ul class="nav nav-pills nav-stacked">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                            <asp:HyperLink ID="hlDownload" runat="server" NavigateUrl='<%# Eval("DownloadLink") %>'
                                Text='<%# Eval("FileName") %>'
                                Target="_blank"></asp:HyperLink>
                        </li>

                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>
                </asp:Repeater>


            </asp:Panel>

            <asp:Panel ID="pnlMesh" runat="server">
                <h2>Mesh</h2>
                <asp:Repeater ID="repMesh" runat="server">
                    <ItemTemplate>
                        <asp:HyperLink ID="hlMesh" runat="server" Font-Size="11px"
                            Text='<%# Eval("Heading")%>'
                            NavigateUrl='<%# "~\AccountNew\ViewMesh.aspx?id=" & Eval("MeSH_ID")%>'
                            Target="_blank" />

                    </ItemTemplate>
                    <SeparatorTemplate>, </SeparatorTemplate>
                </asp:Repeater>
            </asp:Panel>

        </div>

        <div class="span10">

            <div class="row-fluid">




                <div class="span6">

                    <h2>Recommended by (<span id="spanRecsCount"></span>)</h2>
                    <div id="recscontainer" class="reccomblock" style="margin-bottom: 10px; width: 100%;"></div>




                    <h2>Comments (<span id="spanCommentsCount"></span>)</h2>
                    <div id="commentscontainer"></div>
                    <div class="control-group feed_comment">
                        <textarea style="width: 97%; margin-bottom: 4px;" rows="3"></textarea><br />
                        <a class="btn btn-mini fright" style="margin-right: 2px;" id="btnAddComment">Add comment</a>
                        <div class="clearfix"></div>


                    </div>
                </div>
                <div class="span6">
                    <asp:Panel ID="pnlWhatOthers" runat="server" Style="margin-bottom: 20px;">
                        <h2>What others read with it</h2>
                        <asp:Repeater ID="repWhatOthers" runat="server">
                            <ItemTemplate>
                                <div style="margin-bottom: 5px;">
                                    <a href="ViewArticle.aspx?id=<%# Eval("Pubmed_ID")%>" target="_blank"><%# Eval("Title")%></a><br />
                                    <span style="font-size: 11px;">
                                        <b>
                                            <%# Eval("AuthorsStringShort")%>
                                        </b>
                                        <%# Eval("Journal")%>
                                        <i>(<%# Eval("PubYear")%>)</i>

                                    </span>
                                </div>


                            </ItemTemplate>

                        </asp:Repeater>
                    </asp:Panel>
                    <h2>Read by
                    </h2>

                    <div id="recentlyReadBy" style="margin-bottom: 20px;"></div>


                    <a class="btn btn-mini fright" id="btnBackToHistory" style="display: none; margin-top: -2px;">Back to history</a>
                    <h2 style="margin-bottom: 10px;" id="h2History">Your history of reading the article</h2>
                    <div id="divArtHistLoad">
                    </div>

                    <div id="divBeforeAndAfter" style="display: none;">
                        <a class="btn btn-mini" style="margin-top: 6px;" id="btnShowMoreAfter">Show more</a>
                        <div id="divArtHistUPDOWN">
                        </div>
                        <a class="btn btn-mini" style="margin-top: 6px;" id="btnShowMoreBefore">Show more</a>
                    </div>

                    <a class="btn btn-mini" style="margin-top: 6px;" id="btnArtHistShowMore">Show more</a>
                    <dx:ASPxLoadingPanel ID="ASPxLoadingPanel1" runat="server" ClientInstanceName="LoadingPanel"
                        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" ImageSpacing="5px"
                        ContainerElementID="divArtHistLoad" Modal="True">
                        <Image Url="~/App_Themes/DevEx/Web/Loading.gif">
                        </Image>
                    </dx:ASPxLoadingPanel>
                    <dx:ASPxLoadingPanel ID="ASPxLoadingPanel2" runat="server" ClientInstanceName="LoadingPanelBeforeAfter"
                        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" ImageSpacing="5px"
                        ContainerElementID="divArtHistUPDOWN" Modal="True">
                        <Image Url="~/App_Themes/DevEx/Web/Loading.gif">
                        </Image>
                    </dx:ASPxLoadingPanel>
                    <script id="tmplArtHist" type="text/x-jquery-tmpl">

                        <div style="font-family: verdana; font-size: 7pt; color: #FF6600; font-weight: bold;">
                            <a timestamp="${vTimestamp}" style="cursor: pointer;">${vDateTime}&nbsp;|&nbsp;<span style="font-weight: normal;">${vEgo}</span></a>

                        </div>


                        {{if (isFirst == 1)}}
                            <div style="font-family: verdana; font-size: 7pt; color: #FF6600; font-weight: bold;">
                                <img src="../Images/asterisk.png" />
                                First view
                            </div>
                        {{else}} 
                            <div style="font-size: 11px; color: gray;">
                                <i class="icon-arrow-up icon-gray"></i>&nbsp;${vInterval}
                            </div>
                        {{/if}} 
     
                    </script>
                    <script id="tmplArtHistUP" type="text/x-jquery-tmpl">

                        <div style="">
                            <a href="ViewInfoPM.aspx?Pubmed_ID=${vID}" target="_blank">${vTitle}</a>
                            <br />
                            <span style="font-size: 11px; color: #a6a6a6;">${vDateTime}&nbsp;|&nbsp;${vEgo}</span>
                        </div>


                        {{if (isFirst == 1)}}
                            <div style="font-family: verdana; font-size: 7pt; color: #FF6600; font-weight: bold;">
                                <img src="Images/asterisk.png" />
                                First view
                            </div>
                        {{else}} 
                            <div style="font-size: 11px; color: gray;">
                                <i class="icon-arrow-up icon-gray"></i>&nbsp;${vInterval}
                            </div>
                        {{/if}} 
     
                    </script>
                    <script id="tmplArtHistDown" type="text/x-jquery-tmpl">


                        <div style="font-size: 11px; color: gray;">
                            <i class="icon-arrow-down icon-gray"></i>&nbsp;${vInterval}
                        </div>

                        <div style="">
                            <a href="ViewInfoPM.aspx?Pubmed_ID=${vID}" target="_blank">${vTitle}</a>
                            <br />
                            <span style="font-size: 11px; color: #a6a6a6;">${vDateTime}&nbsp;|&nbsp;${vEgo}</span>

                        </div>

                    </script>

                </div>

            </div>
            <div class="row-fluid">
                <div class="span12">
                    <h2>Related data from external systems
                        
                    </h2>
                    <asp:Repeater ID="repWidgets" runat="server">
                        <ItemTemplate>
                            <div style="padding: 3px; margin-top: 4px; text-align: center; background-color: #A1ACC8"><%# Eval("Title")%></div>
                            <uc1:ViewWidget ID="ViewWidget1" runat="server" PubmedID='<%# Eval("PubmedID")%>'
                                PersonID='<%# Eval("PersonID")%>'
                                WidgetID='<%# Eval("WidgetID")%>' />
                        </ItemTemplate>
                    </asp:Repeater>




                </div>

            </div>
        </div>



    </div>



    <%--new recs html--%>

   <%--new recs html--%>

    <div class="modal hide" id="recManager" style="width: 600px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <span style="color: gray">Recommend article: </span><strong>Modal header</strong>
        </div>
        <div class="modal-body">
            <div id="dRecSelect">
            <div style="margin-bottom:4px;"><strong>To:</strong></div>
            <input type="hidden" id="recSelector" style="width: 100%;" />
            <div style="margin-top:10px;" id="recPreselectedPers">
                <div><strong>or click one of following:</strong></div>
                <div id="dAlreadyRec" style="margin-top:5px;">
                    <div class="fleft" style="color: gray;width:170px;text-align:right; margin-right:5px;">Already recommended to:</div>
                    <div class="fleft" id="spAlreadyRec" style="width:380px;"></div>
                    <div class="clearfix"></div>
                </div>
                <div id="dRecentPersons" style="margin-top:5px;">
                    <div class="fleft" style="color: gray;width:170px; text-align:right;margin-right:5px;">Recent persons:</div>
                    <div class="fleft" id="spRecentPersons" style="width:380px;"></div>
                    <div class="clearfix"></div>
                </div>
                <div id="dFollowers" style="margin-top:5px;">
                    <div class="fleft" style="color: gray;width:170px; text-align:right;margin-right:5px;">Followers:</div>
                    <div class="fleft" id="spFollowers" style="width:380px;"></div>
                    <div class="clearfix"></div>
                </div>
                 <div id="dFollowed" style="margin-top:5px;">
                    <div class="fleft" style="color: gray;width:170px; text-align:right;margin-right:5px;">Followed:</div>
                    <div class="fleft" id="spFollowed" style="width:380px;"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
                <div style="margin-top:10px;">
                <div style="margin-bottom:4px;"><strong>add comment:</strong></div>
                      <textarea style="width: 98%" rows="2" id="textComment"></textarea>
                    </div>
                </div>
            <div id="dRecLoading" style="display:none; text-align:center; padding:50px;">
                <img alt="" src="../Images/ajax-loader2.gif" style="margin-right:15px;"/> Sending recommendations...
            </div>
            <div id="dRecDone"  style="display:none; text-align:center; padding:50px;">
                <h3>Done!</h3>
                </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" id="btnRecClose">Close</a>
            <a href="#" class="btn btn-primary" id="btnRecommend">Recommend</a>
        </div>
    </div>


    <script id="tmplPerson" type="text/x-jquery-tmpl">
        <tr style="cursor: pointer;" personid="${Person_ID}">
            <td>
                <b>${DisplayName}</b>
            </td>
        </tr>
    </script>

</asp:Content>

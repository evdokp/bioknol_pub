﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Masters/bsside.Master"
    Theme="Theme2" CodeBehind="ViewGroup.aspx.vb" Inherits="BioWS.ViewGroup1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCloudControl" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp1" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cph1" runat="server">
    <script type="text/javascript">



        $(document).ready(function () {
            $.each('#tblDesc tr', function (index, value) {
              //  alert($(value).html());
            });
        });


    </script>

    <ul class="breadcrumb">
        <li>
            <asp:HyperLink ID="hlViewedGroup" runat="server" />
            <span class="divider">/</span> </li>
        <li>
            <asp:Literal ID="litCurPage" runat="server"></asp:Literal>
        </li>
    </ul>
    <ul class="breadcrumb">
        <li>Group description</li>
    </ul>


        

    <table width="100%" id="tblDesc">
         <tr>
            <td style="width: 180px; text-align:right;" class="smallfade" valign="top" >
                Description
            </td>
            <td>
        <asp:Literal ID="lit_desc" runat="server"></asp:Literal> 
            </td>
        </tr>
        <tr>
            <td style="width: 180px;text-align:right;" class="smallfade" valign="top" >
                Group is administered by
            </td>
            <td>
                <asp:HyperLink ID="hl_admin" runat="server"></asp:HyperLink>
                <span id="sp_admin" runat="server">(it's you, you may
                    <asp:HyperLink ID="hl_edit" runat="server" Text="edit" />
                    this group) </span>
            </td>
        </tr>
        <tr>
            <td style="width: 180px;text-align:right;" class="smallfade" valign="top" >
                Parent group
            </td>
            <td>
                <asp:SqlDataSource ID="sqlParentGroup" runat="server" ConnectionString="<%$ ConnectionStrings:bio_dataConnectionString %>"
                    SelectCommand="SELECT [Group_ID], [ParentGroup_ID], [Title], [ParticipantsCount],[Image50] FROM [v_GroupsParticipantsCount] WHERE ([Group_ID] = @Group_ID)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="hfParentGroupID" Name="Group_ID" PropertyName="Value"
                            Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:HiddenField ID="hfParentGroupID" runat="server" />
                <asp:Repeater ID="repParentGroup" runat="server" DataSourceID="sqlParentGroup">
                    <ItemTemplate>
                    <table>
                    <tr>
        
                    <td><asp:HyperLink ID="hlGroup" runat="server" NavigateUrl='<%# "~\AccountNew\ViewGroup.aspx?GroupID=" & Eval("Group_ID") %>'
                                    Text='<%# Eval("Title") %>' /></td>
                    </tr>
                    </table>

                    </ItemTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <tr>
            <td style="width: 180px;text-align:right;" class="smallfade" valign="top" >
                Child groups
            </td>
            <td>
                <asp:SqlDataSource ID="sqlChildGroups" runat="server" ConnectionString="<%$ ConnectionStrings:bio_dataConnectionString %>"
                    SelectCommand="SELECT [Group_ID], [ParentGroup_ID], [Title], [ParticipantsCount],[Image50] FROM [v_GroupsParticipantsCount] WHERE ([ParentGroup_ID] = @ParentGroup_ID) ORDER BY [ParticipantsCount] DESC">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="ParentGroup_ID" QueryStringField="GroupID" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                 <asp:Repeater ID="repChildGroups" runat="server" DataSourceID="sqlChildGroups">
                    <ItemTemplate>
                    <table>
                    <tr>
               
                    <td><asp:HyperLink ID="hlGroup" runat="server" NavigateUrl='<%# "~\AccountNew\ViewGroup.aspx?GroupID=" & Eval("Group_ID") %>'
                                    Text='<%# Eval("Title") %>' /></td>
                    </tr>
                    </table>

                    </ItemTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
    
    <ul class="breadcrumb">
        <li>
                         <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td valign="middle">
                 
                        Area of expertise 
                </td>
                <td valign="middle" style="padding-left:10px;">
                    <dx:ASPxTrackBar ID="ASPxTrackBar1" runat="server" Step="10" Height="20" BackColor="White"
                        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Position="10"
                        PositionStart="10" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                        <ClientSideEvents PositionChanged="function(s, e) {
    cpMESH.PerformCallback();
}" />
                    </dx:ASPxTrackBar>
                </td>
            </tr>
        </table>
                
        </li>


    </ul>


        <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" ClientInstanceName="cpMESH"
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
            <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
            </LoadingPanelImage>
            <LoadingPanelStyle ImageSpacing="5px">
            </LoadingPanelStyle>
            <PanelCollection>
                <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
                    <asp:ObjectDataSource ID="ods_MESH" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetDataByGroupID" TypeName="BioWS.ArticlesPubmedDataSetTableAdapters.MeSHDiff_OnePersonToAllTableAdapter">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ASPxTrackBar1" DefaultValue="10" Name="TopN" PropertyName="Position"
                                Type="Int32" />
                            <asp:QueryStringParameter Name="GroupID" QueryStringField="GroupID" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <dx:ASPxCloudControl ID="ASPxCloudControl1" runat="server" ClientIDMode="AutoID"
                        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" DataSourceID="ods_mesh"
                        MaxColor="#1B3F91" MinColor="#A9B9DD" TextField="Heading" ValueField="vDiff"
                        ValueColor="#1B3F91">
                        <RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                        </RankProperties>
                        <DisabledStyle ForeColor="#B1B1B8">
                        </DisabledStyle>
                    </dx:ASPxCloudControl>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>


    <ul class="breadcrumb">
        <li>Perticipants and followers</li>
    </ul>



      <div id="divParticipants">
        <table width="100%">
            <tr>
                <td valign="top" style="width: 50%">
                    <asp:ObjectDataSource ID="ods_participants" runat="server" DeleteMethod="Delete"
                        InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByGroupID"
                        TypeName="BioWS.PersonsDataSetTableAdapters.PersonsTableAdapter" UpdateMethod="Update">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="Group_ID" QueryStringField="GroupID" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <dx:ASPxGridView ID="gvParticipants" runat="server" AutoGenerateColumns="False" Width="100%"
                        DataSourceID="ods_participants" KeyFieldName="Person_ID" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                        CssPostfix="DevEx">
                        <Columns>
                            <dx:GridViewDataDateColumn VisibleIndex="0" Caption="Participants:">
                                <PropertiesDateEdit Spacing="0">
                                </PropertiesDateEdit>
                                <DataItemTemplate>
                                    <asp:Image ID="Image5" runat="server" ImageUrl='<%# "~\UserImages\" & Eval("Image50")  %>'
                                        Style="float: left; margin-right: 10px;" />
                                    <asp:HyperLink ID="hlPerson" runat="server" NavigateUrl='<%# "~\Account\ViewPerson.aspx?PersonID=" & Eval("Person_ID") %>'
                                        Text='<%# Eval("UserName") %>' />
                                    <br />
                                    <span style="color: Gray">
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("LastName") & " " & Eval("FirstName")& " " & Eval("Patronimic") %>' />
                                    </span>
                                </DataItemTemplate>
                            </dx:GridViewDataDateColumn>
                        </Columns>
                        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                            <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                            </LoadingPanelOnStatusBar>
                            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                            </LoadingPanel>
                        </Images>
                        <ImagesFilterControl>
                            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                            </LoadingPanel>
                        </ImagesFilterControl>
                        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                            </Header>
                            <LoadingPanel ImageSpacing="5px">
                            </LoadingPanel>
                        </Styles>
                        <StylesEditors ButtonEditCellSpacing="0">
                            <ProgressBar Height="21px">
                            </ProgressBar>
                        </StylesEditors>
                    </dx:ASPxGridView>
                
                </td>
                <td valign="top" style="width: 50%; padding-left: 15px;">
                    <asp:ObjectDataSource ID="odsFollowers" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetDataByFollowedGroupID" TypeName="BioWS.PersonsDataSetTableAdapters.PersonsTableAdapter">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="FollowedGroupID" QueryStringField="GroupID" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" Width="100%"
                        DataSourceID="odsFollowers" KeyFieldName="Person_ID" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                        CssPostfix="DevEx">
                        <Columns>
                            <dx:GridViewDataDateColumn VisibleIndex="0" Caption="Followers:">
                                <PropertiesDateEdit Spacing="0">
                                </PropertiesDateEdit>
                                <DataItemTemplate>
                                    <asp:Image ID="Image5" runat="server" ImageUrl='<%# "~\UserImages\" & Eval("Image50")  %>'
                                        Style="float: left; margin-right: 10px;" />
                                    <asp:HyperLink ID="hlPerson" runat="server" NavigateUrl='<%# "~\Account\ViewPerson.aspx?PersonID=" & Eval("Person_ID") %>'
                                        Text='<%# Eval("UserName") %>' />
                                    <br />
                                    <span style="color: Gray">
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("LastName") & " " & Eval("FirstName")& " " & Eval("Patronimic") %>' />
                                    </span>
                                </DataItemTemplate>
                            </dx:GridViewDataDateColumn>
                        </Columns>
                        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                            <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                            </LoadingPanelOnStatusBar>
                            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                            </LoadingPanel>
                        </Images>
                        <ImagesFilterControl>
                            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                            </LoadingPanel>
                        </ImagesFilterControl>
                        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                            </Header>
                            <LoadingPanel ImageSpacing="5px">
                            </LoadingPanel>
                        </Styles>
                        <StylesEditors ButtonEditCellSpacing="0">
                            <ProgressBar Height="21px">
                            </ProgressBar>
                        </StylesEditors>
                    </dx:ASPxGridView>
                </td>
            </tr>
        </table>
    </div>

</asp:Content>

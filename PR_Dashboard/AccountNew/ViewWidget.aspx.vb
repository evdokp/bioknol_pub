﻿Imports BioWS.BLL.Extensions

Public Class ViewWidget3
    Inherits System.Web.UI.Page

    Private ReadOnly Property ViewedObjectType As String
        Get
            Return Me.QS("objtype")
        End Get
    End Property
    Private ReadOnly Property ViewedObjectID As Integer
        Get
            Return Me.QSInt32("objid")
        End Get
    End Property
    Private ReadOnly Property IsGroup As Boolean
        Get
            Return ViewedObjectType = "g"
        End Get
    End Property
    Private ReadOnly Property WidgetID As Integer
        Get
            Return Me.QSInt32("WidgetID")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.NotPostBack() Then
            SetObjectName()

            Dim pr As New ProfileCommon
            pr = pr.GetProfile(HttpContext.Current.User.Identity.Name)
            BindWidget(Today.AddDays(-pr.DefaultPeriodDays), Today)
        End If
    End Sub



    Private Sub SetObjectName()
        If IsGroup Then
            Dim gr As New BLL.Group(ViewedObjectID)
            hlViewedPerson.Text = gr.Title
            hlViewedPerson.NavigateUrl = gr.URL
        Else
            Dim pers As New BLL.Person(ViewedObjectID)
            hlViewedPerson.Text = pers.PageHeader
            hlViewedPerson.NavigateUrl = pers.URL
        End If


    End Sub



    Private Sub BindWidget(ByVal StartDate As DateTime, ByVal EndDate As DateTime)

        Dim url_anywidget As String = "..\Widgets\ViewWidget.aspx"
        Dim url_distinctids As String = "..\Widgets\ViewPubmedIDs.aspx"
        Dim qsb As New QueryStringBuilder()
        qsb _
            .SetValue("StartDate", StartDate) _
            .SetValue("EndDate", EndDate) _
            .SetQSValue("WidgetID") _
            .SetQSValue("objtype") _
            .SetQSValue("objid")


        If Me.QSInt32("WidgetID") > 0 Then
            ASPxSplitter1.Panes(0).ContentUrl = url_anywidget & qsb.ToQueryString()
            Dim w As New BLL.Widgets.Widget(WidgetID)
            SetPageName(w.Title)
        Else
            ASPxSplitter1.Panes(0).ContentUrl = url_distinctids & qsb.ToQueryString()
            SetPageName("Distinct Pubmed IDs")
        End If
    End Sub

    Private Sub SetPageName(pagename As String)
        Dim Content2 As ContentPlaceHolder = CType(Me.Master.Master.FindControl("cph1"), ContentPlaceHolder)
        Dim hfCurPage As HiddenField = CType(Content2.FindControl("hfCurPage"), HiddenField)
        hfCurPage.Value = pagename
        litCurPage.Text = pagename
    End Sub



    Private Sub SetPeriod1_PeriodChanged(StartDate As Date, EndDate As Date) Handles SetPeriod1.PeriodChanged
        BindWidget(StartDate, EndDate)
    End Sub

End Class
﻿Imports BioWS.BLL.Extensions

Public Class ViewGroup1
    Inherits System.Web.UI.Page

    Private ReadOnly Property GroupID As Integer
        Get
            Return Me.QSInt32("GroupID")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Content2 As ContentPlaceHolder = CType(Me.Master.Master.FindControl("cph1"), ContentPlaceHolder)
        Dim hfCurPage As HiddenField = CType(Content2.FindControl("hfCurPage"), HiddenField)

        hfCurPage.Value = "Group description"
        Me.litCurPage.Text = "Group description"

        Dim gr As New BLL.Group(GroupID)
        hlViewedGroup.Text = gr.Title
        hlViewedGroup.NavigateUrl = "~\AccountNew\ViewGroup.aspx?GroupID=" & gr.Group_ID
        hfParentGroupID.Value = gr.ParentGroup_ID







        If Me.NotPostBack() Then
            '   txt_start.Text = Today.ToShortDateString
            Dim adm As New BLL.Person(gr.AddedByPersonID)
            hl_admin.Text = adm.DisplayName
            hl_admin.NavigateUrl = adm.URL
            Dim IsSamePerson As Boolean = gr.AddedByPersonID = Me.GetProfilePersonID()
            sp_admin.Visible = IsSamePerson
            If IsSamePerson Then
                hl_edit.NavigateUrl = "~\Account\AddEditGroup.aspx?GroupID=" & gr.Group_ID 'TODO - edit url as group property
            End If
        End If
    End Sub

 

End Class
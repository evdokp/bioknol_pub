﻿Imports BioWS.BLL.Extensions

Public Class ManageWidgets
    Inherits System.Web.UI.Page
    Private _currentProfile As ProfileCommon
    Private ReadOnly Property CurrentProfile As ProfileCommon
        Get
            If _currentProfile Is Nothing Then
                _currentProfile = Me.GetProfile()
            End If
            Return _currentProfile
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim pers As New BLL.Person(Me.QSInt32("objid"))
        hlViewedPerson.Text = pers.PageHeader
        hlViewedPerson.NavigateUrl = pers.URL

        Dim Content2 As ContentPlaceHolder = CType(Me.Master.Master.FindControl("cph1"), ContentPlaceHolder)
        Dim hfCurPage As HiddenField = CType(Content2.FindControl("hfCurPage"), HiddenField)


        hfCurPage.Value = "Manage widgets"
        Me.litCurPage.Text = "Manage widgets"


        DataBindWidgets()
        DataBindMyWidgets()
        'ShowHideFollowings()
    End Sub
    Private Sub DataBindWidgets()
        Using widgta As New WidgetsDataSetTableAdapters.WidgetsTableAdapter()
            Dim widgt = widgta.GetDataApproved()
            Dim widgets = From w In widgt
                          Select w.Widget_ID, w.Title, w.Description, IsOn = IsWidgetOn(w.Widget_ID, CurrentProfile.Person_ID), _
                          IsExternal = IIf(w.IsInternal, False, True), w.AllowMultiple



            Dim widgets2 = From w2 In widgets
                           Select w2.Title, w2.Description, w2.IsOn, w2.IsExternal, w2.AllowMultiple, w2.Widget_ID, _
                                   Status = IIf(w2.IsOn, "Is on", "Is off"), StatusCSS = IIf(w2.IsOn, "widgeton", "widgetoff"), _
                                   CommandImage = IIf(w2.IsOn, "~\Images\media_stop_red.png", "~\Images\media_play_green.png"), _
                                   CommandTooltip = IIf(w2.IsOn, "Turn off", "Turn on")


            gvAllApproved.DataSource = widgets2.ToList()
        End Using
        gvAllApproved.DataBind()
    End Sub
    Private Sub DataBindMyWidgets()
        Using widgta As New WidgetsDataSetTableAdapters.WidgetsTableAdapter()
            Dim widgt = widgta.GetDataByAddedByPersonID(CurrentProfile.Person_ID)

            If widgt.Count > 0 Then
                Dim widgets = From w In widgt
                          Select w.Widget_ID, w.Title, w.Description, IsOn = IsWidgetOn(w.Widget_ID, CurrentProfile.Person_ID), _
                           w.AllowMultiple, w.IsApproved




                Dim widgets2 = From w2 In widgets
                               Select w2.Title, w2.Description, w2.AllowMultiple, w2.Widget_ID, _
                                       Approved = IIf(w2.IsApproved, "Is approved", "Is not approved"), StatusCSS = IIf(w2.IsApproved, "widgeton", "widgetoff"), _
                                       CommandImage = IIf(w2.IsOn, "~\Images\media_stop_red.png", "~\Images\media_play_green.png"), _
                                       CommandTooltip = IIf(w2.IsOn, "Turn off", "Turn on")


                gvMyWidgets.DataSource = widgets2.ToList()
                gvMyWidgets.DataBind()
            Else
                pnlMyWidgets.Visible = False
            End If
        End Using

    End Sub
    Protected Function IsWidgetOn(ByVal widgetID As Integer, ByVal personID As Integer) As Boolean
        Dim widgta As New WidgetsDataSetTableAdapters.WidgetsTableAdapter
        Using widgta
            Return widgta.IsWidgetOn(widgetID, personID) > 0
        End Using
    End Function


    Protected Sub lnkb_action_Click(ByVal sender As Object, ByVal e As EventArgs)

        Dim W_ID As Integer = CType(CType(sender, LinkButton).CommandArgument, Integer)



        Using ta As New WidgetsDataSetTableAdapters.LinksPersonsToWidgetsTableAdapter()
            Dim t = ta.GetDataByWidgetIDPersonID(CurrentProfile.Person_ID, W_ID)



            If t.Count = 0 Then
                'turn on
                ta.Insert(CurrentProfile.Person_ID, W_ID, True)
            Else
                Dim r = t.Item(0)
                If r.IsOn = True Then
                    'turn off
                    r.BeginEdit()
                    r.IsOn = False
                    r.EndEdit()
                    ta.Update(r)

                    Dim feedAction As New BLL.Feed.FeedActionWidgetOff() With {.WidgetID = W_ID}
                    feedAction.Save(CurrentProfile.Person_ID)

                Else
                    'turn on
                    r.BeginEdit()
                    r.IsOn = True
                    r.EndEdit()
                    ta.Update(r)

                    Dim feedAction As New BLL.Feed.FeedActionWidgetOn() With {.WidgetID = W_ID}
                    feedAction.Save(CurrentProfile.Person_ID)

                End If
            End If
        End Using

        DataBindWidgets()

    End Sub

End Class
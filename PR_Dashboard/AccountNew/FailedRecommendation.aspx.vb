﻿Imports BioWS.BLL.Extensions


Public Class FailedRecommendation
    Inherits System.Web.UI.Page


    Private Sub InformAdmins()
        If Me.QS("error") <> "onlydoi" Then
            BLL.MailSender.SendMailMessage("noreply@bioknowledgecenter.ru",
             "BioKnol mail service",
             "evdokp@gmail.com",
             String.Empty,
             String.Empty,
             "BioKnol user failed recommedation result",
             GetAdminsMessageBody())
        End If
    End Sub

    Private Function GetAdminsMessageBody() As String
        Dim err As String = Me.QS("error")
        Dim result As String

        Select Case err
            Case "failedarticleparsing"
                result = "Provided URL was recognized as an article, but something went wrong.<br/>"
            Case "notarticlenorules"
                result = "Provided URL was not recognized as an article.<br/>"
            Case "nodomain"
                result = "Domain for the provided URL was not monitored.<br/>"
            Case Else
                result = "Unknown error.<br/>"
        End Select
        result = result & "<br/> " & Me.QS("url").UrlDecode()
        Return result
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim err As String = Me.QS("error")

        Select Case err
            Case "onlydoi"
                litMessage.Text = "Sorry for inconvenience.<br/>This URL was recognized as an article, and DOI was detected, but now we have no page for recommending DOI."
            Case "failedarticleparsing"
                litMessage.Text = "Sorry for inconvenience.<br/>Provided URL was recognized as an article, but something went wrong.<br/>BioKnol administrators were informed."
            Case "notarticlenorules"
                litMessage.Text = "Sorry for inconvenience.<br/>Provided URL was not recognized as an article.<br/>BioKnol administrators were informed."
            Case "nodomain"
                litMessage.Text = "Sorry for inconvenience.<br/>Domain for the provided URL was not monitored.<br/>BioKnol administrators were informed. If it's a scientific magazin web-site, there's a good chance we will monitor it too."
            Case Else
                litMessage.Text = "Something went wrong"
        End Select

        InformAdmins()
    End Sub

End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" Theme="Theme2" MasterPageFile="~/Masters/bsside.master" CodeBehind="ManageWidgets.aspx.vb" Inherits="BioWS.ManageWidgets" %>


<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph1" runat="server">

  <ul class="breadcrumb">
        <li>
            <asp:HyperLink ID="hlViewedPerson" runat="server" />
            <span class="divider">/</span> </li>
        <li>
            <asp:Literal ID="litCurPage" runat="server"></asp:Literal>
        </li>
    </ul>



        <dx:ASPxGridView ID="gvAllApproved" runat="server" AutoGenerateColumns="False" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
        CssPostfix="DevEx" PreviewFieldName="Description" KeyFieldName="Widget_ID" Width="100%">
        <Columns>
            <dx:GridViewDataTextColumn FieldName="Title" ShowInCustomizationForm="True" VisibleIndex="0">
                <DataItemTemplate>
                    <asp:HyperLink ID="hlWidget" runat="server" NavigateUrl='<%# "~\AccountNew\ViewWidgetInfo.aspx?WidgetID=" & Eval("Widget_ID") %>'
                        Text='<%# Eval("Title") %>' />
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn Caption="Is supplied by external systems" FieldName="IsExternal"
                ReadOnly="True" VisibleIndex="1" Width="100px">
                <HeaderStyle Wrap="True" />
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataCheckColumn Caption="Is applicable to multiple articles" FieldName="AllowMultiple"
                VisibleIndex="2" Width="100px">
                <HeaderStyle Wrap="True" />
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataTextColumn FieldName="Status" ShowInCustomizationForm="True" Width="100px"
                VisibleIndex="3">
                <DataItemTemplate>
                    <span class="<%# Eval("StatusCSS") %>">
                        <asp:Literal ID="lit_status" runat="server" Text='<%# Bind("Status") %>'></asp:Literal>
                    </span>
                </DataItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn ShowInCustomizationForm="True" Width="100px" Caption="Action"
                CellStyle-HorizontalAlign="Center" VisibleIndex="4">
                <DataItemTemplate>
                    <asp:LinkButton ID="lnkb_action" OnClick="lnkb_action_Click" runat="server" CommandArgument='<%# Bind("Widget_ID") %>'>
                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# Bind("CommandImage") %>' ToolTip='<%# Bind("CommandTooltip") %>' />
                    </asp:LinkButton>
                </DataItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center">
                </CellStyle>
            </dx:GridViewDataTextColumn>
        </Columns>
        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
            <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
            </LoadingPanelOnStatusBar>
            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
            </LoadingPanel>
        </Images>
        <ImagesFilterControl>
            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
            </LoadingPanel>
        </ImagesFilterControl>
        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
            <Header ImageSpacing="5px" SortingImageSpacing="5px">
            </Header>
            <LoadingPanel ImageSpacing="5px">
            </LoadingPanel>
        </Styles>
        <StylesEditors ButtonEditCellSpacing="0">
            <ProgressBar Height="21px">
            </ProgressBar>
        </StylesEditors>
    </dx:ASPxGridView>
    <br /><br />
    <table width="100%">
        <tr>
            <td style="width: 50%" valign="top">
            </td>
            <td style="width: 50%" valign="top">
                <h5>
                    <span>For developers and data providers </span>
                </h5>
                <dx:ASPxButton ID="btnAddWidget" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                    CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Add widget"
                    PostBackUrl="~/Account/Widget/AddEditWidget.aspx">
                </dx:ASPxButton>
            </td>
        </tr>
    
    </table>
    <asp:Panel ID="pnlMyWidgets" runat="server">


    <h5><span>Widgets I administer</span></h5>
      <dx:ASPxGridView ID="gvMyWidgets" runat="server" AutoGenerateColumns="False" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
        CssPostfix="DevEx" PreviewFieldName="Description" KeyFieldName="Widget_ID" Width="100%">
        <Columns>
            <dx:GridViewDataTextColumn FieldName="Title" ShowInCustomizationForm="True" VisibleIndex="0">
                <DataItemTemplate>
                    <asp:HyperLink ID="hlWidget" runat="server" NavigateUrl='<%# "~\AccountNew\ViewWidgetInfo.aspx?WidgetID=" & Eval("Widget_ID") %>'
                        Text='<%# Eval("Title") %>' />
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn FieldName="Approved" ShowInCustomizationForm="True" Width="100px" Caption="Approved status"
                VisibleIndex="3">
                <DataItemTemplate>
                    <span class="<%# Eval("StatusCSS") %>">
                        <asp:Literal ID="lit_status" runat="server" Text='<%# Bind("Approved") %>'></asp:Literal>
                    </span>
                </DataItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn ShowInCustomizationForm="True" Width="100px" Caption="Action"
                CellStyle-HorizontalAlign="Center" VisibleIndex="4">
                <DataItemTemplate>

                     <asp:HyperLink ID="hlWidget" runat="server" NavigateUrl='<%# "~\Account\Widget\AddEditWidget.aspx?WidgetID=" & Eval("Widget_ID") %>'
                        Text="Edit" />
                </DataItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center">
                </CellStyle>
            </dx:GridViewDataTextColumn>
        </Columns>
        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
            <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
            </LoadingPanelOnStatusBar>
            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
            </LoadingPanel>
        </Images>
        <ImagesFilterControl>
            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
            </LoadingPanel>
        </ImagesFilterControl>
        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
            <Header ImageSpacing="5px" SortingImageSpacing="5px">
            </Header>
            <LoadingPanel ImageSpacing="5px">
            </LoadingPanel>
        </Styles>
        <StylesEditors ButtonEditCellSpacing="0">
            <ProgressBar Height="21px">
            </ProgressBar>
        </StylesEditors>
    </dx:ASPxGridView>
    
    </asp:Panel>
</asp:Content>

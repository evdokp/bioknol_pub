﻿<%@ Page Title="BioKnol people search" Language="vb" AutoEventWireup="false" Theme="Theme2" MasterPageFile="~/Masters/bsside.master" CodeBehind="BrowsePersons.aspx.vb" Inherits="BioWS.BrowsePersons" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph1" runat="server">

    <asp:HiddenField ID="hfViewerID" ClientIDMode="Static" runat="server" />
    <script src="../Scripts/jsrender.js"></script>
    <script src="../Scripts/jquery.hovercard.min.js"></script>
    <script src="../Scripts/Code/PersonHoverCards.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtQuery').watermark('Type e-mail or name to find person ...');
            PageMethods.ClosestBySpec($('#hfViewerID').val(), function (response) {
                RenderPersonsListAndHide(response, '#dClosestBySpec')
            });
            PageMethods.CommentTheSame($('#hfViewerID').val(), function (response) {
                RenderPersonsListAndHide(response, '#dCommentTheSame')
            });
            PageMethods.Followers($('#hfViewerID').val(), function (response) {
                RenderPersonsListAndHide(response, '#dFollowers')
            });

            $('#txtQuery').keyup(function () {
                if ($(this).val().length > 2) {
                    PageMethods.GetByQuery($('#hfViewerID').val(), $('#txtQuery').val(), function (response) {
                        RenderPersonsList(response, '#dSearchResults',false);
                    });
                } else {
                    $('#dSearchResults').html('');
                }
            });

            
        });

        function RenderPersonsList(input, target,hide) {
            var data = JSON.parse(input);
            if (data.length > 0) {
                $(target).html($('#personsTemplate').render(data))
                $(target + ' a.author').PersonHoverCards();
            } else {
                if (hide) {
                    $(target).prev().hide();
                    $(target).hide();
                }
            }
        }

        function RenderPersonsListAndHide(input, target) {
            RenderPersonsList(input, target, true)
        }

    </script>

    <h1>Find people</h1>


    <div class="row-fluid">
        <div class="span6">
            <h2>Search
            </h2>
            <input type="text" id="txtQuery" class="input-medium" style="width:90%">
            <div id="dSearchResults" style="margin-bottom: 20px; margin-top:15px;"></div>
        </div>
        <div class="span6">
            <h2>Closest by specialization
            </h2>
            <div id="dClosestBySpec" style="margin-bottom: 20px;"></div>

            <h2>Comment same articles
            </h2>
            <div id="dCommentTheSame" style="margin-bottom: 20px;"></div>


            <h2>Followers
            </h2>
            <div id="dFollowers" style="margin-bottom: 20px;"></div>


        </div>
    </div>



    <script id="personsTemplate" type="text/x-jsrender">
        <div class="feed_comment">
            <a class="fleft" href="ViewPerson.aspx?objid={{:ID}}&objtype=p" target="_blank" style="margin-right:4px;">
                <img src="{{:Avatar}}" alt="" />
            </a>
        <div class="fleft">
            <a class="author" personid="{{:ID}}" href="ViewPerson.aspx?objid={{:ID}}&objtype=p" target="_blank" style="margin-right:4px;">{{:Name}}</a>
        </div>
            <div class="clearfix"></div>
        </div>
    </script>
</asp:Content>

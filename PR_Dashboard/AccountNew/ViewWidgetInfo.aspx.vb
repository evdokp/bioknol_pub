﻿Imports BioWS.BLL.Extensions


Public Class ViewWidgetInfo
    Inherits System.Web.UI.Page


    Private ReadOnly Property WidgetID As Integer
        Get
            Return Me.QSInt32("WidgetID")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Content2 As ContentPlaceHolder = CType(Me.Master.Master.FindControl("cph1"), ContentPlaceHolder)
        Dim hfCurPage As HiddenField = CType(Content2.FindControl("hfCurPage"), HiddenField)
        hfCurPage.Value = "_neverfound"

        Dim pers As New BLL.Person(Me.GetProfilePersonID())
        hlViewedPerson.Text = pers.PageHeader
        hlViewedPerson.NavigateUrl = pers.URL

        hlManageWidgets.NavigateUrl = "~/AccountNew/ManageWidgets.aspx?objid=" & Me.GetProfilePersonID()

        hfWidgetID.Value = Me.QS("WidgetID")


        Dim widget As New BLL.Widgets.Widget(WidgetID)
        Me.litCurPage.Text = widget.Title


        hfIsWidgetOn.Value = widget.IsOnForPerson(Me.GetProfilePersonID()).ToInt().ToString()



        If Not Page.IsPostBack Then

            

            'litTitle.Text = widget.Title
            litDesc.Text = widget.Description
            litAddedAt.Text = widget.AddedDate.ToString("D")
            litVersion.Text = widget.Version
            hlAddedBy.Text = widget.AddedByPerson.PageHeader
            hlAddedBy.NavigateUrl = widget.AddedByPerson.URL
            If widget.IsInternal Then
                litIsExternal.Text = "This widget is provided by BioKnol"
            Else
                litIsExternal.Text = "This widget is provided by external information system"
            End If

            If widget.AllowMultiple Then
                litAllowMultiple.Text = "This widget is desined to process one or many Pubmed IDs"
            Else
                litAllowMultiple.Text = "This widget is desined to process only single Pubmed ID"
            End If




        End If

        Dim personsTA As New PersonsDataSetTableAdapters.PersonsTableAdapter
        Dim personsT = personsTA.GetDataByWidgetIDisON(widget.Widget_ID)
        If personsT.Count > 0 Then
            gvUsedBy.DataSource = personsT
            gvUsedBy.DataBind()
        Else
            pnlUsedBy.Visible = False
        End If

    End Sub


    <System.Web.Services.WebMethod()> _
    Public Shared Function ToggleWidget(ByVal widgetID As Integer) As String
        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)
        Dim Widget As New BLL.Widgets.Widget(widgetID)
        Widget.ToggleOnOff(p.Person_ID)
        Return String.Empty
    End Function
End Class
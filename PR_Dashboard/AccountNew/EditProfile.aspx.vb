﻿Imports BioWS.BLL.Extensions
Imports System.Xml
Imports System.IO
Imports DevExpress.Web.ASPxUploadControl

Public Class EditProfile1
    Inherits System.Web.UI.Page
    Private Sub EditProfile_PreInit(sender As Object, e As System.EventArgs) Handles Me.PreInit
        If Not String.IsNullOrEmpty(Request.QueryString("finreg")) Then
            Me.Page.MasterPageFile = "~/Basic.master"
        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)

        Dim pers As New BLL.Person(p.Person_ID)
        hlViewedPerson.Text = pers.PageHeader
        hlViewedPerson.NavigateUrl = pers.URL


        Dim Content2 As ContentPlaceHolder = CType(Me.Master.Master.FindControl("cph1"), ContentPlaceHolder)
        Dim hfCurPage As HiddenField = CType(Content2.FindControl("hfCurPage"), HiddenField)

        hfCurPage.Value = "Edit profile"
        Me.litCurPage.Text = "Edit profile"

        
        If Page.IsPostBack = False Then



            If Not String.IsNullOrEmpty(Request.QueryString("finreg")) Then
                Me.pnlFinReg.Visible = True
            Else
                Me.pnlFinReg.Visible = False
            End If



            txtFirstName.Text = pers.FirstName
            txtLastName.Text = pers.LastName


        End If
    End Sub
    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)
        Dim pers As New BLL.Person(p.Person_ID)

        pers.LastName = txtLastName.Text
        pers.FirstName = txtFirstName.Text


        pers.Save()

        pnlSent.Visible = True
        litSent.Text = "Profile saved"


    End Sub



End Class
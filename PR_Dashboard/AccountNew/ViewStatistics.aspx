﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Masters/bsside.master" Theme="Theme2"
    CodeBehind="ViewStatistics.aspx.vb" Inherits="BioWS.ViewStatistics" %>

<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxNavBar" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp1" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSplitter" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Src="../UserControls/SetPeriod.ascx" TagName="SetPeriod" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph1" runat="server">
    <ul class="breadcrumb">
        <li>
            <asp:HyperLink ID="hlViewedPerson" runat="server" />
            <span class="divider">/</span> </li>
        <li>
            <asp:Literal ID="litCurPage" runat="server"></asp:Literal>


      


        </li>
    </ul>

    <table width="100%">
    <tr>
    <td valign="middle" align="left" >  <uc1:SetPeriod ID="SetPeriod1" runat="server" /></td>
    <td valign="middle" align="right" ><asp:DropDownList ID="cmbStatPage" runat="server" class="input-medium" Width="340px" AutoPostBack="true" >
        <asp:ListItem Text="Views and distinct Pubmed IDs timeline" Value="Statistics/ViewsTimeSeries.aspx" Selected="True" />
        <asp:ListItem Text="MESH-headings distribution" Value="Statistics/MESHDistribution.aspx" />
        <asp:ListItem Text="Authors distribution" Value="Statistics/AuthorsDistribution.aspx" />
        <asp:ListItem Text="Journal distribution" Value="Statistics/JournalsDistribution.aspx" />
        <asp:ListItem Text="Countries distribution" Value="Statistics/CountriesDistribution.aspx" />
        <asp:ListItem Text="Publication years distribution" Value="Statistics/PubYearsDistribution.aspx" />
        <asp:ListItem Text="Domains distribution" Value="Statistics/DomainsDistribution.aspx" />
    </asp:DropDownList></td>
    </tr>
    </table>


    <dx:ASPxSplitter ID="ASPxSplitter1" runat="server" ClientIDMode="AutoID" CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css"
        CssPostfix="Office2010Silver" Height="550px">
        <Panes>
            <dx:SplitterPane ContentUrlIFrameName="f1">
                <ContentCollection>
                    <dx:SplitterContentControl ID="SplitterContentControl2" runat="server" SupportsDisabledAttribute="True">
                        <asp:Literal ID="lit_w" runat="server" Text="Select widget in left navigation panel"></asp:Literal>
                    </dx:SplitterContentControl>
                </ContentCollection>
            </dx:SplitterPane>
        </Panes>
        <Styles CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css" CssPostfix="Office2010Silver">
        </Styles>
        <Images SpriteCssFilePath="~/App_Themes/Office2010Silver/{0}/sprite.css">
        </Images>
    </dx:ASPxSplitter>
</asp:Content>

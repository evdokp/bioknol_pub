﻿<%@ Page Title="" Language="vb" Theme="Theme2"  AutoEventWireup="false" MasterPageFile="~/Masters/bsside.master" CodeBehind="ManageSites.aspx.vb" Inherits="BioWS.ManageSites1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph1" runat="server">
 <ul class="breadcrumb">
        <li>
            <asp:HyperLink ID="hlViewedPerson" runat="server" />
            <span class="divider">/</span> </li>
        <li>
            <asp:Literal ID="litCurPage" runat="server"></asp:Literal>
            
        </li>
    </ul>

    
    <br />
        <table width="100%" cellpadding="5" cellspacing="2" class="table">
        <tr>
            <td style="width: 50%;" align="center">
            SiteURL
            </td>
            <td style="width: 25%;  " align="center">
                Added Date
            </td>
            <td style="width: 25%; " align="center">
                Action
            </td>
        </tr>
    </table>
        <div class="blueHeader clearFix">
        <div style="float: left;">
        </div>
        <div style="float: right; padding-right: 5px;">
        </div>
        <div class="clear">
        </div>
    </div>

                 
                    <asp:GridView ID="gv_sites" runat="server"  CssClass="table"
        AutoGenerateColumns="False" DataKeyNames="ID"
                        DataSourceID="ods_sites" AllowPaging="True" PageSize="50" ShowHeader ="false" Width ="100%" GridLines ="None"  >
                        <Columns>
                            <asp:TemplateField >
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("SiteURL") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width ="50%" />
                            </asp:TemplateField>
                            <asp:TemplateField >
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("AddedDate","{0:d}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width ="25%" HorizontalAlign ="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField >
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkb_action" runat="server" CommandArgument='<%# Bind("ID") %>'
                                        OnClick="lnkb_action_Click"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width ="25%" HorizontalAlign ="Center"  />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>


                       <asp:ObjectDataSource ID="ods_sites" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetAllApproved" TypeName="BioWS.MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter"
                        UpdateMethod="Update"></asp:ObjectDataSource>




</asp:Content>

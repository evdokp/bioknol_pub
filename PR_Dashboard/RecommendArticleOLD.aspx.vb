﻿Imports System.Threading
Imports System.Drawing
Imports BioWS.BLL.Extensions
Imports BioWS.BLL.Extensions.Web
Imports DevExpress.Data.Linq




Public Class RecommendArticleOLD
    Inherits System.Web.UI.Page

    Private ReadOnly Property PubmedIDIndicated() As Boolean
        Get
            Return Not Me.QS("PubmedID").IsNullOrEmpty()
        End Get
    End Property
    Private ReadOnly Property PubmedID() As Integer
        Get
            Return Me.QSInt32("PubmedID")
        End Get
    End Property
    Private ReadOnly Property UserID() As Integer
        Get
            Return Me.QSInt32("UserID")
        End Get
    End Property
    Private ReadOnly Property URL() As String
        Get
            Return Me.QS("URL").UrlDecode()
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "Article recommendation"
        CType(Master.Master.FindControl("page_header"), HtmlGenericControl).Visible = False



        If Me.NotPostBack() Then

            Login()

            Dim feedaction As BLL.Feed.FeedActionRecommendation


            '! ==================================================================
            '! Check if article already was recommended by this person
            '! ==================================================================


            Dim recta As New ArticlesDataSetTableAdapters.ArticleRecommendationsTableAdapter
            Dim rect As ArticlesDataSet.ArticleRecommendationsDataTable
            If PubmedIDIndicated Then
                rect = recta.GetDataByPubmedIDPersonID(PubmedID, UserID)
            Else
                rect = recta.GetDataByURLandPersonID(URL, UserID)
            End If


            '! ==================================================================
            '! Display or Parse/Insert/Display
            '! ==================================================================
            If rect.Count > 0 Then
                DisplayArticleInfo(rect(0), feedaction)
            Else
                If PubmedIDIndicated Then
                    ParseAndDisplayArticleInfo(PubmedID, feedaction)
                Else
                    ParseAndDisplayArticleInfo(URL, feedaction)
                End If
            End If
            feedaction.Save(UserID)

            ViewState.Add("FeedAction", feedaction)


        End If

    End Sub


    Private Sub DisplayArticleInfo(ByVal recrow As ArticlesDataSet.ArticleRecommendationsRow, ByRef feedaction As BLL.Feed.FeedActionRecommendation)
        If recrow.IsPubmedIDNull() = False Then
            Dim pma As New BLL.PubMed.PubMedArticle(recrow.PubmedID)
            hl_article.NavigateUrl = "~\ViewInfoPM.aspx?PubmedID=" & pma.Pubmed_ID
            hl_article.Text = pma.Title
            feedaction = New BLL.Feed.FeedActionRecommendation(pma.Pubmed_ID, "", "", "")
        Else
            hl_article.Text = recrow.Title
            If recrow.IsDOINull() = True Then
                hl_article.NavigateUrl = recrow.URL
                feedaction = New BLL.Feed.FeedActionRecommendation(0, "", recrow.URL, recrow.Title)
            Else
                hl_article.NavigateUrl = "http://dx.doi.org/" & recrow.DOI
                feedaction = New BLL.Feed.FeedActionRecommendation(0, recrow.DOI, "", recrow.Title)
            End If
        End If
        hfRecID.Value = recrow.ID
    End Sub
    Private Sub ParseAndDisplayArticleInfo(ByVal vURL As String, ByRef feedaction As BLL.Feed.FeedActionRecommendation)
        Dim vPMID As Integer?
        Dim vDOI As String
        Dim vTitle As String
        Dim vHTML As String
        Dim vPerson As Integer = Request.QueryString("UserID")
        Dim recID As Integer

        '! try find PMID
        '! If found - no other discovery required

        vPMID = BLL.PubMed.GetPubMedIDfromURL(vURL)
        'TODO find PMID from HTML as well, раз уж мы ищем DOI по HTML...

        If vPMID > 0 Then
            BLL.PubMed.ProcessPubmedView(vPMID)
            Dim pma As New BLL.PubMed.PubMedArticle(vPMID)
            hl_article.NavigateUrl = "~\ViewInfoPM.aspx?PubmedID=" & pma.Pubmed_ID
            hl_article.Text = pma.Title
            feedaction = New BLL.Feed.FeedActionRecommendation(pma.Pubmed_ID, "", "", "")
        Else
            '! try find DOI and Title
            vDOI = BLL.DOI.GetDOIbyURL_internal(vURL)
            vHTML = vURL.GetHTML()
            vDOI = BLL.DOI.GetDOIFromHTML(vHTML)
            vTitle = vHTML.GetTitleTagText()
            vPMID = Nothing


            hl_article.Text = vTitle
            If String.IsNullOrEmpty(vDOI) Then
                hl_article.NavigateUrl = vURL
                feedaction = New BLL.Feed.FeedActionRecommendation(0, "", vURL, vTitle)
            Else
                hl_article.NavigateUrl = "http://dx.doi.org/" & vDOI
                feedaction = New BLL.Feed.FeedActionRecommendation(0, vDOI, "", vTitle)
            End If
        End If

        Dim recta As New ArticlesDataSetTableAdapters.ArticleRecommendationsTableAdapter
        If String.IsNullOrEmpty(vDOI) Then
            recta.InsertArticleRecommendation(Now, vPerson, vURL, vPMID, Nothing, vTitle, recID)
        Else
            recta.InsertArticleRecommendation(Now, vPerson, vURL, vPMID, vDOI, vTitle, recID)
        End If

        hfRecID.Value = recID




    End Sub
    Private Sub ParseAndDisplayArticleInfo(ByVal vPMID As Integer, ByRef feedaction As BLL.Feed.FeedActionRecommendation)
        Dim vPerson As Integer = Request.QueryString("UserID")
        Dim recID As Integer
        Dim pma As New BLL.PubMed.PubMedArticle(vPMID)
        hl_article.NavigateUrl = pma.ViewPMURL
        hl_article.Text = pma.Title
        Dim recta As New ArticlesDataSetTableAdapters.ArticleRecommendationsTableAdapter
        recta.InsertArticleRecommendation(Now, vPerson, pma.PubmedURL, vPMID, Nothing, pma.Title, recID)
        hfRecID.Value = recID
        feedaction = New BLL.Feed.FeedActionRecommendation(vPMID, "", "", "")
    End Sub
    Private Sub Login()
        Dim vPerson As Integer

        If User.Identity.IsAuthenticated = True Then
            Dim _profile As New ProfileCommon
            _profile = _profile.GetProfile(User.Identity.Name)
            vPerson = _profile.Person_ID
            SetSessionDefaults()

        Else
            vPerson = Request.QueryString("UserID")
            If vPerson = 0 Then
                Me.Response.Redirect("~\Account\Register.aspx", False)
            Else
                Dim ta As New PersonsDataSetTableAdapters.PersonsTableAdapter
                Dim t = ta.GetDataByID(vPerson)
                Dim r = t.Item(0)
                If User.Identity.IsAuthenticated = False Then
                    FormsAuthentication.SetAuthCookie(r.UserName, False)
                    SetSessionDefaults()
                End If

            End If
        End If
    End Sub
    Protected Sub SetSessionDefaults()
        If Me.Session("StartDate") Is Nothing Then
            Dim _profile As New ProfileCommon
            _profile = _profile.GetProfile(HttpContext.Current.User.Identity.Name)
            Me.Session.Add("StartDate", Today.AddDays(-1 * _profile.DefaultPeriodDays))
        End If
        If Me.Session("EndDate") Is Nothing Then
            Me.Session.Add("EndDate", Today)
        End If
    End Sub

    Protected Sub LinqServerModeDataSource1_Selecting(sender As Object, e As LinqServerModeDataSourceSelectEventArgs) Handles LinqServerModeDataSource1.Selecting
        e.KeyExpression = "Person_ID"
    End Sub


    <System.Web.Services.WebMethod()> _
    Public Shared Function RecommendArticle(ByVal PersonToID As Integer, ByVal RecID As Integer) As String
        Dim s As String
        Dim recpersta As New ArticlesDataSetTableAdapters.ArticleRecommendationsToPersonsTableAdapter
        If recpersta.GetCountByTwoIDs(RecID, PersonToID) = 0 Then
            recpersta.Insert(RecID, PersonToID, False, DateTime.Now)
            s = 1
        Else
            s = 0
        End If
        Return s
    End Function

    Private Sub grid_DataBound(sender As Object, e As System.EventArgs) Handles grid.DataBound
        Dim i As Integer = 0
        Dim recpersta As New ArticlesDataSetTableAdapters.ArticleRecommendationsToPersonsTableAdapter
        Dim recperst = recpersta.GetDataByRecommendationID(hfRecID.Value)
        While i < grid.VisibleRowCount
            If recperst.Select("PersonToID= " & grid.GetRowValues(i, "Person_ID")).Count > 0 Then
                grid.Selection.SelectRow(i)
            End If
            System.Math.Max(System.Threading.Interlocked.Increment(i), i - 1)
        End While
    End Sub

    Private Sub cp1_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cp1.Callback
        ObjectDataSource1.DataBind()
        grid2.DataBind()
    End Sub

    Private Sub cp2_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cp2.Callback

        Dim lines As String() = TextBox1.Text.Split(Chr(10))

        Dim s As String


        s = "<ul>"

        If lines.Length = 1 And lines(0).Length = 0 Then
            s = "no emails entered"
        Else
            Dim prof As New ProfileCommon
            prof = prof.GetProfile(User.Identity.Name)
            Dim pers As New BLL.Person(prof.Person_ID)


            For Each email In lines
                If email.Length > 0 Then
                    If BLL.Common.IsValidEmail(email) Then

                        Dim vBody As String
                        vBody = BLL.MailSender.ReadFile.ReadFile(Page.Server, "Recommendation.htm")
                        vBody = vBody.Replace("<%From%>", pers.PageHeader)
                        vBody = vBody.Replace("<%ArticleTitle%>", hl_article.Text)
                        If String.IsNullOrEmpty(Request.QueryString("PubmedID")) Then
                            vBody = vBody.Replace("<%ArticleURL%>", hl_article.NavigateUrl)
                        Else
                            vBody = vBody.Replace("<%ArticleURL%>", "http://www.ncbi.nlm.nih.gov/pubmed/" & Request.QueryString("PubmedID"))
                        End If

                        Try
                            BLL.MailSender.SendMailMessage("noreply@bioknowledgecenter.ru",
                            "PRD mail service",
                            email.Trim,
                            String.Empty,
                            String.Empty,
                            "Article recommendation from Personal Reference Dashboard user",
                            vBody)

                            s = s & String.Format("<li>{0} - sent</li>", email)
                        Catch ex As Exception
                            ' nothing
                            s = s & String.Format("<li>{0} - error: {1}</li>", email, ex.Message)
                        End Try
                    Else
                        s = s & String.Format("<li>{0} is not valid email</li>", email)
                    End If
                End If
            Next
        End If

        s = s & "</ul>"



        litEmails.Text = s


    End Sub
End Class
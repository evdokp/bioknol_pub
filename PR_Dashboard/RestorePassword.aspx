﻿<%@ Page Theme="Theme2" Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Masters/bs.Master"
    CodeBehind="RestorePassword.aspx.vb" Inherits="BioWS.RestorePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph1" runat="server">
    <div class="row-fluid">
        <div class="span3">
        </div>
        <div class="span6">
            <div class="page-header">
                <h1>
                    Restore password</h1>
            </div>
            <asp:MultiView ID="mv_pass" runat="server" ActiveViewIndex="0">
                <asp:View ID="v_ask" runat="server">
                    <div class="form-horizontal">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label" for="input01">
                                    E-mail:</label>
                                <div class="controls">
                                    <asp:TextBox ID="UserName" runat="server" class="input-xlarge"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                        CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="User Name is required."
                                        ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="input01">
                                </label>
                                <div class="controls">
                                    <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="failureNotification"
                                        ValidationGroup="LoginUserValidationGroup" />
                                    <span class="failureNotification">
                                        <asp:Literal ID="FailureText" runat="server"></asp:Literal>
                                    </span>
                                </div>
                            </div>
                            <div class="form-actions">
                                <asp:Button ID="SubmitButton" class="btn btn-primary" runat="server" CommandName="Login"
                                    Text="Restore password" ValidationGroup="PasswordRecovery1"></asp:Button>
                            </div>
                        </fieldset>
                    </div>
                </asp:View>
                <asp:View ID="v_ready" runat="server">
                    <div class="alert alert-success">
                        Your password was sent to your e-mail.
                    </div>
                    <br />
                    <a class="btn" href="Default.aspx">Return to log in page</a>
                </asp:View>
            </asp:MultiView>
        </div>
        <div class="span3">
        </div>
    </div>
</asp:Content>

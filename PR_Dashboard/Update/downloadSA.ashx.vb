﻿Imports System.Web
Imports System.Web.Services
Imports System.Xml

Public Class downloadSA
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        context.Response.ContentType = "text/xml"
        context.Response.WriteFile("updateSA.xml")

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
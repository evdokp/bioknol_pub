﻿Imports System.Web
Imports System.Web.Services
Imports System.IO


Public Class DownloadFF1
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "application/rdf+xml"
        context.Response.AppendHeader("Content-Disposition", "attachment; filename=""update_prdashboard137.rdf""")
        context.Response.WriteFile(Path.Combine("~\Update", "update_prdashboard137.rdf"))
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
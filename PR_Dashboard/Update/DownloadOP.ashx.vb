﻿Imports System.Web
Imports System.Web.Services

Public Class DownloadOP
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Dim s As String
        s = "<update-info" & _
            " xmlns=""http://www.w3.org/ns/widgets""" & _
            " src=""http://ws.bioknowledgecenter.ru/Downloads/prdashboardop19.oex""" & _
            " version = ""1.9"" ></update-info>"

        context.Response.ContentType = "text/xml"
        context.Response.Write(s)

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
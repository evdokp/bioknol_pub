﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master.Master" CodeBehind="Rating.aspx.vb" Inherits="BioWS.Rating" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" 
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx"   
        DataSourceID="SqlDataSource1" KeyFieldName="Person_ID" Width="100%">
        <Columns>
           <dx:GridViewDataTextColumn  ShowInCustomizationForm="True"  Caption ="Participant" VisibleIndex="0">
                <DataItemTemplate>
                    <asp:HyperLink ID="hlPerson" runat="server"
                     NavigateUrl='<%# "~\Account\ViewPerson.aspx?PersonID=" & Eval("Person_ID") %>'
                     Text ='<%# Eval("UserName") %>'/>
                    <br />
                    <span style="color:Gray">
                        <asp:Literal ID="Literal1" runat="server" Text ='<%# Eval("LastName") & " " & Eval("FirstName")& " " & Eval("Patronimic") %>'/>
                    </span>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>


            <dx:GridViewBandColumn Caption="Days in Personal Reference Dashboard">
            <Columns>
            <dx:GridViewDataTextColumn FieldName="TotalDays" ShowInCustomizationForm="True"  Caption="Total" Width="65px"
                VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ActiveDays"  Caption="Active" Width="65px"
                ShowInCustomizationForm="True" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataProgressBarColumn FieldName="ActiveShare" ReadOnly="True"  Caption="Active share" Width="110px"
                ShowInCustomizationForm="True" VisibleIndex="7">
                <PropertiesProgressBar DisplayFormatString="" Height="" Width="">
                </PropertiesProgressBar>
            </dx:GridViewDataProgressBarColumn>
            </Columns>
            


            </dx:GridViewBandColumn>
            <dx:GridViewBandColumn Caption="Articles views' count">
            <Columns>
            <dx:GridViewDataTextColumn FieldName="PMViewsCount"  Caption="Total"  Width="65px"
                ShowInCustomizationForm="True" VisibleIndex="8">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="DistinctPMViewCount"  Caption="Distinct" Width="65px"
                ShowInCustomizationForm="True" VisibleIndex="9">
            </dx:GridViewDataTextColumn>
            </Columns>
            


            </dx:GridViewBandColumn>

            
        </Columns>
        <SettingsPager PageSize="100">
        </SettingsPager>
        <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="600" />
        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
            <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
            </LoadingPanelOnStatusBar>
            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
            </LoadingPanel>
        </Images>
        <ImagesFilterControl>
            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
            </LoadingPanel>
        </ImagesFilterControl>
        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
            <Header ImageSpacing="5px" SortingImageSpacing="5px">
            </Header>
            <LoadingPanel ImageSpacing="5px">
            </LoadingPanel>
        </Styles>
        <StylesEditors ButtonEditCellSpacing="0">
            <ProgressBar Height="21px">
            </ProgressBar>
        </StylesEditors>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:bio_conntection_string %>" 
        
        SelectCommand="SELECT * FROM [v_Persons_Rating] ORDER BY [ActiveShare] DESC"></asp:SqlDataSource>







</asp:Content>

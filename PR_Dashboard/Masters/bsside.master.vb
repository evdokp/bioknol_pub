﻿Imports BioWS.BLL.Extensions
Imports DevExpress.Web.ASPxUploadControl
Imports System.IO
Imports AzureStorageLibrary

Public Class bsside
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SetMenuItemsLinks()
        ToggleAvatar()
        SetFollowManagerValues()


        If Page.User.Identity.IsAuthenticated Then
            DataBindWidgets()
        End If

    End Sub
    Private Sub ToggleAvatar()
        If Page.User.Identity.IsAuthenticated Then
            If ViewedObjectType = "p" Then
                Dim pers As New BLL.Person(ViewedObjectID)
                imgAvatar.ImageUrl = pers.Image200

                SetActivityStatus(pers.UserName)

                If Not pers.HasImage Then
                    trDelAvatar.Attributes.Add("style", "display:none")
                End If
            ElseIf NoObject Then
                Dim pers As New BLL.Person(ViewerPersonID)

                SetActivityStatus(pers.UserName)

                imgAvatar.ImageUrl = pers.Image200
                If Not pers.HasImage Then
                    trDelAvatar.Attributes.Add("style", "display:none")
                End If
            Else
                imgAvatar.Visible = False
            End If
        Else
            imgAvatar.Visible = False
        End If
    End Sub
    Private Sub SetActivityStatus(ByVal username As String)

        Dim _profile As New ProfileCommon
        _profile = _profile.GetProfile(username)

        Dim lastActivity As DateTime = _profile.LastActivityDate
        Dim distance = DateTime.Today - lastActivity

        Dim status As String
        If distance.TotalDays < 31 Then
            status = "Active"
        Else
            status = "Dormant"
            status = status & "<br/><span style=""font-size:11px;"">inactive for more then 30 days</span>"
        End If

        litStatus.Text = status

    End Sub
    Private Sub SetMenuItemsLinks()
        Dim qsb As New QueryStringBuilder()
        If NoObject Then
            '! no object defined - we set QueryString to point to currently logged in person
            qsb.SetValue("objid", ViewerPersonID).SetValue("objtype", "p")
        Else
            qsb.SetQSValue("objid").SetQSValue("objtype")
            If Me.QS("objtype").IsNullOrEmpty() Then
                qsb.SetValue("objtype", "p")
            End If
        End If


        hlSideMngWidg.NavigateUrl = "~/AccountNew/ManageWidgets.aspx?objid=" & ViewerPersonID
        hlProfile.NavigateUrl = IIf(IsGroup, "~\AccountNew\ViewGroup.aspx", "~\AccountNew\ViewPerson.aspx") & qsb.ToQueryString()
        hlStat.NavigateUrl = "~\AccountNew\ViewStatistics.aspx" & qsb.ToQueryString()
        hlActivity.NavigateUrl = "~\AccountNew\MyActivity.aspx" & qsb.SetValue("followed", 0).ToQueryString()
        hlFollowedActivity.NavigateUrl = "~\AccountNew\MyActivity.aspx" & qsb.SetValue("followed", 1).ToQueryString()

    End Sub







    Private Function IsPersonFollowed() As Boolean
        Dim followta As New PersonsDataSetTableAdapters.FollowingsTableAdapter
        Dim followt = followta.GetDataByPersonFollowedPerson(ViewerPersonID, ViewedObjectID)
        If followt.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub SetFollowManagerValues()
        hfIsGroup.Value = IsGroup.ToInt().ToString()
        hfIsOwn.Value = IsOwn.ToInt().ToString()
        hfIsObjectFollowed.Value = IsPersonFollowed.ToInt().ToString()
        hfViewedPersonID.Value = ViewedObjectID().ToString()
        hfHideAll.Value = NoObject.ToInt().ToString()
        objectTopMenu.Visible = NoObject.Inverse

    End Sub

    Private _curprofile As ProfileCommon
    Private ReadOnly Property CurProfile As ProfileCommon
        Get
            If _curprofile Is Nothing Then
                _curprofile = Me.GetProfile()
            End If
            Return _curprofile
        End Get
    End Property
    Private ReadOnly Property IsOwn As Boolean
        Get
            Return IIf(IsGroup, False, CurProfile.Person_ID = ViewedObjectID)
        End Get
    End Property
    Private ReadOnly Property IsGroup As Boolean
        Get
            Return ViewedObjectType = "g"
        End Get
    End Property

    Private ReadOnly Property ViewerPersonID As Integer
        Get
            Return CurProfile.Person_ID
        End Get
    End Property

    Private ReadOnly Property ViewedObjectType As String
        Get
            Return Me.QS("objtype")
        End Get
    End Property
    Private ReadOnly Property ViewedObjectID As Integer
        Get
            Return Me.QSInt32("objid")
        End Get
    End Property

    Private ReadOnly Property NoObject As Boolean
        Get
            Return Me.QS("objid").IsNullOrEmpty()
        End Get
    End Property

    Private Sub DataBindWidgets()
        Dim ta As New WidgetsDataSetTableAdapters.WidgetsTableAdapter
        Dim resdict As New Dictionary(Of String, String)
        Dim qsb As New QueryStringBuilder()

        If NoObject Then
            qsb.SetValue("objid", ViewerPersonID).SetValue("objtype", "p")
        Else
            qsb.SetQSValue("objid").SetQSValue("objtype")
        End If



        resdict.Add("Distinct Pubmed IDs", "~\AccountNew\ViewWidget.aspx" & qsb.SetValue("WidgetID", 0).ToQueryString())

        Dim t = ta.GetDataByPersonID_ON_MULT(ViewerPersonID)
        If t.Count > 0 Then
            For Each row In t
                resdict.Add(row.Title, "~\AccountNew\ViewWidget.aspx" & qsb.SetValue("WidgetID", row.Widget_ID).ToQueryString())
            Next
        End If


        Repeater1.Visible = True
        Dim widgets = From widget In resdict
                      Select New With {.Title = widget.Key, .NavigateUrl = widget.Value}
        Repeater1.DataSource = widgets
        Repeater1.DataBind()

    End Sub






    Private Sub ASPxUploadControl1_FileUploadComplete(sender As Object, e As FileUploadCompleteEventArgs) Handles ASPxUploadControl1.FileUploadComplete
        e.CallbackData = SavePostedFile(e.UploadedFile)
    End Sub
    Private Function SavePostedFile(ByVal uploadedFile As UploadedFile) As String

        Dim forg, f5050, f200200 As Byte()
        Dim originalName As String = Path.GetFileName(uploadedFile.FileName)
        Dim extension As String = Path.GetExtension(originalName)

        forg = New Byte(uploadedFile.ContentLength) {}
        forg = uploadedFile.FileBytes

        Dim isvalid As String = BLL.ImageHelper.ImageUploadValidate(uploadedFile.ContentType, uploadedFile.ContentLength)
        If isvalid = "true" Then
            Dim fileguid As Guid = Guid.NewGuid

            f5050 = BLL.ImageHelper.MakeThumb(forg, 50, 50)
            f200200 = BLL.ImageHelper.MakeThumbMaxWidth(forg, 200)

            Dim storage As New AzureStorage()
            storage.UploadImage(f5050, String.Format("{0}{1}{2}", fileguid, "_50", extension))
            storage.UploadImage(f200200, String.Format("{0}{1}{2}", fileguid, "_200", extension))

            'BLL.ImageHelper.SaveFile(fileguid, "_50", originalName, f5050, Me.Page)
            'BLL.ImageHelper.SaveFile(fileguid, "_200", originalName, f200200, Me.Page)

            Dim p As New ProfileCommon
            p = p.GetProfile(HttpContext.Current.User.Identity.Name)
            Dim pers As New BLL.Person(p.Person_ID)
            pers.UpdatePersonImage(fileguid.ToString, Path.GetExtension(originalName).Substring(1))
            Return pers.Image200()
        Else
            Return "Error"
        End If
    End Function
End Class
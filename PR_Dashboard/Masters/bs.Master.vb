﻿Imports BioWS.BLL.Extensions


Public Class bs
    Inherits System.Web.UI.MasterPage

    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Page.User.Identity.IsAuthenticated Then
            Dim p As New ProfileCommon
            p = p.GetProfile(HttpContext.Current.User.Identity.Name)
            Dim pers As New BLL.Person(p.Person_ID)
            Dim litUserName As Literal = LoginView1.FindControl("litUserName")
            litUserName.Text = pers.PageHeader

            If pers.IsAdmin = False Then
                Dim pnlManagePRD As Panel = CType(LoginView1.FindControl("pnlManagePRD"), Panel)
                pnlManagePRD.Visible = False
            End If

            hlLogo.NavigateUrl = "~\AccountNew\MyActivity.aspx?followed=0&objtype=p&objid=" & p.Person_ID

            Dim hlEditProfile As HyperLink = CType(LoginView1.FindControl("hlEditProfile"), HyperLink)
            hlEditProfile.NavigateUrl = "~\AccountNew\EditProfile.aspx?objid=" & p.Person_ID

            Dim hlManageWidgets As HyperLink = CType(LoginView1.FindControl("hlManageWidgets"), HyperLink)
            hlManageWidgets.NavigateUrl = "~/AccountNew/ManageWidgets.aspx?objid=" & p.Person_ID


            Dim hlManageSites As HyperLink = CType(LoginView1.FindControl("hlManageSites"), HyperLink)
            hlManageSites.NavigateUrl = "~/AccountNew/ManageSites.aspx?objid=" & p.Person_ID
        Else
            hlLogo.NavigateUrl = "~\Default.aspx"
        End If
    End Sub

    Protected Sub lnkbLogout_Click(sender As Object, e As EventArgs)
        FormsAuthentication.SignOut()
        Response.Redirect("~\Default.aspx")
    End Sub
End Class
﻿Imports DevExpress.Web
Imports System.Drawing
Imports BioWS.BLL.Extensions

Public Class Admin
    Inherits MasterPage


    Private Sub SetMI(menu As DevExpress.Web.ASPxMenu.ASPxMenu, itemname As String, url As String, text As String)
        Dim item As DevExpress.Web.ASPxMenu.MenuItem = menu.Items.FindByName(itemname)
        If Not url.IsNullOrEmpty() Then
            item.NavigateUrl = String.Format("{0}?WidgetID={1}", url, Me.QS("WidgetID"))
        End If
        If Not text.IsNullOrEmpty() Then
            item.Text = text
        End If
        item.Visible = True
    End Sub
    Private ReadOnly Property DomainID() As String
        Get
            Return Me.QS("DomainID")
        End Get
    End Property
    Private ReadOnly Property DomainIndicated() As Boolean
        Get
            Return Not DomainID.IsNullOrEmpty()
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Page.NotPostBack() Then
            pnlDomain.Visible = DomainIndicated
            If DomainIndicated Then
                menuDomain.Items.FindByName("editInfo").NavigateUrl = "~/Account/Admin/EditDomain.aspx?DomainID=" & DomainID
                menuDomain.Items.FindByName("domainStat").NavigateUrl = "~/Account/Admin/DomainStatistics.aspx?DomainID=" & DomainID
            End If
        End If
    End Sub



End Class
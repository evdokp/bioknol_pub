﻿Imports System.Security.Cryptography
Imports System.Web.UI.DataVisualization.Charting
Imports System.Drawing
Imports System.Diagnostics.Contracts


Public Class _Default
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
     

        'CType(Master.Master.FindControl("lit_pagetitle"), Literal).Text = "Personal reference dashboard browser plug-in download"

        If Page.IsPostBack = False Then
            Me.imgbtnIE.ImageUrl = "~/Images/dwldsmalldisabled.png"
            Me.hlFF.ImageUrl = "~/Images/dwldsmalldisabled.png"
            Me.hlCHR.ImageUrl = "~/Images/dwldsmalldisabled.png"
            Me.hlOpera.ImageUrl = "~/Images/dwldsmalldisabled.png"
            Me.hlSafari.ImageUrl = "~/Images/dwldsmalldisabled.png"
        End If

      


    End Sub




    Private Sub btnAccept_Click(sender As Object, e As System.EventArgs) Handles btnAccept.Click
        Me.btnAccept.Enabled = False


        Me.imgbtnIE.PostBackUrl = "~/Downloads/Download.aspx"
        Me.hlFF.NavigateUrl = "~/Downloads/prdashboard137.xpi"
        Me.hlCHR.NavigateUrl = "https://chrome.google.com/webstore/detail/bioknol/ninapddogalfclmalljaadnjnbpnjijj/related"
        Me.hlOpera.NavigateUrl = "~/Downloads/prdashboardop19.oex"
        Me.hlSafari.NavigateUrl = "~/Downloads/prdashboard20.safariextz"


        Me.imgbtnIE.ImageUrl = "~/Images/dwldsmall.png"
        Me.hlFF.ImageUrl = "~/Images/dwldsmall.png"
        Me.hlCHR.ImageUrl = "~/Images/dwldsmall.png"
        Me.hlOpera.ImageUrl = "~/Images/dwldsmall.png"
        Me.hlSafari.ImageUrl = "~/Images/dwldsmall.png"
    End Sub


End Class

﻿Imports DevExpress.Web
Public Class Group
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


     

        If Page.IsPostBack = False Then
            Dim ta As New WidgetsDataSetTableAdapters.WidgetsTableAdapter
            Dim _profile As New ProfileCommon
            _profile = _profile.GetProfile(HttpContext.Current.User.Identity.Name)


            If Request.QueryString("GroupID") = "" Then
                ASPxMenu1.Visible = False
                ASPxMenu2.Visible = False
                ASPxMenu3.Visible = False
                imgAvatar.Visible = False
                divActions.Visible = False
                divWidgets.Visible = False
            Else
                Dim gr As New BLL.Group(Request.QueryString("GroupID"))

                If gr.HasImage Then
                    imgAvatar.ImageUrl = gr.Image200
                Else
                    imgAvatar.Visible = False
                End If

                If gr.AddedByPersonID = _profile.Person_ID Then
                    Me.ASPxMenu3.Items.FindByName("miEdit").NavigateUrl = "~/Account/AddEditGroup.aspx?GroupID=" & gr.Group_ID
                Else
                    Me.ASPxMenu3.Items.FindByName("miEdit").Visible = False
                End If

                Me.ASPxMenu1.Items.FindByName("miOverview").NavigateUrl = "~\Account\ViewGroup.aspx?GroupID=" & Request.QueryString("GroupID")
                Me.ASPxMenu1.Items.FindByName("miActivityFeed").NavigateUrl = "~\Account\GroupHistory.aspx?GroupID=" & Request.QueryString("GroupID")
                Me.ASPxMenu1.Items.FindByName("miReadingProfile").NavigateUrl = "~\Account\ReadingProfileGroup.aspx?GroupID=" & Request.QueryString("GroupID") & "&PersonID=" & _profile.Person_ID
                Me.ASPxMenu1.Items.FindByName("Statistics").NavigateUrl = "~\Account\GroupHistoryStatistics.aspx?GroupID=" & Request.QueryString("GroupID")



                Dim followta As New PersonsDataSetTableAdapters.FollowingsTableAdapter
                Dim followt = followta.GetDataByPersonFollowedGroup(_profile.Person_ID, Request.QueryString("GroupID"))
                If followt.Count > 0 Then
                    Me.ASPxMenu3.Items.FindByName("miFollow").Text = "Unfollow"
                Else
                    Me.ASPxMenu3.Items.FindByName("miFollow").Text = "Follow"
                End If


                Dim t = ta.GetDataByPersonID_ON_MULT(_profile.Person_ID)
                If t.Count > 0 Then
                    Me.ASPxMenu2.Visible = True
                    For i = 0 To t.Count - 1
                        Dim item As New ASPxMenu.MenuItem
                        item.Text = t.Item(i).Title
                        item.Name = t.Item(i).Widget_ID
                        item.NavigateUrl = String.Format("~\Account\GroupHistoryWidgets.aspx?GroupID={0}&WidgetID={1}", Request.QueryString("GroupID"), t.Item(i).Widget_ID)
                        Me.ASPxMenu2.Items.Add(item)
                    Next


                End If
                Dim item1 As New ASPxMenu.MenuItem
                item1.Text = "Distinct Pudmed IDs"
                item1.Name = 0
                item1.NavigateUrl = String.Format("~\Account\GroupHistoryWidgets.aspx?GroupID={0}&WidgetID={1}", Request.QueryString("GroupID"), 0)
                Me.ASPxMenu2.Items.Add(item1)

            End If

        End If

            

    End Sub

    Protected Sub ASPxMenu3_ItemClick(source As Object, e As DevExpress.Web.ASPxMenu.MenuItemEventArgs) Handles ASPxMenu3.ItemClick
        Select Case e.Item.Name
            Case "miFollow"
                Dim _profile As New ProfileCommon
                _profile = _profile.GetProfile(HttpContext.Current.User.Identity.Name)
                Dim followta As New PersonsDataSetTableAdapters.FollowingsTableAdapter
                Dim followt = followta.GetDataByPersonFollowedGroup(_profile.Person_ID, Request.QueryString("GroupID"))
                If followt.Count > 0 Then
                    followta.Delete(_profile.Person_ID, 0, Request.QueryString("GroupID"))
                    e.Item.Text = "Follow"

                    Dim feedaction As New BLL.Feed.FeedActionGroupUnFollow(Request.QueryString("GroupID"))
                    feedaction.Save(_profile.Person_ID)

                Else
                    followta.Insert(_profile.Person_ID, 0, Request.QueryString("GroupID"))
                    e.Item.Text = "Unfollow"

                    Dim feedaction As New BLL.Feed.FeedActionGroupFollow(Request.QueryString("GroupID"))
                    feedaction.Save(_profile.Person_ID)
                End If
        End Select
    End Sub
End Class
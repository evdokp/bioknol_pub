﻿Imports BioWS.BLL.Extensions
Imports BioWS.BLL

Public Class RecommendArticle
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Me.NotPostBack() Then
            Login()
            Dim ri As New RecommendationRedirectItem(Me.QS("URL"))
            Response.Redirect(ri.GetRedirectUrl())
        End If

    End Sub

    Private Sub Login()
        Dim vPerson As Integer

        If User.Identity.IsAuthenticated = True Then
            Dim _profile As New ProfileCommon
            _profile = _profile.GetProfile(User.Identity.Name)
            vPerson = _profile.Person_ID
            SetSessionDefaults()

        Else
            vPerson = Request.QueryString("UserID")
            If vPerson = 0 Then
                Me.Response.Redirect("~\Account\Register.aspx", False)
            Else
                Dim ta As New PersonsDataSetTableAdapters.PersonsTableAdapter
                Dim t = ta.GetDataByID(vPerson)
                Dim r = t.Item(0)
                If User.Identity.IsAuthenticated = False Then
                    FormsAuthentication.SetAuthCookie(r.UserName, False)
                    SetSessionDefaults()
                End If

            End If
        End If
    End Sub
    Protected Sub SetSessionDefaults()
        If Me.Session("StartDate") Is Nothing Then
            Dim _profile As New ProfileCommon
            _profile = _profile.GetProfile(HttpContext.Current.User.Identity.Name)
            Me.Session.Add("StartDate", Today.AddDays(-1 * _profile.DefaultPeriodDays))
        End If
        If Me.Session("EndDate") Is Nothing Then
            Me.Session.Add("EndDate", Today)
        End If
    End Sub

End Class
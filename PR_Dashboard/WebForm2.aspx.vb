﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.Drawing

Public Class WebForm2
    Inherits System.Web.UI.Page

    Private Sub MeshCorellation(ByVal x As Integer, ByVal y As Integer)
        Dim personx = New BLL.Person(x)
        Dim persony = New BLL.Person(y)

        Dim cha = dotsMeshDistr.ChartAreas(0)
        cha.AxisX.Title = personx.DisplayName
        cha.AxisY.Title = persony.DisplayName


        '! custom labels
        cha.AxisX.Crossing = 0
        cha.AxisX.Minimum = 0
        cha.AxisX.Maximum = 200


        Dim xless As New CustomLabel(0, 100, "less specific", 0, LabelMarkStyle.None)
        Dim xmore As New CustomLabel(100, 200, "more specific", 0, LabelMarkStyle.None)
        cha.AxisX.CustomLabels.Add(xless)
        cha.AxisX.CustomLabels.Add(xmore)

        Dim yless As New CustomLabel(0, 100, "less specific", 0, LabelMarkStyle.None)
        Dim ymore As New CustomLabel(100, 200, "more specific", 0, LabelMarkStyle.None)
        cha.AxisY.CustomLabels.Add(yless)
        cha.AxisY.CustomLabels.Add(ymore)

        cha.AxisY.LabelStyle.Angle = -90


        Dim s1 = dotsMeshDistr.Series(0)





        Dim db = New DataClassesMeshDataContext
        Dim data = From item In db.mesh_GetMeshCorrelationForTwoPersons(21, 68)
                   Select item



        For Each item In data
            Dim dp As New DataPoint
            dp.SetValueXY(item.x, item.y)
            dp.ToolTip = item.Heading & "\n" & _
                "Specificity rank for " & personx.DisplayName & ": " & (200 - item.x) & "\n" & _
                "Specificity rank for " & persony.DisplayName & ": " & (200 - item.y)

            s1.Points.Add(dp)
        Next
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim x = 21
        Dim y = 68
        MeshCorellation(x, y)

    End Sub

End Class
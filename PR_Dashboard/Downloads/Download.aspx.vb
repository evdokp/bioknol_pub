﻿Imports System.IO
Public Class Download
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.ContentType = "application/x-unknown"
        Response.AppendHeader("Content-Disposition", "attachment; filename=""prdashboard2430.exe""")
        Response.WriteFile(Path.Combine("~\Downloads", "prdashboard2430.exe"))
    End Sub

End Class
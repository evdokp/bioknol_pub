﻿Imports System.Web
Imports System.Web.Services
Imports System.IO
Imports AzureStorageLibrary

Public Class Download1
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim vFileName As String = context.Request.QueryString("vFileName")
        Dim vOriginalName As String = context.Request.QueryString("vOriginalName")
        vFileName = HttpUtility.UrlDecode(vFileName)
        vOriginalName = HttpUtility.UrlDecode(vOriginalName)
        context.Response.ContentType = "application/x-unknown"
        context.Response.AppendHeader("Content-Disposition", "attachment; filename=""" & vOriginalName & """")
        Dim st As Stream = context.Response.OutputStream
        Dim storage As New AzureStorage
        storage.DownloadFileToStream(vFileName, st)
        context.Response.Flush()

        'context.Response.WriteFile(Path.Combine("~\UploadedArticles", vFileName))
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
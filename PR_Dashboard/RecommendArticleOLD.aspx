﻿<%@ Page 
    Title="" 
    Language="vb" 
    AutoEventWireup="false" 
    MasterPageFile="~/Basic.Master" 
    CodeBehind="RecommendArticleOLD.aspx.vb" 
    Inherits="BioWS.RecommendArticleOLD" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridLookup" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Data.Linq" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallbackPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
    function OnRowClick(e) {
        //Clear the text selection
        _aspxClearSelection();
        //Select the row
        grid.SelectRow(e.visibleIndex, true);
        grid.GetRowValues(e.visibleIndex, 'Person_ID', OnGetRowValues);
    }


    function OnGetRowValues(values) {
        PageMethods.RecommendArticle(values, $('#hfRecID').val(), function (response) {
            cp1.PerformCallback();
        });
    }


</script>


  <h1 style="margin-top:1px; margin-bottom:12px;"><asp:HyperLink ID="hl_article" runat="server" Target="_blank"></asp:HyperLink></h1>
    <asp:HiddenField ID="hfRecID" runat="server" ClientIDMode="Static"  />


    <p>
        This article was added to your recommendation list.
    </p>
    <p>
        You may specifically address this recommendation to certain people.
    </p>





    <table width="100%">
    <tr>
    <td style="width:50%;" valign="top" >
    <h5><span>  Recommend article to PRD users (click on row)</span></h5>
    
    <dx:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False"
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx"  ClientInstanceName="grid"
        DataSourceID="LinqServerModeDataSource1" Width="100%" 
        KeyFieldName="Person_ID">
        <SettingsPager NumericButtonCount="5">
        </SettingsPager>
        <Settings ShowFilterRow="True" />

 <ClientSideEvents RowClick="function(s, e) { OnRowClick(e); }" />


        <Columns>

             <dx:GridViewDataTextColumn VisibleIndex="1" FieldName="CombinedName" CellStyle-HorizontalAlign="Left" Caption="Participant">
                  <Settings AutoFilterCondition="Contains" />

                 <DataItemTemplate>
                 <%# Eval("UserName") %>
                    <br />
                    <span style="color:Gray">
                        <asp:Literal ID="Literal1" runat="server" Text ='<%# Eval("FIO") %>'/><br />
                  
                    </span>
                          <asp:HyperLink ID="HyperLink1" runat="server" style="float:right;"
                     NavigateUrl='<%# "~\Account\ViewPerson.aspx?PersonID=" & Eval("Person_ID") %>'
                     Text ="View profile"
                    />
                </DataItemTemplate>

<CellStyle HorizontalAlign="Left"></CellStyle>
            </dx:GridViewDataTextColumn>
         
        </Columns>
        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
            <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
            </LoadingPanelOnStatusBar>
            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
            </LoadingPanel>
        </Images>
        <ImagesFilterControl>
            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
            </LoadingPanel>
        </ImagesFilterControl>
        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
            <Header ImageSpacing="5px" SortingImageSpacing="5px">
            </Header>
            <LoadingPanel ImageSpacing="5px">
            </LoadingPanel>
        </Styles>
        <StylesEditors ButtonEditCellSpacing="0">
            <ProgressBar Height="21px">
            </ProgressBar>
        </StylesEditors>
    </dx:ASPxGridView>
    <dx:LinqServerModeDataSource ID="LinqServerModeDataSource1" runat="server"   
        ContextTypeName="BioWS.DataClassesPersonsDataContext" TableName="v_Persons" />
        </td>
    <td style="width:50%; padding-left:15px; " valign="top" >
    
       <h5><span>Already recommended to</span></h5>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            DeleteMethod="Delete" InsertMethod="Insert" 
            OldValuesParameterFormatString="original_{0}" 
            SelectMethod="GetDataByRecommendationID" 
            TypeName="BioWS.PersonsDataSetTableAdapters.PersonsTableAdapter" 
            UpdateMethod="Update">
            <DeleteParameters>
                <asp:Parameter Name="Original_Person_ID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="UserName" Type="String" />
                <asp:Parameter Name="FirstName" Type="String" />
                <asp:Parameter Name="LastName" Type="String" />
                <asp:Parameter Name="Patronimic" Type="String" />
                <asp:Parameter Name="RegisterDate" Type="DateTime" />
                <asp:Parameter Name="AvatarGUID" Type="String" />
                <asp:Parameter Name="AvatarExtension" Type="String" />
                <asp:Parameter Name="IsAdmin" Type="Boolean" />
            </InsertParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="hfRecID" Name="RecommendationID" 
                    PropertyName="Value" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="UserName" Type="String" />
                <asp:Parameter Name="FirstName" Type="String" />
                <asp:Parameter Name="LastName" Type="String" />
                <asp:Parameter Name="Patronimic" Type="String" />
                <asp:Parameter Name="RegisterDate" Type="DateTime" />
                <asp:Parameter Name="AvatarGUID" Type="String" />
                <asp:Parameter Name="AvatarExtension" Type="String" />
                <asp:Parameter Name="IsAdmin" Type="Boolean" />
                <asp:Parameter Name="Original_Person_ID" Type="Int32" />
            </UpdateParameters>
        </asp:ObjectDataSource>
        <dx:ASPxCallbackPanel ID="cp1" runat="server" Width="100%" 
            ClientInstanceName="cp1" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" 
            CssPostfix="DevEx">
            <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
            </LoadingPanelImage>
            <LoadingPanelStyle ImageSpacing="5px">
            </LoadingPanelStyle>
            <PanelCollection>
<dx:PanelContent runat="server" SupportsDisabledAttribute="True">



    
    
    <dx:ASPxGridView ID="grid2" runat="server" AutoGenerateColumns="False"
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx"  ClientInstanceName="grid2"
        DataSourceID="ObjectDataSource1" Width="100%" 
        KeyFieldName="Person_ID">
        <Settings ShowFilterRow="True" />

        <Columns>

             <dx:GridViewDataTextColumn VisibleIndex="1" FieldName="CombinedName" CellStyle-HorizontalAlign="Left" Caption="Participant">
                  <Settings AutoFilterCondition="Contains" />

                 <DataItemTemplate>
                 <%# Eval("UserName") %>
                    <br />
                    <span style="color:Gray">
                        <asp:Literal ID="Literal1" runat="server" Text ='<%# Eval("FirstName") %>'/><br />
                  
                    </span>
                          <asp:HyperLink ID="HyperLink1" runat="server" style="float:right;"
                     NavigateUrl='<%# "~\Account\ViewPerson.aspx?PersonID=" & Eval("Person_ID") %>'
                     Text ="View profile"
                    />
                </DataItemTemplate>

<CellStyle HorizontalAlign="Left"></CellStyle>
            </dx:GridViewDataTextColumn>
         
        </Columns>
        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
            <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
            </LoadingPanelOnStatusBar>
            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
            </LoadingPanel>
        </Images>
        <ImagesFilterControl>
            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
            </LoadingPanel>
        </ImagesFilterControl>
        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
            <Header ImageSpacing="5px" SortingImageSpacing="5px">
            </Header>
            <LoadingPanel ImageSpacing="5px">
            </LoadingPanel>
        </Styles>
        <StylesEditors ButtonEditCellSpacing="0">
            <ProgressBar Height="21px">
            </ProgressBar>
        </StylesEditors>
    </dx:ASPxGridView>
    
</dx:PanelContent>
</PanelCollection>
        </dx:ASPxCallbackPanel>
    </td>
    </tr>
    <tr>
        <td style="width:50%" valign="top" >
            <h5><span> Send recommendation by e-mail (write e-mail on each row)</span></h5>
   

            <asp:TextBox ID="TextBox1" runat="server" Rows="6" TextMode="MultiLine"  Width="100%"></asp:TextBox>

            <div style="margin-top:5px;">
    <dx:ASPxButton ID="btnSendEmails" runat="server"   AutoPostBack="false" 
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
        SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Send emails">
        <ClientSideEvents Click="function(s, e) {
	  cp2.PerformCallback();
}" />
    </dx:ASPxButton>
    </div>
    </td>
    <td style="width:50%; padding-top:50px;; padding-left:15px;" valign="top" >
        <dx:ASPxCallbackPanel ID="cp2" runat="server"  ClientInstanceName="cp2"
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
            Width="100%">
            <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
            </LoadingPanelImage>
            <LoadingPanelStyle ImageSpacing="5px">
            </LoadingPanelStyle>
            <PanelCollection>
<dx:PanelContent runat="server" SupportsDisabledAttribute="True">
    <asp:Literal ID="litEmails" runat="server"></asp:Literal>
                </dx:PanelContent>
</PanelCollection>
        </dx:ASPxCallbackPanel>
        </td>
    </tr>
    </table>

 


 

    <br />
    <b>
    <asp:Literal ID="lit_result" runat="server"></asp:Literal>
    </b>


</asp:Content>

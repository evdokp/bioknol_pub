﻿Imports System.Security.Cryptography

Public Class LoginRedirect
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim redirectURL As String = "~\Default.aspx"

        If Request.QueryString("Person_ID") > 0 Then
            Dim ta As New PersonsDataSetTableAdapters.PersonsTableAdapter
            Dim t = ta.GetDataByID(Request.QueryString("Person_ID"))
            If t.Count > 0 Then
                Dim r = t.Item(0)
                FormsAuthentication.SetAuthCookie(r.UserName, False)
                If String.IsNullOrEmpty(Request.QueryString("finreg")) Then
                    redirectURL = "~\Account\ViewPerson.aspx?PersonID=" & r.Person_ID
                Else
                    redirectURL = String.Format("~\Account\Profile\EditProfile.aspx?PersonID={0}&finreg=1", r.Person_ID)
                End If
            End If
        End If



        ' this check should be implemented 
        ' but this requires that finishreg redirect would incl

        'If Request.QueryString("Code").ToString.Length > 0 Then
        '    If IsCodeValid(Request.QueryString("Person_ID"), Request.QueryString("Code")) = True Then


        Response.Redirect(redirectURL, False)
    End Sub

    Protected Function IsCodeValid(ByVal vPersonID As Integer, ByVal vCode As String) As Boolean
        If Encr("this will never be decrypted " & vPersonID) = vCode Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Function Encr(ByVal s As String) As String
        'Convert the string to a byte array 
        Dim bytDataToHash As Byte() = _
        (New UnicodeEncoding()).GetBytes(s)
        'Compute the MD5 hash algorithm 
        Dim bytHashValue As Byte() = _
        New MD5CryptoServiceProvider().ComputeHash(bytDataToHash)
        Return BitConverter.ToString(bytHashValue)
    End Function
End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Article.Master"
    CodeBehind="ViewInfoPM.aspx.vb" Inherits="BioWS.ViewInfoPM" %>

<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSplitter" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxNavBar" TagPrefix="dx" %>
<%@ Register Src="UserControls/Widget.ascx" TagName="Widget" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {



            $("#h3pminfo").click(function () {
                $("#divPminfo").toggle(300);
                var img = $($('#h3pminfo img'));
                ToggleImage(img);
            });

            $("#h3downloads").click(function () {
                $("#divDownloads").toggle(300);
                var img = $($('#h3downloads img'));
                ToggleImage(img);
            });

            $("#h3viewshist").click(function () {
                $("#divViewsHist").toggle(300);
                var img = $($('#h3viewshist img'));
                ToggleImage(img);
            });


            // article history loading
            $("#divViewsHistLoad").html("");
            ShowArtHist();
            $("#btnArtHistShowMore").click(function () {
                ShowArtHist()
            });



            //handle article click
            $('#divArtHistLoad .artview').live('click', function () {
                $('#hfSelectedDate').val($(this).children('input').val());
                var dt = new Date(parseInt($(this).children('input').val().slice(6, 19)));
              
                $("#hfLastIndexAfter").val(0);
                $("#hfLastIndexBefore").val(0);
                $('#divArtHistUPDOWN').html('');
                $('#divArtHistUPDOWN').html('<div class="artview" id="divThisArticle"><b>This article view at ' + dt.format("dd.MM.yyyy HH:mm:ss") + '</b></div>');
                LoadAfter();
                LoadBefore();
            });

            $("#btnShowMoreAfter").hide();
            $("#btnShowMoreBefore").hide();
            LoadingPanel.Hide();
            LoadingPanelBeforeAfter.Hide();

            $("#btnShowMoreAfter").click(function () {
                LoadAfter();
            });
            $("#btnShowMoreBefore").click(function () {
                LoadBefore();
            });
        });


        function ToggleImage(img) {
            if (img.attr('src') == 'Images/minus.gif') {
                img.attr('src', 'Images/plus.gif');
            } else {
                img.attr('src', 'Images/minus.gif');
            }
        }

        function ShowArtHist() {
       
            $('#btnArtHistShowMore').attr('disabled', 'disabled');
            LoadingPanel.Show();


         

            PageMethods.GetArtHist(parseInt($("#hfPubmed").val()), $("#hfPageOpenDateTime").val(), $("#hfLastIndexArtHist").val(), function (response) {
          
                $('#btnArtHistShowMore').removeAttr('disabled');
                LoadingPanel.Hide();
                var obj = jQuery.parseJSON(response);

                $("#hfLastIndexArtHist").val(parseInt($("#hfLastIndexArtHist").val()) + obj.length);

                

                $("#tmplArtHist").tmpl(obj).appendTo("#divArtHistLoad");

                if (obj.length < 10) {
                    $("#btnArtHistShowMore").hide();
                }

            });
        };

        function LoadAfter() {
            $('#btnShowMoreAfter').attr('disabled', 'disabled');
            LoadingPanelBeforeAfter.Show();
            var dt = new Date(parseInt($('#hfSelectedDate').val().slice(6, 19)));
         
            PageMethods.GetAfter(dt, $("#hfPageOpenDateTime").val(), $("#hfLastIndexAfter").val(), function (response) {
                $('#btnShowMoreAfter').removeAttr('disabled');
                LoadingPanelBeforeAfter.Hide();
                var obj = jQuery.parseJSON(response);
                $("#hfLastIndexAfter").val(parseInt($("#hfLastIndexAfter").val()) + obj.length);
                $("#tmplArtHistUP").tmpl(obj).prependTo("#divArtHistUPDOWN");
                if (obj.length < 10) {
                    $("#btnShowMoreAfter").hide();
                } else {
                    $("#btnShowMoreAfter").show();
                    $('#divArtHistUPDOWN').prepend('<img src="Images/bookmark.png" style="float:right;"/>');
                }
            });
        };

        function LoadBefore() {

            $('#btnShowMoreBefore').attr('disabled', 'disabled');
            LoadingPanelBeforeAfter.Show();
            var dt = new Date(parseInt($('#hfSelectedDate').val().slice(6, 19)));
            PageMethods.GetBefore(dt, $("#hfPageOpenDateTime").val(), $("#hfLastIndexBefore").val(), function (response) {
                $('#btnShowMoreBefore').removeAttr('disabled');
                LoadingPanelBeforeAfter.Hide();
                var obj = jQuery.parseJSON(response);
                $("#hfLastIndexBefore").val(parseInt($("#hfLastIndexBefore").val()) + obj.length);
                $("#tmplArtHistDown").tmpl(obj).appendTo("#divArtHistUPDOWN");
                if (obj.length < 10) {
                    $("#btnShowMoreBefore").hide();
                } else {
                    $("#btnShowMoreBefore").show();
                    $('#divArtHistUPDOWN').append('<img src="Images/bookmark.png" style="float:right;"/>');
                }
            });
        };

    </script>
    <asp:HiddenField ID="hfLastIndexArtHist" runat="server" ClientIDMode="Static" Value="0" />
    <asp:HiddenField ID="hfLastIndexAfter" runat="server" ClientIDMode="Static" Value="0" />
    <asp:HiddenField ID="hfLastIndexBefore" runat="server" ClientIDMode="Static" Value="0" />
    <asp:HiddenField ID="hfSelectedDate" runat="server" ClientIDMode="Static" Value="0" />
    <asp:HiddenField ID="hfPageOpenDateTime" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfPubmed" runat="server" ClientIDMode="Static" />
    <h3 class="hunderline" id="h3pminfo" style="margin-top: 2px; padding-top: 5px;">
        <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/minus.gif" />
        Pubmed info
    </h3>
    <div id="divPminfo" style="padding-left: 10px;">
        <h4>
            Abstract
        </h4>
        <asp:Literal ID="lit_abstract" runat="server"></asp:Literal>
        <h4>
            Journal
        </h4>
        <b>
            <asp:Literal ID="lit_journal" runat="server"></asp:Literal></b>
        <h4>
            Authors
        </h4>
        <b>
            <asp:Literal ID="lit_authores" runat="server"></asp:Literal></b>
        <h4>
            MeSH Headings
        </h4>
        <asp:Literal ID="lit_mesh" runat="server"></asp:Literal>
    </div>
    <asp:Panel ID="pnlDownloads" runat="server">
        <h3 class="hunderline" id="h3downloads">
            <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/minus.gif" />
            Downloads
        </h3>
        <div id="divDownloads" style="padding-left: 10px;">
            <dx:ASPxGridView ID="gvDownloads" runat="server" AutoGenerateColumns="False" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                CssPostfix="DevEx">
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex="0" Width="16px">
                        <DataItemTemplate>
                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("Image") %>' />
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="File name">
                        <DataItemTemplate>
                            <asp:Literal ID="litFile" runat="server" Text='<%# Eval("FileName") %>' />
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="2">
                        <DataItemTemplate>
                            <asp:Button ID="Button1" runat="server" Text="Download" BackColor="#336600" Font-Size="10px"
                                ForeColor="White" Width="90px" PostBackUrl='<%# Eval("DownloadLink") %>' />
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                </Columns>
                <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                    </LoadingPanel>
                </Images>
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <LoadingPanel ImageSpacing="5px">
                    </LoadingPanel>
                </Styles>
                <StylesEditors ButtonEditCellSpacing="0">
                    <ProgressBar Height="21px">
                    </ProgressBar>
                </StylesEditors>
            </dx:ASPxGridView>
        </div>
    </asp:Panel>
    <h3 class="hunderline" id="h3viewshist">
        <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/minus.gif" />
        Views history
    </h3>
    <div id="divViewsHist" style="padding-left: 10px;">
        <table width="100%">
            <tr>
                <td valign="top" style="width: 40%;">
                    <div id="divArtHistLoad">
                    </div>
                    <script id="tmplArtHist" type="text/x-jquery-tmpl">

         <div style="" class="artview">
         ${vDateTime}&nbsp;|&nbsp;${vEgo}
         <input type="hidden"  value="${vTimestamp}"/>
         </div>

         
        {{if (isFirst == 1)}}
        <div style="font-family: verdana;font-size: 7pt;color: #FF6600; font-weight:bold;">
            <img src="Images/asterisk.png"/> First view
         </div>
        {{else}} 
        <div class="feed_timeup">
                 ${vInterval}
         </div>
        {{/if}} 
     
                    </script>
                    <dx:ASPxLoadingPanel ID="ASPxLoadingPanel1" runat="server" ClientInstanceName="LoadingPanel"
                        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" ImageSpacing="5px"
                        ContainerElementID="divArtHistLoad" Modal="True">
                        <Image Url="~/App_Themes/DevEx/Web/Loading.gif">
                        </Image>
                    </dx:ASPxLoadingPanel>
                    <dx:ASPxButton ID="btnArtHistShowMore" runat="server" AutoPostBack="false" ClientIDMode="Static"
                        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Height="29px"
                        SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Show more" Width="100%">
                    </dx:ASPxButton>
                </td>
                <td valign="top" style="width: 60%; padding-left: 20px;">
                    <dx:ASPxLoadingPanel ID="ASPxLoadingPanel2" runat="server" ClientInstanceName="LoadingPanelBeforeAfter"
                        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" ImageSpacing="5px"
                        ContainerElementID="divArtHistUPDOWN" Modal="True">
                        <Image Url="~/App_Themes/DevEx/Web/Loading.gif">
                        </Image>
                    </dx:ASPxLoadingPanel>
                    <dx:ASPxButton ID="btnShowMoreAfter" runat="server" AutoPostBack="false" ClientIDMode="Static" 
                        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Height="29px"
                        SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Show more" Width="100%">
                    </dx:ASPxButton>
                    <div id="divArtHistUPDOWN" style="margin-top:3px;margin-bottom:3px;">
                        <div class="artview" id="divThisArticle">This article<br />
                            <i>Click view time on the left to see articles before and after </i>
                        </div>
                    </div>
                          <dx:ASPxButton ID="btnShowMoreBefore" runat="server" AutoPostBack="false" ClientIDMode="Static"  
                        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Height="29px"
                        SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Show more" Width="100%">
                    </dx:ASPxButton>
                    <script id="tmplArtHistUP" type="text/x-jquery-tmpl">

                             <div style="" class="artview">
                             <a href="ViewInfoPM.aspx?Pubmed_ID=${vID}" target="_blank">${vTitle}</a>
                             <br/>
                             ${vDateTime}&nbsp;|&nbsp;${vEgo}
                             <input type="hidden"  value="${vTimestamp}"/>
                             </div>

         
                            {{if (isFirst == 1)}}
                            <div style="font-family: verdana;font-size: 7pt;color: #FF6600; font-weight:bold;">
                                <img src="Images/asterisk.png"/> First view
                             </div>
                            {{else}} 
                            <div class="feed_timeup">
                                     ${vInterval}
                             </div>
                            {{/if}} 
     
                    </script>
                      <script id="tmplArtHistDown" type="text/x-jquery-tmpl">

                          
                            <div class="feed_timedown">
                                     ${vInterval}
                             </div>
                             <div class="clear"></div>
                             <div style="" class="artview">
                             <a href="ViewInfoPM.aspx?Pubmed_ID=${vID}" target="_blank">${vTitle}</a>
                             <br/>
                             ${vDateTime}&nbsp;|&nbsp;${vEgo}
                             <input type="hidden"  value="${vTimestamp}"/>
                             </div>

                    </script>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

﻿Imports DevExpress.Web
Imports System.Drawing
Public Class Widget
    Inherits MasterPage


    Private Sub SetMI(menu As DevExpress.Web.ASPxMenu.ASPxMenu, itemname As String, url As String, text As String)
        Dim item As DevExpress.Web.ASPxMenu.MenuItem = menu.Items.FindByName(itemname)
        If url <> String.Empty Then
            item.NavigateUrl = String.Format("{0}?WidgetID={1}", url, Request.QueryString("WidgetID"))
        End If
        If text <> String.Empty Then
            item.Text = text
        End If
        item.Visible = True
    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load


        If Page.IsPostBack = False Then

            If String.IsNullOrEmpty(Request.QueryString("WidgetID")) Then

                menuProfile.Visible = False

            Else

                Dim _profile As New ProfileCommon
                _profile = _profile.GetProfile(HttpContext.Current.User.Identity.Name)

                Dim widget As New BLL.Widgets.Widget(Request.QueryString("WidgetID"))
                If widget.AddedByPersonID = _profile.Person_ID Then
                    SetMI(menuProfile, "Edit", "~\Account\Widget\AddEditWidget.aspx", String.Empty)
                Else
                    menuProfile.Items.FindByName("Edit").Visible = False
                End If

                If widget.IsOnForPerson(_profile.Person_ID) Then
                    SetMI(menuProfile, "TurnOnOff", String.Empty, "Turn off")
                Else
                    SetMI(menuProfile, "TurnOnOff", String.Empty, "Turn on")
                End If

                '! profile menu items
                SetMI(menuProfile, "Overview", "~\Account\Widget\ViewWidget.aspx", String.Empty)
                SetMI(menuProfile, "Statistics", "~\Account\Widget\ViewWidgetStatistics.aspx", String.Empty)

            End If

            


        End If


    End Sub


    Protected Sub menuProfile_ItemClick(source As Object, e As DevExpress.Web.ASPxMenu.MenuItemEventArgs) Handles menuProfile.ItemClick
        Dim widget As New BLL.Widgets.Widget(Request.QueryString("WidgetID"))
        Dim _profile As New ProfileCommon
        _profile = _profile.GetProfile(HttpContext.Current.User.Identity.Name)
        If widget.ToggleOnOff(_profile.Person_ID) Then
            SetMI(menuProfile, "TurnOnOff", String.Empty, "Turn off")
        Else
            SetMI(menuProfile, "TurnOnOff", String.Empty, "Turn on")
        End If

    End Sub
End Class
﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://bioknowledgecenter.ru/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class LogError_WS
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function LogError_WS_func(ByVal vError As String, ByVal vPluginID As Integer, ByVal vPersonID As Integer) As LogError_WS_resp
        Dim resp As New LogError_WS_resp
        Dim ta As New ErrorsLogDataSetTableAdapters.ErrorsLogTableAdapter
        Try
            ta.Insert(Now(), vPersonID, vError, String.Empty, vPluginID)
            resp.vStatus = "success"
        Catch ex As Exception
            resp.vStatus = "failure"
        End Try
        Return resp
    End Function
    Public Structure LogError_WS_resp
        Dim vStatus As String
    End Structure

End Class
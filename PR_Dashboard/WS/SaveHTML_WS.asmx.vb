﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.IO
Imports AzureStorageLibrary
Imports BioWS.BLL.Extensions


' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://bioknowledgecenter.ru/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class SaveHTML_WS
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function SaveHTML_WS_func(ByVal vHTML As String, ByVal vTitle As String, ByVal vURL As String, ByVal vPluginID As Integer, ByVal vPersonID As Integer) As SaveHTML_WS_resp
        Dim resp As New SaveHTML_WS_resp
        Dim htmlDecoded = HttpUtility.UrlDecode(vHTML)
        vURL = HttpUtility.UrlDecode(vURL)

        Try
            If vHTML.IsNotEmpty() Then
                Dim parsingAttempt As New BLL.ParsingAttempt(vURL, htmlDecoded, BLL.ParsingAction.Origins.SaveHTMLWS)
                Dim parsingResult As BLL.ParsingResult = parsingAttempt.ParsingResult



                If parsingAttempt.LogViewRequired Then
                    Dim fileGUID As String
                    If parsingResult.SaveRequired Then
                        Try
                            fileGUID = Guid.NewGuid().ToString()
                            Dim fullFileName As String = String.Format("{0}.html", fileGUID)
                            Dim storage As New AzureStorage()
                            storage.UploadHtml(htmlDecoded, fullFileName)
                            'fileGUID = BLL.Common.SaveHTML(htmlDecoded)
                        Catch ex As Exception
                            fileGUID = String.Empty
                        End Try
                    End If

                    BLL.ParsingHelper.Log(vURL,
                        vPersonID,
                        BLL.Common.GetIP(),
                        vTitle,
                        vPluginID,
                        parsingResult.PubmedID,
                        parsingResult.DOI,
                        parsingAttempt.DomainMeta.ID,
                        fileGUID)

                End If

                BLL.PubMed.ProcessPMIDinThread(parsingResult.PubmedID)

                If parsingResult.HasPubmed Then
                    If BLL.Widgets.IsAdditionalInfoAvailableExtracted(vPersonID, parsingResult.PubmedID) > 0 Then
                        resp.vStatus = "info"
                    Else
                        resp.vStatus = "success"
                    End If
                Else
                    resp.vStatus = "success"
                End If
            End If

        Catch ex As Exception
            resp.vStatus = "failure"
        End Try

        Return resp
    End Function
    Public Structure SaveHTML_WS_resp
        Dim vStatus As String
    End Structure
    'Protected Delegate Sub ProcessSaveHTML_Delegate(ByVal vHTML As String, ByVal vTitle As String, ByVal vURL As String, ByVal vPluginID As Integer, ByVal vPersonID As Integer, ByVal vIP As String)
    'Protected Sub ProcessSaveHTML(ByVal vHTML As String, ByVal vTitle As String, ByVal vURL As String, ByVal vPluginID As Integer, ByVal vPersonID As Integer, ByVal vIP As String)
    '    'clear strings
    '    vURL = HttpUtility.UrlDecode(vURL)
    '    vHTML = HttpUtility.UrlDecode(vHTML)
    '    'get ID of monitored site
    '    Dim i As Integer = 0
    '    'i = BLL.Common.GetSiteID(vURL)
    '    'determine if we write file
    '    Dim DoWrite As Boolean
    '    DoWrite = DetermineDoWrite(i, vURL)
    '    'get PubmedID if possible
    '    Dim vPMID As Integer
    '    vPMID = ProcessSiteSpecificData(i, vURL)
    '    'get DOI if possible 
    '    Dim vDOI As String
    '    vDOI = BLL.DOI.GetDOIFromHTML(vHTML)
    '    'write file and insert record
    '    If DoWrite = True Then
    '        Dim FileName As String
    '        FileName = WriteFile(vHTML)
    '        Dim ta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
    '        If vPMID = 0 Then
    '            ta.Insert(Now(), vURL, FileName, vPersonID, vIP, vTitle, i, New Integer?, vPluginID, vDOI)
    '        Else
    '            ta.Insert(Now(), vURL, FileName, vPersonID, vIP, vTitle, i, vPMID, vPluginID, vDOI)
    '        End If
    '    End If
    'End Sub
    'Private Function DetermineDoWrite(ByVal i As Integer, ByVal vURL As String) As Boolean
    '    'Dim DoWrite As Boolean
    '    'If BLL.Common.GetSiteID(vURL) = 0 Then
    '    '    DoWrite = False
    '    '    'it is not monitored - somewhy we got into this procedure
    '    'Else
    '    '    DoWrite = True
    '    '    'check if url is under exception string
    '    '    Dim es_ta As New MonitoredSitesDataSetTableAdapters.ExceptionStringsTableAdapter
    '    '    Dim es_t = es_ta.GetDataByMonitoredSite_ID(i)
    '    '    Dim k As Integer
    '    '    For k = 0 To es_t.Count - 1
    '    '        If vURL.Contains(es_t.Item(k).ExceptionString) Then
    '    '            DoWrite = False
    '    '        End If
    '    '    Next
    '    'End If
    '    'Return DoWrite
    'End Function
    'Private Function WriteFile(ByRef vHTML As String) As String
    '    Dim FileName As Guid
    '    Dim FileFolderFullName As String
    '    FileName = Guid.NewGuid()
    '    FileFolderFullName = FileName.ToString & ".html"
    '    File.WriteAllText("c:\websites\BioWS\SavedHTML\" + FileFolderFullName, vHTML)
    '    Return FileName.ToString
    'End Function
    'Private Function ProcessSiteSpecificData(ByRef vSiteID As Integer, ByRef vURL As String) As Integer
    '    'now it will return pubmed ID 
    '    'when we will process different sites - perhaps we will return smth else
    '    Dim vPMID As Integer = 0
    '    Select Case vSiteID
    '        Case 2144
    '            vPMID = BLL.PubMed.GetPubMedIDfromURL(vURL)
    '            BLL.PubMed.ProcessPubmedView(vPMID)
    '        Case Else
    '            'nothing
    '    End Select
    '    Return vPMID
    'End Function
End Class
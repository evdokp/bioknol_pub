﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://bioknowledgecenter.ru/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<SoapDocumentService(RoutingStyle:=SoapServiceRoutingStyle.RequestElement)> _
<ToolboxItem(False)> _
Public Class GetPluginID_WS
    Inherits System.Web.Services.WebService
    <WebMethod()> _
    Public Function GetPluginID_WS_func(ByVal vUniqueIdentifier As String) As GetPluginID_WS_resp
        Dim resp As New GetPluginID_WS_resp
        Dim ta As New PluginsDataSetTableAdapters.PluginsTableAdapter
        Dim t = ta.GetDataByUniqueIdentifier(vUniqueIdentifier)
        If t.Count > 0 Then
            resp.vPluginID = t.Item(0).Plugin_ID
        Else
            ta.InsertPlugin(vUniqueIdentifier, resp.vPluginID)
        End If
        Return resp
    End Function
    Public Structure GetPluginID_WS_resp
        Dim vPluginID As Integer
    End Structure

End Class
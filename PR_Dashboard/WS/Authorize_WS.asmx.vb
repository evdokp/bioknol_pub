﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://bioknowledgecenter.ru/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<SoapDocumentService(RoutingStyle:=SoapServiceRoutingStyle.RequestElement)> _
<ToolboxItem(False)> _
Public Class Authorize_WS
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function Authorize_WS_func(ByVal vUserName As String, ByVal vPassword As String, ByVal LoginOrRegister As Integer) As Authorize_WS_resp
        Dim resp As New Authorize_WS_resp
        'Dim er_ws As New wref_LogError_WS.LogError_WS
        'Dim s As String
        'Try
        '    s = "NOW: " & Now.ToLongTimeString & " USERNAME: " & vUserName & " PASSWORD: " & vPassword & "LoginOrReg: " & LoginOrRegister
        'Catch ex As Exception
        ''    s = "NOW: " & Now.ToLongTimeString & "ERROR: " & ex.Message
        ''End Try

        'er_ws.LogError_WS_func(s, 1000, 1000)

        Select Case LoginOrRegister
            Case 1 'login
                Dim mu As MembershipUser
                mu = Membership.GetUser(vUserName)
                If mu Is Nothing Then
                    resp.vStatus = 3 ' no such e-mail
                    resp.vPersonID = 0
                Else
                    If vPassword = mu.GetPassword Then
                        Dim p As New ProfileCommon
                        p = p.GetProfile(vUserName)
                        resp.vStatus = 1 ' successful login
                        resp.vPersonID = p.Person_ID

                        '! try - forced relogin
                        If User.Identity.IsAuthenticated Then
                            If Not User.Identity.Name = vUserName Then
                                ' forced relogin
                                FormsAuthentication.SignOut()
                                FormsAuthentication.SetAuthCookie(vUserName, False)
                            End If
                        Else
                            FormsAuthentication.SetAuthCookie(vUserName, False)
                        End If
                        '!===================================

                    Else
                        resp.vStatus = 2 ' wrong password
                        resp.vPersonID = 0
                    End If
                End If
            Case 2 'registration
                'create new user
                Dim mu As MembershipUser = Membership.GetUser(vUserName)
                If mu Is Nothing Then
                    'no such user - we can create one
                    Dim i As Integer
                    i = RegisterNewUser(vUserName, vPassword)
                    If i > 0 Then
                        resp.vStatus = 1
                        resp.vPersonID = i
                    Else
                        resp.vStatus = 5
                        resp.vPersonID = 0
                        '  er_ws.LogError_WS_func(vUserName & vPassword & "---" & i, 1000, 1000)
                    End If
                Else
                    resp.vStatus = 4 ' user with this email already exists
                    resp.vPersonID = 0
                End If
            Case Else
                resp.vStatus = 5
                resp.vPersonID = 0
                ' er_ws.LogError_WS_func("PAR:" & vUserName & vPassword, 1000, 1000)
        End Select
        'er_ws.LogError_WS_func("RESPONSE: " & resp.vStatus, 1000, 1000)

        Return resp
    End Function
    Public Function RegisterNewUser(ByVal vUserName As String, ByVal vPassword As String) As Integer
        Dim new_u As MembershipUser
        Dim st As MembershipCreateStatus
        Dim newGUID As Guid = Guid.NewGuid
        Dim er_ws As New wref_LogError_WS.LogError_WS
        Membership.CreateUser(vUserName, vPassword, vUserName, Nothing, Nothing, True, newGUID, st)
        Select Case st
            Case MembershipCreateStatus.DuplicateEmail
                er_ws.LogError_WS_func("Reg---duplemail", 1000, 1000)
                Return -1
                Exit Function
            Case MembershipCreateStatus.DuplicateUserName
                er_ws.LogError_WS_func("Reg---dupleusername", 1000, 1000)
                Return -1
                Exit Function
            Case MembershipCreateStatus.ProviderError
                er_ws.LogError_WS_func("Reg---providererr", 1000, 1000)
                Return -1
                Exit Function
            Case MembershipCreateStatus.Success
                'success
            Case Else
                er_ws.LogError_WS_func("Reg---unknown", 1000, 1000)
                Return -1
                Exit Function
        End Select
        new_u = Membership.GetUser(vUserName)
        'set profile properties to new user
        Dim _p As New BLL.Person(vUserName, String.Empty, String.Empty, String.Empty)

        Dim userProfile As New ProfileCommon
        userProfile = userProfile.GetProfile(vUserName)
        userProfile.Person_ID = _p.Person_ID
        userProfile.DefaultPeriodDays = 90
        userProfile.Save()


        'send mail
        Dim vBody As String = BLL.MailSender.ReadFile.ReadFile(HttpContext.Current.Server, "Registration.htm")
        vBody = vBody.Replace("<%UserName%>", vUserName)
        vBody = vBody.Replace("<%Password%>", vPassword)
        Try
            BLL.MailSender.SendMailMessage("noreply@bioknowledgecenter.ru",
            "PRD mail service",
            vUserName,
            String.Empty,
            String.Empty,
            "Personal Reference Dashboard registration",
            vBody)
        Catch ex As Exception
            ' nothing
        End Try

        Return _p.Person_ID
    End Function


    Public Structure Authorize_WS_resp
        Dim vStatus As Integer
        Dim vPersonID As Integer
    End Structure
End Class



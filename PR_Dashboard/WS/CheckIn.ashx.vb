﻿Imports System.Web
Imports System.Web.Services
Imports System.Drawing
Imports BioWS.BLL.Extensions


Public Class CheckIn
    Implements IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim isValid As Boolean
        Dim myinfostr As String = context.Request.QueryString("myinfo")
        Dim myinfosa As String() = myinfostr.Split(";")
        isValid = myinfosa.Length = 4

        Dim vpluginid, vuserid As Integer
        Dim vpluginver, vpluginbrows As String

        If isValid Then
            isValid = Integer.TryParse(myinfosa(0), vpluginid)
            isValid = Integer.TryParse(myinfosa(1), vuserid)
        End If
        
        If isValid Then
            vpluginver = myinfosa(2)
            vpluginbrows = myinfosa(3)
            Dim logta As New PluginsDataSetTableAdapters.PluginsLogTableAdapter
            logta.Insert(Now, vpluginid, vuserid, vpluginver, vpluginbrows)
        End If

        context.Response.WritePlainText(String.Empty)

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
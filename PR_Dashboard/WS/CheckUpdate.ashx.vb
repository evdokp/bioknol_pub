﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions


Public Class CheckUpdate
    Implements System.Web.IHttpHandler

    Private Function GetVersion(ByVal input As String) As Integer
        input = input.Replace(".", "")
        input = input.Replace("U", "")
        Return input.ToInt32()
    End Function

    Private Const CurrentVersion As Integer = 2390

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim versionString As String = context.Request.QueryString("ver")
        Dim response As String
        Dim isCurrent As Boolean = GetVersion(versionString) = CurrentVersion
        response = IIf(isCurrent, "NOUPDATE", "http://ws.bioknowledgecenter.ru/Update/prdashboard2390U.dll" & vbCrLf & "8fa8da060b8d21ec4c9cadb725d09f60")
        context.Response.WritePlainText(response)
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property


End Class
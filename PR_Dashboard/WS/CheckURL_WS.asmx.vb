﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Drawing
Imports System.Threading
Imports BioWS.BLL.Extensions
Imports BioWS.ArticlesDataSetTableAdapters
Imports BioWS.BLL

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://bioknowledgecenter.ru/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class CheckURL_WS
    Inherits System.Web.Services.WebService


    Private _RequestsTA As ArticleRequestsTableAdapter
    Private ReadOnly Property RequestsTA As ArticleRequestsTableAdapter
        Get
            If _RequestsTA Is Nothing Then
                _RequestsTA = New ArticleRequestsTableAdapter()
            End If
            Return _RequestsTA
        End Get
    End Property
    Private _OffersTA As ArticleOffersTableAdapter
    Private ReadOnly Property OffersTA As ArticleOffersTableAdapter
        Get
            If _OffersTA Is Nothing Then
                _OffersTA = New ArticleOffersTableAdapter()
            End If
            Return _OffersTA
        End Get
    End Property

    Public Property urlDecoded As String
    Public Property PersonID As Integer

    Private ReadOnly Property UploadsCount As Integer
        Get
            Return OffersTA.GetCountByURLwithFile(urlDecoded)
        End Get
    End Property
    Private ReadOnly Property OtherUsersRequests As Integer
        Get
            Return RequestsTA.GetCountByNOTPersonIDandURL(PersonID, urlDecoded)
        End Get
    End Property
    Private _IHaveArticle As Integer?
    Private ReadOnly Property IHaveArticle As Integer
        Get
            If _IHaveArticle Is Nothing Then
                _IHaveArticle = OffersTA.GetCountByPersonIDandURL(PersonID, urlDecoded).Value.ToIntBoolean()
            End If
            Return _IHaveArticle
        End Get
    End Property
    Private ReadOnly Property IRequestedArticle() As Integer
        Get
            Return RequestsTA.GetCountByPersonIDandURL(PersonID, urlDecoded).Value.ToIntBoolean()
        End Get
    End Property
    <WebMethod()> _
    Public Function CheckURL_WS_func(ByVal vURL As String, ByVal vPluginID As Integer, ByVal vPersonID As Integer) As CheckURL_WS_resp
        urlDecoded = vURL.UrlDecode()
        PersonID = vPersonID

        Dim resp As New CheckURL_WS_resp

        '! NON PUBMED PART OF RESPONSE
        resp.vVisitsNumber = 0
        resp.vMessagesNumber = GetMessagesNumber(PersonID)
        resp.vI_RequestArticle = IRequestedArticle
        resp.vI_HaveArticle = IHaveArticle
        resp.vArticleStatus = GetArtStatus(IHaveArticle, UploadsCount, OtherUsersRequests)

        '! PUBMED-RELATED PART OF RESPONSE
        Dim parsingAttempt As New ParsingAttempt(urlDecoded, String.Empty, ParsingAction.Origins.CheckURLWS)
        Dim parsingResult = parsingAttempt.ParsingResult

        If parsingAttempt.DomainMeta.Determined Then
            resp.vSaveHTML = parsingResult.HTMLRequired.ToInt()
            resp.vAdditionalInfo = IIf(parsingResult.HasPubmed, Widgets.IsAdditionalInfoAvailableExtracted(vPersonID, parsingResult.PubmedID), 0)

            If parsingAttempt.LogViewRequired Then
                ParsingHelper.Log(urlDecoded,
                    vPersonID,
                    BLL.Common.GetIP(),
                    String.Empty,
                    vPluginID,
                    parsingResult.PubmedID,
                    parsingResult.DOI,
                    parsingAttempt.DomainMeta.ID,
                    Nothing)
            End If

            PubMed.ProcessPMIDinThread(parsingResult.PubmedID)

        Else
            '! could not find domain - strange!
            '! log for further analisys
        End If
        '! TODO:
        '! - Article requests and offers - try to get by PMID if we succeeded determining it
        Return resp
    End Function




    Private Shared Function GetMessagesNumber(ByVal vPersonID As Integer) As Integer
        Dim i As Integer
        If vPersonID = 0 Then
            i = 0
        Else
            Dim posts_ta As New DataSetMessagesTableAdapters.MessagesTableAdapter
            i = posts_ta.GetCountNewMessagesByPersonToID(vPersonID)
        End If
        Return i
    End Function

    Private Shared Function GetArtStatus(ByVal intIHaveArticle As Integer, ByVal uploadsCount As Integer, ByVal otherUsersRequests As Integer) As ArticleStatuses
        Dim artSt As ArticleStatuses

        If 1 = 1 Then '! now we consider everything an article
            If uploadsCount > 0 Or intIHaveArticle = 1 Then
                '! if there was any upload for the URL, or user himself claimed he has the article
                artSt = ArticleStatuses.Available
            Else
                If otherUsersRequests > 0 Then
                    artSt = ArticleStatuses.WasRequestedBySomeOne
                Else
                    artSt = ArticleStatuses.Article
                End If
            End If
        Else
            'TODO Not article regexes for each domain
            artSt = ArticleStatuses.NotArticle
        End If
        Return artSt
    End Function


    Public Structure CheckURL_WS_resp
        Dim vVisitsNumber As Integer 'redundant
        Dim vAdditionalInfo As Integer
        Dim vMessagesNumber As Integer
        Dim vArticleStatus As Integer
        Dim vI_RequestArticle As Integer
        Dim vI_HaveArticle As Integer
        Dim vSaveHTML As Integer
    End Structure

    Public Enum ArticleStatuses
        NotArticle = 0
        Article = 1
        Available = 2
        WasRequestedBySomeOne = 3
    End Enum
End Class
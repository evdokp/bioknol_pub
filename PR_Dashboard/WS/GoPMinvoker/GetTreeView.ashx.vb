﻿Imports System.Web
Imports System.Web.Services
Imports System.Xml
Imports System.IO

Public Class GetTreeView
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/xml"
        Dim x_wr As New XmlTextWriter(context.Response.OutputStream, Encoding.Unicode)

        x_wr.WriteStartDocument()
        x_wr.WriteStartElement("root")
        StartTopLevel(x_wr)
        x_wr.WriteEndElement()
        x_wr.WriteEndDocument()

        x_wr.Flush()
        x_wr.Close()

    End Sub

    Private Sub StartTopLevel(ByRef x_wr As XmlTextWriter)
        Dim p_ta As New PersonsDataSetTableAdapters.LinksGroupsToPersonsTableAdapter
        Dim ta As New PersonsDataSetTableAdapters.GroupsTableAdapter
        Dim t = ta.GetTopLevel()
        Dim r As PersonsDataSet.GroupsRow
        Dim i As Integer
        For i = 0 To t.Count - 1
            r = t.Item(i)
            WriteNode(r.Title, "g", r.Group_ID, x_wr)
        Next
    End Sub

    Private Sub WriteNode(ByVal vTitle As String, ByVal vType As String, ByVal vID As Integer, ByVal x_wr As XmlTextWriter)
        x_wr.WriteStartElement("group")
        x_wr.WriteAttributeString("title", vTitle)
        x_wr.WriteAttributeString("type", vType)
        x_wr.WriteAttributeString("id", vID)
        Select Case vType
            Case "g"
                Dim p_ta As New PersonsDataSetTableAdapters.LinksGroupsToPersonsTableAdapter
                Dim ta As New PersonsDataSetTableAdapters.GroupsTableAdapter
                Dim t = ta.GetChildren(vID)
                Dim r As PersonsDataSet.GroupsRow
                Dim i As Integer
                For i = 0 To t.Count - 1
                    r = t.Item(i)
                    WriteNode(r.Title, "g", r.Group_ID, x_wr)
                Next
                Dim p_t = p_ta.GetDataByGroup_ID(vID)
                Dim p_r As PersonsDataSet.LinksGroupsToPersonsRow
                For i = 0 To p_t.Count - 1
                    p_r = p_t.Item(i)
                    Dim pers As New BLL.Person(p_r.Person_ID)
                    WriteNode(pers.DisplayName & " (" & pers.UserName & ")", "p", pers.Person_ID, x_wr)
                Next
            Case "p"
                'no child - ok
        End Select
        x_wr.WriteEndElement()
    End Sub







    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
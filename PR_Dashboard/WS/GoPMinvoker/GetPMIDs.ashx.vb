﻿Imports System.Web
Imports System.Web.Services

Public Class GetPMIDs
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        '?Person_ID=0&Group_ID=1&SDate=01.04.2010&EDate=01.04.2010

        Dim Group_ID As Integer = context.Request.QueryString("Group_ID")
        Dim Person_ID As Integer = context.Request.QueryString("Person_ID")
        Dim SDate As Date = context.Request.QueryString("SDate")
        Dim EDate As Date = context.Request.QueryString("EDate")

        Dim ta As New ChartDataSetTableAdapters.DataTable1TableAdapter
        Dim t As ChartDataSet.DataTable1DataTable


        If Group_ID = 0 Then
            'get persons IDs
            t = ta.GetPMIDsByPersonIDandTwoDates(Person_ID, SDate, EDate)
        Else
            'get groups IDs
            t = ta.GetPMIDsByGroupIDandTwoDates(Group_ID, SDate, EDate)
        End If
        Dim i As Integer
        Dim s As String

        For i = 0 To t.Count - 1
            s = s & t.Item(i).vCount
            If i <> t.Count - 1 Then
                s = s & ", "
            End If
        Next

        context.Response.ContentType = "text/plain"
        context.Response.Write(s)

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
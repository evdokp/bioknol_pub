﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Threading
Imports System.Drawing
Imports BioWS.BLL.Extensions
Imports BioWS.BLL.Extensions.Web

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://bioknowledgecenter.ru/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class RequestArticle_WS
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function RequestArticle_WS_func(ByVal vURL As String, ByVal vPluginID As Integer, ByVal vPersonID As Integer) As RequestArticle_WS_resp
        Dim resp As New RequestArticle_WS_resp
        Try
            vURL = HttpUtility.UrlDecode(vURL)
            Dim ta As New ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter
            Dim vDOI As String
            vDOI = BLL.DOI.GetDOIbyURL_internal(vURL)
            ta.Insert(Now(), vURL, New Integer?, vPluginID, vPersonID, String.Empty, vDOI, False, String.Empty)
            Dim d As ProcessRequestArticle_Delegate
            d = New ProcessRequestArticle_Delegate(AddressOf ProcessRequestArticle)
            Dim R As IAsyncResult
            R = d.BeginInvoke(vURL, vPluginID, vPersonID, Nothing, Nothing)
            resp.vStatus = "success"
        Catch ex As Exception
            resp.vStatus = "failure"
        End Try
        Return resp
    End Function
    Public Structure RequestArticle_WS_resp
        Dim vStatus As String
    End Structure
    Protected Delegate Sub ProcessRequestArticle_Delegate(ByVal vURL As String, ByVal vPluginID As Integer, ByVal vPersonID As Integer)
    Protected Sub ProcessRequestArticle(ByVal vURL As String, ByVal vPluginID As Integer, ByVal vPersonID As Integer)
        Dim vPMID As Integer = 0
        vPMID = BLL.PubMed.GetPubMedIDfromURL(vURL)
        Dim ta As New ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter



        'Dim articlerequest As BLL.Feed.FeedActionRecommendatio

        If vPMID > 0 Then
            BLL.PubMed.ProcessPubmedView(vPMID)
            ta.UpdatePubmedID(vPMID, vPersonID, vURL)
            '   articlerequest = New BLL.Feed.FeedActionRecommendation(vPMID, "", "", "")
        Else
            Dim vTitle As String
            Dim vHTML As String
            'TODO - we should check in base by URL first, if there's - try to read it from saved file
            Dim vDOI As String
            vHTML = vURL.GetHTML()
            vDOI = BLL.DOI.GetDOIFromHTML(vHTML)
            vTitle = vHTML.GetTitleTagText()

            If vDOI.Length > 2 Then
                '      articlerequest = New BLL.Feed.FeedActionRecommendation(0, vDOI, "", "")
            Else
                '     articlerequest = New BLL.Feed.FeedActionRecommendation(0, "", vURL, vTitle)
            End If

            ta.UpdateTitleDOI(vTitle, vDOI, vPersonID, vURL)
        End If

        If vPersonID > 0 Then
            'articlerequest.Save(vPersonID)
        End If




    End Sub

End Class
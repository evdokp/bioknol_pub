﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions


Public Class GetSites
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim vType As Integer = context.Request.QueryString("vRequestType")

        Dim ta As New MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter
        Dim t As MonitoredSitesDataSet.MonitoredSitesDataTable
        Select Case vType
            Case 0
                'automatic
                'shows only difference since last load from common list
                Dim vLastLoadDate As Date = context.Request.QueryString("vLastLoadDate")
                t = ta.GetNewByLastLoadDate(vLastLoadDate)                
            Case 1
                'user defined
                'shows all with consideration of user defined exclusions
                Dim vPersonID As Integer = context.Request.QueryString("vPersonID")
                t = ta.GetAllWithoutUserExclusions(vPersonID)
        End Select

        Dim lst As New List(Of String)
        Dim lst2 As New List(Of String)
        lst = (From aa In t
                            Select aa.SiteURL).ToList()


        For Each item In lst
            If IsIP(item) Then
                lst2.Add(item)
            Else
                lst2.Add(item)
                If item.StartsWith("www.") Then
                    lst2.Add(item.Replace("www.", ""))
                Else
                    lst2.Add("www." & item)
                End If
            End If

        Next

        Dim lst3 = (From itm In lst2
                                      Select itm).Distinct().ToList()



        Dim i As Integer
        Dim s As String
        For i = 0 To lst3.Count - 1
            s = s & lst3(i)
            s = s & vbCrLf
        Next

        context.Response.ContentType = "text/plain"
        context.Response.Write(s)

    End Sub

    Private Function IsIP(ByVal value As String) As Boolean
        Return Regex.IsMatch(value, "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}")
    End Function

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
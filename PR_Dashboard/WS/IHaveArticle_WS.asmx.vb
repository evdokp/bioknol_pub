﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports BioWS.BLL.Extensions
Imports BioWS.BLL.Extensions.Web


' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://bioknowledgecenter.ru/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class IHaveArticle_WS
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function IHaveArticle_WS_func(ByVal vURL As String, ByVal vPluginID As Integer, ByVal vPersonID As Integer) As IHaveArticle_WS_resp
        Dim resp As New IHaveArticle_WS_resp
        Try

            vURL = vURL.UrlDecode()
            Dim vDOI As String = BLL.DOI.GetDOIbyURL_internal(vURL)


            Dim ta As New ArticlesDataSetTableAdapters.ArticleOffersTableAdapter
            ta.Insert(Now(), vURL, 0, vPluginID, vPersonID, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)

            Dim d As ProcessIHAVEArticle_Delegate = New ProcessIHAVEArticle_Delegate(AddressOf ProcessIHAVEArticle)
            Dim R As IAsyncResult = d.BeginInvoke(vURL, vPluginID, vPersonID, Nothing, Nothing)


            resp.vStatus = "success"
        Catch ex As Exception
            resp.vStatus = "failure"
        End Try
        Return resp
    End Function
    Public Structure IHaveArticle_WS_resp
        Dim vStatus As String
    End Structure
    Protected Delegate Sub ProcessIHAVEArticle_Delegate(ByVal vURL As String, ByVal vPluginID As Integer, ByVal vPersonID As Integer)
    Protected Sub ProcessIHAVEArticle(ByVal vURL As String, ByVal vPluginID As Integer, ByVal vPersonID As Integer)
        Dim vPMID As Integer = 0
        vPMID = BLL.PubMed.GetPubMedIDfromURL(vURL)
        Dim ta As New ArticlesDataSetTableAdapters.ArticleOffersTableAdapter
        If vPMID > 0 Then
            BLL.PubMed.ProcessPubmedView(vPMID)
            ta.UpdatePubmedID(vPMID, vPersonID, vURL)
        Else
            Dim vTitle As String
            Dim vHTML As String
            'TODO - we should check in base by URL first, if there's - try to read it from saved file
            Dim vDOI As String
            vHTML = vURL.GetHTML()
            vDOI = BLL.DOI.GetDOIFromHTML(vHTML)
            vTitle = vHTML.GetTitleTagText()
            ta.UpdateTitleDOI(vTitle, vDOI, vPersonID, vURL)
        End If
    End Sub

End Class
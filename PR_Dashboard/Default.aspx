﻿<%@ Page Title="Home Page" Language="vb" MasterPageFile="~/Masters/bs.Master" AutoEventWireup="false" Theme="Theme2"
    CodeBehind="Default.aspx.vb" Inherits="BioWS._Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph1" runat="server">
    <div class="row-fluid">
        <div class="span6">
            <div class="page-header">
                <h1>
                    Login <small>by filling the following form</small></h1>
            </div>
            <asp:Login ID="LoginUser" runat="server" EnableViewState="false" RenderOuterTable="false"
                DestinationPageUrl="~/MyPageRedirect.aspx">
                <LayoutTemplate>
                    
                    <div class="form-horizontal">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label" for="input01">
                                    E-mail:</label>
                                <div class="controls">
                                    <asp:TextBox ID="UserName" runat="server" class="input-xlarge"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                        CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="User Name is required."
                                        ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="input01">
                                    Password:</label>
                                <div class="controls">
                                    <asp:TextBox ID="Password" runat="server" CssClass="input-xlarge" TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                        CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required."
                                        ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="input01">
                                </label>
                                <div class="controls">
                                    <table>
                                        <tr>
                                            <td valign="middle">
                                                <asp:CheckBox ID="RememberMe" runat="server" />
                                            </td>
                                            <td valign="middle">
                                                keep me logged in
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="input01">
                                </label>
                                <div class="controls">
                                    <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="failureNotification"
                                        ValidationGroup="LoginUserValidationGroup" />
                                    <span class="failureNotification">
                                        <asp:Literal ID="FailureText" runat="server"></asp:Literal>
                                    </span>
                                </div>
                            </div>
                            <div class="form-actions">
                                
                                <asp:Button ID="LoginButton" class="btn btn-primary" runat="server" CommandName="Login"
                                    Text="Log In" ValidationGroup="LoginUserValidationGroup"></asp:Button>
                                <asp:HyperLink ID="RestorePasswordHL" class="fright" runat="server" EnableViewState="false"
                                    NavigateUrl="~/RestorePassword.aspx">restore password</asp:HyperLink>
                            </div>
                        </fieldset>
                    </div>
                    
                </LayoutTemplate>
            </asp:Login>
        </div>
        <div class="span6">
          
            <div class="page-header">
                <h1>
                    Register <small>by downloading plug-in</small></h1>
            </div>
            <div style="width: 100%; border: 1px solid lightgray; margin-top: 10px; height: 280px;">
                <iframe width="100%" height="100%" frameborder="0" src="Legal/EULA1_1.htm"></iframe>
            </div>
            <div class="form-horizontal">
                <div class="form-actions">
                    <asp:Button ID="btnAccept" class="btn btn-primary" runat="server" CommandName="Login"
                        Text="Accept EULA and download plug-in"></asp:Button>
                </div>
            </div>
      

             <table cellpadding="5" cellspacing="5">
                            <tr>
                                <td valign="middle">
                                    <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/IE_icon.png" />
                                </td>
                                <td valign="middle">
                                    <asp:ImageButton ID="imgbtnIE" runat="server"  Width="110px" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle">
                                    <asp:Image ID="Image8" runat="server" ImageUrl="~/Images/firefox_icon.png" />
                                </td>
                                <td valign="middle">
                                    <asp:HyperLink ID="hlFF" runat="server"></asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle">
                                    <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/chrome_icon.png" />
                                </td>
                                <td valign="middle">
                                    <asp:HyperLink ID="hlCHR" runat="server" Target="_blank"></asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/opera_icon.jpg" />
                                </td>
                                <td valign="middle">
                                    <asp:HyperLink ID="hlOpera" runat="server"></asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle">
                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/safari_icon.png" />
                                </td>
                                <td valign="middle">
                                    <asp:HyperLink ID="hlSafari" runat="server"></asp:HyperLink>
                                </td>
                            </tr>
                        </table>

            <br />
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Basic.Master" CodeBehind="PluginAbout.aspx.vb" Inherits="BioWS.PluginAbout" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<table width="100%">
<tr>
<td valign="middle"  style="width:60px; text-align:center;"   align="center" >
    <asp:Image ID="imgBrowser" runat="server"  />
</td>
<td valign="middle" >
<h1>
    <asp:Literal ID="litTitle" runat="server"></asp:Literal>
</h1>
</td>
</tr>
<tr>
<td valign="middle"  style="width:60px; text-align:center;"   align="center" >
    <asp:Image ID="imgVersion" runat="server"  />
</td>
<td valign="middle">

    <asp:Literal ID="litVersion" runat="server"></asp:Literal>
</td>
</tr>
</table>

<div style="width:70%">
    <asp:Literal ID="litOverview" runat="server"></asp:Literal>
</div>


</asp:Content>

﻿Imports System.Web.UI.DataVisualization.Charting
Imports DevExpress.Web
Public Class ViewInfoPMWidget
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.ASPxSplitter1.Panes(0).ContentUrl = String.Format("Widgets/ViewWidget.aspx?PersonID={0}&WidgetID={1}&Pubmed_ID={2}",
                                                   Request.QueryString("PersonID"),
                                                   Request.QueryString("WidgetID"),
                                                   Request.QueryString("Pubmed_ID"))


        CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "Article information - Widget"
        CType(Master.Master.FindControl("page_header"), System.Web.UI.HtmlControls.HtmlGenericControl).Visible = False

        Dim pma As New BLL.PubMed.PubMedArticle(Request.QueryString("Pubmed_ID"))
        Dim ct1 As ContentPlaceHolder = CType(Master.Master.FindControl("MainContent"), ContentPlaceHolder)
        CType(ct1.FindControl("hl_pm"), HyperLink).Text = pma.Title
        CType(ct1.FindControl("hl_pm"), HyperLink).NavigateUrl = pma.PubmedURL

        Dim b As Integer

    End Sub




End Class
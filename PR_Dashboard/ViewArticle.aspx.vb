﻿Imports BioWS.BLL.Extensions

Public Class ViewArticle2
    Inherits System.Web.UI.Page

    Private _PersonID As Integer?
    Public ReadOnly Property PersonID As Integer
        Get
            If _PersonID Is Nothing Then
                If User.Identity.IsAuthenticated = True Then
                    _PersonID = Me.GetProfilePersonID()
                Else
                    _PersonID = Me.QSInt32("UserID")
                End If
            End If
            Return _PersonID
        End Get
    End Property

    Public ReadOnly Property URL As String
        Get
            Return HttpUtility.UrlDecode(Me.QS("URL"))
        End Get
    End Property

    Private _PubmedID As Integer?
    Public ReadOnly Property PubmedID As Integer
        Get
            If _PubmedID Is Nothing Then
                Dim pmid1 = BLL.PubMed.GetPubMedIDfromURL(URL)
                If pmid1 > 0 Then
                    _PubmedID = pmid1
                Else
                    _PubmedID = BLL.PubMed.FindPubMedIDbyURL(URL)
                End If
            End If
            Return _PubmedID
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If PersonID = 0 Then
            Me.Response.Redirect("~\Account\Register.aspx", False)
        Else
            If Not Me.IsAuthenticated Then
                Dim pers As New BLL.Person(PersonID)
                FormsAuthentication.SetAuthCookie(pers.UserName, False)
            End If


            If PubmedID > 0 Then
                Response.Redirect("~\AccountNew\ViewArticle.aspx?id=" & PubmedID & "&UserID=" & Request.QueryString("UserID"), False)
            Else
                '! try get DOI
                Dim doistr As String = BLL.DOI.GetDOIbyURL_internal(URL)
                If doistr.IsNotEmpty Then
                    '! try get fulltext
                    Dim offersta As New ArticlesDataSetTableAdapters.ArticleOffersTableAdapter
                    Dim offt = offersta.GetWithFilesByDOI(doistr)
                    If offt.Count > 0 Then
                        Dim offerRow = offt.Item(0)
                        Dim downloadlink As String = "~/Download.ashx?vFileName=" & HttpUtility.UrlEncode(offerRow.FileName) & "&vOriginalName=" & HttpUtility.UrlEncode(offerRow.OriginalName)
                        Response.Redirect(downloadlink)
                    Else
                        Me.lit_title.Text = "This is not Pubmed-article URL. No info is available."
                    End If
                Else
                    Me.lit_title.Text = "This is not Pubmed-article URL. No info is available."
                End If
            End If

        End If





    End Sub

End Class
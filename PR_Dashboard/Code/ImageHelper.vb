Imports System.IO

Namespace BLL
    Public Class ImageHelper
        Public Shared Function MakeThumb(ByVal fullsize As Byte(), ByVal newwidth As Integer, ByVal newheight As Integer) As Byte()
            Dim iOriginal, iThumb As System.Drawing.Image
            Dim scaleH, scaleW As Double
            Dim srcRect As Drawing.Rectangle
            ' Grab Original Image
            iOriginal = System.Drawing.Image.FromStream(New MemoryStream(fullsize))
            ' Find Height and Width for Thumbnail Image

            scaleH = iOriginal.Height / newheight
            scaleW = iOriginal.Width / newwidth
            If scaleH = scaleW Then
                srcRect.Width = iOriginal.Width
                srcRect.Height = iOriginal.Height
                srcRect.X = 0
                srcRect.Y = 0
            ElseIf (scaleH) > (scaleW) Then
                srcRect.Width = iOriginal.Width
                srcRect.Height = CInt(newheight * scaleW)
                srcRect.X = 0
                srcRect.Y = CInt((iOriginal.Height - srcRect.Height) / 2)
            Else
                srcRect.Width = CInt(newwidth * scaleH)
                srcRect.Height = iOriginal.Height
                srcRect.X = CInt((iOriginal.Width - srcRect.Width) / 2)
                srcRect.Y = 0
            End If

            iThumb = New System.Drawing.Bitmap(newwidth, newheight)
            Dim g As Drawing.Graphics = Drawing.Graphics.FromImage(iThumb)
            g.DrawImage(iOriginal, New Drawing.Rectangle(0, 0, newwidth, newheight), srcRect, Drawing.GraphicsUnit.Pixel)

            Dim m As New IO.MemoryStream()
            iThumb.Save(m, System.Drawing.Imaging.ImageFormat.Jpeg)
            Return m.GetBuffer()
        End Function
        Public Shared Function MakeThumbMaxWidth(ByVal fullsize As Byte(), ByVal maxwidth As Integer) As Byte()
            Dim iOriginal, iThumb As System.Drawing.Image
            Dim scale As Double
            ' Grab Original Image
            iOriginal = System.Drawing.Image.FromStream(New MemoryStream(fullsize))
            If iOriginal.Width > maxwidth Then
                scale = iOriginal.Width / maxwidth
                Dim newheight As Integer = CInt(iOriginal.Height / scale)
                iThumb = New System.Drawing.Bitmap(iOriginal, maxwidth, newheight)
                Dim m As New IO.MemoryStream()
                iThumb.Save(m, System.Drawing.Imaging.ImageFormat.Jpeg)
                Return m.GetBuffer()
            Else
                Return fullsize
            End If
        End Function
        Public Shared Function MakeThumbMaxHeight(ByVal fullsize As Byte(), ByVal maxheight As Integer) As Byte()
            Dim iOriginal, iThumb As System.Drawing.Image
            Dim scale As Double

            ' Grab Original Image
            iOriginal = System.Drawing.Image.FromStream(New MemoryStream(fullsize))

            If iOriginal.Height > maxheight Then

                scale = iOriginal.Height / maxheight
                Dim newwidth As Integer = CInt(iOriginal.Width / scale)

                iThumb = New System.Drawing.Bitmap(iOriginal, newwidth, maxheight)
                Dim m As New IO.MemoryStream()
                iThumb.Save(m, System.Drawing.Imaging.ImageFormat.Jpeg)
                Return m.GetBuffer()
            Else
                Return fullsize
            End If
        End Function
        Public Shared Function MakeThumbMaxWidthHeight(ByVal fullsize As Byte(), ByVal maxheight As Integer, ByVal maxwidth As Integer) As Byte()
            Dim iOriginal = System.Drawing.Image.FromStream(New MemoryStream(fullsize))
            If iOriginal.Height > iOriginal.Width Then
                ' this is portrait picture
                Return MakeThumbMaxHeight(fullsize, maxheight)
            Else
                ' this is landscape  picture
                Return MakeThumbMaxWidth(fullsize, maxwidth)
            End If
        End Function




        Public Shared Function ImageUploadValidate(ByVal vContentType As String, ByVal vFileSizeBytes As Integer) As String

            Dim vContentTypeIsOk As Boolean
            Dim vFileSizeIsOk As Boolean

            ' check if file size is ok - 3 megabytes
            If vFileSizeBytes < 3145728 Then
                vFileSizeIsOk = True
            Else
                vFileSizeIsOk = False
            End If

            ' check if file type is ok - only pics
            Select Case vContentType
                Case "image/pjpeg"
                    vContentTypeIsOk = True
                Case "image/jpeg"
                    vContentTypeIsOk = True
                Case "image/x-png"
                    vContentTypeIsOk = True
                Case "image/gif"
                    vContentTypeIsOk = True
                Case Else
                    vContentTypeIsOk = False
            End Select


            If vFileSizeIsOk = True And vContentTypeIsOk = True Then
                Return "true"
            Else
                If vFileSizeIsOk = False And vContentTypeIsOk = True Then
                    Return "File size - " & vFileSizeBytes / 1024 & " Kb. Maximum allowed upload file size is <b>3 Mb</b>."
                End If
                If vFileSizeIsOk = True And vContentTypeIsOk = False Then
                    Return "File type - <i>" & vContentType & "</i> is not allowed to be uploaded. Valid files are file with extensions <b>JPG, PNG и GIF</b> "
                End If
                If vFileSizeIsOk = False And vContentTypeIsOk = False Then
                    Return "File size or type is invalid"
                End If
            End If
        End Function

        
    End Class

End Namespace

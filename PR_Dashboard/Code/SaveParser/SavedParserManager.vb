Imports System.Threading
Imports BioWS.mylinq
Imports System.Net
Imports AzureStorageLibrary

Imports BioWS.BLL.Extensions


Namespace BLL.DelayedParsing
    Public Class SavedParserManager


#Region "Properties"
        Private Property StartDate As DateTime
        Private Property EndDate As DateTime
        Public Property Status As SavedParserHelper.ParserManagerStatuses
        Public Property PreparsingDictionary As Dictionary(Of String, Situation) = New Dictionary(Of String, Situation)
        Public Property ParsingDictionary As Dictionary(Of String, Situation) = New Dictionary(Of String, Situation)
#End Region
#Region "Initializing"
        Sub New(ByVal sDate As DateTime, ByVal eDate As DateTime)
            StartDate = sDate
            EndDate = eDate
            Status = SavedParserHelper.ParserManagerStatuses.DefinedNotStarted
            DefinePreparsingDict()
            LoadPreparsingDict(True)
            DefineParsingDict()
        End Sub
        Private Sub DefineParsingDict()
            AddParsingSituation("processed", "Total count of processed records")
            AddParsingSituation("byPR", "Using parsing rules", "processed")

            AddParsingSituation("byPRsuccess", "Success", "byPR")

            AddParsingSituation("byPRsuccessURL", "Successful URL parsing", "byPRsuccess")
            AddParsingSituation("byPRsuccessHTML", "Successful HTML parsing", "byPRsuccess")
            AddParsingSituation("byPRsuccessUPDATED", "Records updated", "byPRsuccess")

            AddParsingSituation("byPRnoresult", "No result", "byPR")

            AddParsingSituation("byPRfailure", "No rule applied or parsing failure", "byPRnoresult")
            AddParsingSituation("byPRnorule", "No rule with ID recognition defined", "byPRnoresult")
            AddParsingSituation("byPRcontainDOI", "Has DOI", "byPRnoresult")

            AddParsingSituation("bydoisearch", "Using DOI search in Pubmed", "processed")

            AddParsingSituation("byDOIfailure", "Pubmed search failure", "bydoisearch")
            AddParsingSituation("byDOIsuccess", "Success", "bydoisearch")
            AddParsingSituation("byDOIupdated", "Records updated", "bydoisearch")
        End Sub
        Private Sub AddParsingSituation(ByVal key As String, ByVal title As String, Optional ByVal parentkey As String = "")
            Dim newsituation As Situation = New Situation()
            newsituation.Key = key
            newsituation.Title = title
            If parentkey.Length > 0 Then
                newsituation.ParentKey = parentkey
            End If
            ParsingDictionary.Add(newsituation.Key, newsituation)
        End Sub
        Private Sub DefinePreparsingDict()
            Dim total As New Situation() With {.Key = "total", .Title = "Total count of records"}
            Dim withpm As New Situation() With {.Key = "withpm", .Title = "With pubmed ID", .ParentKey = "total"}
            Dim withoutpm As New Situation() With {.Key = "withoutpm", .Title = "Without pubmed ID", .ParentKey = "total"}
            Dim withdoi As New Situation() With {.Key = "withdoi", .Title = "With DOI", .ParentKey = "withoutpm"}
            Dim withoutdoi As New Situation() With {.Key = "withoutdoi", .Title = "Without DOI", .ParentKey = "withoutpm"}

            PreparsingDictionary.Add(total.Key, total)
            PreparsingDictionary.Add(withpm.Key, withpm)
            PreparsingDictionary.Add(withoutpm.Key, withoutpm)
            PreparsingDictionary.Add(withdoi.Key, withdoi)
            PreparsingDictionary.Add(withoutdoi.Key, withoutdoi)
        End Sub
        Private Sub LoadPreparsingDict(ByVal isBefore As Boolean)
            Dim dt1, dt2 As String
            dt1 = StartDate.ToString("yyyy-MM-dd")
            dt2 = EndDate.ToString("yyyy-MM-dd")
            Dim ta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
            Dim total As Integer = ta.GetCountAll_ByTwoDates(dt1, dt2)
            Dim withpm As Integer = ta.GetCountWithPubMed_ByTwoDates(dt1, dt2)
            Dim withoutpm As Integer = total - withpm
            Dim withoutdoi As Integer = ta.GetCountWOpubmedWOdoi(dt1, dt2)
            Dim withdoi As Integer = ta.GetCountWOpubmedWITHdoi(dt1, dt2)
            If isBefore Then
                PreparsingDictionary("total").BeforeCount = total
                PreparsingDictionary("withpm").BeforeCount = withpm
                PreparsingDictionary("withoutpm").BeforeCount = withoutpm
                PreparsingDictionary("withdoi").BeforeCount = withdoi
                PreparsingDictionary("withoutdoi").BeforeCount = withoutdoi
            Else
                PreparsingDictionary("total").AfterCount = total
                PreparsingDictionary("withpm").AfterCount = withpm
                PreparsingDictionary("withoutpm").AfterCount = withoutpm
                PreparsingDictionary("withdoi").AfterCount = withdoi
                PreparsingDictionary("withoutdoi").AfterCount = withoutdoi
            End If

        End Sub
#End Region
#Region "Domains and statuses"



        Private Function GetListOfStringsByKeyAndTwoDates(ByVal key As String) As List(Of DomainItem)
            Dim res As List(Of DomainItem) = New List(Of DomainItem)

            Dim query As String
            If PreparsingDictionary.ContainsKey(key) Then
                query = String.Format("EXEC [dbo].[GetDomainsByParsingKeyAndTwoDates]    '{0:yyyy-MM-dd}'  ,'{1:yyyy-MM-dd}'  ,'{2}'", StartDate, EndDate, key)

                Dim dt = BLL.SQLHelper.GetDT_AllStrings(query)
                For i = 0 To dt.Rows.Count - 1
                    Dim di As New DomainItem() With {.ID = dt.Rows(i).Item(0), .Domain = dt.Rows(i).Item(1), .Count = CInt(dt.Rows(i).Item(2))}
                    res.Add(di)
                Next

            End If
            If ParsingDictionary.ContainsKey(key) Then
                Dim situation As Situation = ParsingDictionary(key)
                If situation.DomainsDictionary.Count > 0 Then

                    query = String.Format("SELECT MonitoredSites.ID, MonitoredSites.SiteURL FROM  MonitoredSites WHERE MonitoredSites.ID in ({0})",
                                          situation.DomainsDictionary.Keys.ToList().ToSeparatedString(","))

                    Dim dt = BLL.SQLHelper.GetDT_AllStrings(query)
                    For i = 0 To dt.Rows.Count - 1
                        Dim row As DataRow = dt.Rows(i)
                        Dim id As Integer = row.Item(0)
                        Dim di As New DomainItem() With {.ID = id, .Domain = row.Item(1), .Count = situation.DomainsDictionary(id)}
                        res.Add(di)
                    Next

                End If
            End If




            Return res






        End Function



        Public Function GetStatusString() As String
            Select Case Status
                Case SavedParserHelper.ParserManagerStatuses.DefinedNotStarted
                    Dim days As Integer = DateDiff(DateInterval.Day, StartDate, EndDate) + 1
                    Return String.Format("Parsing period: from <b>{0}</b> to <b>{1}</b> - {2} day(s)", StartDate.ToLongDateString, EndDate.ToLongDateString, days)
                Case SavedParserHelper.ParserManagerStatuses.DefinedRunning
                    Return "Is running"
                Case SavedParserHelper.ParserManagerStatuses.DefinedFinished
                    Return "Finished"
                Case Else
                    Return ""
            End Select
        End Function
        '! functions for domains
        Public Function GetDomains(ByVal key As String) As List(Of DomainItem)
            Dim res As List(Of DomainItem) = New List(Of DomainItem)
            res = GetListOfStringsByKeyAndTwoDates(key)
            Return res
        End Function
#End Region


        Public Sub StartProcessing()
            Dim t As New Threading.Thread(AddressOf InThreadProcessing)
            t.Start()
        End Sub
        Private Sub InThreadProcessing()
            Status = SavedParserHelper.ParserManagerStatuses.DefinedRunning
            Dim db As New DataClassesParsingDataContext
            '! define loop limits
            Dim recordsCount As Integer = PreparsingDictionary("total").BeforeCount
            Const pageSize As Integer = 100
            Dim pagesCount As Integer = CInt(recordsCount / pageSize) + 1

            '! loop over pages
            Using db
                For pageInd As Integer = 0 To pagesCount
                    Dim pageData = From rows In db.GetViewsByTwoDatesTakeSkip(pageSize, pageInd * pageSize, StartDate, EndDate) Select rows
                    ProcessPageData(pageData)
                Next
            End Using

            LoadPreparsingDict(False)

            Status = SavedParserHelper.ParserManagerStatuses.DefinedFinished
        End Sub
        Private Sub ProcessPageData(ByVal pageData As IEnumerable(Of GetViewsByTwoDatesTakeSkipResult))
            '! loop over records in page
            For Each record As GetViewsByTwoDatesTakeSkipResult In pageData
                ProcessRecord(record)
            Next
        End Sub
        Private Sub ProcessRecord(ByVal record As GetViewsByTwoDatesTakeSkipResult)

            IncreaseKeyOne("processed", record.MonitoredSite_ID)

            '! imitate url parsing 
            IncreaseKeyOne("byPR", record.MonitoredSite_ID)
            Dim URLpa As New ParsingAttempt(record.MonitoredSite_ID, record.URL, String.Empty, ParsingAction.Origins.CheckURLWS)
            If Not URLpa.HasRulesWithParsing Then
                IncreaseKeyOne("byPRnoresult", record.MonitoredSite_ID)
                IncreaseKeyOne("byPRnorule", record.MonitoredSite_ID)
                If Not record.DOI.IsNullOrEmpty() Then
                    IncreaseKeyOne("byPRcontainDOI", record.MonitoredSite_ID)
                    TrySearchDOI(record.DOI, record.MonitoredSite_ID)
                End If
            Else
                If Not URLpa.ParsingResult.HTMLRequired Then
                    '! URL Parsing finishes here
                    If URLpa.ParsingResult.HasPubmed Then
                        IncreaseKeyOne("byPRsuccess", record.MonitoredSite_ID)
                        IncreaseKeyOne("byPRsuccessURL", record.MonitoredSite_ID)
                        UpdateRecordsByPR(URLpa.ParsingResult.PubmedID, record.URL, record.MonitoredSite_ID)
                    Else
                        IncreaseKeyOne("byPRnoresult", record.MonitoredSite_ID)
                        IncreaseKeyOne("byPRfailure", record.MonitoredSite_ID)
                        If URLpa.ParsingResult.HasDOI Then
                            IncreaseKeyOne("byPRcontainDOI", record.MonitoredSite_ID)
                            TrySearchDOI(URLpa.ParsingResult.DOI, record.MonitoredSite_ID)
                        End If
                    End If
                Else
                    '! HTML Parsing goes on here
                    Dim HTMLpa As New ParsingAttempt(record.MonitoredSite_ID, record.URL, GetHTML(record.FileName, record.URL), ParsingAction.Origins.SaveHTMLWS)
                    If HTMLpa.ParsingResult.HasPubmed Then
                        IncreaseKeyOne("byPRsuccess", record.MonitoredSite_ID)
                        IncreaseKeyOne("byPRsuccessHTML", record.MonitoredSite_ID)
                        UpdateRecordsByPR(HTMLpa.ParsingResult.PubmedID, record.URL, record.MonitoredSite_ID)
                    Else
                        IncreaseKeyOne("byPRnoresult", record.MonitoredSite_ID)
                        IncreaseKeyOne("byPRfailure", record.MonitoredSite_ID)
                        If HTMLpa.ParsingResult.HasDOI Then
                            IncreaseKeyOne("byPRcontainDOI", record.MonitoredSite_ID)
                            TrySearchDOI(URLpa.ParsingResult.DOI, record.MonitoredSite_ID)
                        End If
                    End If
                End If
            End If


        End Sub
        Private Sub TrySearchDOI(ByVal doi As String, ByVal domainID As Integer)
            Dim pmid As Integer
            Dim success As Boolean = BLL.Common.TryFindPubmedIDbyDOI(doi, pmid)
            IncreaseKeyOne("bydoisearch", domainID)
            If success Then
                IncreaseKeyOne("byDOIsuccess", domainID)
                UpdateRecordsByDOI(pmid, doi, domainID)
            Else
                IncreaseKeyOne("byDOIfailure", domainID)
            End If
        End Sub
        Private Sub UpdateRecordsByPR(ByVal PubmedID As Integer, ByVal url As String, ByVal domainID As Integer)
            Dim ta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
            Dim recCount = ta.UpdatePubmedIDbyURL(PubmedID, url)
            IncreaseKey("byPRsuccessUPDATED", 0, recCount)
        End Sub
        Private Sub UpdateRecordsByDOI(ByVal PubmedID As Integer, ByVal DOI As String, ByVal domainID As Integer)
            Dim ta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
            Dim recCount = ta.UpdatePubmedIDbyDOI(PubmedID, DOI)
            IncreaseKey("byDOIupdated", 0, recCount)
        End Sub


        Private Function GetHTML(ByVal fileguid As String, ByVal url As String)
            'Dim html As String = BLL.Common.ReadSavedHTML(fileguid)
            Dim storage As New AzureStorage()
            Dim html As String = storage.DownloadHtml(String.Format("{0}.html", fileguid))

            If html.IsNullOrEmpty() Then
                Dim wc As New WebClient
                html = wc.DownloadString(url)
            End If
            Return html
        End Function
        Private Sub IncreaseKeyOne(ByVal key As String, ByVal domainID As Integer)
            IncreaseKey(key, domainID, 1)
        End Sub
        Private Sub IncreaseKey(ByVal key As String, ByVal domainID As Integer, ByVal increase As Integer)
            Dim sit As Situation = ParsingDictionary(key)
            sit.CasesCount = sit.CasesCount + increase
            If domainID > 0 Then
                If sit.DomainsDictionary.ContainsKey(domainID).Inverse() Then
                    sit.DomainsDictionary.Add(domainID, 1)
                Else
                    sit.DomainsDictionary(domainID) = sit.DomainsDictionary(domainID) + 1
                End If
            End If
        End Sub

    End Class

End Namespace

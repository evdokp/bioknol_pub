﻿Imports System.Threading
Imports System.Collections.Generic


Namespace BLL.DelayedParsing
    Public Class Situation
        Public Property Key As String
        Public Property Title As String
        Public Property CasesCount As Integer
        Public Property BeforeCount As Integer
        Public Property AfterCount As Integer
        Public Property ParentKey As String
        Public Property DomainsDictionary As Dictionary(Of Integer, Integer) = New Dictionary(Of Integer, Integer)


    End Class
End Namespace

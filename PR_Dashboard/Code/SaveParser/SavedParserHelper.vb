
Namespace BLL.DelayedParsing
    Public Class SavedParserHelper
        Public Enum ParserManagerStatuses
            Undefined
            DefinedNotStarted
            DefinedRunning
            DefinedFinished
        End Enum

        Public Shared Property CreatedWhen As DateTime
        Public Shared Property ParserManager As SavedParserManager
        Public Shared ReadOnly Property ParserManagerStatus As ParserManagerStatuses
            Get
                If ParserManager Is Nothing Then
                    Return ParserManagerStatuses.Undefined
                Else
                    Return ParserManager.Status
                End If
            End Get
        End Property
        Shared Sub New()
            CreatedWhen = Now
        End Sub
        Shared Function GetStatus() As String
            If ParserManagerStatus = ParserManagerStatuses.Undefined Then
                Return "no parser manager defined"
            Else
                Return ParserManager.GetStatusString()
            End If
        End Function
    End Class

End Namespace

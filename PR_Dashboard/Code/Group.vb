﻿Imports System.IO
Imports AzureStorageLibrary

Namespace BLL
    Public Class Group

        Public Property Group_ID() As Integer
        Public Property Description() As String
        Public Property AddedByPersonID() As Integer
        Public Property ParentGroup_ID() As Integer
        Public Property Title() As String
        Public Property RequiresApproval() As Boolean
        Public Property AddedDate() As DateTime
        Private ReadOnly Property EmailHash() As String
            Get
                Return BLL.Common.GetMD5Hash("test@example.com".ToLower())
            End Get
        End Property
        Public ReadOnly Property Image50() As String
            Get
                If HasImage Then
                    Return "https://bioknolstorage.blob.core.windows.net/userimages/" & AvatarGUID & "_50." & AvatarExtension
                Else
                    Return "http://www.gravatar.com/avatar/" & EmailHash & "?s=50&d=identicon"
                End If
            End Get
        End Property
        Public ReadOnly Property Image50abs As String
            Get
                If HasImage Then
                    Return "https://bioknolstorage.blob.core.windows.net/userimages/" & AvatarGUID & "_50." & AvatarExtension
                Else
                    Return "http://www.gravatar.com/avatar/" & EmailHash & "?s=50&d=identicon"
                End If
            End Get
        End Property
        Public ReadOnly Property Image200() As String
            Get
                If HasImage Then
                    Return "https://bioknolstorage.blob.core.windows.net/userimages/" & AvatarGUID & "_200." & AvatarExtension
                Else
                    Return "http://www.gravatar.com/avatar/" & EmailHash & "?s=170&d=identicon"
                End If

            End Get
        End Property
        Public ReadOnly Property HasImage As Boolean
            Get
                Return IIf(AvatarGUID.Length > 0, True, False)
            End Get
        End Property
        Public ReadOnly Property HasParent As Boolean
            Get
                Return ParentGroup_ID > 0
            End Get
        End Property

        Private Property AvatarGUID() As String
        Private Property AvatarExtension As String

#Region "Group count properties"
        Public ReadOnly Property DirectChildGroupsCount As Integer
            Get
                Dim ta As New PersonsDataSetTableAdapters.GroupsTableAdapter
                Return ta.GetChildrenCountByParentID(Group_ID)
            End Get
        End Property
        Public ReadOnly Property AllChildGroupsCount As Integer
            Get
                Dim ta As New PersonsDataSetTableAdapters.GroupsTableAdapter
                Return ta.GetChildrenCountAllByParentID(Group_ID) - 1
            End Get
        End Property
        Public ReadOnly Property GrandChildGroupsCount As Integer
            Get
                Return AllChildGroupsCount - DirectChildGroupsCount
            End Get
        End Property
        Public ReadOnly Property DirectParticipantsCount As Integer
            Get
                Dim ta As New PersonsDataSetTableAdapters.LinksGroupsToPersonsTableAdapter
                Return ta.GetCountByGroup_ID(Group_ID)
            End Get
        End Property
        Public ReadOnly Property AllParticipantsCountInclChildren As Integer
            Get
                Dim ta As New PersonsDataSetTableAdapters.LinksGroupsToPersonsTableAdapter
                Return ta.GetCountInclChildrenByGroup_ID(Group_ID)
            End Get
        End Property
        Public ReadOnly Property GrandChildParticipantsCount As Integer
            Get
                Return AllParticipantsCountInclChildren - DirectParticipantsCount
            End Get
        End Property
        Public ReadOnly Property Anchor As String
            Get
                Return "<a href=""http://www.bioknowledgecenter.ru/Account/ViewGroup.aspx?GroupID=" & Group_ID & """>" & Title & "</a>"
            End Get
        End Property
        Public ReadOnly Property URL As String
            Get
                Return "~\Account\ViewGroup.aspx?GroupID=" & Group_ID
            End Get
        End Property
#End Region

        Public Shared Function GetURLbyID(ByVal vGroup_ID As Integer) As String
            Return "~\Account\ViewGroup.aspx?GroupID=" & vGroup_ID
        End Function
        Public Shared Function IsPersonDirectParticipant(ByVal vGroup_ID As Integer, ByVal vPerson_ID As Integer) As Boolean
            Dim ta As New PersonsDataSetTableAdapters.LinksGroupsToPersonsTableAdapter
            Dim i = ta.GetCountByTwoIDs(vGroup_ID, vPerson_ID)
            If i > 0 Then
                Return True
            Else
                Return False
            End If
        End Function

        Function ParticipationLinksCount(ByVal vPersonID As Integer)
            Dim ta As New PersonsDataSetTableAdapters.LinksGroupsToPersonsTableAdapter
            Return ta.GetCountByTwoIDs(Group_ID, vPersonID)
        End Function


        Sub New(ByVal vTitle As String, ByVal vParentID As Integer, ByVal vAddedByPersonID As Integer, ByVal vDesc As String, ByVal vReqAppr As Boolean)
            Dim ta As New PersonsDataSetTableAdapters.GroupsTableAdapter
            ParentGroup_ID = vParentID
            Title = vTitle
            Description = vDesc
            AddedByPersonID = vAddedByPersonID
            RequiresApproval = vReqAppr
            AddedDate = Now()
            ta.InsertGroup(ParentGroup_ID, Title, Description, AddedByPersonID, RequiresApproval, Group_ID)
        End Sub
        Sub New(ByVal vGroup_ID As Integer)
            Dim ta As New PersonsDataSetTableAdapters.GroupsTableAdapter
            Dim t = ta.GetDataByID(vGroup_ID)
            Group_ID = t.Item(0).Group_ID
            ParentGroup_ID = t.Item(0).ParentGroup_ID
            Title = t.Item(0).Title
            Description = t.Item(0).Description
            RequiresApproval = t.Item(0).RequiresApproval
            AddedDate = t.Item(0).AddedDate
            AddedByPersonID = t.Item(0).AddedByPersonID
            AvatarGUID = t.Item(0).AvatarGUID
            AvatarExtension = t.Item(0).AvatarExtension
        End Sub
        Sub Update()
            Dim ta As New PersonsDataSetTableAdapters.GroupsTableAdapter
            Dim t = ta.GetDataByID(Group_ID)
            Dim r = t.Item(0)
            r.BeginEdit()
            r.Title = Title
            r.Description = Description
            r.RequiresApproval = RequiresApproval
            r.EndEdit()
            ta.Update(r)
        End Sub


        Public Shared Sub PopulateTopLevelGroups(ByVal tv As TreeView)
            Dim ta As New PersonsDataSetTableAdapters.GroupsTableAdapter
            Dim t = ta.GetTopLevel()
            Dim r As PersonsDataSet.GroupsRow
            Dim i As Integer
            For i = 0 To t.Count - 1
                r = t.Item(i)
                Dim new_node As New TreeNode
                new_node.Text = r.Title
                new_node.Value = r.Group_ID
                new_node.Expanded = False
                'new_node.ImageUrl = "~\Images\users2.png"
                If ta.GetChildrenCountByParentID(r.Group_ID) > 0 Then
                    new_node.PopulateOnDemand = True
                Else
                    new_node.PopulateOnDemand = False
                End If
                tv.Nodes.Add(new_node)
            Next
        End Sub
        Public Shared Sub PopulateTopLevelGroupsWithPersons(ByVal tv As TreeView)
            Dim ta As New PersonsDataSetTableAdapters.GroupsTableAdapter
            Dim t = ta.GetTopLevel()
            Dim r As PersonsDataSet.GroupsRow
            Dim i As Integer
            For i = 0 To t.Count - 1
                r = t.Item(i)
                Dim new_node As New TreeNode
                new_node.Text = r.Title
                new_node.Value = "g" & r.Group_ID
                new_node.Expanded = False
                'new_node.ImageUrl = "~\Images\users2.png"
                If ta.GetChildrenCountByParentID(r.Group_ID) > 0 Then
                    new_node.PopulateOnDemand = True
                Else
                    new_node.PopulateOnDemand = False
                End If
                tv.Nodes.Add(new_node)
            Next
        End Sub
        Public Shared Sub PopulateChildNode(ByVal vNode As TreeNode)
            Dim ta As New PersonsDataSetTableAdapters.GroupsTableAdapter
            Dim t = ta.GetChildren(vNode.Value)
            Dim r As PersonsDataSet.GroupsRow
            Dim i As Integer
            For i = 0 To t.Count - 1
                r = t.Item(i)
                Dim new_node As New TreeNode
                new_node.Text = r.Title
                new_node.Value = r.Group_ID
                new_node.Expanded = False
                '  new_node.ImageUrl = "~\Images\users2.png"
                If ta.GetChildrenCountByParentID(r.Group_ID) > 0 Then
                    new_node.PopulateOnDemand = True
                Else
                    new_node.PopulateOnDemand = False
                End If
                vNode.ChildNodes.Add(new_node)
            Next
        End Sub
        Public Shared Sub PopulateChildNodeWithPersons(ByVal vNode As TreeNode)
            Dim p_ta As New PersonsDataSetTableAdapters.LinksGroupsToPersonsTableAdapter
            Dim ta As New PersonsDataSetTableAdapters.GroupsTableAdapter
            Dim t = ta.GetChildren(vNode.Value.Substring(1))
            Dim r As PersonsDataSet.GroupsRow
            Dim i As Integer
            For i = 0 To t.Count - 1
                r = t.Item(i)
                Dim new_node As New TreeNode
                new_node.Text = r.Title
                new_node.Value = "g" & r.Group_ID
                new_node.Expanded = False
                '  new_node.ImageUrl = "~\Images\users2.png"
                If ta.GetChildrenCountByParentID(r.Group_ID) > 0 Or p_ta.GetCountByGroup_ID(r.Group_ID) > 0 Then
                    new_node.PopulateOnDemand = True
                Else
                    new_node.PopulateOnDemand = False
                End If
                vNode.ChildNodes.Add(new_node)
            Next
            Dim p_t = p_ta.GetDataByGroup_ID(vNode.Value.Substring(1))
            Dim p_r As PersonsDataSet.LinksGroupsToPersonsRow
            For i = 0 To p_t.Count - 1
                p_r = p_t.Item(i)
                Dim pers As New BLL.Person(p_r.Person_ID)
                Dim new_node As New TreeNode
                new_node.Text = pers.DisplayName & " (" & pers.UserName & ")"
                new_node.Value = "p" & pers.Person_ID
                new_node.Expanded = False
                new_node.ImageUrl = "~\Images\user1.png"
                new_node.PopulateOnDemand = False
                vNode.ChildNodes.Add(new_node)
            Next

        End Sub
        Public Shared Sub ReFillChildrenTable()
            Dim ta As New PersonsDataSetTableAdapters.GroupsTableAdapter
            ta.Groups_ReFill_table()
        End Sub

        Sub SaveImage(guid As String, extension As String, p As Page)
            If HasImage Then
                DeleteImageAzure()
            End If


            Dim ta As New PersonsDataSetTableAdapters.GroupsTableAdapter
            Dim t = ta.GetDataByID(Group_ID)
            Dim r = t.Item(0)
            r.BeginEdit()
            r.AvatarGUID = guid
            r.AvatarExtension = extension
            r.EndEdit()
            ta.Update(r)



        End Sub

        Public Sub DeleteImageAzure()
            If Not String.IsNullOrEmpty(AvatarGUID) Then
                Dim storage As New AzureStorage()
                storage.DeleteImage(String.Format("{0}{1}.{2}", AvatarGUID, "_50", AvatarExtension))
                storage.DeleteImage(String.Format("{0}{1}.{2}", AvatarGUID, "_200", AvatarExtension))
            End If

            'update tables and properties
            Dim ta As New PersonsDataSetTableAdapters.GroupsTableAdapter
            Dim t = ta.GetDataByID(Group_ID)
            Dim r = t.Item(0)
            r.BeginEdit()
            r.AvatarGUID = Nothing
            r.AvatarExtension = Nothing
            r.EndEdit()
            ta.Update(r)


        End Sub




    End Class

End Namespace

﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports AzureStorageLibrary

Namespace BLL


    Public Class Person

#Region "Properties"
        Public Property IsAdmin() As Boolean
        Public Property RegisterDate() As DateTime
        Public Property UserName() As String
        Public Property Patronimic() As String
        Public Property LastName() As String
        Public Property FirstName() As String
        Public Property Person_ID() As Integer
        Private Property AvatarGUID() As String
        Private Property AvatarExtension As String

#End Region



        Public ReadOnly Property Image50() As String
            Get
                If HasImage Then
                    Return "https://bioknolstorage.blob.core.windows.net/userimages/" & AvatarGUID & "_50." & AvatarExtension
                Else
                    Return "http://www.gravatar.com/avatar/" & EmailHash & "?s=50&d=identicon"
                End If
            End Get
        End Property

        Public ReadOnly Property Image50abs As String
            Get
                If HasImage Then
                    Return "https://bioknolstorage.blob.core.windows.net/userimages/" & AvatarGUID & "_50." & AvatarExtension
                Else
                    Return "http://www.gravatar.com/avatar/" & EmailHash & "?s=50&d=identicon"
                End If
            End Get
        End Property

        Public ReadOnly Property Image200() As String
            Get
                If HasImage Then
                    Return "https://bioknolstorage.blob.core.windows.net/userimages/" & AvatarGUID & "_200." & AvatarExtension
                Else
                    Return "http://www.gravatar.com/avatar/" & EmailHash & "?s=170&d=identicon"
                End If

            End Get
        End Property

        Private ReadOnly Property EmailHash() As String
            Get
                Return BLL.Common.GetMD5Hash(UserName.ToLower())
            End Get
        End Property




        Public ReadOnly Property HasImage As Boolean
            Get
                Return IIf(AvatarGUID.Length > 0, True, False)
            End Get
        End Property
        Public ReadOnly Property PageHeader() As String
            Get
                If LastName.Length > 2 Then
                    Return String.Format("{0} {1} {2}", LastName, FirstName, Patronimic)
                Else
                    Return UserName
                End If
            End Get
        End Property
        Public ReadOnly Property DisplayName() As String
            Get
                Dim s As String
                s = String.Format("{0} {1}", LastName, FirstName)
                If s.Length < 3 Then
                    s = UserName
                End If
                Return s
            End Get
        End Property
        Public ReadOnly Property Credentials() As String
            Get
                If FirstName = String.Empty And Patronimic = String.Empty And LastName = String.Empty Then
                    Return UserName
                Else
                    Return String.Format("{0} {1} {2}", FirstName, Patronimic, LastName)
                End If
            End Get
        End Property
        Public ReadOnly Property DaysSinceRegistration() As Integer
            Get
                Return DateDiff(DateInterval.Day, RegisterDate, Today)
            End Get
        End Property
        Public ReadOnly Property DaysActive() As Integer
            Get
                Dim tad As New PersonsDataSetTableAdapters.PersonsTableAdapter
                Return tad.GetActiveDaysCountByPersonID(Person_ID)
            End Get
        End Property
        Public ReadOnly Property TotalViewsCount As Integer
            Get
                Dim ta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
                Return ta.GetCountByPerson_ID(Person_ID)
            End Get
        End Property
        Public ReadOnly Property TotalArticlesCount As Integer
            Get
                Dim ta As New ViewsLogDataSetTableAdapters.v_PubmedViewsTableAdapter
                Return ta.GetArticlesCountByPersonID(Person_ID)
            End Get
        End Property
        Public ReadOnly Property URL
            Get
                Dim qsm As New QueryStringBuilder()
                qsm.SetValue("objid", Person_ID).SetValue("objtype", "p")
                Return "~\AccountNew\ViewPerson.aspx" & qsm.ToQueryString()
            End Get
        End Property

        Public ReadOnly Property URLabs
            Get
                Dim qsm As New QueryStringBuilder()
                qsm.SetValue("objid", Person_ID).SetValue("objtype", "p")
                Return "http://ws.bioknowledgecenter.ru/AccountNew/ViewPerson.aspx" & qsm.ToQueryString()
            End Get
        End Property

        Public ReadOnly Property MyRequestCount_Pending As Integer
            Get
                Dim ta As New ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter
                Return ta.GetCount_R_PendingByPersonID(Person_ID)
            End Get
        End Property
        Public ReadOnly Property MyRequestCount_Accepted As Integer
            Get
                Dim ta As New ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter
                Return ta.GetCount_R_AcceptedByPersonID(Person_ID)
            End Get
        End Property
        Public ReadOnly Property MyRequestCount_Declined As Integer
            Get
                Dim ta As New ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter
                Return ta.GetCount_R_DeclinedByPersonID(Person_ID)
            End Get
        End Property
        Public ReadOnly Property HaveUnprocessedRequestByPMID(ByVal vPMID As Integer) As Boolean
            Get
                Dim ta As New ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter
                If ta.GetCountUnprocessedByPMIDbyPersonID(vPMID, Person_ID) > 0 Then
                    Return True
                Else
                    Return False
                End If
            End Get
        End Property
        Public ReadOnly Property UnprocessedOtherPeopleRequestsCount As Integer
            Get
                'TODO - make really - UNPROCESSED
                Dim ta As New ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter
                Return ta.GetCountByNOTPersonIDunprocessed(Person_ID)
            End Get
        End Property
        Public ReadOnly Property UploadedByMeArticlesCount As Integer
            Get
                Dim ta As New ArticlesDataSetTableAdapters.ArticleOffersTableAdapter
                Return ta.GetCountByPersonIDwithFiles(Person_ID)
            End Get
        End Property
        Public ReadOnly Property UploadedByNOTMeArticlesCount As Integer
            Get
                Dim ta As New ArticlesDataSetTableAdapters.ArticleOffersTableAdapter
                Return ta.GetCountByNOTPersonIDwithFiles(Person_ID)
            End Get
        End Property
        Public ReadOnly Property UnreadMessagesCount As Integer
            Get
                Dim post_ta As New DataSetMessagesTableAdapters.MessagesTableAdapter
                Return post_ta.GetCountNewMessagesByPersonToID(Person_ID)
            End Get
        End Property
        Public ReadOnly Property ViewPeriodDays As Integer
            Get
                Dim ta As New ViewsLogDataSetTableAdapters.v_PubmedViewsTableAdapter
                Dim vMin, vMax As Date
                vMin = ta.GetMinViewDateByPersonID(Person_ID)
                vMax = ta.GetMaxViewDateByPersonID(Person_ID)
                Return DateDiff(DateInterval.Day, vMin, vMax) + 1
            End Get
        End Property
#Region "Constructors"

        Sub New(ByVal vPersonID As Integer)
            'todo - check if there's such user, record
            Using ta As New PersonsDataSetTableAdapters.PersonsTableAdapter()
                Dim t = ta.GetDataByID(vPersonID).Item(0)
                Person_ID = t.Person_ID
                UserName = t.UserName
                FirstName = t.FirstName
                LastName = t.LastName
                Patronimic = t.Patronimic
                RegisterDate = t.RegisterDate
                IsAdmin = t.IsAdmin
                AvatarGUID = t.AvatarGUID
                AvatarExtension = t.AvatarExtension
            End Using
        End Sub
        Sub New(ByVal vUserName As String)
            'todo - check if there's such user, record
            Using ta As New PersonsDataSetTableAdapters.PersonsTableAdapter()
                Dim t = ta.GetDataByUserName(vUserName).Item(0)
                Person_ID = t.Person_ID
                UserName = t.UserName
                FirstName = t.FirstName
                LastName = t.LastName
                Patronimic = t.Patronimic
                RegisterDate = t.RegisterDate
                IsAdmin = t.IsAdmin
                AvatarGUID = t.AvatarGUID
                AvatarExtension = t.AvatarExtension
            End Using
        End Sub
        Sub New(ByVal vUserName As String, ByVal vFirstName As String, ByVal vLastName As String, ByVal vPatronimic As String)
            Using ta As New PersonsDataSetTableAdapters.PersonsTableAdapter()
                UserName = vUserName
                FirstName = vFirstName
                LastName = vLastName
                Patronimic = vPatronimic
                IsAdmin = False
                ta.InsertPerson(UserName, FirstName, LastName, Patronimic, Person_ID)
            End Using
        End Sub
#End Region
#Region "Methods"

        Sub DeclineRequest(ByVal vRequest_ID As Integer, ByVal vReason As String, ByVal p As Page)
            Using ta As New ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter()
                Dim r = ta.GetDataByID(vRequest_ID).Item(0)
                r.BeginEdit()
                r.IsDeclined = True
                r.Comment = vReason
                r.EndEdit()
                ta.Update(r)



                Dim vTitle As String
                If r.IsPubMed_IDNull = False Then
                    vTitle = IIf(r.PubMed_ID > 0, BLL.PubMed.GetTitleByID_internal(r.PubMed_ID), r.Title)
                Else
                    vTitle = r.Title
                End If

                'send message
                Dim recipients = New List(Of Integer)
                recipients.Add(r.Person_ID)
                If recipients.Count > 0 Then
                    BLL.MessageHelper.Send(Nothing, recipients, "Your request was declined", String.Format("Your request <b>""{0}""</b> was declined for following reason: ""{1}""", BLL.Common.FormAnchor(r.URL, vTitle), vReason), p)
                End If
            End Using


        End Sub

        Public Sub DeleteImageAzure()
            If Not String.IsNullOrEmpty(AvatarGUID) Then
                Dim storage As New AzureStorage()
                storage.DeleteImage(String.Format("{0}{1}.{2}", AvatarGUID, "_50", AvatarExtension))
                storage.DeleteImage(String.Format("{0}{1}.{2}", AvatarGUID, "_200", AvatarExtension))
            End If

            'update tables and properties
            Using ta As New PersonsDataSetTableAdapters.PersonsTableAdapter()
                Dim r = ta.GetDataByID(Person_ID).Item(0)
                r.BeginEdit()
                r.AvatarGUID = Nothing
                r.AvatarExtension = Nothing
                r.EndEdit()
                ta.Update(r)
            End Using

        End Sub

        <ObsoleteAttribute("This method is obsolete. Call AzureStorage and update person instead.", True)>
        Public Sub DeleteImage(server As System.Web.HttpServerUtility)
            If HasImage Then
                Dim path50, path200 As String
                path50 = server.MapPath(Image50)
                path200 = server.MapPath(Image200)
                If File.Exists(path50) Then
                    File.Delete(path50)
                End If
                If File.Exists(path200) Then
                    File.Delete(path200)
                End If


                Using ta As New PersonsDataSetTableAdapters.PersonsTableAdapter()
                    Dim r = ta.GetDataByID(Person_ID).Item(0)
                    r.BeginEdit()

                    r.AvatarGUID = Nothing
                    r.AvatarExtension = Nothing

                    r.EndEdit()
                    ta.Update(r)
                End Using

            End If
        End Sub



        Sub UpdatePersonImage(guid As String, extension As String)
            If HasImage Then
                DeleteImageAzure()
            End If

            Using ta As New PersonsDataSetTableAdapters.PersonsTableAdapter()
                Dim r = ta.GetDataByID(Person_ID).Item(0)
                r.BeginEdit()

                r.AvatarGUID = guid
                r.AvatarExtension = extension

                r.EndEdit()
                ta.Update(r)
            End Using

        End Sub
        Sub Save()
            Using ta As New PersonsDataSetTableAdapters.PersonsTableAdapter()
                Dim r = ta.GetDataByID(Person_ID).Item(0)
                r.BeginEdit()

                r.FirstName = FirstName
                r.LastName = LastName
                r.Patronimic = Patronimic

                r.EndEdit()
                ta.Update(r)
            End Using


        End Sub
#End Region
    End Class
End Namespace
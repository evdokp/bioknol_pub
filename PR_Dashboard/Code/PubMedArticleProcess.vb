﻿

Namespace BLL
    Public Class PubMedArticleProcess
        <ObsoleteAttribute("This method is obsolete. Call ProcessPMIDWeb instead.", True)> 
        Public Shared Sub ProcessPMID(ByVal vPMID As Integer, ByVal DoReprocess As Boolean)
            'Check if there's such article already
            Dim vReprocess As Boolean
            Using ta As New ArticlesPubmedDataSetTableAdapters.ArticlesPubmedTableAdapter()
                If ta.GetCountByPubmedID(vPMID) = 0 Then
                    'process new
                    vReprocess = False
                Else
                    'reprocess old
                    vReprocess = True
                End If
            End Using
            Dim serv As New wref_Pubmed_eFetch.eFetchPubmedService
            Dim req As New wref_Pubmed_eFetch.eFetchRequest
            Dim res As wref_Pubmed_eFetch.eFetchResult

            Try
                ' call NCBI EInfo utility
                req.id = vPMID
                res = serv.run_eFetch(req)
                If vReprocess = True Then
                    If DoReprocess = True Then
                        ProcessMedlineCitation(res.PubmedArticleSet(0).MedlineCitation, vReprocess)
                    End If
                Else
                    ProcessMedlineCitation(res.PubmedArticleSet(0).MedlineCitation, vReprocess)
                End If
            Catch err As Exception
                Dim er_ws As New wref_LogError_WS.LogError_WS
                er_ws.LogError_WS_func("ProcessPMID " & vPMID & ": " & err.Message, 1000, 1000)
            End Try

            ' er_ws.LogError_WS_func("-- ProcessPMID - end", 1000, 1000)

        End Sub
        Private Shared Function ProcessJournal(ByVal Journal As BioWS.wref_Pubmed_eFetch.JournalType, ByVal MedlineJournalInfo As BioWS.wref_Pubmed_eFetch.MedlineJournalInfoType) As Integer
            Using ta As New ArticlesPubmedDataSetTableAdapters.pmJournalsTableAdapter()
                Dim t = ta.GetDataByTitleAndISO(Journal.Title, IIf(Journal.ISOAbbreviation Is Nothing, String.Empty, Journal.ISOAbbreviation))
                Dim sCountry As String
                If MedlineJournalInfo.Country IsNot Nothing Then
                    sCountry = MedlineJournalInfo.Country
                    sCountry = sCountry.ToUpper()
                Else
                    sCountry = String.Empty
                End If


                If t.Count = 0 Then
                    Dim i As Integer
                    ta.InsertJournal(Journal.Title, IIf(Journal.ISOAbbreviation Is Nothing, String.Empty, Journal.ISOAbbreviation), sCountry, i)
                    Return i
                Else
                    Dim r = t.Item(0)
                    'r.BeginEdit()
                    'r.Country = sCountry
                    'r.EndEdit()
                    'ta.Update(r)
                    Return r.Journal_ID
                End If
            End Using
        End Function
        Private Shared Sub ProcessAuthorsList(ByVal vPMID As Integer, ByVal AuthorsList As BioWS.wref_Pubmed_eFetch.AuthorListType)
            Using ta As New ArticlesPubmedDataSetTableAdapters.LinksArticlesToAuthorsTableAdapter()
                ta.DeleteByPubmedID(vPMID)
                If AuthorsList IsNot Nothing Then
                    Dim AuthorsListAuthor = AuthorsList.Author
                    Dim authorsOrder As Integer? = 1
                    For Each Author In AuthorsListAuthor
                        Dim Author_ID As Integer = ProcessAuthor(Author)
                        ta.Insert(vPMID, Author_ID, authorsOrder)
                        authorsOrder = authorsOrder + 1
                    Next
                End If
            End Using
        End Sub
        Private Shared Function ProcessAuthor(ByVal Author As BioWS.wref_Pubmed_eFetch.AuthorType)
            Dim last, fore, initials, full As String

            If Author.Items(0) IsNot Nothing Then
                last = Author.Items(0)
            End If
            If Author.Items.Count > 1 Then
                If Author.Items(1) IsNot Nothing Then
                    fore = Author.Items(1)
                End If
                If Author.Items.Count > 2 Then
                    If Author.Items(2) IsNot Nothing Then
                        initials = Author.Items(2)
                    End If
                End If
            End If

            full = fore & " " & last & " " & initials

            Using ta As New ArticlesPubmedDataSetTableAdapters.pmAuthorsTableAdapter()
                Dim t = ta.GetDataByLastForeInitials(last, fore, initials)
                If t.Count = 0 Then
                    Dim i As Integer
                    ta.InsertAuthor(last, fore, initials, full, i)
                    Return i
                Else
                    Return t.Item(0).Author_ID
                End If
            End Using
        End Function
        Private Shared Sub ProcessMeshHeadingList(ByVal vPMID As Integer, ByVal MeshHeadingList As BioWS.wref_Pubmed_eFetch.MeshHeadingType())
            Using ta As New ArticlesPubmedDataSetTableAdapters.LinksArticlesToMeSHTableAdapter()
                ta.DeleteByPubmedID(vPMID)
                If MeshHeadingList IsNot Nothing Then
                    For Each MeshHeading In MeshHeadingList
                        Dim MeshID As Integer = ProcessMeshHeading(MeshHeading)
                        ta.Insert(vPMID, MeshID)
                    Next
                End If
            End Using
        End Sub
        Private Shared Function ProcessMeshHeading(ByVal MeshHeading As BioWS.wref_Pubmed_eFetch.MeshHeadingType)
            Using ta As New ArticlesPubmedDataSetTableAdapters.pmMeSHTableAdapter()
                Dim t = ta.GetDataByHeading(MeshHeading.DescriptorName.Value)
                If t.Count = 0 Then
                    Dim i As Integer
                    ta.InsertMeSH(MeshHeading.DescriptorName.Value, i)
                    Return i
                Else
                    Return t.Item(0).MeSH_ID
                End If
            End Using
        End Function
        Private Shared Function GetMonth(ByVal vMonth As String) As Integer
            Dim i As Integer
            If Integer.TryParse(vMonth, i) Then
                Return i
            Else
                Select Case vMonth
                    Case "Jan"
                        Return 1
                    Case "Feb"
                        Return 2
                    Case "Mar"
                        Return 3
                    Case "Apr"
                        Return 4
                    Case "May"
                        Return 5
                    Case "Jun"
                        Return 6
                    Case "Jul"
                        Return 7
                    Case "Aug"
                        Return 8
                    Case "Sep"
                        Return 9
                    Case "Oct"
                        Return 10
                    Case "Nov"
                        Return 11
                    Case "Dec"
                        Return 12
                End Select
            End If
        End Function

        <ObsoleteAttribute("This method is obsolete. Call ??? instead.", True)> 
        Private Shared Function ProcessMedlineCitation(ByVal MedlineCitation As BioWS.wref_Pubmed_eFetch.MedlineCitationType, ByVal vReprocess As Boolean)
            ' Dim er_ws As New wref_LogError_WS.LogError_WS
            ' er_ws.LogError_WS_func("-- ProcessMedlineCitation - start", 1000, 1000)

            Dim Article = MedlineCitation.Article
            Dim Journal = Article.Journal
            Dim MedlineJournalInfo = MedlineCitation.MedlineJournalInfo
            'process items required for insert
            Dim Journal_ID As Integer
            Journal_ID = ProcessJournal(Journal, MedlineJournalInfo)

            'do insert | update
            Using ta As New ArticlesPubmedDataSetTableAdapters.ArticlesPubmedTableAdapter()

                Dim IssueYear As Integer?
                If Journal.JournalIssue IsNot Nothing Then
                    If Journal.JournalIssue.PubDate IsNot Nothing Then
                        If Journal.JournalIssue.PubDate.Items(0) IsNot Nothing Then
                            IssueYear = CInt(Regex.Match(Journal.JournalIssue.PubDate.Items(0), "\d\d\d\d").Value)
                        End If
                    End If
                End If

                Dim AbstractText As String = String.Empty
                If Article.Abstract IsNot Nothing Then
                    AbstractText = Article.Abstract.AbstractText
                End If
                Dim DateCreated, DateRevised, DateCompleted As Date
                If MedlineCitation.DateCreated IsNot Nothing Then
                    DateCreated = New Date(MedlineCitation.DateCreated.Year, GetMonth(MedlineCitation.DateCreated.Month), MedlineCitation.DateCreated.Day)
                Else
                    DateCreated = Nothing
                End If
                If MedlineCitation.DateRevised IsNot Nothing Then
                    DateRevised = New Date(MedlineCitation.DateRevised.Year, GetMonth(MedlineCitation.DateRevised.Month), MedlineCitation.DateRevised.Day)
                Else
                    DateRevised = Nothing
                End If
                If MedlineCitation.DateCompleted IsNot Nothing Then
                    DateCompleted = New Date(MedlineCitation.DateCompleted.Year, GetMonth(MedlineCitation.DateCompleted.Month), MedlineCitation.DateCompleted.Day)
                Else
                    DateCompleted = Nothing
                End If

                If vReprocess = False Then
                    ta.Insert(MedlineCitation.PMID, _
                              Article.ArticleTitle, _
                              Now(), _
                              AbstractText, _
                              DateCreated, _
                              DateRevised, _
                              DateCompleted, _
                              Journal_ID, _
                              IssueYear)
                Else
                    ta.UpdateQuery(Article.ArticleTitle, _
                                          Now(), _
                                          AbstractText, _
                                           DateCreated, _
                                        DateRevised, _
                                        DateCompleted, _
                                          Journal_ID, _
                                          IssueYear, _
                                          MedlineCitation.PMID)
                End If
            End Using




            ProcessAuthorsList(MedlineCitation.PMID, Article.AuthorList)
            ProcessMeshHeadingList(MedlineCitation.PMID, MedlineCitation.MeshHeadingList)

            ' er_ws.LogError_WS_func("-- ProcessMedlineCitation - end", 1000, 1000)
            Return True
        End Function
    End Class
End Namespace


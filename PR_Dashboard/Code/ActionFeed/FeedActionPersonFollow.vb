﻿Namespace BLL.Feed
    Public Class FeedActionPersonFollow
        Implements IFeedAction

        Public Property FollowedPersonID() As Integer


        Sub New(vfollowedpersonid As Integer)
            FollowedPersonID = vfollowedpersonid
        End Sub
        Sub New()

        End Sub

        Public Sub Save(ByVal PersonID As Integer)
            Using activitylogTa As New PersonsDataSetTableAdapters.ActivityLogTableAdapter()
                Dim s As String

                s = String.Format("now follows [P:{0}]", FollowedPersonID)

                activitylogTa.Insert(Now, PersonID, s, FeedHelper.ActionTypes.PersonFollow)
            End Using
        End Sub

        Public Function GetHTML(actionstring As String, actiontime As Date) As String Implements IFeedAction.GetHTML
            Return FeedHelper.RendexActionItem(actionstring, actiontime, FeedHelper.ActionTypes.PersonFollow)
        End Function
    End Class
End Namespace


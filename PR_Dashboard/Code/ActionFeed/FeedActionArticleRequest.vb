﻿Namespace BLL.Feed
    Public Class FeedActionArticleRequest
        Implements IFeedAction

        Public Property PubmedID() As Integer
        Public Property DOI() As String
        Public Property URL() As String
        Public Property Title() As String


        Public Function GetHTML(actionstring As String, actiontime As Date) As String Implements IFeedAction.GetHTML
            Return FeedHelper.RendexActionItem(actionstring, actiontime, FeedHelper.ActionTypes.ArticleRequest)
        End Function

        Sub New()

        End Sub

        Sub New(vpmid As Integer, vdoi As String, vurl As String, vtitle As String)
            PubmedID = vpmid
            DOI = vdoi
            URL = vurl
            Title = vtitle
        End Sub

        Public Sub Save(ByVal PersonID As Integer)
            Using activitylogTa As New PersonsDataSetTableAdapters.ActivityLogTableAdapter()
                Dim s As String
                s = "requested article "
                If PubmedID > 0 Then
                    s = String.Format("{0}[PM:{1}]", s, PubmedID)
                Else
                    If DOI.Length > 2 Then
                        s = String.Format("{0}[DOI:{1}]", s, DOI)
                    Else
                        s = String.Format("{0}[URL:{1}:{2}]", s, HttpUtility.UrlEncode(URL), Title)
                    End If
                End If
                activitylogTa.Insert(Now, PersonID, s, FeedHelper.ActionTypes.ArticleRequest)
            End Using
        End Sub

    End Class
End Namespace


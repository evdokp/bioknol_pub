﻿Namespace BLL.Feed
    Public Class FeedActionGroupParticipation
        Implements IFeedAction


        Public Property GroupID() As Integer
        Public Property StartDT() As DateTime
        Public Property EndDT() As DateTime

        Public Function GetHTML(actionstring As String, actiontime As Date) As String Implements IFeedAction.GetHTML
            Return FeedHelper.RendexActionItem(actionstring, actiontime, FeedHelper.ActionTypes.GroupParticipation)
        End Function


        Sub New(vgroupid As Integer, vstartdt As DateTime, venddt As DateTime)
            GroupID = vgroupid
            StartDT = vstartdt
            EndDT = venddt
        End Sub


        Sub New()

        End Sub

        Public Sub Save(ByVal PersonID As Integer)
            Using activitylogTa As New PersonsDataSetTableAdapters.ActivityLogTableAdapter()
                Dim s As String
                s = "changed group participation info:<br/>"
                If StartDT = BLL.Common.constants.vNullDate Then
                    ' deleted
                    s = s & "canceled membership in [G:" & GroupID & "]"
                Else
                    s = s & "is member of [G:" & GroupID & "]"
                    s = s & " since " & StartDT.ToString("dd.MM.yyyy")
                    If EndDT = BLL.Common.constants.vNullDate Then
                        s = s & " until present time"
                    Else
                        s = s & " till " & EndDT.ToString("dd.MM.yyyy")
                    End If
                End If
                activitylogTa.Insert(Now, PersonID, s, FeedHelper.ActionTypes.GroupParticipation)
            End Using
        End Sub



    End Class
End Namespace


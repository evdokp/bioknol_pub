﻿Namespace BLL.Feed
    Public Class FeedActionGroupUnFollow
        Implements IFeedAction

        Public Property FollowedGroupID() As Integer


        Sub New(vfollowedgroupid As Integer)
            FollowedGroupID = vfollowedgroupid
        End Sub
        Sub New()

        End Sub

        Public Sub Save(ByVal PersonID As Integer)
            Using activitylogTa As New PersonsDataSetTableAdapters.ActivityLogTableAdapter()
                Dim s As String

                s = String.Format("no longer follows [G:{0}]", FollowedGroupID)

                activitylogTa.Insert(Now, PersonID, s, FeedHelper.ActionTypes.GroupUnFollow)
            End Using
        End Sub

        Public Function GetHTML(actionstring As String, actiontime As Date) As String Implements IFeedAction.GetHTML
            Return FeedHelper.RendexActionItem(actionstring, actiontime, FeedHelper.ActionTypes.GroupUnFollow)
        End Function
    End Class
End Namespace


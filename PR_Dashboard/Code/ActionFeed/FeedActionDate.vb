﻿Namespace BLL.Feed
    Public Class FeedActionDate
        Implements IFeedAction
        Public Function GetHTML(actionstring As String, actiontime As Date) As String Implements IFeedAction.GetHTML
            Dim s As String
            s = "<div class=""feed_date"">"
            s = s + actiontime.ToString("D")
            s = s + "</div>"
            Return s
        End Function
    End Class
End Namespace


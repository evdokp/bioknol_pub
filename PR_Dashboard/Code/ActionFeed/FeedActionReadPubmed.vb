﻿Namespace BLL.Feed
    Public Class FeedActionReadPubmed
        Implements IFeedAction
        Public Function GetHTML(actionstring As String, actiontime As Date) As String Implements IFeedAction.GetHTML

            Return FeedHelper.RendexActionItem(actionstring, actiontime, FeedHelper.ActionTypes.PubmedView)

          
        End Function
    End Class
End Namespace


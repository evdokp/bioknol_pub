﻿Imports System.Xml.Serialization


Namespace BLL.Feed

    <Serializable()> _
    Public Class FeedActionRecommendation
        Implements IFeedAction

        Public Property PubmedID() As Integer
        Public Property DOI() As String
        Public Property URL() As String
        Public Property Title() As String
        Public Property Persons() As List(Of Integer)

        Private Property ActionDateTime() As DateTime
        Private Property PersonID() As Integer



        Public Function GetHTML(actionstring As String, actiontime As Date) As String Implements IFeedAction.GetHTML
            Return FeedHelper.RendexActionItem(actionstring, actiontime, FeedHelper.ActionTypes.ArticleRecommendation)
        End Function

        Sub New()

        End Sub

        Sub New(vpmid As Integer, vdoi As String, vurl As String, vtitle As String)
            PubmedID = vpmid
            DOI = vdoi
            URL = vurl
            Title = vtitle
            Persons = New List(Of Integer)
        End Sub

        Public Sub Save(ByVal vPersonID As Integer)
            Using activitylogTa As New PersonsDataSetTableAdapters.ActivityLogTableAdapter()
                Dim s As String
                s = "recommeneded article "
                If PubmedID > 0 Then
                    s = String.Format("{0}[PM:{1}]", s, PubmedID)
                Else
                    If DOI.Length > 2 Then
                        s = String.Format("{0}[DOI:{1}]", s, DOI)
                    Else
                        s = String.Format("{0}[URL:{1}:{2}]", s, HttpUtility.UrlEncode(URL), Title)
                    End If
                End If
                ActionDateTime = Now
                PersonID = vPersonID
                activitylogTa.Insert(ActionDateTime, PersonID, s, FeedHelper.ActionTypes.ArticleRecommendation)
            End Using
        End Sub

        Public Sub Update()
            Using activitylogTa As New PersonsDataSetTableAdapters.ActivityLogTableAdapter()
                Dim t = activitylogTa.GetDataByAllParams(ActionDateTime, PersonID, FeedHelper.ActionTypes.ArticleRecommendation)

                If t.Count > 0 Then
                    Dim r = t(0)
                    activitylogTa.UpdateAction(r.Action & GetPersons(), ActionDateTime, PersonID, FeedHelper.ActionTypes.ArticleRecommendation)
                End If
            End Using
        End Sub

        Private Function GetPersons() As String
            Dim s As String = " to "
            Dim posts As String
            Dim maxcount As Integer = 10
            If Persons.Count < maxcount Then
                maxcount = Persons.Count
            Else
                posts = "<br/>and others"
            End If
            For index = 0 To maxcount - 1
                If index < Persons.Count - 1 Then
                    s = String.Format("{0}[P:{1}]<br/>", s, Persons(index))
                Else
                    s = String.Format("{0}[P:{1}]", s, Persons(index))
                End If
            Next
            Return s & posts
        End Function

    End Class
End Namespace


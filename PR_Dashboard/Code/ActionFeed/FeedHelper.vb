﻿
Namespace BLL.Feed
    Public Class FeedHelper
        Public Enum ActionTypes
            ActionDate = 0
            Register = 1
            PubmedView = 2
            DOIView = 3
            WidgetOn = 4
            WidgetOff = 5
            ArticleRequest = 6
            ArticleOffer = 7
            GroupParticipation = 8
            PersonFollow = 9
            PersonUnFollow = 10
            GroupFollow = 11
            GroupUnFollow = 12
            ArticleRecommendation = 13
        End Enum
        Public Shared Function RenderAction(vAction As String, vActionTime As DateTime, vActionType As Integer) As String
            Dim actionType As ActionTypes = DirectCast(vActionType, ActionTypes)
            Dim action As IFeedAction
            Select Case actionType
                Case ActionTypes.ActionDate
                    action = New FeedActionDate
                Case ActionTypes.DOIView
                    action = New FeedActionReadDOI
                Case ActionTypes.PubmedView
                    action = New FeedActionReadPubmed
                Case ActionTypes.Register
                    action = New FeedActionRegister
                Case ActionTypes.WidgetOff
                    action = New FeedActionWidgetOff
                Case ActionTypes.WidgetOn
                    action = New FeedActionWidgetOn
                Case ActionTypes.ArticleRequest
                    action = New FeedActionRecommendation
                Case ActionTypes.ArticleOffer
                    action = New FeedActionArticleOffer
                Case ActionTypes.GroupParticipation
                    action = New FeedActionGroupParticipation
                Case ActionTypes.PersonFollow
                    action = New FeedActionPersonFollow
                Case ActionTypes.PersonUnFollow
                    action = New FeedActionPersonUnFollow
                Case ActionTypes.GroupFollow
                    action = New FeedActionGroupFollow
                Case ActionTypes.GroupUnFollow
                    action = New FeedActionGroupUnFollow
                Case ActionTypes.ArticleRecommendation
                    action = New FeedActionRecommendation
                Case Else
                    Return ""
            End Select
            Return action.GetHTML(vAction, vActionTime)
        End Function
        Public Shared Function RendexActionItem(actionstring As String, actiontime As DateTime, actiontype As ActionTypes) As String
            Dim s As String
            s = String.Format("<div class=""feed_time"">{0:HH:mm}</div><div class=""{1}"">{2}</div><div class=""clear""></div>",
                    actiontime,
                    GetItemCSS(actiontype),
                    RenderActionItemContent(actionstring))
            Return s
        End Function
        Public Shared Function RendexActionItem(actiontime As DateTime, actiontype As ActionTypes, actioncontent As String) As String
            Dim s As String
            s = String.Format("<div class=""feed_time"">{0:HH:mm}</div><div class=""{1}"">{2}</div><div class=""clear""></div>",
                    actiontime,
                    GetItemCSS(actiontype),
                    actioncontent)
            Return s
        End Function
        Private Shared Function RenderActionItemContent(actionstring As String) As String
            Dim mc As MatchCollection
            mc = Regex.Matches(actionstring, "(?<=\[).*?(?=\])")
            Dim replacedict As New Dictionary(Of String, String)
            For Each m As Match In mc
                If Not replacedict.ContainsKey(m.Value) Then
                    replacedict.Add(m.Value, GetReplacement(m.Value))
                End If
            Next
            For Each key As String In replacedict.Keys
                actionstring = actionstring.Replace(String.Format("[{0}]", key), replacedict(key))
            Next
            Return actionstring
        End Function

        Private Shared Function GetSpan(ByVal classname As String, ByVal content As String) As String
            Return String.Format("<span class=""{0}"">{1}</span>", classname, content)
        End Function

        Private Shared Function GetReplacement(input As String) As String
            Dim res As String
            Dim sa As String() = input.Split(":")
            Select Case sa(0)
                Case "W"
                    Using wta As New WidgetsDataSetTableAdapters.WidgetsTableAdapter()
                        Dim wr = wta.GetDataByID(sa(1))(0)
                        res = String.Format("<a href=""Widget/ViewWidget.aspx?WidgetID={0}"">{1}</a>", wr.Widget_ID, wr.Title)
                    End Using
                Case "PM"
                    Dim pmid As Integer
                    If (Integer.TryParse(sa(1), pmid)) Then
                        Dim pm As New BLL.PubMed.PubMedArticle(pmid)
                        res = "<div>"


                        If pm.PubYear > 0 Then
                            res = res & GetSpan("feed_small", pm.PubYear & ", " & pm.Journal) & ", "
                        Else
                            res = res & GetSpan("feed_small", pm.Journal) & ", "
                        End If
                        res = res & GetSpan("feed_small", pm.AuthorsStringShort) & "<br/>"
                        res = res & String.Format("<a href=""../ViewInfoPM.aspx?Pubmed_ID={0}"">{1}</a>", pm.Pubmed_ID, pm.Title) & "<br/>"




                        res = res & "</div>"

                    End If
                Case "DOI"
                    Dim dt As DataTable = BLL.SQLHelper.GetDT_AllStrings("SELECT TOP 1 [Title]   FROM [dbo].[ViewsLog]   where DOI = '" & sa(1) & "'")
                    If (dt.Rows.Count > 0) Then
                        Dim viewsr = dt.Rows(0)
                        res = String.Format("<a href=""http://dx.doi.org/{0}"">{1}</a>", input.Substring(4, input.Length - 4), viewsr(0))
                    End If
                Case "URL"
                    res = String.Format("<a href=""{0}"">{1}</a>", HttpUtility.UrlDecode(sa(1)), sa(2))
                Case "OFFER"
                    Using offersta As New ArticlesDataSetTableAdapters.ArticleOffersTableAdapter()
                        Dim offersr = offersta.GetDataByID(sa(1))(0)
                        res = String.Format("<a href=""{0}"">{1}</a>", "../Download.ashx?vFileName=" & HttpUtility.UrlEncode(offersr.FileName) & "&vOriginalName=" & HttpUtility.UrlEncode(offersr.OriginalName), offersr.Title)
                    End Using
                Case "G"
                    Using groupsta As New PersonsDataSetTableAdapters.GroupsTableAdapter()
                        Dim groupr = groupsta.GetDataByID(CInt(sa(1)))(0)
                        res = String.Format("<a href=""ViewGroup.aspx?GroupID={0}"">{1}</a>", groupr.Group_ID, groupr.Title)
                    End Using
                Case "P"
                    Dim pers As New BLL.Person(CInt(sa(1)))
                    res = String.Format("<a href=""ViewPerson.aspx?PersonID={0}"">{1}</a>", pers.Person_ID, pers.PageHeader)
                Case Else
                    res = ""
            End Select
            Return res
        End Function
        Private Shared Function GetItemCSS(actiontype As ActionTypes) As String
            Select Case actiontype
                Case ActionTypes.ActionDate
                    Return ""
                Case ActionTypes.DOIView
                    Return "feed_doi"
                Case ActionTypes.PubmedView
                    Return "feed_pm"
                Case ActionTypes.Register
                    Return "feed_profile"
                Case ActionTypes.WidgetOn
                    Return "feed_widget"
                Case ActionTypes.WidgetOff
                    Return "feed_widget"
                Case ActionTypes.ArticleRequest
                    Return "feed_articlereq"
                Case ActionTypes.ArticleOffer
                    Return "feed_articleoff"
                Case ActionTypes.GroupParticipation
                    Return "feed_group"
                Case ActionTypes.PersonFollow
                    Return "feed_profile"
                Case ActionTypes.PersonUnFollow
                    Return "feed_profile"
                Case ActionTypes.GroupFollow
                    Return "feed_group"
                Case ActionTypes.GroupUnFollow
                    Return "feed_group"
                Case ActionTypes.ArticleRecommendation
                    Return "feed_articlerec"
                Case Else
                    Return ""
            End Select
        End Function
    End Class
End Namespace



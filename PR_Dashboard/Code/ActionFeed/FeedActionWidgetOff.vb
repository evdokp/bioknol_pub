﻿Namespace BLL.Feed
    Public Class FeedActionWidgetOff
        Implements IFeedAction
        Public Property WidgetID() As Integer
        Public Function GetHTML(actionstring As String, actiontime As Date) As String Implements IFeedAction.GetHTML
            Return FeedHelper.RendexActionItem(actionstring, actiontime, FeedHelper.ActionTypes.WidgetOff)
        End Function
        Public Sub Save(ByVal PersonID As Integer)
            Using activitylogTa As New PersonsDataSetTableAdapters.ActivityLogTableAdapter()
                Dim s As String
                s = String.Format("turned <span class=""feed_off"">off</span> widget [W:{0}]", WidgetID)
                activitylogTa.Insert(Now, PersonID, s, FeedHelper.ActionTypes.WidgetOff)
            End Using
        End Sub
    End Class
End Namespace


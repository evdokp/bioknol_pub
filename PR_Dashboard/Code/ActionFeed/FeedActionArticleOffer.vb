﻿Namespace BLL.Feed
    Public Class FeedActionArticleOffer
        Implements IFeedAction

        Public Property PubmedID() As Integer
        Public Property OfferID() As Integer

        Sub New(vpmid As Integer, vofferid As Integer)
            PubmedID = vpmid
            OfferID = vofferid
        End Sub
        Sub New()

        End Sub
        Public Sub Save(ByVal PersonID As Integer)
            If PersonID > 0 Then
                Using activitylogTa As New PersonsDataSetTableAdapters.ActivityLogTableAdapter()
                    Dim s As String
                    s = "full-text for the requested article "
                    If PubmedID > 0 Then
                        s = String.Format("{0}[PM:{1}]", s, PubmedID)
                    Else
                        s = String.Format("{0}[OFFER:{1}]", s, OfferID)
                    End If
                    s = s & " is now available"
                    activitylogTa.Insert(Now, PersonID, s, FeedHelper.ActionTypes.ArticleOffer)
                End Using
            End If
        End Sub

        Public Function GetHTML(actionstring As String, actiontime As Date) As String Implements IFeedAction.GetHTML
            Return FeedHelper.RendexActionItem(actionstring, actiontime, FeedHelper.ActionTypes.ArticleOffer)
        End Function

    End Class

End Namespace

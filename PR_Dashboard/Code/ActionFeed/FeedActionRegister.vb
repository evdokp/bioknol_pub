﻿Namespace BLL.Feed
    Public Class FeedActionRegister
        Implements IFeedAction
        Public Function GetHTML(actionstring As String, actiontime As Date) As String Implements IFeedAction.GetHTML
            Return FeedHelper.RendexActionItem(actiontime,
                         FeedHelper.ActionTypes.Register,
                         "Registered")
        End Function
    End Class
End Namespace


﻿Imports Microsoft.VisualBasic


Imports AjaxControlToolkit.HTMLEditor
Imports AjaxControlToolkit.HTMLEditor.ToolbarButton

Namespace MyControls
    Public Class CustomEditor
        Inherits Editor
        Protected Overloads Overrides Sub FillTopToolbar()

            TopToolbar.Buttons.Add(New Cut())
            TopToolbar.Buttons.Add(New PasteText())
            TopToolbar.Buttons.Add(New PasteWord())
            TopToolbar.Buttons.Add(New HorizontalSeparator())
            TopToolbar.Buttons.Add(New Redo())
            TopToolbar.Buttons.Add(New Undo())
            TopToolbar.Buttons.Add(New HorizontalSeparator())
            TopToolbar.Buttons.Add(New Bold())
            TopToolbar.Buttons.Add(New Italic())
            TopToolbar.Buttons.Add(New Underline())
            TopToolbar.Buttons.Add(New StrikeThrough())
            TopToolbar.Buttons.Add(New SubScript())
            TopToolbar.Buttons.Add(New SuperScript())
            TopToolbar.Buttons.Add(New HorizontalSeparator())
            TopToolbar.Buttons.Add(New JustifyCenter())
            TopToolbar.Buttons.Add(New JustifyLeft())
            TopToolbar.Buttons.Add(New JustifyRight())
            TopToolbar.Buttons.Add(New HorizontalSeparator())
            TopToolbar.Buttons.Add(New BulletedList())
            TopToolbar.Buttons.Add(New OrderedList())
            TopToolbar.Buttons.Add(New HorizontalSeparator())
            TopToolbar.Buttons.Add(New IncreaseIndent())
            TopToolbar.Buttons.Add(New DecreaseIndent())




            'TopToolbar.Buttons.Add(New AjaxControlToolkit.HTMLEditor.ToolbarButton.Italic())
            'TopToolbar.Buttons.Add(New AjaxControlToolkit.HTMLEditor.ToolbarButton.Italic())
            'TopToolbar.Buttons.Add(New AjaxControlToolkit.HTMLEditor.ToolbarButton.Italic())
            'TopToolbar.Buttons.Add(New AjaxControlToolkit.HTMLEditor.ToolbarButton.Italic())
            'TopToolbar.Buttons.Add(New AjaxControlToolkit.HTMLEditor.ToolbarButton.Italic())
            'TopToolbar.Buttons.Add(New AjaxControlToolkit.HTMLEditor.ToolbarButton.Italic())
            'TopToolbar.Buttons.Add(New AjaxControlToolkit.HTMLEditor.ToolbarButton.Italic())
            'TopToolbar.Buttons.Add(New AjaxControlToolkit.HTMLEditor.ToolbarButton.Italic())
            'TopToolbar.Buttons.Add(New AjaxControlToolkit.HTMLEditor.ToolbarButton.Italic())
            'TopToolbar.Buttons.Add(New AjaxControlToolkit.HTMLEditor.ToolbarButton.Italic())


        End Sub

        Protected Overloads Overrides Sub FillBottomToolbar()
            'BottomToolbar.Buttons.Add(New AjaxControlToolkit.HTMLEditor.ToolbarButton.DesignMode())
            'BottomToolbar.Buttons.Add(New AjaxControlToolkit.HTMLEditor.ToolbarButton.PreviewMode())
        End Sub

    End Class
End Namespace
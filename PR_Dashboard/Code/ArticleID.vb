﻿

Namespace BLL
    Public Enum ArticleIDTypes
        Pubmed = 1
        DOI = 2
        URL = 3
    End Enum

    Public Class ArticleID
        Public IDstr As String
        Public IDint As Integer
        Public IDType As ArticleIDTypes


        Sub New(ByVal rawid As String)
            IDstr = rawid
            If rawid.Contains("http://") Then
                IDType = ArticleIDTypes.URL
            ElseIf rawid.Contains("10.") Then
                IDType = ArticleIDTypes.DOI
            ElseIf Integer.TryParse(rawid, IDint) Then
                IDType = ArticleIDTypes.Pubmed
            Else
                Throw New ArgumentException("can't determine article id type")
            End If
        End Sub
    End Class


    

End Namespace


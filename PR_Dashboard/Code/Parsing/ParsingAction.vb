﻿Imports System.IO
Imports System.Drawing
Imports BioWS.BLL.Extensions

Namespace BLL



    Public Class ParsingAction
        Public Enum ParsingActionTypes
            DoNothing = 1
            SaveHTML = 2
            ParseURL = 3
            ParseHTML = 4
        End Enum
        Public Enum Origins
            CheckURLWS = 1
            SaveHTMLWS = 2
        End Enum
        Public Property SpecificPubmedRegex As String
        Public Property SpecificDOIRegex As String
        Public Property ActionType As ParsingActionTypes
        Public Sub New(ByVal vactiontype As ParsingActionTypes)
            ActionType = vactiontype
        End Sub
        Public Sub New(ByVal row As MonitoredSitesDataSet.ParsingRulesRow)
            ActionType = row.ParsingActionID
            SpecificPubmedRegex = row.SpecificPubmedRegex
            SpecificDOIRegex = row.SpecificDOIRegex
        End Sub


        Public Function TryParse(ByVal origin As ParsingAction.Origins, ByVal input As String, ByRef result As ParsingResult)
            Select Case origin
                Case Origins.CheckURLWS
                    TryParseForCheckWS(input, result)
                Case Origins.SaveHTMLWS
                    TryParseForSaveWS(input, result)
            End Select
        End Function


        Private Function TryParseForCheckWS(
            ByVal url As String,
            ByRef parsingResult As ParsingResult) As Boolean
            Select Case ActionType
                Case ParsingActionTypes.DoNothing
                    ' done, default values are ok
                Case ParsingActionTypes.ParseHTML
                    parsingResult.HTMLRequired = True
                Case ParsingActionTypes.ParseURL
                    ParseURL(url, parsingResult.PubmedID, parsingResult.DOI)
                Case ParsingActionTypes.SaveHTML
                    parsingResult.HTMLRequired = True
            End Select
            Return parsingResult.PubmedID.ToBoolean()
        End Function
        Private Function TryParseForSaveWS(ByVal html As String,
            ByRef parsingResult As ParsingResult) As Boolean

            'HTML is always unrequired
            Select Case ActionType
                Case ParsingActionTypes.DoNothing
                    ' done, default values are ok
                Case ParsingActionTypes.ParseHTML
                    ParseHTML(html, parsingResult.PubmedID, parsingResult.DOI)
                Case ParsingActionTypes.ParseURL
                    ' not applicable
                Case ParsingActionTypes.SaveHTML
                    parsingResult.SaveRequired = True
            End Select

            Return parsingResult.PubmedID.ToBoolean()
        End Function

        Private Sub ParseHTML(ByVal html As String, ByRef pubmedID As Integer, ByRef doi As String)
            Dim strPubmedID As String
            If ParsingHelper.TryParse(GetRegexListPubmed(), html, strPubmedID) Then
                Integer.TryParse(strPubmedID, pubmedID)
            End If
            ParsingHelper.TryParse(GetRegexListDOI(), html, doi)
        End Sub
        Private Sub ParseURL(ByVal url As String, ByRef pubmedID As Integer, ByRef doi As String)
            '! try parse pubmed ID
            Dim pmregexes As New List(Of String)
            If SpecificPubmedRegex.Length > 0 Then
                pmregexes.Add(SpecificPubmedRegex)
            End If

            Dim strPubmedID As String
            If ParsingHelper.TryParse(pmregexes, url, strPubmedID) Then
                Integer.TryParse(strPubmedID, pubmedID)
            End If

            '! try parse DOI
            Dim doiregexes As New List(Of String)
            If SpecificDOIRegex.Length > 0 Then
                doiregexes.Add(SpecificDOIRegex)
            End If
            ParsingHelper.TryParse(doiregexes, url, doi)

        End Sub




        Private Function GetRegexListDOI() As List(Of String)
            Return GetRegexList(SpecificDOIRegex, ParsingHelper.CommonDOIRegexes)
        End Function
        Private Function GetRegexListPubmed() As List(Of String)
            Return GetRegexList(SpecificPubmedRegex, ParsingHelper.CommonPubmedRegexes)
        End Function
        Private Function GetRegexList(ByVal specificRegex As String, ByVal defaultList As List(Of String)) As List(Of String)
            Dim result As New List(Of String)
            If specificRegex.IsNullOrEmpty() Then
                result.AddRange(defaultList)
            Else
                result.Add(specificRegex)
            End If
            Return result
        End Function

    End Class
End Namespace


﻿Imports System.Runtime.CompilerServices
Imports System.Web.Script.Serialization
Imports System.Net


Namespace BLL.Extensions.ParsingExtensions

    Module ParsingExtensions

        <Extension()>
        Public Function OneRegExMatchOnly(source As String, ByVal regex As String) As Boolean
            ' Define a regular expression for repeated words.
            Dim rx As New Regex(regex, RegexOptions.IgnoreCase)
            ' Find matches.
            Dim matches As MatchCollection = rx.Matches(source)
            Dim IsMatchOneOnly = matches.Count = 1
            Return IsMatchOneOnly
        End Function




    End Module


End Namespace


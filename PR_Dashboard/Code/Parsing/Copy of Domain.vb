﻿Imports System.Drawing
Imports System.Threading
Imports BioWS.BLL.Extensions
Imports BioWS.BLL.Extensions.Web

Namespace BLL


    Public Class Domain



        Public Property ID As Integer
        Public Property SiteURL As String
        Public Property IsApproved As Boolean
        Public Property AddedDate As DateTime
        Public Property Title As String
        Public Property DefaultParsingActionType As ParsingAction.ParsingActionTypes

        '! non db properties
        Public ReadOnly Property PageHeader As String
            Get
                If Title.Length = 0 Then
                    If SiteURL.Length = 0 Then
                        Return "no title or URL, ID: " & ID
                    Else
                        Return SiteURL
                    End If
                Else
                    Return Title
                End If
            End Get
        End Property
        Public ReadOnly Property URLAlias As String
            Get
                If SiteURL.StartsWith("www.") Then
                    Return SiteURL.Substring(4, SiteURL.Length - 4)
                Else
                    Return "www." & SiteURL
                End If
            End Get
        End Property

        Public ReadOnly Property RecorderedTotalViews As Integer
            Get
                Dim viewsTA As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
                Return viewsTA.GetCountByDomainIDTotalViews(ID)
            End Get
        End Property
        Public ReadOnly Property RecorderedPubmedViews As Integer
            Get
                Dim viewsTA As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
                Return viewsTA.GetCountByDomainIDPubmedViews(ID)
            End Get
        End Property
        Public ReadOnly Property RecorderedDistinctPubmedIDs As Integer
            Get
                Dim viewsTA As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
                Return viewsTA.GetCountByDomainIDdistinctPM(ID)
            End Get
        End Property

        Private _RecorderedFirstViewDateTime As DateTime?
        Public ReadOnly Property RecorderedFirstViewDateTime As DateTime?
            Get
                If _RecorderedFirstViewDateTime Is Nothing Then
                    Dim viewsTA As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
                    _RecorderedFirstViewDateTime = viewsTA.GetMinTimeStampByDomainID(ID)
                End If
                Return _RecorderedFirstViewDateTime
            End Get
        End Property
        Private _RecorderedLastViewDateTime As DateTime?
        Public ReadOnly Property RecorderedLastViewDateTime As DateTime?
            Get
                If _RecorderedLastViewDateTime Is Nothing Then
                    Dim viewsTA As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
                    _RecorderedLastViewDateTime = viewsTA.GetMaxTimeStampByDomainID(ID)
                End If
                Return _RecorderedLastViewDateTime
            End Get
        End Property
        Public ReadOnly Property RecordingDurationDays As Integer
            Get
                If _RecorderedLastViewDateTime.HasValue And _RecorderedFirstViewDateTime.HasValue Then
                    Dim dt1 As Date = _RecorderedFirstViewDateTime.Value.Date
                    Dim dt2 As Date = _RecorderedLastViewDateTime.Value.Date
                    Dim diff As Integer = DateDiff(DateInterval.Day, dt1, dt2) + 1
                    Return diff
                Else
                    Return 0
                End If
            End Get
        End Property
        Public ReadOnly Property AvgViewsPerDayRecordered As Double
            Get
                If RecordingDurationDays = 0 Then
                    Return 0
                Else
                    Return RecorderedTotalViews / RecordingDurationDays
                End If
            End Get
        End Property

        '! parsing properties
        Public Property Determined As Boolean
        Public Property HTMLRequired As Boolean
        Public ReadOnly Property HTMLRequiredInt As Integer
            Get
                Return HTMLRequired.ToInt()
            End Get
        End Property
        Public ReadOnly Property PubmedDetected As Boolean
            Get
                Return PubmedID.ToBoolean()
            End Get
        End Property
        Public ReadOnly Property LogViewRequired As Boolean
            Get
                Return PubmedDetected Or DOI.IsNullOrEmpty.Inverse() Or SavedHTMLGUID.IsNullOrEmpty().Inverse()
            End Get
        End Property
        Public Property PubmedID As Integer
        Public Property DOI As String
        Private Property ParsingRules As List(Of ParsingRule)
        Private Property IncomingURL As String
        Private Property IncomingHTML As String
        Public Property SavedHTMLGUID As String
        Private Property Origin As ParsingAction.Origins
        Public ReadOnly Property EditURL As String
            Get
                Return "~\Account\Admin\EditDomain.aspx?DomainID=" & ID
            End Get
        End Property

#Region "Constructors"
        '! for managing, when we know domain ID
        Sub New(ByVal vID As Integer)
            Dim msTA As New MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter
            Dim msRow = msTA.GetDataByID(vID)(0)
            ID = msRow.ID
            SiteURL = msRow.SiteURL
            IsApproved = msRow.IsApproved
            AddedDate = msRow.AddedDate
            Title = msRow.Title
            DefaultParsingActionType = msRow.DefaultParsingActionID
        End Sub

        '! used in parsing
        Sub New(ByVal server As System.Web.HttpServerUtility, ByVal url As String, ByVal html As String, ByVal vorigin As ParsingAction.Origins)
            Initialize(url)
            If Determined Then
                Dim actions As New List(Of ParsingAction)
                IncomingHTML = html
                IncomingURL = url
                Origin = vorigin
                LoadParsingRules()
                Dim rightRule As ParsingRule = GetRightRule(url)
                If Not rightRule Is Nothing Then
                    actions.Add(rightRule.ParsingAction)
                End If
                '! execute default action
                Dim defaultParsingAction As New ParsingAction(DefaultParsingActionType, String.Empty, String.Empty)
                actions.Add(defaultParsingAction)
                ExecuteActions(server, actions)
            End If
        End Sub

        '! when creating new domain - used in management
        Sub New(ByVal vSiteURL As String, ByVal vTitle As String, ByVal vDefaultParsingAction As Integer)
            Dim domainsTA As New MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter
            SiteURL = vSiteURL
            Title = vTitle
            DefaultParsingActionType = vDefaultParsingAction
            domainsTA.InsertMonitoredSite(SiteURL, False, Title, DefaultParsingActionType, ID)
        End Sub
#End Region
#Region "Methods"
        Public Sub Delete()
            Dim userExclTA As New MonitoredSitesDataSetTableAdapters.MonitoredSitesUserExclusionsTableAdapter
            userExclTA.DeleteByDomainID(ID)
            Dim parsingRulesTA As New MonitoredSitesDataSetTableAdapters.ParsingRulesTableAdapter
            parsingRulesTA.DeleteByDomainID(ID)
            Dim viewsTA As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
            viewsTA.DeleteByDomainID(ID)
            Dim domainsTA As New MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter
            domainsTA.DeleteByID(ID)
            BLL.ParsingHelper.Reload()
        End Sub
        Sub Update()
            Dim msTA As New MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter
            Dim msRow = msTA.GetDataByID(ID)(0)
            msRow.BeginEdit()

            msRow.SiteURL = SiteURL
            msRow.IsApproved = IsApproved
            msRow.Title = Title
            msRow.DefaultParsingActionID = DefaultParsingActionType

            msRow.EndEdit()
            msTA.Update(msRow)

        End Sub
        Private Sub LoadParsingRules()
            Dim prT = ParsingHelper.GetParsingRulesRows(ID)
            ParsingRules = New List(Of ParsingRule)
            For Each prRow As MonitoredSitesDataSet.ParsingRulesRow In prT
                Dim parsingRule As New ParsingRule(prRow)
                ParsingRules.Add(parsingRule)
            Next
        End Sub
        Public Sub Log(ByVal vURL As String, ByVal vPersonID As Integer, ByVal vIP As String, ByVal vTitle As String, ByVal vPluginID As Integer)

            If LogViewRequired Then
                Dim viewsTA As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
                Dim pmid As Integer?

                If PubmedID > 0 Then
                    pmid = PubmedID
                Else
                    pmid = New Integer?()
                End If


                If DOI.IsNullOrEmpty().Inverse() And vTitle.IsNullOrEmpty() Then
                    vTitle = ("http://dx.doi.org/" & DOI).GetHTML().GetTitleTagText()
                End If
                viewsTA.Insert(Now, vURL, SavedHTMLGUID, vPersonID, vIP, vTitle, ID, pmid, vPluginID, DOI)
            End If
        End Sub
        Public Sub ProcessPMID()
            If PubmedID > 0 Then
                Dim t As New Thread(AddressOf BLL.PubMed.ProcessPubmedView)
                t.Start(PubmedID)
            End If
        End Sub
        Private Sub Initialize(ByVal url As String)
            Dim domainRow As MonitoredSitesDataSet.MonitoredSitesRow = ParsingHelper.GetDomainRow(url)
            Determined = domainRow IsNot Nothing
            If Determined Then
                ID = domainRow.ID
                DefaultParsingActionType = domainRow.DefaultParsingActionID
            End If
        End Sub
        Private Function GetRightRule(ByVal url As String) As ParsingRule
            Return ParsingRules.Find(Function(r As ParsingRule) r.IsValid(url))
        End Function
        Private Sub ExecuteActions(ByVal server As System.Web.HttpServerUtility, ByVal actions As List(Of ParsingAction))
            For Each pa As ParsingAction In actions
                If pa.TryParse(server, Origin, IncomingURL, IncomingHTML, HTMLRequired, PubmedID, DOI, SavedHTMLGUID) Then
                    Exit For
                End If
            Next
        End Sub
#End Region
    End Class
End Namespace



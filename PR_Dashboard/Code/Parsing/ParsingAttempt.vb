﻿Imports System.Drawing
Imports System.Threading
Imports BioWS.BLL.Extensions
Imports BioWS.BLL.Extensions.Web

Namespace BLL

    Public Class ParsingResult
        Public Property PubmedID As Integer
        Public Property DOI As String
        Public Property HTMLRequired As Boolean
        Public Property SaveRequired As Boolean

        Public ReadOnly Property HasPubmed As Boolean
            Get
                Return PubmedID.ToBoolean()
            End Get
        End Property
        Public ReadOnly Property HasDOI As Boolean
            Get
                Return DOI.IsNotEmpty()
            End Get
        End Property
    End Class

    Public Class ParsingAttempt
        Public Property DomainMeta As DomainMeta
        Public Property ParsingResult As ParsingResult
        Private Property ParsingRules As List(Of ParsingRule)
        Private Property IncomingURL As String
        Private Property IncomingHTML As String
        Private Property Origin As ParsingAction.Origins

        Public ReadOnly Property LogViewRequired As Boolean
            Get
                Return ParsingResult.HasPubmed Or ParsingResult.HasDOI Or ParsingResult.SaveRequired
            End Get
        End Property
        Public ReadOnly Property HasRulesWithParsing As Boolean
            Get
                Return ParsingRules.Find(Function(r As ParsingRule) r.IsRuleForParsing) IsNot Nothing
            End Get
        End Property


        Sub New(ByVal domainID As Integer, ByVal url As String, ByVal html As String, ByVal vorigin As ParsingAction.Origins)
            DomainMeta = New DomainMeta(domainID)
            If DomainMeta.Determined Then
                Initialize(url, html, vorigin)
            End If
        End Sub

        Sub New(ByVal url As String, ByVal html As String, ByVal vorigin As ParsingAction.Origins)
            DomainMeta = New DomainMeta(url)
            If DomainMeta.Determined Then
                Initialize(url, html, vorigin)
            End If
        End Sub

        Private Sub Initialize(ByVal url As String, ByVal html As String, ByVal vorigin As ParsingAction.Origins)
            ParsingResult = New ParsingResult()
            Dim actions As New List(Of ParsingAction)
            IncomingHTML = html
            IncomingURL = url
            Origin = vorigin
            LoadParsingRules()
            Dim rightRules = GetRightRule(url)
            If rightRules.Count > 0 Then
                For Each Rule In rightRules
                    actions.Add(Rule.ParsingAction)
                Next
            End If
            '! execute default action
            Dim defaultParsingAction As New ParsingAction(DomainMeta.DefaultParsingActionType)
            actions.Add(defaultParsingAction)
            ExecuteActions(actions)
        End Sub
        Private Function GetRightRule(ByVal url As String) As List(Of ParsingRule)
            Return ParsingRules.Where(Function(r As ParsingRule) r.IsValid(url)).ToList
        End Function
        Private Sub ExecuteActions(ByVal actions As List(Of ParsingAction))
            For Each pa As ParsingAction In actions

                If Origin = ParsingAction.Origins.CheckURLWS Then
                    If pa.TryParse(ParsingAction.Origins.CheckURLWS, IncomingURL, ParsingResult) Then
                        Exit For
                    End If
                End If

                If Origin = ParsingAction.Origins.SaveHTMLWS Then
                    If pa.TryParse(ParsingAction.Origins.SaveHTMLWS, IncomingHTML, ParsingResult) Then
                        Exit For
                    End If
                End If

            Next
        End Sub
        Private Sub LoadParsingRules()
            Dim prT = ParsingHelper.GetParsingRulesRows(DomainMeta.ID)
            ParsingRules = New List(Of ParsingRule)
            For Each prRow As MonitoredSitesDataSet.ParsingRulesRow In prT
                Dim parsingRule As New ParsingRule(prRow)
                ParsingRules.Add(parsingRule)
            Next
        End Sub

    End Class


End Namespace



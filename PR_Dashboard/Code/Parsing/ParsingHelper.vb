﻿Imports System.Drawing
Imports System.Linq
Imports BioWS.BLL.Extensions
Imports BioWS.BLL.Extensions.Web


Namespace BLL
    Public Class ParsingHelper


        Private Shared Property DomainsTBL As MonitoredSitesDataSet.MonitoredSitesDataTable
        Private Shared Property ParsingRulesTBL As MonitoredSitesDataSet.ParsingRulesDataTable
        Public Shared Property CommonDOIRegexes As List(Of String)
        Public Shared Property CommonPubmedRegexes As List(Of String)




        Public Shared ReadOnly Property DomainsCount As Integer
            Get
                Return DomainsTBL.Count
            End Get
        End Property
        Public Shared ReadOnly Property ParsingRulesCount As Integer
            Get
                Return ParsingRulesTBL.Count
            End Get
        End Property

        Shared Sub New()
            Reload()
        End Sub

        Shared Sub Reload()
            '! get domains
            Dim msTA As New MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter
            DomainsTBL = msTA.GetAllApproved()

            '! get parsing rules
            Dim prTA As New MonitoredSitesDataSetTableAdapters.ParsingRulesTableAdapter
            ParsingRulesTBL = prTA.GetData()

            '! fill common DOI regexes
            CommonDOIRegexes = New List(Of String)
            CommonDOIRegexes.Add("(10\.(\d)+/(\S)+)\d")

            '! fill common Pubmed regexes
            CommonPubmedRegexes = New List(Of String)

        End Sub
        Shared Function GetParsingRulesRows(ByVal domainID As Integer) As MonitoredSitesDataSet.ParsingRulesRow()
            Return ParsingRulesTBL.Select("MonitoredSiteID=" & domainID)
        End Function
        Shared Function GetDomainRow(ByVal originalURL As String) As MonitoredSitesDataSet.MonitoredSitesRow
            Dim url As String
            url = HttpUtility.UrlDecode(originalURL)
            Dim domainOriginal = Regex.Match(url, "(?<=http\:\/\/).*?(?=(\/|\\|$))").Value
            Dim domainAlias As String = IIf(domainOriginal.StartsWith("www."), domainOriginal.Substring(4, domainOriginal.Length - 4), "www." & domainOriginal)
            Dim foundRows As MonitoredSitesDataSet.MonitoredSitesRow() = DomainsTBL.Select(String.Format("SiteURL like '{0}%' or SiteURL like '{1}%'", domainOriginal, domainAlias))
            If foundRows.Length > 0 Then
                Return foundRows(0)
            Else
                Return Nothing
            End If
        End Function

        Shared Function TryParse(ByVal regexes As List(Of String), ByVal source As String, ByRef result As String) As Boolean
            Dim res As Boolean
            For Each regex As String In regexes
                If TryParse(regex, source, result) Then
                    res = True
                    Exit For
                End If
            Next
            Return res
        End Function
        Private Shared Function TryParse(ByVal regex As String, ByVal source As String, ByRef result As String) As Boolean
            ' Define a regular expression for repeated words.
            Dim rx As New Regex(regex, RegexOptions.IgnoreCase)
            ' Find matches.
            Dim matches As MatchCollection = rx.Matches(source)
            Dim distinctMatches As IEnumerable(Of String) = From m As Match In matches Select m.Value Distinct






            If distinctMatches.Count = 1 Then
                result = distinctMatches(0)
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Sub Log(ByVal vURL As String,
            ByVal vPersonID As Integer,
            ByVal vIP As String,
            ByVal vTitle As String,
            ByVal vPluginID As Integer,
            ByVal vPubmedID As Integer,
            ByVal vDOI As String,
            ByVal vDomainMetaID As Integer,
            ByVal sSaveHTMLGuid As String)

            Using viewsTA As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter()
                Dim pmid As Integer?

                If vPubmedID > 0 Then
                    pmid = vPubmedID
                Else
                    pmid = New Integer?()
                End If


                If vDOI.IsNullOrEmpty().Inverse() And vTitle.IsNullOrEmpty() Then
                    vTitle = ("http://dx.doi.org/" & vDOI).GetHTML().GetTitleTagText()
                End If
                viewsTA.Insert(Now, vURL, sSaveHTMLGuid, vPersonID, vIP, vTitle, vDomainMetaID, pmid, vPluginID, vDOI)
            End Using

        End Sub
    End Class
End Namespace



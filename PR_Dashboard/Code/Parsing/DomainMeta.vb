
Namespace BLL
    Public Class DomainMeta
        Public Property ID As Integer
        Public Property SiteURL As String
        Public Property IsApproved As Boolean
        Public Property AddedDate As DateTime
        Public Property Title As String
        Public Property DefaultParsingActionType As ParsingAction.ParsingActionTypes

        Public ReadOnly Property RecorderedTotalViews As Integer
            Get
                Dim viewsTA As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
                Return viewsTA.GetCountByDomainIDTotalViews(ID)
            End Get
        End Property
        Public ReadOnly Property RecorderedPubmedViews As Integer
            Get
                Dim viewsTA As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
                Return viewsTA.GetCountByDomainIDPubmedViews(ID)
            End Get
        End Property
        Public ReadOnly Property RecorderedDistinctPubmedIDs As Integer
            Get
                Dim viewsTA As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
                Return viewsTA.GetCountByDomainIDdistinctPM(ID)
            End Get
        End Property

        Private _RecorderedFirstViewDateTime As DateTime?
        Public ReadOnly Property RecorderedFirstViewDateTime As DateTime?
            Get
                If _RecorderedFirstViewDateTime Is Nothing Then
                    Dim viewsTA As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
                    _RecorderedFirstViewDateTime = viewsTA.GetMinTimeStampByDomainID(ID)
                End If
                Return _RecorderedFirstViewDateTime
            End Get
        End Property
        Private _RecorderedLastViewDateTime As DateTime?
        Public ReadOnly Property RecorderedLastViewDateTime As DateTime?
            Get
                If _RecorderedLastViewDateTime Is Nothing Then
                    Dim viewsTA As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
                    _RecorderedLastViewDateTime = viewsTA.GetMaxTimeStampByDomainID(ID)
                End If
                Return _RecorderedLastViewDateTime
            End Get
        End Property
        Public ReadOnly Property RecordingDurationDays As Integer
            Get
                If _RecorderedLastViewDateTime.HasValue And _RecorderedFirstViewDateTime.HasValue Then
                    Dim dt1 As Date = _RecorderedFirstViewDateTime.Value.Date
                    Dim dt2 As Date = _RecorderedLastViewDateTime.Value.Date
                    Dim diff As Integer = DateDiff(DateInterval.Day, dt1, dt2) + 1
                    Return diff
                Else
                    Return 0
                End If
            End Get
        End Property
        Public ReadOnly Property AvgViewsPerDayRecordered As Double
            Get
                If RecordingDurationDays = 0 Then
                    Return 0
                Else
                    Return RecorderedTotalViews / RecordingDurationDays
                End If
            End Get
        End Property

        Public ReadOnly Property PageHeader As String
            Get
                If Title.Length = 0 Then
                    If SiteURL.Length = 0 Then
                        Return "no title or URL, ID: " & ID
                    Else
                        Return SiteURL
                    End If
                Else
                    Return Title
                End If
            End Get
        End Property
        Public ReadOnly Property EditURL As String
            Get
                Return "~\Account\Admin\EditDomain.aspx?DomainID=" & ID
            End Get
        End Property
        Public ReadOnly Property URLAlias As String
            Get
                If SiteURL.StartsWith("www.") Then
                    Return SiteURL.Substring(4, SiteURL.Length - 4)
                Else
                    Return "www." & SiteURL
                End If
            End Get
        End Property
        Public Property Determined As Boolean

        Sub New(ByVal vID As Integer)
            Dim msTA As New MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter
            Dim msRow = msTA.GetDataByID(vID)(0)
            Determined = True
            InitializeByRow(msRow)
        End Sub
        Sub New(ByVal url As String)
            Dim domainRow As MonitoredSitesDataSet.MonitoredSitesRow = ParsingHelper.GetDomainRow(url)
            Determined = domainRow IsNot Nothing
            If Determined Then
                InitializeByRow(domainRow)
            End If
        End Sub
        Private Sub InitializeByRow(ByVal msRow As MonitoredSitesDataSet.MonitoredSitesRow)
            ID = msRow.ID
            SiteURL = msRow.SiteURL
            IsApproved = msRow.IsApproved
            AddedDate = msRow.AddedDate
            Title = msRow.Title
            DefaultParsingActionType = msRow.DefaultParsingActionID
        End Sub
        Sub New(ByVal vSiteURL As String, ByVal vTitle As String, ByVal vDefaultParsingAction As Integer)
            Dim domainsTA As New MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter
            SiteURL = vSiteURL
            Title = vTitle
            DefaultParsingActionType = vDefaultParsingAction
            domainsTA.InsertMonitoredSite(SiteURL, False, Title, DefaultParsingActionType, ID)
            Determined = True
        End Sub
        Public Sub Delete()
            Dim userExclTA As New MonitoredSitesDataSetTableAdapters.MonitoredSitesUserExclusionsTableAdapter
            userExclTA.DeleteByDomainID(ID)
            Dim parsingRulesTA As New MonitoredSitesDataSetTableAdapters.ParsingRulesTableAdapter
            parsingRulesTA.DeleteByDomainID(ID)
            Dim viewsTA As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
            viewsTA.DeleteByDomainID(ID)
            Dim domainsTA As New MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter
            domainsTA.DeleteByID(ID)
            BLL.ParsingHelper.Reload()
        End Sub
        Sub Update()
            Dim msTA As New MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter
            Dim msRow = msTA.GetDataByID(ID)(0)
            msRow.BeginEdit()

            msRow.SiteURL = SiteURL
            msRow.IsApproved = IsApproved
            msRow.Title = Title
            msRow.DefaultParsingActionID = DefaultParsingActionType

            msRow.EndEdit()
            msTA.Update(msRow)

        End Sub
    End Class

End Namespace

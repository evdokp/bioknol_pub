﻿Imports BioWS.BLL.Extensions
Imports BioWS.BLL.Extensions.ParsingExtensions


Namespace BLL
    Public Class ParsingRule
        Public Property ID As Integer
        Public Property URLValidatingRegex As String
        Public Property ParsingAction As ParsingAction
        Public ReadOnly Property IsRuleForParsing As Boolean
            Get
                Return ParsingAction.ActionType = BLL.ParsingAction.ParsingActionTypes.ParseHTML Or ParsingAction.ActionType = BLL.ParsingAction.ParsingActionTypes.ParseURL
            End Get
        End Property
        Sub New(ByVal row As MonitoredSitesDataSet.ParsingRulesRow)
            ID = row.ID
            URLValidatingRegex = row.URLValidatingRegex
            ParsingAction = New ParsingAction(row)
        End Sub
        Public Function IsValid(ByVal vurl As String) As Boolean
            Return vurl.OneRegExMatchOnly(URLValidatingRegex)
        End Function
    End Class
End Namespace


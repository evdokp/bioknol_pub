﻿Imports Microsoft.VisualBasic
Imports System.Web.HttpRuntime

Public Class RegularHelper
    Public Const cacheKey As String = "regEv"
    Public Shared tickscount As Integer
    Public Shared currentDT As DateTime

    Shared Sub ItemRemoved(ByVal key As String, ByVal value As Object, ByVal reason As CacheItemRemovedReason)
        If Not reason = CacheItemRemovedReason.Removed Then
            PeriodicTick()
        End If
    End Sub

    Shared Sub PeriodicTick()
        'Dim ta As New TasksDataSetTableAdapters.TasksToDoTableAdapter
        'Dim tt = ta.GetData_Actual()
        'If tt.Count > 0 Then
        '    Dim tr = tt(0)
        '    ' выполняем задачу
        '    Select Case tr.TastTypeID
        '        Case 1
        '            BLL.PubMed.ExtractPubmedIDfromURL(Today.AddDays(-1), Today.AddDays(1))
        '        Case Else
        '            'nothing
        '    End Select
        '    ' отмечаем, что задача выполнена
        '    tr.BeginEdit()
        '    tr.Done = True
        '    tr.EndEdit()
        '    ta.Update(tr)
        'End If
        currentDT = DateTime.Now
        If tickscount = Integer.MaxValue Then
            tickscount = 0
        Else
            tickscount = tickscount + 1
        End If

        ' нужно запустить кэш заново
        InitiateTick()
    End Sub

    Shared Sub InitiateTick()
        Dim o As Object
        o = Cache(cacheKey)
        If o Is Nothing Then
            Dim expirationTime As DateTime = Date.Now.AddMinutes(1)
            Cache.Insert(cacheKey,
                1,
                Nothing,
                expirationTime,
                Cache.NoSlidingExpiration,
                CacheItemPriority.NotRemovable,
                New CacheItemRemovedCallback(AddressOf ItemRemoved))
        End If
    End Sub


End Class


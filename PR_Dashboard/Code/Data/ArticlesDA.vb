﻿Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Web
Imports BioWS.BLL.Extensions




<DataObject()> _
Public Class ArticlesDA

    Shared Function ArticlesByWidget(ByVal WidgetID As Integer, ByVal vPMID As Integer) As ArticlesPubmedDataSet.ArticlesPubmedDataTable
        Select Case WidgetID
            Case 1
                Return WhatOthersReadWithIt(vPMID)
        End Select
    End Function


    Shared Function WhatOthersReadWithIt(ByVal vPMID As Integer) As ArticlesPubmedDataSet.ArticlesPubmedDataTable
        Using ta As New ArticlesPubmedDataSetTableAdapters.ArticlesPubmedTableAdapter()
            Dim tt = ta.GetViewedTogetherHINT_data(5, 2, vPMID)
            Return tt
        End Using
    End Function

    Shared Function GetArticlesFromPubmedByQuery_byGroup(ByVal query As String, ByVal pid As Integer, ByRef vCount As Integer, ByVal vStartDate As DateTime, ByVal vEndDate As DateTime) As List(Of BLL.PubMed.PubMedArticle)
        'this is list of articles which we will show
        Dim l As New List(Of BLL.PubMed.PubMedArticle)

        'this is list of results of pubmed search
        Dim PMIDList As List(Of Integer)

        'this is how we get results from pubmed (list of integer)
        PMIDList = BLL.PubMed.GetPMIDlistByQuery(query, GetDistinctHistoryPMIDS_byGroup(pid, vStartDate, vEndDate).ListToString(","), vCount)

        'getting PMARTICLES
        l = BLL.PubMed.GetPubMedArticlesByPMIDlist(PMIDList)
        Return l
    End Function
    Shared Function GetArticlesFromPubmedByQuery_byPerson(ByVal query As String, ByVal pid As Integer, ByRef vCount As Integer, ByVal vStartDate As DateTime, ByVal vEndDate As DateTime) As List(Of BLL.PubMed.PubMedArticle)
        'this is list of articles which we will show
        Dim l As New List(Of BLL.PubMed.PubMedArticle)

        'this is list of results of pubmed search
        Dim PMIDList As List(Of Integer)

        'this is how we get results from pubmed (list of integer)
        PMIDList = BLL.PubMed.GetPMIDlistByQuery(query, GetDistinctHistoryPMIDS_byPerson(pid, vStartDate, vEndDate).ListToString(","), vCount)

        'getting PMARTICLES
        l = BLL.PubMed.GetPubMedArticlesByPMIDlist(PMIDList)
        Return l
    End Function
    Shared Function GetArticlesPersonHistory(ByVal pid As Integer, ByVal vStartDate As DateTime, ByVal vEndDate As DateTime) As List(Of BLL.PubMed.PubMedArticle)
        Dim l As New List(Of BLL.PubMed.PubMedArticle)
        Dim PMIDList As List(Of Integer)
        PMIDList = GetDistinctHistoryPMIDS_byPerson(pid, vStartDate, vEndDate)
        l = BLL.PubMed.GetPubMedArticlesByPMIDlist(PMIDList)
        Return l
    End Function

    Shared Function GetArticlesGroupHistory(ByVal gid As Integer, ByVal vStartDate As DateTime, ByVal vEndDate As DateTime) As List(Of BLL.PubMed.PubMedArticle)
        Dim l As New List(Of BLL.PubMed.PubMedArticle)
        Dim PMIDList As List(Of Integer)
        PMIDList = GetDistinctHistoryPMIDS_byGroup(gid, vStartDate, vEndDate)
        l = BLL.PubMed.GetPubMedArticlesByPMIDlist(PMIDList)
        Return l
    End Function

    Shared Function GetArticlesEmpty() As List(Of BLL.PubMed.PubMedArticle)
        Dim l As New List(Of BLL.PubMed.PubMedArticle)
        Return l
    End Function
    Shared Function GetDistinctHistoryPMIDS_byPerson(ByVal pid As Integer, ByVal vStartDate As DateTime, ByVal vEndDate As DateTime) As List(Of Integer)
        Using pmidsTA As New ViewsLogDataSetTableAdapters.v_PubmedViewsTableAdapter()
            Dim pmidsTBL = pmidsTA.GetDistinctPMIDsByPersonIDandTwoDates(pid, vStartDate, vEndDate)
            Dim result As New List(Of Integer)
            pmidsTBL.ProcessTable(Of ViewsLogDataSet.v_PubmedViewsRow)(Sub(r) result.Add(r.PubMed_ID))
            Return result
        End Using
    End Function



    Shared Function GetDistinctHistoryPMIDS_byGroup(ByVal pid As Integer, ByVal vStartDate As DateTime, ByVal vEndDate As DateTime) As List(Of Integer)
        Using ta As New ViewsLogDataSetTableAdapters.v_PubmedViewsTableAdapter()
            Dim t = ta.GetDistinctPMIDsByGroupIDandTwoDates(vStartDate, vEndDate, pid)
            Dim i As Integer
            Dim li As New List(Of Integer)

            For i = 0 To t.Count - 1
                li.Add(t.Item(i).PubMed_ID)
            Next

            Return li
        End Using


    End Function


End Class

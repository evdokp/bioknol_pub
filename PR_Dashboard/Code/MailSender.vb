﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Net.Mail
Imports BioWS.BLL.Extensions
Imports System.Runtime.InteropServices


Namespace BLL





    Public Class MailSender

        Public Class ReadFile
            Public Shared Function ReadFile(ByVal vServer As System.Web.HttpServerUtility, ByVal vMessageFile As String) As String
                Dim fileurl As String = vServer.MapPath("~\Mails\" & vMessageFile)
                Dim sr As StreamReader = New StreamReader(fileurl, True)
                Dim line As String
                Dim linefull As String = String.Empty

                Do
                    line = sr.ReadLine()
                    linefull = linefull + line
                Loop Until line Is Nothing
                sr.Close()

                Return linefull
            End Function

        End Class


        Private Shared Function GetMailAddress(ByVal address As String, Optional ByVal displayName As String = "") As MailAddress
            Dim addr As MailAddress
            If displayName.IsNullOrEmpty() Then
                addr = New MailAddress(address)
            Else
                addr = New MailAddress(address, displayName)
            End If
            Return addr
        End Function

        Public Shared Sub SendMailMessage(ByVal fromEmail As String, _
                                                     ByVal fromDisplayName As String, _
                                                     ByVal recepient As String, _
                                                     ByVal bcc As String, _
                                                     ByVal cc As String, _
                                                     ByVal subject As String, _
                                                     ByVal body As String)

            Dim message As New MailMessage()
            Dim aFrom As MailAddress = GetMailAddress(fromEmail, fromDisplayName)

            message.Sender = aFrom
            message.From = aFrom
            message.To.Add(GetMailAddress(recepient))

            If cc.IsNotEmpty() Then
                message.CC.Add(cc)
            End If

            message.Subject = subject
            message.Body = body

            message.IsBodyHtml = True

            Dim smtp As New SmtpClient() With {.Host = "localhost", .Credentials = New System.Net.NetworkCredential()}
            smtp.Send(message)
        End Sub
    End Class
End Namespace

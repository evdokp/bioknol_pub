﻿
Option Strict Off
Option Explicit On

Imports System
Imports System.Web
Imports System.Web.Profile


Public Class ProfileCommon
    Inherits System.Web.Profile.ProfileBase

    Public Overridable Property Person_ID() As Integer
        Get
            Return CType(Me.GetPropertyValue("Person_ID"), Integer)
        End Get
        Set(ByVal value As Integer)
            Me.SetPropertyValue("Person_ID", value)
        End Set
    End Property

    Public Overridable Property DefaultPeriod() As String
        Get
            Return CType(Me.GetPropertyValue("DefaultPeriod"), String)
        End Get
        Set(ByVal value As String)
            Me.SetPropertyValue("DefaultPeriod", value)
        End Set
    End Property
    Public Overridable Property DefaultPeriodDays() As Integer
        Get
            Return CType(Me.GetPropertyValue("DefaultPeriodDays"), Integer)
        End Get
        Set(ByVal value As Integer)
            Me.SetPropertyValue("DefaultPeriodDays", value)
        End Set
    End Property

    Public Overridable Function GetProfile(ByVal username As String) As ProfileCommon
        Return CType(ProfileBase.Create(username), BioWS.ProfileCommon)
    End Function

End Class
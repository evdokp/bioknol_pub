﻿


Public Class QueryStringBuilder
    Private QSValues As NameValueCollection

    Public Sub New()
        QSValues = New NameValueCollection()
    End Sub

    Public Function SetQSValue(name As String) As QueryStringBuilder
        Return SetValue(name, HttpContext.Current.Request.QueryString(name))
    End Function

    Public Function SetValue(name As String, value As String) As QueryStringBuilder
        If QSValues.AllKeys.Contains(name) Then
            QSValues.Item(name) = value
        Else
            QSValues.Add(name, value)
        End If
        Return Me
    End Function



    Public Function ToQueryString() As String
        Return "?" & String.Join("&", Array.ConvertAll(QSValues.AllKeys, Function(key) String.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(QSValues(key)))))
    End Function
End Class

﻿Imports System.IO
Imports AzureStorageLibrary


Namespace BLL
    Namespace FileHelper


        Public Class SaveFile


            Public Shared Sub SaveArticleAzure(ByVal vUrl As String,
                                             ByVal vPmid As Integer,
                                             ByVal vDoi As String,
                                             ByVal vPluginId As Integer,
                                             ByVal vPersonId As Integer,
                                             ByVal fileData As Byte(),
                                             ByVal contentType As String,
                                             ByVal originalName As String,
                                             ByVal vTitle As String,
                                             ByVal vDescription As String)


                Using ta As New ArticlesDataSetTableAdapters.ArticleOffersTableAdapter()

                    Dim fileFolderName As Guid = Guid.NewGuid()
                    Dim fileFolderFullName As String = fileFolderName.ToString + Path.GetExtension(originalName)

                    ta.Insert(Now(), vUrl, vPmid, vPluginId, vPersonId, fileFolderFullName, vTitle, originalName, contentType, vDescription, vDoi)

                    Dim storage As New AzureStorage()
                    storage.UploadArticle(fileData, fileFolderFullName)

                End Using



            End Sub
            <ObsoleteAttribute("This method is obsolete. Call SaveArticleAzure instead.", True)>
            Public Shared Sub SaveArticle(ByVal vUrl As String,
                                             ByVal vPmid As Integer,
                                             ByVal vDoi As String,
                                             ByVal vPluginId As Integer,
                                             ByVal vPersonId As Integer,
                                             ByVal fileData As Byte(),
                                             ByVal contentType As String,
                                             ByVal originalName As String,
                                             ByVal vTitle As String,
                                             ByVal vDescription As String)



                Using ta As New ArticlesDataSetTableAdapters.ArticleOffersTableAdapter()

                    Dim fileFolderName As Guid = Guid.NewGuid()
                    Dim fileFolderFullName As String = fileFolderName.ToString + Path.GetExtension(originalName)

                    ta.Insert(Now(), vUrl, vPmid, vPluginId, vPersonId, fileFolderFullName, vTitle, originalName, contentType, vDescription, vDoi)

                    Const myBufferSize As Integer = 1024
                    Dim myInputStream As Stream = New MemoryStream(fileData)
                    Dim myOutputStream As Stream = File.OpenWrite("c:\websites\BioWS\UploadedArticles\" + fileFolderFullName)
                    Dim buffer As Byte() = New Byte(myBufferSize) {}
                    Dim numbytes As Integer = myInputStream.Read(buffer, 0, myBufferSize)
                    While (numbytes) > 0
                        myOutputStream.Write(buffer, 0, numbytes)
                        numbytes = myInputStream.Read(buffer, 0, myBufferSize)
                    End While
                    myInputStream.Close()
                    myOutputStream.Close()
                End Using
            End Sub





        End Class
    End Namespace
End Namespace
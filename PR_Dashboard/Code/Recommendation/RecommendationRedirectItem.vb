﻿Imports BioKnol.DataAccess


Namespace BLL
    Public Class RecommendationRedirectItem


        Private PMID As Integer?
        Private Url As String
        Private _DecodedUrl As String
        Public ReadOnly Property DecodedUrl() As String
            Get
                If String.IsNullOrEmpty(_DecodedUrl) Then
                    _DecodedUrl = HttpUtility.UrlDecode(Url)
                End If
                Return _DecodedUrl
            End Get
        End Property
        Private _dm As DomainMeta
        Public ReadOnly Property HasDomain() As Boolean
            Get
                _dm = New DomainMeta(DecodedUrl)
                Return _dm.Determined
            End Get
        End Property
        Public ReadOnly Property IsArticle() As Boolean
            Get
                Dim prT = ParsingHelper.GetParsingRulesRows(_dm.ID)
                Dim ParsingRules = New List(Of ParsingRule)()
                For Each prRow As MonitoredSitesDataSet.ParsingRulesRow In prT
                    Dim parsingRule As New ParsingRule(prRow)
                    ParsingRules.Add(parsingRule)
                Next

                Dim rightRules = ParsingRules.Where(Function(r As ParsingRule) r.IsValid(Url)).ToList
                Return rightRules.Count > 0
            End Get
        End Property


        Private ReadOnly Property HasPMID As Boolean
            Get
                Dim viewsrep As New ViewsRepository
                Return viewsrep.TryGetPMIDbyUrl(DecodedUrl, PMID)
            End Get
        End Property
        Private ReadOnly Property HasDOI As Boolean
            Get
                Dim viewsrep As New ViewsRepository
                Return viewsrep.HasDOIByURL(DecodedUrl)
            End Get
        End Property


        Sub New(ByVal incomingUrl As String)
            Url = incomingUrl
        End Sub

        Public Function GetRedirectUrl() As String
            If HasDomain Then
                If HasPMID Then
                    Return String.Format("~/AccountNew/ViewArticle.aspx?id={0}&recommend=1", PMID)
                Else
                    If HasDOI Then
                        Return GetFailedURL("onlydoi")
                    Else
                        If IsArticle Then
                            Return GetFailedURL("failedarticleparsing")
                        Else
                            Return GetFailedURL("notarticlenorules")
                        End If
                    End If
                End If
            Else
                Return GetFailedURL("nodomain")
            End If

            Return DecodedUrl
        End Function

        Private Function GetFailedURL(ByVal err As String) As String
            Return String.Format("~/AccountNew/FailedRecommendation.aspx?url={0}&error={1}", Url, err)
        End Function

    End Class

    


End Namespace

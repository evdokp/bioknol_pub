Imports System.Runtime.CompilerServices
Imports System.Web.UI

Namespace BLL.Extensions
    Module PageExtensions

        <Extension()>
        Public Function GetProfilePersonID(ByVal vPage As Page) As Integer
            Dim _pr As New ProfileCommon
            _pr = _pr.GetProfile(vPage.User.Identity.Name)
            Return _pr.Person_ID
        End Function

        <Extension()>
        Public Sub SetTitlesToGrandMaster(ByVal vPage As Page, ByVal vPageTitle As String, ByVal vCurObject As String)
            CType(vPage.Master.Master.FindControl("lit_pagetitle"), Literal).Text = vPageTitle
            CType(vPage.Master.Master.FindControl("lit_curobj"), Literal).Text = vCurObject
        End Sub

        <Extension()>
        Public Sub SetTitlesToMaster(ByVal vPage As Page, ByVal vPageTitle As String, ByVal vCurObject As String)
            CType(vPage.Master.FindControl("lit_pagetitle"), Literal).Text = vPageTitle
            CType(vPage.Master.FindControl("lit_curobj"), Literal).Text = vCurObject
        End Sub

        <Extension()> _
        Public Function QS(page As Page, propertyname As String) As String
            Return page.Request.QueryString(propertyname)
        End Function

        <Extension()> _
        Public Function QS(page As MasterPage, propertyname As String) As String
            Return page.Request.QueryString(propertyname)
        End Function

        <Extension()> _
        Public Function QSInt32(page As Page, propertyname As String) As Integer
            Return Convert.ToInt32(page.Request.QueryString(propertyname))
        End Function

        <Extension()> _
        Public Function QSInt32(page As MasterPage, propertyname As String) As Integer
            Return Convert.ToInt32(page.Request.QueryString(propertyname))
        End Function

        <Extension()> _
        Public Function NotPostBack(page As Page) As Boolean
            Return Not page.IsPostBack
        End Function

        <Extension()> _
        Public Function IsAuthenticated(page As Page) As Boolean
            Return page.User.Identity.IsAuthenticated
        End Function

        <Extension()> _
        Public Function GetProfile(page As Page) As ProfileCommon
            Dim _profile As New ProfileCommon
            _profile = _profile.GetProfile(page.User.Identity.Name)
            Return _profile
        End Function

        <Extension()> _
        Public Function GetProfile(page As MasterPage) As ProfileCommon
            Dim _profile As New ProfileCommon
            _profile = _profile.GetProfile(page.Page.User.Identity.Name)
            Return _profile
        End Function

    End Module

End Namespace
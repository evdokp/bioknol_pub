Imports System.Runtime.CompilerServices

Namespace BLL.Extensions



    Module IntegerExtensions
        <Extension()>
        Public Function ListToString(ByVal source As List(Of Integer)) As String
            Dim s As String
            For i = 0 To source.Count - 1
                Dim IsNotLast As Boolean = i < source.Count - 1
                s = String.Format("{0}{1}", s, source(i))
                If IsNotLast Then
                    s = s & ";"
                End If
            Next
            Return s
        End Function

        <Extension()>
        Public Function ListToString(ByVal source As List(Of Integer), ByVal delimiter As String) As String
            Dim s As String
            For i = 0 To source.Count - 1
                Dim IsNotLast As Boolean = i < source.Count - 1
                s = String.Format("{0}{1}", s, source(i))
                If IsNotLast Then
                    s = s & delimiter
                End If
            Next
            Return s
        End Function

        <Extension()>
        Public Function ToBoolean(ByVal value As Integer) As Boolean
            Return IIf(value = 0, False, True)
        End Function

        <Extension()>
        Public Function ToIntBoolean(ByVal value As Integer) As Integer
            Return IIf(value = 0, 0, 1)
        End Function

    End Module

End Namespace

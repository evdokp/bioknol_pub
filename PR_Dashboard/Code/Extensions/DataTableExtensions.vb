Imports System.Runtime.CompilerServices

Namespace BLL.Extensions
    Module DataTableExtensions
        <Extension()> _
        Sub ProcessTable(Of T)(table As DataTable, ProcessorFunc As Action(Of T))
            Dim lst2 As New List(Of T)
            For Each row As DataRow In table.Rows
                lst2.Add(DirectCast(DirectCast(row, Object), T))
            Next
            For Each elem In lst2
                ProcessorFunc(elem)
            Next
        End Sub



    End Module

End Namespace

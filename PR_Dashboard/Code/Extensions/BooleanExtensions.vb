Imports System.Runtime.CompilerServices

Namespace BLL.Extensions
    Module BooleanExtensions
        <Extension()> _
        Public Function Inverse(source As Boolean) As Boolean
            Return Not source
        End Function

        <Extension()> _
        Public Function ToInt(ByVal value As Boolean) As Integer
            Return IIf(value, 1, 0)
        End Function


    End Module

End Namespace

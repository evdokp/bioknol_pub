Imports System.Runtime.CompilerServices
Imports System.Web
Imports System.Web.Script.Serialization


Namespace BLL.Extensions
    Module WebExtensions
        <Extension()> _
        Public Sub WritePlainText(response As HttpResponse, input As String)
            response.ContentType = "text/plain"
            response.Write(input)
        End Sub

        <Extension()> _
        Public Sub WriteJSON(response As HttpResponse, input As Object)
            Dim serializer As New JavaScriptSerializer()
            response.ContentType = "text/plain"
            response.Write(serializer.Serialize(input))
        End Sub
    End Module

End Namespace

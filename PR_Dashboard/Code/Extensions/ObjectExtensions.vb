Imports System.Runtime.CompilerServices
Imports System.Web.Script.Serialization

Namespace BLL.Extensions
    Module ObjectExtensions
        <Extension()>
        Public Function ToJSON(ByVal obj As Object) As String
            Dim serializer As New JavaScriptSerializer()
            Return serializer.Serialize(obj)
        End Function

        <Extension()>
        Private Function GetCommandArguement(Of T)(ByVal obj As Object) As T
            Dim result As T
            Dim sender = DirectCast(obj, IButtonControl)
            Dim ca = sender.CommandArgument
            result = DirectCast(DirectCast(ca, Object), T)
            Return result
        End Function

        <Extension()> _
        Public Function ToSeparatedString(collection As List(Of Integer), separator As String) As String
            Dim isFirst As Boolean = True
            Dim sb = New StringBuilder()
            For Each v As Integer In collection
                If Not isFirst Then
                    sb.Append(separator)
                End If
                sb.Append(v.ToString())
                isFirst = False
            Next
            Return sb.ToString()
        End Function



        <Extension()>
        Private Function GetRow(Of T)(obj As DataRowView) As T
            Dim result As T
            Dim datarow As DataRow = obj.Row
            result = DirectCast(DirectCast(datarow, Object), T)
            Return result
        End Function

        <Extension()>
        Public Function ToListOfIntegers(objects As List(Of Object)) As List(Of Integer)
            Dim res As New List(Of Integer)
            For Each obj As Object In objects
                Dim pid As Integer = CInt(obj)
                res.Add(pid)
            Next
            Return res
        End Function

    End Module

End Namespace

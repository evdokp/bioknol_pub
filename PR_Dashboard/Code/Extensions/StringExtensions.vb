﻿Imports System.Runtime.CompilerServices
Imports System.Web.Script.Serialization
Imports System.Net


Namespace BLL.Extensions

    Module StringExtensions

        <Extension()>
        Public Function SubstringToLength(ByVal aString As String, ByVal length As Integer) As String
            Dim res As String
            res = aString.Substring(0, Math.Min(aString.Length, length))
            If aString.Length >= length Then
                res = res & "..."
            End If
            Return res
        End Function

        <Extension()>
        Public Function ToInt32(source As String) As Integer
            Return Convert.ToInt32(source)
        End Function

        <Extension()> _
        Public Function IsNullOrEmpty(source As String) As Boolean
            Return String.IsNullOrEmpty(source)
        End Function

        <Extension()> _
        Public Function IsNotEmpty(source As String) As Boolean
            Return Not String.IsNullOrEmpty(source)
        End Function

        <Extension()> _
        Public Function UrlDecode(source As String) As String
            Return HttpUtility.UrlDecode(source)
        End Function

        <Extension()> _
        Public Function StripTags(ByVal input As String) As String
            Dim s As String
            s = Regex.Replace(input, "<.*?>", "")
            s = Regex.Replace(s, "&nbsp;", " ")
            s = Regex.Replace(s, "&quot;", """")
            Return s
        End Function

        <Extension()> _
        Public Function StripTagsWithLineBreaks(ByVal input As String) As String
            Dim s As String
            s = Regex.Replace(input, "<br/>", vbCrLf)
            s = Regex.Replace(s, "<p.*?>", vbCrLf)
            s = Regex.Replace(s, "<.*?>", "")
            s = Regex.Replace(s, "&nbsp;", " ")
            s = Regex.Replace(s, "&quot;", """")
            Return s
        End Function


    End Module

End Namespace


Namespace BLL.Extensions.Web
    Module StringExtensions


        <Extension()>
        Public Function GetHTML(ByVal url As String) As String
            Using wc As New WebClient()
                Return wc.DownloadString(url)
            End Using
        End Function

        <Extension()>
        Public Function GetTitleTagText(ByVal vHTML As String) As String
            Dim s As String
            s = Regex.Match(vHTML, "(?<=<title.*>)([\s\S]*)(?=</title>)", RegexOptions.IgnoreCase).Value
            Return s.Trim
        End Function

    End Module
End Namespace


﻿Imports System.IO
Imports BioWS.BLL.Extensions
Imports BioWS.BLL.Extensions.Web
Imports System.Threading


Namespace BLL
    Public Class PubMed
        Public Shared Sub ExtractPubmedIDfromURL(ByVal vFrom As DateTime, ByVal vTo As DateTime)
            'runs through log and extracts where possible, vFrom, vTo - date period
            Using ta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter()
                Dim t As ViewsLogDataSet.ViewsLogDataTable
                Dim r As ViewsLogDataSet.ViewsLogRow
                'http://www.ncbi.nlm.nih.gov/pubmed/19829395?ordinalpos=1&itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum

                Const pattern As String = "(?<=http://www.ncbi.nlm.nih.gov/pubmed/"
                Dim pid As Integer
                Dim i As Integer
                t = ta.GetDataByTwoDatesAndSiteID(2144, vFrom, vTo)
                For i = 0 To t.Count - 1
                    r = t.Item(i)
                    pid = GetPubMedIDfromURL(r.URL)
                    If pid > 0 Then
                        r.BeginEdit()
                        r.PubMed_ID = pid
                        r.EndEdit()
                        ta.Update(r)
                    End If
                Next

                t = ta.GetDataByTwoDatesAndSiteID(1665, vFrom, vTo)
                For i = 0 To t.Count - 1
                    r = t.Item(i)
                    pid = GetPubMedIDfromURL(r.URL)
                    If pid > 0 Then
                        r.BeginEdit()
                        r.PubMed_ID = pid
                        r.EndEdit()
                        ta.Update(r)
                    End If
                Next
            End Using
        End Sub
        Public Shared Sub ProcessNewPubmedArticles()
            'берем таблицу - список Pumed_ID, которых еще нет у нас в таблице ArticlesPubmed
            Using pmta As New ArticlesPubmedDataSetTableAdapters.ArticlesPubmedTableAdapter
                Using ta As New ArticlesPubmedDataSetTableAdapters.UnprocessedPubmedTableAdapter
                    Dim t = ta.GetData()
                    Dim i As Integer
                    Dim vTitle As String
                    'пробегаемся по ней и выполняем следующие действия:
                    For i = 0 To t.Count - 1
                        vTitle = GetPubmedTitleFromWS(t.Item(i).Pubmed_ID)
                        If vTitle = String.Empty Then
                            'nothing
                        Else
                            'Вставляем запись в таблицу ArticlesPubmed
                            pmta.Insert(t.Item(i).Pubmed_ID, vTitle, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
                        End If
                    Next
                End Using
            End Using
        End Sub
        Public Shared Function GetPubmedTitleFromWS(ByVal vPMID As Integer)
            Try
                Dim serv As New wref_Pubmed_eFetch.eFetchPubmedService
                Dim req As New wref_Pubmed_eFetch.eFetchRequest
                Dim res As wref_Pubmed_eFetch.eFetchResult
                ' call NCBI EInfo utility
                req.id = vPMID
                res = serv.run_eFetch(req)
                Return res.PubmedArticleSet(0).MedlineCitation.Article.ArticleTitle
            Catch eee As Exception
                Return String.Empty
            End Try
        End Function
        Public Shared Function GetPubmedArticleHTMLbyID(ByVal Pubmed_ID As Integer) As String
            Dim vURL As String = "http://www.ncbi.nlm.nih.gov/pubmed/" & Pubmed_ID.ToString
            Return vURL.GetHTML()
        End Function
        Public Shared Function GetPubMedIDfromURL(ByVal vURL As String) As Integer
            Dim s As String
            Dim i As Integer
            s = Regex.Match(vURL, "(?<=www.ncbi.nlm.nih.gov/pubmed/)\d{1,30}").Value
            If s = String.Empty Then
                i = 0
            Else
                Integer.TryParse(s, i)
            End If
            Return i
        End Function
        Public Shared Function FindPubMedIDbyURL(ByVal vURL As String) As Integer
            Dim i As Integer
            Using ta As New ViewsLogDataSetTableAdapters.v_PubmedViewsTableAdapter()
                If ta.GetPMIDbyURL(vURL).HasValue Then
                    i = ta.GetPMIDbyURL(vURL)
                Else
                    i = 0
                End If
            End Using
            Return i
        End Function


        Public Shared Sub ProcessPMIDinThread(ByVal vPMID As Integer)
            If vPMID > 0 Then
                Dim t As New Thread(AddressOf ProcessPubmedView)
                t.Start(vPMID)
            End If
        End Sub

        Public Shared Sub ProcessPubmedView(ByVal vPMID As Object)
            Dim intPMID = CInt(vPMID)

            'If vPMID > 0 Then
            '    'check if there's such article in ArticlesPubmed table
            '    Dim ta As New ArticlesPubmedDataSetTableAdapters.ArticlesPubmedTableAdapter
            '    If ta.GetCountByPubmedID(vPMID) = 0 Then
            '        'if there's no - get title from HTML, insert
            '        Dim vTitle As String
            '        vTitle = GetPubmedTitleFromWS(vPMID)
            '        ta.Insert(vPMID, vTitle, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
            '    End If
            'End If
            ProcessPubmedArticleWeb.ProcessPMID(intPMID, False)
        End Sub

        Public Shared Function GetTitleByID_internal(ByVal vPMID As Integer) As String
            Using ta As New ArticlesPubmedDataSetTableAdapters.ArticlesPubmedTableAdapter()
                Dim t = ta.GetDataByPMID(vPMID)
                If t.Count > 0 Then
                    Return t.Item(0).Title
                Else
                    Return String.Empty
                End If
            End Using
        End Function
        Public Shared Function GetURLfromID(ByVal vPMID As Integer) As String
            Return "http://www.ncbi.nlm.nih.gov/pubmed/" & vPMID
        End Function


        Public Class PubMedArticle
            Private ReadOnly _Pubmed_ID As Integer
            Private _Journal_ID As Integer
            Public Property Title As String
            Public Property Abstract As String
            Public Property PubYear As Integer
            Public ReadOnly Property Journal As String
                Get
                    If _Journal_ID = 0 Then
                        Return "<i>Journal unknown</i>"
                    Else
                        Dim j_ta As New ArticlesPubmedDataSetTableAdapters.pmJournalsTableAdapter
                        Return j_ta.GetDataByJournal_ID(_Journal_ID).Item(0).Title
                    End If

                End Get
            End Property
            Public ReadOnly Property AuthorsString As String
                Get
                    Using ta As New ArticlesPubmedDataSetTableAdapters.pmAuthorsTableAdapter()
                        Dim t = ta.GetDataByPubmedID(Pubmed_ID)
                        Dim s As String
                        For i = 0 To t.Count - 1
                            If i <> t.Count - 1 Then
                                s = String.Format("{0}{1} {2}, ", s, t.Item(i).LastName, t.Item(i).Initials)
                            Else
                                s = s & t.Item(i).FullName
                            End If
                        Next
                        Return s
                    End Using
                End Get
            End Property


            Public ReadOnly Property AuthorsStringShort As String
                Get
                    Using ta As New ArticlesPubmedDataSetTableAdapters.pmAuthorsTableAdapter()
                        Dim t = ta.GetDataByPubmedID(Pubmed_ID)
                        Dim s As String


                        If t.Count > 2 Then
                            Dim rowFirst = t.Item(0)
                            Dim rowLast = t.Item(t.Count - 1)
                            s = String.Format("{0} ... {1}", GetAuthorItem(rowFirst), GetAuthorItem(rowLast))
                        End If
                        If t.Count = 2 Then
                            Dim rowFirst = t.Item(0)
                            Dim rowLast = t.Item(1)
                            s = String.Format("{0}, {1}", GetAuthorItem(rowFirst), GetAuthorItem(rowLast))
                        End If
                        If t.Count = 1 Then
                            s = GetAuthorItem(t.Item(0))
                        End If

                        Return s
                    End Using
                End Get
            End Property



            Private Function GetAuthorItem(ByVal row As ArticlesPubmedDataSet.pmAuthorsRow) As String
                Dim s As String = String.Empty
                If Not row.IsLastNameNull Then
                    s = s & row.LastName
                End If
                If Not row.IsInitialsNull Then
                    s = String.Format("{0} {1}", s, row.Initials)
                End If
                Return s
            End Function


            Public ReadOnly Property MeshString As String
                Get
                    Using ta As New ArticlesPubmedDataSetTableAdapters.pmMeSHTableAdapter()
                        Dim t = ta.GetDataByPubmedID(Pubmed_ID)
                        Dim s As String
                        For i = 0 To t.Count - 1
                            If i <> t.Count - 1 Then
                                s = s & t.Item(i).Heading & "<br/>"
                            Else
                                s = s & t.Item(i).Heading
                            End If
                        Next
                        Return s
                    End Using
                End Get
            End Property




            Public ReadOnly Property Pubmed_ID() As Integer
                Get
                    Return _Pubmed_ID
                End Get
            End Property
            Public ReadOnly Property PubmedURL As String
                Get
                    Return GetURLfromID(Pubmed_ID)
                End Get
            End Property
            Public ReadOnly Property ViewPMURL As String
                Get
                    Return "~\ViewInfoPM.aspx?Pubmed_ID=" & Pubmed_ID
                End Get
            End Property
            Public ReadOnly Property ViewsByPerson(ByVal vPerson_ID As Integer) As Integer
                Get
                    Dim ta As New ViewsLogDataSetTableAdapters.v_PubmedViewsTableAdapter
                    Return ta.GetViewsCountByPMIDbyPersonID(Pubmed_ID, vPerson_ID)
                End Get
            End Property
            Public ReadOnly Property ViewsByNotPerson(ByVal vPerson_ID As Integer) As Integer
                Get
                    Dim ta As New ViewsLogDataSetTableAdapters.v_PubmedViewsTableAdapter
                    Return ta.GetViewsCountByPMIDbyNOTPersonID(Pubmed_ID, vPerson_ID)
                End Get
            End Property
            Public ReadOnly Property ViewersCountNotPerson(ByVal vPersonID As Integer) As Integer
                Get
                    Dim ta As New ViewsLogDataSetTableAdapters.v_PubmedViewsTableAdapter
                    Return ta.GetViewersCountByPubmedIDbyNotPersonID(Pubmed_ID, vPersonID)
                End Get
            End Property
            Public ReadOnly Property OffersWithFilesCount() As Integer
                Get
                    Dim ta As New ArticlesDataSetTableAdapters.ArticleOffersTableAdapter
                    Return ta.GetCountByPubmedIDwithFiles(Pubmed_ID)
                End Get
            End Property
            Public ReadOnly Property PersonsCountWithOffersWithoutFiles(ByVal vPersonID As Integer) As Integer
                Get
                    Dim ta As New ArticlesDataSetTableAdapters.ArticleOffersTableAdapter
                    Return ta.GetCountByPubmedIDNOTPersonIDwithoutFiles(Pubmed_ID, vPersonID)
                End Get
            End Property
            Public ReadOnly Property PersonsCountRequestedNotPerson(ByVal vPersonID As Integer) As Integer
                Get
                    Dim ta As New ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter
                    Return ta.GetPersonCountByPubmedIDNotPersonIDwithoutOffers(vPersonID, Pubmed_ID)
                End Get
            End Property
            'Public ReadOnly Property HavePersonOfferedWithoutFiles(ByVal vPersonID As Integer) As Boolean
            '    Get
            '        Dim ta As New ArticlesDataSetTableAdapters.ArticleOffersTableAdapter
            '        If ta.GetCountByPersonIDbyPubmedIDwithoutFiles(vPersonID, Pubmed_ID) > 0 Then
            '            Return True
            '        Else
            '            Return False
            '        End If
            '    End Get
            'End Property
            'Public ReadOnly Property HavePersonPendingRequests(ByVal vPersonID As Integer) As Boolean
            '    Get
            '        Dim ta As New ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter
            '        If ta.GetCount_PendingByPersonIDbyPubmedID(vPersonID, Pubmed_ID) > 0 Then
            '            Return True
            '        Else
            '            Return False
            '        End If
            '    End Get
            'End Property
            Sub New(ByVal vPMID As Integer)
                Using ta As New ArticlesPubmedDataSetTableAdapters.ArticlesPubmedTableAdapter()
                    Dim t = ta.GetDataByPMID(vPMID)
                    If t.Count > 0 Then
                        Dim r = t.Item(0)

                        _Pubmed_ID = r.Pubmed_ID()
                        If r.Title = String.Empty Then
                            Title = "_"
                        Else
                            Title = r.Title.TrimStart("[").TrimEnd("]")
                        End If

                        If r.IsAbstractNull Then
                            Abstract = "<i>No abstract available</i>"
                        Else
                            Abstract = r.Abstract
                        End If

                        If r.IsJournal_IDNull Then
                            _Journal_ID = 0
                        Else
                            _Journal_ID = r.Journal_ID
                        End If

                        If r.IsJournal_IDNull Then
                            PubYear = 0
                        Else
                            PubYear = r.PubYear
                        End If




                    End If
                End Using
            End Sub






        End Class


#Region "search"

        Public Shared Function GetPubMedArticlesByPMIDlist(ByVal vList As List(Of Integer)) As List(Of PubMedArticle)
            Dim l As New List(Of PubMedArticle)
            For Each i In vList
                Dim vPMA As New PubMedArticle(i)
                l.Add(vPMA)
            Next
            Return l
        End Function
        Public Shared Function GetPMIDlistByQuery(ByVal Query As String, ByVal HistoryPMIDstring As String, ByRef foundCount As Integer) As List(Of Integer)
            Dim serv As New wref_Pubmed_eUtils.eUtilsService
            Dim req, req2 As New wref_Pubmed_eUtils.eSearchRequest
            Dim res, res2 As wref_Pubmed_eUtils.eSearchResult
            ' call NCBI EInfo utility
            req.term = HistoryPMIDstring
            req.RetMax = 1
            req.usehistory = "y"
            res = serv.run_eSearch(req)

            Dim WebEnv As String
            Dim QueryKey As String

            WebEnv = res.WebEnv
            QueryKey = res.QueryKey

            req2.QueryKey = QueryKey
            req2.WebEnv = WebEnv
            req2.term = Query
            req2.RetMax = 10000
            res2 = serv.run_eSearch(req2)

            Dim i As Integer
            Dim li As New List(Of Integer)
            For i = 0 To res2.IdList.Count - 1
                li.Add(CInt(res2.IdList(i)))
            Next
            foundCount = res2.Count

            Return li
        End Function
        'Public Shared Function ListOfIntegerToString(ByVal li As List(Of Integer))
        '    Dim s As String
        '    Dim i As Integer
        '    For i = 0 To li.Count - 1
        '        If i = li.Count - 1 Then
        '            s = s & li(i)
        '        Else
        '            s = String.Format("{0}{1},", s, li(i))
        '        End If
        '    Next
        '    Return s
        'End Function
        Private Shared Function GetVPMID(ByRef artidslist As wref_Pubmed_eFetch.ArticleIdType()) As Integer
            Dim k As Integer
            For k = 0 To artidslist.Count - 1
                If artidslist(k).IdType = 7 Then
                    Return CInt(artidslist(k).Value)
                    Exit For
                End If
            Next
        End Function
        'Public Shared Function GetPMArticlesDataByIDString(ByVal IDstring As String) As List(Of PubMedArticle)
        '    If IDstring Is Nothing Then
        '        Return Nothing
        '    Else
        '        Dim serv As New wref_Pubmed_eFetch.eFetchPubmedService
        '        Dim req As New wref_Pubmed_eFetch.eFetchRequest
        '        Dim res As wref_Pubmed_eFetch.eFetchResult

        '        req.id = IDstring
        '        res = serv.run_eFetch(req)

        '        Dim i As Integer
        '        Dim s As String

        '        Dim pmart_list As New List(Of PubMedArticle)

        '        For i = 0 To res.PubmedArticleSet.Count - 1
        '            Dim pmart As New PubMedArticle(Pubmed_ID)
        '            pmart.Pubmed_ID = GetVPMID(res.PubmedArticleSet(i).PubmedData.ArticleIdList)
        '            pmart.Title = res.PubmedArticleSet(i).MedlineCitation.Article.ArticleTitle
        '            pmart_list.Add(pmart)
        '        Next
        '        Return pmart_list
        '    End If
        'End Function








#End Region
    End Class


End Namespace

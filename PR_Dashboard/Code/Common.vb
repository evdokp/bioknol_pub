﻿Imports System.Drawing
Imports System.IO

Imports BioWS.BLL.Extensions
Namespace BLL
    Public Class Common
        Public Class constants
            Public Const vNullDate As Date = #1/1/2100#
            Public Const vSavedHTMLpath As String = "C:\WebSites\BioWS\SavedHTML\"
        End Class

        Public Sub New(m As Object)
            m = m
        End Sub

        Public Shared Function GetGravatar(ByVal username As String, ByVal size As Integer) As String
            Dim hash As String = BLL.Common.GetMD5Hash(username.ToLower())
            Return "http://www.gravatar.com/avatar/" & hash & "?s=" & size.ToString() & "&d=identicon"
        End Function


        Public Shared Function HttpUtilityUrlDecode(ByVal input As String) As String
            Return HttpUtility.UrlDecode(input)
        End Function
        Public Shared Function DaysAgo(ByVal vDate As Date) As String
            Dim i As Integer
            i = DateDiff(DateInterval.Day, vDate, Today)
            Select Case i
                Case 0
                    Return "Today"
                Case 1
                    Return "1 day ago"
                Case Else
                    Return i & " days ago"
            End Select
        End Function
        Public Shared Function FormAnchor(ByVal vURL, ByVal vTitle) As String
            Return "<a href=""" & vURL & """>" & vTitle & "</a>"
        End Function

        <Obsolete("This method is depricated. Use AzureStorage.UploadHTML instead", True)>
        Public Shared Function SaveHTML(ByVal html As String) As String
            Dim FileName As Guid = Guid.NewGuid()
            File.WriteAllText(String.Format("{0}{1}.html", BLL.Common.constants.vSavedHTMLpath, FileName), html)
            Return FileName.ToString
        End Function

        <Obsolete("This method is depricated. Use AzureStorage.DownloadHTML instead", True)>
        Public Shared Function ReadSavedHTML(ByVal fileguid As String) As String
            Dim filepath As String = String.Format("{0}{1}.html", BLL.Common.constants.vSavedHTMLpath, fileguid)
            If File.Exists(filepath) Then
                Dim tr As TextReader = New StreamReader(filepath)
                Return tr.ReadToEnd()
            Else
                Return String.Empty
            End If
        End Function


        Public Shared Function TryFindPubmedIDbyDOI(ByVal doi As String, ByRef pubmedID As Integer) As Boolean
            Dim findservice As New wref_Pubmed_eUtils.eUtilsService
            Dim request As New wref_Pubmed_eUtils.eSearchRequest
            Dim response As wref_Pubmed_eUtils.eSearchResult
            request.term = doi
            response = findservice.run_eSearch(request)
            If response.Count = 1 Then
                pubmedID = CInt(response.IdList(0))
                Return True
            Else
                Return False
            End If
        End Function

 
        Public Shared Function GetMD5Hash(input As String) As String
            Dim x As New System.Security.Cryptography.MD5CryptoServiceProvider()
            Dim bs As Byte() = System.Text.Encoding.UTF8.GetBytes(input)
            bs = x.ComputeHash(bs)
            Dim s As New System.Text.StringBuilder()
            For Each b As Byte In bs
                s.Append(b.ToString("x2").ToLower())
            Next
            Dim password As String = s.ToString()
            Return password
        End Function




        Public Shared Function GetIP() As String
            Dim vIP As String
            Try
                vIP = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                If vIP.IsNullOrEmpty() Then
                    vIP = HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR")
                End If
            Catch ex As Exception
                Return String.Empty
            End Try
            Return vIP
        End Function



        Public Shared Function IsValidEmail(ByVal email As String) As Boolean
            Dim rx As New Regex("\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b", RegexOptions.IgnoreCase)
            Dim matches As MatchCollection = rx.Matches(email)
            Return matches.Count.ToBoolean()
        End Function
    End Class
End Namespace


﻿
Namespace BLL
    Public Class Mesh
        Public Property ID As Integer
        Public Property Title As String
        Sub New(ByVal vID As Integer)
            Dim ta As New ArticlesPubmedDataSetTableAdapters.pmMeSHTableAdapter
            Dim tbl = ta.GetDataByID(vID)
            ID = tbl(0).MeSH_ID
            Title = tbl(0).Heading
        End Sub

    End Class
End Namespace

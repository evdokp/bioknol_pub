﻿Namespace BLL


    Public Class SearchHelper
        Public Shared Function Text_CONTAINS_Arg2_Or(ByVal words As String) As String
            Dim s As String = ""
            words = words.Trim
            words = Regex.Replace(words, " {2,50}", " ")
            Dim isFirstCondition As Boolean = True
            Dim pw, nw As String
            pw = MakeOrBlock(PositiveWords(words))
            nw = MakeOrBlock(NegativeWords(words))
            If pw <> "" Then
                s += "(" + pw + ")"
                If nw <> "" Then
                    s += " AND NOT (" & nw & ")"
                End If
            Else
                If nw <> "" Then
                    s += "NOT (" & nw & ")"
                End If
            End If
            Return s
        End Function
        Public Shared Function Text_CONTAINS_Arg2_And(ByVal words As String) As String
            Dim mc As MatchCollection
            Dim s As String = ""
            'mc = Regex.Matches(words, "\W.{1,500}?(,|$)")
            words = RemoveExtraSpaces(words)
            mc = Regex.Matches(words, ".{2,500}?(,|$)")
            Dim i As Integer = 0
            For Each m As Match In mc
                Dim nc As String
                nc = Text_CONTAINS_Arg2_And_SingleRegion(m.Value)
                If i > 0 And nc <> "" And s <> "" Then
                    s += " OR "
                End If
                s += nc
                i += 1
            Next
            's = Text_CONTAINS_Arg2_And_SingleRegion(words)
            Return s
        End Function
        Public Shared Function Text_CONTAINS_Arg2_And_SingleRegion(ByVal words As String) As String
            Dim s As String = ""
            words = words.Trim
            'words = Regex.Replace(words, "\s", " ")
            words = Regex.Replace(words, " {2,50}", " ")
            Dim isFirstCondition As Boolean = True
            Dim pw, nw As String
            pw = MakeAndBlock(PositiveWords(words))
            nw = MakeOrBlock(NegativeWords(words))
            If pw <> "" Then
                s += String.Format("({0})", pw)
                If nw <> "" Then
                    s += String.Format(" AND NOT ({0})", nw)
                End If
            Else
                'If nw <> "" Then
                '    s += "NOT (" & nw & ")"
                'End If
            End If
            Return s
        End Function
        Public Shared Function PositiveWords(ByRef ss As String) As List(Of String)
            Dim sl As New List(Of String)
            Dim mc As MatchCollection
            Dim m As Match
            ' сначала без ковычек
            mc = Regex.Matches(ss, "(?<![""-])\b\w{1,50}\b(?!"")", RegexOptions.IgnoreCase)
            For Each m In mc
                sl.Add("""" & m.Value & "*""")
            Next
            mc = Regex.Matches(ss, "(?<!-"")(?<="")\b\w{1,50}\b(?="")", RegexOptions.IgnoreCase)
            For Each m In mc
                sl.Add("""" & m.Value & "*""")
            Next
            Return sl
        End Function
        Public Shared Function NegativeWords(ByRef ss As String) As List(Of String)
            Dim sl As New List(Of String)
            Dim mc As MatchCollection
            Dim m As Match
            ' сначала без ковычек
            mc = Regex.Matches(ss, "(?<=-)\b\w{1,50}\b(?!"")", RegexOptions.IgnoreCase)
            For Each m In mc
                sl.Add("""" & m.Value & "*""")
            Next
            mc = Regex.Matches(ss, "(?<=-"")\b\w{1,50}\b(?="")", RegexOptions.IgnoreCase)
            For Each m In mc
                sl.Add("""" & m.Value & "*""")
            Next
            Return sl
        End Function
        Public Shared Function MakeAndBlock(ByRef sl As List(Of String)) As String
            Dim s As String = ""
            If sl.Count = 1 Then
                s = sl(0)
            ElseIf sl.Count > 1 Then
                s = sl(0)
                For i = 1 To sl.Count - 1
                    s += " AND " & sl(i)
                Next
            End If
            Return s
        End Function
        Public Shared Function MakeOrBlock(ByRef sl As List(Of String)) As String
            Dim s As String = ""
            If sl.Count = 1 Then
                s = sl(0)
            ElseIf sl.Count > 1 Then
                s = sl(0)
                For i = 1 To sl.Count - 1
                    s += " OR " & sl(i)
                Next
            End If
            Return s
        End Function
        Shared Function RemoveExtraSpaces(ByVal so As String) As String
            Dim s As String = ""
            s = so.Replace(vbCrLf, " ")
            s = s.Replace("&nbsp;", " ")
            s = s.Replace(vbCr, " ")
            s = s.Replace(vbLf, " ")
            s = s.Replace("  ", " ")

            While Not so.Equals(s)
                so = s
                s = so.Replace("  ", " ")
            End While
            s = s.TrimStart
            s = s.TrimEnd
            Return s
        End Function
    End Class
End Namespace
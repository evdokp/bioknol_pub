﻿Imports System.Net
Imports System.IO
Imports System.Threading
Imports BioWS.BLL.Extensions


Namespace BLL
    Public Class Widgets

        Public Shared Function ValidateBooleanResponse(ByVal boolURL As String, pmIDs As String) As String


            Dim res As String
            Dim wrq As WebRequest = WebRequest.Create(boolURL)
            wrq.Method = "POST"
            Dim postData As String = "PubmedIDs=" & pmIDs
            wrq.ContentType = "Application/x-www-form-urlencoded"
            Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
            wrq.ContentLength = byteArray.Length
            Dim dataStream As Stream = wrq.GetRequestStream()
            dataStream.Write(byteArray, 0, byteArray.Length)
            dataStream.Close()
            Try
                Dim response As WebResponse = wrq.GetResponse()
                Console.WriteLine(CType(response, HttpWebResponse).StatusDescription)
                dataStream = response.GetResponseStream()
                Dim reader As New StreamReader(dataStream)
                Dim responseFromServer As String = reader.ReadToEnd()
                Dim respInteger As Integer

                If Integer.TryParse(responseFromServer, respInteger) Then
                    If respInteger = 0 Then
                        res = "no information available"
                    Else
                        res = "additional information available"
                    End If
                Else
                    res = "unable to parse response"
                End If


                reader.Close()
                dataStream.Close()
                response.Close()
            Catch ex As Exception
                res = ex.Message
            End Try

            Return res
        End Function


        Private Shared Function BooleanResponseFromInfoWS(ByVal vURL As String, ByVal PMID As Integer) As Boolean
            Dim res As Boolean
            Dim wrq As WebRequest = WebRequest.Create(vURL)
            wrq.Method = "POST"
            Dim postData As String = "PubmedIDs=" & PMID
            wrq.ContentType = "Application/x-www-form-urlencoded"
            Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
            wrq.ContentLength = byteArray.Length
            Dim dataStream As Stream = wrq.GetRequestStream()
            dataStream.Write(byteArray, 0, byteArray.Length)
            dataStream.Close()
            Dim response As WebResponse = wrq.GetResponse()
            Console.WriteLine(CType(response, HttpWebResponse).StatusDescription)
            dataStream = response.GetResponseStream()
            Dim reader As New StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()
            Dim respInteger As Integer
            respInteger = CInt(responseFromServer)

            If respInteger = 0 Then
                res = False
            Else
                res = True
            End If

            reader.Close()
            dataStream.Close()
            response.Close()
            Return res
        End Function
        Public Shared Function IsAdditionalInfoAvailableByWidget(ByVal vPMID As Integer, ByVal vWidgetID As Integer) As Integer
            Try
                Dim PMID As Integer
                PMID = vPMID
                If PMID > 0 Then
                    'this is pubmed article - we can work with it
                    Dim ta As New WidgetsDataSetTableAdapters.WidgetsTableAdapter
                    Dim t = ta.GetDataByID(vWidgetID)
                    Dim r As WidgetsDataSet.WidgetsRow
                    r = t.Item(0)
                    'set variable to false
                    Dim NewInfoAvailable As Boolean
                    NewInfoAvailable = False
                    Dim i As Integer
                    If r.IsInternal = False Then
                        'check external widgets
                        NewInfoAvailable = BooleanResponseFromInfoWS(r.Info_WS_URL, PMID)
                    Else
                        'check internal widgets
                        Select Case r.InternalConst
                            Case "WhatOthersReadWithIt"
                                NewInfoAvailable = WhatOthersReadWithIt_Boolean(PMID)
                        End Select
                    End If
                    'return result
                    If NewInfoAvailable = True Then
                        Return 1
                    Else
                        Return 0
                    End If
                Else
                    Return 0 'no
                End If
            Catch ex As Exception
                Return 0 'no
            End Try
        End Function
        Public Shared Function IsAdditionalInfoAvailableExtracted(ByVal vPersonID As Integer, ByVal PMID As Integer) As Integer
            If PMID > 0 And vPersonID > 0 Then
                'this is pubmed article - we can work with it
                Dim ta As New WidgetsDataSetTableAdapters.WidgetsTableAdapter
                Dim t = ta.GetDataByPersonID_ON(vPersonID)
                If t.Count > 0 Then
                    Dim r As WidgetsDataSet.WidgetsRow
                    'set variable to false
                    Dim NewInfoAvailable As Boolean
                    NewInfoAvailable = False
                    Dim i As Integer
                    For i = 0 To t.Count - 1
                        r = t.Item(i)
                        If r.IsInternal = False Then
                            'check external widgets
                            NewInfoAvailable = BooleanResponseFromInfoWS(r.Info_WS_URL, PMID)
                        Else
                            'check internal widgets
                            Select Case r.InternalConst
                                Case "WhatOthersReadWithIt"
                                    NewInfoAvailable = WhatOthersReadWithIt_Boolean(PMID)
                            End Select
                        End If
                        If NewInfoAvailable = True Then
                            Exit For
                        End If
                    Next
                    'return result
                    If NewInfoAvailable = True Then
                        Return 1
                    Else
                        Return 0
                    End If

                Else
                    Return 0 'no
                End If
            Else
                Return 0 'no
            End If
        End Function

        Public Shared Function WhatOthersReadWithIt_Boolean(ByVal vPMID As Integer) As Boolean
            Dim ta As New ArticlesPubmedDataSetTableAdapters.ArticlesPubmedTableAdapter
            If ta.GetViewedTogetherHINT_count(2, vPMID) > 0 Then
                Return True
            Else
                Return False
            End If
            Return False
        End Function
        Protected Shared Function IsWidgetOn(ByVal widgetID As Integer, ByVal personID As Integer) As Boolean
            Dim widgta As New WidgetsDataSetTableAdapters.WidgetsTableAdapter
            If widgta.IsWidgetOn(widgetID, personID) > 0 Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Class Widget

            Public Property Widget_ID() As Integer
            Public Property Title() As String
            Public Property Description() As String
            Public Property Info_WS_URL() As String
            Public Property Data_WS_URL() As String
            Public Property InternalConst() As String
            Public Property IsInternal() As Boolean
            Public Property AllowMultiple() As Boolean
            Public Property AddedByPersonID() As Integer
            Public Property AddedDate() As Date
            Public Property IsApproved() As Boolean
            Public Property Version() As String


            Public ReadOnly Property URL() As String
                Get
                    Return "~\Account\Widget\ViewWidget.aspx?WidgetID=" & Widget_ID
                End Get
            End Property
            Public ReadOnly Property EditURL() As String
                Get
                    Return "~\Account\Widget\AddEditWidget.aspx?WidgetID=" & Widget_ID
                End Get
            End Property
            Public ReadOnly Property StatisticsURL() As String
                Get
                    Return "~\Account\Widget\ViewWidgetStatistics.aspx?WidgetID=" & Widget_ID
                End Get
            End Property

            Public Function ExternalResponse(ByVal vInput As String, ByRef ContentType As String)
                Dim wrq As WebRequest = WebRequest.Create(Data_WS_URL)
                wrq.Method = "POST"
                Dim postData As String = "PubmedIDs=" & vInput
                wrq.ContentType = "Application/x-www-form-urlencoded"
                Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
                wrq.ContentLength = byteArray.Length
                Dim dataStream As Stream
                Try
                    dataStream = wrq.GetRequestStream()
                Catch ex As Exception
                    dataStream = Nothing
                End Try
                If dataStream IsNot Nothing Then
                    dataStream.Write(byteArray, 0, byteArray.Length)
                    dataStream.Close()
                    Dim response As WebResponse
                    Try
                        response = wrq.GetResponse()
                    Catch ex As Exception
                        response = Nothing
                    End Try

                    Dim ct As String = response.ContentType
                    If ct.Split(";").Count > 1 Then
                        ct = ct.Split(";")(0)
                    End If
                    ContentType = ct
                    Select Case ct
                        Case "image/png"
                            dataStream = response.GetResponseStream()
                            Dim reader As New StreamReader(dataStream)
                            Dim b As Byte()
                            b = ReadFully(reader.BaseStream)
                            Return b
                        Case "text/plain"
                            Console.WriteLine(CType(response, HttpWebResponse).StatusDescription)
                            dataStream = response.GetResponseStream()
                            Dim reader As New StreamReader(dataStream)
                            Dim responseFromServer As String = reader.ReadToEnd()
                            reader.Close()
                            dataStream.Close()
                            response.Close()
                            Return responseFromServer
                        Case "text/html"
                            Console.WriteLine(CType(response, HttpWebResponse).StatusDescription)
                            dataStream = response.GetResponseStream()
                            Dim reader As New StreamReader(dataStream)
                            Dim responseFromServer As String = reader.ReadToEnd()
                            reader.Close()
                            dataStream.Close()
                            response.Close()
                            Return responseFromServer
                        Case Else
                            Return Nothing
                    End Select
                Else
                    Return Nothing

                End If

            End Function

            Private _AddedByPerson As BLL.Person
            Public ReadOnly Property AddedByPerson() As BLL.Person
                Get
                    If _AddedByPerson Is Nothing Then
                        _AddedByPerson = New BLL.Person(AddedByPersonID)
                    End If
                    Return _AddedByPerson
                End Get
            End Property

            Sub New(ByVal Widget_ID As Integer)
                Dim ta As New WidgetsDataSetTableAdapters.WidgetsTableAdapter
                Dim t = ta.GetDataByID(Widget_ID)
                If t.Count > 0 Then
                    Dim r = t.Item(0)
                    Me.Widget_ID = Widget_ID
                    Title = r.Title
                    Description = r.Description
                    Info_WS_URL = r.Info_WS_URL
                    Data_WS_URL = r.Data_WS_URL
                    InternalConst = r.InternalConst
                    IsInternal = r.IsInternal
                    AllowMultiple = r.AllowMultiple
                    AddedByPersonID = r.AddedByPersonID
                    IsApproved = r.IsApproved
                    AddedDate = r.AddedDate
                    Version = r.Version
                End If
            End Sub

            Public Shared Function ReadFully(ByVal stream As Stream) As Byte()
                Dim buffer As Byte() = New Byte(32767) {}
                Using ms As New MemoryStream()
                    While True
                        Dim read As Integer = stream.Read(buffer, 0, buffer.Length)
                        If read <= 0 Then
                            Return ms.ToArray()
                        End If
                        ms.Write(buffer, 0, read)
                    End While
                End Using
            End Function

            Public Function ToggleOnOff(ByVal personID As Integer) As Boolean
                Dim res As Boolean
                Dim ta As New WidgetsDataSetTableAdapters.LinksPersonsToWidgetsTableAdapter
                Dim t = ta.GetDataByWidgetIDPersonID(personID, Widget_ID)
                If t.Count = 0 Then
                    'turn on
                    ta.Insert(personID, Widget_ID, True)
                    res = True
                Else
                    Dim r = t.Item(0)
                    If r.IsOn = True Then
                        'turn off
                        r.BeginEdit()
                        r.IsOn = False
                        r.EndEdit()
                        ta.Update(r)

                        Dim feedAction As New BLL.Feed.FeedActionWidgetOff() With {.WidgetID = Widget_ID}
                        feedAction.Save(personID)
                        res = False
                    Else
                        'turn on
                        r.BeginEdit()
                        r.IsOn = True
                        r.EndEdit()
                        ta.Update(r)

                        Dim feedAction As New BLL.Feed.FeedActionWidgetOn() With {.WidgetID = Widget_ID}
                        feedAction.Save(personID)
                        res = True
                    End If
                End If

                Return res
            End Function


            Public Sub Log()
                Dim logTA As New WidgetsDataSetTableAdapters.WidgetLogTableAdapter
                logTA.LogWidget(Widget_ID)
            End Sub

            Public Function IsOnForPerson(ByVal personID As Integer) As Boolean
                Return IsWidgetOn(Widget_ID, personID)
            End Function

            Public Sub New(vTitle As String,
                            vDescription As String,
                            vInfo_WS_URL As String,
                            vData_WS_URL As String,
                            vInternalConst As String,
                            vIsInternal As Boolean,
                            vAllowMultiple As Boolean,
                            vAddedByPersonID As Integer,
                            vVersion As String)



                Title = vTitle
                Description = vDescription
                Info_WS_URL = vInfo_WS_URL
                Data_WS_URL = vData_WS_URL
                InternalConst = vInternalConst
                IsInternal = vIsInternal
                AllowMultiple = vAllowMultiple
                AddedByPersonID = vAddedByPersonID
                AddedDate = Now
                IsApproved = False
                Version = vVersion


                Dim widgetTA As New WidgetsDataSetTableAdapters.WidgetsTableAdapter
                widgetTA.InsertWidget(Title,
                    Description,
                    Info_WS_URL,
                    Data_WS_URL,
                    InternalConst,
                    IsInternal,
                    AllowMultiple,
                    AddedByPersonID,
                    Now,
                    IsApproved,
                    Version,
                    Widget_ID)


            End Sub

            Public Sub Save()
                Dim ta As New WidgetsDataSetTableAdapters.WidgetsTableAdapter
                Dim t = ta.GetDataByID(Widget_ID)

                Dim r = t.Item(0)
                r.BeginEdit()

                r.Title = Title
                r.Description = Description
                r.Info_WS_URL = Info_WS_URL
                r.Data_WS_URL = Data_WS_URL
                r.InternalConst = InternalConst
                r.IsInternal = IsInternal
                r.AllowMultiple = AllowMultiple
                r.AddedByPersonID = AddedByPersonID
                r.IsApproved = IsApproved
                r.AddedDate = AddedDate
                r.Version = Version

                r.EndEdit()
                ta.Update(r)

            End Sub
        End Class

    End Class
End Namespace
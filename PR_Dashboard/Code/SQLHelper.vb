﻿Imports System.Data
Imports System.Data.SqlClient

Namespace BLL
    Public Class SQLHelper

        Public Shared Function GetDT_AllStrings(queryString As String) As DataTable
            Dim conString = ConfigurationManager.ConnectionStrings("bio_conntection_string").ConnectionString
            Using connection As New SqlConnection(conString)
                Dim command As New SqlCommand(queryString, connection) With {.CommandTimeout = 30000}
                connection.Open()
                Dim reader As SqlDataReader = command.ExecuteReader()
                Dim dt As New DataTable()
                Dim fc As Integer = reader.FieldCount
                Dim i As Integer = 0
                While i < fc
                    Dim col As New DataColumn() With {.DataType = Type.[GetType]("System.String")}
                    Dim colName As String = reader.GetName(i)
                    col.ColumnName = colName
                    dt.Columns.Add(col)
                    System.Math.Max(System.Threading.Interlocked.Increment(i), i - 1)
                End While
                Try
                    ' adding rows
                    Dim lastRowDT As DateTime = DateTime.MinValue
                    While reader.Read()
                        Dim r As DataRow = Nothing
                        r = dt.NewRow()
                        i = 0
                        While i < fc
                            r(i) = reader(i).ToString()
                            System.Math.Max(System.Threading.Interlocked.Increment(i), i - 1)
                        End While
                        dt.Rows.Add(r)
                    End While
                Finally
                    reader.Close()
                End Try
                Return dt
            End Using




        End Function


    End Class
End Namespace


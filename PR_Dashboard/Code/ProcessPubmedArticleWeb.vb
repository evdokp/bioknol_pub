﻿Imports BioKnol.PubmedParsingLibrary
Public Class ProcessPubmedArticleWeb
    Public Shared Sub ProcessPMID(ByVal vPMID As Integer, ByVal reprocess As Boolean)
        Try
            Dim pmService As New PubmedService()
            Dim result = pmService.Parse(vPMID)

            Using ta As New ArticlesPubmedDataSetTableAdapters.ArticlesPubmedTableAdapter()

                Dim journalID As Integer = GetJournalID(result.Journal)


                If ta.GetCountByPubmedID(vPMID) = 0 Then
                    'process new
                    reprocess = False
                Else
                    'reprocess old
                    reprocess = True
                End If


                If reprocess = False Then
                    ta.Insert(vPMID,
                              result.ArticleTitle,
                              Now(),
                              result.ArticleAbstract,
                              result.DateCreated,
                              result.DateRevised,
                              result.DateCompleted,
                              journalID,
                              result.IssueYear)
                Else
                    ta.UpdateQuery(result.ArticleTitle,
                                          Now(),
                                          result.ArticleAbstract,
                                          result.DateCreated,
                                          result.DateRevised,
                                          result.DateCompleted,
                                          journalID,
                                          result.IssueYear,
                                          vPMID)
                End If

                ProcessAuthorsList(vPMID, result.AuthorsList)
                ProcessMeshHeadingList(vPMID, result.MeshHeadingsList)

            End Using
        Catch err As Exception
            Dim er_ws As New wref_LogError_WS.LogError_WS
            er_ws.LogError_WS_func("ProcessPMID " & vPMID & ": " & err.Message, 1000, 1000)
        End Try

        ' er_ws.LogError_WS_func("-- ProcessPMID - end", 1000, 1000)

    End Sub

    Private Shared Sub ProcessMeshHeadingList(vPMID As Integer, meshHeadingsList As List(Of String))
        Using ta As New ArticlesPubmedDataSetTableAdapters.LinksArticlesToMeSHTableAdapter()
            Using meshta As New ArticlesPubmedDataSetTableAdapters.pmMeSHTableAdapter()
                ta.DeleteByPubmedID(vPMID)
                For Each MeshHeading In meshHeadingsList
                    Dim MeshID As Integer
                    Dim t = meshta.GetDataByHeading(MeshHeading)
                    If t.Count = 0 Then
                        Dim i As Integer
                        meshta.InsertMeSH(MeshHeading, i)
                        MeshID = i
                    Else
                        MeshID = t.Item(0).MeSH_ID
                    End If
                    ta.Insert(vPMID, MeshID)
                Next
            End Using
        End Using
    End Sub

    Private Shared Sub ProcessAuthorsList(vPMID As Integer, authorsList As List(Of AuthorParsingResult))

        Using ta As New ArticlesPubmedDataSetTableAdapters.LinksArticlesToAuthorsTableAdapter()
            ta.DeleteByPubmedID(vPMID)
            If authorsList IsNot Nothing Then
                Dim authorsOrder As Integer? = 1
                For Each author In authorsList
                    Dim Author_ID As Integer = ProcessAuthor(author)
                    ta.Insert(vPMID, Author_ID, authorsOrder)
                    authorsOrder = authorsOrder + 1
                Next
            End If
        End Using

    End Sub
    Private Shared Function ProcessAuthor(ByVal Author As AuthorParsingResult)
        Using ta As New ArticlesPubmedDataSetTableAdapters.pmAuthorsTableAdapter()
            Dim t = ta.GetDataByLastForeInitials(Author.Last, Author.Fore, Author.Initials)
            If t.Count = 0 Then
                Dim i As Integer
                ta.InsertAuthor(Author.Last, Author.Fore, Author.Initials, Author.Full, i)
                Return i
            Else
                Return t.Item(0).Author_ID
            End If
        End Using
    End Function

    Private Shared Function GetJournalID(journal As JournalParsingResult) As Integer
        Using ta As New ArticlesPubmedDataSetTableAdapters.pmJournalsTableAdapter()
            Dim t = ta.GetDataByTitleAndISO(journal.Title, IIf(journal.ISOAbbreviation Is Nothing, String.Empty, journal.ISOAbbreviation))
            Dim sCountry As String
            If journal.Country IsNot Nothing Then
                sCountry = journal.Country
                sCountry = sCountry.ToUpper()
            Else
                sCountry = String.Empty
            End If


            If t.Count = 0 Then
                Dim i As Integer
                ta.InsertJournal(journal.Title, IIf(journal.ISOAbbreviation Is Nothing, String.Empty, journal.ISOAbbreviation), sCountry, i)
                Return i
            Else
                Dim r = t.Item(0)
                'r.BeginEdit()
                'r.Country = sCountry
                'r.EndEdit()
                'ta.Update(r)
                Return r.Journal_ID
            End If
        End Using
    End Function
End Class

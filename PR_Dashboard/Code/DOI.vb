﻿Imports BioWS.BLL.Extensions.Web

Namespace BLL

    Public Class DOIItem
        Public Property DOI As String
        Public ReadOnly Property URL As String
            Get
                Return "http://dx.doi.org/" & DOI
            End Get
        End Property
        Public Function GetTitle() As String
            Dim html = URL.GetHTML()
            Return html.GetTitleTagText()
        End Function
        Sub New(ByVal doistring As String)
            DOI = doistring
        End Sub
    End Class

    Public Class DOI
        Public Shared Function GetDOIbyURL_internal(ByVal vURL As String) As String
            Dim ta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
            Dim res As String
            ta.GetDOIbyURL(vURL, res)
            Return res
        End Function
        Public Shared Function GetDOIFromHTML(ByVal vHTML As String) As String
            Dim s As String
            s = Regex.Match(vHTML, "(?<=http://dx.doi.org/).{0,100}?(?="")", RegexOptions.IgnoreCase).Value
            Return s.Trim
        End Function
    End Class
End Namespace

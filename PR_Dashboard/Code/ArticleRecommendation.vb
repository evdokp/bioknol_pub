Namespace BLL
    Public Class ArticleRecommendation
        Public TimeStamp As String
        Public Author As String
        Public AuthorID As Integer
        Public PersonallyAddressed As Integer
        Public IsViewerPerson As Integer
        Public HasAddressedRecs As Integer
        Public SubRecsCount As Integer
        Public AvatarURL As String
    End Class

End Namespace

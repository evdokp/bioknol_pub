﻿Namespace BLL


    Public Class ArticleComment
        Public TimeStamp As String
        Public Author As String
        Public AuthorID As Integer
        Public Comment As String
        Public AvatarURL As String
    End Class
End Namespace
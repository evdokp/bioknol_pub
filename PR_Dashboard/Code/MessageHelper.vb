﻿Imports BioWS.BLL.Extensions
Namespace BLL
    Public Class MessageHelper

        Public Shared Sub Send(ByVal personFromID As Integer?,
            ByVal recipients As List(Of Integer),
            ByVal subject As String,
            ByVal body As String,
            ByVal p As Page)

            Dim messagesta As New DataSetMessagesTableAdapters.MessagesTableAdapter
            Dim newMessageID As Integer

            If subject.IsNullOrEmpty() Then
                subject = body.StripTags().SubstringToLength(60)
            End If




            Dim persFromFIO As String
            If personFromID Is Nothing Then
                persFromFIO = "PRD mail service"
            Else
                Dim persFROM As New BLL.Person(CInt(personFromID))
                persFromFIO = persFROM.PageHeader
            End If


            For Each personToID As Integer In recipients
                messagesta.InsertMessage(personFromID, personToID, subject, body, newMessageID)
                Dim persTO As New BLL.Person(personToID)

                'send mail
                Dim vBody As String
                vBody = BLL.MailSender.ReadFile.ReadFile(p.Server, "NewMessage.htm")
                Dim perstoFIO As String = IIf(persTO.FirstName.Length = 0, "PRD user", persTO.FirstName)
                vBody = vBody.Replace("<%FIO%>", perstoFIO)
                vBody = vBody.Replace("<%From%>", persFromFIO)
                vBody = vBody.Replace("<%Title%>", subject)
                vBody = vBody.Replace("<%Content%>", body)


                If personFromID IsNot Nothing Then
                    vBody = vBody.Replace("<%ReplyURL%>", String.Format("<p>You can reply to this message on <a href=""http://ws.bioknowledgecenter.ru/Account/Messages/ComposeMessage.aspx?replyOnID={0}&PersonToID={1}"">this page</a></p>", newMessageID, personFromID))
                Else
                    vBody = vBody.Replace("<%ReplyURL%>", "")
                End If

                Try
                    BLL.MailSender.SendMailMessage("noreply@bioknowledgecenter.ru",
                    "PRD mail service",
                    persTO.UserName,
                    String.Empty,
                    String.Empty,
                    "New message on Personal Reference Dashboard website",
                    vBody)
                Catch ex As Exception
                    ' nothing
                End Try



            Next


        End Sub
    End Class
End Namespace



﻿Imports System.Reflection

Namespace BLL.Emails
    Public Class Recommendation

        Public Property hAvatarURL As String
        Public Property hPersonURL As String
        Public Property hDisplayName As String
        Public Property hComment As String
        Public Property hArticleTitle As String
        Public Property hArticleURL As String
        Public Property hFulltext As String
        Private Property EmailBody As String
        Private Property EmailTitle As String
        Private Property RecID As Integer

        Sub New(ByVal personFromID As Integer, ByVal vcomment As String, ByVal vrecID As Integer)

            RecID = vrecID


            '!+ Person's properties
            Dim personfrom As New BLL.Person(personFromID)
            hAvatarURL = personfrom.Image50abs
            hPersonURL = personfrom.URLabs
            hDisplayName = personfrom.DisplayName

            '!+ Comment
            If vcomment.Length > 0 Then
                hComment = String.Format("<div class=""comment"">{0}</div>", vcomment)
            End If

            '!+ Recommendation properties
            Using recta As New ArticlesDataSetTableAdapters.ArticleRecommendationsTableAdapter()
                Dim recr = recta.GetDataByID(RecID)(0)
                If Not recr.IsPubmedIDNull Then
                    Dim pma As New BLL.PubMed.PubMedArticle(recr.PubmedID)
                    hArticleTitle = pma.Title
                    hArticleURL = pma.PubmedURL
                Else
                    Dim doi As New DOIItem(recr.DOI)
                    hArticleTitle = recr.Title
                    hArticleURL = doi.URL
                End If
            End Using

            '!+ Fulltext
            Using offersta As New ArticlesDataSetTableAdapters.ArticleOffersTableAdapter()
                Dim offt = offersta.GetWithFilesByRecommendationID(RecID)
                If offt.Count > 0 Then
                    Dim offerRow = offt.Item(0)
                    Dim link As String = "http://ws.bioknowledgecenter.ru/Download.ashx?vFileName=" & HttpUtility.UrlEncode(offerRow.FileName) & "&vOriginalName=" & HttpUtility.UrlEncode(offerRow.OriginalName)
                    hFulltext = "<div style=""margin-top:20px;""><a href=""" & link & """ style=""padding:5px; background-color:green; color:white; font-weight:bold"">Get full text</a></div>"
                End If
            End Using


            '! now, based on what we know - we form body
            EmailBody = GetBody()
            EmailTitle = hDisplayName & " recommends you an article on BioKnol"
        End Sub

        Public Sub SendToEmailsList(ByVal emails As List(Of String))
            For Each email As String In emails
                SendToEmail(email)
            Next
        End Sub

        Private Sub SendToEmail(ByVal email As String)
            Try
                '! try send mail
                BLL.MailSender.SendMailMessage("noreply@bioknowledgecenter.ru", _
                                           "BioKnol mail service", _
                                           email.Trim, _
                                           String.Empty, _
                                           String.Empty, _
                                           EmailTitle, _
                                           EmailBody)

                '! check email table record
                Using ta As New ArticlesDataSetTableAdapters.ArticleRecommendationsToEmailsTableAdapter()
                    Dim t = ta.GetDataByTwoIDs(RecID, email)
                    If t.Count() = 0 Then
                        ta.Insert(RecID, email, DateTime.Now)
                    Else
                        Dim row = t(0)
                        row.BeginEdit()
                        row.TimeStamp = Now
                        row.EndEdit()
                        ta.Update(row)
                    End If
                End Using
            Catch ex As Exception
                'nothing yet
            End Try
        End Sub

        Private Function GetBody() As String
            Dim vBody As String = BLL.MailSender.ReadFile.ReadFile(HttpContext.Current.Server, "Recommendation.htm")
            For Each _Property As PropertyInfo In Me.GetType().GetProperties()
                If _Property.Name.StartsWith("h") Then
                    vBody = vBody.Replace(String.Format("<%{0}%>", _Property.Name), _Property.GetValue(Me, Nothing))
                End If
            Next
            Return vBody
        End Function

    End Class
End Namespace


﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Stat4_mesh.aspx.vb" Inherits="BioWS.Stat4_mesh" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallbackPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
            <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="100%" 
                ClientInstanceName="cpMain" 
                CssFilePath="~/App_Themes/SoftOrange/{0}/styles.css" CssPostfix="SoftOrange" 
                LoadingPanelImagePosition="Top">
                <LoadingPanelImage Url="~/App_Themes/SoftOrange/Web/Loading.gif">
                </LoadingPanelImage>
            <PanelCollection>
<dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">

   
        <table width="100%">
            <tr>
                <td valign="top" style="width:330px;">
                     <table>
            <tr>
                <td valign="top">
                    <dx:ASPxDateEdit ID="ASPxDateEdit1" runat="server" CssFilePath="~/App_Themes/SoftOrange/{0}/styles.css"
                        CssPostfix="SoftOrange"  DisplayFormatString="dd MMMM yyyy г., dddd" 
                                    EditFormat="Custom"  
                                    EditFormatString="dd.MM.yyyy" Width="220px"
                                    UseMaskBehavior="True"
                        SpriteCssFilePath="~/App_Themes/SoftOrange/{0}/sprite.css" >
                        <CalendarProperties>
                            <HeaderStyle Spacing="1px" />
                            <FooterStyle Spacing="17px" />
                        </CalendarProperties>
                        <ButtonStyle Cursor="pointer" Width="11px">
                        </ButtonStyle>
                        <ValidationSettings>
                            <ErrorFrameStyle ImageSpacing="4px">
                                <ErrorTextPaddings PaddingLeft="4px" />
                            </ErrorFrameStyle>
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                </td>
            </tr>
            <tr>
                  <td valign="top">
                    <dx:ASPxDateEdit ID="ASPxDateEdit2" runat="server" CssFilePath="~/App_Themes/SoftOrange/{0}/styles.css"
                        CssPostfix="SoftOrange"  DisplayFormatString="dd MMMM yyyy г., dddd" 
                                    EditFormat="Custom"   Width="220px"
                                    EditFormatString="dd.MM.yyyy" 
                                    UseMaskBehavior="True"
                        SpriteCssFilePath="~/App_Themes/SoftOrange/{0}/sprite.css" >
                        <CalendarProperties>
                            <HeaderStyle Spacing="1px" />
                            <FooterStyle Spacing="17px" />
                        </CalendarProperties>
                        <ButtonStyle Cursor="pointer" Width="11px">
                        </ButtonStyle>
                        <ValidationSettings>
                            <ErrorFrameStyle ImageSpacing="4px">
                                <ErrorTextPaddings PaddingLeft="4px" />
                            </ErrorFrameStyle>
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                </td>
            </tr>
            <tr>
            <td>
            <dx:ASPxButton ID="btnRefreshList" runat="server" 
                    CssFilePath="~/App_Themes/SoftOrange/{0}/styles.css" CssPostfix="SoftOrange" 
                    SpriteCssFilePath="~/App_Themes/SoftOrange/{0}/sprite.css" 
                    Text="Reload MESH-headings list" Width="218px" AutoPostBack="False">
                <ClientSideEvents Click="function(s, e) {
	cpMain.PerformCallback(1);
}" />
                </dx:ASPxButton>
            </td>
            </tr>
        </table>


                <div style="height:500px;  overflow-y:auto;  width:320px; border:1px solid gray;">
                    <dx:ASPxCheckBoxList ID="ASPxCheckBoxList1" runat="server" 
                        DataSourceID="ObjectDataSource1" Height="500px" TextField="Heading" 
                        ValueField="MeSH_ID" Width="300px" ClientInstanceName="chkListMesh">
                        <Border BorderStyle="None" />
                    </dx:ASPxCheckBoxList>
               
                    </div>
                         <dx:ASPxButton ID="ASPxButton1" runat="server" AutoPostBack="False"  style="margin-top:7px;"
                        CssFilePath="~/App_Themes/SoftOrange/{0}/styles.css" CssPostfix="SoftOrange" 
                        SpriteCssFilePath="~/App_Themes/SoftOrange/{0}/sprite.css" Text="Unselect all">
                             <ClientSideEvents Click="function(s, e) {
	chkListMesh.UnselectAll();
}" />
                    </dx:ASPxButton>
                </td>
                <td valign="top">

                     <table>
            <tr>
                <td valign="top" >
                    <dx:ASPxDateEdit ID="ASPxDateEdit3" runat="server" CssFilePath="~/App_Themes/SoftOrange/{0}/styles.css"
                        CssPostfix="SoftOrange"  DisplayFormatString="dd MMMM yyyy г., dddd" 
                                    EditFormat="Custom"  
                                    EditFormatString="dd.MM.yyyy" Width="220px"
                                    UseMaskBehavior="True"
                        SpriteCssFilePath="~/App_Themes/SoftOrange/{0}/sprite.css" >
                        <CalendarProperties>
                            <HeaderStyle Spacing="1px" />
                            <FooterStyle Spacing="17px" />
                        </CalendarProperties>
                        <ButtonStyle Cursor="pointer" Width="11px">
                        </ButtonStyle>
                        <ValidationSettings>
                            <ErrorFrameStyle ImageSpacing="4px">
                                <ErrorTextPaddings PaddingLeft="4px" />
                            </ErrorFrameStyle>
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                </td>
            </tr>
            <tr>
                  <td valign="top">
                    <dx:ASPxDateEdit ID="ASPxDateEdit4" runat="server" CssFilePath="~/App_Themes/SoftOrange/{0}/styles.css"
                        CssPostfix="SoftOrange"  DisplayFormatString="dd MMMM yyyy г., dddd" 
                                    EditFormat="Custom"   Width="220px"
                                    EditFormatString="dd.MM.yyyy" 
                                    UseMaskBehavior="True"
                        SpriteCssFilePath="~/App_Themes/SoftOrange/{0}/sprite.css" >
                        <CalendarProperties>
                            <HeaderStyle Spacing="1px" />
                            <FooterStyle Spacing="17px" />
                        </CalendarProperties>
                        <ButtonStyle Cursor="pointer" Width="11px">
                        </ButtonStyle>
                        <ValidationSettings>
                            <ErrorFrameStyle ImageSpacing="4px">
                                <ErrorTextPaddings PaddingLeft="4px" />
                            </ErrorFrameStyle>
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                </td>
            </tr>
            <tr>
            <td>
            Select groupping interval:
            <asp:DropDownList ID="ddl_grint" runat="server" >
                    <asp:ListItem Text="Day" Value="Day" ></asp:ListItem>
                    <asp:ListItem Text="Week" Value="Week" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="2 Weeks" Value="2 Weeks"></asp:ListItem>
                    <asp:ListItem Text="Month" Value="Month"></asp:ListItem>
                </asp:DropDownList>
                </td>
            </tr>
            <tr>
            <td>
            <dx:ASPxButton ID="btnRedrawChart" runat="server" 
                    CssFilePath="~/App_Themes/SoftOrange/{0}/styles.css" CssPostfix="SoftOrange" 
                    SpriteCssFilePath="~/App_Themes/SoftOrange/{0}/sprite.css" 
                    Text="Redraw chart" Width="218px" AutoPostBack="False">
                <ClientSideEvents Click="function(s, e) {
	cpMain.PerformCallback(2);
}" />
                </dx:ASPxButton>
            </td>
            </tr>
        </table>


      
                    <asp:Chart ID="Chart1" runat="server" Width="1000px" Height="500px">
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1">
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>

                </td>
            </tr>
           
        </table>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            DeleteMethod="Delete" InsertMethod="Insert" 
            OldValuesParameterFormatString="original_{0}" 
            SelectMethod="GetTop500mostFreqByTwoDates" 
            TypeName="BioWS.ArticlesPubmedDataSetTableAdapters.pmMeSHTableAdapter" 
            UpdateMethod="Update">
            <DeleteParameters>
                <asp:Parameter Name="Original_MeSH_ID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Heading" Type="String" />
            </InsertParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="ASPxDateEdit1" Name="StartDate" 
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="ASPxDateEdit2" Name="EndDate" 
                    PropertyName="Value" Type="String" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="Heading" Type="String" />
                <asp:Parameter Name="Original_MeSH_ID" Type="Int32" />
            </UpdateParameters>
        </asp:ObjectDataSource>



</dx:PanelContent>
</PanelCollection>
</dx:ASPxCallbackPanel>
    </div>
    </form>
</body>
</html>

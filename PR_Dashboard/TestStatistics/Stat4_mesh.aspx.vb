﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.Web.UI.DataVisualization.Charting.Utilities
Imports System.Drawing

Public Class Stat4_mesh
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            ASPxDateEdit1.Value = New Date(2010, 1, 1)
            ASPxDateEdit2.Value = Today

            ASPxDateEdit3.Value = New Date(2010, 1, 1)
            ASPxDateEdit4.Value = Today
        End If

    End Sub

    Private Sub ASPxCallbackPanel1_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel1.Callback
        If e.Parameter = "1" Then
            ObjectDataSource1.DataBind()
            ASPxCheckBoxList1.DataBind()
        Else
            Dim inpSeriesDict As New Dictionary(Of Integer, String)
            For Each seli As DevExpress.Web.ASPxEditors.ListEditItem In ASPxCheckBoxList1.SelectedItems
                inpSeriesDict.Add(seli.Value, seli.Text)
            Next
            CreateChart(inpSeriesDict)
        End If
    End Sub

    Private Sub CreateChart(ByVal inpSeriesDict As Dictionary(Of Integer, String))

        Dim meshidsStr As String
        Dim counter As Integer = 0
        Dim firstHandled As Boolean = False
        For Each kv In inpSeriesDict
            meshidsStr = meshidsStr & kv.Key

            If firstHandled = False Then
                CreateSerie(kv.Key, kv.Value, AxisType.Primary)
                Chart1.ChartAreas("ChartArea1").AxisY.Title = kv.Value & ", %"
                firstHandled = True
            Else
                CreateSerie(kv.Key, kv.Value, AxisType.Secondary)
                Chart1.ChartAreas("ChartArea1").AxisY2.Title = kv.Value & ", %"
            End If

            If counter < inpSeriesDict.Count - 1 Then
                meshidsStr = meshidsStr & ","
            End If
            counter = counter + 1
        Next


        '! set up chart
        Dim cha As ChartArea = Chart1.ChartAreas("ChartArea1")
        cha.AxisY.MajorGrid.LineColor = Color.LightGray
        cha.AxisY.LineColor = Drawing.Color.LightGray

        cha.AxisY2.MajorGrid.LineColor = Color.LightGray
        cha.AxisY2.LineColor = Drawing.Color.LightGray
        cha.AxisY2.MajorGrid.LineDashStyle = ChartDashStyle.DashDot


        cha.AxisX.MajorGrid.LineColor = Color.LightGray
        cha.AxisX.LineColor = Drawing.Color.LightGray

        cha.AxisX.IntervalType = DateTimeIntervalType.Weeks
        cha.AxisX.Interval = 2
        cha.AxisX.LabelStyle.Format = "MMM yy"



        cha.AxisX.IsMarginVisible = False




 
        Dim l As New Legend() With {.Docking = Docking.Bottom, .Alignment = Drawing.StringAlignment.Far, .IsDockedInsideChartArea = False}
        Me.Chart1.Legends.Add(l)


        '! request data
        Dim query As String = String.Format("EXEC [stat_GetMeshsFrequency] '{0}'  ,'{1}'  ,'{2}'", meshidsStr, ASPxDateEdit3.Date.ToString("yyyy-MM-dd"), ASPxDateEdit4.Date.ToString("yyyy-MM-dd"))
        Dim dt1 As DataTable = BLL.SQLHelper.GetDT_AllStrings(query)

        fillDataPoints(inpSeriesDict, dt1)


        SetGroupping()

    End Sub
    Private Sub CreateSerie(ByVal meshid As Integer, ByVal meshname As String, ByVal axity As AxisType)
        Dim serie1 As New Series
        serie1.YAxisType = axity
        serie1.Name = meshid
        serie1.LegendText = meshname
        serie1.BorderWidth = 2
        serie1.ChartType = SeriesChartType.Spline
        serie1.ChartArea = "ChartArea1"
        Chart1.Series.Add(serie1)
    End Sub
    Private Sub fillDataPoints(ByVal inpSeriesDict As Dictionary(Of Integer, String), ByVal dt1 As DataTable)
        For Each meshid In inpSeriesDict.Keys
            Dim MeshSerie As Series = Chart1.Series(meshid.ToString())
            Dim filterExpression = "MeshID='" & meshid.ToString() & "'"
            Dim MeshTable = dt1.Select(filterExpression)
            fillMeshSeries(MeshSerie, MeshTable)
        Next
    End Sub
    Private Sub fillMeshSeries(ByVal meshSerie As Series, ByVal meshTable As DataRow())
        For Each dRow In meshTable
            Dim xValue As Date = CDate(dRow(1))
            Dim yValue As Double = CDbl(dRow(2))
            Dim dp As New DataPoint
            dp.SetValueXY(xValue, yValue)
            meshSerie.Points.Add(dp)
        Next
    End Sub
    Private Sub SetGroupping()
        Dim it As IntervalType
        Dim int As Integer


        Select Case ddl_grint.SelectedValue
            Case "Day"
                it = IntervalType.Days
                int = 1
            Case "Week"
                it = IntervalType.Weeks
                int = 1
            Case "2 Weeks"
                it = IntervalType.Weeks
                int = 2
            Case "Month"
                it = IntervalType.Months
                int = 1
        End Select
        Me.Chart1.ChartAreas("ChartArea1").AxisX.IntervalType = it
        Me.Chart1.ChartAreas("ChartArea1").AxisX.LabelStyle.IntervalType = it
        For Each Ser In Me.Chart1.Series
            Me.Chart1.DataManipulator.Group("SUM, X:LAST", int, it, Ser)
        Next
        
    End Sub
End Class
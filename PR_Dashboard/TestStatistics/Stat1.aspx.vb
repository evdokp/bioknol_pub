﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.Web.UI.DataVisualization.Charting.Utilities
Imports System.Drawing

Public Class Stat1
    Inherits Page

    Private Shared Function GetDt2() As DataTable
        Const query As String = "SELECT distinct [Person_ID],[ViewDate]  FROM [dbo].[ViewsLog]  where Person_ID>0   order by Person_ID"
        Dim dt1 As DataTable = BLL.SQLHelper.GetDT_AllStrings(query)

        Dim dt2 As New DataTable()
        Dim colPersonID As New DataColumn() With {.DataType = System.Type.[GetType]("System.Int32"), .ColumnName = "PersonID"}
        Dim colViewDate As New DataColumn() With {.DataType = System.Type.[GetType]("System.DateTime"), .ColumnName = "ViewDate"}
        dt2.Columns.Add(colPersonID)
        dt2.Columns.Add(colViewDate)

        For Each dRow In dt1.Rows
            Dim r As DataRow = dt2.NewRow()
            r(0) = dRow(0)
            r(1) = CType(dRow(1), Date)
            dt2.Rows.Add(r)
        Next
        Return dt2
    End Function
    Private Sub SetChart()
        Dim cha As ChartArea = Chart1.ChartAreas("ChartArea1")
        cha.AxisX.MajorGrid.Enabled = False

        cha.AxisY.Title = "Пользователи"
        cha.AxisY.TitleAlignment = Drawing.StringAlignment.Center
        cha.AxisY.MajorTickMark.Interval = 10
        cha.AxisY.MajorTickMark.Enabled = True
        cha.AxisY.IntervalType = DateTimeIntervalType.Number
        cha.AxisY.Interval = 10
        cha.AxisY.IsMarginVisible = False
        cha.AxisY.MajorGrid.Enabled = False

        cha.AxisX.IntervalType = DateTimeIntervalType.Months
        cha.AxisX.Interval = 3



        cha.AxisX.LabelStyle.Format = "MMM yy"
        cha.AxisX.Title = "Активные дни"




    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dt2 As DataTable = GetDt2()
        Dim minDate As Date = dt2.Compute("min(ViewDate)", "")
        Dim maxDate As Date = dt2.Compute("max(ViewDate)", "")


        Dim personsList = (From r In dt2.AsEnumerable() Select r(0)).Distinct().ToList()


        SetChart()

        ' define chart
        Dim Series1 As Series = Chart1.Series("Series1")
        Series1.ChartType = SeriesChartType.Point
        Series1.XValueType = ChartValueType.Date

        Dim curDate As Date = minDate
        While curDate < maxDate
            For Each personStr In personsList
                Dim fe As String = String.Format("ViewDate='{0}' AND PersonID={1}", curDate.ToShortDateString(), personStr)
                Dim dt3 = dt2.Select(fe)
                If dt3.Count > 0 Then
                    Dim yVal As Double = personsList.IndexOf(personStr) + 1
                    Dim dp As New DataPoint
                    'dp.MarkerStyle = MarkerStyle.Circle
                    'dp.MarkerSize = 5
                    'dp.MarkerColor = Drawing.Color.LightYellow
                    dp.SetValueXY(curDate, yVal)
                    Series1.Points.Add(dp)
                End If
            Next
            curDate = curDate.AddDays(1)
        End While



    End Sub

End Class
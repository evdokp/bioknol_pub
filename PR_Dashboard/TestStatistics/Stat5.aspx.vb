﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.Web.UI.DataVisualization.Charting.Utilities
Imports System.Drawing
Imports System.Threading

Public Class Stat5
    Inherits Page

    Private Shared Function GetDt2() As DataTable
        Const query As String = " select t.Person_ID, vHour, -1.0 / log(vCount*1.0/vTotalCount*1.0) as vHourShare from  " & _
" (SELECT Person_ID,   " & _
" datepart(hour,[ViewsLog].TimeStamp) as vHour,   " & _
" count(ID) as vCount    " & _
" FROM [ViewsLog]    " & _
" where Person_ID > 0   " & _
" group by Person_ID,datepart(hour,[ViewsLog].TimeStamp)   " & _
" ) as t inner join (  " & _
" SELECT Person_ID, count(ID) as vTotalCount    " & _
" FROM [ViewsLog]    " & _
" where Person_ID > 0   " & _
" group by Person_ID  " & _
" ) as t2 on t.Person_ID = t2.Person_ID  " & _
" inner join  " & _
" (  SELECT Person_ID, count(distinct ViewDate) as vDaysCount    " & _
" FROM [ViewsLog]    " & _
" where Person_ID > 0   " & _
" group by Person_ID  " & _
" ) as t3 on t.Person_ID = t3.Person_ID  " & _
" where t3.vDaysCount >6 and t2.vTotalCount > 40 " & _
" order by vHourShare "

        Dim dt1 As DataTable = BLL.SQLHelper.GetDT_AllStrings(query)

        Dim dt2 As New DataTable()
        Dim colPersonID As New DataColumn() With {.DataType = System.Type.[GetType]("System.Int32"), .ColumnName = "PersonID"}
        Dim colHour As New DataColumn() With {.DataType = System.Type.[GetType]("System.Int32"), .ColumnName = "vHour"}
        Dim colCount As New DataColumn() With {.DataType = System.Type.[GetType]("System.Double"), .ColumnName = "vCount"}
        dt2.Columns.Add(colPersonID)
        dt2.Columns.Add(colHour)
        dt2.Columns.Add(colCount)

        For Each dRow In dt1.Rows
            Dim r As DataRow = dt2.NewRow()
            r(0) = CInt(dRow(0))
            r(1) = CInt(dRow(1))
            r(2) = CDbl(dRow(2))
            dt2.Rows.Add(r)
        Next
        Return dt2
    End Function
    Private Sub SetChart()
        Dim cha1 As ChartArea = Chart1.ChartAreas("ChartArea1")
        Dim cha2 As ChartArea = Chart1.ChartAreas("ChartArea2")
        Dim chaList As New List(Of ChartArea)
        chaList.Add(cha1)
        chaList.Add(cha2)

        For Each cha In chaList
            cha.AxisX.MajorGrid.Enabled = True
            cha.AxisX.MajorGrid.LineColor = Color.LightGray

            cha.AxisY.TitleAlignment = Drawing.StringAlignment.Center
            cha.AxisY.MajorTickMark.Interval = 10
            cha.AxisY.MajorTickMark.Enabled = True
            cha.AxisY.IntervalType = DateTimeIntervalType.Number
            cha.AxisY.Interval = 10
            cha.AxisY.IsMarginVisible = False
            cha.AxisY.MajorGrid.Enabled = False
            cha.AxisX.Crossing = 0
            cha.AxisX.IsMarginVisible = False
            cha.AxisX.Interval = 1
        Next
        
        cha2.AxisX.Title = "Время суток"
        cha1.AxisY.Title = "Пользователи"
        cha2.AxisY.LabelStyle.Enabled = False
        cha2.AxisY.MajorTickMark.Enabled = False

        ' Set the chart area alignmnet. This will cause the axes to align vertically.
        cha2.AlignmentOrientation = AreaAlignmentOrientations.Vertical
        cha2.AlignWithChartArea = "ChartArea1"



        ' Disable X Axis Labels for the first chart area.
        cha1.AxisX.LabelStyle.Enabled = False

        ' Disable the tick marks of the X axis.
        cha1.AxisX.MajorTickMark.Enabled = False
        cha1.AxisX.MinorTickMark.Enabled = False

        ' Disable End labels for Y axes.
        cha1.AxisY.LabelStyle.IsEndLabelVisible = False
        cha2.AxisY.LabelStyle.IsEndLabelVisible = False


    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dt2 As DataTable = GetDt2()



        Dim personsList = (From r In dt2.AsEnumerable() Select r(0)).Distinct().ToList()
        Dim personsOrderedList = (From pp In personsList Order By pp Select pp).ToList()





        SetChart()

        ' define chart
        Dim Series1 As Series = Chart1.Series("Series1")
        Dim Series2 As Series = Chart1.Series("Series2")

        Series2.ChartType = SeriesChartType.Column


        Series1.ChartType = SeriesChartType.Point
        Series1.XValueType = ChartValueType.Int32

        For i = 0 To 23
            Dim filterCondition As String = String.Format("vHour={0}", i)
            Dim dt3 = dt2.Select(filterCondition)
            Dim vTotal As Integer = 0
            For Each row In dt3
                Dim dp As New DataPoint
                Dim lRow As Integer = CInt(row(2) * 10 + 1)
                vTotal = vTotal + lRow
                dp.MarkerSize = lRow
                dp.SetValueXY(i, personsOrderedList.IndexOf(row(0)) + 1)
                Series1.Points.Add(dp)
            Next
            Dim dp2 As New DataPoint
            dp2.SetValueXY(i, vTotal)
            Series2.Points.Add(dp2)

        Next




        'Dim curDate As Date = minDate
        'While curDate < maxDate
        '    For Each personStr In personsList
        '        Dim fe As String = String.Format("ViewDate='{0}' AND PersonID={1}", curDate.ToShortDateString(), personStr)
        '        Dim dt3 = dt2.Select(fe)
        '        If dt3.Count > 0 Then
        '            Dim yVal As Double = personsList.IndexOf(personStr) + 1
        '            Dim dp As New DataPoint
        '            'dp.MarkerStyle = MarkerStyle.Circle
        '            'dp.MarkerSize = 5
        '            'dp.MarkerColor = Drawing.Color.LightYellow
        '            dp.SetValueXY(curDate, yVal)
        '            Series1.Points.Add(dp)
        '        End If
        '    Next
        '    curDate = curDate.AddDays(1)
        'End While



    End Sub

End Class

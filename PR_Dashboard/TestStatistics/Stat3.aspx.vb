﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.Web.UI.DataVisualization.Charting.Utilities
Imports System.Drawing

Public Class Stat3
    Inherits Page

    Private Shared Function GetDt2() As DataTable
        Const query As String = "select Person_ID, Count(ViewDate) as vCount from (SELECT distinct [Person_ID],[ViewDate]  FROM [dbo].[ViewsLog]  where Person_ID>0  ) as t group by Person_ID order by vCount desc"
        Dim dt1 As DataTable = BLL.SQLHelper.GetDT_AllStrings(query)


        Return dt1
    End Function
    Private Sub SetChart()
        Dim cha As ChartArea = Chart1.ChartAreas("ChartArea1")
        cha.AxisX.MajorGrid.Enabled = False

        cha.AxisY.Title = "Пользователи"
        cha.AxisY.TitleAlignment = Drawing.StringAlignment.Center
        cha.AxisY.MajorTickMark.Interval = 10
        cha.AxisY.MajorTickMark.Enabled = True
        cha.AxisY.IntervalType = DateTimeIntervalType.Number
        cha.AxisY.Interval = 10
        cha.AxisY.IsMarginVisible = False
        cha.AxisY.MajorGrid.Enabled = False



        cha.AxisX.Title = "Активные дни"




    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dt2 As DataTable = GetDt2()


        SetChart()

        ' define chart
        Dim Series1 As Series = Chart1.Series("Series1")
        Series1.ChartType = SeriesChartType.Point
        Series1.XValueType = ChartValueType.Int32


        Dim k As Integer
        k = 1
        For Each dRow In dt2.Rows
            Dim activedaysCount As Integer = CType(dRow(1), Integer)
            Dim personID As Integer = dRow(0)

            For i = 1 To activedaysCount
                Dim dp As New DataPoint
                dp.SetValueXY(i, k)
                Series1.Points.Add(dp)
            Next
            k = k + 1

        Next

    End Sub

End Class
﻿Public Class MyPageRedirect
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim vPerson As Integer

        If User.Identity.IsAuthenticated = True Then
            Dim _profile As New ProfileCommon
            _profile = _profile.GetProfile(User.Identity.Name)
            vPerson = _profile.Person_ID
            SetSessionDefaults()
        Else
            vPerson = Request.QueryString("UserID")
        End If

        If vPerson = 0 Then
            Me.Response.Redirect("~\Account\Register.aspx", False)
        Else
            Dim ta As New PersonsDataSetTableAdapters.PersonsTableAdapter
            Dim t = ta.GetDataByID(vPerson)
            Dim r = t.Item(0)
            If User.Identity.IsAuthenticated = False Then
                FormsAuthentication.SetAuthCookie(r.UserName, False)
                SetSessionDefaults()
            End If
            Dim posts_ta As New DataSetMessagesTableAdapters.MessagesTableAdapter
            If posts_ta.GetCountNewMessagesByPersonToID(vPerson) > 0 Then
                Me.Response.Redirect("~\Account\Messages\MyMessages.aspx", False)
            Else
                Me.Response.Redirect("~\AccountNew\MyActivity.aspx?followed=0&objtype=p&objid=" & vPerson, False)
            End If
        End If
    End Sub


    Protected Sub SetSessionDefaults()
        If Me.Session("StartDate") Is Nothing Then
            Dim _profile As New ProfileCommon
            _profile = _profile.GetProfile(HttpContext.Current.User.Identity.Name)
            Me.Session.Add("StartDate", Today.AddDays(-1 * _profile.DefaultPeriodDays))
        End If
        If Me.Session("EndDate") Is Nothing Then
            Me.Session.Add("EndDate", Today)
        End If
    End Sub

End Class
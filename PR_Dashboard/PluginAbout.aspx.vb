﻿Public Class PluginAbout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "About PRD browser plug-in"
        CType(Master.Master.FindControl("page_header"), HtmlGenericControl).Visible = False


        Dim version As String = Request.QueryString("Version")
        Select Case Request.QueryString("Browser")
            Case "IE"
                litTitle.Text = "Internet explorer PRD plug-in"
                imgBrowser.ImageUrl = "~/Images/IE_icon.png"
                checkVersion("2.4.3.0U", version)
            Case "FF"
                litTitle.Text = "Firefox PRD plug-in"
                imgBrowser.ImageUrl = "~/Images/firefox_icon.png"
                checkVersion("1.3.7", version)
            Case "Ch"
                litTitle.Text = "Google Chrome PRD plug-in"
                imgBrowser.ImageUrl = "~/Images/chrome_icon.png"
                checkVersion("1.2.6", version)
            Case "Sa"
                litTitle.Text = "Apply Safari PRD plug-in"
                imgBrowser.ImageUrl = "~/Images/safari_icon.png"
                checkVersion("2.1", version)
            Case "Op"
                litTitle.Text = "Opera PRD plug-in"
                imgBrowser.ImageUrl = "~/Images/opera_icon.jpg"
                checkVersion("1.9", version)
            Case Else

        End Select

        Dim ciBodyTA As New DataSetContentItemsTableAdapters.v_ContentItemsTableAdapter
        Dim ciR = ciBodyTA.GetDataByID(12)(0)

        litOverview.Text = ciR.Body

    End Sub

    Protected Sub checkVersion(ByVal lastVers As String, ByVal curVers As String)
        If lastVers = curVers Then
            litVersion.Text = "Your plug-in is up to date"
            imgVersion.ImageUrl = "~/Images/check24.png"
        Else
            litVersion.Text = "Your plug-in is out of date"
            imgVersion.ImageUrl = "~/Images/warning24.png"
        End If
    End Sub

End Class
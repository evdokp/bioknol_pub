/// <reference path="../TSDef/jquery.d.ts" />
/// <reference path="../TSDef/jsrender_fix.d.ts" />
/// <reference path="../jsrender.js" />



class ViewArticleManager { 
    commentsandrecsLoadUrl = "../../Handler/reccomGetInitialDataForArticle.ashx";
    constructor (public articleid: string) { }

    Init() { 
        this.RequestCommentsAndRecs();
    }

    LoadTemplate(name) {
        $.get("../../Templates/JSR/" + name + ".html"), (template) =>
			$.templates(name, template);
        );
    }
    private RequestCommentsAndRecs() { 
        $.get(this.commentsandrecsLoadUrl, {articleid : this.articleid}, function (response) {
            alert(response);
        });
    }
}
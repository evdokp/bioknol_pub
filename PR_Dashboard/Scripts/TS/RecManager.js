var RecManager = (function () {
    function RecManager(options) {
        this.options = options;
        this.articleId = options.articleid;
        this.articleTitle = options.articletitle;
        if(options.newRecommendationCallback != undefined) {
            this.newRecommendationCallback = options.newRecommendationCallback;
        } else {
            this.newRecommendationCallback = function () {
            };
        }
        if(options.commentIncreaseCallback != undefined) {
            this.commentIncreaseCallback = options.commentIncreaseCallback;
        } else {
            this.commentIncreaseCallback = function () {
            };
        }
        if(options.beforeShowCallback != undefined) {
            this.beforeShowCallback = options.beforeShowCallback;
        } else {
            this.beforeShowCallback = function () {
            };
        }
        this.personSelect2 = $('#recSelector');
        this.Recommend();
    }
    RecManager.prototype.SetData = function (data) {
        this.personSelect2.select2("data", data);
    };
    RecManager.prototype.GetData = function () {
        return this.personSelect2.select2("data");
    };
    RecManager.prototype.IncludeIntoData = function (id, text) {
        var ids = this.personSelect2.select2("val");
        if(ids.indexOf(id) == -1) {
            var data = this.GetData();
            var newitem = {
                id: id.toString(),
                text: text
            };
            data.push(newitem);
            this.personSelect2.select2("val", "");
            this.personSelect2.select2("data", data);
        }
    };
    RecManager.prototype.Recommend = function () {
        var _this = this;
        $.get('../Handler/recRecommendArticle.ashx', {
            articleid: this.articleId
        }, function (response) {
            var res = $.parseJSON(response);
            var isNew = res["IsNew"];
            _this.recommendationId = res["RecID"];
            _this.Show();
            if(isNew === 1) {
                _this.newRecommendationCallback(_this.articleId);
            }
        });
    };
    RecManager.prototype.Show = function () {
        var _this = this;
        $('#recManager .modal-body').children().hide();
        $('#dRecSelect').show();
        this.beforeShowCallback(this.articleId);
        $('#textComment').val('');
        $('#recManager').modal();
        $('#recManager .modal-header strong').text(this.articleTitle);
        this.ShowAlreadyRecommended();
        this.ShowSelect2();
        $('#btnRecClose').click(function () {
            $('#recManager').modal('hide');
        });
        $('#btnRecommend').click(function () {
            var ids = _this.personSelect2.select2("val").join(',');
            $('#recManager .modal-body').children().hide();
            $('#dRecLoading').show();
            $.get('../Handler/Recommendations/RecommendTo.ashx', {
                recid: _this.recommendationId,
                ids: ids,
                comment: $('#textComment').val()
            }, function (response) {
                if($('#textComment').val().length > 0) {
                    _this.commentIncreaseCallback(_this.articleId);
                }
                $('#recManager .modal-body').children().hide();
                $('#dRecDone').show();
                setTimeout(function () {
                    $('#recManager').modal('hide');
                }, 700);
            });
        });
    };
    RecManager.prototype.ShowSelect2 = function () {
        var options = [];
        options["minimumInputLength"] = 1;
        options["multiple"] = true , options["placeholder"] = "Type BioKnol user name or e-mail";
        options["formatResult"] = this.FormatResult;
        options["tags"] = function (query) {
            $.getJSON('../Handler/Recommendations/GetPersonsByQuery.ashx', {
                query: query.term
            }, function (response) {
                var data = {
                    results: []
                };
                data.results = response;
                query.callback(data);
            });
        };
        this.personSelect2.select2("destroy");
        this.personSelect2.select2(options);
    };
    RecManager.prototype.FormatResult = function (item) {
        if(isNaN(item.id) === true || item.id === 0) {
            return "<i>Select BioKnol user or add a valid email:</i> <b>" + item.text + "</b>";
        } else {
            return '<img src="' + item.img + '" alt="" class="rnd4 fleft"/><div class="fleft" style="margin-left:5px; margin-top:4px;">' + item.text + '</div><div class="clearfix"></div>';
        }
    };
    RecManager.prototype.ShowAlreadyRecommended = function () {
        var _this = this;
        $.getJSON('../Handler/Recommendations/GetPersonsByRecID.ashx', {
            recid: this.recommendationId
        }, function (response) {
            _this.ShowPersonsBlock('#dAlreadyRec', '#spAlreadyRec', response.Already);
            _this.ShowPersonsBlock('#dRecentPersons', '#spRecentPersons', response.Recent);
            _this.ShowPersonsBlock('#dFollowers', '#spFollowers', response.Followers);
            _this.ShowPersonsBlock('#dFollowed', '#spFollowed', response.Followed);
            var mng = _this;
            $('#recPreselectedPers a').click(function () {
                mng.IncludeIntoData($(this).attr('personid'), $(this).text());
            });
        });
    };
    RecManager.prototype.ShowPersonsBlock = function (containerSel, datacontSel, data) {
        var arrLength = data.length;
        if(arrLength === 0) {
            $(containerSel).hide();
        } else {
            $(containerSel).show();
            var str = '';
            $.each(data, function (index, value) {
                str = str.concat('<a personid="' + value.PersonID + '" style="cursor:pointer">' + value.DisplayName + '</a>');
                if(index < arrLength - 1) {
                    str = str.concat(', ');
                }
            });
            $(datacontSel).html(str);
        }
    };
    return RecManager;
})();

var ViewArticleManager = (function () {
    function ViewArticleManager(articleid) {
        this.articleid = articleid;
        this.commentsandrecsLoadUrl = "../../Handler/reccomGetInitialDataForArticle.ashx";
    }
    ViewArticleManager.prototype.Init = function () {
        this.RequestCommentsAndRecs();
    };
    ViewArticleManager.prototype.RequestCommentsAndRecs = function () {
        $.get(this.commentsandrecsLoadUrl, {
            articleid: this.articleid
        }, function (response) {
            alert(response);
        });
    };
    return ViewArticleManager;
})();

/// <reference path="../TSDef/jquery.d.ts" />
/// <reference path="../TSDef/bootstrap.d.ts" />
/// <reference path="../jsrender.js" />

class RecManager {
    recommendationId: number;
    articleId: string;
    articleTitle: string;
    personSelect2: any;
    newRecommendationCallback: any;
    commentIncreaseCallback: any;
    beforeShowCallback: any;

    constructor (public options: any) {
        this.articleId = options.articleid;
        this.articleTitle = options.articletitle
        if (options.newRecommendationCallback != undefined) {
            this.newRecommendationCallback = options.newRecommendationCallback;
        } else {
            this.newRecommendationCallback = function () { };
        }
        if (options.commentIncreaseCallback != undefined) {
            this.commentIncreaseCallback = options.commentIncreaseCallback;
        } else {
            this.commentIncreaseCallback = function () { };
        }
        if (options.beforeShowCallback != undefined) {
            this.beforeShowCallback = options.beforeShowCallback;
        } else {
            this.beforeShowCallback = function () { };
        }
        this.personSelect2 = $('#recSelector');
        this.Recommend();
    }

    SetData(data) {
        this.personSelect2.select2("data", data);
    }
    GetData() {
        return this.personSelect2.select2("data");
    }
    IncludeIntoData(id: any, text: string) {
        var ids = this.personSelect2.select2("val");
        if (ids.indexOf(id) == -1) {
            var data = this.GetData();
            var newitem = { id: id.toString(), text: text };
            data.push(newitem);
            this.personSelect2.select2("val", "");
            this.personSelect2.select2("data", data);
        }
    }

    Recommend() {
        $.get('../Handler/recRecommendArticle.ashx', { articleid: this.articleId }, (response) => {
            var res = $.parseJSON(response);
            var isNew = res["IsNew"];
            this.recommendationId = res["RecID"];
            this.Show();
            if (isNew === 1) {
                this.newRecommendationCallback(this.articleId)
            }
        });
    }

    Show() {
        // step 1
        $('#recManager .modal-body').children().hide();
        $('#dRecSelect').show();
        //----------
        this.beforeShowCallback(this.articleId);
        $('#textComment').val('');
        $('#recManager').modal();
        $('#recManager .modal-header strong').text(this.articleTitle);
        this.ShowAlreadyRecommended();
        this.ShowSelect2();
        $('#btnRecClose').click(() =>{ 
            $('#recManager').modal('hide');
        });
        $('#btnRecommend').click(() =>{
            var ids = this.personSelect2.select2("val").join(',');
            // step 2
            $('#recManager .modal-body').children().hide();
            $('#dRecLoading').show();
            //----------
            $.get('../Handler/Recommendations/RecommendTo.ashx',
                    { recid: this.recommendationId, ids: ids, comment: $('#textComment').val() },
                    (response) =>{
                        if ($('#textComment').val().length > 0) {
                            this.commentIncreaseCallback(this.articleId)
                        }
                        // step 3
                        $('#recManager .modal-body').children().hide();
                        $('#dRecDone').show();
                        setTimeout(function () { $('#recManager').modal('hide'); }, 700);
                        //----------
                    });
        });
    }

    ShowSelect2() {
        var options = [];
        options["minimumInputLength"] = 1;
        options["multiple"] = true,
        options["placeholder"] = "Type BioKnol user name or e-mail";
        options["formatResult"] = this.FormatResult;
        options["tags"] = (query) =>
        {
            $.getJSON('../Handler/Recommendations/GetPersonsByQuery.ashx', { query: query.term }, function (response) {
                var data = { results: [] };
                data.results = response;
                query.callback(data);
            });
        }
        this.personSelect2.select2("destroy");
        this.personSelect2.select2(options);
    }

    FormatResult(item: any) {
        if (isNaN(item.id) === true || item.id === 0) {
            return "<i>Select BioKnol user or add a valid email:</i> <b>" + item.text + "</b>";
        }
        else {
            return '<img src="' + item.img + '" alt="" class="rnd4 fleft"/><div class="fleft" style="margin-left:5px; margin-top:4px;">' + item.text + '</div><div class="clearfix"></div>'
        }
    }

    ShowAlreadyRecommended() {
        $.getJSON('../Handler/Recommendations/GetPersonsByRecID.ashx', { recid: this.recommendationId }, (response) =>
        {
            this.ShowPersonsBlock('#dAlreadyRec', '#spAlreadyRec', response.Already);
            this.ShowPersonsBlock('#dRecentPersons', '#spRecentPersons', response.Recent);
            this.ShowPersonsBlock('#dFollowers', '#spFollowers', response.Followers);
            this.ShowPersonsBlock('#dFollowed', '#spFollowed', response.Followed);
            var mng = this;
            $('#recPreselectedPers a').click(function () {
                mng.IncludeIntoData($(this).attr('personid'), $(this).text());
            });
        });
    }

    ShowPersonsBlock(containerSel: string, datacontSel: string, data: any) {
        var arrLength = data.length;
        if (arrLength === 0) {
            $(containerSel).hide();
        } else {
            $(containerSel).show();
            var str = '';
            $.each(data, (index, value) =>{
                str = str.concat('<a personid="' + value.PersonID + '" style="cursor:pointer">' + value.DisplayName + '</a>');
                if (index < arrLength - 1) {
                    str = str.concat(', ');
                }
            });
            $(datacontSel).html(str);
        }
    }



}
class SavedParserManager
	constructor:(@initialStatus)->
		this.ParserText = $('#hfCurParser').val();
		this.StatusID = @initialStatus
		this.FinalUpdate = false;
		switch @initialStatus
			when '0' 
				this.SetUndefinedParser()
			when '1' 
				this.SetDefinedParser()
			when '2'
				this.SetRunningParser()
			when '3'
				this.SetRunningParser()
				$('#imgParsing').attr('src','../../Images/check.png')
				btnFinish.SetEnabled(true);
		
		

	Check: () ->
		this.UpdateStatusID()
		$('#info').html(this.StatusID + ' ' + this.FinalUpdate)
	
	SetUndefinedParser:() ->
		$('#pnlDefineParser').show()
		$('#dCurrentParser').hide()
		$('#dDomains').hide()
		$('#dParsing').hide()
		btnReset.SetEnabled(true);
		btnRun.SetEnabled(true);

	SetDefinedParser: () ->
		$('#pnlDefineParser').hide()
		$('#dCurrentParser').show()
		$('#divCurParser').html(this.ParserText)
		$('#imgParsing').attr('src','../../Images/parserLoader.gif')
		cpCurrentParser.PerformCallback()
	
	SetRunningParser: () ->
		this.SetDefinedParser()
		$('#dParsing').show()
		$('#imgParsing').attr('src','../../Images/parserLoader.gif')
		cpParsing.PerformCallback()
		btnReset.SetEnabled(false);
		btnRun.SetEnabled(false);
		btnFinish.SetEnabled(false);

	UpdateStatusID: () ->
		PageMethods.GetStatusID(
			(response) =>
				this.StatusID = parseInt(response)
				this.UpdateParsingResults();
		);

	UpdateParsingResults: () ->
		if this.StatusID is 2
			cpParsing.PerformCallback()
		if this.StatusID is 3 
			if this.FinalUpdate == false
				cpCurrentParser.PerformCallback()
				cpParsing.PerformCallback()
				$('#imgParsing').attr('src','../../Images/check.png')
				btnFinish.SetEnabled(true);
				this.FinalUpdate = true




	ResetParser: () ->
		PageMethods.ResetParser(
			(response) =>
				this.StatusID = 0
				this.FinalUpdate = false
				this.SetUndefinedParser()
		);

	Run: () ->
		PageMethods.Run(
			(response) =>
				this.SetRunningParser()
		);		

	DefineParser: (@dt1,@dt2,@offset) ->
		PageMethods.DefineParser(
			@dt1
			@dt2
			@offset
			(response) =>
				this.ParserText = response
				this.SetDefinedParser()
		);

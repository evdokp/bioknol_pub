var SavedParserManager;

SavedParserManager = (function() {

  function SavedParserManager(initialStatus) {
    this.initialStatus = initialStatus;
    this.ParserText = $('#hfCurParser').val();
    this.StatusID = this.initialStatus;
    this.FinalUpdate = false;
    switch (this.initialStatus) {
      case '0':
        this.SetUndefinedParser();
        break;
      case '1':
        this.SetDefinedParser();
        break;
      case '2':
        this.SetRunningParser();
        break;
      case '3':
        this.SetRunningParser();
        $('#imgParsing').attr('src', '../../Images/check.png');
        btnFinish.SetEnabled(true);
    }
  }

  SavedParserManager.prototype.Check = function() {
    this.UpdateStatusID();
    return $('#info').html(this.StatusID + ' ' + this.FinalUpdate);
  };

  SavedParserManager.prototype.SetUndefinedParser = function() {
    $('#pnlDefineParser').show();
    $('#dCurrentParser').hide();
    $('#dDomains').hide();
    $('#dParsing').hide();
    btnReset.SetEnabled(true);
    return btnRun.SetEnabled(true);
  };

  SavedParserManager.prototype.SetDefinedParser = function() {
    $('#pnlDefineParser').hide();
    $('#dCurrentParser').show();
    $('#divCurParser').html(this.ParserText);
    $('#imgParsing').attr('src', '../../Images/parserLoader.gif');
    return cpCurrentParser.PerformCallback();
  };

  SavedParserManager.prototype.SetRunningParser = function() {
    this.SetDefinedParser();
    $('#dParsing').show();
    $('#imgParsing').attr('src', '../../Images/parserLoader.gif');
    cpParsing.PerformCallback();
    btnReset.SetEnabled(false);
    btnRun.SetEnabled(false);
    return btnFinish.SetEnabled(false);
  };

  SavedParserManager.prototype.UpdateStatusID = function() {
    var _this = this;
    return PageMethods.GetStatusID(function(response) {
      _this.StatusID = parseInt(response);
      return _this.UpdateParsingResults();
    });
  };

  SavedParserManager.prototype.UpdateParsingResults = function() {
    if (this.StatusID === 2) cpParsing.PerformCallback();
    if (this.StatusID === 3) {
      if (this.FinalUpdate === false) {
        cpCurrentParser.PerformCallback();
        cpParsing.PerformCallback();
        $('#imgParsing').attr('src', '../../Images/check.png');
        btnFinish.SetEnabled(true);
        return this.FinalUpdate = true;
      }
    }
  };

  SavedParserManager.prototype.ResetParser = function() {
    var _this = this;
    return PageMethods.ResetParser(function(response) {
      _this.StatusID = 0;
      _this.FinalUpdate = false;
      return _this.SetUndefinedParser();
    });
  };

  SavedParserManager.prototype.Run = function() {
    var _this = this;
    return PageMethods.Run(function(response) {
      return _this.SetRunningParser();
    });
  };

  SavedParserManager.prototype.DefineParser = function(dt1, dt2, offset) {
    var _this = this;
    this.dt1 = dt1;
    this.dt2 = dt2;
    this.offset = offset;
    return PageMethods.DefineParser(this.dt1, this.dt2, this.offset, function(response) {
      _this.ParserText = response;
      return _this.SetDefinedParser();
    });
  };

  return SavedParserManager;

})();

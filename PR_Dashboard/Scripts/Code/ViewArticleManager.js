var ViewArticleManager,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

ViewArticleManager = (function() {

  function ViewArticleManager(articleid) {
    this.LoadRecsAndComments = __bind(this.LoadRecsAndComments, this);

    var mng,
      _this = this;
    this.commentsandrecsurl = $Url.resolve("~/Handler/reccomGetInitialDataForArticle.ashx");
    this.subRecsURL = $Url.resolve("~/Handler/reccomGetRecRecipients.ashx");
    this.addcommentURL = $Url.resolve("~/Handler/reccomAddComment.ashx");
    this.requestURL = $Url.resolve("~/Handler/RequestPubmedArticle.ashx");
    this.LoadTemplate('comment');
    this.LoadTemplate('recommendation');
    this.articleid = articleid;
    this.LoadRecsAndComments();
    $('.feed_comment textarea').watermark('add comment...');
    $('.feed_comment textarea').keyup(function() {
      return $(this).parent().removeClass('error');
    });
    mng = this;
    $('.feed_comment #btnAddComment').click(function() {
      var comment, txtComment;
      txtComment = $(this).parent().find('textarea');
      comment = txtComment.val();
      if (comment.length > 0) {
        mng.AddComment(txtComment.val());
        return txtComment.val('');
      } else {
        return txtComment.parent().addClass('error');
      }
    });
    $('#hlRequest').click(function() {
      return _this.RequestPubmedArticle();
    });
  }

  ViewArticleManager.prototype.RequestPubmedArticle = function() {
    var _this = this;
    if (!$('#hlRequest').parent().hasClass('disabled')) {
      return $.get(this.requestURL, {
        PubmedID: this.articleid
      }, function(response) {
        $('#hlRequest').text('Requested');
        return $('#hlRequest').parent().addClass('disabled');
      });
    }
  };

  ViewArticleManager.prototype.LoadTemplate = function(name) {
    return $.get($Url.resolve("~/Templates/JSR/" + name + ".html"), function(template) {
      return $.templates(name, template);
    });
  };

  ViewArticleManager.prototype.LoadRecsAndComments = function() {
    var mng;
    mng = this;
    return $.get(this.commentsandrecsurl, {
      articleid: this.articleid
    }, function(response) {
      var obj;
      obj = jQuery.parseJSON(response);
      $('#commentscontainer').html('');
      $('#commentscontainer').append($.render.comment(obj.Comments));
      $('#commentscontainer a.author').PersonHoverCards();
      $('#spanCommentsCount').text(obj.Comments.length);
      if (obj.Recommendations.length > 0) {
        $('#spanRecsCount').parent().show();
        $('#recscontainer').show();
        $('#spanRecsCount').text(obj.Recommendations.length);
        $('#recscontainer').html('');
        $('#recscontainer').append($.render.recommendation(obj.Recommendations));
        $('#recscontainer a.author').PersonHoverCards();
      } else {
        $('#spanRecsCount').parent().hide();
        $('#recscontainer').hide();
      }
      $('.feed_comment .recipients').click(function() {
        var _this = this;
        if ($(this).attr('isopen') === '0') {
          $(this).attr('isopen', '1');
          return $.get(mng.subRecsURL, {
            articleid: mng.articleid,
            authorid: $(this).attr('authorid')
          }, function(response) {
            var lastDiv;
            obj = jQuery.parseJSON(response);
            lastDiv = $(_this).closest('.feed_comment').find('.clearfix').next();
            lastDiv.append('<div class="header">recommends to:</div>');
            lastDiv.append($.render.recommendation(obj));
            lastDiv.find('a.author').PersonHoverCards();
            return lastDiv.css('border', 'solid 1px #A1ACC8');
          });
        }
      });
      return $('.tiptip').tipTip({
        defaultPosition: "top"
      });
    });
  };

  ViewArticleManager.prototype.AddComment = function(comment) {
    var _this = this;
    return $.get(this.addcommentURL, {
      articleid: this.articleid,
      comment: comment
    }, function(response) {
      var obj;
      obj = jQuery.parseJSON(response);
      $('#commentscontainer').append($.render.comment(obj));
      return $('#spanCommentsCount').text(parseInt($('#spanCommentsCount').text()) + 1);
    });
  };

  return ViewArticleManager;

})();

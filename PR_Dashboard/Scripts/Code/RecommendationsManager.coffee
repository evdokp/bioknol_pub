class RecommendationsManager
	constructor:()->
		this.articleid = ''
		this.recid = 0
		this.sendingCounter = 0
		this.Callback = undefined 

	Init: () ->
		mng = this
		$('#recModal #recAlreadyRec tr').live('click',->
			mng.RemoveRec($(this))
		)
		$('#recModal #recSearchResults tr').live('click',->
			mng.SelectPerson($(this))
		)
		$('#recModal #recPersonQuery').keyup(->
			mng.LoadByQuery($('#recModal #recPersonQuery').val())
		)
		$('#recModal #recSendEmails').click(->
			mng.SendEmails()
		)
		$('#recSaveUpdateClose').click(->
			mng.SaveUpdateClose()
		)
	SetCallback: (callback) =>
		this.Callback = callback

	SaveUpdateClose: () ->
		comment = $('#recAddRecommedComment').val()
		$.get(
			$Url.resolve("~/Handler/recSaveCommentUpdate.ashx")
			recid : this.recid
			comment : $('#recAddRecommedComment').val()
			(response) => 
				$('#recModal').modal('hide')
				if this.Callback isnt undefined
					this.Callback()
				if comment.length > 0
					if fm isnt undefined
						fm.GetCommentsManager().IncreaseCommentsCount(this.articleid)
						$('#recAddRecommedComment').val('')
		)


	SendEmails: () ->
		$('#recModal #recSendEmails').button('loading')
		emails = $('#recModal #txtRecEmails').val().split('\n')
		this.sendingCounter = emails.length
		$.each(emails,(index,value)=>
			comment = $('#recAddRecommedComment').val()
			this.SendEmail(value,comment)
		)
	SendEmail: (email,comment) ->
		$.get(
			$Url.resolve("~/Handler/recSendEmail.ashx")
			recid : this.recid
			email : email
			comment : comment
			(response) => 
				label = ''
				if response is 'success'
					label = '<span class="label label-success">Success</span>'
				else
					label = '<span class="label label-important">Failure</span>'
				$('#recModal #tblEmailResults').append('<tr><td>'+label+' '+email+ '</td></tr>')
				this.sendingCounter--
				if this.sendingCounter is 0
					$('#recModal #recSendEmails').button('reset')
					$('#recModal #txtRecEmails').val('')
		)
		
		

	RemoveRec: (tr) ->
		personid = tr.attr('personid')
		$.get(
			$Url.resolve("~/Handler/recRemoveRec.ashx")
			recid : this.recid
			persontoid : personid
			(response) => 
				tr.remove()
		)

	Show: (articleid, articletitle) ->
		this.articleid = articleid
		if fm isnt undefined
			fm.GetCommentsManager().CollapseAllDivsByArticleID(this.articleid)
		$('#recModal #recArticleTitle').html(articletitle)
		$('#recModal #tblEmailResults').html('')
		$('#recModal').modal()
		$.get(
			$Url.resolve("~/Handler/recRecommendArticle.ashx")
			articleid : articleid
			(response) => 
				result = jQuery.parseJSON(response)
				if parseInt(result.IsNew) is 1
					if fm isnt undefined
						fm.GetCommentsManager().IncreaseRecsCount(this.articleid)
				this.recid = parseInt(result.RecID)
				this.LoadAlready()
				this.LoadRecent()
		)

	LoadByQuery:(query)->
		if query.length > 1
			$('#recModal #recSearchResults').html('')
			$.get(
				$Url.resolve("~/Handler/recGetPersonsByLike.ashx")
				recid : this.recid
				tags : query
				(response) => 
					data = jQuery.parseJSON(response)
					this.RenderPersons('#recModal #recSearchResults',data)
			)
		if query.length is 0
			this.LoadRecent()

	LoadAlready:()->
		$('#recModal #recAlreadyRec').html('')
		$.get(
			$Url.resolve("~/Handler/recGetPersonsAlready.ashx")
			recid : this.recid
			(response) => 
				data = jQuery.parseJSON(response)
				this.RenderPersons('#recModal #recAlreadyRec',data)
		)

	LoadRecent: ()->
		$('#recModal #recSearchResults').html('')
		$.get(
			$Url.resolve("~/Handler/recGetRecentPersons.ashx")
			recid : this.recid
			(response) => 
				data = jQuery.parseJSON(response)
				this.RenderPersons('#recModal #recSearchResults',data)
		)

	RenderPersons: (target,data)->
		if data.length > 0 
			$(target).show()
			$("#tmplPerson").tmpl(data).appendTo(target)
			
	
	SelectPerson:(tr) ->
		personid = tr.attr('personid')
		$.get(
			$Url.resolve("~/Handler/recRecommendArticleToPerson.ashx")
			recid : this.recid
			persontoid : personid
			(response) => 
				tr.remove()
				this.LoadAlready()
		)
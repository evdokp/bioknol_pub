class ViewArticleManager
	constructor:(articleid)->
		this.commentsandrecsurl = $Url.resolve("~/Handler/reccomGetInitialDataForArticle.ashx")
		this.subRecsURL = $Url.resolve("~/Handler/reccomGetRecRecipients.ashx")
		this.addcommentURL = $Url.resolve("~/Handler/reccomAddComment.ashx")
		this.requestURL = $Url.resolve("~/Handler/RequestPubmedArticle.ashx")
		this.LoadTemplate('comment');
		this.LoadTemplate('recommendation');
		this.articleid = articleid
		this.LoadRecsAndComments()
		$('.feed_comment textarea').watermark('add comment...');
		$('.feed_comment textarea').keyup(->$(this).parent().removeClass('error'))
		mng = this
		$('.feed_comment #btnAddComment').click(->
			txtComment = $(this).parent().find('textarea')
			comment = txtComment.val()
			if comment.length > 0
				mng.AddComment(txtComment.val())
				txtComment.val('')
			else
				txtComment.parent().addClass('error')
		)
		$('#hlRequest').click(=>
			this.RequestPubmedArticle()
		)

	RequestPubmedArticle: () ->
		if not $('#hlRequest').parent().hasClass('disabled')
			$.get(
				this.requestURL
				PubmedID : this.articleid
				(response) => 
					$('#hlRequest').text('Requested')
					$('#hlRequest').parent().addClass('disabled')
			)

	LoadTemplate: (name)->
		$.get($Url.resolve("~/Templates/JSR/" + name + ".html"), (template)->
			$.templates(name,template);
		)

	LoadRecsAndComments: () =>
		mng = this
		$.get(this.commentsandrecsurl,
			articleid : this.articleid
			(response) -> 
				obj = jQuery.parseJSON(response)
				$('#commentscontainer').html('')
				$('#commentscontainer').append($.render.comment(obj.Comments))
				$('#commentscontainer a.author').PersonHoverCards();
				$('#spanCommentsCount').text(obj.Comments.length)

				if obj.Recommendations.length>0
					$('#spanRecsCount').parent().show();
					$('#recscontainer').show()
					$('#spanRecsCount').text(obj.Recommendations.length)
					$('#recscontainer').html('')
					$('#recscontainer').append($.render.recommendation(obj.Recommendations))
					$('#recscontainer a.author').PersonHoverCards();
				else
					$('#spanRecsCount').parent().hide();
					$('#recscontainer').hide();
				$('.feed_comment .recipients').click(->
					if $(this).attr('isopen') is '0'
						$(this).attr('isopen','1')
						$.get(
							mng.subRecsURL
							articleid : mng.articleid
							authorid : $(this).attr('authorid')
							(response) =>
								obj = jQuery.parseJSON(response)
								lastDiv = $(this).closest('.feed_comment').find('.clearfix').next()
								lastDiv.append('<div class="header">recommends to:</div>')
								lastDiv.append($.render.recommendation(obj))
								lastDiv.find('a.author').PersonHoverCards();
								lastDiv.css('border','solid 1px #A1ACC8')
						)
				)
				$('.tiptip').tipTip({ defaultPosition: "top" })
		)
	
	AddComment:(comment)->
		$.get(
			this.addcommentURL
			articleid : this.articleid
			comment : comment
			(response) =>
				obj = jQuery.parseJSON(response)
				$('#commentscontainer').append($.render.comment(obj))
				$('#spanCommentsCount').text(parseInt($('#spanCommentsCount').text())+1)
		)
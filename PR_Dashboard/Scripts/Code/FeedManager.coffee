last = false

$(window).scroll(->
	if last is false
		if $(window).scrollTop() is ($(document).height() - $(window).height())
			fm.LoadMore()
)

class FeedManager
	constructor:()->
		this.fim = new FeedItemManager()
		this.commentsMng = new CommentsManager()
		this.CurQuery = ''
	
	LoadTemplate: (name)->
		$.get($Url.resolve("~/Templates/JSR/" + name + ".html"), (template)->
			$.templates(name,template);
		)

	GetCommentsManager: () ->
		this.commentsMng

	Init: () =>
		$("#hfLastIndex").val(0)
		this.LoadTemplate('comments');
		this.LoadTemplate('pubmed');
		this.LoadTemplate('date');
		this.LoadTemplate('doi');
		this.commentsMng.Init()

		
		$.get($Url.resolve("~/Templates/JSR/feed.html"),
		(response)=> 
			$.templates('feed',response)
			this.PostInit()
		)
	
	TogglePubmedSearch:()=>
		if this.CurQuery.length is 0
			$('#divForwardToPubmedSearch').hide()
		else
			$('#divForwardToPubmedSearch').show()
			$('#divForwardToPubmedSearch a span').html('Search <i>"' + this.CurQuery + '"</i> in Pubmed')
			$('#divForwardToPubmedSearch a').attr('href', 'http://www.ncbi.nlm.nih.gov/pubmed?term=' + this.CurQuery)

	PostInit: () =>
		$('#txtQuery').watermark('Your query here...');
		$('#txtQuery').keypress((e)=>
			if e.keyCode is 13
				this.CurQuery = $('#txtQuery').val()
				this.TogglePubmedSearch()
				this.ClearFeed()
				this.LoadMore()
		)
		$('#btnFind').click(=>
			this.CurQuery = $('#txtQuery').val()
			this.TogglePubmedSearch()
			this.ClearFeed()
			this.LoadMore()
		)
		$('#btnClearQuery').click(=>
			$('#txtQuery').val('')
			this.CurQuery = ''
			this.TogglePubmedSearch()
			this.ClearFeed()
			this.LoadMore()
		)
		$('#btnShowMore').click(=>
			this.LoadMore()
		)
		fm = this
		$('.feedtype').click(->
			$('#hfFeedtype').val($(this).attr('feedtype'))
			$('#tblFeed').html('')
			last = false
			$("#btnShowMore").show()
			$("#hfLastIndex").val(0)
			fm.LoadMore()
		)

	ClearFeed: () =>
		$("#hfLastIndex").val(0)
		$('#tblFeed').html('')

	LoadMore:() =>
		$('#btnShowMore').attr('disabled', 'disabled')
		LoadingPanel.Show()
		PageMethods.GetFeed($('#hfIsFollowed').val(),$('#hfFeedtype').val(),$('#hfPersonID').val(), 
		$("#hfPageOpenDateTime").val(), 
		$("#hfLastIndex").val(), 
		this.CurQuery,
		(response)=>
			$('#btnShowMore').removeAttr('disabled')
			LoadingPanel.Hide()
			obj = jQuery.parseJSON(response)
			this.ShowResult(obj)
		)

	ShowResult:(obj) =>
		
		$("#hfLastIndex").val(parseInt($("#hfLastIndex").val()) + obj.length)
		str = $.render.feed(obj)
		$('#tblFeed').append(str)
		$('.tiptip').tipTip({ defaultPosition: "top" })
		this.fim.AttachEventHandlers()
		this.commentsMng.AttachEventHandlers()
		if obj.length < 10
			$("#btnShowMore").hide()
			last = true
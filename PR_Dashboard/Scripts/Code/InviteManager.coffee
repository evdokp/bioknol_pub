class InviteManager
	constructor:()->
		this.sendingCounter = 0

	Init: () ->
		mng = this
		$('#invModal #invSendEmails').click(->
			mng.SendEmails()
		)
	Show: (articleid) ->
		this.articleid = articleid
		$('#invModal #tblEmailResults').html('')
		$('#invModal').modal()

	SendEmails: () ->
		$('#invModal #invSendEmails').button('loading')
		emails = $('#invModal #txtInvEmails').val().split('\n')
		this.sendingCounter = emails.length
		$.each(emails,(index,value)=>
			this.SendEmail(value)
		)
	SendEmail: (email) ->
		$.get(
			$Url.resolve("~/Handler/invSendEmail.ashx")
			email : email
			(response) => 
				label = ''
				if response is 'success'
					label = '<span class="label label-success">Success</span>'
				else
					label = '<span class="label label-important">Failure</span>'
				$('#invModal #tblEmailResults').append('<tr><td>'+label+' '+email+ '</td></tr>')
				this.sendingCounter--
				if this.sendingCounter is 0
					$('#invModal #invSendEmails').button('reset')
					$('#invModal #txtInvEmails').val('')
		)
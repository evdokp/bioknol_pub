last = false

$(window).scroll(->
	if last is false
		if $(window).scrollTop() is ($(document).height() - $(window).height())
			fm.LoadMore()
)

class FeedManager
	constructor:()->
		this.fim = new FeedItemManager()
		this.commentsMng = new CommentsManager()
		this.feedTmpl = ''
		this.CurQuery = ''


	GetCommentsManager: () ->
		this.commentsMng

	Init: () =>
		$("#hfLastIndex").val(0)
		$('#tmplDate').load($Url.resolve("~/Templates/feed_date.htm"))
		$('#tmplPubmed').load($Url.resolve("~/Templates/feed_pubmed.htm"))
		$('#tmplDoi').load($Url.resolve("~/Templates/feed_doi.htm"))
		$('#tmplRecomendators').load($Url.resolve("~/Templates/feed_recomendators.htm"))
		this.commentsMng.Init()
		$.get($Url.resolve("~/Templates/feed.htm"),
		(response)=> 
			this.feedTmpl = response
			this.PostInit()
		)

	PostInit: () =>
		$('#txtQuery').watermark('Your query here...');
		$('#txtQuery').keypress((e)=>
			if e.keyCode is 13
				this.CurQuery = $('#txtQuery').val()
				this.ClearFeed()
				this.LoadMore()
		)
		$('#btnFind').click(=>
			this.CurQuery = $('#txtQuery').val()
			this.ClearFeed()
			this.LoadMore()
		)
		$('#btnClearQuery').click(=>
			$('#txtQuery').val('')
			this.CurQuery = ''
			this.ClearFeed()
			this.LoadMore()
		)
		$('#btnShowMore').click(=>
			this.LoadMore()
		)
		fm = this
		$('.feedtype').click(->
			$('#hfFeedtype').val($(this).attr('feedtype'))
			$('#tblFeed').html('')
			last = false
			$("#btnShowMore").show()
			$("#hfLastIndex").val(0)
			fm.LoadMore()
		)

	ClearFeed: () =>
		$("#hfLastIndex").val(0)
		$('#tblFeed').html('')

	LoadMore:() =>
		$('#btnShowMore').attr('disabled', 'disabled')
		LoadingPanel.Show()
		PageMethods.GetFeed($('#hfIsFollowed').val(),$('#hfFeedtype').val(),$('#hfPersonID').val(), 
		$("#hfPageOpenDateTime").val(), 
		$("#hfLastIndex").val(), 
		this.CurQuery,
		(response)=>
			$('#btnShowMore').removeAttr('disabled')
			LoadingPanel.Hide()
			obj = jQuery.parseJSON(response)
			this.ShowResult(obj)
		)

	ShowResult:(obj) =>
		$("#hfLastIndex").val(parseInt($("#hfLastIndex").val()) + obj.length)
		$.tmpl(this.feedTmpl, obj).appendTo('#tblFeed')
		$('.tiptip').tipTip({ defaultPosition: "top" })
		this.fim.AttachEventHandlers()
		this.commentsMng.AttachEventHandlers()
		if obj.length < 10
			$("#btnShowMore").hide()
			last = true
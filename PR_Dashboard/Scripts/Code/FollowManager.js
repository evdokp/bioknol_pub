var FollowManager;

FollowManager = (function() {

  FollowManager.name = 'FollowManager';

  function FollowManager() {}

  FollowManager.prototype.Init = function() {
    if (parseInt($('#hfHideAll').val()) === 1) {
      $('#btnWriteMessage').hide();
      return $('#btnFollow').hide();
    } else {
      if (parseInt($('#hfIsOwn').val()) === 1) {
        $('#btnWriteMessage').hide();
        $('#btnFollow').hide();
      } else {
        this.InitFollowButton();
      }
      if (parseInt($('#hfIsGroup').val()) === 1) {
        return $('#btnWriteMessage').hide();
      }
    }
  };

  FollowManager.prototype.IsFollowed = function() {
    return parseInt($('#hfIsObjectFollowed').val());
  };

  FollowManager.prototype.InitFollowButton = function() {
    var _this = this;
    if (this.IsFollowed() === 1) {
      $('#btnFollow').text('Unfollow');
      $('#spanFollowed').show();
    } else {
      $('#btnFollow').text('Follow');
    }
    return $('#btnFollow').click(function() {
      return _this.ToggleFollowing();
    });
  };

  FollowManager.prototype.ToggleFollowing = function() {
    var _this = this;
    return $.get($Url.resolve("~/Handler/ToggleFollow.ashx"), {
      objectid: $('#hfViewedPersonID').val(),
      isgroup: 0
    }, function(response) {
      if (_this.IsFollowed() === 1) {
        $('#spanFollowed').hide();
        $('#btnFollow').text('Follow');
        return $('#hfIsObjectFollowed').val(0);
      } else {
        $('#spanFollowed').show();
        $('#btnFollow').text('Unfollow');
        return $('#hfIsObjectFollowed').val(1);
      }
    });
  };

  return FollowManager;

})();

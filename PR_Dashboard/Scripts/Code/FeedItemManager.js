var FeedItemManager,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

FeedItemManager = (function() {

  FeedItemManager.name = 'FeedItemManager';

  function FeedItemManager() {
    this.ToggleTrash = __bind(this.ToggleTrash, this);

    this.ToggleFavorite = __bind(this.ToggleFavorite, this);

    this.AttachEventHandlers = __bind(this.AttachEventHandlers, this);

    var fim;
    this.abstracturl = $Url.resolve("~/Handler/GetAbstract.ashx");
    this.widgets = $Url.resolve("~/Handler/GetWidgets.ashx");
    this.fulltext = $Url.resolve("~/Handler/GetFulltext.ashx");
    this.request = $Url.resolve("~/Handler/RequestPubmedArticle.ashx");
    this.toggletag = $Url.resolve("~/Handler/ToggleTag.ashx");
    fim = this;
    $('#btnAbstract').live('click', function() {
      var targetDiv;
      targetDiv = $(this).parents('tr').find('#divAbstract');
      return fim.ToggleAbstract($(this).attr('pubmedID'), targetDiv);
    });
    $('.btnRequest').live('click', function() {
      var anchor;
      anchor = $(this);
      return fim.RequestPubmedArticle(anchor);
    });
  }

  FeedItemManager.prototype.ToggleAbstract = function(pmid, targetDiv) {
    var abstract,
      _this = this;
    this.pmid = pmid;
    this.targetDiv = targetDiv;
    if (this.targetDiv.is(":visible")) {
      return this.targetDiv.hide();
    } else {
      abstract = '';
      return $.get(this.abstracturl, {
        PubmedID: this.pmid
      }, function(response) {
        var target;
        _this.targetDiv.show();
        target = _this.targetDiv.find('div');
        target.html(response);
        return _this.MarkAsRead(_this.pmid);
      });
    }
  };

  FeedItemManager.prototype.GetWidgetsDDL = function(pmid, targetUL) {
    var _this = this;
    this.pmid = pmid;
    this.targetUL = targetUL;
    this.targetUL.html('<img src="http://ws.bioknowledgecenter.ru/Images/ajax-loader.gif"/>');
    return $.get(this.widgets, {
      PubmedID: this.pmid
    }, function(response) {
      return _this.targetUL.html(response);
    });
  };

  FeedItemManager.prototype.GetFulltextDDL = function(pmid, targetUL) {
    var _this = this;
    this.pmid = pmid;
    this.targetUL = targetUL;
    this.targetUL.html('<img src="http://ws.bioknowledgecenter.ru/Images/ajax-loader.gif"/>');
    return $.get(this.fulltext, {
      PubmedID: this.pmid
    }, function(response) {
      return _this.targetUL.html(response);
    });
  };

  FeedItemManager.prototype.RequestPubmedArticle = function(anchor) {
    var pmid,
      _this = this;
    this.anchor = anchor;
    pmid = this.anchor.attr('pubmedID');
    return $.get(this.request, {
      PubmedID: pmid
    }, function(response) {
      return _this.anchor.text('Requested');
    });
  };

  FeedItemManager.prototype.AttachEventHandlers = function() {
    var fim;
    fim = this;
    $('.btnFavorite').off('click').click(function() {
      return fim.ToggleFavorite($(this).parent().parent(), $(this).attr('articleID'), $(this).attr('isdoi'));
    });
    $('.btnTrash').off('click').click(function() {
      return fim.ToggleTrash($(this).parent().parent(), $(this).attr('articleID'), $(this).attr('isdoi'));
    });
    $('.btnWidgets').off('click').click(function() {
      var ul;
      ul = $(this).parent().find('ul');
      return fim.GetWidgetsDDL(ul.attr('pubmedID'), ul);
    });
    return $('.btnFulltext').off('click').click(function() {
      var ul;
      ul = $(this).parent().find('ul');
      return fim.GetFulltextDDL(ul.attr('pubmedID'), ul);
    });
  };

  FeedItemManager.prototype.MarkAsRead = function(pmid) {
    this.pmid = pmid;
    return $('tr[articleid="' + this.pmid + '"] .unread').remove();
  };

  FeedItemManager.prototype.ToggleFavorite = function(parenttd, id, isdoi, ison) {
    var _this = this;
    this.parenttd = parenttd;
    this.id = id;
    this.isdoi = isdoi;
    this.ison = ison;
    return $.get(this.toggletag, {
      articleid: this.id,
      isdoi: this.isdoi,
      tag: 1
    }, function(response) {
      if (response === 'inserted') {
        $('.btnFaveDisp[articleid="' + _this.id + '"]').show();
        $('.btnFavorite[articleid="' + _this.id + '"]').addClass('active');
      }
      if (response === 'deleted') {
        $('.btnFaveDisp[articleid="' + _this.id + '"]').hide();
        return $('.btnFavorite[articleid="' + _this.id + '"]').removeClass('active');
      }
    });
  };

  FeedItemManager.prototype.ToggleTrash = function(parenttd, id, isdoi) {
    var _this = this;
    this.parenttd = parenttd;
    this.id = id;
    this.isdoi = isdoi;
    return $.get(this.toggletag, {
      articleid: this.id,
      isdoi: this.isdoi,
      tag: 2
    }, function(response) {
      var colspan, tr;
      if (response === 'inserted') {
        $('.btnTrashDisp[articleid="' + _this.id + '"]').show();
        $('.btnTrash[articleid="' + _this.id + '"]').addClass('active');
        tr = $(_this.parenttd).parent();
        colspan = tr.find('td').length;
        $('tr[articleid="' + _this.id + '"]').html('<td colspan="' + colspan + '" class="smallfade"><div style="width:150px;" class="mauto">moved to trash</div></td>');
      }
      if (response === 'deleted') {
        $('.btnTrashDisp[articleid="' + _this.id + '"]').hide();
        $('.btnTrash[articleid="' + _this.id + '"]').removeClass('active');
        tr = $(_this.parenttd).parent();
        colspan = tr.find('td').length;
        return $('tr[articleid="' + _this.id + '"]').html('<td colspan="' + colspan + '" class="smallfade"><div style="width:150px;" class="mauto">removed from trash</div></td>');
      }
    });
  };

  return FeedItemManager;

})();

var CommentsManager,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

CommentsManager = (function() {

  function CommentsManager() {
    this.LoadRecsAndComments = __bind(this.LoadRecsAndComments, this);

    this.CollapseAllDivs = __bind(this.CollapseAllDivs, this);

    this.CollapseAllDivsByArticleID = __bind(this.CollapseAllDivsByArticleID, this);

    this.AttachEventHandlers = __bind(this.AttachEventHandlers, this);
    this.initialLoadURL = $Url.resolve("~/Handler/reccomGetInitialDataForArticle.ashx");
    this.addcommentURL = $Url.resolve("~/Handler/reccomAddComment.ashx");
    this.subRecsURL = $Url.resolve("~/Handler/reccomGetRecRecipients.ashx");
  }

  CommentsManager.prototype.LoadTemplate = function(name) {
    return $.get($Url.resolve("~/Templates/JSR/" + name + ".html"), function(template) {
      return $.templates(name, template);
    });
  };

  CommentsManager.prototype.Init = function() {
    this.LoadTemplate('commentsdetails');
    this.LoadTemplate('comment');
    return this.LoadTemplate('recommendation');
  };

  CommentsManager.prototype.AttachEventHandlers = function() {
    var cm;
    cm = this;
    return $('.linkRecsComments').off('click').click(function() {
      return cm.LoadRecsAndComments($(this).closest('tr'));
    });
  };

  CommentsManager.prototype.CollapseAllDivsByArticleID = function(articleid) {
    return $('tr[articleid="' + articleid + '"]').each(function(index) {
      return $(this).find('#divRecCom').html('<div></div>').hide();
    });
  };

  CommentsManager.prototype.CollapseAllDivs = function(tr) {
    return this.CollapseAllDivsByArticleID(tr.attr('articleid'));
  };

  CommentsManager.prototype.LoadRecsAndComments = function(tr) {
    var articleid, datablock, mng;
    articleid = tr.attr('articleid');
    datablock = tr.find('#divRecCom');
    if (datablock.is(":visible") === false) {
      this.CollapseAllDivs(tr);
      datablock.show();
      datablock.find('div').html('<img src="' + $Url.resolve("~/Images/ajax-loader.gif") + '" />');
      mng = this;
      return $.get(this.initialLoadURL, {
        articleid: articleid
      }, function(response) {
        var obj;
        datablock.find('div').html('');
        obj = jQuery.parseJSON(response);
        datablock.find('div').append($.render.commentsdetails(obj));
        $('.tiptip').tipTip({
          defaultPosition: "top"
        });
        datablock.find('.author').PersonHoverCards();
        $('.feed_comment textarea').watermark('add comment...');
        $('.feed_comment textarea').keyup(function() {
          return $(this).parent().removeClass('error');
        });
        $('.feed_comment .recipients').click(function() {
          var _this = this;
          if ($(this).attr('isopen') === '0') {
            $(this).attr('isopen', '1');
            return $.get(mng.subRecsURL, {
              articleid: $(this).closest('tr').attr('articleid'),
              authorid: $(this).attr('authorid')
            }, function(response) {
              var lastDiv;
              obj = jQuery.parseJSON(response);
              lastDiv = $(_this).closest('.feed_comment').find('.clearfix').next();
              lastDiv.append('<div class="header">recommends to:</div>');
              lastDiv.append($.render.recommendation(obj));
              lastDiv.find('.author').PersonHoverCards();
              return lastDiv.css('border', 'solid 1px #A1ACC8');
            });
          }
        });
        return $('.feed_comment #btnAddComment').click(function() {
          var comment, txtComment;
          txtComment = $(this).parent().find('textarea');
          comment = txtComment.val();
          if (comment.length > 0) {
            mng.AddComment($(this), articleid, txtComment.val());
            return txtComment.val('');
          } else {
            return txtComment.parent().addClass('error');
          }
        });
      });
    } else {
      return this.CollapseAllDivs(tr);
    }
  };

  CommentsManager.prototype.AddComment = function(addbutton, articleid, comment) {
    var _this = this;
    return $.get(this.addcommentURL, {
      articleid: articleid,
      comment: comment
    }, function(response) {
      var insBefore, obj;
      insBefore = addbutton.closest('.feed_comment');
      obj = jQuery.parseJSON(response);
      $($.render.comment(obj)).insertBefore(insBefore);
      return _this.IncreaseCommentsCount(articleid);
    });
  };

  CommentsManager.prototype.IncreaseCommentsCount = function(articleid) {
    return $('tr[articleid="' + articleid + '"]').each(function(index) {
      var commentsCountSpan;
      commentsCountSpan = $(this).find('#commentsCount');
      if (commentsCountSpan.text() === 'Add comment') {
        $(this).find('.icon-comment').show();
        return commentsCountSpan.text('1');
      } else {
        return commentsCountSpan.text(parseInt(commentsCountSpan.text()) + 1);
      }
    });
  };

  CommentsManager.prototype.IncreaseRecsCount = function(articleid) {
    return $('tr[articleid="' + articleid + '"]').each(function(index) {
      var recsCountSpan;
      recsCountSpan = $(this).find('#recsCount');
      if (recsCountSpan.text().length === 0) {
        $(this).find('.icon-thumbs-up').show();
        recsCountSpan.show();
        return recsCountSpan.text('1');
      } else {
        return recsCountSpan.text(parseInt(recsCountSpan.text()) + 1);
      }
    });
  };

  return CommentsManager;

})();

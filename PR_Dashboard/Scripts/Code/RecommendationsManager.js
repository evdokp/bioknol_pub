var RecommendationsManager,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

RecommendationsManager = (function() {

  function RecommendationsManager() {
    this.SetCallback = __bind(this.SetCallback, this);
    this.articleid = '';
    this.recid = 0;
    this.sendingCounter = 0;
    this.Callback = void 0;
  }

  RecommendationsManager.prototype.Init = function() {
    var mng;
    mng = this;
    $('#recModal #recAlreadyRec tr').live('click', function() {
      return mng.RemoveRec($(this));
    });
    $('#recModal #recSearchResults tr').live('click', function() {
      return mng.SelectPerson($(this));
    });
    $('#recModal #recPersonQuery').keyup(function() {
      return mng.LoadByQuery($('#recModal #recPersonQuery').val());
    });
    $('#recModal #recSendEmails').click(function() {
      return mng.SendEmails();
    });
    return $('#recSaveUpdateClose').click(function() {
      return mng.SaveUpdateClose();
    });
  };

  RecommendationsManager.prototype.SetCallback = function(callback) {
    return this.Callback = callback;
  };

  RecommendationsManager.prototype.SaveUpdateClose = function() {
    var comment,
      _this = this;
    comment = $('#recAddRecommedComment').val();
    return $.get($Url.resolve("~/Handler/recSaveCommentUpdate.ashx"), {
      recid: this.recid,
      comment: $('#recAddRecommedComment').val()
    }, function(response) {
      $('#recModal').modal('hide');
      if (_this.Callback !== void 0) {
        _this.Callback();
      }
      if (comment.length > 0) {
        if (fm !== void 0) {
          fm.GetCommentsManager().IncreaseCommentsCount(_this.articleid);
          return $('#recAddRecommedComment').val('');
        }
      }
    });
  };

  RecommendationsManager.prototype.SendEmails = function() {
    var emails,
      _this = this;
    $('#recModal #recSendEmails').button('loading');
    emails = $('#recModal #txtRecEmails').val().split('\n');
    this.sendingCounter = emails.length;
    return $.each(emails, function(index, value) {
      var comment;
      comment = $('#recAddRecommedComment').val();
      return _this.SendEmail(value, comment);
    });
  };

  RecommendationsManager.prototype.SendEmail = function(email, comment) {
    var _this = this;
    return $.get($Url.resolve("~/Handler/recSendEmail.ashx"), {
      recid: this.recid,
      email: email,
      comment: comment
    }, function(response) {
      var label;
      label = '';
      if (response === 'success') {
        label = '<span class="label label-success">Success</span>';
      } else {
        label = '<span class="label label-important">Failure</span>';
      }
      $('#recModal #tblEmailResults').append('<tr><td>' + label + ' ' + email + '</td></tr>');
      _this.sendingCounter--;
      if (_this.sendingCounter === 0) {
        $('#recModal #recSendEmails').button('reset');
        return $('#recModal #txtRecEmails').val('');
      }
    });
  };

  RecommendationsManager.prototype.RemoveRec = function(tr) {
    var personid,
      _this = this;
    personid = tr.attr('personid');
    return $.get($Url.resolve("~/Handler/recRemoveRec.ashx"), {
      recid: this.recid,
      persontoid: personid
    }, function(response) {
      return tr.remove();
    });
  };

  RecommendationsManager.prototype.Show = function(articleid, articletitle) {
    var _this = this;
    this.articleid = articleid;
    if (fm !== void 0) {
      fm.GetCommentsManager().CollapseAllDivsByArticleID(this.articleid);
    }
    $('#recModal #recArticleTitle').html(articletitle);
    $('#recModal #tblEmailResults').html('');
    $('#recModal').modal();
    return $.get($Url.resolve("~/Handler/recRecommendArticle.ashx"), {
      articleid: articleid
    }, function(response) {
      var result;
      result = jQuery.parseJSON(response);
      if (parseInt(result.IsNew) === 1) {
        if (fm !== void 0) {
          fm.GetCommentsManager().IncreaseRecsCount(_this.articleid);
        }
      }
      _this.recid = parseInt(result.RecID);
      _this.LoadAlready();
      return _this.LoadRecent();
    });
  };

  RecommendationsManager.prototype.LoadByQuery = function(query) {
    var _this = this;
    if (query.length > 1) {
      $('#recModal #recSearchResults').html('');
      $.get($Url.resolve("~/Handler/recGetPersonsByLike.ashx"), {
        recid: this.recid,
        tags: query
      }, function(response) {
        var data;
        data = jQuery.parseJSON(response);
        return _this.RenderPersons('#recModal #recSearchResults', data);
      });
    }
    if (query.length === 0) {
      return this.LoadRecent();
    }
  };

  RecommendationsManager.prototype.LoadAlready = function() {
    var _this = this;
    $('#recModal #recAlreadyRec').html('');
    return $.get($Url.resolve("~/Handler/recGetPersonsAlready.ashx"), {
      recid: this.recid
    }, function(response) {
      var data;
      data = jQuery.parseJSON(response);
      return _this.RenderPersons('#recModal #recAlreadyRec', data);
    });
  };

  RecommendationsManager.prototype.LoadRecent = function() {
    var _this = this;
    $('#recModal #recSearchResults').html('');
    return $.get($Url.resolve("~/Handler/recGetRecentPersons.ashx"), {
      recid: this.recid
    }, function(response) {
      var data;
      data = jQuery.parseJSON(response);
      return _this.RenderPersons('#recModal #recSearchResults', data);
    });
  };

  RecommendationsManager.prototype.RenderPersons = function(target, data) {
    if (data.length > 0) {
      $(target).show();
      return $("#tmplPerson").tmpl(data).appendTo(target);
    }
  };

  RecommendationsManager.prototype.SelectPerson = function(tr) {
    var personid,
      _this = this;
    personid = tr.attr('personid');
    return $.get($Url.resolve("~/Handler/recRecommendArticleToPerson.ashx"), {
      recid: this.recid,
      persontoid: personid
    }, function(response) {
      tr.remove();
      return _this.LoadAlready();
    });
  };

  return RecommendationsManager;

})();

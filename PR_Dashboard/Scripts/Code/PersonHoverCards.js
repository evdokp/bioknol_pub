(function() {

  (function($, window) {
    return $.extend($.fn, {
      PersonHoverCards: function(options) {
        var settings,
          _this = this;
        this.dataURL = $Url.resolve("~/Handler/hovercardGetPerson.ashx");
        this.defaultOptions = {
          myOption: 'default-value',
          another: 'default'
        };
        settings = $.extend({}, this.defaultOptions, options);
        $.get($Url.resolve("~/Templates/JSR/personhovercard.html"), function(template) {
          return $.templates('personhovercard', template);
        });
        return this.each(function(i, el) {
          var $el, containerid, hovercontainer, img, personid;
          $el = $(el);
          personid = $el.attr('personid');
          containerid = 'personHCcontent' + personid;
          hovercontainer = '<div class="smallfade fleft" style="margin-right:5px; ">Specializes in:</div> <div class="';
          hovercontainer = hovercontainer.concat(containerid).concat('"></div>');
          img = $el.parent().parent().find('.avatar[personid="' + personid + '"]');
          return $el.hovercard({
            detailsHTML: hovercontainer,
            width: 300,
            cardImgSrc: img.attr('src'),
            onHoverIn: function() {
              $('.' + containerid).html('loading...');
              return $.get(_this.dataURL, {
                personid: personid
              }, function(response) {
                return $('.' + containerid).html($.render.personhovercard($.parseJSON(response)));
              });
            }
          });
        });
      }
    });
  })(this.jQuery || this.Zepto, this);

}).call(this);

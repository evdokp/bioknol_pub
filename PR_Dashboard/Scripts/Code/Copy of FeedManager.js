var FeedManager, last,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

last = false;

$(window).scroll(function() {
  if (last === false) {
    if ($(window).scrollTop() === ($(document).height() - $(window).height())) {
      return fm.LoadMore();
    }
  }
});

FeedManager = (function() {

  function FeedManager() {
    this.ShowResult = __bind(this.ShowResult, this);

    this.LoadMore = __bind(this.LoadMore, this);

    this.ClearFeed = __bind(this.ClearFeed, this);

    this.PostInit = __bind(this.PostInit, this);

    this.Init = __bind(this.Init, this);
    this.fim = new FeedItemManager();
    this.commentsMng = new CommentsManager();
    this.feedTmpl = '';
    this.CurQuery = '';
  }

  FeedManager.prototype.GetCommentsManager = function() {
    return this.commentsMng;
  };

  FeedManager.prototype.Init = function() {
    var _this = this;
    $("#hfLastIndex").val(0);
    $('#tmplDate').load($Url.resolve("~/Templates/feed_date.htm"));
    $('#tmplPubmed').load($Url.resolve("~/Templates/feed_pubmed.htm"));
    $('#tmplDoi').load($Url.resolve("~/Templates/feed_doi.htm"));
    $('#tmplRecomendators').load($Url.resolve("~/Templates/feed_recomendators.htm"));
    this.commentsMng.Init();
    return $.get($Url.resolve("~/Templates/feed.htm"), function(response) {
      _this.feedTmpl = response;
      return _this.PostInit();
    });
  };

  FeedManager.prototype.PostInit = function() {
    var fm,
      _this = this;
    $('#txtQuery').watermark('Your query here...');
    $('#txtQuery').keypress(function(e) {
      if (e.keyCode === 13) {
        _this.CurQuery = $('#txtQuery').val();
        _this.ClearFeed();
        return _this.LoadMore();
      }
    });
    $('#btnFind').click(function() {
      _this.CurQuery = $('#txtQuery').val();
      _this.ClearFeed();
      return _this.LoadMore();
    });
    $('#btnClearQuery').click(function() {
      $('#txtQuery').val('');
      _this.CurQuery = '';
      _this.ClearFeed();
      return _this.LoadMore();
    });
    $('#btnShowMore').click(function() {
      return _this.LoadMore();
    });
    fm = this;
    return $('.feedtype').click(function() {
      $('#hfFeedtype').val($(this).attr('feedtype'));
      $('#tblFeed').html('');
      last = false;
      $("#btnShowMore").show();
      $("#hfLastIndex").val(0);
      return fm.LoadMore();
    });
  };

  FeedManager.prototype.ClearFeed = function() {
    $("#hfLastIndex").val(0);
    return $('#tblFeed').html('');
  };

  FeedManager.prototype.LoadMore = function() {
    var _this = this;
    $('#btnShowMore').attr('disabled', 'disabled');
    LoadingPanel.Show();
    return PageMethods.GetFeed($('#hfIsFollowed').val(), $('#hfFeedtype').val(), $('#hfPersonID').val(), $("#hfPageOpenDateTime").val(), $("#hfLastIndex").val(), this.CurQuery, function(response) {
      var obj;
      $('#btnShowMore').removeAttr('disabled');
      LoadingPanel.Hide();
      obj = jQuery.parseJSON(response);
      return _this.ShowResult(obj);
    });
  };

  FeedManager.prototype.ShowResult = function(obj) {
    $("#hfLastIndex").val(parseInt($("#hfLastIndex").val()) + obj.length);
    $.tmpl(this.feedTmpl, obj).appendTo('#tblFeed');
    $('.tiptip').tipTip({
      defaultPosition: "top"
    });
    this.fim.AttachEventHandlers();
    this.commentsMng.AttachEventHandlers();
    if (obj.length < 10) {
      $("#btnShowMore").hide();
      return last = true;
    }
  };

  return FeedManager;

})();

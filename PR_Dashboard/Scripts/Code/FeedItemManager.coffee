class FeedItemManager
	constructor:()->
		this.abstracturl = $Url.resolve("~/Handler/GetAbstract.ashx")
		this.widgets = $Url.resolve("~/Handler/GetWidgets.ashx")
		this.fulltext = $Url.resolve("~/Handler/GetFulltext.ashx")
		this.request = $Url.resolve("~/Handler/RequestPubmedArticle.ashx")
		this.toggletag = $Url.resolve("~/Handler/ToggleTag.ashx")
		fim = this
		$('#btnAbstract').live('click',->
			targetDiv = $(this).parents('tr').find('#divAbstract')
			fim.ToggleAbstract($(this).attr('pubmedID'), targetDiv)
		)
		$('.btnRequest').live('click',->
			anchor = $(this)
			fim.RequestPubmedArticle(anchor)
		)


	ToggleAbstract: (@pmid,@targetDiv) ->
		if @targetDiv.is(":visible")
			@targetDiv.hide()
		else
			abstract = ''
			$.get(
				this.abstracturl
				PubmedID : @pmid
				(response) => 
					@targetDiv.show()
					target = @targetDiv.find('div')
					target.html(response)
					this.MarkAsRead(@pmid)
			)
	
	GetWidgetsDDL: (@pmid,@targetUL) ->
		@targetUL.html('<img src="http://ws.bioknowledgecenter.ru/Images/ajax-loader.gif"/>')
		$.get(
			this.widgets
			PubmedID : @pmid
			(response) => 
				@targetUL.html(response)
		)

	GetFulltextDDL: (@pmid,@targetUL) ->
		@targetUL.html('<img src="http://ws.bioknowledgecenter.ru/Images/ajax-loader.gif"/>')
		$.get(
			this.fulltext
			PubmedID : @pmid
			(response) => 
				@targetUL.html(response)
		)
		
	RequestPubmedArticle: (@anchor) ->
		pmid = @anchor.attr('pubmedID')
		$.get(
			this.request
			PubmedID : pmid
			(response) => 
				@anchor.text('Requested')
		)

	AttachEventHandlers: =>
		fim = this
		$('.btnFavorite').off('click').click(->
			fim.ToggleFavorite($(this).parent().parent(),$(this).attr('articleID'),$(this).attr('isdoi'))
		)
		$('.btnTrash').off('click').click(->
			fim.ToggleTrash($(this).parent().parent(),$(this).attr('articleID'),$(this).attr('isdoi'))
		)
		$('.btnWidgets').off('click').click(->
			ul = $(this).parent().find('ul')
			fim.GetWidgetsDDL(ul.attr('pubmedID'), ul)
		)
		$('.btnFulltext').off('click').click(->
			ul = $(this).parent().find('ul')
			fim.GetFulltextDDL(ul.attr('pubmedID'), ul)
		)

	MarkAsRead: (@pmid) ->
		$('tr[articleid="' + @pmid + '"] .unread').remove()

	ToggleFavorite: (@parenttd, @id,@isdoi,@ison) =>
		$.get(
			this.toggletag
			articleid : @id
			isdoi : @isdoi
			tag : 1
			(response) => 
				if response is 'inserted'
					$('.btnFaveDisp[articleid="' + @id + '"]').show()
					$('.btnFavorite[articleid="' + @id + '"]').addClass('active')
				if response is 'deleted'
					$('.btnFaveDisp[articleid="' + @id + '"]').hide()
					$('.btnFavorite[articleid="' + @id + '"]').removeClass('active')	
		)
	
	ToggleTrash: (@parenttd, @id,@isdoi) =>
		$.get(
			this.toggletag
			articleid : @id
			isdoi : @isdoi
			tag : 2
			(response) => 
				if response is 'inserted'
					$('.btnTrashDisp[articleid="' + @id + '"]').show()
					$('.btnTrash[articleid="' + @id + '"]').addClass('active')
					tr = $(@parenttd).parent()
					colspan = tr.find('td').length
					$('tr[articleid="' + @id + '"]').html('<td colspan="'+colspan+'" class="smallfade"><div style="width:150px;" class="mauto">moved to trash</div></td>')
				if response is 'deleted'
					$('.btnTrashDisp[articleid="' + @id + '"]').hide()
					$('.btnTrash[articleid="' + @id + '"]').removeClass('active')	
					tr = $(@parenttd).parent()
					colspan = tr.find('td').length
					$('tr[articleid="' + @id + '"]').html('<td colspan="'+colspan+'" class="smallfade"><div style="width:150px;" class="mauto">removed from trash</div></td>')
		)
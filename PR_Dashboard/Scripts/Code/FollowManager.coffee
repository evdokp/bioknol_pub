class FollowManager
	constructor:()->
		

	Init: () ->
		if parseInt($('#hfHideAll').val()) is 1
			$('#btnWriteMessage').hide()
			$('#btnFollow').hide()
		else 
			if parseInt($('#hfIsOwn').val()) is 1
				$('#btnWriteMessage').hide()
				$('#btnFollow').hide()
			else
				this.InitFollowButton()
			if parseInt($('#hfIsGroup').val()) is 1
				$('#btnWriteMessage').hide()

	IsFollowed:()->
		parseInt($('#hfIsObjectFollowed').val())

	InitFollowButton: () ->
		
		if this.IsFollowed() is 1
			$('#btnFollow').text('Unfollow')
			$('#spanFollowed').show()
		else
			$('#btnFollow').text('Follow')
		$('#btnFollow').click(=> this.ToggleFollowing())

	ToggleFollowing: () ->
		$.get(
			$Url.resolve("~/Handler/ToggleFollow.ashx")
			objectid : $('#hfViewedPersonID').val()
			isgroup : 0
			(response) => 
				if this.IsFollowed() is 1
					$('#spanFollowed').hide()
					$('#btnFollow').text('Follow')
					$('#hfIsObjectFollowed').val(0)
				else
					$('#spanFollowed').show()
					$('#btnFollow').text('Unfollow')
					$('#hfIsObjectFollowed').val(1)

		)
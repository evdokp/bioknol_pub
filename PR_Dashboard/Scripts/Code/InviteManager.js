var InviteManager;

InviteManager = (function() {

  InviteManager.name = 'InviteManager';

  function InviteManager() {
    this.sendingCounter = 0;
  }

  InviteManager.prototype.Init = function() {
    var mng;
    mng = this;
    return $('#invModal #invSendEmails').click(function() {
      return mng.SendEmails();
    });
  };

  InviteManager.prototype.Show = function(articleid) {
    this.articleid = articleid;
    $('#invModal #tblEmailResults').html('');
    return $('#invModal').modal();
  };

  InviteManager.prototype.SendEmails = function() {
    var emails,
      _this = this;
    $('#invModal #invSendEmails').button('loading');
    emails = $('#invModal #txtInvEmails').val().split('\n');
    this.sendingCounter = emails.length;
    return $.each(emails, function(index, value) {
      return _this.SendEmail(value);
    });
  };

  InviteManager.prototype.SendEmail = function(email) {
    var _this = this;
    return $.get($Url.resolve("~/Handler/invSendEmail.ashx"), {
      email: email
    }, function(response) {
      var label;
      label = '';
      if (response === 'success') {
        label = '<span class="label label-success">Success</span>';
      } else {
        label = '<span class="label label-important">Failure</span>';
      }
      $('#invModal #tblEmailResults').append('<tr><td>' + label + ' ' + email + '</td></tr>');
      _this.sendingCounter--;
      if (_this.sendingCounter === 0) {
        $('#invModal #invSendEmails').button('reset');
        return $('#invModal #txtInvEmails').val('');
      }
    });
  };

  return InviteManager;

})();

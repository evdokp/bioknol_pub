class CommentsManager
	constructor:()->
		this.initialLoadURL = $Url.resolve("~/Handler/reccomGetInitialDataForArticle.ashx")
		this.addcommentURL = $Url.resolve("~/Handler/reccomAddComment.ashx")
		this.subRecsURL = $Url.resolve("~/Handler/reccomGetRecRecipients.ashx")

	LoadTemplate: (name)->
		$.get($Url.resolve("~/Templates/JSR/" + name + ".html"), (template)->
			$.templates(name,template);
		)

	Init: () ->
		this.LoadTemplate('commentsdetails');
		this.LoadTemplate('comment');
		this.LoadTemplate('recommendation');

	AttachEventHandlers: =>
		cm = this
		$('.linkRecsComments').off('click').click(->
			cm.LoadRecsAndComments($(this).closest('tr'))
		)

	CollapseAllDivsByArticleID: (articleid) =>
		$('tr[articleid="' + articleid + '"]').each((index)->
			$(this).find('#divRecCom').html('<div></div>').hide()
		)

	CollapseAllDivs: (tr) =>
		this.CollapseAllDivsByArticleID(tr.attr('articleid'))

	LoadRecsAndComments: (tr) =>
		
		articleid = tr.attr('articleid')
		datablock = tr.find('#divRecCom')
		if datablock.is(":visible") is false
			this.CollapseAllDivs(tr)
			datablock.show()
			datablock.find('div').html('<img src="' + $Url.resolve("~/Images/ajax-loader.gif") + '" />')
			mng = this
			$.get(
					this.initialLoadURL
					articleid : articleid
					(response) -> 
						datablock.find('div').html('')
						obj = jQuery.parseJSON(response)
						datablock.find('div').append($.render.commentsdetails(obj))
						$('.tiptip').tipTip({ defaultPosition: "top" })
						datablock.find('.author').PersonHoverCards();
						$('.feed_comment textarea').watermark('add comment...');
						$('.feed_comment textarea').keyup(->
							$(this).parent().removeClass('error')
						)
						$('.feed_comment .recipients').click(->
							if $(this).attr('isopen') is '0'
								$(this).attr('isopen','1')
								$.get(
									mng.subRecsURL
									articleid : $(this).closest('tr').attr('articleid')
									authorid : $(this).attr('authorid')
									(response) =>
										obj = jQuery.parseJSON(response)
										lastDiv = $(this).closest('.feed_comment').find('.clearfix').next()
										lastDiv.append('<div class="header">recommends to:</div>')
										lastDiv.append($.render.recommendation(obj))
										lastDiv.find('.author').PersonHoverCards();
										lastDiv.css('border','solid 1px #A1ACC8')
								)
						)
						$('.feed_comment #btnAddComment').click(->
							txtComment = $(this).parent().find('textarea')
							comment = txtComment.val()
							if comment.length > 0
								mng.AddComment($(this),articleid,txtComment.val())
								txtComment.val('')
							else
								txtComment.parent().addClass('error')
						)
			)
		else
			this.CollapseAllDivs(tr)

	AddComment:(addbutton,articleid,comment)->
		$.get(
			this.addcommentURL
			articleid : articleid
			comment : comment
			(response) =>
				insBefore = addbutton.closest('.feed_comment')
				obj = jQuery.parseJSON(response)
				$($.render.comment(obj)).insertBefore(insBefore)
				this.IncreaseCommentsCount(articleid)
		)

	IncreaseCommentsCount:(articleid) ->
		$('tr[articleid="' + articleid + '"]').each((index)->
			commentsCountSpan = $(this).find('#commentsCount')
			if commentsCountSpan.text() is 'Add comment'
				$(this).find('.icon-comment').show()
				commentsCountSpan.text('1')
			else
				commentsCountSpan.text(parseInt(commentsCountSpan.text())+1)
		)

	IncreaseRecsCount:(articleid) ->
		$('tr[articleid="' + articleid + '"]').each((index)->
			recsCountSpan = $(this).find('#recsCount')
			if recsCountSpan.text().length is 0
				$(this).find('.icon-thumbs-up').show()
				recsCountSpan.show()
				recsCountSpan.text('1')
			else
				recsCountSpan.text(parseInt(recsCountSpan.text())+1)
		)
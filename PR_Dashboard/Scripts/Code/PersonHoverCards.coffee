(($, window) ->
	$.extend $.fn, PersonHoverCards: (options) ->
		@dataURL = $Url.resolve("~/Handler/hovercardGetPerson.ashx")
		@defaultOptions = 
			myOption: 'default-value'
			another:  'default'
		settings = $.extend({}, @defaultOptions, options)
		$.get($Url.resolve("~/Templates/JSR/personhovercard.html"), (template)->
			$.templates('personhovercard',template);
		)

		@each (i, el) =>
			$el = $(el)
			personid = $el.attr('personid')
			containerid = 'personHCcontent'+personid
			hovercontainer = '<div class="smallfade fleft" style="margin-right:5px; ">Specializes in:</div> <div class="'
			hovercontainer = hovercontainer.concat(containerid).concat('"></div>')
			img = $el.parent().parent().find('.avatar[personid="' + personid + '"]')
			$el.hovercard(
				detailsHTML:hovercontainer
				width: 300
				cardImgSrc: img.attr('src')
				onHoverIn: =>
					$('.' + containerid).html('loading...')
					$.get(@dataURL,
						personid:personid,
						(response)->
							$('.' + containerid).html($.render.personhovercard($.parseJSON(response)))
					)
					
			)
	
) this.jQuery or this.Zepto, this
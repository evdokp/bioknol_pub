interface JQueryStatic {
template( str: any ): string;
templates(name: string, template: any): any;
}

declare var jQuery: JQueryStatic;
declare var $: JQueryStatic;
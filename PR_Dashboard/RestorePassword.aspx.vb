﻿Public Class RestorePassword
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub SubmitButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SubmitButton.Click
        Dim u As MembershipUser
        u = Membership.GetUser(Me.UserName.Text, False)

        If u Is Nothing Then
            Me.FailureText.Text = "There's no user with such e-mail."
        Else
            Dim vBody As String
            vBody = BLL.MailSender.ReadFile.ReadFile(Me.Page.Server, "RemindPassword.htm")
            vBody = vBody.Replace("<%Password%>", u.GetPassword())
            vBody = vBody.Replace("<%UserName%>", u.UserName)
            Dim userProfile As New ProfileCommon
            userProfile = userProfile.GetProfile(u.UserName)

            Dim _p As New BLL.Person(userProfile.UserName)
            Dim perstoFIO As String = IIf(_p.FirstName.Length = 0, "PRD user", _p.FirstName)
            vBody = vBody.Replace("<%FIO%>", perstoFIO)
            BLL.MailSender.SendMailMessage("noreply@bioknowledgecenter.ru", "Plug-in for Biblio-sensor", u.UserName, String.Empty, String.Empty, "Plug-in for BiblioSensor password restore", vBody)
            mv_pass.ActiveViewIndex = 1
        End If

    End Sub

End Class
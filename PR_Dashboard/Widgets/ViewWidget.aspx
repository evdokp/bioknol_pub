﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewWidget.aspx.vb" Inherits="BioWS.ViewWidget" %>
<%@ Register assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxLoadingPanel" tagprefix="dx" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="background: none;">
<head runat="server">
    <title></title>
      <script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js"></script>
    <script type="text/javascript">

        var isfirstload = "0";

        $(document).ready(function () {


            LoadingPanel.Show();
            $('#myframe').Load(function () {
                
            });

        });

        function MyFunction() {
            var frm = document.getElementById('form1');
            if (frm) {
                frm.submit();
            }
        }
        window.onload = function () {
            MyFunction();
        };

        function hide() {
            if (isfirstload == "1") {
                LoadingPanel.Hide();
            } else {
                isfirstload = "1"
            }
        }

    </script>
</head>
<body style="background: none;">
      <dx:ASPxLoadingPanel ID="ASPxLoadingPanel1" runat="server"  ClientInstanceName="LoadingPanel"
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx"   
        ImageSpacing="5px" ContainerElementID="my_iframe" Modal="True" Text="Loading content provided by the widget&hellip;">
        <Image Url="~/App_Themes/DevEx/Web/Loading.gif">
        </Image>
    </dx:ASPxLoadingPanel>
  
    <form 
            
            method="post"
            target="my_iframe" 
            runat="server" 
            id="form1">
    
    <asp:HiddenField 
            ID="PubmedIDs" 
            runat="server"  Value="none"
            />

    </form>
    <iframe name="my_iframe"  frameborder="0"  id="myframe" onload="hide();"
            src="" 
            width="100%"  
            height="544" 
            ></iframe>
            
</body>
</html>

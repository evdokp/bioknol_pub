﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions

Public Class Handler1
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim wID As Integer = context.Request("WidgetID")
        Dim w As New BLL.Widgets.Widget(wID)
        Dim l = ArticlesDA.GetDistinctHistoryPMIDS_byPerson(context.Request("PersonID"), Today.AddDays(-100), Today.AddDays(1))
        Dim s As String
        Dim ct As String
        Dim res As Object
        If l.Count = 0 Then
            res = w.ExternalResponse("none", ct)
        Else
            s = l.ListToString()
            res = w.ExternalResponse(s, ct)
        End If
        context.Response.Write(res)
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
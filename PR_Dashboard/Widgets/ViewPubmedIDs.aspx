﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewPubmedIDs.aspx.vb" Inherits="BioWS.ViewPubmedIDs" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1.Export, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" style=" background :none;">
<head runat="server">
    <title></title>
</head>
<body style=" background :none;">
    <form id="form1" runat="server">
    <div style="width:480px; ;word-wrap:break-word">


        <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" 
            GridViewID="ASPxGridView1">
        </dx:ASPxGridViewExporter>

        <table cellspacing="3">
        <tr>
        <td><dx:ASPxButton ID="btnXLS" runat="server" 
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
            SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Export to XLS" 
            Width="147px">
        </dx:ASPxButton></td>
        <td>
        <dx:ASPxButton ID="btnXLSX" runat="server" 
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
            SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Export to XLSX" 
            Width="147px">
        </dx:ASPxButton></td>   <td>
        <dx:ASPxButton ID="ASPxButton1" runat="server" 
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
            SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Export to CSV" 
            Width="147px">
        </dx:ASPxButton></td>
        </tr>
        
        
        <tr> 
        <td colspan="3">
        
      



        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" 
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
            <Columns>
                <dx:GridViewDataTextColumn Caption="PubmedID" Name="PubmedID" VisibleIndex="0" FieldName="PubmedID">
                </dx:GridViewDataTextColumn>
            </Columns>
            <SettingsPager PageSize="18">
            </SettingsPager>
            <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                </LoadingPanelOnStatusBar>
                <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                </LoadingPanel>
            </Images>
            <ImagesFilterControl>
                <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                </LoadingPanel>
            </ImagesFilterControl>
            <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="5px">
                </LoadingPanel>
            </Styles>
            <StylesEditors ButtonEditCellSpacing="0">
                <ProgressBar Height="21px">
                </ProgressBar>
            </StylesEditors>
        </dx:ASPxGridView>
  
        </td>
        </tr>
        </table>

        
        


    </div>
    </form>
</body>
</html>

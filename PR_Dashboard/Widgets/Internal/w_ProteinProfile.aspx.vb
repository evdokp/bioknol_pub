﻿Imports System.Net
Imports DevExpress.Web.ASPxCloudControl

Public Class w_ProteinProfile
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim pmid_batch_str As String
        pmid_batch_str = Context.Request("PubmedIDs")
        Dim proteins As New Dictionary(Of String, String)
        Dim pmid_array = pmid_batch_str.Split(";")
        For Each pmidstr As String In pmid_array
            Dim pmid As Integer
            If Integer.TryParse(pmidstr, pmid) = True Then
                ProcessPMID(pmid, proteins)
            End If
        Next

        Dim s As String
        s = "<ul>"

        Dim proteinslst = From aa In proteins
                          Select aa
                          Order By aa.Value


        For Each bb In proteinslst
            s = s & "<li><a href=""http://www.uniprot.org/uniprot/" & bb.Key & """ target=""top"">" & bb.Value & "</a></li>"

        Next
        s = s & "</ul>"
        Me.Literal1.Text = s
    End Sub
    Private Sub ProcessPMID(pmid As Integer, proteins As Dictionary(Of String, String))
        Dim wc As New WebClient
        Dim url = "http://www.uniprot.org/uniprot/?query=citation:(" & pmid.ToString & ")&format=tab&columns=protein names,id"
        Dim result As String
        result = wc.DownloadString(url)
        Dim lines As String()
        lines = result.Split(vbLf)
        ' process lines
        For i = 1 To lines.Count - 1
            If lines(i).Length > 0 Then
                Dim line_arr As String()
                line_arr = lines(i).Split(vbTab)
                Dim proteinname As String
                If line_arr(0).IndexOf("(") > 0 Then
                    proteinname = line_arr(0).Substring(0, line_arr(0).IndexOf("(") - 1)
                Else
                    proteinname = line_arr(0)
                End If
                Dim proteinid As String
                proteinid = line_arr(1)
                If proteins.ContainsKey(proteinid) = False Then
                    proteins.Add(proteinid, proteinname)
                End If
            End If
        Next
    End Sub
End Class
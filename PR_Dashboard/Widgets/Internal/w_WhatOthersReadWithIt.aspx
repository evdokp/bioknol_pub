﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="w_WhatOthersReadWithIt.aspx.vb" Inherits="BioWS.w_WhatOthersReadWithIt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="background-image :none; background-color :White ; padding:10px;">
    <form id="form1" runat="server">
    <div>
             <asp:ObjectDataSource ID="ods_articles" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="ArticlesByWidget" TypeName="BioWS.ArticlesDA">
                    <SelectParameters>
                        <asp:Parameter Name="WidgetID" Type="Int32" />
                        <asp:Parameter Name="vPMID" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
             <asp:GridView ID="gv_articles" runat="server" AutoGenerateColumns="False" DataSourceID="ods_articles"
                    Width="100%" ShowHeader="false" GridLines="None">
                    <Columns>
                        <asp:TemplateField SortExpression="Title">
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" Text='<%# Bind("Title") %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HyperLink ID="hl_pubmed" runat="server" ImageUrl="~/Images/pubmed.gif" Target="_blank"></asp:HyperLink>
                                <asp:HyperLink ID="hl_doi" runat="server" ImageUrl="~/Images/doi_icon.jpg" Target="_blank"></asp:HyperLink>
                                <asp:HyperLink ID="hl_download" runat="server" ImageUrl="~/Images/export1.png" Target="_blank"></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle Width="100px" VerticalAlign="Middle" HorizontalAlign="left" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        There's no additional information in this widget.
                    </EmptyDataTemplate>
                </asp:GridView>
    </div>
    </form>
</body>
</html>

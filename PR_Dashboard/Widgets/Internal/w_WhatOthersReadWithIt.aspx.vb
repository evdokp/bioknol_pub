﻿Public Class w_WhatOthersReadWithIt
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.ods_articles.SelectParameters("WidgetID").DefaultValue = Request.QueryString("WidgetID")
        Me.ods_articles.SelectParameters("vPMID").DefaultValue = Request.QueryString("PubmedID")
        Me.ods_articles.DataBind()
        Me.gv_articles.DataBind()
    End Sub
    Protected Sub gv_articles_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gv_articles.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim r_row As ArticlesPubmedDataSet.ArticlesPubmedRow
            r_row = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row, ArticlesPubmedDataSet.ArticlesPubmedRow)

            Dim hl_pubmed As HyperLink
            hl_pubmed = e.Row.Cells(1).FindControl("hl_pubmed")
            Dim hl_doi As HyperLink
            hl_doi = e.Row.Cells(1).FindControl("hl_doi")
            Dim Hyperlink1 As HyperLink
            Hyperlink1 = e.Row.Cells(0).FindControl("Hyperlink1")
            Dim hl_download As HyperLink
            hl_download = e.Row.Cells(1).FindControl("hl_download")



            'show DOI
            'If r_row.IsDOINull = False Then
            '    If r_row.DOI.Length > 2 Then
            '        Hyperlink1.NavigateUrl = "http://dx.doi.org/" & r_row.DOI
            '        hl_doi.Visible = True
            '        hl_doi.ToolTip = "DOI: " & r_row.DOI
            '        hl_doi.NavigateUrl = "http://dx.doi.org/" & r_row.DOI
            '    Else
            '        hl_doi.Visible = False
            '    End If
            'Else
            hl_doi.Visible = False
            'End If


            Dim _pr As New ProfileCommon
            _pr = _pr.GetProfile(User.Identity.Name)

            'show Pubmed

            Dim pm As New BLL.PubMed.PubMedArticle(r_row.Pubmed_ID)
            If pm.OffersWithFilesCount > 0 Then
                hl_download.Visible = True
                hl_download.ToolTip = "Download available"
                If pm.OffersWithFilesCount = 1 Then
                    Dim ta As New ArticlesDataSetTableAdapters.ArticleOffersTableAdapter
                    Dim r = ta.GetDataByPubmedIDwithFiles(r_row.Pubmed_ID).Item(0)
                    hl_download.NavigateUrl = "~\Download.ashx?vFileName=" & HttpUtility.UrlEncode(r.FileName) & "&vOriginalName=" & HttpUtility.UrlEncode(r.OriginalName)
                Else
                    hl_download.NavigateUrl = pm.ViewPMURL
                End If
            Else
                hl_download.Visible = False
            End If
            Hyperlink1.NavigateUrl = pm.ViewPMURL
            hl_pubmed.NavigateUrl = pm.PubmedURL
            hl_pubmed.Visible = True
            hl_pubmed.ToolTip = "PMID: " & pm.Pubmed_ID
                

        End If
    End Sub
End Class
﻿Imports BioWS.BLL.Extensions

Public Class ViewPubmedIDs
    Inherits System.Web.UI.Page

    Private ReadOnly Property ViewedObjectType As String
        Get
            Return Me.QS("objtype")
        End Get
    End Property
    Private ReadOnly Property ViewedObjectID As Integer
        Get
            Return Me.QSInt32("objid")
        End Get
    End Property
    Private ReadOnly Property IsGroup As Boolean
        Get
            Return ViewedObjectType = "g"
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim l As List(Of Integer)
        If IsGroup Then
            'this is group
            l = ArticlesDA.GetDistinctHistoryPMIDS_byGroup(ViewedObjectID, Me.QS("StartDate"), Me.QS("EndDate"))
        Else
            'this is person
            l = ArticlesDA.GetDistinctHistoryPMIDS_byPerson(ViewedObjectID, Me.QS("StartDate"), Me.QS("EndDate"))
        End If

        Dim pmids = From pmid In l
                    Select PubmedID = pmid, EmptyField = ""

        Dim var = pmids.ToList

        ASPxGridView1.DataSource = var 'TODO - extension databind to source
        ASPxGridView1.DataBind()




    End Sub

    Protected Sub btnXLSX_Click(sender As Object, e As EventArgs) Handles btnXLSX.Click
        ASPxGridViewExporter1.WriteXlsToResponse()


    End Sub

    Protected Sub btnXLS_Click(sender As Object, e As EventArgs) Handles btnXLS.Click
        ASPxGridViewExporter1.WriteXlsxToResponse()

    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click
        ASPxGridViewExporter1.WriteCsvToResponse()

    End Sub
End Class
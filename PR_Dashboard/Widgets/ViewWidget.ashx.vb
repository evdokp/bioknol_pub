﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions

Public Class ViewWidget1
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Dim wID As Integer = context.Request.QueryString("WidgetID")
        Dim w As New BLL.Widgets.Widget(wID)

        Dim l As List(Of Integer)
        If context.Request.QueryString("PersonID") = "" Then
            'this is group
            l = ArticlesDA.GetDistinctHistoryPMIDS_byGroup(context.Request.QueryString("GroupID"), context.Request.QueryString("StartDate"), context.Request.QueryString("EndDate"))
        Else
            'this is person
            l = ArticlesDA.GetDistinctHistoryPMIDS_byPerson(context.Request.QueryString("PersonID"), context.Request.QueryString("StartDate"), context.Request.QueryString("EndDate"))
        End If

        Dim s As String
        Dim ct As String
        Dim res As Object
        If l.Count = 0 Then
            res = w.ExternalResponse("none", ct)
        Else
            s = l.ListToString()
            res = w.ExternalResponse(s, ct)
        End If
        Select Case ct
            Case "text/plain"
                context.Response.Write(res)
            Case "text/html"
                context.Response.Write(res)
            Case "image/png"
                Dim f As Byte()
                f = res
                context.Response.ClearContent()
                context.Response.ClearHeaders()
                context.Response.Buffer = True
                context.Response.ContentType = "image/png"
                context.Response.AppendHeader("Accept-Header", f.Length.ToString())
                context.Response.AppendHeader("Pragma", "public")
                context.Response.BinaryWrite(f)
                context.Response.Flush()
                context.Response.End()
        End Select

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
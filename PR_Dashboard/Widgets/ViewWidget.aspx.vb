﻿Imports System.IO
Imports System.Net
Imports BioWS.BLL.Extensions

Public Class ViewWidget
    Inherits System.Web.UI.Page


    Private ReadOnly Property ViewedObjectType As String
        Get
            Return Me.QS("objtype")
        End Get
    End Property
    Private ReadOnly Property ViewedObjectID As Integer
        Get
            Return Me.QSInt32("objid")
        End Get
    End Property
    Private ReadOnly Property IsGroup As Boolean
        Get
            Return ViewedObjectType = "g"
        End Get
    End Property
    Private ReadOnly Property WidgetID As Integer
        Get
            Return Me.QSInt32("WidgetID")
        End Get
    End Property

    Private _widget As BLL.Widgets.Widget
    Private ReadOnly Property Widget As BLL.Widgets.Widget
        Get
            If _widget Is Nothing Then
                _widget = New BLL.Widgets.Widget(WidgetID)
            End If
            Return _widget
        End Get
    End Property



    Private ReadOnly Property IDsAreFromQS As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.QS("PubmedIDs"))
        End Get
    End Property

    Private ReadOnly Property IsSingleIDView As Boolean
        Get
            Return Not String.IsNullOrEmpty(Request.QueryString("Pubmed_ID"))
        End Get
    End Property

    Private Function GetActionURL() As String
        Dim res As String
        If IDsAreFromQS Then
            '! get url from query string 
            res = Me.QS("actionURL").UrlDecode()
        Else
            If Widget.IsInternal = True Then
                Select Case Widget.InternalConst
                    Case "WhatOthersReadWithIt"
                        res = "Internal\w_WhatOthersReadWithIt.aspx" & New QueryStringBuilder().SetQSValue("WidgetID").SetQSValue("Pubmed_ID").ToQueryString()
                    Case "ProteinProfile"
                        res = "Internal\w_ProteinProfile.aspx" & New QueryStringBuilder().SetValue("PersonID", ViewedObjectID).ToQueryString()
                    Case Else
                        res = String.Empty
                End Select
            Else
                res = Widget.Data_WS_URL
            End If
        End If
        Return res
    End Function

    Private Function GetPostData() As String
        Dim res As String
        If IDsAreFromQS Then
            res = Me.QS("PubmedIDs")
        Else
            Dim idsList As List(Of Integer)
            If IsSingleIDView Then
                idsList = New List(Of Integer)
                idsList.Add(Me.QSInt32("Pubmed_ID"))
            Else
                If IsGroup Then
                    idsList = ArticlesDA.GetDistinctHistoryPMIDS_byGroup(ViewedObjectID, Me.QS("StartDate"), Me.QS("EndDate"))
                Else
                    idsList = ArticlesDA.GetDistinctHistoryPMIDS_byPerson(ViewedObjectID, Me.QS("StartDate"), Me.QS("EndDate"))
                End If
            End If


            If idsList.Count = 0 Then
                res = "none"
            Else
                res = idsList.ListToString()
            End If
        End If

        Return res


    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IDsAreFromQS Then
            Widget.Log()
        End If

        form1.Action = GetActionURL()
        PubmedIDs.Value = GetPostData()
    End Sub

End Class
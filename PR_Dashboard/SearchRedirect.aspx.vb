﻿Public Class SearchRedirect
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim vPerson As Integer

        If User.Identity.IsAuthenticated = True Then
            Dim _profile As New ProfileCommon
            _profile = _profile.GetProfile(User.Identity.Name)
            vPerson = _profile.Person_ID
        Else
            vPerson = Request.QueryString("UserID")

        End If

        If vPerson = 0 Then
            Me.Response.Redirect("~\Account\Register.aspx", False)
        Else
            Dim ta As New PersonsDataSetTableAdapters.PersonsTableAdapter
            Dim t = ta.GetDataByID(vPerson)
            Dim r = t.Item(0)
            If User.Identity.IsAuthenticated = False Then
                FormsAuthentication.SetAuthCookie(r.UserName, False)
            End If
            Me.Response.Redirect("~\Account\Search.aspx?PersonID=" & vPerson & "&query=" & Request.QueryString("query"), False)
         
        End If
    End Sub

End Class
﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewItem.aspx.vb" Inherits="BioWS.ViewItem" Theme="HelpTheme"  %>

<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSplitter" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js"></script>
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true"
        EnablePartialRendering="true" EnableScriptGlobalization="true" EnableScriptLocalization="true">
    </ajaxToolkit:ToolkitScriptManager>
    <script type="text/javascript">
        $(document).ready(function () {


            if ($('#hfIsPostBack').val() == 0) {
                $('#hfHASH').val(window.location.hash);
                cp1.PerformCallback();
            }

        });

        function NodeClick(s, e) {

            window.location.hash = e.node.name;

            cp2.PerformCallback(e.node.name);


        }
    

    </script>
    <asp:HiddenField ID="hfIsPostBack" runat="server" ClientIDMode="Static" />

    <div>
        <dx:ASPxCallbackPanel ID="cp1" runat="server" ClientInstanceName="cp1" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
            CssPostfix="DevEx" Width="100%">
            <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
            </LoadingPanelImage>
            <LoadingPanelStyle ImageSpacing="5px">
            </LoadingPanelStyle>
            <PanelCollection>
                <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                    <dx:ASPxSplitter ID="ASPxSplitter1" runat="server" Orientation="Vertical" ClientInstanceName="splitter"
                        FullscreenMode="True" CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css"
                        CssPostfix="Office2010Silver" Height="100%" Width="100%">
                        <Panes>
                            <dx:SplitterPane MaxSize="80" AllowResize="False" ShowCollapseBackwardButton="False"
                                ShowCollapseForwardButton="False" ShowSeparatorImage="False" Size="80px">
                                <ContentCollection>
                                    <dx:SplitterContentControl runat="server" SupportsDisabledAttribute="True">
                                        <table width="100%">
                                            <tr>
                                                <td valign="middle">
                                                    <asp:Image ID="img_title" runat="server" ImageUrl="~/Images/title.png" />
                                                </td>
                                                <td valign="middle" style="font-size: 32px; font-family: Cambria; font-weight: bold;
                                                    font-style: italic; text-align: right; color: #0052A4;">
                                                    Online documentation
                                                </td>
                                            </tr>
                                        </table>
                                    </dx:SplitterContentControl>
                                </ContentCollection>
                            </dx:SplitterPane>
                            <dx:SplitterPane ShowCollapseBackwardButton="False" ShowCollapseForwardButton="False"
                                ShowSeparatorImage="False">
                                <Panes>
                                    <dx:SplitterPane Size="300px" ShowCollapseBackwardButton="True" ShowCollapseForwardButton="True">
                                        <ContentCollection>
                                            <dx:SplitterContentControl runat="server" SupportsDisabledAttribute="True">
                                                <dx:ASPxTreeView ID="tv1" runat="server" AllowSelectNode="true" AutoPostBack="false">
                                                    <ClientSideEvents NodeClick="NodeClick" />
                                                    <Images SpriteCssFilePath="~/App_Themes/Office2003Silver/{0}/sprite.css">
                                                        <NodeLoadingPanel Url="~/App_Themes/Office2003Silver/Web/tvNodeLoading.gif">
                                                        </NodeLoadingPanel>
                                                        <LoadingPanel Url="~/App_Themes/Office2003Silver/Web/Loading.gif">
                                                        </LoadingPanel>
                                                    </Images>
                                                    <Styles CssFilePath="~/App_Themes/Office2003Silver/{0}/styles.css" CssPostfix="Office2003Silver">
                                                    </Styles>
                                                </dx:ASPxTreeView>
                                            </dx:SplitterContentControl>
                                        </ContentCollection>
                                    </dx:SplitterPane>
                                    <dx:SplitterPane ShowCollapseBackwardButton="True" ShowCollapseForwardButton="True"
                                        Name="ContentUrlPane" ScrollBars="Auto" ContentUrlIFrameName="contentUrlPane">
                                        <ContentCollection>
                                            <dx:SplitterContentControl runat="server" SupportsDisabledAttribute="True">
                                                <dx:ASPxCallbackPanel ID="cp2" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" ClientInstanceName="cp2"
                                                    CssPostfix="DevEx" Width="100%">
                                                    <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
                                                    </LoadingPanelImage>
                                                    <LoadingPanelStyle ImageSpacing="5px">
                                                    </LoadingPanelStyle>
                                                    <PanelCollection>
                                                        <dx:PanelContent runat="server" SupportsDisabledAttribute="True">
                                                            <asp:HiddenField ID="hfHASH" runat="server" ClientIDMode="Static" />
                                                            <h1>
                                                                <asp:Literal ID="litTitle" runat="server"></asp:Literal>
                                                            </h1>
                                                            <div id="divBody" style="padding: 5px;">
                                                                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                                            </div>
                                                            <asp:Repeater ID="repChildren" runat="server">
                                                                <HeaderTemplate>
                                                                    <h3>
                                                                        Topics in this section:</h3>
                                                                    <ul>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <li>
                                                                        <asp:HyperLink ID="hlChild" runat="server" Text='<%# Eval("Title") %>' NavigateUrl='<%# Eval("URL") %>'></asp:HyperLink>
                                                                    </li>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </ul>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                        </dx:PanelContent>
                                                    </PanelCollection>
                                                </dx:ASPxCallbackPanel>
                                            </dx:SplitterContentControl>
                                        </ContentCollection>
                                    </dx:SplitterPane>
                                </Panes>
                                <ContentCollection>
                                    <dx:SplitterContentControl runat="server" SupportsDisabledAttribute="True">
                                    </dx:SplitterContentControl>
                                </ContentCollection>
                            </dx:SplitterPane>
                        </Panes>
                        <Styles CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css" CssPostfix="Office2010Silver">
                        </Styles>
                        <Images SpriteCssFilePath="~/App_Themes/Office2010Silver/{0}/sprite.css">
                        </Images>
                    </dx:ASPxSplitter>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
    </div>
    </form>
</body>
</html>

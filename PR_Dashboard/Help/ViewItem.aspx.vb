﻿Imports System.Drawing
Imports System.Linq

Public Class ViewItem
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
   
        If Page.IsPostBack = False Then
            hfIsPostBack.Value = 0
        Else
            hfIsPostBack.Value = 1
        End If

    End Sub


    Private Sub setNode(ByVal pathList As List(Of Integer), ByVal r As DataSetContentItems.v_ContentItems1Row, ByVal node As DevExpress.Web.ASPxTreeView.TreeViewNode)
        If pathList.Contains(r.ID) Then
            If pathList(pathList.Count - 1) = r.ID Then
                tv1.SelectedNode = node
                node.Expanded = True
                setContentByID(r.ID)
            Else
                node.Expanded = True
            End If
        End If
        If r.ChildrenCount > 0 Then
            PopulateNode(r.ID, node, pathList)
        End If
    End Sub


    Private Sub setContentByID(ByVal ciID As Integer)
        Dim ciBodyTA As New DataSetContentItemsTableAdapters.v_ContentItemsTableAdapter
        Dim ciR = ciBodyTA.GetDataByID(ciID)(0)
        litTitle.Text = ciR.Title
        Literal1.Text = ciR.Body




        Dim ciChildTA As New DataSetContentItemsTableAdapters.v_ContentItems1TableAdapter
        Dim ciChildT = ciChildTA.GetDataByParentID(ciID)

        If ciChildT.Count = 0 Then
            repChildren.Visible = False
        Else
            Dim ciChildDataSource = From ci In ciChildT
                        Select Title = ci.Title, URL = String.Format("~\Help\ViewItem.aspx?QS={0}#{1}/{2}", ci.ID, hfHASH.Value, ci.ID)

            repChildren.DataSource = ciChildDataSource
            repChildren.DataBind()
        End If

    End Sub

    Private Sub PopulateTV(ByVal path As List(Of Integer))
        Dim ciTa As New DataSetContentItemsTableAdapters.v_ContentItems1TableAdapter
        Dim ctTopT = ciTa.GetTopLevel()
        For i = 0 To ctTopT.Count - 1
            Dim r = ctTopT(i)
            Dim node As New DevExpress.Web.ASPxTreeView.TreeViewNode
            node.Name = r.ID
            node.Text = r.Title
            tv1.Nodes.Add(node)
            setNode(path, r, node)
        Next
    End Sub


    Private Sub PopulateNode(ByVal parentid As Integer, ByVal parentnode As DevExpress.Web.ASPxTreeView.TreeViewNode, ByVal path As List(Of Integer))
        Dim ciTa As New DataSetContentItemsTableAdapters.v_ContentItems1TableAdapter
        Dim ctT = ciTa.GetDataByParentID(parentid)
        For i = 0 To ctT.Count - 1
            Dim r = ctT(i)
            Dim node As New DevExpress.Web.ASPxTreeView.TreeViewNode
            node.Name = String.Format("{0}/{1}", parentnode.Name, r.ID)
            node.Text = r.Title
            parentnode.Nodes.Add(node)
            setNode(path, r, node)
        Next
    End Sub

    Private Sub cp1_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cp1.Callback
        Dim initialpath As String = hfHASH.Value
        initialpath = initialpath.Replace("#", "")
        Dim pathArray As String() = initialpath.Split("/")
        Dim pathIntList As New List(Of Integer)
        For Each s In pathArray
            Dim id As Integer
            If Integer.TryParse(s, id) Then
                pathIntList.Add(id)
            End If
        Next
        PopulateTV(pathIntList)
        hfIsPostBack.Value = 1
    End Sub
    Private Sub cp2_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cp2.Callback
        Dim id As Integer
        Dim sa As String() = e.Parameter.Split("/")
        hfHASH.Value = e.Parameter
        id = sa(sa.Length - 1)
        setContentByID(id)
    End Sub
End Class
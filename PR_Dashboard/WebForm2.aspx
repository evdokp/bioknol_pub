﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" Theme="Theme2" MasterPageFile="~/Masters/bsside.master" CodeBehind="WebForm2.aspx.vb" Inherits="BioWS.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph1" runat="server">

    
       <asp:Chart ID="dotsMeshDistr" runat="server" Width="500px" Height="500px"  >
        <Series>
            <asp:Series Name="Series1" ChartType="Point" MarkerSize="7"  MarkerBorderWidth="1" MarkerBorderColor="Black">
            </asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
                <AxisY IsLabelAutoFit="False" TitleFont="Microsoft Sans Serif, 10pt, style=Bold">
                    
                    <MajorGrid  LineColor="LightGray" />
                    <MajorTickMark LineColor="Gray" />
                </AxisY>
                <AxisX IsLabelAutoFit="False" TitleFont="Microsoft Sans Serif, 10pt, style=Bold">
                    <MajorGrid  LineColor="LightGray" />
                    <MajorTickMark LineColor="Gray" />
                </AxisX>
            </asp:ChartArea>
        </ChartAreas>
           
    </asp:Chart>

</asp:Content>

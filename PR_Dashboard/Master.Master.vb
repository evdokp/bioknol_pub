﻿Public Class Master
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If HttpContext.Current.User.Identity.IsAuthenticated = True Then
            Dim _profile As New ProfileCommon
            _profile = _profile.GetProfile(HttpContext.Current.User.Identity.Name)
            Me.ASPxPopupMenu1.Items(0).NavigateUrl = "~\Account\ViewPerson.aspx?PersonID=" & _profile.Person_ID
            Me.ASPxPopupMenu1.Items.FindByText("History search").NavigateUrl = "~\Account\Search.aspx?PersonID=" & _profile.Person_ID
            Dim pers As New BLL.Person(_profile.Person_ID)
            If pers.UnreadMessagesCount = 0 Then
                Me.ASPxPopupMenu1.Items.FindByName("miMessages").Text = "Messages"
            Else
                Me.ASPxPopupMenu1.Items.FindByName("miMessages").Text = "Messages (" & pers.UnreadMessagesCount & ")"
            End If
        Else
            Me.ASPxPopupMenu1.Visible = False


        End If
    End Sub

End Class
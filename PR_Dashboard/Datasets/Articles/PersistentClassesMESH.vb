Imports System
Imports DevExpress.Xpo
Imports DevExpress.Data.Filtering
Namespace bio_data

	Public Class pmMeSH
		Inherits XPLiteObject
		Dim fMeSH_ID As Integer
		<Key(true)> _
		Public Property MeSH_ID() As Integer
			Get
				Return fMeSH_ID
			End Get
			Set(ByVal value As Integer)
				SetPropertyValue(Of Integer)("MeSH_ID", fMeSH_ID, value)
			End Set
		End Property
		Dim fHeading As String
		<Size(1000)> _
		Public Property Heading() As String
			Get
				Return fHeading
			End Get
			Set(ByVal value As String)
				SetPropertyValue(Of String)("Heading", fHeading, value)
			End Set
		End Property
		Public Sub New(ByVal session As Session)
			MyBase.New(session)
		End Sub
		Public Sub New()
			MyBase.New(Session.DefaultSession)
		End Sub
		Public Overrides Sub AfterConstruction()
			MyBase.AfterConstruction()
		End Sub
	End Class

	<NonPersistent()> _
	Public Class GetDistinctMESHByPersonID
		Inherits PersistentBase
		Dim fHeading As String
		Public Property Heading() As String
			Get
				Return fHeading
			End Get
			Set(ByVal value As String)
				SetPropertyValue(Of String)("Heading", fHeading, value)
			End Set
		End Property
		Public Sub New(ByVal session As Session)
			MyBase.New(session)
		End Sub
		Public Sub New()
			MyBase.New(Session.DefaultSession)
		End Sub
		Public Overrides Sub AfterConstruction()
			MyBase.AfterConstruction()
		End Sub
	End Class
	Public Module Bio_dataSprocHelper
		Public Function ExecGetDistinctMESHByPersonID(ByVal session As Session, ByVal personID As Integer) As DevExpress.Xpo.DB.SelectedData
			Return session.ExecuteSproc("GetDistinctMESHByPersonID", New OperandValue(personID))
		End Function

		Public Function ExecGetDistinctMESHByPersonIDIntoObjects(ByVal session As Session, ByVal personID As Integer) As System.Collections.Generic.ICollection(Of GetDistinctMESHByPersonID)
			Return session.GetObjectsFromSproc(Of GetDistinctMESHByPersonID)("GetDistinctMESHByPersonID", New OperandValue(personID))
		End Function

		Public Function ExecGetDistinctMESHByPersonIDIntoDataView(ByVal session As Session, ByVal personID As Integer) As XPDataView
			Dim sprocData As DevExpress.Xpo.DB.SelectedData = session.ExecuteSproc("GetDistinctMESHByPersonID", New OperandValue(personID))
			Return New XPDataView(session.Dictionary, session.GetClassInfo(GetType(GetDistinctMESHByPersonID)), sprocData)
		End Function
		Public Sub ExecGetDistinctMESHByPersonIDIntoDataView(ByVal dataView As XPDataView, ByVal session As Session, ByVal personID As Integer)
			Dim sprocData As DevExpress.Xpo.DB.SelectedData = session.ExecuteSproc("GetDistinctMESHByPersonID", New OperandValue(personID))
			dataView.PopulateProperties(session.GetClassInfo(GetType(GetDistinctMESHByPersonID)))
			dataView.LoadData(sprocData)
		End Sub
	End Module


End Namespace

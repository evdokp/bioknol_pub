﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RequestArticle.aspx.vb" Inherits="BioWS.RequestArticle" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
          <br />
        <asp:Literal ID="Literal1" runat="server">URL:</asp:Literal><br />
        <asp:TextBox ID="txt_URL" runat="server"></asp:TextBox>
        <br /><br />
        <asp:Literal ID="Literal2" runat="server">PlugInID:</asp:Literal><br />
        <asp:TextBox ID="txt_PlugInID" runat="server"></asp:TextBox>
        <br /><br />
        <asp:Literal ID="Literal3" runat="server">PersonID:</asp:Literal><br />
        <asp:TextBox ID="txt_PersonID" runat="server"></asp:TextBox>
        <br /><br />

        <asp:Button ID="btn_test" runat="server" Text="Test" />
        <br /><br />
        <asp:Literal ID="lit_result" runat="server"></asp:Literal><br />
        
        
    </div>
    </form>
</body>
</html>

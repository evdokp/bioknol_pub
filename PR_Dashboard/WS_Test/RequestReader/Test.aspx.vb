﻿Imports System.Net
Imports System.IO
Public Class Test
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Dim res As Boolean
        Dim wrq As WebRequest = WebRequest.Create("http://ws.bioknowledgecenter.ru/WS_Test/RequestReader/ReadRequest.ashx")
        wrq.Method = "POST"
        Dim postData As String = "PubmedIDs=10001010;1201203;1231312"
        wrq.ContentType = "Application/x-www-form-urlencoded"
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
        wrq.ContentLength = byteArray.Length
        Dim dataStream As Stream = wrq.GetRequestStream()
        dataStream.Write(byteArray, 0, byteArray.Length)
        dataStream.Close()
        Dim response As WebResponse = wrq.GetResponse()
        Console.WriteLine(CType(response, HttpWebResponse).StatusDescription)
        dataStream = response.GetResponseStream()
        Dim reader As New StreamReader(dataStream)
        Dim responseFromServer As String = reader.ReadToEnd()
        Dim respInteger As Integer
        respInteger = CInt(responseFromServer)

        If respInteger = 0 Then
            res = False
        Else
            res = True
        End If

        reader.Close()
        dataStream.Close()
        response.Close()


    End Sub
End Class
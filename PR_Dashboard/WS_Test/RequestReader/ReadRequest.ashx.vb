﻿Imports System.Web
Imports System.Web.Services
Imports System.IO


Public Class ReadRequest
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim s As String
        context.Response.ContentType = "text/plain"
        Dim enc = System.Text.Encoding.GetEncoding(1251)
        Using sr As StreamReader = New StreamReader(context.Request.InputStream, enc)
            s = sr.ReadToEnd
            Dim path As String = HttpContext.Current.Server.MapPath("~/WS_Test/RequestReader/Request.txt")
            System.IO.File.WriteAllText(path, s)
        End Using
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
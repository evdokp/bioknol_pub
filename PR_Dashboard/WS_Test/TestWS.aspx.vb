﻿Imports System.Web
Imports System.Net
Imports System.IO

Public Class TestWS
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Shared Function Post() As String
        Dim strDataToPost As String = ""

        Dim fn As String = "~/RequestPlugin.txt"
        strDataToPost = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(fn))

        Dim enc = System.Text.Encoding.UTF8
        'Dim enc = System.Text.UTF8Encoding.Unicode

        Dim myWebRequest As WebRequest
        Dim myRequestStream As Stream
        Dim myStreamWriter As StreamWriter

        Dim myWebResponse As WebResponse
        Dim myResponseStream As Stream
        Dim myStreamReader As StreamReader


        myWebRequest = WebRequest.Create("http://localhost:3042/ws/Authorize_WS.asmx")
        '        myWebRequest = WebRequest.Create("http://localhost:4820/VideoZnanie/H/ReadRequest.ashx")


        ' Set the method to "POST" and the content type so the
        ' server knows to expect form data in the body of the
        ' request.
        With myWebRequest
            .Method = "POST"
            .ContentType = "text/xml"
        End With


        ' write our data to the Stream using the StreamWriter.
        myRequestStream = myWebRequest.GetRequestStream()
        myStreamWriter = New StreamWriter(myRequestStream, enc)
        myStreamWriter.Write(strDataToPost)
        myStreamWriter.Flush()
        myStreamWriter.Close()
        myRequestStream.Close()

        ' Get the response from the remote server.
        myWebResponse = myWebRequest.GetResponse()

        ' Get the server's response status?

        ' Just like when we sent the data, we'll get a reference
        ' to the response Stream, connect a StreamReader to the
        ' Stream and use the reader to actually read the reply.
        myResponseStream = myWebResponse.GetResponseStream()
        myStreamReader = New StreamReader(myResponseStream)
        Dim res As String = myStreamReader.ReadToEnd()
        myStreamReader.Close()
        myResponseStream.Close()

        ' Close the WebResponse
        myWebResponse.Close()
        Return res
    End Function


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Dim s As String
        s = Post()
        Me.Literal1.Text = s
    End Sub
End Class
﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GetPluginID.aspx.vb" Inherits="BioWS.GetPluginID" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Literal ID="Literal1" runat="server">MAC or other unique identifier:</asp:Literal><br />
        <asp:TextBox ID="txt_MAC" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="btn_test" runat="server" Text="Test" />
            <br />
            <br />
            <asp:Literal
                ID="lit_result" runat="server"></asp:Literal>
    </div>
    </form>
</body>
</html>

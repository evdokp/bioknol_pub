﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Authorize.aspx.vb" Inherits="BioWS.Authorize" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
        <br />
        <asp:Literal ID="Literal1" runat="server">UserName:</asp:Literal><br />
        <asp:TextBox ID="txt_UserName" runat="server"></asp:TextBox>
        <br /><br />
        <asp:Literal ID="Literal2" runat="server">Password:</asp:Literal><br />
        <asp:TextBox ID="txt_Password" runat="server"></asp:TextBox>
        <br /><br />
        <asp:Literal ID="Literal3" runat="server">Login (1) or Register (2):</asp:Literal><br />
        <asp:TextBox ID="txt_LorR" runat="server"></asp:TextBox>
        <br /><br />

        <asp:Button ID="btn_test" runat="server" Text="Test" />
        <br /><br />
        <asp:Literal ID="lit_result" runat="server"></asp:Literal><br />
        
        
    </div>
    </form>
</body>
</html>

﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LogError.aspx.vb" Inherits="BioWS.LogError" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
          <br />
        <asp:Literal ID="Literal1" runat="server">Error:</asp:Literal><br />
        <asp:TextBox ID="txt_Error" runat="server"></asp:TextBox>
        
              <br />
        <asp:Literal ID="Literal2" runat="server">Person_ID:</asp:Literal><br />
        <asp:TextBox ID="txt_PersonID" runat="server"></asp:TextBox>
        
              <br />
        <asp:Literal ID="Literal3" runat="server">Plugin_ID:</asp:Literal><br />
        <asp:TextBox ID="txt_PluginID" runat="server"></asp:TextBox>
        
             <br /><br />

        <asp:Button ID="btn_test" runat="server" Text="Test" />
        <br /><br />
        <asp:Literal ID="lit_result" runat="server"></asp:Literal><br />
        
        
    </div>
    </form>
</body>
</html>

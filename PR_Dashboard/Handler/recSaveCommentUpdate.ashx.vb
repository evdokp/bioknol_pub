﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions
Imports BioWS.BLL.Extensions.Web

Public Class recSaveCommentUpdate
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim recid As Integer = CInt(context.Request("recid"))
        Dim comment As String = context.Request("comment")

        Dim db = New FeedDataClassesDataContext
        db.rec_UpdateWithComment(recid, comment, DateTime.Now)

        context.Response.WritePlainText(String.Empty)
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
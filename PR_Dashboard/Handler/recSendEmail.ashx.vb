﻿Imports System.Web
Imports System.Web.Services

Public Class recSendEmail
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim response As String
        Dim _pr As New ProfileCommon
        _pr = _pr.GetProfile(HttpContext.Current.User.Identity.Name)
        Dim email As String = context.Request("email")
        Dim recid As String = CInt(context.Request("recid"))
        Dim comment As String = context.Request("comment")

        Dim personfrom As New BLL.Person(_pr.Person_ID)
        Dim recta As New ArticlesDataSetTableAdapters.ArticleRecommendationsTableAdapter
        Dim recr = recta.GetDataByID(recid)(0)
        Dim title As String
        If Not recr.IsPubmedIDNull Then
            Dim pma As New BLL.PubMed.PubMedArticle(recr.PubmedID)
            title = pma.Title
        Else
            title = recr.Title
        End If



        Dim vBody As String = BLL.MailSender.ReadFile.ReadFile(HttpContext.Current.Server, "Recommendation.htm")
        vBody = vBody.Replace("<%From%>", personfrom.PageHeader)
        vBody = vBody.Replace("<%ArticleTitle%>", recr.Title)
        vBody = vBody.Replace("<%ArticleURL%>", recr.URL)
        vBody = vBody.Replace("<%Comment%>", comment)

        Try
            BLL.MailSender.SendMailMessage("noreply@bioknowledgecenter.ru",
            "BioKnol mail service",
            email.Trim,
            String.Empty,
            String.Empty,
            "Article recommendation from BioKnol user",
            vBody)

            response = "success"
        Catch ex As Exception
            response = "failure"
        End Try

        context.Response.ContentType = "text/plain"
        context.Response.Write(response)
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
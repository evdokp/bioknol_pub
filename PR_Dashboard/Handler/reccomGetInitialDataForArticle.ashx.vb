﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions
Imports BioWS.BLL.Extensions.Web
Imports BioWS.BLL


Public Class reccomGetInitialDataForArticle
    Implements System.Web.IHttpHandler







    Public Class ComRecContainer
        Public Comments As List(Of ArticleComment)
        Public Recommendations As List(Of ArticleRecommendation)
    End Class
    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim aid As New ArticleID(context.Request("articleid"))

        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)



        Dim db = New FeedDataClassesDataContext

        Dim res = New ComRecContainer()
        res.Comments = (From cc In db.com_GetComments(aid.IDstr)
                        Select New ArticleComment() With {.Author = cc.DisplayName,
                                                          .AuthorID = cc.CommentAuthorID,
                                                          .Comment = cc.Comment,
                                                          .TimeStamp = cc.Timestamp.ToString("dd.MM.yyyy HH:mm"),
                                                          .AvatarURL = IIf(cc.Image50.Length < 12, BLL.Common.GetGravatar(cc.UserName, 50), "https://bioknolstorage.blob.core.windows.net/userimages/" & cc.AvatarGUID & "_50." & cc.AvatarExtension)
                            }).ToList()
        res.Recommendations = (From rr In db.rec_GetRecommendations(aid.IDstr, p.Person_ID)
                               Select New ArticleRecommendation() With {.Author = rr.DisplayName,
                                             .AuthorID = rr.PersonID,
                                             .TimeStamp = rr.Timestamp.ToString("dd.MM.yyyy HH:mm"),
                                             .PersonallyAddressed = rr.PersonallyAddressed,
                                            .IsViewerPerson = rr.IsViewerPerson,
                                                                         .SubRecsCount = rr.RecsCount,
                                          .AvatarURL = IIf(rr.Image50.Length < 12, BLL.Common.GetGravatar(rr.UserName, 50), "https://bioknolstorage.blob.core.windows.net/userimages/" & rr.AvatarGUID & "_50." & rr.AvatarExtension)
                                    }).ToList()


        context.Response.WriteJSON(res)
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
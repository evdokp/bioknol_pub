﻿Imports System.Web
Imports System.Web.Services

Public Class RequestPubmedArticle
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim pubmedID As Integer = CInt(context.Request("PubmedID"))
        Dim _pr As New ProfileCommon
        _pr = _pr.GetProfile(HttpContext.Current.User.Identity.Name)
        Dim pmurl = "http://www.ncbi.nlm.nih.gov/pubmed/" & pubmedID
        Dim ta As New ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter
        ta.Insert(Now, pmurl, pubmedID, 0, _pr.Person_ID, String.Empty, String.Empty, False, String.Empty)
        context.Response.ContentType = "text/plain"
        context.Response.Write(String.Empty)
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
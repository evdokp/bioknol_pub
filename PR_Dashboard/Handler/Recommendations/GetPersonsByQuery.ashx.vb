﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions


Public Class GetPersonsByQuery
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim _pr As New ProfileCommon
        _pr = _pr.GetProfile(HttpContext.Current.User.Identity.Name)

        Dim query As String = context.Request("query")

        Dim ta As New PersonsDataSetTableAdapters.v_PersonsTableAdapter
        Dim t = ta.GetDataByQueryMinusSelf(String.Format("%{0}%", query), _pr.Person_ID)


        Dim recent = (From person In t
                      Select id = person.Person_ID, text = person.DisplayName,
                                        img = IIf(person.Image50.Length < 12, BLL.Common.GetGravatar(person.UserName, 50), "https://bioknolstorage.blob.core.windows.net/userimages/" & person.AvatarGUID & "_50." & person.AvatarExtension)
                                                  ).ToList()

        context.Response.WriteJSON(recent)

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
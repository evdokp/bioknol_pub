﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions


Public Class AlreadyRecommended
    Implements System.Web.IHttpHandler

    Private Class PersonsForRecommendation
        Public Already As Object
        Public Recent As Object
        Public Followers As Object
        Public Followed As Object
    End Class

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim _pr As New ProfileCommon
        _pr = _pr.GetProfile(HttpContext.Current.User.Identity.Name)
        Dim recid As Integer = CInt(context.Request("recid"))


        Dim personsta As New PersonsDataSetTableAdapters.v_PersonsTableAdapter
        Dim personst = personsta.GetAlreadyRecommendedToByRecID(recid)

        Dim emailsta As New ArticlesDataSetTableAdapters.ArticleRecommendationsToEmailsTableAdapter
        Dim emailst = emailsta.GetDataByRecID(recid)


        Dim recentt = personsta.GetRecentByRecommendatorPersonIDRecIDwoAlready(_pr.Person_ID, recid)
        Dim followerst = personsta.GetFollowersByFollowedPersonID(_pr.Person_ID)
        Dim followedt = personsta.GetFollowedByFollowerID(_pr.Person_ID)


        Dim container As New PersonsForRecommendation()


        Dim followers = (From person In followerst
                                 Select PersonID = person.Person_ID.ToString, person.DisplayName).ToList()

        Dim followed = (From person In followedt
                                 Select PersonID = person.Person_ID.ToString, person.DisplayName).ToList()


        Dim recent = (From person In recentt
                                 Select PersonID = person.Person_ID.ToString, person.DisplayName).ToList()

        container.Recent = recent.ToList()
        container.Followers = followers.ToList()
        container.Followed = followed.ToList()


        Dim users = From person In personst
                                 Select PersonID = person.Person_ID.ToString(), DisplayName = person.DisplayName
        Dim emails = From email In emailst
                        Select PersonID = email.Email, DisplayName = email.Email

        container.Already = users.Union(emails).ToList()

        context.Response.ContentType = "text/plain"
        context.Response.Write(container.ToJSON())

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
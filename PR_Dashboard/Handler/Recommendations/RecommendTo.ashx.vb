﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions

Public Class RecommendTo
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim _pr As New ProfileCommon
        _pr = _pr.GetProfile(HttpContext.Current.User.Identity.Name)
        Dim recid As Integer = CInt(context.Request("recid"))
        Dim idsStrArr As String = context.Request("ids")
        Dim comment As String = context.Request("comment")
        Dim emails As New List(Of String)

        '! handling array of IDs
        For Each strid In idsStrArr.Split(",")
            Dim personid As Integer
            If Integer.TryParse(strid, personid) Then
                '! this is BioKnol user
                RecommendArticle(personid, recid)
            Else
                '! this is email, probably
                emails.Add(strid)
            End If
        Next

        '!recommend to list of emails
        Dim reccomentation As New BLL.Emails.Recommendation(_pr.Person_ID, comment, recid)
        reccomentation.SendToEmailsList(emails)


        '!handling comment
        If comment.Length > 0 Then
            Dim db = New FeedDataClassesDataContext
            db.rec_AddCommentByRecID(recid, comment, Now)
        End If

        context.Response.WritePlainText(String.Empty)
    End Sub


    Public Sub RecommendArticle(ByVal PersonToID As Integer, ByVal RecID As Integer)
        Dim ta As New ArticlesDataSetTableAdapters.ArticleRecommendationsToPersonsTableAdapter
        Dim t = ta.GetDataByTwoIDs(RecID, PersonToID)
        If t.Count() = 0 Then
            ta.Insert(RecID, PersonToID, False, DateTime.Now)
        Else
            Dim row = t(0)
            row.BeginEdit()
            row.TimeStamp = Now
            row.EndEdit()
            ta.Update(row)
        End If
    End Sub
    

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
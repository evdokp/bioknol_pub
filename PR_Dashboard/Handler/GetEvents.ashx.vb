﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions


Public Class GetEvents
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim personID As Integer = CInt(context.Request("PersonID"))


        Dim ta As New ViewsLogDataSetTableAdapters.v_timeline_pubmedviewsTableAdapter
        Dim t = ta.GetDataByPersonID(personID)


        Dim tt = From myevent In t
                Select start = myevent.ActionTime.AddHours(4).ToString("MMM dd yyyy HH:mm:ss ") & "GMT +0400", durationEvent = "false", title = myevent.Title.SubstringToLength(100)

        Dim cont As New EventDataContainer
        'cont.dateTimeFormat = "iso8601"
        cont.events = tt.ToList()
        cont.wikiURL = "http://simile.mit.edu/shelf/"
        cont.wikiSection = "Simile Cubism Timeline"
        Dim res As String = cont.ToJSON
        res = res.Replace("""", "'")
        context.Response.ContentType = "text/plain"
        context.Response.Write(res)



        'context.Response.WriteFile("~\events.js")


    End Sub


    Public Structure EventDataContainer
        'Dim dateTimeFormat As String
        Dim wikiURL As String
        Dim wikiSection As String
        Dim events As Object
    End Structure


    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
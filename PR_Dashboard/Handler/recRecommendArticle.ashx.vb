﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions
Imports BioWS.BLL.Extensions.Web
Imports BioWS.BLL

Public Class recRecommendArticle
    Implements System.Web.IHttpHandler

    Private _CurProfilePersonID As Integer?
    Public ReadOnly Property CurProfilePersonID As Integer
        Get
            If _CurProfilePersonID Is Nothing Then
                Dim _pr As New ProfileCommon
                _pr = _pr.GetProfile(HttpContext.Current.User.Identity.Name)
                _CurProfilePersonID = _pr.Person_ID
            End If
            Return _CurProfilePersonID
        End Get
    End Property
    
    Public AID As ArticleID

    Public Class AddRecommendationContainer
        Public RecID As Integer
        Public IsNew As Integer
    End Class

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        AID = New ArticleID(context.Request("articleid"))

        Dim result As New AddRecommendationContainer()

        Dim rect = GetRecommendationsTable()
        If rect.Rows.Count > 0 Then
            'get from row
            result.RecID = rect(0).ID
            result.IsNew = 0
        Else
            result.RecID = InsertRecommendationRow()
            result.IsNew = 1
        End If

        'return recommendation ID
        context.Response.WriteJSON(result)

    End Sub


    Public Function GetRecommendationsTable() As ArticlesDataSet.ArticleRecommendationsDataTable
        Dim recta As New ArticlesDataSetTableAdapters.ArticleRecommendationsTableAdapter
        Dim rect As ArticlesDataSet.ArticleRecommendationsDataTable
        If AID.IDType = ArticleIDTypes.DOI Then
            rect = recta.GetDataByDOIPersonID(CurProfilePersonID, AID.IDstr)
        Else
            rect = recta.GetDataByPubmedIDPersonID(AID.IDint, CurProfilePersonID)
        End If
        Return rect
    End Function
    Public Function InsertRecommendationRow() As Integer
        Dim recta As New ArticlesDataSetTableAdapters.ArticleRecommendationsTableAdapter
        Dim recID As Integer?
        If AID.IDType = ArticleIDTypes.DOI Then
            Dim di As New DOIItem(AID.IDstr)
            recta.InsertArticleRecommendation(Now, CurProfilePersonID, di.URL, Nothing, AID.IDstr, di.GetTitle(), recID)
        Else
            Dim pma As New PubMed.PubMedArticle(AID.IDint)
            recta.InsertArticleRecommendation(Now, CurProfilePersonID, pma.PubmedURL, pma.Pubmed_ID, String.Empty, pma.Title, recID)
        End If

        Return recID
    End Function




    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
﻿Imports System.Web
Imports System.Web.Services

Public Class GetWidgets
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim pubmedID As Integer = CInt(context.Request("PubmedID"))

        Dim _pr As New ProfileCommon
        _pr = _pr.GetProfile(HttpContext.Current.User.Identity.Name)

        Dim widgetsTA As New WidgetsDataSetTableAdapters.WidgetsTableAdapter
        Dim widgetsTBL = widgetsTA.GetDataByPersonID_ON(_pr.Person_ID)

        Dim res As String = ""
        Dim HasWidgets As Boolean = widgetsTBL.Count > 0
        If HasWidgets Then

            For i = 0 To widgetsTBL.Count - 1
                res = res & "<li>"
                Dim r = widgetsTBL.Item(i)
                Dim img As String

                If BLL.Widgets.IsAdditionalInfoAvailableByWidget(pubmedID, r.Widget_ID) > 0 Then
                    img = "<img src=""..\Images\lightbulb_on.png""/>"
                Else
                    img = "<img src=""..\Images\lightbulb.png""/>"
                End If

                res = res & String.Format("<li><a target=""_blank"" href=""../ViewInfoPMWidget.aspx?PersonID={0}&WidgetID={1}&Pubmed_ID={2}"">{4}&nbsp;&nbsp;{3}</a></li>", _pr.Person_ID, r.Widget_ID, pubmedID, r.Title, img)

                

                res = res & "</li>"
            Next

        Else
            res = "<li><div style=""padding:5px;"">Turn on at least one widget</div></li>"
        End If

        context.Response.ContentType = "text/plain"
        context.Response.Write(res)


    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
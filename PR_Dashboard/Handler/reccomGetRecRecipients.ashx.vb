﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions
Imports BioWS.BLL.Extensions.Web
Imports BioWS.BLL

Public Class reccomGetRecRecipients
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim aid As New ArticleID(context.Request("articleid"))
        Dim recAuthorID As Integer = CInt(context.Request("authorid"))

        Dim db = New FeedDataClassesDataContext
        Dim profile As New ProfileCommon
        profile = profile.GetProfile(HttpContext.Current.User.Identity.Name)


        Dim res = (From rr In db.rec_GetRecommendationRecipients(aid.IDstr, recAuthorID)
                   Select New ArticleRecommendation() With {.Author = rr.DisplayName,
                                          .AuthorID = rr.Person_ID,
                                          .TimeStamp = "",
                                          .PersonallyAddressed = 0,
                                         .IsViewerPerson = IIf(rr.Person_ID = profile.Person_ID, 1, 0),
                                                                      .SubRecsCount = 0,
                                                                      .AvatarURL = IIf(rr.Image50.Length < 12, BLL.Common.GetGravatar(rr.UserName, 50), "https://bioknolstorage.blob.core.windows.net/userimages/" & rr.AvatarGUID & "_50." & rr.AvatarExtension)
                                 }).ToList()

        context.Response.WriteJSON(res)


    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
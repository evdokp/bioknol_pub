﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions

Public Class DeleteAvatar
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)
        Dim pers As New BLL.Person(p.Person_ID)
        pers.DeleteImageAzure()
        context.Response.WritePlainText(pers.Image200)
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions

Public Class hovercardGetPerson
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim personid As Integer = context.Request("personid").ToInt32()

        Dim ta As New DataSetMeshFreqTableAdapters.v_PersonsMeshFrequenciesTableAdapter
        Dim tt = ta.GetTopNByPersonID(10, personid)

        Dim res = From mesh In tt
                  Select mesh.MeSH_ID, mesh.Heading

        context.Response.WriteJSON(res)

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
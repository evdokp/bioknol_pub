﻿Imports System.Web
Imports System.Web.Services

Public Class ToggleFollow
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim objectid As Integer = context.Request("objectid")
        Dim isgroup As Integer = context.Request("isgroup")

        Dim _profile As New ProfileCommon
        _profile = _profile.GetProfile(HttpContext.Current.User.Identity.Name)

        If isgroup = 0 Then
            Dim followta As New PersonsDataSetTableAdapters.FollowingsTableAdapter
            Dim followt = followta.GetDataByPersonFollowedPerson(_profile.Person_ID, objectid)
            If followt.Count > 0 Then
                followta.Delete(_profile.Person_ID, objectid, 0)
                Dim feedaction As New BLL.Feed.FeedActionPersonUnFollow(objectid)
                feedaction.Save(_profile.Person_ID)

            Else
                followta.Insert(_profile.Person_ID, objectid, 0)
                Dim feedaction As New BLL.Feed.FeedActionPersonFollow(objectid)
                feedaction.Save(_profile.Person_ID)
            End If
        End If

        context.Response.ContentType = "text/plain"
        context.Response.Write(String.Empty)
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
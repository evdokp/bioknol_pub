﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions

Public Class recGetPersonsByLike
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim _pr As New ProfileCommon
        _pr = _pr.GetProfile(HttpContext.Current.User.Identity.Name)
        Dim recid As String = CInt(context.Request("recid"))
        Dim query As String = context.Request("query")

        Dim ta As New PersonsDataSetTableAdapters.v_PersonsTableAdapter
        Dim t = ta.GetDataByQueryByRecIDwoAlready(String.Format("%{0}%", query), _pr.Person_ID, recid)

        Dim recent = (From person In t
                                  Select person.Person_ID, person.DisplayName).ToList()

        context.Response.ContentType = "text/plain"
        context.Response.Write(recent.ToJSON())
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
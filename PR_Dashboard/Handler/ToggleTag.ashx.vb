﻿Imports System.Web
Imports System.Web.Services

Public Class ToggleTag
    Implements System.Web.IHttpHandler

    Private Enum TagTypes
        Favorite = 1
        Trash = 2
    End Enum


    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Dim articleid As Object = context.Request("articleid")
        Dim isdoi As Boolean = CBool(context.Request("isdoi"))
        Dim tagtype As TagTypes = CType(CInt(context.Request("tag")), TagTypes)
        Dim _pr As New ProfileCommon
        _pr = _pr.GetProfile(HttpContext.Current.User.Identity.Name)

        Dim ta As New DataSetTagsTableAdapters.LinksTagsToArticleIDsTableAdapter

        Dim response As String

        If isdoi Then
            Dim doi As String = CStr(articleid)
            If ta.GetCountByDOI(tagtype, doi, _pr.Person_ID) = 0 Then
                ta.InsertDOITag(tagtype, doi, _pr.Person_ID)
                response = "inserted"
            Else
                ta.DeleteDOITag(tagtype, _pr.Person_ID, doi)
                response = "deleted"
            End If
        Else
            Dim pmid As Integer = CInt(articleid)
            If ta.GetCountByPubmedID(tagtype, pmid, _pr.Person_ID) = 0 Then
                ta.InsertPubmedTag(tagtype, pmid, _pr.Person_ID)
                response = "inserted"
            Else
                ta.DeletePubmedTag(tagtype, _pr.Person_ID, pmid)
                response = "deleted"
            End If
        End If


        context.Response.ContentType = "text/plain"
        context.Response.Write(response)

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
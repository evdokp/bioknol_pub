﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions


Public Class recGetPersonsAlready
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim recid As Integer = CInt(context.Request("recid"))


        Dim ta As New PersonsDataSetTableAdapters.v_PersonsTableAdapter
        Dim t = ta.GetAlreadyRecommendedToByRecID(recid)

        Dim recent = (From person In t
                                 Select person.Person_ID, person.DisplayName).ToList()

        context.Response.ContentType = "text/plain"
        context.Response.Write(recent.ToJSON())
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
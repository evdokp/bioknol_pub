﻿Imports System.Web
Imports System.Web.Services

Public Class GetFulltext
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim pubmedID As Integer = CInt(context.Request("PubmedID"))
        Dim _pr As New ProfileCommon
        _pr = _pr.GetProfile(HttpContext.Current.User.Identity.Name)


        Dim s As String = ""
        Dim pm As New BLL.PubMed.PubMedArticle(pubmedID)
        Dim offersTA As New ArticlesDataSetTableAdapters.ArticleOffersTableAdapter
        Dim offersTBL = offersTA.GetDataByPubmedIDwithFiles(pubmedID)
        If offersTBL.Count > 0 Then

            Dim offerRow = offersTBL.Item(0)
            Dim link As String = "http://ws.bioknowledgecenter.ru/Download.ashx?vFileName=" & HttpUtility.UrlEncode(offerRow.FileName) & "&vOriginalName=" & HttpUtility.UrlEncode(offerRow.OriginalName)
            s = "<li><a href=""" + link + """  target=""_blank"">Download</a></li>"

        Else
            Dim reqTA As New ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter
            If reqTA.GetCountByPersonIDPubmedID(pubmedID, _pr.Person_ID) > 0 Then
                'already requested
                s = s & "<li class=""ddlpadding"">Already requested</li>"
            Else
                'request allowed
                s = s & "<li><a class=""btnRequest"" pubmedID=""" + context.Request("PubmedID") + """>Request</a></li>"
            End If
            'and add upload opportunity
            Dim pmurl = "http://www.ncbi.nlm.nih.gov/pubmed/" & pubmedID
            Dim link As String = "http://ws.bioknowledgecenter.ru/UploadArticle.aspx?URL=" & HttpUtility.UrlEncode(pmurl) & "&PlugInID=0&UserID=" & _pr.Person_ID
            s = s & "<li><a href=""" + link + """ target=""_blank"">Upload</a></li>"

        End If



        context.Response.ContentType = "text/plain"
        context.Response.Write(s)

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions


Public Class recRecommendArticleToPerson
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Dim recid As Integer = CInt(context.Request("recid"))
        Dim persontoid As String = CInt(context.Request("persontoid"))


        RecommendArticle(persontoid, recid)
        context.Response.WritePlainText(String.Empty)
    End Sub

    Public Sub RecommendArticle(ByVal PersonToID As Integer, ByVal RecID As Integer)
        Dim recpersta As New ArticlesDataSetTableAdapters.ArticleRecommendationsToPersonsTableAdapter
        If recpersta.GetCountByTwoIDs(RecID, PersonToID) = 0 Then
            recpersta.Insert(RecID, PersonToID, False, DateTime.Now)
        End If
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
﻿Imports System.Web
Imports System.Web.Services

Public Class invSendEmail
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Dim response As String
        Dim _pr As New ProfileCommon
        _pr = _pr.GetProfile(HttpContext.Current.User.Identity.Name)
        Dim email As String = context.Request("email")

        Dim personfrom As New BLL.Person(_pr.Person_ID)
        


        Dim vBody As String = BLL.MailSender.ReadFile.ReadFile(HttpContext.Current.Server, "Invitation.htm")
        vBody = vBody.Replace("<%From%>", personfrom.PageHeader)
        
        Try
            BLL.MailSender.SendMailMessage("noreply@bioknowledgecenter.ru",
            "BioKnol",
            email.Trim,
            String.Empty,
            String.Empty,
            "Invitation to join BIOKNOL (a social network for PubMed/GoogleScholar readers)",
            vBody)

            response = "success"
        Catch ex As Exception
            response = "failure"
        End Try

        context.Response.ContentType = "text/plain"
        context.Response.Write(response)

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
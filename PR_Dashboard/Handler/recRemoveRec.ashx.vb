﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions

Public Class recRemoveRec
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim recid As String = CInt(context.Request("recid"))
        Dim persontoid As String = CInt(context.Request("persontoid"))


        Dim recta As New ArticlesDataSetTableAdapters.ArticleRecommendationsToPersonsTableAdapter
        recta.Delete(recid, persontoid)

        context.Response.ContentType = "text/plain"
        context.Response.Write(String.Empty)
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
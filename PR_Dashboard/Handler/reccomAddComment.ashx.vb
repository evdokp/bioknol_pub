﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions
Imports BioWS.BLL.Extensions.Web
Imports BioWS.BLL

Public Class reccomAddComment
    Implements System.Web.IHttpHandler




    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim aid As New ArticleID(context.Request("articleid"))
        Dim comment = context.Request("comment")



        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)
        Dim db = New FeedDataClassesDataContext

        Dim newComment As ArticleComment = Nothing


        If aid.IDType = ArticleIDTypes.Pubmed Then
            newComment = (From cc In db.com_AddComment(Nothing, aid.IDint, comment, p.Person_ID, DateTime.Now)
                          Select New ArticleComment() With {.Author = cc.DisplayName,
                                                                                                            .AuthorID = cc.CommentAuthorID,
                                                                                                            .Comment = cc.Comment,
                                                                                                            .TimeStamp = cc.Timestamp.ToString("dd.MM.yyyy HH:mm"),
                                                                                                            .AvatarURL = IIf(cc.Image50.Length < 12, BLL.Common.GetGravatar(cc.UserName, 50), "https://bioknolstorage.blob.core.windows.net/userimages/" & cc.AvatarGUID & "_50." & cc.AvatarExtension)
                                                                               }).FirstOrDefault()



        ElseIf aid.IDType = ArticleIDTypes.DOI Then
            newComment = (From cc In db.com_AddComment(aid.IDstr, Nothing, comment, p.Person_ID, DateTime.Now)
                          Select New ArticleComment() With {.Author = cc.DisplayName,
                                                                                                          .AuthorID = cc.CommentAuthorID,
                                                                                                          .Comment = cc.Comment,
                                                                                                          .TimeStamp = cc.Timestamp.ToString("dd.MM.yyyy HH:mm"),
                                                                                                            .AvatarURL = IIf(cc.Image50.Length < 12, BLL.Common.GetGravatar(cc.UserName, 50), "https://bioknolstorage.blob.core.windows.net/userimages/" & cc.AvatarGUID & "_50." & cc.AvatarExtension)
                              }).FirstOrDefault()



        End If


        context.Response.WriteJSON(newComment)

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
﻿Imports System.Web
Imports System.Web.Services
Imports BioWS.BLL.Extensions


Public Class GetAbstract
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim pubmedID As Integer = context.Request("PubmedID").ToInt32()


        Dim pm As New BLL.PubMed.PubMedArticle(pubmedID)

        Dim _pr As New ProfileCommon
        _pr = _pr.GetProfile(HttpContext.Current.User.Identity.Name)

        '! registering view
        Dim ta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
        ta.InsertViewFromFeed(_pr.Person_ID, BLL.Common.GetIP(), pm.Pubmed_ID)


        context.Response.WritePlainText(pm.Abstract)


    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
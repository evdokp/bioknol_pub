﻿Imports DevExpress.Xpo

Public Class ReadingProfileGroup
    Inherits System.Web.UI.Page

    Dim session1 As Session

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'session1 = New Session()
        'XpoDataSource1.Session = session1


        If Page.IsPostBack = False Then
            
            Dim gr As New BLL.Group(Request.QueryString("GroupID"))
            CType(Master.Master.FindControl("lit_pagetitle"), Literal).Text = gr.Title
            CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "Reading profile"

            If String.IsNullOrEmpty(Request.QueryString("cond")) = False Then
                txtCondition.Text = HttpUtility.UrlDecode(Request.QueryString("cond"))
                DatabindArticles()
            End If

        End If





    End Sub

    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        MyBase.Render(writer)
        'Session1.Dispose()
    End Sub


    Private Sub DatabindArticles()
        Dim selJournals As List(Of Object) = grJournals.GridView.GetSelectedFieldValues("Journal_ID")
        Dim selAuthors As List(Of Object) = grAuthors.GridView.GetSelectedFieldValues("Author_ID")
        Dim selMESH As List(Of Object) = grMESH.GridView.GetSelectedFieldValues("MeSH_ID")
        Dim selPubYears As List(Of Object) = grPubYears.GridView.GetSelectedFieldValues("PubYear")

        Dim condition As String
        If cmbFilterType.Value = "all" Then
            condition = BLL.SearchHelper.Text_CONTAINS_Arg2_And(txtCondition.Text)
        Else
            condition = BLL.SearchHelper.Text_CONTAINS_Arg2_Or(txtCondition.Text)
        End If

        Dim strJournals As String = ObjListToString(selJournals)
        Dim strAuthors As String = ObjListToString(selAuthors)
        Dim strMESH As String = ObjListToString(selMESH)
        Dim strPubYears As String = ObjListToString(selPubYears)



        odsArticles.SelectParameters("Fullt").DefaultValue = condition
        odsArticles.SelectParameters("Journals").DefaultValue = strJournals
        odsArticles.SelectParameters("PubYears").DefaultValue = strPubYears
        odsArticles.SelectParameters("Authors").DefaultValue = strAuthors
        odsArticles.SelectParameters("MESH").DefaultValue = strMESH
        odsArticles.SelectParameters("ObjectType").DefaultValue = "G"

        odsArticles.DataBind()
    End Sub
    Protected Sub btnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click


        DatabindArticles()

    End Sub


    Private Function ObjListToString(objlist As List(Of Object)) As String
        Dim res As String = ""
        Dim maxindex As Integer = objlist.Count - 1
        For i = 0 To maxindex
            If i < maxindex Then
                res = String.Format("{0}{1},", res, objlist(i))
            Else
                res = res & objlist(i).ToString
            End If
        Next
        Return res
    End Function
End Class
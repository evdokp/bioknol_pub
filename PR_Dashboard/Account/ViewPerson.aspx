﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Person.Master"
    CodeBehind="ViewPerson.aspx.vb" Inherits="BioWS.ViewPerson" %>

<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCloudControl" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp1" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {



            $("#ddlStat").change(function () {
                ProcessStatisticsChange();
            });



            $("#h3groups").click(function () {
                $("#divGroups").toggle(300);
                var img = $($('#h3groups img'));
                ToggleImage(img);
            });

            $("#h3MESH i").click(function () {
                $("#divMESH").toggle(300);
                $("#h3MESH span b").toggle(100);
                var img = $($('#h3MESH img'));
                ToggleImage(img);
            });

            $("#h3activity").click(function () {
                $("#divActivity").toggle(300);
                var img = $($('#h3activity img'));
                ToggleImage(img);
            });




            $("#hStat i").click(function () {
                $("#divStat").toggle(300);
                $("#hStat .hunderline_block").toggle(100);
                var img = $($('#imgStat'));
                ToggleImage(img);
            });


            $("#hFollowings").click(function () {
                $("#divFollowings").toggle(300);
                var img = $($('#hFollowings img'));
                ToggleImage(img);
            });

        });

        function ToggleImage(img) {
            if (img.attr('src') == '../Images/minus.gif') {
                img.attr('src', '../Images/plus.gif');
            } else {
                img.attr('src', '../Images/minus.gif');
            }
        }

    

    </script>
    <h5 id="h3groups" style="margin-top: 7px;">
        <span>
            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/minus.gif" />
            Participation in groups </span>
    </h5>
    <div id="divGroups">
        <dx:ASPxTreeView ID="tv_groups" runat="server" ClientIDMode="AutoID" AllowSelectNode="false"
            NodeLinkMode="TextAndImage" Width="200px">
            <Images SpriteCssFilePath="~/App_Themes/Office2003Silver/{0}/sprite.css">
                <NodeLoadingPanel Url="~/App_Themes/Office2003Silver/Web/tvNodeLoading.gif">
                </NodeLoadingPanel>
                <LoadingPanel Url="~/App_Themes/Office2003Silver/Web/Loading.gif">
                </LoadingPanel>
            </Images>
            <Styles CssFilePath="~/App_Themes/Office2003Silver/{0}/styles.css" CssPostfix="Office2003Silver">
            </Styles>
        </dx:ASPxTreeView>
    </div>
    <h5 id="h3MESH">
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td valign="middle">
                    <span><i style="font-style: normal;">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/minus.gif" />
                        Area of expertise </i></span>
                </td>
                <td valign="middle">
                    <dx:ASPxTrackBar ID="ASPxTrackBar1" runat="server" Step="10" Height="20" BackColor="White"
                        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Position="10"
                        PositionStart="10" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                        <ClientSideEvents PositionChanged="function(s, e) {
    cpMESH.PerformCallback();
}" />
                    </dx:ASPxTrackBar>
                </td>
            </tr>
        </table>
    </h5>
    <div id="divMESH">
        <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" ClientInstanceName="cpMESH"
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
            <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
            </LoadingPanelImage>
            <LoadingPanelStyle ImageSpacing="5px">
            </LoadingPanelStyle>
            <PanelCollection>
                <dx:PanelContent runat="server" SupportsDisabledAttribute="True">
                    <asp:ObjectDataSource ID="ods_MESH" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="BioWS.ArticlesPubmedDataSetTableAdapters.MeSHDiff_OnePersonToAllTableAdapter">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ASPxTrackBar1" DefaultValue="10" Name="TopN" PropertyName="Position"
                                Type="Int32" />
                            <asp:QueryStringParameter Name="PersonID" QueryStringField="PersonID" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <dx:ASPxCloudControl ID="ASPxCloudControl1" runat="server" ClientIDMode="AutoID"
                        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" DataSourceID="ods_mesh"
                        MaxColor="#1B3F91" MinColor="#A9B9DD" TextField="Heading" ValueField="vDiff"
                        ValueColor="#1B3F91">
                        <RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                        </RankProperties>
                        <DisabledStyle ForeColor="#B1B1B8">
                        </DisabledStyle>
                    </dx:ASPxCloudControl>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
    </div>
    <h5 id="h3activity">
        <span>
            <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/minus.gif" />
            Activity </span>
    </h5>
    <div id="divActivity">
        <table width="100%">
            <tr>
                <td valign="top" class="td_label">
                    Registration date:
                </td>
                <td valign="top" class="td_field">
                    <asp:Literal ID="litRegTerm" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td valign="top" class="td_label">
                    Active days:
                </td>
                <td valign="top" class="td_field">
                    <div style="float: left;">
                        <asp:Literal ID="litActiveDays" runat="server"></asp:Literal>&nbsp;
                    </div>
                    <div style="float: left;">
                        <asp:Chart ID="Chart1" runat="server" Width="50px" Height="50px">
                            <Series>
                                <asp:Series ChartType="Pie" Name="Series1" CustomProperties="PieDrawingStyle=Concave, PieLabelStyle=Disabled, PieStartAngle=270">
                                </asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ChartArea1">
                                </asp:ChartArea>
                            </ChartAreas>
                        </asp:Chart>
                    </div>
                    <div class="clear">
                    </div>
                </td>
            </tr>
            <tr>
                <td valign="top" class="td_label">
                    Uses widgets:
                </td>
                <td valign="top" class="td_field">
                    <asp:GridView ID="gvWidgets" runat="server" AutoGenerateColumns="False" DataKeyNames="Widget_ID"
                        DataSourceID="odsWidgets" ShowHeader="False" GridLines="None">
                        <Columns>
                            <asp:HyperLinkField DataNavigateUrlFields="Widget_ID" DataNavigateUrlFormatString="~/Account/Widget/ViewWidget.aspx?WidgetID={0}"
                                DataTextField="Title" />
                        </Columns>
                        <EmptyDataTemplate>
                            <span style="font-style: italic; font-weight: normal;">No widgets </span>
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <asp:ObjectDataSource ID="odsWidgets" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetDataByPersonID_ON" TypeName="BioWS.WidgetsDataSetTableAdapters.WidgetsTableAdapter">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="Person_ID" QueryStringField="PersonID" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td valign="top" class="td_label">
                </td>
                <td valign="top" class="td_field">
                </td>
            </tr>
        </table>
    </div>
    <asp:Panel ID="pnlFL" runat="server">
        <h5 id="hFollowings">
            <span>
                <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/minus.gif" />
                Followings </span>
        </h5>
        <div id="divFollowings">
            <table width="100%">
                <tr>
                    <td valign="top" style="width: 50%">
                        <asp:SqlDataSource ID="sqlFollowsPersons" runat="server" ConnectionString="<%$ ConnectionStrings:bio_dataConnectionString %>"
                            SelectCommand="SELECT [FollowedPersonID], [FollowedUserName], [FollowedFirstName], [FollowedLastName], [FollowedPatronimic],[FollowedImage50] FROM [v_Followings] WHERE ([Person_ID] = @Person_ID) and (FollowedGroupID is null)">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="Person_ID" QueryStringField="PersonID" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <dx:ASPxGridView ID="gvFollows" runat="server" AutoGenerateColumns="False" Width="100%"
                            DataSourceID="sqlFollowsPersons" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                            CssPostfix="DevEx">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Follows:" ShowInCustomizationForm="True" VisibleIndex="0">
                                    <DataItemTemplate>
                                        <asp:Image ID="Image5" runat="server" ImageUrl='<%# "~\UserImages\" & Eval("FollowedImage50")  %>'
                                            Style="float: left; margin-right: 10px;" />
                                        <asp:HyperLink ID="hlPerson" runat="server" NavigateUrl='<%# "~\Account\ViewPerson.aspx?PersonID=" & Eval("FollowedPersonID") %>'
                                            Text='<%# Eval("FollowedUserName") %>' />
                                        <br />
                                        <span style="color: Gray">
                                            <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("FollowedLastName") & " " & Eval("FollowedFirstName")& " " & Eval("FollowedPatronimic") %>' />
                                        </span>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <SettingsPager PageSize="10">
                            </SettingsPager>
                            <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                                </LoadingPanelOnStatusBar>
                                <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                                </LoadingPanel>
                            </Images>
                            <ImagesFilterControl>
                                <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                                </LoadingPanel>
                            </ImagesFilterControl>
                            <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                </Header>
                                <LoadingPanel ImageSpacing="5px">
                                </LoadingPanel>
                            </Styles>
                            <StylesEditors ButtonEditCellSpacing="0">
                                <ProgressBar Height="21px">
                                </ProgressBar>
                            </StylesEditors>
                        </dx:ASPxGridView>
                        <asp:SqlDataSource ID="sqlFollowsGroups" runat="server" ConnectionString="<%$ ConnectionStrings:bio_dataConnectionString %>"
                            SelectCommand="SELECT [FollowedGroupID], [FollowedGroupTitle],[FollowedGroupImage50] FROM [v_Followings] WHERE ([Person_ID] = @Person_ID) and (FollowedPersonID is null)">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="Person_ID" QueryStringField="PersonID" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <div style="margin-top: 4px;">
                            <dx:ASPxGridView ID="gvFollowsGroups" runat="server" AutoGenerateColumns="False"
                                Width="100%" ClientInstanceName="clgvFollowsGroups" DataSourceID="sqlFollowsGroups"
                                CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="Follows groups:" ShowInCustomizationForm="True"
                                        VisibleIndex="0">
                                        <DataItemTemplate>
                                            <asp:Image ID="Image5" runat="server" ImageUrl='<%# "~\UserImages\" & Eval("FollowedGroupImage50")  %>'
                                                Style="float: left; margin-right: 10px;" />
                                            <asp:HyperLink ID="hlGroup" runat="server" NavigateUrl='<%# "~\Account\ViewGroup.aspx?GroupID=" & Eval("FollowedGroupID") %>'
                                                Text='<%# Eval("FollowedGroupTitle") %>' />
                                        </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsPager PageSize="10">
                                </SettingsPager>
                                <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                    <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                                    </LoadingPanelOnStatusBar>
                                    <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                                    </LoadingPanel>
                                </Images>
                                <ImagesFilterControl>
                                    <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                                    </LoadingPanel>
                                </ImagesFilterControl>
                                <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                    </Header>
                                    <LoadingPanel ImageSpacing="5px">
                                    </LoadingPanel>
                                </Styles>
                                <StylesEditors ButtonEditCellSpacing="0">
                                    <ProgressBar Height="21px">
                                    </ProgressBar>
                                </StylesEditors>
                            </dx:ASPxGridView>
                        </div>
                    </td>
                    <td valign="top" style="width: 50%; padding-left: 15px;">
                        <asp:SqlDataSource ID="sqlFollowedBy" runat="server" ConnectionString="<%$ ConnectionStrings:bio_dataConnectionString %>"
                            SelectCommand="SELECT [Person_ID], 
                                        [UserName], [FirstName], [LastName], [Patronimic],[Image50] FROM [v_Followings] WHERE ([FollowedPersonID] = @Person_ID) and (FollowedGroupID is null)">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="Person_ID" QueryStringField="PersonID" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <dx:ASPxGridView ID="gvFollowedBy" runat="server" AutoGenerateColumns="False" Width="100%"
                            DataSourceID="sqlFollowedBy" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                            CssPostfix="DevEx">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Is followed by:" ShowInCustomizationForm="True"
                                    VisibleIndex="0">
                                    <DataItemTemplate>
                                        <asp:Image ID="Image5" runat="server" ImageUrl='<%# "~\UserImages\" & Eval("Image50")  %>'
                                            Style="float: left; margin-right: 10px;" />
                                        <asp:HyperLink ID="hlPerson" runat="server" NavigateUrl='<%# "~\Account\ViewPerson.aspx?PersonID=" & Eval("Person_ID") %>'
                                            Text='<%# Eval("UserName") %>' />
                                        <br />
                                        <span style="color: Gray">
                                            <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("LastName") & " " & Eval("FirstName")& " " & Eval("Patronimic") %>' />
                                        </span>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <SettingsPager PageSize="10">
                            </SettingsPager>
                            <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                                </LoadingPanelOnStatusBar>
                                <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                                </LoadingPanel>
                            </Images>
                            <ImagesFilterControl>
                                <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                                </LoadingPanel>
                            </ImagesFilterControl>
                            <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                </Header>
                                <LoadingPanel ImageSpacing="5px">
                                </LoadingPanel>
                            </Styles>
                            <StylesEditors ButtonEditCellSpacing="0">
                                <ProgressBar Height="21px">
                                </ProgressBar>
                            </StylesEditors>
                        </dx:ASPxGridView>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>

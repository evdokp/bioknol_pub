﻿Public Class ViewTimeLine
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim pers As New BLL.Person(CType(Request.QueryString("PersonID"), Integer))
        CType(Master.FindControl("lit_pagetitle"), Literal).Text = pers.PageHeader
        CType(Master.FindControl("lit_curobj"), Literal).Text = "Timeline"
        hfPersonID.Value = pers.Person_ID


    End Sub

End Class
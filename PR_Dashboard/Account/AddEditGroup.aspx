﻿<%@ Page Title="" 
Language="vb" 
AutoEventWireup="false" 
MasterPageFile="~/Group.Master" 
CodeBehind="AddEditGroup.aspx.vb" 
Inherits="BioWS.AddEditGroup" ValidateRequest="false"  %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


   <h5>
                    <span>Group information</span>
                </h5>
                 <asp:Panel ID="pnlSent" runat="server" Visible="false" >
        
            <div class="blockinfo">
                <asp:Literal ID="litSent" runat="server"></asp:Literal>
            </div>
              
              <br />
            
        </asp:Panel>
    <table width="100%" cellpadding="4" cellspacing="4">
        <tr>
            <td valign="top" style="width: 170px; text-align: right;">
               Title:
            </td>
            <td valign="top" style="">


                <dx:ASPxTextBox ID="txtTitle" runat="server" 
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Width="300px">
                </dx:ASPxTextBox>


            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 170px; text-align: right;">
                Description:</td>
            <td valign="top" style="">


                <dx:ASPxMemo ID="txtDesc" runat="server" Height="100px" Width="100%" 
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                </dx:ASPxMemo>


              


            </td>
        </tr>
       
        <tr>
            <td valign="top" style="width: 170px; text-align: right;">
                &nbsp;</td>
            <td valign="top" style="">


                <dx:ASPxButton ID="btnAddSave" runat="server" 
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Save">
                </dx:ASPxButton>


            </td>
        </tr>
    </table>


    <asp:Panel ID="pnlPicture" runat="server">


    
   <h5>
                    <span>Group picture</span>
                </h5>
                   <table width="100%"  cellpadding="4" cellspacing="4">
        <tr>
            <td valign="top" style="width: 170px; text-align: right;">
                Select file for new picture:
            </td>
            <td valign="top" style="">


            <div style="margin-bottom:6px;">
            
                <asp:FileUpload ID="FileUpload2" runat="server" />
                </div>
                <dx:ASPxButton ID="btnUpload" runat="server" Text="Upload" 
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" 
                    CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                    

                </dx:ASPxButton>
<asp:Literal ID="lit_error" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 170px; text-align: right;">
                Current group picture:</td>
            <td valign="top" style="">
               <div style="margin-bottom:6px;">
                <asp:Image ID="Image1" runat="server"  />
                </div>
                   <dx:ASPxButton ID="btnDelete" runat="server" Text="Delete current picture" 
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" 
                    CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" 
                    Width="200px">
                </dx:ASPxButton>
            </td>
        </tr>
    
    </table>

        </asp:Panel>

</asp:Content>

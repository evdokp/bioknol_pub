﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Person.master"
    CodeBehind="ReadingProfilePerson.aspx.vb" Inherits="BioWS.ReadingProfilePerson" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Xpo.v12.1.Web, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Xpo" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
  <script type="text/javascript">
      $(document).ready(function () {


          $("#h3crit").click(function () {
              $("#tblCrit").toggle(300);
              var img = $($('#h3crit img'));
              ToggleImage(img);
          });
      });

      function ToggleImage(img) {
          if (img.attr('src') == '../Images/minus.gif') {
              img.attr('src', '../Images/plus.gif');
          } else {
              img.attr('src', '../Images/minus.gif');
          }
      }
            </script>

    <h3 class="hunderline" id="h3crit" style="margin-top:2px; padding-top:5px;">
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/minus.gif" />
                    Search over reading history
                </h3>
    <table width="100%" id="tblCrit">
        <tr>
            <td valign="top" style="width: 170px; text-align: right;">
                Title or abstract contains:
            </td>
            <td valign="top" style="">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
            <td style="width:70px;">
            
            
                <dx:ASPxComboBox ID="cmbFilterType" runat="server"  Width="100%"
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Spacing="0" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" ValueType="System.String">
                    <Items>
                        <dx:ListEditItem Text="all of" Value="all" Selected="true"  />
                        <dx:ListEditItem Text="any of" Value="any" />
                    </Items>
                    <LoadingPanelImage Url="~/App_Themes/DevEx/Editors/Loading.gif">
                    </LoadingPanelImage>
                    <LoadingPanelStyle ImageSpacing="5px">
                    </LoadingPanelStyle>
                    <ButtonStyle Width="13px">
                    </ButtonStyle>
                </dx:ASPxComboBox>
                </td>
            <td>
                <dx:ASPxTextBox ID="txtCondition" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                    CssPostfix="DevEx" NullText="Enter query here" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"
                    Width="100%">
                </dx:ASPxTextBox>
                </td>
            </tr>
            </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 170px; text-align: right;">
                Journals:
            </td>
            <td valign="top" style="">
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:bio_dataConnectionString %>"
                    SelectCommand="SELECT distinct pmJournals.Journal_ID,pmJournals.Title FROM [dbo].[GetDistinctPMIDsByPersonID] (@PersonID) as t
inner join ArticlesPubmed on ArticlesPubmed.Pubmed_ID = t.Pubmed_ID
inner join pmJournals on ArticlesPubmed.Journal_ID = pmJournals.Journal_ID order by pmJournals.Title">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="PersonID" QueryStringField="PersonID" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <dx:ASPxGridLookup ID="grJournals" runat="server" AllowUserInput="False" AutoGenerateColumns="False"
                    AutoResizeWithContainer="True" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                    CssPostfix="DevEx" DataSourceID="SqlDataSource1" KeyFieldName="Journal_ID" SelectionMode="Multiple"
                    Spacing="0" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Width="100%">
                      
                    <GridViewProperties>
                        <Settings ShowFilterRow="True" />
                        <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>
                    </GridViewProperties>
                    <Columns>
                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="20px">
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="Title" VisibleIndex="1">
                            <Settings AutoFilterCondition="Contains" />
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <GridViewImages SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                        <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                        </LoadingPanelOnStatusBar>
                        <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                        </LoadingPanel>
                    </GridViewImages>
                    <GridViewImagesFilterControl>
                        <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                        </LoadingPanel>
                    </GridViewImagesFilterControl>
                    <GridViewStyles>
                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                        </Header>
                        <LoadingPanel ImageSpacing="5px">
                        </LoadingPanel>
                    </GridViewStyles>
                    <GridViewStylesEditors ButtonEditCellSpacing="0">
                        <ProgressBar Height="21px">
                        </ProgressBar>
                    </GridViewStylesEditors>
                    <ButtonStyle Width="13px">
                    </ButtonStyle>
                </dx:ASPxGridLookup>
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 170px; text-align: right;">
                Authors:
            </td>
            <td valign="top" style="">
                <asp:SqlDataSource ID="sqlAuthors" runat="server" ConnectionString="<%$ ConnectionStrings:bio_dataConnectionString %>"
                    SelectCommand="SELECT distinct pmAuthors.Author_ID, pmAuthors.LastName, pmAuthors.Initials FROM [dbo].[GetDistinctPMIDsByPersonID] (@PersonID) as t
inner join LinksArticlesToAuthors on LinksArticlesToAuthors.Pubmed_ID = t.Pubmed_ID
inner join pmAuthors on LinksArticlesToAuthors.Author_ID = pmAuthors.Author_ID
order by pmAuthors.LastName, pmAuthors.Initials ">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="PersonID" QueryStringField="PersonID" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <dx:ASPxGridLookup ID="grAuthors" runat="server" AllowUserInput="False" AutoGenerateColumns="False"
                    AutoResizeWithContainer="True" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                    CssPostfix="DevEx" DataSourceID="sqlAuthors" KeyFieldName="Author_ID" SelectionMode="Multiple"
                    Spacing="0" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Width="100%"
                    TextFormatString="{0} {1}">
                    <GridViewProperties>
                        <Settings ShowFilterRow="True" />
                        <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>
                    </GridViewProperties>
                    <Columns>
                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="20px">
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="LastName" VisibleIndex="1">
                            <Settings AutoFilterCondition="Contains" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Initials" VisibleIndex="2">
                            <Settings AutoFilterCondition="Contains" />
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <GridViewImages SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                        <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                        </LoadingPanelOnStatusBar>
                        <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                        </LoadingPanel>
                    </GridViewImages>
                    <GridViewImagesFilterControl>
                        <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                        </LoadingPanel>
                    </GridViewImagesFilterControl>
                    <GridViewStyles>
                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                        </Header>
                        <LoadingPanel ImageSpacing="5px">
                        </LoadingPanel>
                    </GridViewStyles>
                    <GridViewStylesEditors ButtonEditCellSpacing="0">
                        <ProgressBar Height="21px">
                        </ProgressBar>
                    </GridViewStylesEditors>
                    <ButtonStyle Width="13px">
                    </ButtonStyle>
                </dx:ASPxGridLookup>
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 170px; text-align: right;">
                Publication years:
            </td>
            <td valign="top" style="">
                <asp:SqlDataSource ID="sqlPubYears" runat="server" ConnectionString="<%$ ConnectionStrings:bio_dataConnectionString %>"
                    SelectCommand="SELECT distinct ArticlesPubmed.PubYear FROM [dbo].[GetDistinctPMIDsByPersonID] (@PersonID) as t
inner join ArticlesPubmed on ArticlesPubmed.Pubmed_ID = t.Pubmed_ID where PubYear is not null order by PubYear desc ">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="PersonID" QueryStringField="PersonID" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <dx:ASPxGridLookup ID="grPubYears" runat="server" AllowUserInput="False" AutoGenerateColumns="False"
                    AutoResizeWithContainer="True" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                    CssPostfix="DevEx" DataSourceID="sqlPubYears" SelectionMode="Multiple" Spacing="0"
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Width="100%" TextFormatString="{0}"
                    KeyFieldName="PubYear">
                    <GridViewProperties>
                        <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>
                    </GridViewProperties>
                    <Columns>
                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="20px">
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="PubYear" VisibleIndex="0">
                            <CellStyle HorizontalAlign="Left">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <GridViewImages SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                        <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                        </LoadingPanelOnStatusBar>
                        <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                        </LoadingPanel>
                    </GridViewImages>
                    <GridViewImagesFilterControl>
                        <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                        </LoadingPanel>
                    </GridViewImagesFilterControl>
                    <GridViewStyles>
                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                        </Header>
                        <LoadingPanel ImageSpacing="5px">
                        </LoadingPanel>
                    </GridViewStyles>
                    <GridViewStylesEditors ButtonEditCellSpacing="0">
                        <ProgressBar Height="21px">
                        </ProgressBar>
                    </GridViewStylesEditors>
                    <ButtonStyle Width="13px">
                    </ButtonStyle>
                </dx:ASPxGridLookup>
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 170px; text-align: right;">
                MESH headings:
            </td>
            <td valign="top" style="">
                <asp:SqlDataSource ID="sqlMESH" runat="server" ConnectionString="<%$ ConnectionStrings:bio_dataConnectionString %>"
                    SelectCommand="SELECT distinct pmMeSH.MeSH_ID , pmMeSH.Heading FROM [dbo].[GetDistinctPMIDsByPersonID] (@PersonID) as t
inner join LinksArticlesToMeSH on LinksArticlesToMeSH.Pubmed_ID = t.Pubmed_ID
inner join pmMeSH on pmMeSH.MeSH_ID = LinksArticlesToMeSH.MeSH_ID order by pmMeSH.Heading  ">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="PersonID" QueryStringField="PersonID" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <dx:ASPxGridLookup ID="grMESH" runat="server" AutoGenerateColumns="False" DataSourceID="sqlMESH"
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" KeyFieldName="MeSH_ID"
                    Spacing="0" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Width="100%"
                    SelectionMode="Multiple">
                    <GridViewProperties>
                        <Settings ShowFilterRow="True" />
                        <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>
                    </GridViewProperties>
                    <Columns>
                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="20px">
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="Heading" VisibleIndex="1">
                            <Settings AutoFilterCondition="Contains" />
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <GridViewImages SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                        <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                        </LoadingPanelOnStatusBar>
                        <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                        </LoadingPanel>
                    </GridViewImages>
                    <GridViewImagesFilterControl>
                        <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                        </LoadingPanel>
                    </GridViewImagesFilterControl>
                    <GridViewStyles>
                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                        </Header>
                        <LoadingPanel ImageSpacing="5px">
                        </LoadingPanel>
                    </GridViewStyles>
                    <GridViewStylesEditors ButtonEditCellSpacing="0">
                        <ProgressBar Height="21px">
                        </ProgressBar>
                    </GridViewStylesEditors>
                    <ButtonStyle Width="13px">
                    </ButtonStyle>
                </dx:ASPxGridLookup>
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 170px; text-align: right;">
                &nbsp;
            </td>
            <td valign="top" style="">
                <dx:ASPxButton ID="btnFind" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                    CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Find">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>


    <br />


    
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" 
        DataSourceID="odsArticles" KeyFieldName="Pubmed_ID"  
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Width="100%">
        <Templates>
        <DetailRow>
           <asp:Literal ID="Literal1" runat="server" Text ='<%# Eval("Abstract") %>'/>
           <br /><br />
            <asp:HyperLink ID="hlPerson" runat="server"
                     NavigateUrl='<%# "~\ViewInfoPM.aspx?Pubmed_ID=" & Eval("Pubmed_ID") %>'
                     Text ="View article page in PRD"
                      Target="_blank" 
                    />
                    &nbsp;|
                    &nbsp;
                    <asp:HyperLink ID="HyperLink1" runat="server"
                     NavigateUrl='<%# "http://www.ncbi.nlm.nih.gov/pubmed/" & Eval("Pubmed_ID") %>'
                     Text ="View article page in Pubmed"
                     Target="_blank" 
                    />
                    &nbsp;|
                    &nbsp;
                    <asp:HyperLink ID="HyperLink2" runat="server"
                     NavigateUrl='<%# "~/RecommendArticle.aspx?PubmedID=" & Eval("Pubmed_ID") & "&UserID=" & Request.QueryString("PersonID")  %>'
                     Text ="Recommend"
                     Target="_blank" 
                    />

                    
        </DetailRow>
        </Templates>
        <Columns>
            <dx:GridViewDataTextColumn FieldName="Title" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
        </Columns>
        
        <SettingsPager PageSize="25" Position="Top">
        </SettingsPager>
        <Settings GridLines="Horizontal" ShowColumnHeaders="False" />
        <SettingsText EmptyDataRow="Specify search criterieas above" />
        <SettingsDetail ShowDetailRow="True" />
        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
            <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
            </LoadingPanelOnStatusBar>
            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
            </LoadingPanel>
        </Images>
        <ImagesFilterControl>
            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
            </LoadingPanel>
        </ImagesFilterControl>
        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
            <Header ImageSpacing="5px" SortingImageSpacing="5px">
            </Header>
            <EmptyDataRow Font-Italic="True">
            </EmptyDataRow>
            <LoadingPanel ImageSpacing="5px">
            </LoadingPanel>
        </Styles>
        <StylesEditors ButtonEditCellSpacing="0">
            <ProgressBar Height="21px">
            </ProgressBar>
        </StylesEditors>
        <Border BorderStyle="None" />
    </dx:ASPxGridView>
    <asp:ObjectDataSource ID="odsArticles" runat="server" 
        
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByAllParams" 
        TypeName="BioWS.ArticlesPubmedDataSetTableAdapters.ArticlesPubmedTableAdapter" 
        >
        
        <SelectParameters>
            <asp:Parameter Name="Fullt" Type="String" DefaultValue="thiswillneverbefound" />
            <asp:Parameter Name="Journals" Type="String" />
            <asp:Parameter Name="PubYears" Type="String" />
            <asp:Parameter Name="Authors" Type="String" />
            <asp:Parameter Name="MESH" Type="String" />
            <asp:Parameter Name="ObjectType" Type="String" />
            <asp:QueryStringParameter Name="ObjectID" QueryStringField="PersonID" 
                Type="Int32" />
        </SelectParameters>
        
    </asp:ObjectDataSource>
</asp:Content>


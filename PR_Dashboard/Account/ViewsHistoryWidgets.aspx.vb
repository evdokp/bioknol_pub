﻿Imports System.Web.UI.DataVisualization.Charting
Imports DevExpress.Web
Imports BioWS.BLL.Extensions

Public Class ViewsHistoryWidgets
    Inherits System.Web.UI.Page

    Private ReadOnly Property PersonID As Integer
        Get
            Return Me.QSInt32("PersonID")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.NotPostBack() Then
            Dim pers As New BLL.Person(PersonID)
            Me.SetTitlesToGrandMaster("History of article views - Widgets", pers.PageHeader)

            Dim pr As New ProfileCommon
            pr = pr.GetProfile(HttpContext.Current.User.Identity.Name)

            BindWidget(Today.AddDays(-pr.DefaultPeriodDays), Today)

        End If
    End Sub



    Private Sub BindWidget(ByVal StartDate As DateTime, ByVal EndDate As DateTime)
        If Me.QS("WidgetID").IsNotEmpty() Then


            ASPxSplitter1.Panes(0).ContentUrl = String.Format("../Widgets/ViewWidget.aspx?PersonID={0}&WidgetID={1}&StartDate={2}&EndDate={3}",
                                                       Me.QS("PersonID"),
                                                       Me.QS("WidgetID"),
                                                       StartDate,
                                                       EndDate)
        Else
            ASPxSplitter1.Panes(0).ContentUrl = "../Widgets/ViewPubmedIDs.aspx?PersonID=" & Me.QS("PersonID")
        End If
    End Sub



    Private Sub SetPeriod1_PeriodChanged(StartDate As Date, EndDate As Date) Handles SetPeriod1.PeriodChanged
        BindWidget(StartDate, EndDate)
    End Sub
End Class
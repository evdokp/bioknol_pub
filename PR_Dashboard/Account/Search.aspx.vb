﻿
Imports BioWS.BLL.Extensions
Imports System.Drawing


Public Class Search
    Inherits System.Web.UI.Page

    Protected ReadOnly Property CurPersonID As Integer
        Get
            If Me.QS("PersonID").IsNullOrEmpty() Then
                Return Me.GetProfilePersonID()
            Else
                Return Me.QSInt32("PersonID")
            End If
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.NotPostBack() Then


            Dim pers As New BLL.Person(CurPersonID)
            Me.SetTitlesToGrandMaster(pers.PageHeader, "History search")

            If Me.QS("query").IsNotEmpty() Then
                txtCondition.Text = HttpUtility.UrlDecode(Me.QS("query"))
                Find(HttpUtility.UrlDecode(Me.QS("query")))
            Else
                ASPxGridView1.Visible = False
            End If
        End If
    End Sub

    Private Sub Find(ByVal userquery As String)
        Dim foundCount As Integer
        Dim historystring As String = ArticlesDA.GetDistinctHistoryPMIDS_byPerson(CurPersonID, SetPeriod1.StartDate, SetPeriod1.EndDate).ListToString(",")
        Dim pmlist As List(Of Integer) = BLL.PubMed.GetPMIDlistByQuery(userquery, historystring, foundCount)
        Dim pmliststr As String = IIf(pmlist.Count = 0, "-1000", pmlist.ListToString(","))

        Dim sqlquery As String = "SELECT     TOP (1000) ArticlesPubmed.Pubmed_ID, ArticlesPubmed.Title, ArticlesPubmed.PubYear, pmJournals.Title AS JournalTitle, convert(varchar, MAX(v_PubmedViews.TimeStamp), 102) +' '+ convert(varchar(5),MAX(v_PubmedViews.TimeStamp),108) " & _
                     " AS LastViewDate, COUNT(v_PubmedViews.TimeStamp) AS ViewsCountByPerson, v_ViewsCountByPubmedID.ViewsCount, " & _
                    "   v_pmArticleAuthorsConcat.AuthorsString FROM         ArticlesPubmed INNER JOIN " & _
                     "  pmJournals ON ArticlesPubmed.Journal_ID = pmJournals.Journal_ID INNER JOIN " & _
                      " v_PubmedViews ON ArticlesPubmed.Pubmed_ID = v_PubmedViews.PubMed_ID INNER JOIN " & _
                      " v_ViewsCountByPubmedID ON ArticlesPubmed.Pubmed_ID = v_ViewsCountByPubmedID.PubMed_ID INNER JOIN " & _
                      " v_pmArticleAuthorsConcat ON ArticlesPubmed.Pubmed_ID = v_pmArticleAuthorsConcat.Pubmed_ID " & _
                    " WHERE     (v_PubmedViews.Person_ID = " & CurPersonID & " ) and ArticlesPubmed.Pubmed_ID in (" & pmliststr & ") " & _
                    " GROUP BY ArticlesPubmed.Pubmed_ID, ArticlesPubmed.Title, ArticlesPubmed.PubYear, pmJournals.Title, v_PubmedViews.Person_ID, v_ViewsCountByPubmedID.ViewsCount, " & _
        " v_pmArticleAuthorsConcat.AuthorsString order by MAX(v_PubmedViews.TimeStamp) desc"

        Dim table = BLL.SQLHelper.GetDT_AllStrings(sqlquery)


        If foundCount = 0 Then
            ASPxGridView1.Visible = False
            lit_result.Text = "No articles found. Try another query or widen history period."
            pnlEmpty.Visible = True

        Else
            pnlEmpty.Visible = False
            ASPxGridView1.Visible = True
            ASPxGridView1.DataSource = table
            ASPxGridView1.DataBind()

            lit_result.Text = String.Format("<b>{0}</b> article(s) found", foundCount)

        End If
    End Sub



    'Protected Sub btn_find_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnFind.Click()
    '    Find(txtCondition.Text)
    'End Sub
    Private Sub btnFind_Click(sender As Object, e As System.EventArgs) Handles btnFind.Click
        Find(txtCondition.Text)
    End Sub

    'Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        Dim r_row As BLL.PubMed.PubMedArticle
    '        r_row = CType(e.Row.DataItem, BLL.PubMed.PubMedArticle)

    '        Dim HyperLink1 As HyperLink
    '        HyperLink1 = e.Row.Cells(1).FindControl("HyperLink1")

    '        HyperLink1.Text = r_row.Title
    '        HyperLink1.NavigateUrl = r_row.ViewPMURL

    '        Dim hl_download As HyperLink
    '        hl_download = e.Row.Cells(2).FindControl("hl_download")

    '        Dim pm As New BLL.PubMed.PubMedArticle(r_row.Pubmed_ID)
    '        If pm.OffersWithFilesCount > 0 Then
    '            hl_download.Visible = True
    '            hl_download.ToolTip = "Download available"
    '            hl_download.NavigateUrl = pm.ViewPMURL
    '        Else
    '            hl_download.Visible = False
    '        End If






    '    End If
    'End Sub


    Private Sub SetPeriod1_PeriodChanged(StartDate As Date, EndDate As Date) Handles SetPeriod1.PeriodChanged
        Find(txtCondition.Text)
    End Sub

End Class
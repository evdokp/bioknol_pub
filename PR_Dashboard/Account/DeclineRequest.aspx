﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master.Master"
    CodeBehind="DeclineRequest.aspx.vb" Inherits="BioWS.DeclineRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Decline request
    </h2>
    <fieldset class="register" style="width: 500px;">
        <legend>Request decline information</legend>
        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="View1" runat="server">
                  <p>
                        <asp:Literal ID="lit_title" runat="server"></asp:Literal>
                </p>

                <p>
                    Decline reason:<br />
                       <asp:TextBox ID="txtDR" runat="server" TextMode="MultiLine" Rows="4" Width="320px"
                        CssClass="textEntry"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="dr_req" runat="server" ControlToValidate="txtDR"
                        CssClass="failureNotification" ErrorMessage="Decline reason is required." ToolTip="Decline reason is required."
                        ValidationGroup="Decline">*</asp:RequiredFieldValidator>


                </p>

                  <p>
                    <asp:Button ID="btn_decline" runat="server" Text="Decline request" ValidationGroup ="Decline" />
                </p>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <p>
                   Request was declined. 
                </p>
                <p>
                
                    <asp:Button ID="btn_goback" runat="server" Text="Go back other people requests" />
                </p>
            </asp:View>
        </asp:MultiView>
    </fieldset>
</asp:Content>

﻿<%@ Page Title="My requests" Language="vb" AutoEventWireup="false" MasterPageFile="~/Basic.Master"
    CodeBehind="MyRequests.aspx.vb" Inherits="BioWS.MyRequests" %>
    <%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <div class="nolink">
        <dx:ASPxTabControl ID="tabs" runat="server" ActiveTabIndex="1" ClientIDMode="AutoID"
            CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css" CssPostfix="Office2010Silver"
            SpriteCssFilePath="~/App_Themes/Office2010Silver/{0}/sprite.css" TabSpacing="0px"
            Width="100%">
            <Tabs>
                <dx:Tab Name="Pending">
                </dx:Tab>
                <dx:Tab Name="Declined">
                </dx:Tab>
            
            </Tabs>
            <Paddings Padding="2px" PaddingLeft="5px" PaddingRight="5px" />
            <ContentStyle>
                <Border BorderColor="#868B91" BorderStyle="Solid" BorderWidth="1px" />
            </ContentStyle>
        </dx:ASPxTabControl>
    </div>
    <br /><br />
    
    <table width="100%" cellpadding="5" cellspacing="2">
        <tr>
            <td style="width: 100px;" align="center">
                Request date
            </td>
            <td style="width: 100px; border-left: 1px solid grey;" align="center">
                Request time
            </td>
            <td style="border-left: 1px solid grey;" align="center">
                Article title
            </td>
            <td style="width: 100px; border-left: 1px solid grey;" align="center">
                Info / status
            </td>
            <td style="width: 100px; border-left: 1px solid grey;" align="center">
                Action
            </td>
        </tr>
    </table>
    <div class="blueHeader clearFix">
        <div style="float: left;">
        </div>
        <div style="float: right; padding-right: 5px;">
        </div>
        <div class="clear">
        </div>
    </div>
    <asp:ObjectDataSource ID="ods_requests" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData_R_PendingByPersonID"
        TypeName="BioWS.ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter" UpdateMethod="Update">
        <SelectParameters>
            <asp:ProfileParameter Name="Person_ID" PropertyName="Person_ID" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ods_declined" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData_R_DeclinedByPersonID"
        TypeName="BioWS.ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter" UpdateMethod="Update">
        <SelectParameters>
            <asp:ProfileParameter Name="Person_ID" PropertyName="Person_ID" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:GridView ID="gv_requests" runat="server" AutoGenerateColumns="False" DataSourceID="ods_requests"
        GridLines="None" ShowHeader="False" CellPadding="5" CellSpacing="2" Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="TimeStamp" SortExpression="TimeStamp">
                <ItemTemplate>
                    <div style="padding-left: 8px;">
                        <asp:Literal ID="lit_date" runat="server" Text='<%# Bind("RequestDate", "{0:d}") %>'></asp:Literal>
                    </div>
                </ItemTemplate>
                <ItemStyle Width="100px" VerticalAlign="Top" HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="TimeStamp" SortExpression="TimeStamp">
                <ItemTemplate>
                    <div style="padding-left: 8px;">
                        <asp:Literal ID="lit_time" runat="server" Text='<%# Bind("RequestDate", "{0:t}") %>'></asp:Literal>
                    </div>
                </ItemTemplate>
                <ItemStyle Width="100px" VerticalAlign="Top" HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField SortExpression="Title">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank"></asp:HyperLink>
                    <asp:Literal ID="lit_comment" runat="server"></asp:Literal>
                </ItemTemplate>
                <ItemStyle VerticalAlign="Top" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Image ID="img_domain" runat="server" ImageUrl="~/Images/document_error.png" />
                    <asp:HyperLink ID="hl_pubmed" Target="_blank" runat="server" ImageUrl="~/Images/pubmed.gif"></asp:HyperLink>
                    <asp:HyperLink ID="hl_doi" Target="_blank" runat="server" ImageUrl="~/Images/doi_icon.jpg"></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle Width="100px" VerticalAlign="Top" HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkb_action" runat="server" CommandArgument='<%# Eval("ID") %>'
                        OnClick="lnkb_action_Click"></asp:LinkButton>
                    <cc1:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure?"
                        TargetControlID="lnkb_action" />
                </ItemTemplate>
                <ItemStyle Width="100px" VerticalAlign="Top" HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>

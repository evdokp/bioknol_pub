﻿Imports BioWS.BLL.Extensions



Public Class MyRequests
    Inherits System.Web.UI.Page

    Private _personID As Integer?
    Private ReadOnly Property PersonID As Integer
        Get
            If _personID Is Nothing Then
                Dim _pr As New ProfileCommon
                _pr = _pr.GetProfile(User.Identity.Name)
                _personID = _pr.Person_ID
            End If
            Return _personID
        End Get
    End Property
    Private Sub SetTab(ByVal tabtypename As String, ByVal tabdigit As Integer)
        Dim tab As DevExpress.Web.ASPxTabControl.Tab
        tab = tabs.Tabs.FindByName(tabtypename)
        tab.Text = String.Format("{0} ({1})", tabtypename, tabdigit)
    End Sub
    Private Sub SetTabs(ByVal person As BLL.Person)
        SetTab("Pending", person.MyRequestCount_Pending)
        SetTab("Declined", person.MyRequestCount_Declined)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If User.Identity.IsAuthenticated Then
            Dim person As New BLL.Person(PersonID)
            Me.SetTitlesToGrandMaster("My requests", person.PageHeader)
            SetTabs(person)

            If Me.NotPostBack() Then
                tabs.ActiveTab = tabs.Tabs.FindByName("Pending")
                SetGV("Pending")
            End If
        Else
            Response.Redirect("~\Account\Login.aspx")
        End If

        
    End Sub

    Private Sub SetGV(ByVal type As String)
        If type = "Pending" Then
            gv_requests.DataSourceID = "ods_requests"
        Else
            gv_requests.DataSourceID = "ods_declined"
        End If
        gv_requests.DataBind()
    End Sub

    Private Sub ASPxTabControl1_TabClick(source As Object, e As DevExpress.Web.ASPxTabControl.TabControlCancelEventArgs) Handles tabs.TabClick
        SetGV(e.Tab.Name)
    End Sub

    Private Sub gv_requests_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gv_requests.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim _pr As New ProfileCommon
            _pr = _pr.GetProfile(User.Identity.Name)

            Dim r_row As ArticlesDataSet.ArticleRequestsRow
            r_row = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row, ArticlesDataSet.ArticleRequestsRow)
            Dim Hyperlink1 As HyperLink
            Hyperlink1 = e.Row.Cells(2).FindControl("Hyperlink1")
            Dim hl_pubmed As HyperLink
            hl_pubmed = e.Row.Cells(3).FindControl("hl_pubmed")
            Dim hl_doi As HyperLink
            hl_doi = e.Row.Cells(3).FindControl("hl_doi")

            Dim lnkb_action As LinkButton = e.Row.Cells(4).FindControl("lnkb_action")


            If r_row.IsPubMed_IDNull = True Then
                Hyperlink1.Text = r_row.Title
                Hyperlink1.NavigateUrl = r_row.URL
                hl_pubmed.Visible = False
            Else
                Dim ta As New ArticlesPubmedDataSetTableAdapters.ArticlesPubmedTableAdapter
                Dim r = ta.GetDataByPMID(r_row.PubMed_ID).Item(0)
                Hyperlink1.Text = r.Title
                Hyperlink1.NavigateUrl = "~\ViewInfoPM.aspx?Pubmed_ID=" & r_row.PubMed_ID & "&UserID=" & _pr.Person_ID
                hl_pubmed.NavigateUrl = "http://www.ncbi.nlm.nih.gov/pubmed/" & r_row.PubMed_ID
                hl_pubmed.Visible = True
                hl_pubmed.ToolTip = "PMID: " & r_row.PubMed_ID
            End If
            If r_row.IsDOINull = True Then
                hl_doi.Visible = False
            Else
                If r_row.DOI = String.Empty Then
                    hl_doi.Visible = False
                Else
                    hl_doi.Visible = True
                    hl_doi.NavigateUrl = "http://dx.doi.org/" & r_row.DOI
                End If
            End If

            Dim img_domain As Image = e.Row.Cells(3).FindControl("img_domain")
            'If BLL.Common.GetSiteID(r_row.URL) = 0 Then
            '    img_domain.Visible = True
            '    img_domain.ToolTip = "URL is not in domain list"
            'Else
            '    img_domain.Visible = False
            'End If
            img_domain.Visible = False

            Dim lit As Literal
            lit = e.Row.Cells(2).FindControl("lit_comment")
            If Me.gv_requests.DataSourceID = "ods_declined" Then
                If r_row.IsCommentNull Then
                    lit.Text = "<br/><i>Decline reason not indicated</i>"
                Else
                    lit.Text = "<br/><i>Decline reason</i>: <b>" & r_row.Comment & "</b>"
                End If
                lnkb_action.Text = "Delete"
            Else
                Dim dd As Integer
                dd = DateDiff(DateInterval.Day, r_row.RequestDate, Now)
                If dd < 1 Then
                    lit.Text = "<br/><i>Pending less then a day</i>"
                ElseIf dd = 1 Then
                    lit.Text = "<br/><i>Pending for 1 day</i>"
                Else
                    lit.Text = "<br/><i>Pending for " & dd & " days</i>"
                End If

                lnkb_action.Text = "Cancel"

            End If

        End If
    End Sub





    Protected Sub lnkb_action_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim request_id As Integer = CType(CType(sender, LinkButton).CommandArgument, Integer)
        Dim ta As New ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter
        ta.Delete(request_id)
        gv_requests.DataBind()
    End Sub


End Class
﻿Imports System.Web.UI.DataVisualization.Charting
Imports DevExpress.Web
Public Class GroupHistoryWidgets
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim gr As New BLL.Group(Request.QueryString("GroupID"))
            CType(Master.Master.FindControl("lit_pagetitle"), Literal).Text = gr.Title
            CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "History of article views - Widgets"

            Dim pr As New ProfileCommon
            pr = pr.GetProfile(HttpContext.Current.User.Identity.Name)

        
            BindWidget(Today.AddDays(-pr.DefaultPeriodDays), Today)

        End If

    End Sub
    Private Sub SetPeriod1_PeriodChanged(StartDate As Date, EndDate As Date) Handles SetPeriod1.PeriodChanged
        BindWidget(StartDate, EndDate)
    End Sub
    Private Sub BindWidget(ByVal StartDate As DateTime, ByVal EndDate As DateTime)
        If Request.QueryString("WidgetID") > 0 Then
            Me.ASPxSplitter1.Panes(0).ContentUrl = String.Format("..\Widgets\ViewWidget.aspx?GroupID={0}&WidgetID={1}&StartDate={2}&EndDate={3}",
                                                       Request.QueryString("GroupID"),
                                                       Request.QueryString("WidgetID"),
                                                       StartDate,
                                                       EndDate)
        Else
            Me.ASPxSplitter1.Panes(0).ContentUrl = String.Format("..\Widgets\ViewWidget.aspx?GroupID={0}&StartDate={1}&EndDate={2}",
                                                        Request.QueryString("GroupID"),
                                                        StartDate,
                                                        EndDate)
        End If
    End Sub
  
End Class
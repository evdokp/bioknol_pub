﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Person.master"
    CodeBehind="Search.aspx.vb" Inherits="BioWS.Search" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Xpo.v12.1.Web, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Xpo" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="../UserControls/SetPeriod.ascx" tagname="SetPeriod" tagprefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  
      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>


        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnFind">
     <table width="100%" style="padding-top:10px; padding-left:10px;">
     
        <tr>
            <td style="width:100px;" valign="middle">
    <uc1:SetPeriod ID="SetPeriod1" runat="server" />
    </td>
            <td valign="middle" >
                <dx:ASPxTextBox ID="txtCondition" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                    CssPostfix="DevEx" NullText="Enter query here" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"
                    Width="100%">
                </dx:ASPxTextBox>
            </td>
            <td valign="middle" style="width:80px">
                 <dx:ASPxButton ID="btnFind" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" Width="80px"
                    CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Find">
                </dx:ASPxButton>
            
            </td>
            <td>   <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/ajax-loader.gif" />
                            </ProgressTemplate>
                        </asp:UpdateProgress></td>
        </tr>
        </table>
     </asp:Panel>

        

                        <span style="float:right; font-style:italic;"><asp:Literal ID="lit_result" runat="server"></asp:Literal></span>

        

<%--
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetArticlesFromPubmedByQuery" TypeName="BioWS.ArticlesDA">
                <SelectParameters>
                    <asp:Parameter Name="query" DefaultValue="this will never be found blah-blah" Type="String" />
                    <asp:ProfileParameter PropertyName="Person_ID" Name="pid" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>--%>
      
            <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False"  
                CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Width="100%">
                <Columns>
                    <dx:GridViewDataTextColumn FieldName="Title" VisibleIndex="0">
                         <DataItemTemplate>
                  
                  <%# Eval("PubYear")%>, <%# Eval("JournalTitle")%>, <%# Eval("AuthorsString")%><br />
                  
                    <asp:HyperLink ID="hlArticle" runat="server"  CssClass="link"
                     NavigateUrl='<%# "~\ViewInfoPM.aspx?Pubmed_ID=" & Eval("Pubmed_ID") %>'
                     Text ='<%# Eval("Title") %>'/>
                  
                </DataItemTemplate>
                         <CellStyle CssClass="link">
                         </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="LastViewDate" VisibleIndex="1" Caption="Last view" Width="70">
    
                    </dx:GridViewDataTextColumn>
                   
                    <dx:GridViewBandColumn Caption="Views count" VisibleIndex="2">
                    <Columns>
                    <dx:GridViewDataTextColumn FieldName="ViewsCountByPerson" VisibleIndex="3" Caption="by me" Width="35">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ViewsCount" VisibleIndex="4" Caption= "by all" Width="35">
                    </dx:GridViewDataTextColumn> 
                    </Columns>
                    </dx:GridViewBandColumn>
                </Columns>
                <SettingsBehavior AllowSort="False" />
                <SettingsPager PageSize="1000">
                </SettingsPager>
                <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                    </LoadingPanel>
                </Images>
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <LoadingPanel ImageSpacing="5px">
                    </LoadingPanel>
                </Styles>
                <StylesEditors ButtonEditCellSpacing="0">
                    <ProgressBar Height="21px">
                    </ProgressBar>
                </StylesEditors>
            </dx:ASPxGridView>
      
             <asp:Panel ID="pnlEmpty" runat="server">
             <div style ="text-align: center ;">
                <br /><br /><br />
                    <asp:Image ID="Image3" runat="server"  ImageUrl ="~/Images/SearchEDT.png"/>
                <br /><br /><br /><br />
                
                </div>
       
            </asp:Panel>


        </ContentTemplate>
    </asp:UpdatePanel>
    <br /><br /><br /><br />
</asp:Content>

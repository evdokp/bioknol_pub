﻿Imports BioWS.BLL.Extensions


Public Class GroupHistory
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim gr As New BLL.Group(Me.QSInt32("GroupID"))
        Me.SetTitlesToGrandMaster(gr.Title, "Activity feed")
        hfPageOpenDateTime.Value = Now.ToString
        hfGroupID.Value = Me.QS("GroupID")
    End Sub


    <System.Web.Services.WebMethod()> _
    Public Shared Function GetFeed(ByVal GroupID As Integer,
                                   ByVal EarlierThen As String,
                                    ByVal LastIndex As Integer) As String


        Dim dt As Date
        DateTime.TryParse(EarlierThen, dt)

        Dim db = New DataClassesFeedDataContext


        Dim tt = From c In db.GetActivityFeedGroup(20, EarlierThen, LastIndex, GroupID)
                Select vZero = 0, vHTML = BLL.Feed.FeedHelper.RenderAction(c.Action, c.ActionTime, c.ActionType)


        Return tt.ToList().ToJSON

    End Function
End Class
﻿Imports System.IO
Imports AzureStorageLibrary


Public Class AddEditGroup
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("GroupID") = "" Then
                'add
                CType(Master.Master.FindControl("lit_pagetitle"), Literal).Text = "Adding group"
                CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "New group"
                Me.btnAddSave.Text = "Add new group"
                pnlPicture.Visible = False
            Else
                'edit
                CType(Master.Master.FindControl("lit_pagetitle"), Literal).Text = "Editing group"
                Dim g As New BLL.Group(CType(Request.QueryString("GroupID"), Integer))
                CType(Master.Master.FindControl("lit_curobj"), Literal).Text = g.Title
                Me.txtTitle.Text = g.Title
                Me.txtDesc.Text = g.Description
                Me.btnAddSave.Text = "Save"

                If g.HasImage Then
                    Image1.ImageUrl = g.Image200
                    Image1.Visible = True

                Else
                    lit_error.Text = "<i>no picture</i>"
                    Image1.Visible = False
                    btnDelete.Visible = False
                End If
            End If
        End If
    End Sub


    Private Sub btnAddSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddSave.Click
        Dim g As BLL.Group
        If Request.QueryString("GroupID") = "" Then
            Dim _pr As New ProfileCommon
            _pr = _pr.GetProfile(User.Identity.Name)

            Dim group = New BLL.Group(Me.txtTitle.Text, 1, _pr.Person_ID, Me.txtDesc.Text, False)
            BLL.Group.ReFillChildrenTable()

            Response.Redirect("~\Account\AddEditGroup.aspx?GroupID=" & group.Group_ID)

        Else
            g = New BLL.Group(CInt(Request.QueryString("GroupID")))
            g.Title = Me.txtTitle.Text
            g.Description = Me.txtDesc.Text
            g.Update()
        End If
        pnlSent.Visible = True
        litSent.Text = "Group information saved"
    End Sub

    Protected Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        If FileUpload2.HasFile = True Then
            Dim forg, f5050, f200200 As Byte()
            Dim originalName As String = Path.GetFileName(FileUpload2.PostedFile.FileName)
            Dim extension As String = Path.GetExtension(originalName)
            forg = New Byte(FileUpload2.PostedFile.InputStream.Length) {}
            FileUpload2.PostedFile.InputStream.Read(forg, 0, forg.Length)

            Dim isvalid As String
            isvalid = BLL.ImageHelper.ImageUploadValidate(FileUpload2.PostedFile.ContentType, FileUpload2.PostedFile.ContentLength)
            If isvalid = "true" Then
                Dim fileguid As Guid = Guid.NewGuid

                f5050 = BLL.ImageHelper.MakeThumb(forg, 50, 50)
                f200200 = BLL.ImageHelper.MakeThumbMaxWidth(forg, 200)

                Dim storage As New AzureStorage()
                storage.UploadImage(f5050, String.Format("{0}{1}{2}", fileguid, "_50", extension))
                storage.UploadImage(f200200, String.Format("{0}{1}{2}", fileguid, "_200", extension))

                'BLL.ImageHelper.SaveFile(fileguid, "_50", originalName, f5050, Me)
                'BLL.ImageHelper.SaveFile(fileguid, "_200", originalName, f200200, Me)

                Dim g = New BLL.Group(CInt(Request.QueryString("GroupID")))
                g.SaveImage(fileguid.ToString, Path.GetExtension(originalName).Substring(1), Page)

                Image1.ImageUrl = g.Image200
                Image1.Visible = True
                btnDelete.Visible = True
                Dim ct1 As ContentPlaceHolder = CType(Master.Master.FindControl("MainContent"), ContentPlaceHolder)
                CType(ct1.FindControl("imgAvatar"), Image).Visible = True
                CType(ct1.FindControl("imgAvatar"), Image).ImageUrl = g.Image200

            Else
                Me.lit_error.Text = "<br/>" & isvalid
            End If


        End If
    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim g = New BLL.Group(CInt(Request.QueryString("GroupID")))
        g.DeleteImageAzure()

        Image1.Visible = False
        lit_error.Text = "<i>no picture</i>"
        Dim ct1 As ContentPlaceHolder = CType(Master.Master.FindControl("MainContent"), ContentPlaceHolder)
        CType(ct1.FindControl("imgAvatar"), Image).Visible = False
        btnDelete.Visible = False
    End Sub
End Class
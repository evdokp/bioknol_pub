﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Basic.Master"
    CodeBehind="OtherPeopleRequests.aspx.vb" Inherits="BioWS.OtherPeopleRequests" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
    </h1>
 
    <br />
    <asp:ObjectDataSource ID="ods_requests" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetOtherPeopleRequestsUnaccepted" TypeName="BioWS.ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter">
        <SelectParameters>
            <asp:ProfileParameter Name="Person_ID" PropertyName="Person_ID" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <table width="100%" cellpadding="5" cellspacing="2">
        <tr>
            <td style="width: 100px;" align="center">
                Request date
            </td>
            <td style="width: 100px; border-left: 1px solid grey;" align="center">
                Request time
            </td>
            <td style="border-left: 1px solid grey;" align="center">
                Article title
            </td>
            <td style="width: 100px; border-left: 1px solid grey;" align="center">
                Info / status
            </td>
            <td style="width: 100px; border-left: 1px solid grey;" align="center">
                Action
            </td>
        </tr>
    </table>
    <div class="blueHeader clearFix">
        <div style="float: left;">
        </div>
        <div style="float: right; padding-right: 5px;">
        </div>
        <div class="clear">
        </div>
    </div>
    <asp:GridView ID="gv_requests" runat="server" AutoGenerateColumns="False" DataSourceID="ods_requests"
        GridLines="None" ShowHeader="False" CellPadding="5" CellSpacing="2" Width="100%"
        DataKeyNames="ID">
        <Columns>
            <asp:TemplateField HeaderText="TimeStamp" SortExpression="TimeStamp">
                <ItemTemplate>
                    <div style="padding-left: 8px;">
                        <asp:Literal ID="lit_date" runat="server" Text='<%# Bind("RequestDate", "{0:d}") %>'></asp:Literal>
                    </div>
                </ItemTemplate>
                <ItemStyle Width="100px" VerticalAlign="Top" HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="TimeStamp" SortExpression="TimeStamp">
                <ItemTemplate>
                    <div style="padding-left: 8px;">
                        <asp:Literal ID="lit_time" runat="server" Text='<%# Bind("RequestDate", "{0:t}") %>'></asp:Literal>
                    </div>
                </ItemTemplate>
                <ItemStyle Width="100px" VerticalAlign="Top" HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField SortExpression="Title">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank"></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle VerticalAlign="Top" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Image ID="img_domain" runat="server" ImageUrl="~/Images/document_error.png" />
                    <asp:HyperLink ID="hl_pubmed" Target="_blank" runat="server" ImageUrl="~/Images/pubmed.gif"></asp:HyperLink>
                    <asp:HyperLink ID="hl_doi" Target="_blank" runat="server" ImageUrl="~/Images/doi_icon.jpg"></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle Width="100px" VerticalAlign="Top" HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Image ID="img1" runat="server" ImageUrl="~/Images/document_gear.png" />
               
                <ajaxToolkit:HoverMenuExtender ID="img1_HoverMenuExtender" runat="server" 
                    DynamicServicePath="" Enabled="True" PopupControlID="Panel1"  PopupPosition ="Left" 
                    TargetControlID="img1">
                </ajaxToolkit:HoverMenuExtender>

             
                   <asp:Panel ID="Panel1" runat="server" >
                        <div class="popup_in"  style="text-align:left">
                            <h4>
                                Select action:</h4>
                            <asp:LinkButton ID="lnkb_upload" runat="server" OnClick="lnkb_upload_Click" CommandArgument='<%# Eval("ID") %>'>Upload paper</asp:LinkButton>
                            <br />
                            <br />
                            Decline request for following reason:<br />
                            <asp:LinkButton ID="lnkb_notarticle" runat="server" OnClick="lnkb_notarticle_Click"
                                CommandArgument='<%# Eval("ID")  %>'>Is not article</asp:LinkButton><br />
                            <asp:LinkButton ID="lnkb_duplicate" runat="server" OnClick="lnkb_duplicate_Click"
                                CommandArgument='<%# Eval("ID") %>'>Duplicate request</asp:LinkButton><br />
                            Specify another reason:<br />
                            <asp:TextBox ID="txt_reason" runat="server" TextMode="MultiLine" Rows="2"></asp:TextBox>
                            <br />
                            <asp:LinkButton ID="lnkb_decline" runat="server" OnClick="lnkb_decline_Click" CommandArgument='<%# Eval("ID") & ";" & Container.DataItemIndex  %>'
                                ValidationGroup="reas_V">Decline</asp:LinkButton><br />
                        </div>
                    </asp:Panel>
                </ItemTemplate>
                <ItemStyle Width="100px" VerticalAlign="Top" HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master.Master"
    CodeBehind="MyPage.aspx.vb" Inherits="BioWS.MyPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <br />
    <table width="100%" cellpadding="4" cellspacing="4">
        <tr>
            <td valign="top" style="width: 50%;">
                <div class="blueHeader clearFix">
                    Quick statistics
                    <div class="clear">
                    </div>
                </div>
                <table cellpadding="3" cellspacing="3">
                    <tr>
                        <td valign="top" style="width: 200px;">
                            Views
                        </td>
                        <td valign="top" style="width: 30%; text-align: right;">
                            <b>
                                <asp:Literal ID="lit_views" runat="server"></asp:Literal></b>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 200px;">
                            Articles
                        </td>
                        <td valign="top" style="width: 30%; text-align: right;">
                            <b>
                                <asp:Literal ID="lit_art" runat="server"></asp:Literal></b>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 200px;">
                            Period, days
                        </td>
                        <td valign="top" style="width: 30%; text-align: right;">
                            <b>
                                <asp:Literal ID="lit_period" runat="server"></asp:Literal></b>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 200px;">
                            Average articles/day
                        </td>
                        <td valign="top" style="width: 30%; text-align: right;">
                            <b>
                                <asp:Literal ID="lit_art_avg" runat="server"></asp:Literal></b>
                        </td>
                    </tr>
                </table>
                <hr />
                <asp:HyperLink ID="hl_history" runat="server"> >> Go to views history</asp:HyperLink><br />
                <asp:HyperLink ID="hl_stat" runat="server"> >> Go to charts</asp:HyperLink>
                <br />
                <br />
                <br />
                <div class="blueHeader clearFix">
                    Articles
                    <div class="clear">
                    </div>
                </div>
                <table cellpadding="3" cellspacing="3">
                    <tr>
                        <td valign="top" style="width: 200px;">
                            <asp:HyperLink ID="hl_requests" runat="server">My requests</asp:HyperLink>
                        </td>
                        <td valign="top" style="width: 30%; text-align: right;">
                            <b>
                                <asp:Literal ID="lit_requests" runat="server"></asp:Literal></b>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 200px;">
                            <asp:HyperLink ID="hl_other_requests" runat="server">Other people requests</asp:HyperLink>
                        </td>
                        <td valign="top" style="width: 30%; text-align: right;">
                            <b>
                                <asp:Literal ID="lit_other_requests" runat="server"></asp:Literal></b>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 200px;">
                            <asp:HyperLink ID="hl_avail_art" runat="server">Available articles</asp:HyperLink>
                        </td>
                        <td valign="top" style="width: 30%; text-align: right;">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 200px; padding-left: 20px;">
                            Uploaded by me
                        </td>
                        <td valign="top" style="width: 30%; text-align: right;">
                            <b>
                                <asp:Literal ID="lit_art_byme" runat="server"></asp:Literal></b>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 200px; padding-left: 20px;">
                            My requests (accepted)
                        </td>
                        <td valign="top" style="width: 30%; text-align: right;">
                            <b>
                                <asp:Literal ID="lit_art_myreq" runat="server"></asp:Literal></b>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 200px; padding-left: 20px;">
                            Uploaded by others
                        </td>
                        <td valign="top" style="width: 30%; text-align: right;">
                            <b>
                                <asp:Literal ID="lit_art_notme" runat="server"></asp:Literal></b>
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top" style="width: 50%;">
                <asp:Panel ID="pnl_admin" runat="server">
            <div class="blueHeader clearFix">
                    Manage system
                    <div class="clear">
                    </div>
                </div>
                <table cellpadding="3" cellspacing="3">
                    <tr>
                        <td valign="top">
                            Total users:
                        </td>
                        <td valign="top" style="text-align: right;">
                            <b>
                                <asp:Literal ID="lit_users" runat="server"></asp:Literal></b>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Total groups:
                        </td>
                        <td valign="top" style="text-align: right;">
                            <b>
                                <asp:Literal ID="lit_groups" runat="server"></asp:Literal></b>
                        </td>
                    </tr>
                </table>
                <hr />
                <asp:HyperLink ID="HyperLink2" runat="server"  NavigateUrl ="~/Account/Admin/EditMonitoredSites.aspx"> >> Manage domains</asp:HyperLink><br />
                <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl ="~/Account/Admin/ManageUsers.aspx"> >> Manage users</asp:HyperLink><br />
                <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl ="~/Account/Admin/ManageGroupsStructure.aspx"> >> Manage groups</asp:HyperLink><br />
                <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl ="~/Account/Admin/ViewStat.aspx"> >> View overall statistics</asp:HyperLink>
                <br />
                <br />                <br />


                </asp:Panel>

                <div class="blueHeader clearFix">
                    My profile
                    <div class="clear">
                    </div>
                </div>
                <table cellpadding="3" cellspacing="3">
                    <tr>
                        <td valign="top">
                            Name
                        </td>
                        <td valign="top" style="text-align: right;">
                            <b>
                                <asp:Literal ID="lit_name" runat="server"></asp:Literal></b>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            E-mail
                        </td>
                        <td valign="top" style="text-align: right;">
                            <b>
                                <asp:Literal ID="lit_email" runat="server"></asp:Literal></b>
                        </td>
                    </tr>
                </table>
                <hr />
                <asp:HyperLink ID="HyperLink1" runat="server"> >> Edit profile</asp:HyperLink><br />
                <asp:HyperLink ID="hl_sites" runat="server"> >> Manage monitored sites</asp:HyperLink>                <br />
                <asp:HyperLink ID="hl_message" runat="server"> >> Messages</asp:HyperLink><br />
                <br />
                <br />
                <div class="blueHeader clearFix">
                    Affiliation
                    <div class="clear">
                    </div>
                </div>
                ... participation in science groups will be here
            </td>
        </tr>
    </table>
</asp:Content>

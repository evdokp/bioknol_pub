﻿Imports BioWS.BLL.Extensions

Public Class ManageSites
    Inherits Page

    Private _curProfilePersonId As Integer?
    Public ReadOnly Property CurProfilePersonId As Integer
        Get
            If _curProfilePersonId Is Nothing Then
                _curProfilePersonId = Me.GetProfilePersonID()
            End If
            Return _curProfilePersonId
        End Get
    End Property



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim pers As New BLL.Person(Me.GetProfilePersonID)
        Me.SetTitlesToGrandMaster("Manage sites", pers.PageHeader)
    End Sub

    Protected Sub lnkb_action_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim vId = sender.GetCommandArguement(Of Integer)()
        Dim ta As New MonitoredSitesDataSetTableAdapters.MonitoredSitesUserExclusionsTableAdapter
        Dim hasExclusion As Boolean = ta.GetCountByBothParams(vId, CurProfilePersonId) > 0
        If hasExclusion Then
            ta.DeleteQuery(vId, CurProfilePersonId)
        Else
            ta.Insert(vId, CurProfilePersonId)
        End If
        gv_sites.DataBind()
    End Sub

    Protected Sub gv_sites_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gv_sites.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim drw = DirectCast(e.Row.DataItem, DataRowView)
            Dim datarow = drw.Row
            Dim msRow = DirectCast(datarow, MonitoredSitesDataSet.MonitoredSitesRow)


            Dim ta As New MonitoredSitesDataSetTableAdapters.MonitoredSitesUserExclusionsTableAdapter
            Dim hasExclusion As Boolean = ta.GetCountByBothParams(msRow.ID, CurProfilePersonId) > 0
            Dim lnkbAction As LinkButton = e.Row.Cells(2).FindControl("lnkb_action")
            lnkbAction.Font.Bold = hasExclusion
            lnkbAction.Text = IIf(hasExclusion, "Include", "Exclude")
        End If
    End Sub
End Class
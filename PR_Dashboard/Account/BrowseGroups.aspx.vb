﻿Imports BioWS.BLL.Extensions

Public Class BrowseGroups
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "Browse groups"
        CType(Master.Master.FindControl("page_header"), System.Web.UI.HtmlControls.HtmlGenericControl).Visible = False

        hfPageOpenDateTime.Value = Now.ToString

    End Sub

    Private Structure GroupInfo
        Dim Group_ID As Integer
        Dim Title As String
        Dim Description As String
        Dim ParticipantsCount As Integer
        Dim PaddingLeft As String
        Dim MESH As Object
        Dim AllowEdit As Integer
        Dim ShowParticipation As Integer
        Dim DirectParticipant As Integer
        Dim Image50 As String
        Dim Avatar As String
    End Structure


    <System.Web.Services.WebMethod()> _
    Public Shared Function GetMyGroups() As String
        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)

        Dim db As New DataClassesPersonsDataContext

        Dim tt = From gr In db.v_GroupsSorted1s
                 Where gr.AddedByPersonID = p.Person_ID
                 Order By gr.Title
                 Select gr.Group_ID, gr.Title, gr.Description,
                     gr.Image50,
                     Avatar = IIf(gr.Image50.Length < 12, BLL.Common.GetGravatar("test@example.com", 50), "https://bioknolstorage.blob.core.windows.net/userimages/" & gr.AvatarGUID & "_50." & gr.AvatarExtension),
        gr.ParticipantsCount, PaddingLeft = "padding-left:0px;",
                   MESH = Nothing

        Dim ttl = tt.ToList()
        Dim reslst As New List(Of GroupInfo)

        For Each a In ttl
            Dim gi As New GroupInfo() With {.Group_ID = a.Group_ID,
                                          .Title = a.Title,
                                          .Description = a.Description,
                                          .ParticipantsCount = a.ParticipantsCount,
                                          .PaddingLeft = a.PaddingLeft,
                                        .AllowEdit = 1,
                                        .Image50 = a.Image50,
                                          .Avatar = a.Avatar,
                                            .ShowParticipation = 0, .DirectParticipant = 0,
                                          .MESH = (From m In db.MeSHDiff_OneGroupToAll(6, a.Group_ID)
                                                   Select m.MeSH_ID, m.Heading).ToList}
            reslst.Add(gi)
        Next
        Return reslst.ToJSON
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetGroupsPart() As String
        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)

        Dim db As New DataClassesPersonsDataContext

        Dim tt = From gr In db.GetGroupsByPersonParticipation(p.Person_ID)
                 Select gr.Group_ID, gr.Title, gr.Description, gr.ParticipantsCount, gr.Image50,
                     Avatar = IIf(gr.Image50.Length < 12, BLL.Common.GetGravatar("test@example.com", 50), "https://bioknolstorage.blob.core.windows.net/userimages/" & gr.AvatarGUID & "_50." & gr.AvatarExtension),
                    PaddingLeft = String.Format("padding-left:{0}px;", gr.Distance * 10),
                   MESH = Nothing

        Dim ttl = tt.ToList()
        Dim reslst As New List(Of GroupInfo)

        For Each a In ttl
            Dim gi As New GroupInfo() With {.Group_ID = a.Group_ID,
                                          .Title = a.Title,
                                          .Description = a.Description,
                                            .AllowEdit = 0,
                                          .ParticipantsCount = a.ParticipantsCount, .Image50 = a.Image50,
                                               .ShowParticipation = 1, .DirectParticipant = IIf(BLL.Group.IsPersonDirectParticipant(a.Group_ID, p.Person_ID), 1, 0),
                                          .PaddingLeft = a.PaddingLeft,
                                          .MESH = (From m In db.MeSHDiff_OneGroupToAll(6, a.Group_ID) _
                                            Select m.MeSH_ID, m.Heading).ToList}
            reslst.Add(gi)
        Next

        Return reslst.ToJSON

    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetSearch(ByVal condition As String,
                                     ByVal EarlierThen As String,
                                    ByVal LastIndex As Integer) As String

        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)

        Dim db As New DataClassesPersonsDataContext

        Dim tt = From gr In db.GetGroupsByText(10, EarlierThen, LastIndex, BLL.SearchHelper.Text_CONTAINS_Arg2_And(condition))
                Select gr.Group_ID, gr.Title, gr.Description, gr.ParticipantsCount, gr.Image50, PaddingLeft = "padding-left:0px;", _
                   MESH = Nothing

        Dim ttl = tt.ToList()
        Dim reslst As New List(Of GroupInfo)

        For Each a In ttl
            Dim gi As New GroupInfo() With {.Group_ID = a.Group_ID,
                                          .Title = a.Title,
                                          .Description = a.Description,
                                            .AllowEdit = 0,
                                          .ParticipantsCount = a.ParticipantsCount,
                                               .ShowParticipation = 0, .DirectParticipant = 0,
                                          .PaddingLeft = a.PaddingLeft,
                                            .Image50 = a.Image50,
                                          .MESH = (From m In db.MeSHDiff_OneGroupToAll(6, a.Group_ID) _
                                            Select m.MeSH_ID, m.Heading).ToList}
            reslst.Add(gi)
        Next

        Return reslst.ToJSON






    End Function


End Class
﻿Public Class MyPage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CType(Master.Master.FindControl("lit_pagetitle"), Literal).Text = "My page"
        Dim _profile As New ProfileCommon
        _profile = _profile.GetProfile(User.Identity.Name)
        Dim pers As New BLL.Person(_profile.Person_ID)
        CType(Master.Master.FindControl("lit_curobj"), Literal).Text = pers.PageHeader


        Dim p As New BLL.Person(CType(Request.QueryString("Person_ID"), Integer))
        Me.lit_views.Text = p.TotalViewsCount
        Me.lit_art.Text = p.TotalArticlesCount
        Me.lit_period.Text = p.ViewPeriodDays
        Me.lit_art_avg.Text = Math.Round(p.TotalArticlesCount / p.ViewPeriodDays, 2)
        Me.hl_history.NavigateUrl = "~\Account\ViewsHistory.aspx?Person_ID=" & Request.QueryString("Person_ID")
        Me.hl_stat.NavigateUrl = "~\Account\MyStatistics.aspx?Person_ID=" & Request.QueryString("Person_ID")
        Me.hl_requests.NavigateUrl = "~\Account\MyRequests.aspx?Person_ID=" & Request.QueryString("Person_ID")
        Me.lit_requests.Text = p.MyRequestCount_Pending + p.MyRequestCount_Declined
        Me.lit_other_requests.Text = p.UnprocessedOtherPeopleRequestsCount
        Me.lit_email.Text = p.UserName
        Me.lit_name.Text = p.LastName & " " & p.FirstName & " " & p.Patronimic
        Me.hl_message.NavigateUrl = "~\Account\ViewMessages.aspx?Person_ID=" & Request.QueryString("Person_ID")
        Me.hl_avail_art.NavigateUrl = "~\Account\ViewAvailableArticles.aspx?Person_ID=" & Request.QueryString("Person_ID")
        Me.lit_art_byme.Text = p.UploadedByMeArticlesCount
        Me.lit_art_notme.Text = p.UploadedByNOTMeArticlesCount
        Me.lit_art_myreq.Text = p.MyRequestCount_Accepted
        Me.hl_sites.NavigateUrl = "~\Account\ManageSites.aspx?Person_ID=" & Request.QueryString("Person_ID")
        Me.hl_other_requests.NavigateUrl = "~\Account\OtherPeopleRequests.aspx?Person_ID=" & Request.QueryString("Person_ID")

        If Roles.IsUserInRole(p.UserName, "Admin") = True Then
            Me.pnl_admin.Visible = True
        Else
            Me.pnl_admin.Visible = False
        End If

    End Sub

End Class
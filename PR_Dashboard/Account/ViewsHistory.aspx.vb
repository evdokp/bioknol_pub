﻿Imports BioWS.BLL.Extensions

Public Class ViewsHistory
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim pers As New BLL.Person(Me.QSInt32("PersonID"))
        Me.SetTitlesToGrandMaster(pers.PageHeader, "Activity feed")
        hfPageOpenDateTime.Value = Now.ToString
        hfPersonID.Value = Me.QS("PersonID")
        btnTimeLine.PostBackUrl = "~\Account\ViewTimeLine.aspx?PersonID=" & Me.QS("PersonID")
    End Sub



    <System.Web.Services.WebMethod()> _
    Public Shared Function GetFeed(ByVal PersonID As Integer,
                                   ByVal EarlierThen As String,
                                    ByVal LastIndex As Integer) As String


        Dim dt As Date
        DateTime.TryParse(EarlierThen, dt)

        Dim db = New DataClassesFeedDataContext


        Dim tt = From c In db.GetActivityFeed(20, EarlierThen, LastIndex, PersonID)
                Select vZero = 0, vHTML = BLL.Feed.FeedHelper.RenderAction(c.Action, c.ActionTime, c.ActionType)


        Return tt.ToList().ToJSON

    End Function
End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master.Master"
    CodeBehind="MyStatistics.aspx.vb" Inherits="BioWS.MyStatistics" MaintainScrollPositionOnPostback ="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table width="100%">
        <tr>
            <td style="width: 25%; vertical-align: top;">
                <ul>
                    <li>View statistics</li>
                    <li>
                        <asp:HyperLink ID="hl_sites" runat="server">Manage monitored sites</asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="hl_messages" runat="server">View messages</asp:HyperLink>
                    </li>
                </ul>
            </td>
            <td style="width: 75%; vertical-align: top;">
                <fieldset class="register">
                    <legend>Statistics</legend>
                    <asp:MultiView ID="mv_charts" runat="server">
                        <asp:View ID="v_charts" runat="server">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    from
                                    <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                                    to
                                    <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
                                    <asp:Button ID="btnRefresh" runat="server" Text="Refresh" />
                                    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}"
                                        SelectMethod="GetViewsCountByPersonIDbyDatebyTwoDates" TypeName="BioWS.ChartDataSetTableAdapters.DataTable1TableAdapter">
                                        <SelectParameters>
                                            <asp:QueryStringParameter Name="Person_ID" QueryStringField="Person_ID" Type="Int32" />
                                            <asp:Parameter Name="StartDate" Type="DateTime" />
                                            <asp:Parameter Name="EndDate" Type="DateTime" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                    <asp:Chart ID="Chart1" runat="server" DataSourceID="ObjectDataSource2" Width="660px">
                                        <Series>
                                            <asp:Series ChartType="Spline" Name="Series1" XValueMember="vDate" YValueMembers="vCount"
                                                BorderWidth="3">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1">
                                                <AxisY>
                                                    <MajorGrid LineColor="LightGray" />
                                                </AxisY>
                                                <AxisX>
                                                    <MajorGrid LineColor="LightGray" />
                                                </AxisX>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                        <Titles>
                                            <asp:Title Name="Title1" Text="Views count by date" BackColor="#9eb8d3" BackGradientStyle="LeftRight"
                                                BorderColor="Silver">
                                            </asp:Title>
                                        </Titles>
                                    </asp:Chart>
                                    <br />
                                    <asp:Chart ID="Chart2" runat="server" Width="660px">
                                        <Titles>
                                            <asp:Title Name="Title1" Text="Top 20 sites by views count" BackColor="#9eb8d3" BackGradientStyle="LeftRight"
                                                BorderColor="Silver">
                                            </asp:Title>
                                        </Titles>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1">
                                                <AxisY>
                                                    <MajorGrid LineColor="LightGray" />
                                                </AxisY>
                                                <AxisX>
                                                    <MajorGrid LineColor="LightGray" />
                                                </AxisX>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                        <Series>
                                            <asp:Series Name="Serie1" ChartArea="ChartArea1">
                                            </asp:Series>
                                        </Series>
                                    </asp:Chart>
                                    <br />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:View>
                        <asp:View ID="v_nocharts" runat="server">
                            There is no data to display.
                        </asp:View>
                    </asp:MultiView>
                </fieldset>
                <fieldset class="register">
                    <legend>Views log</legend>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                    
                    
                
                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" DeleteMethod="Delete"
        InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByPersonID"
        TypeName="BioWS.ViewsLogDataSetTableAdapters.ViewsLogTableAdapter" UpdateMethod="Update">
        
        
        <SelectParameters>
            <asp:QueryStringParameter Name="Person_ID" QueryStringField="Person_ID" Type="Int32" />
        </SelectParameters>
        
    </asp:ObjectDataSource>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="ObjectDataSource1" Width ="100%">
        <Columns>
        
            <asp:TemplateField HeaderText="Date" SortExpression="TimeStamp">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("TimeStamp", "{0:d}") %>'></asp:Label>
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Time" SortExpression="TimeStamp">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("TimeStamp", "{0:T}") %>'></asp:Label>
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField HeaderText="URL" SortExpression="URL">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("URL") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("URL") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="HTML" SortExpression="FileName">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" Text="View saved HTML" NavigateUrl='<%# "~\SavedHTML\" & Eval("FileName") & ".html" %>'
                        Target="_blank" />
                </ItemTemplate>
            </asp:TemplateField>
        
        </Columns>
        <EmptyDataTemplate>
        There is no data to display. 
        </EmptyDataTemplate>
    </asp:GridView>
                
                </ContentTemplate>
                    </asp:UpdatePanel>
                
                
                
                </fieldset>
                
            </td>
        </tr>
    </table>
</asp:Content>

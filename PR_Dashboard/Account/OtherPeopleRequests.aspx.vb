﻿Imports BioWS.BLL.Extensions

Public Class OtherPeopleRequests
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim p As New BLL.Person(Me.GetProfilePersonID())
        Me.SetTitlesToGrandMaster("Other people requests", p.PageHeader)
    End Sub

    Protected Sub gv_requests_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gv_requests.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim r_row As ArticlesDataSet.ArticleRequestsRow
            r_row = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row, ArticlesDataSet.ArticleRequestsRow)
            Dim Hyperlink1 As HyperLink
            Hyperlink1 = e.Row.Cells(2).FindControl("Hyperlink1")
            Dim hl_pubmed As HyperLink
            hl_pubmed = e.Row.Cells(3).FindControl("hl_pubmed")
            Dim hl_doi As HyperLink
            hl_doi = e.Row.Cells(3).FindControl("hl_doi")


            Dim _pr As New ProfileCommon
            _pr = _pr.GetProfile(User.Identity.Name)

            Dim img_domain As Image = e.Row.Cells(3).FindControl("img_domain")
            'If BLL.Common.GetSiteID(r_row.URL) = 0 Then
            '    img_domain.Visible = True
            '    img_domain.ToolTip = "URL is not in domain list"
            'Else
            '    img_domain.Visible = False
            'End If
            img_domain.Visible = False

            If r_row.IsPubMed_IDNull = True Then
                Hyperlink1.Text = HttpUtility.HtmlEncode(r_row.Title)
                Hyperlink1.NavigateUrl = r_row.URL
                hl_pubmed.Visible = False
            Else
                Dim ta As New ArticlesPubmedDataSetTableAdapters.ArticlesPubmedTableAdapter
                Dim t = ta.GetDataByPMID(r_row.PubMed_ID)
                If t.Count > 0 Then
                    Dim r = t.Item(0)
                    Hyperlink1.Text = r.Title
                    Hyperlink1.NavigateUrl = "~\ViewInfoPM.aspx?Pubmed_ID=" & r_row.PubMed_ID & "&UserID=" & _pr.Person_ID
                    hl_pubmed.NavigateUrl = "http://www.ncbi.nlm.nih.gov/pubmed/" & r_row.PubMed_ID
                    hl_pubmed.Visible = True
                    hl_pubmed.ToolTip = "PMID: " & r_row.PubMed_ID
                End If
            End If
            If r_row.IsDOINull = True Then
                hl_doi.Visible = False
            Else
                If r_row.DOI = String.Empty Then
                    hl_doi.Visible = False
                Else
                    hl_doi.Visible = True
                    hl_doi.NavigateUrl = "http://dx.doi.org/" & r_row.DOI
                End If
            End If


        End If
    End Sub

    Protected Sub lnkb_upload_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim request_id As Integer = CType(CType(sender, LinkButton).CommandArgument, Integer)
        Me.Response.Redirect("~\UploadArticle.aspx?Request_ID=" & request_id, False)
    End Sub
    Protected Sub lnkb_notarticle_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim request_id As Integer = CType(CType(sender, LinkButton).CommandArgument, Integer)
        Dim p As New BLL.Person(User.Identity.Name)
        p.DeclineRequest(request_id, "URL indicated in request is not URL of an article", Page)
        Me.gv_requests.DataBind()
    End Sub
    Protected Sub lnkb_duplicate_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim request_id As Integer = CType(CType(sender, LinkButton).CommandArgument, Integer)
        Dim p As New BLL.Person(User.Identity.Name)
        p.DeclineRequest(request_id, "this request duplicates another request", Page)
        Me.gv_requests.DataBind()
    End Sub
    Protected Sub lnkb_decline_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim ca As String = CType(CType(sender, LinkButton).CommandArgument, String)
        'Dim ca As String = sender.GetCommandArguement(Of String)()
        Dim ca_arr As String() = ca.Split(";")
        Dim request_id = ca_arr(0).ToInt32()
        Dim rowindex = ca_arr(1).ToInt32()
        Dim txt_reason As TextBox = gv_requests.Rows(rowindex).FindControl("txt_reason")
        Dim p As New BLL.Person(User.Identity.Name)
        p.DeclineRequest(request_id, txt_reason.Text, Page)
        gv_requests.DataBind()
    End Sub

End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Group.Master"
    CodeBehind="ViewGroup.aspx.vb" Inherits="BioWS.ViewGroup" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCloudControl" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp1" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {



            $("#ddlStat").change(function () {
                ProcessStatisticsChange();
            });

            $("#h3MESH i").click(function () {
                $("#divMESH").toggle(300);
                $("#h3MESH span b").toggle(100);
                var img = $($('#h3MESH img'));
                ToggleImage(img);
            });

            $("#hStat i").click(function () {
                $("#divStat").toggle(300);
                $("#hStat .hunderline_block").toggle(100);
                var img = $($('#imgStat'));
                ToggleImage(img);
            });

            $("#h3about").click(function () {
                $("#divAbout").toggle(300);
                var img = $($('#h3about img'));
                ToggleImage(img);
            });
            $("#h3participants").click(function () {
                $("#divParticipants").toggle(300);
                var img = $($('#h3participants img'));
                ToggleImage(img);
            });


        });

        function ToggleImage(img) {
            if (img.attr('src') == '../Images/minus.gif') {
                img.attr('src', '../Images/plus.gif');
            } else {
                img.attr('src', '../Images/minus.gif');
            }
        }

 


    </script>
    <h5 id="h3about" style="margin-top: 7px;">
        <span>
            <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/minus.gif" />
            About group </span>
    </h5>
    <div id="divAbout">
        <table width="100%">
            <tr>
                <td valign="top" style="width: 50%">
                    <h4>
                        Group is administered by</h4>
                    <asp:HyperLink ID="hl_admin" runat="server"></asp:HyperLink>
                    <span id="sp_admin" runat="server">(it's you, you may
                        <asp:HyperLink ID="hl_edit" runat="server" Text="edit" />
                        this group) </span>
                    <br />
                    <br />
                    <asp:Panel ID="pnl_parent" runat="server">
                        <h4>
                            Parent group</h4>
                        <asp:SqlDataSource ID="sqlParentGroup" runat="server" ConnectionString="<%$ ConnectionStrings:bio_dataConnectionString %>"
                            SelectCommand="SELECT [Group_ID], [ParentGroup_ID], [Title], [ParticipantsCount],[Image50] FROM [v_GroupsParticipantsCount] WHERE ([Group_ID] = @Group_ID)">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfParentGroupID" Name="Group_ID" PropertyName="Value"
                                    Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:HiddenField ID="hfParentGroupID" runat="server" />
                        <dx:ASPxGridView ID="gvParentGroup" runat="server" AutoGenerateColumns="False" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                            CssPostfix="DevEx" DataSourceID="sqlParentGroup" KeyFieldName="Group_ID" Width="100%">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Group title" ShowInCustomizationForm="True" VisibleIndex="0">
                                    <DataItemTemplate>
                                        <asp:Image ID="Image5" runat="server" ImageUrl='<%# "~\UserImages\" & Eval("Image50")  %>'
                                            Style="float: left; margin-right: 10px;" />
                                        <asp:HyperLink ID="hlGroup" runat="server" NavigateUrl='<%# "~\Account\ViewGroup.aspx?GroupID=" & Eval("Group_ID") %>'
                                            Text='<%# Eval("Title") %>' />
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Participants count" FieldName="ParticipantsCount"
                                    VisibleIndex="1" Width="60px">
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                                </LoadingPanelOnStatusBar>
                                <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                                </LoadingPanel>
                            </Images>
                            <ImagesFilterControl>
                                <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                                </LoadingPanel>
                            </ImagesFilterControl>
                            <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                </Header>
                                <LoadingPanel ImageSpacing="5px">
                                </LoadingPanel>
                            </Styles>
                            <StylesEditors ButtonEditCellSpacing="0">
                                <ProgressBar Height="21px">
                                </ProgressBar>
                            </StylesEditors>
                        </dx:ASPxGridView>
                        <br />
                        <br />
                    </asp:Panel>
                    <asp:Panel ID="pnl_child" runat="server">
                        <h4>
                            Child groups</h4>
                        <asp:SqlDataSource ID="sqlChildGroups" runat="server" ConnectionString="<%$ ConnectionStrings:bio_dataConnectionString %>"
                            SelectCommand="SELECT [Group_ID], [ParentGroup_ID], [Title], [ParticipantsCount],[Image50] FROM [v_GroupsParticipantsCount] WHERE ([ParentGroup_ID] = @ParentGroup_ID) ORDER BY [ParticipantsCount] DESC">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="ParentGroup_ID" QueryStringField="GroupID" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <dx:ASPxGridView ID="gvChildGroups" runat="server" AutoGenerateColumns="False" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                            CssPostfix="DevEx" DataSourceID="sqlChildGroups" KeyFieldName="Group_ID" Width="100%">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Group title" ShowInCustomizationForm="True" VisibleIndex="0">
                                    <DataItemTemplate>
                                        <asp:Image ID="Image5" runat="server" ImageUrl='<%# "~\UserImages\" & Eval("Image50")  %>'
                                            Style="float: left; margin-right: 10px;" />
                                        <asp:HyperLink ID="hlGroup" runat="server" NavigateUrl='<%# "~\Account\ViewGroup.aspx?GroupID=" & Eval("Group_ID") %>'
                                            Text='<%# Eval("Title") %>' />
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Participants count" FieldName="ParticipantsCount"
                                    VisibleIndex="1" Width="60px">
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                                </LoadingPanelOnStatusBar>
                                <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                                </LoadingPanel>
                            </Images>
                            <ImagesFilterControl>
                                <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                                </LoadingPanel>
                            </ImagesFilterControl>
                            <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                </Header>
                                <LoadingPanel ImageSpacing="5px">
                                </LoadingPanel>
                            </Styles>
                            <StylesEditors ButtonEditCellSpacing="0">
                                <ProgressBar Height="21px">
                                </ProgressBar>
                            </StylesEditors>
                        </dx:ASPxGridView>
                        <br />
                        <br />
                    </asp:Panel>
                </td>
                <td valign="top" style="width: 50%">
                    <asp:Literal ID="lit_desc" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    </div>
    <h5 id="h3MESH">
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td valign="middle">
                    <span><i style="font-style: normal;">
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/minus.gif" />
                        Area of expertise </i></span>
                </td>
                <td valign="middle">
                    <dx:ASPxTrackBar ID="ASPxTrackBar1" runat="server" Step="10" Height="20" BackColor="White"
                        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Position="10"
                        PositionStart="10" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                        <ClientSideEvents PositionChanged="function(s, e) {
    cpMESH.PerformCallback();
}" />
                    </dx:ASPxTrackBar>
                </td>
            </tr>
        </table>
    </h5>
    <div id="divMESH">
        <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" ClientInstanceName="cpMESH"
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
            <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
            </LoadingPanelImage>
            <LoadingPanelStyle ImageSpacing="5px">
            </LoadingPanelStyle>
            <PanelCollection>
                <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
                    <asp:ObjectDataSource ID="ods_MESH" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetDataByGroupID" TypeName="BioWS.ArticlesPubmedDataSetTableAdapters.MeSHDiff_OnePersonToAllTableAdapter">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ASPxTrackBar1" DefaultValue="10" Name="TopN" PropertyName="Position"
                                Type="Int32" />
                            <asp:QueryStringParameter Name="GroupID" QueryStringField="GroupID" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <dx:ASPxCloudControl ID="ASPxCloudControl1" runat="server" ClientIDMode="AutoID"
                        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" DataSourceID="ods_mesh"
                        MaxColor="#1B3F91" MinColor="#A9B9DD" TextField="Heading" ValueField="vDiff"
                        ValueColor="#1B3F91">
                        <RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                            <dx:RankProperties></dx:RankProperties>
                        </RankProperties>
                        <DisabledStyle ForeColor="#B1B1B8">
                        </DisabledStyle>
                    </dx:ASPxCloudControl>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
    </div>
    <h5 id="h3participants" style="margin-top: 2px; padding-top: 5px;">
        <span>
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/minus.gif" />
            Participants and followers </span>
    </h5>
    <div id="divParticipants">
        <table width="100%">
            <tr>
                <td valign="top" style="width: 50%">
                    <asp:ObjectDataSource ID="ods_participants" runat="server" DeleteMethod="Delete"
                        InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByGroupID"
                        TypeName="BioWS.PersonsDataSetTableAdapters.PersonsTableAdapter" UpdateMethod="Update">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="Group_ID" QueryStringField="GroupID" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <dx:ASPxGridView ID="gvParticipants" runat="server" AutoGenerateColumns="False" Width="100%"
                        DataSourceID="ods_participants" KeyFieldName="Person_ID" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                        CssPostfix="DevEx">
                        <Columns>
                            <dx:GridViewDataDateColumn VisibleIndex="0" Caption="Participants:">
                                <PropertiesDateEdit Spacing="0">
                                </PropertiesDateEdit>
                                <DataItemTemplate>
                                    <asp:Image ID="Image5" runat="server" ImageUrl='<%# "~\UserImages\" & Eval("Image50")  %>'
                                        Style="float: left; margin-right: 10px;" />
                                    <asp:HyperLink ID="hlPerson" runat="server" NavigateUrl='<%# "~\Account\ViewPerson.aspx?PersonID=" & Eval("Person_ID") %>'
                                        Text='<%# Eval("UserName") %>' />
                                    <br />
                                    <span style="color: Gray">
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("LastName") & " " & Eval("FirstName")& " " & Eval("Patronimic") %>' />
                                    </span>
                                </DataItemTemplate>
                            </dx:GridViewDataDateColumn>
                        </Columns>
                        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                            <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                            </LoadingPanelOnStatusBar>
                            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                            </LoadingPanel>
                        </Images>
                        <ImagesFilterControl>
                            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                            </LoadingPanel>
                        </ImagesFilterControl>
                        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                            </Header>
                            <LoadingPanel ImageSpacing="5px">
                            </LoadingPanel>
                        </Styles>
                        <StylesEditors ButtonEditCellSpacing="0">
                            <ProgressBar Height="21px">
                            </ProgressBar>
                        </StylesEditors>
                    </dx:ASPxGridView>
                    <asp:Panel ID="pnl_part" runat="server">
                        <h4>
                            Your participation</h4>
                        <asp:ObjectDataSource ID="ods_part" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                            OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByTwoIDs"
                            TypeName="BioWS.PersonsDataSetTableAdapters.LinksGroupsToPersonsTableAdapter"
                            UpdateMethod="Update">
                            <DeleteParameters>
                                <asp:Parameter Name="Original_Link_ID" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="Group_ID" Type="Int32" />
                                <asp:Parameter Name="Person_ID" Type="Int32" />
                                <asp:Parameter Name="FromDate" Type="DateTime" />
                                <asp:Parameter Name="ToDate" Type="DateTime" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:QueryStringParameter Name="Group_ID" QueryStringField="GroupID" Type="Int32" />
                                <asp:ProfileParameter Name="Person_ID" PropertyName="Person_ID" Type="Int32" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="Group_ID" Type="Int32" />
                                <asp:Parameter Name="Person_ID" Type="Int32" />
                                <asp:Parameter Name="FromDate" Type="DateTime" />
                                <asp:Parameter Name="ToDate" Type="DateTime" />
                                <asp:Parameter Name="Original_Link_ID" Type="Int32" />
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                        <asp:GridView ID="gv_part" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                            GridLines="None" DataKeyNames="Link_ID" DataSourceID="ods_part">
                            <Columns>
                                <asp:TemplateField HeaderText="Group_ID" SortExpression="Group_ID">
                                    <ItemTemplate>
                                        since <span style="color: Green;">
                                            <asp:Literal ID="lit_FromDate" runat="server" Text='<%# Bind("FromDate","{0:d}") %>' />
                                        </span>till <span style="color: Green;">
                                            <asp:Literal ID="lit_ToDate" runat="server" />
                                        </span>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Link_ID" InsertVisible="False" SortExpression="Link_ID">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imb_delete" runat="server" CommandName="Delete" ToolTip="Delete"
                                            ImageUrl="~/Images/small_delete.png" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                You are not member of the group
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <br />
                        <dx:ASPxButton ID="btn_Join" AutoPostBack="False" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                            CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                        </dx:ASPxButton>
                        <dx:ASPxPopupControl ID="ASPxPopupControl1" runat="server" ClientInstanceName="pcAddEntry"
                            ClientIDMode="AutoID" CloseAction="CloseButton" PopupHorizontalAlign="WindowCenter"
                            PopupVerticalAlign="WindowCenter" CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css"
                            HeaderText="Add participation entry" CssPostfix="Office2010Silver" EnableHotTrack="False"
                            PopupElementID="btn_Join" Width="340px" SpriteCssFilePath="~/App_Themes/Office2010Silver/{0}/sprite.css"
                            Modal="True" ShowHeader="True" ShowFooter="False">
                            <LoadingPanelImage Url="~/App_Themes/Office2010Silver/Web/Loading.gif">
                            </LoadingPanelImage>
                            <LoadingPanelStyle ImageSpacing="5px">
                            </LoadingPanelStyle>
                            <ContentCollection>
                                <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                                    <asp:MultiView ID="mv1" runat="server">
                                        <asp:View ID="v1" runat="server">
                                            <table width="100%">
                                                <tr>
                                                    <td valign="top" style="width: 80px;">
                                                        start date
                                                    </td>
                                                    <td valign="top">
                                                        <asp:TextBox ID="txt_start" runat="server" Width="100px"></asp:TextBox>
                                                        <asp:MaskedEditExtender ID="mee1" runat="server" TargetControlID="txt_start" Mask="99/99/9999"
                                                            MaskType="date" CultureName="ru-Ru" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" />
                                                        <asp:MaskedEditValidator ID="mev1" runat="server" ControlExtender="mee1" ControlToValidate="txt_start" />
                                                        <asp:TextBoxWatermarkExtender ID="tbwme1" runat="server" TargetControlID="txt_start"
                                                            WatermarkText="DD.MM.YYYY" WatermarkCssClass="TextBoxWatermark" />
                                                        <asp:RequiredFieldValidator ID="rfv1" runat="server" ControlToValidate="txt_start"
                                                            ErrorMessage="Start date is required" ValidationGroup="addentr" Display="None"
                                                            CssClass="invalid_msg"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width: 80px;">
                                                        end date
                                                    </td>
                                                    <td valign="top">
                                                        <asp:TextBox ID="txt_end" runat="server" Width="100px"></asp:TextBox>
                                                        <asp:MaskedEditExtender ID="mee2" runat="server" TargetControlID="txt_end" Mask="99/99/9999"
                                                            MaskType="date" CultureName="ru-Ru" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" />
                                                        <asp:MaskedEditValidator ID="mev2" runat="server" ControlExtender="mee2" ControlToValidate="txt_end" />
                                                        <asp:TextBoxWatermarkExtender ID="tbwm2" runat="server" TargetControlID="txt_end"
                                                            WatermarkText="DD.MM.YYYY" WatermarkCssClass="TextBoxWatermark" />
                                                    
                                                        <br />
                                                        <span style="color: Gray; font-size: 10px;">leave blank in case you are adding current
                                                            participation</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width: 80px;">
                                                    </td>
                                                    <td valign="top">
                                                        <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="addentr" CssClass="invalid_msg"
                                                            DisplayMode="SingleParagraph" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width: 80px;">
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Button ID="btnAddPart" runat="server" Text="Add" ValidationGroup="addentr" />
                                                        <input id="btnCancel" type="button" value="Cancel" onclick="closepopup()" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                        <asp:View ID="v2" runat="server">
                                            Participation entry was successfully added.<br />
                                            <br />
                                            <asp:Button ID="btnOneMore" runat="server" Text="One more entry" />
                                            <input id="btnClose" type="button" value="Close" onclick="closepopup()" />
                                        </asp:View>
                                    </asp:MultiView>
                                    <script type="text/javascript">
                                        function closepopup() { pcAddEntry.Hide(); }
                                    
                                    
                                    </script>
                                </dx:PopupControlContentControl>
                            </ContentCollection>
                        </dx:ASPxPopupControl>
                    </asp:Panel>
                </td>
                <td valign="top" style="width: 50%; padding-left: 15px;">
                    <asp:ObjectDataSource ID="odsFollowers" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetDataByFollowedGroupID" TypeName="BioWS.PersonsDataSetTableAdapters.PersonsTableAdapter">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="FollowedGroupID" QueryStringField="GroupID" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" Width="100%"
                        DataSourceID="odsFollowers" KeyFieldName="Person_ID" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                        CssPostfix="DevEx">
                        <Columns>
                            <dx:GridViewDataDateColumn VisibleIndex="0" Caption="Followers:">
                                <PropertiesDateEdit Spacing="0">
                                </PropertiesDateEdit>
                                <DataItemTemplate>
                                    <asp:Image ID="Image5" runat="server" ImageUrl='<%# "~\UserImages\" & Eval("Image50")  %>'
                                        Style="float: left; margin-right: 10px;" />
                                    <asp:HyperLink ID="hlPerson" runat="server" NavigateUrl='<%# "~\Account\ViewPerson.aspx?PersonID=" & Eval("Person_ID") %>'
                                        Text='<%# Eval("UserName") %>' />
                                    <br />
                                    <span style="color: Gray">
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("LastName") & " " & Eval("FirstName")& " " & Eval("Patronimic") %>' />
                                    </span>
                                </DataItemTemplate>
                            </dx:GridViewDataDateColumn>
                        </Columns>
                        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                            <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                            </LoadingPanelOnStatusBar>
                            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                            </LoadingPanel>
                        </Images>
                        <ImagesFilterControl>
                            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                            </LoadingPanel>
                        </ImagesFilterControl>
                        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                            </Header>
                            <LoadingPanel ImageSpacing="5px">
                            </LoadingPanel>
                        </Styles>
                        <StylesEditors ButtonEditCellSpacing="0">
                            <ProgressBar Height="21px">
                            </ProgressBar>
                        </StylesEditors>
                    </dx:ASPxGridView>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

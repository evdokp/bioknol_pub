﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ViewsHistory

    '''<summary>
    '''hfLastIndex control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfLastIndex As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfPageOpenDateTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfPageOpenDateTime As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfInitialTopMessages control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfInitialTopMessages As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfPersonID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfPersonID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''btnTimeLine control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnTimeLine As Global.DevExpress.Web.ASPxEditors.ASPxButton

    '''<summary>
    '''ASPxLoadingPanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLoadingPanel1 As Global.DevExpress.Web.ASPxLoadingPanel.ASPxLoadingPanel

    '''<summary>
    '''btnShowMore control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnShowMore As Global.DevExpress.Web.ASPxEditors.ASPxButton
End Class

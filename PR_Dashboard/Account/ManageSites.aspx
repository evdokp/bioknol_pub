﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Person.Master"
    CodeBehind="ManageSites.aspx.vb" Inherits="BioWS.ManageSites" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 
    <br />
        <table width="100%" cellpadding="5" cellspacing="2">
        <tr>
            <td style="width: 50%;" align="center">
          
            SiteURL
            </td>
            <td style="width: 25%;  border-left: 1px solid grey;" align="center">
                Added Date
            </td>
            <td style="width: 25%; border-left: 1px solid grey;" align="center">
                Action
            </td>
        </tr>
    </table>
        <div class="blueHeader clearFix">
        <div style="float: left;">
        </div>
        <div style="float: right; padding-right: 5px;">
        </div>
        <div class="clear">
        </div>
    </div>

                 
                    <asp:GridView ID="gv_sites" runat="server" 
        AutoGenerateColumns="False" DataKeyNames="ID"
                        DataSourceID="ods_sites" AllowPaging="True" PageSize="50" ShowHeader ="false" Width ="100%" GridLines ="None"  >
                        <Columns>
                            <asp:TemplateField >
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("SiteURL") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width ="50%" />
                            </asp:TemplateField>
                            <asp:TemplateField >
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("AddedDate","{0:d}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width ="25%" HorizontalAlign ="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField >
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkb_action" runat="server" CommandArgument='<%# Bind("ID") %>'
                                        OnClick="lnkb_action_Click"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width ="25%" HorizontalAlign ="Center"  />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>


                       <asp:ObjectDataSource ID="ods_sites" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetAllApproved" TypeName="BioWS.MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter"
                        UpdateMethod="Update"></asp:ObjectDataSource>
                
</asp:Content>

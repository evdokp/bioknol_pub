﻿Public Class ViewWidget2
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim widget As New BLL.Widgets.Widget(Request.QueryString("WidgetID"))

        If Not Page.IsPostBack Then

            CType(Master.Master.FindControl("page_header"), System.Web.UI.HtmlControls.HtmlGenericControl).Visible = False
            CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "Widget"

            litTitle.Text = widget.Title
            litDesc.Text = widget.Description
            litAddedAt.Text = widget.AddedDate.ToString("D")
            litVersion.Text = widget.Version
            hlAddedBy.Text = widget.AddedByPerson.PageHeader
            hlAddedBy.NavigateUrl = widget.AddedByPerson.URL
            If widget.IsInternal Then
                litIsExternal.Text = "This widget is provided by Personal Reference Dashboard"
            Else
                litIsExternal.Text = "This widget is provided by external information system"
            End If

            If widget.AllowMultiple Then
                litAllowMultiple.Text = "This widget is desined to process one or many Pubmed IDs"
            Else
                litAllowMultiple.Text = "This widget is desined to process only single Pubmed ID"
            End If




        End If

        Dim personsTA As New PersonsDataSetTableAdapters.PersonsTableAdapter
        Dim personsT = personsTA.GetDataByWidgetIDisON(widget.Widget_ID)
        If personsT.Count > 0 Then
            gvUsedBy.DataSource = personsT
            gvUsedBy.DataBind()
        Else
            pnlUsedBy.Visible = False
        End If

    End Sub

End Class
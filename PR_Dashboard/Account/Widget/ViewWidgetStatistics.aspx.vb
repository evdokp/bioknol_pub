﻿Imports System.Web.UI.DataVisualization.Charting
Public Class ViewWidgetStatistics
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            CType(Master.Master.FindControl("page_header"), System.Web.UI.HtmlControls.HtmlGenericControl).Visible = False
            CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "Widget"
            Dim widget As New BLL.Widgets.Widget(Request.QueryString("WidgetID"))
            litTitle.Text = widget.Title

            Dim pr As New ProfileCommon
            pr = pr.GetProfile(HttpContext.Current.User.Identity.Name)



            DataBindChart(Today.AddDays(-pr.DefaultPeriodDays), Today)
        End If
    End Sub


    Private Sub SetPeriod1_PeriodChanged(StartDate As Date, EndDate As Date) Handles SetPeriod1.PeriodChanged
        DataBindChart(StartDate, EndDate)
    End Sub


    Protected Sub CreateChart()
        Dim cha As ChartArea = Me.Chart1.ChartAreas("ChartArea1")
        cha.BorderColor = Drawing.Color.LightGray
        cha.BorderWidth = 1
        cha.BorderDashStyle = ChartDashStyle.Solid
        cha.AxisY.MajorGrid.LineColor = Drawing.Color.LightGray
        cha.AxisX.MajorGrid.LineColor = Drawing.Color.LightGray
        cha.AxisY.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha.AxisX.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha.AxisY.LineColor = Drawing.Color.LightGray
        cha.AxisX.LineColor = Drawing.Color.LightGray
        cha.AxisX.IntervalType = DateTimeIntervalType.Days
        cha.AxisX.IsMarginVisible = False



        Dim sPMIDs As Series = Me.Chart1.Series("Series1")
        sPMIDs.ChartArea = "ChartArea1"
        sPMIDs.ChartType = SeriesChartType.Area
        sPMIDs.XValueType = ChartValueType.Date





    End Sub

    Protected Sub DataBindChart(StartDate As Date, EndDate As Date)
        CreateChart()

        Dim s1 As Series = Chart1.Series("Series1")
        s1.Points.Clear()

        Dim logTA As New WidgetsDataSetTableAdapters.WidgetLogTableAdapter
        Dim logT = logTA.GetDataByWidgetIDandTwoDates(Request.QueryString("WidgetID"), StartDate, EndDate)

        Dim curDT As DateTime = StartDate

        While curDT < EndDate
            Dim dp1 As New DataPoint()
            Dim curDatesT = From d In logT
                            Select d Where d.LogDate = curDT

            If curDatesT.Count > 0 Then
                dp1.SetValueXY(curDT, curDatesT.ToList()(0).ViewsCount)
            Else
                dp1.SetValueXY(curDT, 0)
            End If

            s1.Points.Add(dp1)
            curDT = curDT.AddDays(1)
        End While



    End Sub
End Class
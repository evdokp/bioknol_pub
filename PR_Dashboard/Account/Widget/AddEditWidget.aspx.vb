﻿Imports System.Xml
Imports System.Drawing
Public Class AddEditWidget
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        CType(Master.Master.FindControl("page_header"), System.Web.UI.HtmlControls.HtmlGenericControl).Visible = False
        If Page.IsPostBack = False Then
            If String.IsNullOrEmpty(Request.QueryString("WidgetID")) Then
                '! add
                litTitle.Text = "New widget"
                CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "Edit new widget information"
                btnSave.Text = "Add widget"
            Else
                '! edit
                Dim widget As New BLL.Widgets.Widget(Request.QueryString("WidgetID"))
                CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "Edit widget information"
                litTitle.Text = widget.Title
                txtTitle.Text = widget.Title
                txtDesc.Html = widget.Description
                txtVersion.Text = widget.Version
                dtLastRelease.Date = widget.AddedDate
                txtBooleanURL.Text = widget.Info_WS_URL
                txtInfoURL.Text = widget.Data_WS_URL
                chkAllowMultiple.Checked = widget.AllowMultiple
            End If
        End If




    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If String.IsNullOrEmpty(Request.QueryString("WidgetID")) Then
            '! add
            Dim _profile As New ProfileCommon
            _profile = _profile.GetProfile(HttpContext.Current.User.Identity.Name)
            Dim widget As New BLL.Widgets.Widget(txtTitle.Text, txtDesc.Html, txtBooleanURL.Text, txtInfoURL.Text, String.Empty, False, chkAllowMultiple.Checked, _profile.Person_ID, txtVersion.Text)
            Response.Redirect(widget.EditURL)
        Else
            '! edit
            Dim widget As New BLL.Widgets.Widget(Request.QueryString("WidgetID"))
            litTitle.Text = txtTitle.Text
            widget.Title = txtTitle.Text
            widget.Description = txtDesc.Html
            widget.Version = txtVersion.Text
            widget.AddedDate = dtLastRelease.Date
            widget.Info_WS_URL = txtBooleanURL.Text
            widget.Data_WS_URL = txtInfoURL.Text
            widget.AllowMultiple = chkAllowMultiple.Checked
            widget.Save()
            pnlSaved.Visible = True
        End If
    End Sub

    Protected Sub btnTest_Click(sender As Object, e As EventArgs) Handles btnTest.Click
        litBoolResponse.Text = BLL.Widgets.ValidateBooleanResponse(txtBooleanURL.Text, txtTestPMIDs.Text)

        Dim pmids As String = txtTestPMIDs.Text.Replace(",", ";")
        pmids = pmids.Replace(" ", "")

        ASPxSplitter1.Panes(0).ContentUrl = String.Format("../../Widgets/ViewWidget.aspx?PubmedIDs={0}&actionURL={1}",
                                                   pmids, HttpUtility.UrlEncode(txtInfoURL.Text))


    End Sub
End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Widget.master" CodeBehind="ViewWidget.aspx.vb" Inherits="BioWS.ViewWidget2" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

 <h1 style="margin-top:1px; margin-bottom:12px;">
            <asp:Literal ID="litTitle" runat="server"></asp:Literal>
</h1>

<h5><span>About the widget</span></h5>


    <table width="100%" id="tblCrit">
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Description:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litDesc" runat="server" />
            </td>
        </tr>
         <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Pubmed IDs processing mode:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litAllowMultiple" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Provided by:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litIsExternal" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Current version:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litVersion" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Last release date:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litAddedAt" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Added by:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:HyperLink ID="hlAddedBy" runat="server" />
            </td>
        </tr>
    </table>

    <asp:Panel ID="pnlUsedBy" runat="server">
    <h5><span>
    This widget is used by
    </span></h5>


  
    <table width="100%" id="Table1">
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
              
            </td>
            <td valign="top" style="" class="td_field">
       

            
                <dx:ASPxGridView ID="gvUsedBy" runat="server" AutoGenerateColumns="False"  Width="100%"
                   KeyFieldName="Person_ID" 
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                    <Columns>
                        <dx:GridViewDataDateColumn  VisibleIndex="0" Caption="Participants:">
                            <PropertiesDateEdit Spacing="0">
                            </PropertiesDateEdit>
                           <DataItemTemplate>
                                <asp:Image ID="Image5" runat="server" ImageUrl = '<%# "~\UserImages\" & Eval("Image50")  %>' style="float:left; margin-right:10px;"/>



                    <asp:HyperLink ID="hlPerson" runat="server"
                     NavigateUrl='<%# "~\Account\ViewPerson.aspx?PersonID=" & Eval("Person_ID") %>'
                     Text ='<%# Eval("UserName") %>'
                    />
                    <br />
                    <span style="color:Gray">
                        <asp:Literal ID="Literal1" runat="server" Text ='<%# Eval("LastName") & " " & Eval("FirstName")& " " & Eval("Patronimic") %>'/>
                    </span>
                    
                
                </DataItemTemplate>
                        </dx:GridViewDataDateColumn>
                    </Columns>
                    <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                        <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                        </LoadingPanelOnStatusBar>
                        <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                        </LoadingPanel>
                    </Images>
                    <ImagesFilterControl>
                        <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                        </LoadingPanel>
                    </ImagesFilterControl>
                    <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                        </Header>
                        <LoadingPanel ImageSpacing="5px">
                        </LoadingPanel>
                    </Styles>
                    <StylesEditors ButtonEditCellSpacing="0">
                        <ProgressBar Height="21px">
                        </ProgressBar>
                    </StylesEditors>
                </dx:ASPxGridView>

       
            </td>
        </tr>
        </table>
         
    </asp:Panel>
    

</asp:Content>

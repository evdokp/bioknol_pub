﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Widget.master"
    CodeBehind="AddEditWidget.aspx.vb" Inherits="BioWS.AddEditWidget" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
    <%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSplitter" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1 style="margin-top: 1px; margin-bottom: 12px;">
        <asp:Literal ID="litTitle" runat="server"></asp:Literal>
    </h1>
    <h5>
        <span>Edit widget information</span></h5>
    
    <table width="100%">
    <asp:Panel ID="pnlSaved" runat="server" Visible="false">
     
            <tr>
                <td valign="top" style="text-align: right;" class="td_label">
                &nbsp;
                </td>
                <td valign="top" style="" class="td_field">
                    <div class="blockinfo">
                        Widget infromation saved
                    </div>
                </td>
            </tr>
        
    </asp:Panel>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Title:
            </td>
            <td valign="top" style="" class="td_field">
                <dx:ASPxTextBox ID="txtTitle" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                    CssPostfix="DevEx" NullText="Enter title here" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"
                    Width="100%">
                    <ValidationSettings ValidationGroup="edit">
                        <RequiredField IsRequired="True" ErrorText=" " />
                    </ValidationSettings>
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Description:
            </td>
            <td valign="top" style="" class="td_field">
                <dx:ASPxHtmlEditor ID="txtDesc" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                    CssPostfix="DevEx" Width="535px">
                    <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                        <ViewArea>
                            <Border BorderColor="#9DA0AA" />
                        </ViewArea>
                    </Styles>
                    <StylesEditors ButtonEditCellSpacing="0">
                    </StylesEditors>
                    <StylesStatusBar>
                        <StatusBar TabSpacing="0px">
                            <Paddings Padding="0px" />
                        </StatusBar>
                    </StylesStatusBar>
                    <SettingsImageUpload>
                        <ValidationSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png">
                        </ValidationSettings>
                    </SettingsImageUpload>
                    <SettingsValidation ValidationGroup="edit">
                        <RequiredField IsRequired="True" />
                    </SettingsValidation>
                    <SettingsImageSelector>
                        <CommonSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png"></CommonSettings>
                    </SettingsImageSelector>
                    <SettingsDocumentSelector>
                        <CommonSettings AllowedFileExtensions=".rtf, .pdf, .doc, .docx, .odt, .txt, .xls, .xlsx, .ods, .ppt, .pptx, .odp">
                        </CommonSettings>
                    </SettingsDocumentSelector>
                    <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                        <LoadingPanel Url="~/App_Themes/DevEx/HtmlEditor/Loading.gif">
                        </LoadingPanel>
                    </Images>
                    <ImagesFileManager>
                        <FolderContainerNodeLoadingPanel Url="~/App_Themes/DevEx/Web/tvNodeLoading.gif">
                        </FolderContainerNodeLoadingPanel>
                        <LoadingPanel Url="~/App_Themes/DevEx/Web/Loading.gif">
                        </LoadingPanel>
                    </ImagesFileManager>
                </dx:ASPxHtmlEditor>
            </td>
        </tr>
      
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Current version:
            </td>
            <td valign="top" style="" class="td_field">
                <dx:ASPxTextBox ID="txtVersion" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                    CssPostfix="DevEx" NullText="Enter version here" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"
                    Width="100%">
                    <ValidationSettings ValidationGroup="edit">
                        <RequiredField IsRequired="True"  ErrorText=" " />
                    </ValidationSettings>
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Last release date:
            </td>
            <td valign="top" style="" class="td_field">
                <dx:ASPxDateEdit ID="dtLastRelease" runat="server" DisplayFormatString="dd.MM.yyyy"
                    EditFormat="Custom" EditFormatString="dd.MM.yyyy" UseMaskBehavior="True" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                    CssPostfix="DevEx" Spacing="0" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" 
                    NullText="Enter last release date here" Width="200px">
                    <CalendarProperties>
                        <HeaderStyle Spacing="1px" />
                    </CalendarProperties>
                    <ButtonStyle Width="13px">
                    </ButtonStyle>
                    <ValidationSettings ValidationGroup="edit">
                        <RequiredField IsRequired="True"  ErrorText=" "  />
                    </ValidationSettings>
                </dx:ASPxDateEdit>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Boolean URL:
            </td>
            <td valign="top" style="" class="td_field">
                <dx:ASPxTextBox ID="txtBooleanURL" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                    CssPostfix="DevEx" NullText="Enter boolean URL here" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"
                    Width="100%">
                    <ValidationSettings ValidationGroup="edit">
                        <RequiredField IsRequired="True"  ErrorText=" "  />
                    </ValidationSettings>
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Information URL:
            </td>
            <td valign="top" style="" class="td_field">
                <dx:ASPxTextBox ID="txtInfoURL" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                    CssPostfix="DevEx" NullText="Enter information URL here" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"
                    Width="100%">
                    <ValidationSettings ValidationGroup="edit">
                        <RequiredField IsRequired="True"   ErrorText=" " />
                    </ValidationSettings>
                </dx:ASPxTextBox>
            </td>
        </tr>
            <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Allows multiple Pubmed IDs:
            </td>
            <td valign="top" style="" class="td_field">
            
                <dx:ASPxCheckBox ID="chkAllowMultiple" runat="server" CheckState="Unchecked" 
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                    <ValidationSettings ValidationGroup="edit">
                        <RequiredField IsRequired="True"  ErrorText=" "  />
                    </ValidationSettings>
                </dx:ASPxCheckBox>
            
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                &nbsp;
            </td>
            <td valign="top" style="" class="td_field">
                <dx:ASPxButton ID="btnSave" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                    CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" 
                    Text="Save" ValidationGroup="edit">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>

<br />

    <h5>
        <span>Test widget responses</span></h5>
            <table width="100%">
              <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Test Pubmed IDs:
            </td>
            <td valign="top" style="" class="td_field">
                <dx:ASPxTextBox ID="txtTestPMIDs" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"  NullText="Enter test Pubmed IDs here. Comma separated."
                    CssPostfix="DevEx"  SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"
                    Width="100%">
                    <ValidationSettings ValidationGroup = "test">
                        <RequiredField IsRequired="True"   ErrorText=" " />
                    </ValidationSettings>
                </dx:ASPxTextBox>
            </td>
        </tr>


      
           <tr>
            <td valign="top" style="text-align: right;" class="td_label">
        Information response:
            </td>
            <td valign="top" style="" class="td_field">
                 <dx:ASPxButton ID="btnTest" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" ValidationGroup = "test"
                    CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Test">
                </dx:ASPxButton>
               <br />
               <br />
            </td>
        </tr>




             <tr>
            <td valign="top" style="text-align: right;" class="td_label">
        Boolean response:
            </td>
            <td valign="top" style="" class="td_field">
             <asp:Literal ID="litBoolResponse" runat="server">
             <span class="EmptyDataText">Response from boolean URL will be displayed here</span>
             </asp:Literal>
            </td>
        </tr>
        
           <tr>
            <td valign="top" style="text-align: right;" class="td_label">
        Information response:
            </td>
            <td valign="top" style="" class="td_field">
             

                                        <dx:ASPxSplitter ID="ASPxSplitter1" runat="server" ClientIDMode="AutoID" CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css"
            CssPostfix="Office2010Silver"  Width="535px"  Height="550px">
            <panes>
            <dx:SplitterPane ContentUrlIFrameName="f1">
                <ContentCollection>
                    <dx:SplitterContentControl ID="SplitterContentControl2" runat="server" SupportsDisabledAttribute="True">
             <span class="EmptyDataText">Response from information URL will be displayed here</span>
                    </dx:SplitterContentControl>
                </ContentCollection>
            </dx:SplitterPane>
        </panes>
            <styles cssfilepath="~/App_Themes/Office2010Silver/{0}/styles.css" csspostfix="Office2010Silver">
        </styles>
            <images spritecssfilepath="~/App_Themes/Office2010Silver/{0}/sprite.css">
        </images>
        </dx:ASPxSplitter>
            </td>
        </tr>
            

        </table>


</asp:Content>

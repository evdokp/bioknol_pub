﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master.master"
    CodeBehind="ComposeMessage.aspx.vb" Inherits="BioWS.ComposeMessage" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {

            $("#hfLastIndex").val(0);
            $("#btnShowMore").hide();

            if (parseInt($('#hfInitialTopMessages').val()) > 0) {

                $("#btnShowHistory").click(function () {
                    PageMethods.GetMessages(10, $("#hfPageOpenDateTime").val(), $("#hfLastIndex").val(), $("#hfPersonTo").val(), function (response) {
                        $("#messagesList").html("");
                        $("#btnShowMore").show();
                        $("#btnShowHistory").hide();
                        var obj = jQuery.parseJSON(response);
                        ShowResult(obj);
                    });
                });

                $("#btnShowMore").click(function () {
                    PageMethods.GetMessages(10, $("#hfPageOpenDateTime").val(), $("#hfLastIndex").val(), $("#hfPersonTo").val(), function (response) {
                        var obj = jQuery.parseJSON(response);
                        ShowResult(obj);
                    });
                });
            }
        });

        function ShowResult(obj) {
            //  alert(obj.length);
            $("#hfLastIndex").val(parseInt($("#hfLastIndex").val()) + obj.length);
            $("#messagesTemplate").tmpl(obj).appendTo("#messagesList");
            $("h5 span").text('Message history (total messages: ' + $("#hfTotalMessagesCount").val() + ', shown ' + $("#messagesList tr").length + ')');
            var msgtotal = parseInt($("#hfTotalMessagesCount").val());
            var msgnow = parseInt($("#hfLastIndex").val());
            if (msgtotal <= msgnow) {
                $("#btnShowMore").hide();
            } else {
                $("#btnShowMore").show();
            };
        };


        
        
    </script>
    
    <asp:HiddenField ID="hfFromName" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hfPersonFrom" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hfPersonTo" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hfPageOpenDateTime" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfInitialTopMessages" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfTotalMessagesCount" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfLastIndex" runat="server" ClientIDMode="Static" />

    <div style="padding: 15px;">
        <asp:Panel ID="pnlSent" runat="server">
           <table width="100%" cellpadding="3" cellspacing="3">
                <tr>
                    <td valign="top" style="width: 70px; text-align: right;">
                        
                    </td>
                    <td valign="top">
            <div class="blockinfo">
                <asp:Literal ID="litSent" runat="server"></asp:Literal>
                <br />
                <br />
                <dx:ASPxButton ID="btnToMessages" runat="server" Text="Back to messages" 
                    PostBackUrl="MyMessages.aspx" CausesValidation = "False" 
                    ValidationGroup="nevervalidate" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" 
                    CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" 
                    Width="193px">
                </dx:ASPxButton>
            </div>
                    </td>
                </tr>
                </table>


            
        </asp:Panel>
        <asp:Panel ID="pnlReply" runat="server">
            <table width="100%" cellpadding="3" cellspacing="3">
                <tr>
                    <td valign="top" style="width: 70px; text-align: right;">
                        Reply on:
                    </td>
                    <td valign="top">
                        <div style="height: 250px; overflow: scroll; border: 1px solid lightgray; padding: 5px;">
                            <asp:Literal ID="litReplyOn" runat="server"></asp:Literal>
                        </div>
                    </td>
                </tr>
             </table>
        </asp:Panel>

          <asp:Panel ID="pnlTo" runat="server">
         <table width="100%" cellpadding="3" cellspacing="3">
           <tr>
                    <td valign="top" style="width: 70px; text-align: right;">
                        To:
                    </td>
                    <td valign="top">
                        <dx:ASPxTextBox ID="txtReplyTo" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                            CssPostfix="DevEx" ReadOnly="True" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"
                            Width="100%">
                        </dx:ASPxTextBox>
                    </td>
                </tr>
            </table>
                  </asp:Panel>
        <table width="100%" cellpadding="3" cellspacing="3">
            <tr>
                <td valign="top" style="width: 70px; text-align: right;">
                    <asp:Literal ID="litTo" runat="server"></asp:Literal>
                </td>
                <td valign="top">
                    <dx:LinqServerModeDataSource ID="LinqServerModeDataSource1" runat="server" ContextTypeName="BioWS.DataClassesPersonsDataContext"
                        TableName="v_Persons" />
                    <dx:ASPxGridLookup ID="lookupTO" runat="server" DataSourceID="LinqServerModeDataSource1"
                        KeyFieldName="Person_ID" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx"
                        Spacing="0" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Width="100%"
                        AutoGenerateColumns="False" SelectionMode="Multiple" TextFormatString="{0}" AllowUserInput="False"
                        AutoResizeWithContainer="True">
                        <GridViewProperties>
                            <SettingsBehavior AllowSelectByRowClick="True"></SettingsBehavior>
                            <Settings ShowFilterRow="true" />
                        </GridViewProperties>
                        <Columns>
                            <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="20" />
                            <dx:GridViewDataTextColumn VisibleIndex="1" FieldName="CombinedName" CellStyle-HorizontalAlign="Left"
                                Caption="Participant" Width="400">
                                <Settings AutoFilterCondition="Contains" />
                                <DataItemTemplate>
                                    <asp:HyperLink ID="hlPerson" runat="server" NavigateUrl='<%# "~\Account\ViewPerson.aspx?PersonID=" & Eval("Person_ID") %>'
                                        Text='<%# Eval("UserName") %>' />
                                    <br />
                                    <span style="color: Gray">
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("FIO") %>' />
                                    </span>
                                </DataItemTemplate>
                                <CellStyle HorizontalAlign="Left">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <GridViewImages SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                            <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                            </LoadingPanelOnStatusBar>
                            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                            </LoadingPanel>
                        </GridViewImages>
                        <GridViewImagesFilterControl>
                            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                            </LoadingPanel>
                        </GridViewImagesFilterControl>
                        <GridViewStyles>
                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                            </Header>
                            <LoadingPanel ImageSpacing="5px">
                            </LoadingPanel>
                        </GridViewStyles>
                        <GridViewStylesEditors ButtonEditCellSpacing="0">
                            <ProgressBar Height="21px">
                            </ProgressBar>
                        </GridViewStylesEditors>
                        <ValidationSettings>
                            <RequiredField ErrorText="Recipients are required" IsRequired="True" />
                        </ValidationSettings>
                    </dx:ASPxGridLookup>
                </td>
            </tr>
            <tr>
                <td valign="top" style="width: 70px; text-align: right;">
                    Subject:
                </td>
                <td valign="top">
                    <dx:ASPxTextBox ID="txtSubj" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                        CssPostfix="DevEx" NullText="Enter subject here" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"
                        Width="100%">
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td valign="top" style="width: 70px; text-align: right;">
                    Message:
                </td>
                <td valign="top">
                    <dx:ASPxHtmlEditor ID="txtBody" runat="server" Height="300px" Width="100%" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                        CssPostfix="DevEx"
                        >
                        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                            <ViewArea>
                                <Border BorderColor="#9DA0AA" />
                            </ViewArea>
                        </Styles>
                        <StylesEditors ButtonEditCellSpacing="0">
                        </StylesEditors>
                        <StylesStatusBar>
                            <StatusBar TabSpacing="0px">
                                <Paddings Padding="0px" />
                            </StatusBar>
                        </StylesStatusBar>
                        <SettingsImageUpload UploadImageFolder=""></SettingsImageUpload>
                        <SettingsValidation ErrorText="Message content is invalid">
                            <RequiredField ErrorText="Message cannot be empty" IsRequired="True" />
                        </SettingsValidation>
                        <SettingsImageSelector>
                            <CommonSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png"></CommonSettings>
                        </SettingsImageSelector>
                        <SettingsDocumentSelector>
                            <CommonSettings AllowedFileExtensions=".rtf, .pdf, .doc, .docx, .odt, .txt, .xls, .xlsx, .ods, .ppt, .pptx, .odp">
                            </CommonSettings>
                        </SettingsDocumentSelector>
                        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                            <LoadingPanel Url="~/App_Themes/DevEx/HtmlEditor/Loading.gif">
                            </LoadingPanel>
                        </Images>
                        <ImagesFileManager>
                            <FolderContainerNodeLoadingPanel Url="~/App_Themes/DevEx/Web/tvNodeLoading.gif">
                            </FolderContainerNodeLoadingPanel>
                            <LoadingPanel Url="~/App_Themes/DevEx/Web/Loading.gif">
                            </LoadingPanel>
                        </ImagesFileManager>
                    </dx:ASPxHtmlEditor>
                </td>
            </tr>
            <tr>
                <td valign="top" style="width: 70px; text-align: right;">
                    &nbsp;
                </td>
                <td valign="top">
                    <dx:ASPxButton ID="btnSend" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                        CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Send message"
                        Width="154px">
                    </dx:ASPxButton>
                </td>
            </tr>
            <tr>
                <td valign="top" style="width: 70px; text-align: right;">
                    &nbsp;</td>
                <td valign="top">
                    <dx:ASPxButton ID="btnShowHistory" runat="server"  ClientIDMode="Static"  
                        AutoPostBack="false"  ValidationGroup="never" CausesValidation="false" 
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
        SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Show message history">
    </dx:ASPxButton>
           
                </td>
            </tr>
        </table>
    </div>
    <h5 style="padding-left:30px;"><span></span>
    </h5>
  
    
    <div style="padding-left:30px; padding-right:30px;" >
        <div id="messagesList"  width="100%" >
        </div>
    </div>

    <script id="messagesTemplate" type="text/x-jquery-tmpl">
    <div style="margin:10px; padding:10px; border-bottom: 1px solid lightgray;">
    <table cellpadding="7" cellspacing = "6" border="0" width="100%">
                 <tr >
                     <td valign="top" style="text-align:right; width:170px; font-family:Verdana; font-size:8pt; " class="${CSS}">
                     <a href="../ViewPerson.aspx?PersonID=${PersonFromID}">${FullNameFrom}</a>
                     <br/>
                     <br/>
                     <span style=" font-family:Verdana; font-size:8pt; color:#999999">
                        ${MessageDate}<br/>
                            ${MessageTime}
                            </span>
                     </td>
                     <td valign="top" style="border-left:1px solid lightgray;" >
                        {{html Body}}
                     </td>
                    
                 </tr>
           </table>        
           </div>
                          
                   
    </script>

     <table width="100%" cellpadding="3" cellspacing="3" style="padding:15px;">
                <tr>
                    <td valign="top" style="width: 70px; text-align: right;">
                     
                    </td>
                    <td valign="top">
                          <dx:ASPxButton ID="btnShowMore" runat="server"  ClientIDMode="Static"  AutoPostBack="false"  ValidationGroup="never" CausesValidation="false" 
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
        SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Show more">
    </dx:ASPxButton>
                    </td>
                </tr>
                </table>

   
   <br /><br /><br /><br />
</asp:Content>

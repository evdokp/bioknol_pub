﻿Imports System.Xml
Imports System.Drawing
Imports System.IO
Imports BioWS.BLL.Extensions
Imports DevExpress.Data.Linq


Public Class ComposeMessage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim _profile As New ProfileCommon
            _profile = _profile.GetProfile(User.Identity.Name)
            Dim pers As New BLL.Person(_profile.Person_ID)




            CType(Master.FindControl("lit_pagetitle"), Literal).Text = pers.PageHeader
            CType(Master.FindControl("lit_curobj"), Literal).Text = "New message"

            lookupTO.GridView.Width = 450
            pnlSent.Visible = False



            If String.IsNullOrEmpty(Request.QueryString("replyOnID")) Then
                pnlReply.Visible = False
            Else
                Dim db As New DataClassesPersonsDataContext
                Dim message As mylinq.v_Message
                message = (From m In db.v_Messages
                          Where m.ID = Request.QueryString("replyOnID")
                          Select m).ToList()(0)
                litReplyOn.Text = message.Body
                txtSubj.Text = "RE: " & message.Title
                litTo.Text = "Copy to:"
                lookupTO.ValidationSettings.ValidationGroup = "none"
            End If

            If String.IsNullOrEmpty(Request.QueryString("PersonToID")) AndAlso String.IsNullOrEmpty(Request.QueryString("replyOnID")) Then
                pnlTo.Visible = False
                Me.hfInitialTopMessages.Value = -1
                btnShowHistory.Visible = False
                litTo.Text = "To:"
            Else
                Dim personto As New BLL.Person(CInt(Request.QueryString("PersonToID")))
                txtReplyTo.Text = String.Format("{0} / {1}", personto.DisplayName, personto.UserName)
                litTo.Text = "Copy to:"
                lookupTO.ValidationSettings.ValidationGroup = "none"

                Dim ta As New DataSetMessagesTableAdapters.MessagesTableAdapter
                Dim dt As Date = Now()
                Dim cnt As Integer
                ta.GetMessagesCount(CInt(Request.QueryString("PersonToID")), _profile.Person_ID, dt, cnt)
                Me.hfInitialTopMessages.Value = 10 'TODO to constant
                Me.hfPageOpenDateTime.Value = dt.ToString
                Me.hfTotalMessagesCount.Value = cnt
                Me.hfPersonTo.Value = CInt(Request.QueryString("PersonToID"))
                Me.hfPersonFrom.Value = _profile.Person_ID
                Me.hfFromName.Value = String.Format("{0} / {1}", personto.DisplayName, personto.UserName)

            End If
        End If
    End Sub
    Protected Sub LinqServerModeDataSource1_Selecting(sender As Object, e As LinqServerModeDataSourceSelectEventArgs) Handles LinqServerModeDataSource1.Selecting
        e.KeyExpression = "Person_ID"
    End Sub

    Protected Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        Dim p As New ProfileCommon
        p = p.GetProfile(User.Identity.Name)
        Dim selpersonsint As List(Of Integer) = lookupTO.GridView.GetSelectedFieldValues("Person_ID").ToListOfIntegers()
        '! check for yourself
        If selpersonsint.Contains(p.Person_ID) Then
            selpersonsint.Remove(p.Person_ID)
        End If
        If Not String.IsNullOrEmpty(Request.QueryString("PersonToID")) Then
            If Not selpersonsint.Contains(CInt(Request.QueryString("PersonToID"))) Then
                selpersonsint.Add(CInt(Request.QueryString("PersonToID")))
            End If
        End If

        BLL.MessageHelper.Send(p.Person_ID, selpersonsint, txtSubj.Text, txtBody.Html, Page)

        pnlSent.Visible = True
        litSent.Text = String.Format("Message was sent to {0} recipient(s)", selpersonsint.Count)

        ResetControls()




    End Sub


    Private Sub ResetControls()
        lookupTO.Value = Nothing
        lookupTO.Text = String.Empty

        txtBody.Html = String.Empty
        txtSubj.Text = String.Empty


    End Sub





    <System.Web.Services.WebMethod()> _
    Public Shared Function GetMessages(ByVal TopN As Integer, ByVal EarlierThen As String, ByVal LastIndex As Integer, ByVal PersonToID As Integer) As String
        Dim dt As Date
        DateTime.TryParse(EarlierThen, dt)
        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)
        Dim db As New DataClassesPersonsDataContext

        Dim tt = From message In db.GetMessages(TopN, p.Person_ID, PersonToID, EarlierThen, LastIndex)
                Select message.PersonFromID, message.FullNameFrom, message.Body, MessageDate = message.MessageDateTime.ToShortDateString, MessageTime = message.MessageDateTime.ToShortTimeString, CSS = IIf(message.PersonFromID = p.Person_ID, "msg_row1", "msg_row2")

        Return tt.ToList().ToJSON

    End Function

End Class
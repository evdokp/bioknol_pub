﻿Imports System.Drawing
Imports BioWS.BLL.Extensions
Imports DevExpress.Data



Public Class MyMessages
    Inherits System.Web.UI.Page

    Private _profilePersonID As Integer = 0
    Private ReadOnly Property ProfilePersonID As Integer
        Get
            If _profilePersonID = 0 Then
                Dim _profile As New ProfileCommon
                _profile = _profile.GetProfile(User.Identity.Name)
                _profilePersonID = _profile.Person_ID
            End If
            Return _profilePersonID
        End Get
    End Property

    Private Sub SetMessagesGV()
        ASPxGridView1.GroupBy(ASPxGridView1.Columns("MessageDate"))
        ASPxGridView1.SortBy(ASPxGridView1.Columns("MessageDate"), ColumnSortOrder.Descending)
        ASPxGridView1.ExpandAll()
        ASPxGridView1.Columns("FIOTo").Visible = False

        ViewState.Add("InOut", "Inbox")
        linqSource.DataBind()

        ASPxGridView1.DataBind()
        If ASPxGridView1.VisibleRowCount > 1 Then
            ASPxGridView1.FocusedRowIndex = 1
            ASPxGridView1.Selection.SelectRow(1)
        End If
        ASPxTreeView1.SelectedNode = ASPxTreeView1.Nodes.FindByName("Inbox")
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.NotPostBack() Then
            Dim pers As New BLL.Person(ProfilePersonID)
            Me.SetTitlesToMaster(pers.PageHeader, "Messages")
            SetMessagesGV()
        End If
    End Sub

    Private Sub linqSource_Selecting(sender As Object, e As DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs) Handles linqSource.Selecting
        Dim _profile As New ProfileCommon
        _profile = _profile.GetProfile(User.Identity.Name)
        Dim db As New DataClassesPersonsDataContext
   


        If ViewState("InOut") = "Inbox" Then
            e.QueryableSource = From m In db.v_Messages
                            Where m.PersonToID = _profile.Person_ID
                            Order By m.MessageDate Descending
                            Select m
        Else
            e.QueryableSource = From m In db.v_Messages
                            Where m.PersonFromID = _profile.Person_ID
                            Order By m.MessageDate Descending
                            Select m
        End If

        
    End Sub

    Private Sub ASPxTreeView1_NodeClick(source As Object, e As DevExpress.Web.ASPxTreeView.TreeViewNodeEventArgs) Handles ASPxTreeView1.NodeClick
        ViewState("InOut") = e.Node.Name
        linqSource.DataBind()
        ASPxGridView1.DataBind()


        If e.Node.Name = "Inbox" Then
            ASPxGridView1.Columns("FIOTo").Visible = False
            ASPxGridView1.Columns("FIOFrom").Visible = True
        Else
            ASPxGridView1.Columns("FIOTo").Visible = True
            ASPxGridView1.Columns("FIOFrom").Visible = False
        End If


    End Sub

    Private Sub ASPxGridView1_FocusedRowChanged(sender As Object, e As System.EventArgs) Handles ASPxGridView1.FocusedRowChanged
        If ViewState("InOut") = "Inbox" Then
            Dim r As mylinq.v_Message
            r = DirectCast(ASPxGridView1.GetRow(ASPxGridView1.FocusedRowIndex), mylinq.v_Message)
            If r IsNot Nothing Then
                Dim messagesta As New DataSetMessagesTableAdapters.MessagesTableAdapter
                messagesta.SetAsRead(r.ID)
                r.IsRead = True
            End If
        End If
    End Sub
    Private Sub ASPxGridView1_SelectionChanged(sender As Object, e As System.EventArgs) Handles ASPxGridView1.SelectionChanged
        If ViewState("InOut") = "Inbox" Then
            Dim r As mylinq.v_Message
            r = DirectCast(ASPxGridView1.GetRow(ASPxGridView1.FocusedRowIndex), mylinq.v_Message)
            If r IsNot Nothing Then
                Dim messagesta As New DataSetMessagesTableAdapters.MessagesTableAdapter
                messagesta.SetAsRead(r.ID)
            End If
            r.IsRead = True
            ' litBody.Text = r.Body
        End If
    End Sub
    Private Sub ASPxGridView1_HtmlRowPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles ASPxGridView1.HtmlRowPrepared
        Dim dataitem As Object = ASPxGridView1.GetRow(ASPxGridView1.FocusedRowIndex)

        If dataitem IsNot Nothing Then
            Dim type As Type = dataitem.GetType
            If dataitem IsNot Nothing Then
                Dim isRead As Boolean = CType(type.GetProperty("IsRead").GetValue(dataitem, Nothing), Integer)
                e.Row.Font.Bold = Not isRead
            End If


            Dim r As mylinq.v_Message
            r = DirectCast(ASPxGridView1.GetRow(e.VisibleIndex), mylinq.v_Message)
            If r IsNot Nothing Then
                e.Row.Font.Bold = Not r.IsRead
            End If
        End If

    End Sub



    Protected Sub ASPxMenu1_ItemClick(source As Object, e As DevExpress.Web.ASPxMenu.MenuItemEventArgs) Handles ASPxMenu1.ItemClick
        If e.Item.Name = "miReply" Then
            If ASPxGridView1.FocusedRowIndex > -1 Then
                Dim replyonmid As Integer = CInt(ASPxGridView1.GetSelectedFieldValues("ID")(0))
                Dim fromid As Integer = CInt(ASPxGridView1.GetSelectedFieldValues("PersonFromID")(0))
                If fromid <> 0 Then
                    Dim path As String = String.Format("~\Account\Messages\ComposeMessage.aspx?replyOnID={0}&PersonToID={1}", replyonmid, fromid)
                    Response.Redirect(path, False)
                End If
            End If
        End If
    End Sub

    Public Function IsPersonLinkVisible(ByVal ID) As Boolean
        Return IIf(ID = 0, False, True)
    End Function

End Class
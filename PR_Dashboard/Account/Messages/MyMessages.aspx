﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master.master"
    CodeBehind="MyMessages.aspx.vb" Inherits="BioWS.MyMessages" %>

<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSplitter" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxNavBar" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script language="javascript" type="text/javascript">

        function OnSplitterPaneResized(s, e) {
            var name = e.pane.name;
            if  (name == 'gridContainer')
                ResizeControl(grMessages, e.pane);
            
        }
        function ResizeControl(control, splitterPane) {
            control.SetWidth(splitterPane.GetClientWidth());
            control.SetHeight(splitterPane.GetClientHeight() - menuMessages.GetHeight());
        }


        function OnGridEndCallback() {
            spl.GetPaneByName("gridContainer").RaiseResizedEvent();
        }

        function OnGetRowValues(values) {
         
         $('#msgBody').html(values);
         lp1.Hide();
     }


     function OnGridFocusedRowChanged() {

    
         try
         {
            if (lp1 != undefined) {
             lp1.Show();
            }
         }
         catch (err)
         {

         }
 
         grMessages.GetRowValues(grMessages.GetFocusedRowIndex(), 'Body', OnGetRowValues);
     }

    </script>
    <dx:ASPxSplitter ID="ASPxSplitter1" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" ClientInstanceName="spl"
        CssPostfix="DevEx" Height="600px">
        <Panes>
            <dx:SplitterPane MaxSize="170" ShowCollapseBackwardButton="True">
                <Separators>
                    <SeparatorStyle>
                        <BorderTop BorderStyle="None" />
                    </SeparatorStyle>
                </Separators>
                <Separator>
                    <SeparatorStyle>
                        <BorderTop BorderStyle="None" />
                    </SeparatorStyle>
                </Separator>
                <PaneStyle>
                    <BorderLeft BorderStyle="None" />
                    <BorderTop BorderStyle="None" />
                </PaneStyle>
                <ContentCollection>
                    <dx:SplitterContentControl runat="server" SupportsDisabledAttribute="True">
                        <dx:ASPxTreeView ID="ASPxTreeView1" runat="server" AllowSelectNode="True">
                            <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                <NodeLoadingPanel Url="~/App_Themes/DevEx/Web/tvNodeLoading.gif">
                                </NodeLoadingPanel>
                                <LoadingPanel Url="~/App_Themes/DevEx/Web/Loading.gif">
                                </LoadingPanel>
                            </Images>
                            <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                            </Styles>
                            <Nodes>
                                <dx:TreeViewNode Text="Inbox" Name="Inbox"  >
                                    <Image Url="~/Images/mailbox_empty.png">
                                    </Image>
                                </dx:TreeViewNode>
                                <dx:TreeViewNode Text="Sent items" Name="Outbox">
                                    <Image Url="~/Images/mail_out.png">
                                    </Image>
                                </dx:TreeViewNode>
                            </Nodes>
                        </dx:ASPxTreeView>
                    </dx:SplitterContentControl>
                </ContentCollection>
            </dx:SplitterPane>
            <dx:SplitterPane>
                <Separator>
                    <SeparatorStyle>
                        <BorderBottom BorderStyle="None" />
                    </SeparatorStyle>
                </Separator>
                <Panes>
                    <dx:SplitterPane ShowCollapseBackwardButton="True" ShowCollapseForwardButton="True" Name="gridContainer" >
                        <PaneStyle>
                            <Paddings Padding="0px" />
                            <BorderTop BorderStyle="None" />
                            <BorderRight BorderStyle="None" />
                        </PaneStyle>
                        <ContentCollection>
                            <dx:SplitterContentControl runat="server" SupportsDisabledAttribute="True">
                                <div class="nolink">
                                    <dx:ASPxMenu ID="ASPxMenu1" runat="server" AutoSeparators="RootOnly" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" ClientInstanceName="menuMessages"
                                        CssPostfix="DevEx" ShowPopOutImages="True" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"
                                        Width="100%" ItemAutoWidth="False" >
                                        <Items>
                                            <dx:MenuItem Text="New message" NavigateUrl="~/Account/Messages/ComposeMessage.aspx">
                                                <Image Url="~/Images/mail_new.png">
                                                </Image>
                                            </dx:MenuItem>
                                            <dx:MenuItem Text="Reply" Name="miReply">
                                                <Image Url="~/Images/mail_out.png">
                                                </Image>
                                            </dx:MenuItem>
                                        </Items>
                                        <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
                                        </LoadingPanelImage>
                                        <ItemSubMenuOffset FirstItemX="2" LastItemX="2" X="2" />
                                        <ItemStyle DropDownButtonSpacing="10px" PopOutImageSpacing="10px" />
                                        <LoadingPanelStyle ImageSpacing="5px">
                                        </LoadingPanelStyle>
                                        <SubMenuStyle GutterImageSpacing="9px" GutterWidth="3px" />
                                        <Border BorderStyle="None" />
                                        <BorderBottom BorderStyle="None" />
                                    </dx:ASPxMenu>
                                    <dx:LinqServerModeDataSource ID="linqSource" runat="server" ContextTypeName="BioWS.DataClassesPersonsDataContext"
                                        TableName="v_Messages" />
                                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" ClientInstanceName="grMessages"
                                        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" DataSourceID="linqSource"
                                        KeyFieldName="ID" Width="100%">
                                   
                                        <Columns>
                                            <dx:GridViewDataDateColumn FieldName="MessageDateTime" Width="120px" ShowInCustomizationForm="True"
                                                VisibleIndex="1">
                                                <PropertiesDateEdit Spacing="0" DisplayFormatString="dd.MM.yyyy HH:mm">
                                                </PropertiesDateEdit>
                                            </dx:GridViewDataDateColumn>
                                            <dx:GridViewDataDateColumn FieldName="MessageDate" ShowInCustomizationForm="True"
                                                VisibleIndex="2">
                                                <PropertiesDateEdit Spacing="0" DisplayFormatString="dd.MM.yyyy">
                                                </PropertiesDateEdit>
                                            </dx:GridViewDataDateColumn>
                                            <dx:GridViewDataTextColumn FieldName="FIOFrom" Caption="From" ShowInCustomizationForm="True"
                                                Width="170px" VisibleIndex="5">
                                                <DataItemTemplate>


                                                    <asp:HyperLink ID="hlPerson" 
                                                      Visible ='<%# IsPersonLinkVisible(DataBinder.Eval(Container.DataItem, "PersonFromID")) %>'
 
                                                    runat="server" 
                                                    NavigateUrl='<%# "~\Account\ViewPerson.aspx?PersonID=" & Eval("PersonFromID") %>'
                                                    Text='<%# Eval("UserNameFrom") & "<br />" %>' />


                                                 
                                                    <span style="color: Gray">
                                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("FIOFrom") %>' />
                                                    </span>
                                                </DataItemTemplate>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="FIOTo" ShowInCustomizationForm="True" Width="170px"
                                                Caption="To" VisibleIndex="8">
                                                <DataItemTemplate>
                                                    <asp:HyperLink ID="hlPerson" runat="server" NavigateUrl='<%# "~\Account\ViewPerson.aspx?PersonID=" & Eval("PersonToID") %>'
                                                        Text='<%# Eval("UserNameTo") %>' />
                                                    <br />
                                                    <span style="color: Gray">
                                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("FIOTo") %>' />
                                                    </span>
                                                </DataItemTemplate>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="Title" Caption="Subject" ShowInCustomizationForm="True"
                                                VisibleIndex="9">
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                             <ClientSideEvents EndCallback="function(s, e) {	OnGridEndCallback();}"  
                                             SelectionChanged= "function(s, e) {OnGridFocusedRowChanged();}"
                                         />
                                        <SettingsBehavior AllowFocusedRow="True"  
                                        AllowSelectByRowClick="true" 
                                        EnableRowHotTrack = "true"  
                                        AllowSelectSingleRowOnly="true"  
                                          ProcessFocusedRowChangedOnServer="true" 
                                        
                                            />
                                        <Settings ShowGroupPanel="True" ShowVerticalScrollBar="True" VerticalScrollableHeight ="100"/>
                                              <settingspager pagesize="18" numericbuttoncount="4" />
                                                <border borderwidth="0px"></border>

                                        <SettingsLoadingPanel Mode="Disabled" />

                                        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                            <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                                            </LoadingPanelOnStatusBar>
                                            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                                            </LoadingPanel>
                                        </Images>
                                        <ImagesFilterControl>
                                            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                                            </LoadingPanel>
                                        </ImagesFilterControl>
                                        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                            </Header>
                                            <LoadingPanel ImageSpacing="5px">
                                            </LoadingPanel>
                                        </Styles>
                                        <StylesEditors ButtonEditCellSpacing="0">
                                            <ProgressBar Height="21px">
                                            </ProgressBar>
                                        </StylesEditors>
                                        <Border BorderStyle="None" />
                                    </dx:ASPxGridView>
                                </div>
                            </dx:SplitterContentControl>
                        </ContentCollection>
                    </dx:SplitterPane>
                    <dx:SplitterPane ShowCollapseBackwardButton="True" ScrollBars="Auto" ShowCollapseForwardButton="True">
                        <PaneStyle>
                            <Paddings Padding="0px" />
                            <BorderRight BorderStyle="None" />
                        </PaneStyle>
                        <ContentCollection>
                            <dx:SplitterContentControl runat="server" SupportsDisabledAttribute="True">
                                <div id="msgBody" style="padding: 5px;">
                                    <asp:Literal ID="litBody" runat="server"></asp:Literal>



                                </div>
                                <dx:ASPxLoadingPanel ID="ASPxLoadingPanel1" runat="server" ClientInstanceName="lp1"
                                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" ContainerElementID="msgBody"
                                    ImageSpacing="5px" Text="Loading message&amp;hellip;">
                                    <Image Url="~/App_Themes/DevEx/Web/Loading.gif">
                                    </Image>
                                </dx:ASPxLoadingPanel>
                            </dx:SplitterContentControl>
                        </ContentCollection>
                    </dx:SplitterPane>
                </Panes>
                <ContentCollection>
                    <dx:SplitterContentControl runat="server" SupportsDisabledAttribute="True">
                    </dx:SplitterContentControl>
                </ContentCollection>
            </dx:SplitterPane>
        </Panes>
        <ClientSideEvents PaneResized="OnSplitterPaneResized" />
        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
        </Styles>
        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
        </Images>
    </dx:ASPxSplitter>
</asp:Content>

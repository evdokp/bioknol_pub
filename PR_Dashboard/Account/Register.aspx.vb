﻿Public Class Register
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        RegisterUser.ContinueDestinationPageUrl = Request.QueryString("ReturnUrl")
        CType(Master.Master.FindControl("lit_pagetitle"), Literal).Text = "Register"
    End Sub

    Protected Sub RegisterUser_CreatedUser(ByVal sender As Object, ByVal e As EventArgs) Handles RegisterUser.CreatedUser
        FormsAuthentication.SetAuthCookie(RegisterUser.UserName, False)

        Dim continueUrl As String = RegisterUser.ContinueDestinationPageUrl
        If String.IsNullOrEmpty(continueUrl) Then
            continueUrl = "~/"
        End If

        Dim txt_first_name As TextBox
        Dim txt_last_name As TextBox
        Dim txt_patronimic As TextBox
        txt_first_name = Me.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtFirstName")
        txt_last_name = Me.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtLastName")
        txt_patronimic = Me.RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("txtPatronimic")
        Dim _p As New BLL.Person(Me.RegisterUser.UserName, txt_first_name.Text, txt_last_name.Text, txt_patronimic.Text)

        Dim userProfile As New ProfileCommon
        userProfile = userProfile.GetProfile(Me.RegisterUser.UserName)
        userProfile.Person_ID = _p.Person_ID
        userProfile.DefaultPeriodDays = 90
        userProfile.Save()

       


        'send mail
        Dim vBody As String = BLL.MailSender.ReadFile.ReadFile(Page.Server, "Registration.htm")
        vBody = vBody.Replace("<%UserName%>", Me.RegisterUser.UserName)
        vBody = vBody.Replace("<%Password%>", Me.RegisterUser.Password)
        Try
            BLL.MailSender.SendMailMessage("noreply@bioknowledgecenter.ru",
            "PRD mail service",
            Me.RegisterUser.UserName,
            String.Empty,
            String.Empty,
            "Personal Reference Dashboard registration",
            vBody)
        Catch ex As Exception
            ' nothing
        End Try



        Response.Redirect(continueUrl)
    End Sub

    Private Sub RegisterUser_CreatingUser(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.LoginCancelEventArgs) Handles RegisterUser.CreatingUser
        Dim cuw As CreateUserWizard = sender
        cuw.Email = cuw.UserName
    End Sub

End Class
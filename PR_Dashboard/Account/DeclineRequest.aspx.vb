﻿Imports System.IO
Imports BioWS.BLL.Extensions

Public Class DeclineRequest
    Inherits System.Web.UI.Page


    Private ReadOnly Property RequestID As Integer
        Get
            Return Me.QSInt32("Request_ID")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.NotPostBack() Then
            Dim vPMID As Integer
            Dim vTitle As String
            Dim vDOI As String

            'TODO - вообще должен быть класс REQUEST
            Dim ta As New ArticlesDataSetTableAdapters.ArticleRequestsTableAdapter
            Dim r = ta.GetDataByID(RequestID).Item(0)
            vDOI = IIf(r.IsDOINull, String.Empty, r.DOI)
            vPMID = IIf(r.IsPubMed_IDNull, 0, r.PubMed_ID)
            vTitle = IIf(vPMID.ToBoolean(), BLL.PubMed.GetTitleByID_internal(vPMID), r.Title)
            
            lit_title.Text = "Title: <br/> <b>" & vTitle & "</b>"





            Me.MultiView1.ActiveViewIndex = 0
        End If

    End Sub

  
    Protected Sub btn_goback_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_goback.Click
        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)
        Me.Response.Redirect("~\Account\OtherPeopleRequests.aspx?Person_ID=" & p.Person_ID, False)
    End Sub

    
    Protected Sub btn_decline_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_decline.Click
        Dim p As New BLL.Person(User.Identity.Name)
        p.DeclineRequest(Request.QueryString("Request_ID"), Me.txtDR.Text, Page)
        Me.MultiView1.ActiveViewIndex = 1
    End Sub
End Class
﻿<%@ Page Title="Log In" Language="vb" MasterPageFile="~/Basic.Master" AutoEventWireup="false"
    CodeBehind="Login.aspx.vb" Inherits="BioWS.Login" %>

    
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
 
    <p>
        Please enter your username and password.
        <asp:HyperLink ID="RegisterHyperLink" runat="server" EnableViewState="false">Register</asp:HyperLink> if you don't have an account.
        <br />
        If you have forgotten your password you may restore it - <asp:HyperLink ID="RestorePasswordHL" runat="server" EnableViewState="false" NavigateUrl ="~/Account/RemindPassword.aspx" >restore password</asp:HyperLink>  
        
    </p>
    <asp:Login ID="LoginUser" runat="server" EnableViewState="false" 
        RenderOuterTable="false" DestinationPageUrl="~/MyPageRedirect.aspx">
        <LayoutTemplate>
            <span class="failureNotification">
                <asp:Literal ID="FailureText" runat="server"></asp:Literal>
            </span>
            <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="failureNotification" 
                 ValidationGroup="LoginUserValidationGroup"/>
            <div class="accountInfo" style="width:400px;">
                <fieldset class="login" >
                    <legend>Account Information</legend>
                    <p>
                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Username:</asp:Label>
                        <asp:TextBox ID="UserName" runat="server" CssClass="textEntry"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" 
                             CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="User Name is required." 
                             ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                    </p>
                    <p>
                        <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                        <asp:TextBox ID="Password" runat="server" CssClass="textEntry" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" 
                             CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required." 
                             ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                    </p>
                    <p>
                        <asp:CheckBox ID="RememberMe" runat="server"/>
                        <asp:Label ID="RememberMeLabel" runat="server" AssociatedControlID="RememberMe" CssClass="inline">Keep me logged in</asp:Label>
                    </p>
                </fieldset>
                <p>
                    

                            
          <dx:ASPxButton ID="LoginButton" runat="server"  CommandName="Login"
              CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
              Height="28px" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" 
              Text="Log In" Width="163px"   ValidationGroup="LoginUserValidationGroup">
          </dx:ASPxButton>


                </p>
            </div>
        </LayoutTemplate>
    </asp:Login>
</asp:Content>
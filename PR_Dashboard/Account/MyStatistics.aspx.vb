﻿Imports System.Web.UI.DataVisualization.Charting
Public Class MyStatistics
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.hl_sites.NavigateUrl = "~\Account\ManageSites.aspx?Person_ID=" & Request.QueryString("Person_ID")
        Me.hl_messages.NavigateUrl = "~\Account\ViewMessages.aspx?Person_ID=" & Request.QueryString("Person_ID")


        If Page.IsPostBack = False Then
            Me.txtFrom.Text = Today.AddDays(-30)
            Me.txtTo.Text = Today
            Me.ObjectDataSource2.SelectParameters("StartDate").DefaultValue = Me.txtFrom.Text
            Me.ObjectDataSource2.SelectParameters("EndDate").DefaultValue = Me.txtTo.Text
        End If

        Dim vta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
        If vta.GetCountByPerson_ID(Request.QueryString("Person_ID")) = 0 Then
            Me.mv_charts.ActiveViewIndex = 1
        Else
            Me.mv_charts.ActiveViewIndex = 0
            MakeChart2()
        End If

    End Sub
    Private Sub MakeChart2()
        Dim s1 As New Series
        s1 = Me.Chart2.Series("Serie1")
        s1.ChartType = SeriesChartType.Bar

        Dim ta As New ChartDataSetTableAdapters.DataTable2TableAdapter
        Dim t = ta.GetBarsBySitesTopNByPerson_ID(20, Me.txtFrom.Text, Me.txtTo.Text, Request.QueryString("Person_ID"))
        s1.Points.Clear()

        Dim i As Integer

        For i = 0 To t.Count - 1
            Dim dp As New DataPoint
            dp.SetValueXY(t.Item(i).vTitle, t.Item(i).vCount)
            s1.Points.Add(dp)

        Next
        Me.Chart2.ChartAreas("ChartArea1").AxisX.Interval = 1


    End Sub

    Private Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Me.ObjectDataSource2.SelectParameters("StartDate").DefaultValue = Me.txtFrom.Text
        Me.ObjectDataSource2.SelectParameters("EndDate").DefaultValue = Me.txtTo.Text
        Me.ObjectDataSource2.DataBind()
        Me.Chart1.DataBind()
        MakeChart2()

    End Sub
End Class
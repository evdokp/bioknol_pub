﻿<%@ Page Title="Register" Language="vb" MasterPageFile="~/Basic.Master" AutoEventWireup="false"
    CodeBehind="Register.aspx.vb" Inherits="BioWS.Register" %>

  
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>


<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

    <asp:CreateUserWizard ID="RegisterUser" runat="server" EnableViewState="false">
        <LayoutTemplate>
            <asp:PlaceHolder ID="wizardStepPlaceholder" runat="server"></asp:PlaceHolder>
            <asp:PlaceHolder ID="navigationPlaceholder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
        <WizardSteps>
            <asp:CreateUserWizardStep ID="RegisterUserWizardStep" runat="server">
                <ContentTemplate>
                    
                    <p>
                        Use the form below to create a new account.
                    </p>
                    <p>
                        Passwords are required to be a minimum of <%= Membership.MinRequiredPasswordLength %> characters in length.
                    </p>
                    <span class="failureNotification">
                        <asp:Literal ID="ErrorMessage" runat="server"></asp:Literal>
                    </span>
                    <asp:ValidationSummary ID="RegisterUserValidationSummary" runat="server" CssClass="failureNotification" 
                         ValidationGroup="RegisterUserValidationGroup"/>
                    <div class="accountInfo">
                        <fieldset class="register">
                            <legend>Account Information</legend>
                            <p>
                                <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">E-mail:</asp:Label>
                                    <asp:TextBox ID="UserName" runat="server" CssClass="textEntry" ></asp:TextBox>
                                <asp:TextBox ID="Email" runat="server" CssClass="textEntry" Visible ="false" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="UserName" 
                                     CssClass="failureNotification" ErrorMessage="E-mail is required." ToolTip="E-mail is required." 
                                     ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                            </p>
                            <p>
                                <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                                <asp:TextBox ID="Password" runat="server" CssClass="textEntry" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" 
                                     CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required." 
                                     ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                            </p>
                            <p>
                                <asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword">Confirm Password:</asp:Label>
                                <asp:TextBox ID="ConfirmPassword" runat="server" CssClass="textEntry" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="ConfirmPassword" CssClass="failureNotification" Display="Dynamic" 
                                     ErrorMessage="Confirm Password is required." ID="ConfirmPasswordRequired" runat="server" 
                                     ToolTip="Confirm Password is required." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" 
                                     CssClass="failureNotification" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match."
                                     ValidationGroup="RegisterUserValidationGroup">*</asp:CompareValidator>
                            </p>
                            
                            <p>
                                <asp:Label ID="lblFirstName" runat="server" AssociatedControlID="txtFirstName">First name:</asp:Label>
                                <asp:TextBox ID="txtFirstName" runat="server" CssClass="textEntry"></asp:TextBox>                                
                            </p>
                            
                            <p>
                                <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName">Last name:</asp:Label>
                                <asp:TextBox ID="txtLastName" runat="server" CssClass="textEntry"></asp:TextBox>                                
                            </p>

                            <p>
                                <asp:Label ID="lblPatronimic" runat="server" AssociatedControlID="txtPatronimic">Second name (patronimic):</asp:Label>
                                <asp:TextBox ID="txtPatronimic" runat="server" CssClass="textEntry"></asp:TextBox>                                
                            </p>

                            <h3 style="margin-top:10px; margin-left:10px; margin-bottom:2px; padding:0px 0px 0px 0px">CONTRIBUTOR LICENSE AGREEMENT</h3>
                            <div style ="height :200px; overflow:scroll ; border : 1px solid #a0a0a0; padding:3px;margin-left:10px; margin-top:6px; margin-right:7px;">
                            
                     

<p>Thank you for your interest in the Open Knowledge Exchange Project (“OKEP”). To clarify 
the intellectual property license granted with Contributors from any person or entity, OKEP 
requires that each contributor submit a Contributor License Agreement (“CLA”), 
indicating agreement to the license terms as follows. This license is for your protection as 
a Contributor as well as the protection of OKEP and its users; it does not change your 
rights to use your own Contributors for any other purpose. </p>


<p>The license is being granted to the Neosemantic Consortia (the “Consortia”), which is a non-profit coalition of research institutions. Please read this document carefully and click checkbox to confirm your agreement with the statements below. </p>

<p>The Contributor accepts and agrees to the following terms and conditions for the Contributor's present and future Contributions submitted to the Consortia. Except for the license granted herein to the Consortia and recipients of software distributed by the Consortia, Contributor reserves all right, title, and interest in to Contributions.</p>


<p>1. <b>Definitions</b>.<br />“You” means the person, intentionally sharing 
    information on his or her visits to the selected web sites.<br />“Consortia” 
    means Neosemantic Consortia.<br />“Contribution” means any of the following: the 
    Internet address from which You accessed the web site, date and time, the 
    Internet address of the web site from which You linked to the site, the name of 
    the file or words you searched, items clicked on a page, and the browser and 
    operating system used.
</p>

<p>2. <b>Grant of Copyright License</b>.<br />Subject to the terms and conditions of 
    this Agreement, the You hereby grant to the Consortia and to recipients of 
    software distributed by the Consortia a perpetual, worldwide, non-exclusive, 
    no-charge, royalty-free, irrevocable copyright license to reproduce, modify, 
    prepare derivative works of, publicly display, publicly perform, sublicense, 
    dual-license, and distribute the Contributions and such derivative works.
</p>
<p>
3. <b>Disclaimer of Warranty</b>.<br />You provide the Contributions on an &quot;AS IS&quot; 
    BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied, 
    including, without limitation, any warranties or conditions of title, 
    non-infringement, merchantability, or fitness for a particular purpose.
</p>
</div>
                            
                            <p>
                            
                        <asp:Label ID="Label1" runat="server" text ="I HAVE READ THIS AGREEMENT<br/>AND AGREE TO ALL OF THE PROVISIONS CONTAINED ABOVE." ></asp:Label>    
                        </p>
                        
                        <asp:RadioButtonList ID="rbl_lic" runat="server" RepeatDirection ="Horizontal"   RepeatLayout ="Table" >
                        <asp:ListItem Text ="Yes, I do" Value ="1"></asp:ListItem>
                        <asp:ListItem Text ="No, I don't" Value ="0" Selected =True ></asp:ListItem>
                        </asp:RadioButtonList>
                        
                        
                        <asp:RangeValidator 
                        ID="RangeValidator1" 
                        runat="server" 
                        ErrorMessage="Agreement should be accepted" 
                         ControlToValidate ="rbl_lic"
                          MinimumValue ="1"
                           MaximumValue ="2"
                            ValidationGroup="RegisterUserValidationGroup"
                             CssClass="failureNotification" 
                        >*
                        </asp:RangeValidator>
                        
                               
                        </fieldset>
                        
                        
                        
                        
                        <p>
         

                      
          <dx:ASPxButton ID="CreateUserButton" runat="server"  CommandName="MoveNext"
              CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
              Height="28px" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" 
             Text="Create new account" Width="163px"   ValidationGroup="RegisterUserValidationGroup">
          </dx:ASPxButton>



                        </p>



                    </div>
                </ContentTemplate>
                <CustomNavigationTemplate>
                </CustomNavigationTemplate>
            </asp:CreateUserWizardStep>
<asp:CompleteWizardStep runat="server"></asp:CompleteWizardStep>
        </WizardSteps>
    </asp:CreateUserWizard>
    <br />
    <br />
</asp:Content>
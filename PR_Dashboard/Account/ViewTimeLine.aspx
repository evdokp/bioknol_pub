﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master.master" CodeBehind="ViewTimeLine.aspx.vb" Inherits="BioWS.ViewTimeLine" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />     
  <script src="http://static.simile.mit.edu/timeline/api-2.3.0/timeline-api.js?bundle=true" type="text/javascript"></script>     


      <style type="text/css">
          .timeline-event-label
          {
              font-size: 10px;
              width:700px;
          }
      </style>


</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

  


    <script type="text/javascript">
        var tl;
        $(document).ready(function () {


            //------------------------------------




            var eventSource = new Timeline.DefaultEventSource(0);

            var theme = Timeline.ClassicTheme.create();
            theme.event.bubble.width = 320;
            theme.event.bubble.height = 220;
            theme.event.track.height = 15;
            theme.event.tape.height = 8;
            var d = Timeline.DateTime.parseGregorianDateTime("1900")


            var bandInfos = [
          Timeline.createBandInfo({
              width: "88%",
              intervalUnit: Timeline.DateTime.DAY,
              intervalPixels: 100,
              eventSource: eventSource,
              theme: theme,
              zoomIndex: 10
          }),
          Timeline.createBandInfo({
              width: "6%",
              intervalUnit: Timeline.DateTime.MONTH,
              intervalPixels: 100,
              eventSource: eventSource,
              overview: true,
              theme: theme
          }),
          Timeline.createBandInfo({
              width: "6%",
              intervalUnit: Timeline.DateTime.YEAR,
              intervalPixels: 200,
              eventSource: eventSource,
              overview: true,
              theme: theme
          })
        ];
            bandInfos[1].syncWith = 0;
            bandInfos[2].syncWith = 0;
            bandInfos[2].highlight = true;


            tl = Timeline.create(document.getElementById("tl"), bandInfos, Timeline.HORIZONTAL);

            tl.loadJSON("../Handler/GetEvents.ashx?PersonID=" + $('#hfPersonID').val(), function (json, url) {
                eventSource.loadJSON(json, '');
            });
        });


        $(document).resize(function () {
            if (resizeTimerID == null) { resizeTimerID = window.setTimeout(function () { resizeTimerID = null; tl.layout(); }, 500); }
        });



    </script>
       <asp:HiddenField ID="hfPersonID" runat="server" ClientIDMode="Static"  />
     <div id="tl" class="timeline-default" style="height: 450px; border: 1px solid #aaa;overflow-x:hidden; overflow-y:hidden;}">
       
    </div>

</asp:Content>

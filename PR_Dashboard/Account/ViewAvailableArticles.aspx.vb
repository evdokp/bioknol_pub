﻿Public Class ViewAvailableArticles
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim _pr As New ProfileCommon
        _pr = _pr.GetProfile(User.Identity.Name)

        Dim p As New BLL.Person(_pr.Person_ID)

        CType(Master.Master.FindControl("lit_pagetitle"), Literal).Text = "Available articles"
        CType(Master.Master.FindControl("lit_curobj"), Literal).Text = p.PageHeader

        Me.ASPxTabControl1.Tabs.FindByName("byme").Text = "Uploaded by me (" & p.UploadedByMeArticlesCount & ")"
        Me.ASPxTabControl1.Tabs.FindByName("req").Text = "My accepted requests (" & p.MyRequestCount_Accepted & ")"
        Me.ASPxTabControl1.Tabs.FindByName("notme").Text = "Uploaded by others (" & p.UploadedByNOTMeArticlesCount & ")"


        If Page.IsPostBack = False Then
            Me.ASPxTabControl1.ActiveTab = Me.ASPxTabControl1.Tabs.FindByName("byme")
            Me.gv_offers.DataSourceID = "ods_offers"
            Me.gv_offers.DataBind()
        End If
    End Sub


    Protected Sub gv_offers_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gv_offers.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim r_row As ArticlesDataSet.ArticleOffersRow
            r_row = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row, ArticlesDataSet.ArticleOffersRow)
            Dim hl_title As HyperLink
            hl_title = e.Row.Cells(1).FindControl("hl_title")
            Dim hl_pubmed As HyperLink
            hl_pubmed = e.Row.Cells(3).FindControl("hl_pubmed")
            Dim hl_doi As HyperLink
            hl_doi = e.Row.Cells(3).FindControl("hl_doi")

            Dim _pr As New ProfileCommon
            _pr = _pr.GetProfile(User.Identity.Name)



            Dim lnkb_download As LinkButton = e.Row.Cells(2).FindControl("lnkb_download")
            lnkb_download.PostBackUrl = "~\Download.ashx?vFileName=" & HttpUtility.UrlEncode(r_row.FileName) & "&vOriginalName=" & HttpUtility.UrlEncode(r_row.OriginalName)

            If r_row.IsPubMed_IDNull = True Then
                hl_title.Text = r_row.Title
                hl_title.NavigateUrl = r_row.URL
                hl_pubmed.Visible = False
            Else
                If r_row.PubMed_ID = 0 Then
                    hl_title.Text = r_row.Title
                    hl_title.NavigateUrl = r_row.URL
                    hl_pubmed.Visible = False
                Else
                    Dim ta As New ArticlesPubmedDataSetTableAdapters.ArticlesPubmedTableAdapter
                    Dim t = ta.GetDataByPMID(r_row.PubMed_ID)
                    If t.Count = 0 Then
                        hl_title.Text = "Ошибка"
                        e.Row.Visible = False
                        ' TODO - найти все такие PMID и обработать
                    Else
                        Dim r = t.Item(0)
                        hl_title.Text = r.Title
                        hl_title.NavigateUrl = "~\ViewInfoPM.aspx?Pubmed_ID=" & r_row.PubMed_ID & "&UserID=" & _pr.Person_ID
                        hl_pubmed.NavigateUrl = "http://www.ncbi.nlm.nih.gov/pubmed/" & r_row.PubMed_ID
                        hl_pubmed.Visible = True
                        hl_pubmed.ToolTip = "PMID: " & r_row.PubMed_ID
                    End If
                    
                End If
            End If

            If r_row.IsDOINull = True Then
                hl_doi.Visible = False
            Else
                If r_row.DOI = String.Empty Then
                    hl_doi.Visible = False
                Else
                    hl_doi.Visible = True
                    hl_doi.NavigateUrl = "http://dx.doi.org/" & r_row.DOI
                End If
            End If


            Dim img_domain As Image = e.Row.Cells(3).FindControl("img_domain")
            'If BLL.Common.GetSiteID(r_row.URL) = 0 Then
            '    img_domain.Visible = True
            '    img_domain.ToolTip = "URL is not in domain list"
            'Else
            '    img_domain.Visible = False
            'End If
            img_domain.Visible = False
        End If

    End Sub

    Private Sub ASPxTabControl1_TabClick(source As Object, e As DevExpress.Web.ASPxTabControl.TabControlCancelEventArgs) Handles ASPxTabControl1.TabClick
        Select Case e.Tab.Name
            Case "byme"
                Me.gv_offers.DataSourceID = "ods_offers"
            Case "req"
                Me.gv_offers.DataSourceID = "ods_accepted"
            Case "notme"
                Me.gv_offers.DataSourceID = "ods_offersnotme"
        End Select
        Me.gv_offers.DataBind()
    End Sub
  
   
End Class
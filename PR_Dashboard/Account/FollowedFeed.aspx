﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Person.Master"
    CodeBehind="FollowedFeed.aspx.vb" Inherits="BioWS.FollowedFeed" %>
    <%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxRoundPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<%@ Register assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxLoadingPanel" tagprefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    
    
    
    
  
     <script type="text/javascript">
         $(document).ready(function () {
             $("#hfLastIndex").val(0);


             PageMethods.GetFeed($('#hfPersonID').val(), $("#hfPageOpenDateTime").val(), $("#hfLastIndex").val(), function (response) {
                 $("#divFeed").html("");
                 var obj = jQuery.parseJSON(response);
                 ShowResult(obj);
             });



             $("#btnShowMore").click(function () {
                 $('#btnShowMore').attr('disabled', 'disabled');
                 LoadingPanel.Show();
                 PageMethods.GetFeed($('#hfPersonID').val(), $("#hfPageOpenDateTime").val(), $("#hfLastIndex").val(), function (response) {
                     $('#btnShowMore').removeAttr('disabled');
                     LoadingPanel.Hide();
                     var obj = jQuery.parseJSON(response);
                     ShowResult(obj);
                 });
             });




         });

         function ShowResult(obj) {
    
             $("#hfLastIndex").val(parseInt($("#hfLastIndex").val()) + obj.length);
             $("#tmplFeed").tmpl(obj).appendTo("#divFeed");

             if (obj.length<10)
             {
                 $("#btnShowMore").hide();
             }

         };


        
     </script>
    <asp:HiddenField ID="hfPageOpenDateTime" runat="server" ClientIDMode="Static"  />
    <asp:HiddenField ID="hfInitialTopMessages" runat="server" ClientIDMode="Static"  />
       <asp:HiddenField ID="hfPersonID" runat="server" ClientIDMode="Static"  />
    <asp:HiddenField ID="hfLastIndex" runat="server" ClientIDMode="Static"  />


      <div id="divFeed"  >
      </div>
    <script id="tmplFeed" type="text/x-jquery-tmpl">
         {{html vHTML}}
    </script>
    <dx:ASPxLoadingPanel ID="ASPxLoadingPanel1" runat="server"  ClientInstanceName="LoadingPanel"
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
        ImageSpacing="5px" ContainerElementID="divFeed" Modal="True">
        <Image Url="~/App_Themes/DevEx/Web/Loading.gif">
        </Image>
    </dx:ASPxLoadingPanel>
 
    <br />
     <dx:ASPxButton ID="btnShowMore" runat="server"  AutoPostBack="false"  ClientIDMode="Static" 
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
        Height="29px" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" 
        Text="Show more" Width="100%">
    </dx:ASPxButton>





</asp:Content>

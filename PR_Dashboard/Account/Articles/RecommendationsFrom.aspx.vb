﻿Public Class RecommendationsFrom
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      

        If Page.IsPostBack = False Then
            Dim pers As New BLL.Person(CType(Request.QueryString("PersonID"), Integer))
            CType(Master.Master.FindControl("lit_pagetitle"), Literal).Text = pers.PageHeader
            CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "Articles recommended by the person"


            Dim recta As New ArticlesDataSetTableAdapters.v_ArticleRecommendationsTableAdapter
            Dim allt = recta.GetDataByPersonFromID(Request.QueryString("PersonID"))


            Dim alltt = From a In allt
                        Select a.Title, URL = GetURL(a)

            ASPxTitleIndex1.DataSource = alltt
            ASPxTitleIndex1.DataBind()


        End If
    End Sub

    Private Function GetURL(r As ArticlesDataSet.v_ArticleRecommendationsRow) As String
        If r.IsPubmedIDNull() Then
            If r.IsDOINull() = False Then
                Return "http://dx.doi.org/" & r.DOI
            Else
                Return r.URL
            End If
        Else
            Return "../../ViewInfoPM.aspx?Pubmed_ID=" & r.PubmedID
        End If
    End Function

End Class
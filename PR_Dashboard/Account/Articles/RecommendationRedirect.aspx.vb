﻿Public Class RecommendationRedirect
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim _profile As New ProfileCommon
        _profile = _profile.GetProfile(HttpContext.Current.User.Identity.Name)

        Dim recta As New ArticlesDataSetTableAdapters.ArticleRecommendationsTableAdapter
        Dim recr = recta.GetDataByID(Request.QueryString("RecommendationID"))(0)

        Dim recpersta As New ArticlesDataSetTableAdapters.ArticleRecommendationsToPersonsTableAdapter
        recpersta.UpdateIsViewed(True, Request.QueryString("RecommendationID"), _profile.Person_ID)

        Dim vdoi As String
        If recr.IsDOINull() Then
            vdoi = ""
        End If

        Response.Redirect(GetURL(recr), False)
    End Sub

    Private Function GetURL(r As ArticlesDataSet.ArticleRecommendationsRow) As String
        If r.IsPubmedIDNull() Then
            If r.IsDOINull() = False Then
                Return "http://dx.doi.org/" & r.DOI
            Else
                Return r.URL
            End If
        Else
            Return "../../ViewInfoPM.aspx?Pubmed_ID=" & r.PubmedID
        End If
    End Function

End Class
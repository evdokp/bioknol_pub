﻿Imports System.Threading
Public Class RecommendationsTo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then
            Dim pers As New BLL.Person(CType(Request.QueryString("PersonID"), Integer))
            CType(Master.Master.FindControl("lit_pagetitle"), Literal).Text = pers.PageHeader
            CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "Articles recommended to the person"
            Dim _profile As New ProfileCommon
            _profile = _profile.GetProfile(HttpContext.Current.User.Identity.Name)
            Dim isOwn As Boolean = (_profile.Person_ID = Request.QueryString("PersonID"))

            Dim recta As New ArticlesDataSetTableAdapters.v_ArticleRecommendationsTableAdapter
            Dim newt = recta.GetNewByPersonToID(Request.QueryString("PersonID"))
            Dim allt = recta.GetDataByPersonToID(Request.QueryString("PersonID"))

            If newt.Count > 0 And isOwn Then
                ASPxGridView1.DataSource = newt
                ASPxGridView1.DataBind()
            Else
                pnlNew.Visible = False
            End If

            Dim alltt = From a In allt
                        Select a.Title, URL = GetURL(a)

            ASPxTitleIndex1.DataSource = alltt
            ASPxTitleIndex1.DataBind()


        End If


    End Sub

    Private Function GetURL(r As ArticlesDataSet.v_ArticleRecommendationsRow) As String
        If r.IsPubmedIDNull() Then
            If r.IsDOINull() = False Then
                Return "http://dx.doi.org/" & r.DOI
            Else
                Return r.URL
            End If
        Else
            Return "../../ViewInfoPM.aspx?Pubmed_ID=" & r.PubmedID
        End If
    End Function

End Class
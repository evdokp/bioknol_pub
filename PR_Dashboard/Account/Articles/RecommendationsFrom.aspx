﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Person.master" CodeBehind="RecommendationsFrom.aspx.vb" Inherits="BioWS.RecommendationsFrom" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTitleIndex" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<dx:ASPxTitleIndex ID="ASPxTitleIndex1" runat="server" NameField="Title" 
        TextField="Title" Width="100%" 
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
        FilterBoxSpacing="16px" GroupSpacing="22px" 
        ImageFolder="~/App_Themes/DevEx/{0}/" 
        SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" 
        NavigateUrlField="URL">
    <FilterBox Caption="Filter:&amp;nbsp;&amp;nbsp;" />
    <BackToTopImage>
        <SpriteProperties CssClass="dxWeb_tiBackToTop_DevEx" />
    </BackToTopImage>
    <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
    </LoadingPanelImage>
    <IndexPanelItemStyle Font-Names="Tahoma" Font-Size="11pt">
    </IndexPanelItemStyle>
    <IndexPanelSeparatorStyle BackColor="#B8BAC2" Height="1px">
    </IndexPanelSeparatorStyle>
    <BackToTopStyle ImageSpacing="3px">
    </BackToTopStyle>
    <GroupContentStyle BackColor="White">
    </GroupContentStyle>
    <ColumnSeparatorStyle>
        <Paddings Padding="0px" />
    </ColumnSeparatorStyle>
    <ColumnStyle>
        <Paddings PaddingLeft="10px" PaddingRight="10px" />
    </ColumnStyle>
    <LoadingPanelStyle ImageSpacing="5px">
    </LoadingPanelStyle>
    <ItemStyle Wrap="True">
    <Paddings PaddingBottom="2px" PaddingLeft="0px" PaddingRight="8px" 
        PaddingTop="3px" />
    </ItemStyle>
    <DisabledStyle ForeColor="#B1B1B8">
    </DisabledStyle>
    </dx:ASPxTitleIndex>


</asp:Content>

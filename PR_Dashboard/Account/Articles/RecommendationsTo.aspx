﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Person.master" CodeBehind="RecommendationsTo.aspx.vb" Inherits="BioWS.RecommendationsTo" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTitleIndex" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="pnlNew" runat="server">


<h1>
New
</h1>

<dx:ASPxGridView ID="ASPxGridView1" runat="server"  Width="100%"
        AutoGenerateColumns="False" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" 
        CssPostfix="DevEx">
    <Columns>
        <dx:GridViewDataTextColumn VisibleIndex="0" Width="20px">
           <DataItemTemplate>
               <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/asterisk.png" />
                </DataItemTemplate>
        </dx:GridViewDataTextColumn>
           <dx:GridViewDataTextColumn  ShowInCustomizationForm="True"  Caption ="Recommended article" VisibleIndex="1"
                >
                <DataItemTemplate>
                    <asp:HyperLink ID="hlPerson" runat="server"
                     NavigateUrl='<%# "~\Account\Articles\RecommendationRedirect.aspx?RecommendationID=" & Eval("ID") %>'
                     Text ='<%# Eval("Title") %>'
                    />
                    <br />
                    <span style="color:Gray">
                        <asp:Literal ID="Literal1" runat="server" Text ='<%# Eval("Timestamp") %>'/>
                    </span>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
           <dx:GridViewDataTextColumn  ShowInCustomizationForm="True"  Caption ="Recommended by" VisibleIndex="2"
                >
                <DataItemTemplate>
                    <asp:HyperLink ID="hlPerson" runat="server"
                     NavigateUrl='<%# "~\Account\ViewPerson.aspx?PersonID=" & Eval("PersonID") %>'
                     Text ='<%# Eval("UserName") %>'
                    />
                    <br />
                    <span style="color:Gray">
                        <asp:Literal ID="Literal1" runat="server" Text ='<%# Eval("FIO") %>'/>
                    </span>
                    
                
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
     
    </Columns>
    <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
        </LoadingPanelOnStatusBar>
        <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <LoadingPanel ImageSpacing="5px">
        </LoadingPanel>
    </Styles>
    <StylesEditors ButtonEditCellSpacing="0">
        <ProgressBar Height="21px">
        </ProgressBar>
    </StylesEditors>
    </dx:ASPxGridView>
 

<h1>
All
</h1>

    </asp:Panel>

<dx:ASPxTitleIndex ID="ASPxTitleIndex1" runat="server" NameField="Title" 
        TextField="Title" Width="100%" 
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
        FilterBoxSpacing="16px" GroupSpacing="22px" 
        ImageFolder="~/App_Themes/DevEx/{0}/" 
        SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" 
        NavigateUrlField="URL">
    <FilterBox Caption="Filter:&amp;nbsp;&amp;nbsp;" />
    <BackToTopImage>
        <SpriteProperties CssClass="dxWeb_tiBackToTop_DevEx" />
    </BackToTopImage>
    <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
    </LoadingPanelImage>
    <IndexPanelItemStyle Font-Names="Tahoma" Font-Size="11pt">
    </IndexPanelItemStyle>
    <IndexPanelSeparatorStyle BackColor="#B8BAC2" Height="1px">
    </IndexPanelSeparatorStyle>
    <BackToTopStyle ImageSpacing="3px">
    </BackToTopStyle>
    <GroupContentStyle BackColor="White">
    </GroupContentStyle>
    <ColumnSeparatorStyle>
        <Paddings Padding="0px" />
    </ColumnSeparatorStyle>
    <ColumnStyle>
        <Paddings PaddingLeft="10px" PaddingRight="10px" />
    </ColumnStyle>
    <LoadingPanelStyle ImageSpacing="5px">
    </LoadingPanelStyle>
    <ItemStyle>
    <Paddings PaddingBottom="2px" PaddingLeft="0px" PaddingRight="8px" 
        PaddingTop="3px" />
    </ItemStyle>
    <DisabledStyle ForeColor="#B1B1B8">
    </DisabledStyle>
    </dx:ASPxTitleIndex>



</asp:Content>

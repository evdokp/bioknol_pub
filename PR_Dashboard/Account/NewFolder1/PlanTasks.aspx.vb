﻿Public Class PlanTasks
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btn_pubmed_add_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_pubmed_add.Click
        Dim ta As New TasksDataSetTableAdapters.TasksToDoTableAdapter

        Dim sDate As Date = Me.txt_pubmed_from.Text

        Dim i, k As Integer
        k = txt_pubmed_days.Text

        For i = 0 To k
            ta.Insert(1, New DateTime(sDate.Year, sDate.Month, sDate.Day, 2, 0, 0).AddDays(i), False)
        Next

        Me.lit_pubmed_result.Text = "Tasks were added"

        Me.ObjectDataSource1.DataBind()
        Me.GridView1.DataBind()


    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ms_row As TasksDataSet.TasksToDoRow
            ms_row = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row, TasksDataSet.TasksToDoRow)

            Dim Literal1 As Literal
            Literal1 = e.Row.Cells(1).FindControl("Literal1")
            If ms_row.TastTypeID = 1 Then
                Literal1.Text = "Pumbed IDs extraction"
            End If
        End If

    End Sub
End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master.Master"
    CodeBehind="SendMessages.aspx.vb" Inherits="BioWS.SendMessages" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>

<%@ Register Assembly="BioWS" Namespace="BioWS.MyControls" TagPrefix="custom" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Send Messages</h1>
        
    <asp:Literal ID="lit_result" runat="server"></asp:Literal>
        
        
    <fieldset class="register">
        <legend>Message Information</legend>
        <p>
            <asp:Label ID="lbl_title" runat="server" AssociatedControlID="txt_title">Title:</asp:Label>
            <asp:TextBox ID="txt_title" runat="server" CssClass="textEntry"></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="lbl_body" runat="server" AssociatedControlID="txt_body">Message:</asp:Label>
            <cc1:Editor ID="txt_body" runat="server" />
        </p>
        <p>
            <asp:Button ID="btn_send" runat="server" Text="send" />
        </p>
    </fieldset>
       
    
    
</asp:Content>

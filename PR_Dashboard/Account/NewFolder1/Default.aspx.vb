﻿Public Class _Default1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub btn_pubmedarticles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_pubmedarticles.Click
        BLL.PubMed.ProcessNewPubmedArticles()
    End Sub

    Private Sub btn_decode_urls_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_decode_urls.Click
        Using ta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter()
            Dim t = ta.GetData
            Dim i As Integer
            For i = 0 To t.Count - 1
                Dim r = t.Item(i)
                r.BeginEdit()
                r.URL = HttpUtility.UrlDecode(r.URL)
                r.EndEdit()
                ta.Update(r)
            Next
        End Using
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        BLL.PubMed.ExtractPubmedIDfromURL(Today.AddMonths(-2), Today.AddMonths(1))

    End Sub

    Protected Sub btn_admin_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_admin.Click
        Roles.AddUserToRole("katil@list.ru", "Admin")
        'Roles.AddUserToRole("lisitsa.av@gmail.com ", "Admin")

    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click
        Me.Literal1.Text = BLL.SearchHelper.Text_CONTAINS_Arg2_And("evdok pav")
    End Sub
End Class
﻿Public Class EditMonitoredSites
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ms_row As MonitoredSitesDataSet.MonitoredSitesRow
            ms_row = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row, MonitoredSitesDataSet.MonitoredSitesRow)

            Dim imb_chart As ImageButton
            imb_chart = e.Row.Cells(3).FindControl("imb_viewstat")

            Using ta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter()
                If ta.GetCountByMonitoredSite_ID(ms_row.ID) = 0 Then
                    imb_chart.ImageUrl = "~\Images\chart_fade.png"
                Else
                    imb_chart.ImageUrl = "~\Images\chart.png"
                End If
            End Using


            Dim im1 As Image
            im1 = e.Row.Cells(2).FindControl("Image1")

            If ms_row.IsApproved = True Then
                im1.ImageUrl = "~\Images\check2.png"
            Else
                im1.ImageUrl = "~\Images\check2_fade.png"
            End If

        End If

    End Sub
End Class
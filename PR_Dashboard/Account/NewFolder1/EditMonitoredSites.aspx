﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master.Master"
    CodeBehind="EditMonitoredSites.aspx.vb" Inherits="BioWS.EditMonitoredSites" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="BioWS.MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter">
    </asp:ObjectDataSource>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="ObjectDataSource1"
        PageSize="20" Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="ID" InsertVisible="False" SortExpression="ID">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="SiteURL" SortExpression="SiteURL">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("SiteURL") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="IsApproved" SortExpression="IsApproved">
                <ItemTemplate>
                    
                    <asp:Image ID="Image1" runat="server" />
                    
                    
                </ItemTemplate>
                <ItemStyle   HorizontalAlign ="Center"  />
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False" >
                <ItemTemplate>
                
                    <asp:ImageButton ID="imb_look" runat="server" ImageUrl =  "~/Images/view.png" PostBackUrl  ='<%# "http://" & Eval("SiteURL") %>' />
                    <asp:ImageButton ID="imb_viewstat" runat="server" PostBackUrl  ='<%#  "~\Account\Admin\ViewStatBySite.aspx?Site_ID=" &  Eval("ID") %>' />
                    </ItemTemplate>
                <ItemStyle Width ="100px" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>

﻿Imports System.Web.UI.DataVisualization.Charting
Public Class ViewStatByUser
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.txtFrom.Text = Today.AddDays(-30)
            Me.txtTo.Text = Today
            Me.ObjectDataSource2.SelectParameters("StartDate").DefaultValue = Today.AddDays(-20)
            Me.ObjectDataSource2.SelectParameters("EndDate").DefaultValue = Today.AddDays(1)
        End If

        Dim vta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
        If vta.GetCountByPerson_ID(Request.QueryString("Person_ID")) = 0 Then
            Me.mv_charts.ActiveViewIndex = 1
        Else
            Me.mv_charts.ActiveViewIndex = 0
            MakeChart2()

        End If

    End Sub
    Private Sub MakeChart2()
        Dim s1 As New Series
        s1.Name = "Serie1"
        s1.ChartType = SeriesChartType.Bar

        Dim ta As New ChartDataSetTableAdapters.DataTable2TableAdapter
        Dim t = ta.GetBarsBySitesTopNByPerson_ID(20, Me.txtFrom.Text, Me.txtTo.Text, Request.QueryString("Person_ID"))

        Dim i As Integer

        For i = 0 To t.Count - 1
            Dim dp As New DataPoint
            dp.SetValueXY(t.Item(i).vTitle, t.Item(i).vCount)
            s1.Points.Add(dp)

        Next
        Me.Chart2.ChartAreas("ChartArea1").AxisX.Interval = 1
        Me.Chart2.Series.Add(s1)

    End Sub

End Class
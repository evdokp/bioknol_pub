﻿Imports System.Text.RegularExpressions

Public Class MergeSites
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub


    Protected Sub btn_merge_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_merge.Click
        'Dim s As String

        'Dim new_id, old_id As Integer
        'new_id = Me.lb_1.SelectedValue

        's = s & "This item: <b>" & Me.lb_1.SelectedItem.Text & "</b> has replaced following items:<br/><br/>"


        'Dim ii As Integer()
        'ii = Me.lb_2.GetSelectedIndices

        'Dim vl_ta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
        'Dim es_ta As New MonitoredSitesDataSetTableAdapters.ExceptionStringsTableAdapter
        'Dim ms_ta As New MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter

        's = s & "<ul>"

        'For Each i As Integer In ii

        '    s = s & "<li>"
        '    s = s & Me.lb_2.Items(i).Text

        '    old_id = Me.lb_2.Items(i).Value
        '    vl_ta.UpdateMonitoredSiteID(new_id, old_id)
        '    es_ta.UpdateMonitoredSiteID(new_id, old_id)
        '    ms_ta.Delete(old_id)

        '    s = s & "</li>"
        'Next
        's = s & "</ul>"
        'Me.Literal1.Text = s

        'Me.lb_1.DataBind()
        'Me.lb_2.DataBind()

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Dim s As String
        Dim ms_ta As New MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter
        Dim ms_r As MonitoredSitesDataSet.MonitoredSitesRow
        Dim ii As Integer()
        ii = Me.lb_2.GetSelectedIndices

        s = s & "Following items has been transformed into ""domain only"" view:<br/><br/>"
        s = s & "<ul>"

        Dim new_title As String

        For Each i As Integer In ii
            new_title = String.Empty
            s = s & "<li>"


            ms_r = ms_ta.GetDataByID(Me.lb_2.Items(i).Value).Item(0)
            new_title = Regex.Match(Me.lb_2.Items(i).Text, "(?<=http\:\/\/).*?(?=(\/|\\|$))").Value

            If new_title = String.Empty Then
                new_title = Regex.Match(Me.lb_2.Items(i).Text, "^.{1,100}?(?=(\/|\\|$))").Value
                If new_title = String.Empty Then
                    s = s & "Failure: " & Me.lb_2.Items(i).Text
                Else
                    ms_r.BeginEdit()
                    ms_r.SiteURL = new_title
                    ms_r.EndEdit()
                    ms_ta.Update(ms_r)
                    s = s & "Success: " & new_title
                End If
            Else
                ms_r.BeginEdit()
                ms_r.SiteURL = new_title
                ms_r.EndEdit()
                ms_ta.Update(ms_r)
                s = s & "Success: " & new_title
            End If
            s = s & "</li>"
        Next
        s = s & "</ul>"
        Me.Literal1.Text = s
        Me.lb_1.DataBind()
        Me.lb_2.DataBind()
    End Sub
End Class
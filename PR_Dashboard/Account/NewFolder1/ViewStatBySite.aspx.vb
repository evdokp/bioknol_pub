﻿Imports System.Web.UI.DataVisualization.Charting
Public Class ViewStatBySite
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.txtFrom.Text = Today.AddDays(-30)
            Me.txtTo.Text = Today
        End If

        Dim vta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
        If vta.GetCountByMonitoredSite_ID(Request.QueryString("Site_ID")) = 0 Then
            Me.mv_charts.ActiveViewIndex = 1
        Else
            Me.mv_charts.ActiveViewIndex = 0
            MakeChart2()
            MakeChart3()
        End If

        Dim ta As New MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter
        Dim r = ta.GetDataByID(Request.QueryString("Site_ID")).Item(0)
        If Page.IsPostBack = False Then
            Me.txt_site.Text = r.SiteURL
            Me.lit_sitesaved.Text = String.Empty
        End If

    End Sub
    Private Sub btn_savesite_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_savesite.Click
        Dim ta As New MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter
        Dim r = ta.GetDataByID(Request.QueryString("Site_ID")).Item(0)
        r.BeginEdit()
        r.SiteURL = Me.txt_site.Text
        r.EndEdit()
        ta.Update(r)
        Me.lit_sitesaved.Text = "Site info saved"
    End Sub
    Private Sub MakeChart2()
        Dim s1 As New Series
        s1.Name = "Serie1"
        s1.ChartType = SeriesChartType.Bar

        Dim ta As New ChartDataSetTableAdapters.DataTable2TableAdapter
        Dim t = ta.GetBarsByUsersTopNBySiteID(20, Me.txtFrom.Text, Me.txtTo.Text, Request.QueryString("Site_ID"))

        Dim i As Integer

        For i = 0 To t.Count - 1
            Dim dp As New DataPoint
            dp.SetValueXY(t.Item(i).vTitle, t.Item(i).vCount)
            s1.Points.Add(dp)

        Next
        Me.Chart2.ChartAreas("ChartArea1").AxisX.Interval = 1
        Me.Chart2.Series.Add(s1)

    End Sub
    Private Sub MakeChart3()
        Dim sta As New MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter
        Dim r = sta.GetDataByID(Request.QueryString("Site_ID")).Item(0)
        Dim ms_length As Integer
        ms_length = r.SiteURL.Length
        
        Dim s1 As New Series
        s1.Name = "Serie1"
        s1.ChartType = SeriesChartType.Bar

        Dim ta As New ChartDataSetTableAdapters.DataTable2TableAdapter
        Dim t = ta.GetBarsBySiteIDTopN(20, Me.txtFrom.Text, Me.txtTo.Text, Request.QueryString("Site_ID"))

        Dim newlen As Integer
        Dim i As Integer
        For i = 0 To t.Count - 1
            Dim dp As New DataPoint
            newlen = Math.Min((ms_length + 30), t.Item(i).vTitle.Length)
            dp.SetValueXY(t.Item(i).vTitle.Substring(0, newlen), t.Item(i).vCount)
            s1.Points.Add(dp)
        Next
        Me.Chart3.ChartAreas("ChartArea1").AxisX.Interval = 1
        Me.Chart3.Series.Add(s1)
    End Sub
End Class
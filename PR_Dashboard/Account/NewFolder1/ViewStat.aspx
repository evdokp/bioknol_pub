﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Basic.Master"
    CodeBehind="ViewStat.aspx.vb" Inherits="BioWS.ViewStat" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    from
    <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
    to
    <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
    <asp:Button ID="btnRefresh" runat="server" Text="Refresh" />
    
    <br /><br />
    <asp:Chart ID="bar_requests" runat="server" Width="450px">
        <Titles>
            <asp:Title Name="Title1" Text="Unprocessed paper requests' count (current)" BackColor="#9eb8d3" BackGradientStyle="LeftRight"
                BorderColor="Silver">
            </asp:Title>
        </Titles>
        
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
                <AxisY>
                    <MajorGrid LineColor="LightGray" />
                </AxisY>
                <AxisX>
                    <MajorGrid LineColor="LightGray" />
                </AxisX>
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>

    <table width ="100%">
    <tr>
    <td style ="width :50%;">
    <asp:Chart ID="pie_pubmedid" runat="server" Palette="BrightPastel" Width="450px">
        <Series>
            <asp:Series Name="Default" ChartType="Pie" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240"
                CustomProperties="LabelsHorizontalLineSize=2, PieLabelStyle=Outside, PieDrawingStyle=Concave, CollectedThreshold=5, PieLineColor=SlateGray, CollectedLegendText=Другие\n#VAL (#PERCENT), CollectedLabel=Другие"
                Label="#VALX\n#VAL (#PERCENT)" LegendText="#VALX" XValueType="String" YValueType="Int32">
                <SmartLabelStyle AllowOutsidePlotArea="Yes" IsOverlappedHidden="False" MaxMovingDistance="40" />
            </asp:Series>
        </Series>
        <Titles>
            <asp:Title Name="Title1" Text="Share of PubMedID-detected records" BackColor="#9eb8d3"
                BackGradientStyle="LeftRight" BorderColor="Silver">
            </asp:Title>
        </Titles>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent"
                BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                <Area3DStyle Rotation="0" />
                <AxisY LineColor="64, 64, 64, 64">
                    <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                    <MajorGrid LineColor="64, 64, 64, 64" />
                </AxisY>
                <AxisX LineColor="64, 64, 64, 64">
                    <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                    <MajorGrid LineColor="64, 64, 64, 64" />
                </AxisX>
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
    
    </td>
    <td style ="width :50%;">
    <asp:Chart ID="pie_doi" runat="server" Palette="BrightPastel" Width="450px">
        <Series>
            <asp:Series Name="Default" ChartType="Pie" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240"
                CustomProperties="LabelsHorizontalLineSize=2, PieLabelStyle=Outside, PieDrawingStyle=Concave, CollectedThreshold=5, PieLineColor=SlateGray, CollectedLegendText=Другие\n#VAL (#PERCENT), CollectedLabel=Другие"
                Label="#VALX\n#VAL (#PERCENT)" LegendText="#VALX" XValueType="String" YValueType="Int32">
                <SmartLabelStyle AllowOutsidePlotArea="Yes" IsOverlappedHidden="False" MaxMovingDistance="40" />
            </asp:Series>
        </Series>
        <Titles>
            <asp:Title Name="Title1" Text="Share of DOI-detected records" >
            </asp:Title>
        </Titles>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent"
                BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                <Area3DStyle Rotation="0" />
                <AxisY LineColor="64, 64, 64, 64">
                    <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                    <MajorGrid LineColor="64, 64, 64, 64" />
                </AxisY>
                <AxisX LineColor="64, 64, 64, 64">
                    <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                    <MajorGrid LineColor="64, 64, 64, 64" />
                </AxisX>
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
    
    </td>
    
    </tr>
    
    </table>
    
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetViewsCountOverall" TypeName="BioWS.ChartDataSetTableAdapters.DataTable1TableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="txtFrom" PropertyName="text" Name="StartDate" Type="DateTime" />
            <asp:ControlParameter ControlID="txtTo" PropertyName="text" Name="EndDate" Type="DateTime" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:Chart ID="Chart1" runat="server" DataSourceID="ObjectDataSource2" Width="940px">
        <Series>
            <asp:Series ChartType="Spline" Name="Series1" XValueMember="vDate" YValueMembers="vCount"
                BorderWidth="3">
            </asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
                <AxisY>
                	<striplines>
											<asp:stripline TextLineAlignment="Far" Font="Trebuchet MS, 8.25pt" StripWidth="50" Text="Standard Deviation" IntervalOffset="20" BackColor="64, 241, 185, 168"></asp:stripline>
											<asp:stripline TextLineAlignment="Far" Font="Trebuchet MS, 8.25pt" BorderWidth="2" BorderColor="252, 180, 65" Text="Mean" IntervalOffset="40"></asp:stripline>
											<asp:stripline TextLineAlignment="Far" Font="Trebuchet MS, 8.25pt" BorderWidth="2" BorderColor="224, 64, 10" Text="Median" IntervalOffset="50" TextAlignment="Near"></asp:stripline>
										</striplines>
                    <MajorGrid LineColor="LightGray" />
                </AxisY>
                <AxisX>
                    <MajorGrid LineColor="LightGray" />
                </AxisX>
            </asp:ChartArea>
        </ChartAreas>

        
        
        <Titles>
            <asp:Title Name="Title1" Text="Views count by date" BackColor="#9eb8d3" BackGradientStyle="LeftRight"
                BorderColor="Silver">
            </asp:Title>
        </Titles>
    </asp:Chart>
    <asp:Chart ID="Chart2" runat="server" Width="940px" Height ="380">
        <Titles>
            <asp:Title Name="Title1" Text="Top 20 users by views count" BackColor="#9eb8d3" BackGradientStyle="LeftRight"
                BorderColor="Silver">
            </asp:Title>
        </Titles>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
                <AxisY>
                    <MajorGrid LineColor="LightGray" />
                </AxisY>
                <AxisX>
                    <MajorGrid LineColor="LightGray" />
                </AxisX>
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
    <asp:Chart ID="Chart3" runat="server" Width="940px" Height="380">
        <Titles>
            <asp:Title Name="Title1" Text="Top 20 URLs by views count" BackColor="#9eb8d3" BackGradientStyle="LeftRight"
                BorderColor="Silver">
            </asp:Title>
        </Titles>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
                <AxisY>
                    <MajorGrid LineColor="LightGray" />
                </AxisY>
                <AxisX>
                    <MajorGrid LineColor="LightGray" />
                </AxisX>
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
    
</asp:Content>

﻿Public Class ManageUsers
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ms_row As PersonsDataSet.PersonsRow
            ms_row = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row, PersonsDataSet.PersonsRow)

            Dim imb_chart As ImageButton
            imb_chart = e.Row.Cells(3).FindControl("imb_viewstat")

            Using ta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter()
                If ta.GetCountByPerson_ID(ms_row.Person_ID) = 0 Then
                    imb_chart.ImageUrl = "~\Images\chart_fade.png"
                Else
                    imb_chart.ImageUrl = "~\Images\chart.png"
                End If
            End Using


        End If
    End Sub
End Class
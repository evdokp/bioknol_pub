﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master.Master"
    CodeBehind="ViewStatBySite.aspx.vb" Inherits="BioWS.ViewStatBySite" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="text-align: right;">
        <asp:ImageButton ID="imb_list" runat="server" PostBackUrl="~/Account/Admin/EditMonitoredSites.aspx"
            ImageUrl="~/Images/form_blue.png" ToolTip="Back to list of sites" />
    </div>
    <fieldset class="register">
        <legend>Statistics</legend>
        <asp:MultiView ID="mv_charts" runat="server">
            <asp:View ID="v_charts" runat="server">
                from
                <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                to
                <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
                <asp:Button ID="btnRefresh" runat="server" Text="Refresh" />
                <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetViewsCountBySiteIDandTwoDates" TypeName="BioWS.ChartDataSetTableAdapters.DataTable1TableAdapter">
                    <SelectParameters>
                        <asp:QueryStringParameter QueryStringField="Site_ID" Name="MonitoredSite_ID" Type="Int32" />
                        <asp:ControlParameter ControlID="txtFrom" PropertyName="text" Name="StartDate" Type="DateTime" />
                        <asp:ControlParameter ControlID="txtTo" PropertyName="text" Name="EndDate" Type="DateTime" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:Chart ID="Chart1" runat="server" DataSourceID="ObjectDataSource2" Width="840px">
                    <Series>
                        <asp:Series ChartType="Spline" Name="Series1" XValueMember="vDate" YValueMembers="vCount"
                            BorderWidth="3">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1">
                            <AxisY>
                                <MajorGrid LineColor="LightGray" />
                            </AxisY>
                            <AxisX>
                                <MajorGrid LineColor="LightGray" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <asp:Title Name="Title1" Text="Views count by date"     BackColor="#9eb8d3" BackGradientStyle="LeftRight" BorderColor="Silver">
                        </asp:Title>
                    </Titles>
                </asp:Chart>
                <br />
                <asp:Chart ID="Chart2" runat="server" Width="840px">
                    <Titles>
                        <asp:Title Name="Title1" Text="Top 20 users by views count"     BackColor="#9eb8d3" BackGradientStyle="LeftRight" BorderColor="Silver">
                        </asp:Title>
                    </Titles>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1">
                            <AxisY>
                                <MajorGrid LineColor="LightGray" />
                            </AxisY>
                            <AxisX>
                                <MajorGrid LineColor="LightGray" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
                <br />
                <asp:Chart ID="Chart3" runat="server" Width="840px">
                    <Titles>
                        <asp:Title Name="Title1" Text="Top 20 URLs by views count"     BackColor="#9eb8d3" BackGradientStyle="LeftRight" BorderColor="Silver">
                        </asp:Title>
                    </Titles>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1">
                            <AxisY>
                                <MajorGrid LineColor="LightGray" />
                            </AxisY>
                            <AxisX>
                                <MajorGrid LineColor="LightGray" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </asp:View>
            <asp:View ID="v_nocharts" runat="server">
                There is no data to display.
            </asp:View>
        </asp:MultiView>
    </fieldset>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <fieldset class="register">
                <legend>Edit domain information</legend>
                <p>
                    <asp:Label ID="lbl_site" runat="server" Text="Domain" AssociatedControlID="txt_site"></asp:Label>
                    <asp:TextBox ID="txt_site" runat="server" Width="336px">www.uniprot.org</asp:TextBox>
                </p>
                <p>
                    <asp:Button ID="btn_savesite" runat="server" Text="Save" />
                </p>
                <p>
                    <span style="color: Green;">
                        <asp:Literal ID="lit_sitesaved" runat="server">
                        </asp:Literal></span>
                </p>
            </fieldset>
            <fieldset class="register">
                <legend>Edit exception strings</legend>
                <asp:GridView ID="gv_ex" runat="server" AutoGenerateColumns="False" DataKeyNames="ID"
                    DataSourceID="ods_exceptions">
                    <Columns>
                        <asp:CommandField ShowDeleteButton="True" />
                        <asp:TemplateField HeaderText="ExceptionString" SortExpression="ExceptionString">
                            <ItemTemplate>
                                <div style="padding: 4px;">
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("ExceptionString") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                            <ItemStyle ForeColor="Red" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <br />
                <asp:ObjectDataSource ID="ods_exceptions" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByMonitoredSite_ID"
                    TypeName="BioWS.MonitoredSitesDataSetTableAdapters.ExceptionStringsTableAdapter"
                    UpdateMethod="Update">
                    <DeleteParameters>
                        <asp:Parameter Name="Original_ID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:QueryStringParameter Name="MonitoredSite_ID" QueryStringField="Site_ID" Type="Int32" />
                        <asp:Parameter Name="ExceptionString" Type="String" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:QueryStringParameter Name="MonitoredSite_ID" QueryStringField="Site_ID" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="MonitoredSite_ID" Type="Int32" />
                        <asp:Parameter Name="ExceptionString" Type="String" />
                        <asp:Parameter Name="Original_ID" Type="Int32" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:FormView ID="fv_ex" runat="server" DataKeyNames="ID" DataSourceID="ods_exceptions"
                    DefaultMode="Insert">
                    <InsertItemTemplate>
                        Аdd new exception string:
                        <asp:TextBox ID="ExceptionStringTextBox" runat="server" Text='<%# Bind("ExceptionString") %>' />
                        <br />
                        <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                            Text="Insert" />
                        &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False"
                            CommandName="Cancel" Text="Cancel" />
                    </InsertItemTemplate>
                </asp:FormView>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

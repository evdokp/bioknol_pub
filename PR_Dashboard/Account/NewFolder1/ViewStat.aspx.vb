﻿Imports System.Web.UI.DataVisualization.Charting

Public Class ViewStat
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then
            Me.txtFrom.Text = Today.AddDays(-30)
            Me.txtTo.Text = Today
        End If
        MakeChart()
        MakeChart1()
        MakeChartPubmed()

        MakeChartDOI()

        MakeRequests()



    End Sub
    Private Sub MakeChartViewsHistory()
        Dim ax_x As Axis = Me.Chart1.ChartAreas("ChartArea1").AxisX
        Dim ax_y As Axis = Me.Chart1.ChartAreas("ChartArea1").AxisY

        ax_y.Title = "Views count"
        ax_y.Interval = 0
        ax_y.IsLabelAutoFit = True
        ax_y.LabelAutoFitStyle = LabelAutoFitStyles.DecreaseFont

        ax_x.Title = "Date"
        ax_x.Interval = 1
        ax_x.LabelStyle.IsEndLabelVisible = False
        ax_x.IsLabelAutoFit = True
        ax_x.LabelAutoFitStyle = LabelAutoFitStyles.LabelsAngleStep90



        'statistics
        'Calculate descriptive statistics
        Dim mean As Double = Chart1.DataManipulator.Statistics.Mean("Series1")
        Dim median As Double = Chart1.DataManipulator.Statistics.Median("Series1")
        Dim variance As Double = Chart1.DataManipulator.Statistics.Variance("Series1", True)

        'Set Strip line item
        Chart1.ChartAreas("ChartArea1").AxisY.StripLines(0).IntervalOffset = mean - Math.Sqrt(variance)
        Chart1.ChartAreas("ChartArea1").AxisY.StripLines(0).StripWidth = 2.0 * Math.Sqrt(variance)
        'Set Strip line item
        Chart1.ChartAreas("ChartArea1").AxisY.StripLines(1).IntervalOffset = mean
        'Set Strip line item
        Chart1.ChartAreas("ChartArea1").AxisY.StripLines(2).IntervalOffset = median
        'Fill labels
        ' 	TableResults.Rows[0].Cells[1].Text = mean.ToString("G5");
        'TableResults.Rows[1].Cells[1].Text = Math.Sqrt( variance ).ToString("G5");
        'double stdeviation = Math.Sqrt( variance );
        'TableResults.Rows[2].Cells[1].Text = median.ToString("G5");


    End Sub

    Private Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        'Me.ObjectDataSource2.DataBind()
        'Me.Chart1.DataBind()


    End Sub

    Private Sub MakeChart()
        Dim s1 As New Series
        s1.Name = "Serie1"
        s1.ChartType = SeriesChartType.Bar

        Dim ta As New ChartDataSetTableAdapters.DataTable2TableAdapter
        Dim t = ta.GetBarsBySitesTopN(20, Me.txtFrom.Text, Me.txtTo.Text)
        s1.CustomProperties = "DrawingStyle=LightToDark"

        Dim i As Integer

        For i = 0 To t.Count - 1
            Dim dp As New DataPoint
            dp.SetValueXY(t.Item(i).vTitle, t.Item(i).vCount)
            s1.Points.Add(dp)

        Next
        Me.Chart2.ChartAreas("ChartArea1").AxisX.Interval = 1
        Me.Chart2.Series.Add(s1)

        Dim ax_x As Axis = Me.Chart2.ChartAreas("ChartArea1").AxisX
        Dim ax_y As Axis = Me.Chart2.ChartAreas("ChartArea1").AxisY

        ax_y.Title = "Views count"
        

        ax_x.Title = "Domains"
        


    End Sub
    Private Sub MakeChart1()
        Dim s1 As New Series
        s1.Name = "Serie1"
        s1.ChartType = SeriesChartType.Bar
        s1.CustomProperties = "DrawingStyle=LightToDark"
        Dim ta As New ChartDataSetTableAdapters.DataTable2TableAdapter
        Dim t = ta.GetBarsByUsersTopN(20, Me.txtFrom.Text, Me.txtTo.Text)

        Dim i As Integer

        For i = 0 To t.Count - 1
            Dim dp As New DataPoint
            dp.SetValueXY(t.Item(i).vTitle, t.Item(i).vCount)
            s1.Points.Add(dp)

        Next
        Me.Chart3.ChartAreas("ChartArea1").AxisX.Interval = 1
        Me.Chart3.Series.Add(s1)

        Dim ax_x As Axis = Me.Chart3.ChartAreas("ChartArea1").AxisX
        Dim ax_y As Axis = Me.Chart3.ChartAreas("ChartArea1").AxisY

        ax_y.Title = "Views count"
        ax_x.Title = "Users"
        

    End Sub
    Private Sub MakeRequests()
        Dim s1 As New Series
        s1.Name = "Serie1"
        s1.ChartType = SeriesChartType.Column
        s1.XValueType = ChartValueType.Int32
        s1.YValueType = ChartValueType.Int32
        s1.XAxisType = AxisType.Primary
        s1.CustomProperties = "DrawingStyle=LightToDark"

        Dim ta As New ChartDataSetTableAdapters.DataTable2TableAdapter
        Dim t = ta.GetCurrentPendingRequestByDuration()
        Dim i As Integer

        For i = 0 To t.Count - 1
            Dim dp As New DataPoint
            dp.SetValueXY(CType(t.Item(i).vTitle, Integer), t.Item(i).vCount)
            s1.Points.Add(dp)
        Next

        Me.bar_requests.ChartAreas("ChartArea1").AxisX.Interval = 0
        Me.bar_requests.ChartAreas("ChartArea1").AxisX.LabelStyle.IsEndLabelVisible = False
        Me.bar_requests.ChartAreas("ChartArea1").AxisX.IsLabelAutoFit = True
        Me.bar_requests.ChartAreas("ChartArea1").AxisX.LabelAutoFitStyle = LabelAutoFitStyles.DecreaseFont
        Me.bar_requests.ChartAreas("ChartArea1").AxisX.Title = "Days pending"
        Me.bar_requests.ChartAreas("ChartArea1").AxisY.Title = "Requests count"
        Me.bar_requests.ChartAreas("ChartArea1").AxisY.Interval = 1
        Me.bar_requests.ChartAreas("ChartArea1").AxisY.LabelStyle.IsEndLabelVisible = False
        Me.bar_requests.ChartAreas("ChartArea1").AxisY.IsLabelAutoFit = True
        Me.bar_requests.ChartAreas("ChartArea1").AxisY.LabelAutoFitStyle = LabelAutoFitStyles.DecreaseFont

        Me.bar_requests.Series.Add(s1)


    End Sub

    Private Sub MakeChartPubmed()
        Dim ta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
        Dim vAll, vPubmed, vUnPubmed As Integer
        vAll = ta.GetCountAll_ByTwoDates(Me.txtFrom.Text, Me.txtTo.Text)
        vPubmed = ta.GetCountWithPubMed_ByTwoDates(Me.txtFrom.Text, Me.txtTo.Text)
        vUnPubmed = vAll - vPubmed

        Dim dp As New DataPoint
        dp.SetValueXY("ID was not detected", vUnPubmed)

        Dim dp1 As New DataPoint
        dp1.SetValueXY("ID was detected", vPubmed)
        dp1.CustomProperties += "Exploded=true"

        Me.pie_pubmedid.Series("Default").Points.Add(dp)
        Me.pie_pubmedid.Series("Default").Points.Add(dp1)


    End Sub
    Private Sub MakeChartDOI()
        Dim ta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
        Dim vAll, vDOI, vUnDOI As Integer
        vAll = ta.GetCountAll_ByTwoDates(Me.txtFrom.Text, Me.txtTo.Text)
        vDOI = ta.GetCountWithDOI_ByTwoDates(Me.txtFrom.Text, Me.txtTo.Text)
        vUnDOI = vAll - vDOI

        Dim dp As New DataPoint
        dp.SetValueXY("ID was not detected", vUnDOI)

        Dim dp1 As New DataPoint
        dp1.SetValueXY("ID was detected", vDOI)
        dp1.CustomProperties += "Exploded=true"

        Me.pie_doi.Series("Default").Points.Add(dp)
        Me.pie_doi.Series("Default").Points.Add(dp1)


    End Sub

    Private Sub Chart1_Customize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart1.Customize
        MakeChartViewsHistory()
    End Sub
End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master.Master"
    CodeBehind="ManageUsers.aspx.vb" Inherits="BioWS.ManageUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" DeleteMethod="Delete"
        InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData"
        TypeName="BioWS.PersonsDataSetTableAdapters.PersonsTableAdapter" UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_Person_ID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="UserName" Type="String" />
            <asp:Parameter Name="FirstName" Type="String" />
            <asp:Parameter Name="LastName" Type="String" />
            <asp:Parameter Name="Patronimic" Type="String" />
            <asp:Parameter Name="RegisterDate" Type="DateTime" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="UserName" Type="String" />
            <asp:Parameter Name="FirstName" Type="String" />
            <asp:Parameter Name="LastName" Type="String" />
            <asp:Parameter Name="Patronimic" Type="String" />
            <asp:Parameter Name="RegisterDate" Type="DateTime" />
            <asp:Parameter Name="Original_Person_ID" Type="Int32" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Person_ID"
        DataSourceID="ObjectDataSource1">
        <Columns>
            <asp:TemplateField HeaderText="Person_ID" InsertVisible="False" 
                SortExpression="Person_ID">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Person_ID") %>'></asp:Label>
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField HeaderText="UserName" SortExpression="UserName">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField HeaderText="FirstName" SortExpression="FirstName">
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField HeaderText="LastName" SortExpression="LastName">
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("LastName") %>'></asp:Label>
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Patronimic" SortExpression="Patronimic">
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("Patronimic") %>'></asp:Label>
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField HeaderText="RegisterDate" SortExpression="RegisterDate">
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("RegisterDate") %>'></asp:Label>
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField>
            
                <ItemTemplate>
                
                
                    <asp:ImageButton ID="imb_viewstat" runat="server" PostBackUrl  ='<%#  "~\Account\Admin\ViewStatByUser.aspx?Person_ID=" &  Eval("Person_ID") %>' />
                    </ItemTemplate>
                <ItemStyle Width ="100px" />
            
            
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>

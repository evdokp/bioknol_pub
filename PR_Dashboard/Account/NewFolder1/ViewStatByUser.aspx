﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master.Master"
    CodeBehind="ViewStatByUser.aspx.vb" Inherits="BioWS.ViewStatByUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset class="register">
        <legend>Statistics</legend>
        <asp:MultiView ID="mv_charts" runat="server">
            <asp:View ID="v_charts" runat="server">
                from
                <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                to
                <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
                <asp:Button ID="btnRefresh" runat="server" Text="Refresh" />
                <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetViewsCountByPersonIDbyDatebyTwoDates" TypeName="BioWS.ChartDataSetTableAdapters.DataTable1TableAdapter">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="Person_ID" QueryStringField="Person_ID" Type="Int32" />
                        <asp:Parameter Name="StartDate" Type="DateTime" />
                        <asp:Parameter Name="EndDate" Type="DateTime" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:Chart ID="Chart1" runat="server" DataSourceID="ObjectDataSource2" Width="840px">
                    <Series>
                        <asp:Series ChartType="Spline" Name="Series1" XValueMember="vDate" YValueMembers="vCount"
                            BorderWidth="3">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1">
                            <AxisY>
                                <MajorGrid LineColor="LightGray" />
                            </AxisY>
                            <AxisX>
                                <MajorGrid LineColor="LightGray" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <asp:Title Name="Title1" Text="Views count by date" BackColor="#9eb8d3" BackGradientStyle="LeftRight"
                            BorderColor="Silver">
                        </asp:Title>
                    </Titles>
                </asp:Chart>
                <br />
                <asp:Chart ID="Chart2" runat="server" Width="840px">
                    <Titles>
                        <asp:Title Name="Title1" Text="Top 20 sites by views count" BackColor="#9eb8d3" BackGradientStyle="LeftRight"
                            BorderColor="Silver">
                        </asp:Title>
                    </Titles>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1">
                            <AxisY>
                                <MajorGrid LineColor="LightGray" />
                            </AxisY>
                            <AxisX>
                                <MajorGrid LineColor="LightGray" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
                <br />
            </asp:View>
            <asp:View ID="v_nocharts" runat="server">
                There is no data to display.
            </asp:View>
        </asp:MultiView>
    </fieldset>
</asp:Content>

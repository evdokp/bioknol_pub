﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master.Master" CodeBehind="PlanTasks.aspx.vb" Inherits="BioWS.PlanTasks" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">



<h1>Plan tasks</h1>

<h3>Add PubMed extraction tasks</h3>
    Start from <asp:TextBox ID="txt_pubmed_from" runat="server"></asp:TextBox>
    <br />
    for <asp:TextBox ID="txt_pubmed_days" runat="server"></asp:TextBox> days.
     <br />
    <asp:Button ID="btn_pubmed_add" runat="server" Text="Add" /><br />
    <span style ="color:Green ;"><asp:Literal ID="lit_pubmed_result" runat="server"></asp:Literal></span>


    
    <h3>View unexecuted tasks</h3>
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" DeleteMethod="Delete" 
        InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" 
        SelectMethod="GetDataUnexecuted" 
        TypeName="BioWS.TasksDataSetTableAdapters.TasksToDoTableAdapter" 
        UpdateMethod="Update">
    <DeleteParameters>
        <asp:Parameter Name="Original_ID" Type="Int32" />
    </DeleteParameters>
    <InsertParameters>
        <asp:Parameter Name="TastTypeID" Type="Int32" />
        <asp:Parameter Name="ScheduledDateTime" Type="DateTime" />
        <asp:Parameter Name="Done" Type="Boolean" />
    </InsertParameters>
    <UpdateParameters>
        <asp:Parameter Name="TastTypeID" Type="Int32" />
        <asp:Parameter Name="ScheduledDateTime" Type="DateTime" />
        <asp:Parameter Name="Done" Type="Boolean" />
        <asp:Parameter Name="Original_ID" Type="Int32" />
    </UpdateParameters>
    </asp:ObjectDataSource>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="ObjectDataSource1">
        <Columns>
            <asp:TemplateField HeaderText="ID" InsertVisible="False" SortExpression="ID">
                
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Task" SortExpression="TastTypeID">
                
                <ItemTemplate>
                    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                    
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ScheduledDateTime" HeaderText="ScheduledDateTime" 
                SortExpression="ScheduledDateTime" />
        </Columns>
    </asp:GridView>

</asp:Content>

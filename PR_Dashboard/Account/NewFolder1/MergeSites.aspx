﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master.Master"
    CodeBehind="MergeSites.aspx.vb" Inherits="BioWS.MergeSites" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    

    
    <table width="100%">
        <tr>
            <td style="width: 50%;">
                <h3>
                    Select basic site</h3>
                <asp:ListBox ID="lb_1" runat="server" Height="300px" DataSourceID="ods_1" DataTextField="SiteURL"
                    DataValueField="ID" Width="344px"   ></asp:ListBox>
                <asp:ObjectDataSource ID="ods_1" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetDataOrdered" TypeName="BioWS.MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter">
                </asp:ObjectDataSource>
            </td>
            <td style="width: 50%;">
                <h3>
                    Select duplicates to be merged</h3>
                <asp:ListBox ID="lb_2" runat="server" Height="300px" DataSourceID="ods_2" DataTextField="SiteURL"
                    DataValueField="ID" Width="344px" SelectionMode="Multiple"></asp:ListBox>
                <asp:ObjectDataSource ID="ods_2" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetDataOrdered" TypeName="BioWS.MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter">
                </asp:ObjectDataSource>
            </td>
        </tr>
    </table><br />
    <asp:Button ID="btn_merge" runat="server" Text="Remove duplicates" /><br /><br />
    <asp:Button ID="Button1" runat="server" Text="Extract domain" /><br /><br />
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        
            </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>

﻿Imports System.Web.UI.DataVisualization.Charting
Imports DevExpress.Web
Public Class ViewsHistoryStatistics
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Me.Response.Redirect("~/AccountNew/ViewStatistics.aspx?objid=" & Request.QueryString("PersonID") & "&objtype=p")




            'Dim pers As New BLL.Person(CType(Request.QueryString("PersonID"), Integer))
            'CType(Master.Master.FindControl("lit_pagetitle"), Literal).Text = "History of article views - Statistics"
            'CType(Master.Master.FindControl("lit_curobj"), Literal).Text = pers.PageHeader
            'Dim pr As New ProfileCommon
            'pr = pr.GetProfile(HttpContext.Current.User.Identity.Name)

            'BindStatisticsPage(Today.AddDays(-pr.DefaultPeriodDays), Today)

        End If
    End Sub



    Private Sub BindStatisticsPage(ByVal StartDate As DateTime, ByVal EndDate As DateTime)
        ASPxSplitter1.Panes(0).ContentUrl = String.Format(cmbStatPage.SelectedItem.Value & "?Type=Person&ObjectID={0}&StartDate={1}&EndDate={2}",
                                                   Request.QueryString("PersonID"),
                                                   StartDate,
                                                   EndDate)
    End Sub



    Private Sub SetPeriod1_PeriodChanged(StartDate As Date, EndDate As Date) Handles SetPeriod1.PeriodChanged
        BindStatisticsPage(StartDate, EndDate)
    End Sub

    Private Sub cmbStatPage_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cmbStatPage.SelectedIndexChanged
        BindStatisticsPage(SetPeriod1.StartDate, SetPeriod1.EndDate)
    End Sub
End Class
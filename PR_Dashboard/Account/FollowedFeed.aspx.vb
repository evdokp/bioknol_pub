﻿Imports BioWS.BLL.Extensions

Public Class FollowedFeed
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim pers As New BLL.Person(CType(Request.QueryString("PersonID"), Integer))
        CType(Master.Master.FindControl("lit_pagetitle"), Literal).Text = pers.PageHeader
        CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "Followed feed"

        Me.hfPageOpenDateTime.Value = Now.ToString
        Me.hfPersonID.Value = Request.QueryString("PersonID")
    End Sub



    <System.Web.Services.WebMethod()> _
    Public Shared Function GetFeed(ByVal PersonID As Integer, ByVal EarlierThen As String,
                                    ByVal LastIndex As Integer) As String


        Dim dt As Date
        DateTime.TryParse(EarlierThen, dt)

        Dim db = New DataClassesFeedDataContext


        Dim tt = From c In db.GetActivityFeedFollowed(20, EarlierThen, LastIndex, PersonID)
                Select vZero = 0, vHTML = BLL.Feed.FeedHelper.RenderAction(c.Action, c.ActionTime, c.ActionType)


        Return tt.ToList().ToJSON

    End Function
End Class
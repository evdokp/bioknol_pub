﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Person.Master"  
    CodeBehind="ViewsHistoryStatistics.aspx.vb" Inherits="BioWS.ViewsHistoryStatistics" %>

<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxNavBar" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp1" %>


<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSplitter" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register src="../UserControls/SetPeriod.ascx" tagname="SetPeriod" tagprefix="uc1" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    


         
    <table width="100%">
    <tr>
    <td style="width:100px;">
    <uc1:SetPeriod ID="SetPeriod1" runat="server" />
    </td>
    <td style="padding-left:10px;">
    
        <dx:ASPxComboBox ID="cmbStatPage" runat="server"  Width="100%" AutoPostBack="true" 
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Spacing="0" 
            SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" 
            ValueType="System.String" SelectedIndex="0">
            <Items>
                <dx:ListEditItem Text="Views and distinct Pubmed IDs timeline" 
                    Value="Statistics/ViewsTimeSeries.aspx" Selected="True"  />
                <dx:ListEditItem Text="MESH-headings distribution" Value="Statistics/MESHDistribution.aspx" />
                <dx:ListEditItem Text="Authors distribution" Value="Statistics/AuthorsDistribution.aspx" />
                <dx:ListEditItem Text="Journal distribution" Value="Statistics/JournalsDistribution.aspx" />
                <dx:ListEditItem  Text="Countries distribution"  Value="Statistics/CountriesDistribution.aspx"/>
                <dx:ListEditItem Text="Publication years distribution" Value="Statistics/PubYearsDistribution.aspx" />
                <dx:ListEditItem Text="Domains distribution" Value="Statistics/DomainsDistribution.aspx" />
            </Items>
            <LoadingPanelImage Url="~/App_Themes/DevEx/Editors/Loading.gif">
            </LoadingPanelImage>
            <LoadingPanelStyle ImageSpacing="5px">
            </LoadingPanelStyle>
            <ButtonStyle Width="13px">
            </ButtonStyle>
        </dx:ASPxComboBox>
    </td>
    </tr>
    </table>
    



                                        <dx:ASPxSplitter ID="ASPxSplitter1" runat="server" ClientIDMode="AutoID" CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css"  
                                     
            CssPostfix="Office2010Silver" Height="550px">
            <panes>
            <dx:SplitterPane ContentUrlIFrameName="f1"  PaneStyle-Border-BorderColor ="LightGray"  PaneStyle-Border-BorderStyle="Solid" ScrollBars="Auto"   >
                <ContentCollection>
                    <dx:SplitterContentControl ID="SplitterContentControl2" runat="server" SupportsDisabledAttribute="True"  >
                      
                    </dx:SplitterContentControl>
                </ContentCollection>
            </dx:SplitterPane>
        </panes>
            <styles cssfilepath="~/App_Themes/Office2010Silver/{0}/styles.css" csspostfix="Office2010Silver">
        </styles>
            <images spritecssfilepath="~/App_Themes/Office2010Silver/{0}/sprite.css">
        </images>
        </dx:ASPxSplitter>

     

</asp:Content>

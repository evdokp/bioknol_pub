﻿Imports System.Globalization
Imports BioWS.BLL.Extensions

Public Class ViewGroup
    Inherits System.Web.UI.Page

    Private ReadOnly Property GroupID As Integer
        Get
            Return Me.QSInt32("GroupID")
        End Get
    End Property

    Private ReadOnly Property IsUserAuthenticated() As Boolean
        Get
            Return Me.IsAuthenticated()
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim gr As New BLL.Group(GroupID)
        Me.SetTitlesToGrandMaster(gr.Title, "Group information")

        lit_desc.Text = gr.Description


        pnl_parent.Visible = gr.HasParent
        If gr.HasParent Then
            hfParentGroupID.Value = gr.ParentGroup_ID
        End If

        pnl_child.Visible = gr.DirectChildGroupsCount > 0


        'person participation
        pnl_part.Visible = IsUserAuthenticated()
        If IsUserAuthenticated() Then
            btn_Join.Text = IIf(gr.ParticipationLinksCount(Me.GetProfilePersonID) = 0, "Join", "Add entry")
        End If



        If Me.NotPostBack() Then
            txt_start.Text = Today.ToShortDateString
            Dim adm As New BLL.Person(gr.AddedByPersonID)
            hl_admin.Text = adm.DisplayName
            hl_admin.NavigateUrl = adm.URL
            Dim IsSamePerson As Boolean = gr.AddedByPersonID = Me.GetProfilePersonID()
            sp_admin.Visible = IsSamePerson
            If IsSamePerson Then
                hl_edit.NavigateUrl = "~\Account\AddEditGroup.aspx?GroupID=" & gr.Group_ID 'TODO - edit url as group property
            End If
        End If




    End Sub




    Private Sub btnAddPart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPart.Click
        Dim ta As New PersonsDataSetTableAdapters.LinksGroupsToPersonsTableAdapter
        
        Dim vEndDate As Date = IIf(txt_end.Text.IsNullOrEmpty(), BLL.Common.constants.vNullDate, txt_end.Text)
        
        Dim feedaction As New BLL.Feed.FeedActionGroupParticipation(Request.QueryString("GroupID"), Me.txt_start.Text, vEndDate)
        feedaction.Save(Me.GetProfilePersonID())

        ta.Insert(Me.QSInt32("GroupID"), Me.GetProfilePersonID(), txt_start.Text, vEndDate)
        txt_end.Text = String.Empty
        txt_start.Text = String.Empty
        mv1.ActiveViewIndex = 1
        gv_part.DataBind()
    End Sub

    Private Sub btnOneMore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOneMore.Click
        mv1.ActiveViewIndex = 0
    End Sub


    Private Sub gv_part_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gv_part.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim p_row As PersonsDataSet.LinksGroupsToPersonsRow
            p_row = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row, PersonsDataSet.LinksGroupsToPersonsRow)
            Dim lit_ToDate As Literal = e.Row.Cells(1).FindControl("lit_ToDate")
            lit_ToDate.Text = IIf(p_row.ToDate = BLL.Common.constants.vNullDate, "now", p_row.ToDate.ToShortDateString)
        End If
    End Sub

    Protected Sub gv_part_RowDeleted(sender As Object, e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles gv_part.RowDeleted
        Dim feedaction As New BLL.Feed.FeedActionGroupParticipation(Me.QSInt32("GroupID"), BLL.Common.constants.vNullDate, BLL.Common.constants.vNullDate)
        feedaction.Save(Me.GetProfilePersonID())
    End Sub

    
End Class
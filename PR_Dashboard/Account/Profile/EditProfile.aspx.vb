﻿Imports System.Xml
Imports System.IO
Imports AzureStorageLibrary
Imports DevExpress.Web.ASPxUploadControl





Public Class EditProfile
    Inherits System.Web.UI.Page
    Private Sub EditProfile_PreInit(sender As Object, e As System.EventArgs) Handles Me.PreInit
        If Not String.IsNullOrEmpty(Request.QueryString("finreg")) Then
            Me.Page.MasterPageFile = "~/Basic.master"
        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            Dim p As New ProfileCommon
            p = p.GetProfile(HttpContext.Current.User.Identity.Name)
            Dim pers As New BLL.Person(p.Person_ID)

            CType(Master.Master.FindControl("lit_pagetitle"), Literal).Text = pers.PageHeader
            CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "Edit profile"
            If Not String.IsNullOrEmpty(Request.QueryString("finreg")) Then
                Me.pnlFinReg.Visible = True
            Else
                Me.pnlFinReg.Visible = False
            End If

            If pers.HasImage Then
                Image1.ImageUrl = pers.Image200
            Else
                Image1.ImageUrl = "~/Images/unknownUser.gif"
            End If

            txtFirstName.Text = pers.FirstName
            txtLastName.Text = pers.LastName
            txtPatronimic.Text = pers.Patronimic

        End If
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)
        Dim pers As New BLL.Person(p.Person_ID)

        pers.LastName = txtLastName.Text
        pers.FirstName = txtFirstName.Text
        pers.Patronimic = txtPatronimic.Text

        pers.Save()

        pnlSent.Visible = True
        litSent.Text = "Profile saved"


    End Sub





    Protected Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        If FileUpload1.HasFile = True Then
            Dim forg, f5050, f200200 As Byte()
            Dim originalName As String = Path.GetFileName(FileUpload1.PostedFile.FileName)
            Dim extension As String = Path.GetExtension(originalName)
            forg = New Byte(FileUpload1.PostedFile.InputStream.Length) {}
            FileUpload1.PostedFile.InputStream.Read(forg, 0, forg.Length)

            Dim isvalid As String = BLL.ImageHelper.ImageUploadValidate(FileUpload1.PostedFile.ContentType, FileUpload1.PostedFile.ContentLength)
            If isvalid = "true" Then
                Dim fileguid As Guid = Guid.NewGuid

                f5050 = BLL.ImageHelper.MakeThumb(forg, 50, 50)
                f200200 = BLL.ImageHelper.MakeThumbMaxWidth(forg, 200)

                Dim storage As New AzureStorage()
                storage.UploadImage(f5050, String.Format("{0}{1}{2}", fileguid, "_50", extension))
                storage.UploadImage(f200200, String.Format("{0}{1}{2}", fileguid, "_200", extension))

                'BLL.ImageHelper.SaveFile(fileguid, "_50", originalName, f5050, Me)
                'BLL.ImageHelper.SaveFile(fileguid, "_200", originalName, f200200, Me)

                Dim p As New ProfileCommon
                p = p.GetProfile(HttpContext.Current.User.Identity.Name)
                Dim pers As New BLL.Person(p.Person_ID)
                pers.UpdatePersonImage(fileguid.ToString, Path.GetExtension(originalName).Substring(1))

                Image1.ImageUrl = pers.Image200
                Dim ct1 As ContentPlaceHolder = CType(Master.Master.FindControl("MainContent"), ContentPlaceHolder)
                CType(ct1.FindControl("imgAvatar"), Image).Visible = True
                CType(ct1.FindControl("imgAvatar"), Image).ImageUrl = pers.Image200

            Else
                Me.lit_error.Text = "<br/>" & isvalid
            End If


        End If
    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim p As New ProfileCommon
        p = p.GetProfile(HttpContext.Current.User.Identity.Name)
        Dim pers As New BLL.Person(p.Person_ID)
        pers.DeleteImageAzure()

        Image1.ImageUrl = "~/Images/unknownUser.gif"
        Dim ct1 As ContentPlaceHolder = CType(Master.Master.FindControl("MainContent"), ContentPlaceHolder)
        CType(ct1.FindControl("imgAvatar"), Image).Visible = False


    End Sub


End Class
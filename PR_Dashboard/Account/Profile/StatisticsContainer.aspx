﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="StatisticsContainer.aspx.vb" Inherits="BioWS.StatisticsContainer" %>

<%@ Register assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxSplitter" tagprefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <dx:ASPxSplitter ID="ASPxSplitter1" runat="server" FullscreenMode="True" 
             Width="100%" >
            <panes>
                <dx:SplitterPane MaxSize="300px">
                    <contentcollection>
                        <dx:SplitterContentControl runat="server" SupportsDisabledAttribute="True">
                        .1212
                        </dx:SplitterContentControl>
                    </contentcollection>
                </dx:SplitterPane>
                <dx:SplitterPane>
                    <Panes>
                        <dx:SplitterPane MaxSize="150px">
                            <ContentCollection>
                                <dx:SplitterContentControl runat="server" SupportsDisabledAttribute="True">
                                </dx:SplitterContentControl>
                            </ContentCollection>
                        </dx:SplitterPane>
                        <dx:SplitterPane>
                            <ContentCollection>
                                <dx:SplitterContentControl runat="server" SupportsDisabledAttribute="True">
                                </dx:SplitterContentControl>
                            </ContentCollection>
                        </dx:SplitterPane>
                    </Panes>
                    <ContentCollection>
<dx:SplitterContentControl runat="server" SupportsDisabledAttribute="True">
aaaa
</dx:SplitterContentControl>
</ContentCollection>
                </dx:SplitterPane>
            </panes>
        </dx:ASPxSplitter>
    
    </div>
    </form>
</body>
</html>

﻿Imports BioWS.BLL.DelayedParsing
Imports System.Web.Services

Public Class Parser
    Inherits System.Web.UI.Page

    Private ReadOnly Property PasingManager As BLL.DelayedParsing.SavedParserManager
        Get
            Return BLL.DelayedParsing.SavedParserHelper.ParserManager
        End Get
    End Property
    Private ReadOnly Property PasingManagerDefined As Boolean
        Get
            Return BLL.DelayedParsing.SavedParserHelper.ParserManager IsNot Nothing
        End Get
    End Property

    Private Sub SetInitialTitles()
        CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "Saved HTML parser"
        CType(Master.Master.FindControl("page_header"), HtmlGenericControl).Visible = False
        dStart.Date = Today.AddDays(-2)
        dEnd.Date = Today.AddDays(-1)
        hfCurStatus.Value = CInt(BLL.DelayedParsing.SavedParserHelper.ParserManagerStatus).ToString
        If BLL.DelayedParsing.SavedParserHelper.ParserManagerStatus <> SavedParserHelper.ParserManagerStatuses.Undefined Then
            hfCurParser.Value = BLL.DelayedParsing.SavedParserHelper.ParserManager.GetStatusString()
        End If



        If PasingManagerDefined Then
            SetTreeListFields(listParsing)
            SetTreeListFields(ASPxTreeList1)
            DataBindTreeList(PasingManager.PreparsingDictionary, ASPxTreeList1)
            DataBindTreeList(PasingManager.ParsingDictionary, listParsing)
        End If


    End Sub

    Private Shared Sub DataBindTreeList(ByVal input As Dictionary(Of String, Situation), ByVal list As DevExpress.Web.ASPxTreeList.ASPxTreeList)
        list.DataSource = input.Values
        list.DataBind()
        list.ExpandAll()
    End Sub
    Private Shared Sub SetTreeListFields(ByVal list As DevExpress.Web.ASPxTreeList.ASPxTreeList)
        list.KeyFieldName = "Key"
        list.PreviewFieldName = "Title"
        list.ParentFieldName = "ParentKey"
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SetInitialTitles()
    End Sub

    <WebMethod()> _
    Public Shared Function GetStatus() As String
        Return String.Format("{0:HH:mm:ss} {1}", Now, BLL.DelayedParsing.SavedParserHelper.GetStatus())
    End Function

    <WebMethod()> _
    Public Shared Function GetStatusID() As String
        Return CInt(BLL.DelayedParsing.SavedParserHelper.ParserManagerStatus).ToString
    End Function

    <WebMethod()> _
    Public Shared Function ResetParser() As String
        BLL.DelayedParsing.SavedParserHelper.ParserManager = Nothing
        Return String.Empty
    End Function

    <WebMethod()> _
    Public Shared Function Run() As String
        BLL.DelayedParsing.SavedParserHelper.ParserManager.StartProcessing()
        Return String.Empty
    End Function

    <WebMethod()> _
    Public Shared Function DefineParser(ByVal dt1 As DateTime, ByVal dt2 As DateTime, ByVal dtOffset As Integer) As String
        dt1 = dt1.AddMinutes(-dtOffset)
        dt2 = dt2.AddMinutes(-dtOffset)

        Dim parser As New BLL.DelayedParsing.SavedParserManager(dt1, dt2)
        BLL.DelayedParsing.SavedParserHelper.ParserManager = parser
        Dim s As String = parser.GetStatusString()
        Return s
    End Function

    Private Sub cpCurrentParser_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cpCurrentParser.Callback
        SetTreeListFields(ASPxTreeList1)
        DataBindTreeList(PasingManager.PreparsingDictionary, ASPxTreeList1)
    End Sub

    Private Sub cpParsing_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cpParsing.Callback
        SetTreeListFields(listParsing)
        DataBindTreeList(PasingManager.ParsingDictionary, listParsing)
    End Sub

    Private Sub cpDomains_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cpDomains.Callback
        gridDomains.DataSource = PasingManager.GetDomains(e.Parameter)
        gridDomains.DataBind()
    End Sub
End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Basic.Master"
    ValidateRequest="false" CodeBehind="Parser.aspx.vb" Inherits="BioWS.Parser" %>

<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/SavedParser/SavedParserManager.js" type="text/javascript"></script>
    <script type="text/javascript">
        var mng;
        $(document).ready(function () {
            mng = new SavedParserManager($('#hfCurStatus').val(), $('#hfCurParser').val());

            Check();
            $('#btnStart').click(function () {
                $('status').html('');
            });
            $('#btnDefine').click(function () {
                var tmpDT = new Date();
                var dtOffset = tmpDT.getTimezoneOffset();
                mng.DefineParser(dt1.GetValue(), dt2.GetValue(), dtOffset);
            });
            $('#btnReset').click(function () {
                if (btnReset.GetEnabled()) {
                    mng.ResetParser();
                }
            })

            $('#btnFinish').click(function () {
                if (btnFinish.GetEnabled()) {
                    mng.ResetParser();
                }
            })

            $('#btnRun').click(function () {
                if (btnRun.GetEnabled()) {
                    mng.Run();
                    btnReset.SetEnabled(false);
                    btnRun.SetEnabled(false);
                }
            })
        });


        function Check() {
            mng.Check();
            setTimeout(Check, 1000);
        }

        function parsingRowChanged(s, e) {
            GetDomains(listParsing.GetFocusedNodeKey())
        }
        function preparsingRowChanged(s, e) {
            GetDomains(listPreparsing.GetFocusedNodeKey())
        }

        function GetDomains(key) {
            $('#dDomains').show();
            cpDomains.PerformCallback(key);
        }

    </script>
    <asp:HiddenField ID="hfCurStatus" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfCurParser" runat="server" ClientIDMode="Static" />
    <asp:Panel ID="pnlDefineParser" runat="server" ClientIDMode="Static">
        <h5>
            <span>Define parsing period </span>
        </h5>
        <table style="padding-left: 20px;">
            <tr>
                <td>
                    <dx:ASPxDateEdit ID="dStart" runat="server" Height="24px" ClientInstanceName="dt1"
                        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Spacing="0"
                        DisplayFormatString="dd.MM.yyyy" EditFormat="Custom" EditFormatString="dd.MM.yyyy"
                        UseMaskBehavior="True" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                        <CalendarProperties>
                            <HeaderStyle Spacing="1px" />
                        </CalendarProperties>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                    </dx:ASPxDateEdit>
                </td>
                <td>
                    <dx:ASPxDateEdit ID="dEnd" runat="server" Height="24px" ClientInstanceName="dt2"
                        DisplayFormatString="dd.MM.yyyy" EditFormat="Custom" EditFormatString="dd.MM.yyyy"
                        UseMaskBehavior="True" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx"
                        Spacing="0" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                        <CalendarProperties>
                            <HeaderStyle Spacing="1px" />
                        </CalendarProperties>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                    </dx:ASPxDateEdit>
                </td>
                <td>
                    <dx:ASPxButton ID="btnDefine" runat="server" Text="Define" ClientIDMode="Static"
                        AutoPostBack="false" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx"
                        SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td valign="top" style="width: 600px;">
                <div id="dCurrentParser" style="display: none;" >
                    <h5>
                        <span>Current parser</span>
                    </h5>
                    <div style="padding-left: 20px;">
                        <div id="divCurParser" style="background-color: #31496e; color: White; width: 590px;
                            padding: 5px; margin-bottom: 5px;">
                            Current parser</div>
                        <dx:ASPxCallbackPanel ID="cpCurrentParser" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                            ClientInstanceName="cpCurrentParser" CssPostfix="DevEx" Width="100%">
                            <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
                            </LoadingPanelImage>
                            <LoadingPanelStyle ImageSpacing="5px">
                            </LoadingPanelStyle>
                            <PanelCollection>
                                <dx:PanelContent runat="server" SupportsDisabledAttribute="True">
                                    <dx:ASPxTreeList ID="ASPxTreeList1" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                                        ClientInstanceName="listPreparsing" CssPostfix="DevEx" Width="600px" 
                                        AutoGenerateColumns="False">
                                        <Columns>
                                            <dx:TreeListTextColumn Caption="Title" FieldName="Title" 
                                                ShowInCustomizationForm="True" VisibleIndex="0">
                                            </dx:TreeListTextColumn>
                                            <dx:TreeListTextColumn Caption="Count before" FieldName="BeforeCount" 
                                                ShowInCustomizationForm="True" VisibleIndex="1">
                                            </dx:TreeListTextColumn>
                                            <dx:TreeListTextColumn Caption="Count after" FieldName="AfterCount" 
                                                ShowInCustomizationForm="True" VisibleIndex="1">
                                            </dx:TreeListTextColumn>
                                        </Columns>
                                        <Settings SuppressOuterGridLines="True" />
                                        <SettingsBehavior AllowFocusedNode="True" AllowSort="False" />
                                        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                            <LoadingPanel Url="~/App_Themes/DevEx/TreeList/Loading.gif">
                                            </LoadingPanel>
                                        </Images>
                                        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                                            <LoadingPanel ImageSpacing="5px">
                                            </LoadingPanel>
                                        </Styles>
                                        <ClientSideEvents FocusedNodeChanged="preparsingRowChanged" />
                                        <StylesEditors ButtonEditCellSpacing="0">
                                        </StylesEditors>
                                    </dx:ASPxTreeList>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxCallbackPanel>
                        <table cellpadding="0" cellspacing="0" style="margin-top: 6px;">
                            <tr>
                                <td>
                                    <dx:ASPxButton ID="btnRun" runat="server" Text="Start parsing" ClientIDMode="Static"
                                        ClientInstanceName="btnRun" AutoPostBack="false" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                                        CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                    </dx:ASPxButton>
                                </td>
                                <td style="padding-left: 5px;">
                                    <dx:ASPxButton ID="btnReset" runat="server" Text="Reset current parser" ClientIDMode="Static"
                                        ClientInstanceName="btnReset" AutoPostBack="false" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                                        CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="dParsing" style="display: none; margin-top:17px; margin-bottom:50px;">
                    <table width="100%" style="margin-bottom:6px;">
                        <tr>
                            <td valign="middle" style="width:16px;">
                                <asp:Image ID="imgParsing" runat="server" ImageUrl="~/Images/parserLoader.gif" ClientIDMode="Static" />
                            </td>
                            <td valign="middle">
                                <h5 style="padding:0px; margin:0px;">
                                    <span>Parsing process and results</span>
                                </h5>
                            </td>
                        </tr>
                    </table>
                    
                    <div style="padding-left: 20px;">
                        <dx:ASPxCallbackPanel ID="cpParsing" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                         ShowLoadingPanel="false"
                            ClientInstanceName="cpParsing" CssPostfix="DevEx" Width="100%">
                            <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
                            </LoadingPanelImage>
                            <LoadingPanelStyle ImageSpacing="5px">
                            </LoadingPanelStyle>
                            <PanelCollection>
                                <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                                    <dx:ASPxTreeList ID="listParsing" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                                        ClientInstanceName="listParsing" CssPostfix="DevEx" Width="600px">
                                        <Settings SuppressOuterGridLines="True" />
                                        <SettingsBehavior AllowFocusedNode="True" AllowSort="False" />
                                        <ClientSideEvents FocusedNodeChanged="parsingRowChanged" />
                                        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                            <LoadingPanel Url="~/App_Themes/DevEx/TreeList/Loading.gif">
                                            </LoadingPanel>
                                        </Images>
                                                <Columns>
                                            <dx:TreeListTextColumn Caption="Title" FieldName="Title" 
                                                ShowInCustomizationForm="True" VisibleIndex="0">
                                            </dx:TreeListTextColumn>
                                            <dx:TreeListTextColumn Caption="Cases count" FieldName="CasesCount" 
                                                ShowInCustomizationForm="True" VisibleIndex="1">
                                            </dx:TreeListTextColumn>
                                        </Columns>
                                        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                                            <LoadingPanel ImageSpacing="5px">
                                            </LoadingPanel>
                                        </Styles>
                                        <StylesEditors ButtonEditCellSpacing="0">
                                        </StylesEditors>
                                    </dx:ASPxTreeList>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxCallbackPanel>
                            <table cellpadding="0" cellspacing="0" style="margin-top: 6px;">
                            <tr>
                                <td>
                                    <dx:ASPxButton ID="btnFinish" runat="server" Text="Finish with selected period" ClientIDMode="Static"
                                        ClientInstanceName="btnFinish" AutoPostBack="false" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                                        CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </td>
            <td valign="top" style="padding-left: 7px;">
                <div id="dDomains" style="display: none;">
                    <h5>
                        <span>Domains for selected situation</span>
                    </h5>
                    <dx:ASPxCallbackPanel ID="cpDomains" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                        ClientInstanceName="cpDomains" CssPostfix="DevEx" Width="100%">
                        <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
                        </LoadingPanelImage>
                        <LoadingPanelStyle ImageSpacing="5px">
                        </LoadingPanelStyle>
                        <PanelCollection>
                            <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
                                <dx:ASPxGridView ID="gridDomains" runat="server" Width="100%" Caption="Domains" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                                    CssPostfix="DevEx" AutoGenerateColumns="False">
                                    <Columns>
                                        <dx:GridViewDataHyperLinkColumn Caption="Domain" FieldName="ID" Name="Domain" ShowInCustomizationForm="True"
                                            VisibleIndex="0">
                                            <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Account/Admin/EditDomain.aspx?DomainID={0}"
                                                Target="_blank" TextField="Domain">
                                            </PropertiesHyperLinkEdit>
                                            <CellStyle HorizontalAlign="Right">
                                            </CellStyle>
                                        </dx:GridViewDataHyperLinkColumn>
                                        <dx:GridViewDataTextColumn FieldName="Count" ShowInCustomizationForm="True" VisibleIndex="1">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsBehavior AllowSort="False" />
                                    <SettingsPager PageSize="1000">
                                    </SettingsPager>
                                    <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                        <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                                        </LoadingPanelOnStatusBar>
                                        <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                                        </LoadingPanel>
                                    </Images>
                                    <ImagesFilterControl>
                                        <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                                        </LoadingPanel>
                                    </ImagesFilterControl>
                                    <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                        </Header>
                                        <LoadingPanel ImageSpacing="5px">
                                        </LoadingPanel>
                                    </Styles>
                                    <StylesEditors ButtonEditCellSpacing="0">
                                        <ProgressBar Height="21px">
                                        </ProgressBar>
                                    </StylesEditors>
                                </dx:ASPxGridView>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxCallbackPanel>
                </div>
            </td>
        </tr>
    </table>
    <%--<div id="info" style="border:2px solid red; padding:10px;"> </div>--%>
</asp:Content>

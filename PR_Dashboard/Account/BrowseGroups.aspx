﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Basic.Master"
    CodeBehind="BrowseGroups.aspx.vb" Inherits="BioWS.BrowseGroups" EnableEventValidation="false" %>

<%@ Register Src="../UserControls/uc_GroupItem.ascx" TagName="uc_GroupItem" TagPrefix="uc1" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
    <%@ Register assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxLoadingPanel" tagprefix="dx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function ShowResult(obj) {
            $("#hfLastIndex").val(parseInt($("#hfLastIndex").val()) + obj.length);
            $("#tmplGroup").tmpl(obj).appendTo("#divSearch");
            $("#divSearch .group:first").css('border-top', 'none');
            if (obj.length < 10) {
                $("#btnShowMore").hide();
            }
        };


        $(document).ready(function () {
            $("#hfLastIndex").val(0);
            $("#btnShowMore").hide();

            $('#btnFind').click(function () {
                $("#btnShowMore").show();
                $("#hfLastIndex").val(0);
                $("#divMyGroups").html("");
                $("#divPartGroups").html("");
                $('#h4mygroups').hide();
                $('#h4partgroups').hide();
                $('#divMyGroups').hide();
                $('#divPartGroups').hide();
                $('#h4Search').show();
                lpSearch.Show();
                PageMethods.GetSearch(txtCondition.GetValue(), $("#hfPageOpenDateTime").val(), $("#hfLastIndex").val(), function (response) {
                    lpSearch.Hide();
                    $("#divSearch").html("");
                    var obj = jQuery.parseJSON(response);
                    if (obj.length == 0) {
                        $("#btnShowMore").hide();
                        $('#divSearchEDT').show();
                    } else {
                        $('#divSearchEDT').hide();
                        ShowResult(obj);
                    }
                });
            });


            $("#btnShowMore").click(function () {
                $('#btnShowMore').attr('disabled', 'disabled');
                lpSearch.Show();
                PageMethods.GetSearch(txtCondition.GetValue(), $("#hfPageOpenDateTime").val(), $("#hfLastIndex").val(), function (response) {
                    $('#btnShowMore').removeAttr('disabled');
                    lpSearch.Hide();
                    var obj = jQuery.parseJSON(response);
                    ShowResult(obj);
                });
            });



            lpMyGroups.Show();
            PageMethods.GetMyGroups(function (response) {
                $("#divMyGroups").html("");
                var obj = jQuery.parseJSON(response);
                if (obj.length == 0) {
                    $('#h4mygroups').hide();
                    $('#divMyGroups').hide();
                } else {
                    $("#tmplGroup").tmpl(obj).appendTo("#divMyGroups");
                    $("#divMyGroups .group:first").css('border-top', 'none');
                }
                lpMyGroups.Hide();
            });

            lpPartGroups.Show();
            PageMethods.GetGroupsPart(function (response) {
                $("#divPartGroups").html("");
                var obj = jQuery.parseJSON(response);
                if (obj.length == 0) {
                    $('#h4partgroups').hide();
                    $('#divPartGroups').hide();
                } else {
                    $("#tmplGroup").tmpl(obj).appendTo("#divPartGroups");
                    $("#divPartGroups .group:first").css('border-top', 'none');
                }
                lpPartGroups.Hide();
            });

        });

    

        
    </script>
           <asp:HiddenField ID="hfLastIndex" runat="server" ClientIDMode="Static"  />
               <asp:HiddenField ID="hfPageOpenDateTime" runat="server" ClientIDMode="Static"  />
               <asp:HiddenField ID="hfCondition" runat="server" ClientIDMode="Static"  />
    <table width="100%">
        <tr>
            <td style="width: 70%; padding-right: 20px;" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <dx:ASPxTextBox ID="txtCondition" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" ClientIDMode="Static"  ClientInstanceName="txtCondition"
                                CssPostfix="DevEx" NullText="Enter query here to find groups by title or description"
                                SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Width="100%" Height="20px">
                                <Paddings Padding="3px" />
                            </dx:ASPxTextBox>
                        </td>
                        <td style="width: 70px; padding-left: 4px;">
                            <dx:ASPxButton ID="btnFind" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" AutoPostBack="false"  ClientIDMode="Static" 
                                CssPostfix="DevEx" HorizontalAlign="Center" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"
                                Width="20px" Height="24px" Text="Find">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
                <h5 id="h4mygroups">
                    <span>Groups I own or can administer</span>
                </h5>
                    <dx:ASPxLoadingPanel ID="lp1" runat="server"  ClientInstanceName="lpMyGroups"
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx"  
        ImageSpacing="5px" ContainerElementID="divMyGroups" Modal="True">
        <Image Url="~/App_Themes/DevEx/Web/Loading.gif">
        </Image>
    </dx:ASPxLoadingPanel>
                <div id="divMyGroups">
                <br /><br /><br /><br /><br /><br /><br />
                </div>
                
                <h5 id="h4partgroups">
                    <span>Groups I'm member of</span>
                </h5>
                    <dx:ASPxLoadingPanel ID="lp2" runat="server"  ClientInstanceName="lpPartGroups"
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx"  
        ImageSpacing="5px" ContainerElementID="divMyGroups" Modal="True">
        <Image Url="~/App_Themes/DevEx/Web/Loading.gif">
        </Image>
    </dx:ASPxLoadingPanel>
                <div id="divPartGroups">
                <br /><br /><br /><br /><br /><br /><br />
                </div>

                 <h5 id="h4Search" style="display:none;">
                    <span>Search results</span>
                </h5>

                 <div id="divSearch">
                <br /><br /><br /><br /><br /><br /><br />
                </div>
                <div id="divSearchEDT" style="font-family:Verdana; font-size:8pt; font-style:italic; text-align:center; display:none; color:#777777;">
                Sorry, your search did not return any results
                </div>
                    <dx:ASPxLoadingPanel ID="lp3" runat="server"  ClientInstanceName="lpSearch"
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
        ImageSpacing="5px" ContainerElementID="divSearch" Modal="True">
        <Image Url="~/App_Themes/DevEx/Web/Loading.gif">
        </Image>
    </dx:ASPxLoadingPanel>
 
    <br />
     <dx:ASPxButton ID="btnShowMore" runat="server"  AutoPostBack="false"  ClientIDMode="Static" 
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
        Height="29px" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" 
        Text="Show more" Width="100%">
    </dx:ASPxButton>






                <script id="tmplGroup" type="text/x-jquery-tmpl">
                <div class="group" style="${PaddingLeft}">
                <table width="100%">
                <tr>
                <td valign="top" style="width:60px;" class="group_img">
                <div>
                <img src="${Avatar}" />
                </div>
                </td>
                <td valign="top" style="padding-left:15px;">
                <a href="ViewGroup.aspx?GroupID=${Group_ID}">${Title}</a>
                
             <div class="groupdesc">
                  {{html Description}}
                
                  <div style="padding-right:40px; padding-top:3px;">
                    {{tmpl(MESH) "#tmplMESHs"}}
                  </div>
                      <div class="clear"></div>
                  <div style="float:right; margin-top:5px; text-align:right;">
                  {{if (AllowEdit == 1)}}
                    <a href="AddEditGroup.aspx?GroupID=${Group_ID}">Edit group</a>
                  {{/if}}
                  {{if (AllowEdit == 1) && (ParticipantsCount > 0)}}
                  &nbsp;&sdot;&nbsp;
                  {{/if}}
                  {{if (ParticipantsCount == 0)}}{{else (ParticipantsCount == 1)}}<b>${ParticipantsCount}</b> member{{else}}<b>${ParticipantsCount}</b> members{{/if}} 
               
                    {{if (ShowParticipation == 1)}}
                  <br/><span style="color:gray;">
                             {{if (DirectParticipant == 1)}}
                             you participate directly
                            {{else}}
                            participation is inherited
                          {{/if}}
                          </span>
                  {{/if}}




                  </div>
                  <div class="clear"></div>
                    </span>
                </td>
                </tr>
                </table>
                     
                </div>
                </script>


                   <script id="tmplMESHs" type="text/x-jquery-tmpl">
                <div class="tag" style="float:left;">
            ${Heading}
                </div>
                </script>
            </td>
            <td style="width: 30%; padding-left: 15px;" valign="top">
            
                <dx:ASPxButton ID="btnCreate" runat="server" 
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    Height="24px" PostBackUrl="~/Account/AddEditGroup.aspx" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Create new group" 
                    Width="100%">
                </dx:ASPxButton>
            
            </td>
        </tr>
    </table>
</asp:Content>

﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.Drawing
Imports BioWS.BLL.Extensions
Imports BioWS.PersonsDataSetTableAdapters


Public Class ViewPerson
    Inherits System.Web.UI.Page

    Private ReadOnly Property ViewedPersonID As Integer
        Get
            Return Me.QSInt32("PersonID")
        End Get
    End Property
    Private _groupsTA As GroupsTableAdapter
    Private ReadOnly Property GroupsTA As GroupsTableAdapter
        Get
            If _groupsTA Is Nothing Then
                _groupsTA = New GroupsTableAdapter()
            End If
            Return _groupsTA
        End Get
    End Property


    Protected Sub SaveDefauls()
        Dim _profile As New ProfileCommon
        _profile = _profile.GetProfile(User.Identity.Name)
        If _profile.DefaultPeriodDays = 0 Then
            _profile.DefaultPeriodDays = 90
            _profile.Save()
        End If
    End Sub

    Private Sub SetParticipationInfo(ByVal pers As BLL.Person)
        litRegTerm.Text = String.Format("{0:dd MMMM yyyy} ({1} day(s))", pers.RegisterDate, pers.DaysSinceRegistration)
        Dim daysSinceRegistration As Integer = pers.DaysSinceRegistration
        If daysSinceRegistration > 0 Then
            Dim activeshare As Double = pers.DaysActive / daysSinceRegistration 'TODO move to person class as property
            litActiveDays.Text = String.Format("{0} ({1}%)", pers.DaysActive, Math.Round((100 * activeshare), 2))
            Dim dp1 As New DataPoint
            dp1.SetValueXY("active", activeshare)
            Dim dp2 As New DataPoint
            dp2.SetValueXY("active", 1 - activeshare)
            dp2.Color = Color.Transparent
            Chart1.Series(0).Points.Add(dp1)
            Chart1.Series(0).Points.Add(dp2)
        End If
    End Sub
    Private Sub ShowHideFollowings()
        Dim followta As New FollowingsTableAdapter
        Dim showFollowings As Boolean = False


        Dim showGVFollows As Boolean = Not followta.GetCountFollows(ViewedPersonID) = 0
        gvFollows.Visible = showGVFollows
        showFollowings = showGVFollows.Inverse()

        Dim showGVFollowsGroups As Boolean = Not followta.GetCountFollowsGroups(ViewedPersonID) = 0
        gvFollowsGroups.Visible = showGVFollowsGroups
        showFollowings = showGVFollowsGroups.Inverse()

        Dim showGVFollowedBy As Boolean = Not followta.GetCountPersonFollowedBy(ViewedPersonID) = 0
        gvFollowedBy.Visible = showGVFollowedBy
        showFollowings = showGVFollowedBy.Inverse()

        pnlFL.Visible = showFollowings
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim pers As New BLL.Person(ViewedPersonID)
        Me.SetTitlesToGrandMaster(pers.PageHeader, "Profile")
        If Me.NotPostBack() Then
            PopulateTopLevelGroups()
            SetParticipationInfo(pers)
            ShowHideFollowings()
        End If
    End Sub


    Protected Sub PopulateTopLevelGroups()
        tv_groups.Styles.Link.HoverStyle.CssClass = "hoverlink"
        Dim groupsTBL = GroupsTA.GetDataByPersonIDParentGroupID(ViewedPersonID, 1) 'TODO - 1 to constants

        If groupsTBL.Count = 0 Then
            tv_groups.Visible = False
        Else
            For i = 0 To groupsTBL.Count - 1
                Dim r = groupsTBL.Item(i)
                Dim node As New DevExpress.Web.ASPxTreeView.TreeViewNode() With {.Text = r.Title, .NavigateUrl = BLL.Group.GetURLbyID(r.Group_ID)}

                If BLL.Group.IsPersonDirectParticipant(r.Group_ID, ViewedPersonID) = True Then
                    node.Image.Url = "~\Images\folder16.png"
                    node.ToolTip = "Directly"
                Else
                    node.Image.Url = "~\Images\folder16gray.png"
                    node.ToolTip = "Inherited"
                End If

                Me.tv_groups.Nodes.Add(node)
                node.Expanded = True
                PopulateNode(r.Group_ID, node)
            Next
        End If

    End Sub
    Protected Sub PopulateNode(ByVal vParGroupID As Integer, ByVal vParNode As DevExpress.Web.ASPxTreeView.TreeViewNode)
        Dim t = GroupsTA.GetDataByPersonIDParentGroupID(ViewedPersonID, vParGroupID)
        t.ProcessTable(Of PersonsDataSet.GroupsRow)(Sub(r) ProcessGroupRow(r, vParNode))
    End Sub
    Private Sub ProcessGroupRow(ByVal r As PersonsDataSet.GroupsRow, ByVal vParNode As DevExpress.Web.ASPxTreeView.TreeViewNode)
        Dim node As New DevExpress.Web.ASPxTreeView.TreeViewNode() With {.Text = r.Title, .NavigateUrl = BLL.Group.GetURLbyID(r.Group_ID)}
        node.Image.Url = "~\Images\folder16.png"
        node.ToolTip = IIf(BLL.Group.IsPersonDirectParticipant(r.Group_ID, ViewedPersonID), "Directly", "Inherited")
        vParNode.Nodes.Add(node)
        node.Expanded = True
        PopulateNode(r.Group_ID, node)
    End Sub


    



End Class
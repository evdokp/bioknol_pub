﻿Imports ColorCode
Imports System.Net
Imports System.Xml

Public Class RegexEditor
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    


    End Sub

    Protected Sub btnLoadHTML_Click(sender As Object, e As EventArgs) Handles btnLoadHTML.Click
        Dim wc As New WebClient
        Dim code As String
        Try
            code = wc.DownloadString(txtURL.Text)
            Dim colorizer = New CodeColorizer()
            Dim colorcode = colorizer.Colorize(code, Languages.Html)
            Literal1.Text = colorcode
            hfHTML.Value = code
        Catch ex As Exception
            Literal1.Text = ex.Message
        End Try

    End Sub

    Protected Sub btnTestRegex_Click(sender As Object, e As EventArgs) Handles btnTestRegex.Click
        ' Define a regular expression for repeated words.
        Dim rx As New Regex(txtRegex.Text, RegexOptions.IgnoreCase)
        ' Find matches.
        Dim matches As MatchCollection = rx.Matches(txtURL.Text)

        gvURLMATCH.DataSource = matches
        gvURLMATCH.DataBind()


        Dim htmlmatches As MatchCollection = rx.Matches(hfHTML.Value)

        gvHTMLMatches.DataSource = htmlmatches
        gvHTMLMatches.DataBind()







    End Sub
End Class
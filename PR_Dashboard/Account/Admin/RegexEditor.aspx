﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RegexEditor.aspx.vb" Inherits="BioWS.RegexEditor" ValidateRequest="false" Title="Personal regerence dashboard regular expressions tester"  %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="background: none;">
    <form id="form1" runat="server">
    <div>
        <asp:HiddenField ID="hfHTML" runat="server" />

    <table width="100%">
    <tr>
    <td valign="top" style="width:650px">
    <dx:ASPxLabel ID="lblURL" runat="server" Text="URL:" 
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Width="100%">
        </dx:ASPxLabel>

    <table width="100%" cellpadding="0" cellspacing = "0" border="0">
    <tr>
    <td valign="middle" >
        <dx:ASPxTextBox ID="txtURL" runat="server" Width="100%"  NullText ="Enter URL here with 'http://'"
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
            SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
            <ValidationSettings>
                <RequiredField ErrorText=" " IsRequired="True" />
            </ValidationSettings>
        </dx:ASPxTextBox>
    
    
    </td>
    <td valign="middle"  style="width:100px; padding-left:5px;">
        <dx:ASPxButton ID="btnLoadHTML" runat="server" Text="Load HTML" Width="100%" 
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
            SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
        </dx:ASPxButton>
    
    </td>
    </tr>
    </table>

    <div style="border:1px solid gray; margin-top:10px; overflow:scroll; width:650px; height:500px;">
    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
    </div>
    </td>
    <td valign="top" >
        <dx:ASPxLabel ID="lblRegex" runat="server" Text="Regular expression to test:" 
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Width="100%">
        </dx:ASPxLabel>
     
       <table width="100%" cellpadding="0" cellspacing = "0" border="0">
    <tr>
    <td valign="middle" >
        <dx:ASPxTextBox ID="txtRegex" runat="server" Width="100%" 
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
            SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
        </dx:ASPxTextBox>
    
    
    </td>
    <td valign="middle"  style="width:100px; padding-left:5px;">
        <dx:ASPxButton ID="btnTestRegex" runat="server" Text="Test regex" Width="100%" 
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
            SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
        </dx:ASPxButton>
    
    </td>
    </tr>
    </table>


        <br />
        <dx:ASPxLabel ID="lblURLMatch" runat="server" Text="URL matches:" >
        </dx:ASPxLabel>
        <dx:ASPxGridView ID="gvURLMATCH" runat="server" AutoGenerateColumns="False" 
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Width="100%">
            <Columns>
                <dx:GridViewDataTextColumn Caption="Match" VisibleIndex="0" FieldName="Value">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Position" VisibleIndex="1" Width="100px" 
                    FieldName="Index">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Length" VisibleIndex="2" Width="100px" 
                    FieldName="Length">
                </dx:GridViewDataTextColumn>
            </Columns>
            <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                </LoadingPanelOnStatusBar>
                <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                </LoadingPanel>
            </Images>
            <ImagesFilterControl>
                <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                </LoadingPanel>
            </ImagesFilterControl>
            <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="5px">
                </LoadingPanel>
            </Styles>
            <StylesEditors ButtonEditCellSpacing="0">
                <ProgressBar Height="21px">
                </ProgressBar>
            </StylesEditors>
        </dx:ASPxGridView>
        <br />
           <dx:ASPxLabel ID="lblHTMLMATCH" runat="server" Text="HTML matches:">
        </dx:ASPxLabel>
           <dx:ASPxGridView ID="gvHTMLMatches" runat="server" AutoGenerateColumns="False" 
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Width="100%">
            <Columns>
                <dx:GridViewDataTextColumn Caption="Match" VisibleIndex="0" FieldName="Value">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Position" VisibleIndex="1" Width="100px" 
                    FieldName="Index">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Length" VisibleIndex="2" Width="100px" 
                    FieldName="Length">
                </dx:GridViewDataTextColumn>
            </Columns>
            <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                </LoadingPanelOnStatusBar>
                <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                </LoadingPanel>
            </Images>
            <ImagesFilterControl>
                <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                </LoadingPanel>
            </ImagesFilterControl>
            <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="5px">
                </LoadingPanel>
            </Styles>
            <StylesEditors ButtonEditCellSpacing="0">
                <ProgressBar Height="21px">
                </ProgressBar>
            </StylesEditors>
        </dx:ASPxGridView>
    </td>
    </tr>
    </table>
    </div>
    </form>
</body>
</html>

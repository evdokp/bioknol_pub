﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Masters/Admin.Master"  
    CodeBehind="DeleteDomain.aspx.vb" Inherits="BioWS.DeleteDomain" %>
    <%@ Register assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <h1 style="margin-top:1px; margin-bottom:12px;">
            <asp:Literal ID="litTitle" runat="server"></asp:Literal>
</h1>

<h5><span>Warning!</span></h5>
<div class="invalid_msg">
    <ul>
    <li>Number of views currently recordered (total/with Pubmed ID):
       <b> <asp:Literal ID="litRecCount" runat="server"></asp:Literal></b>
    </li>
    <li>Recordered period:
        <b><asp:Literal ID="litPeriod" runat="server"></asp:Literal></b>
    </li>
    <li>No more views for the domain will be recordered</li>
    <li>There will be no way to restore recordered views history</li>
    <li>Loss of recordered history may influence other PRD data, based and calculated on that history. Recalculation for influenced period may be required.</li>
    </ul>
</div>


                <dx:ASPxButton ID="btnDelete" runat="server" 
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Delete domain" >
                </dx:ASPxButton>
    <ajaxToolkit:ConfirmButtonExtender  ID="cbe1" runat="server" TargetControlID="btnDelete" ConfirmText ="Are you sure to delete domain?" ConfirmOnFormSubmit="true" />
</asp:Content>

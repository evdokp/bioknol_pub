﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Masters/Admin.Master"
    CodeBehind="ManageDomains.aspx.vb" Inherits="BioWS.ManageDomains" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="margin-top: 15px;">


    <h5><span>
   Domains in database
    </span></h5>

        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
            CssPostfix="DevEx" DataSourceID="SqlDataSource1" KeyFieldName="Person_ID" Width="100%" >
            <Columns>
                <dx:GridViewDataTextColumn Caption="Parsing rules count" FieldName="ParsingRulesCount"
                    VisibleIndex="2" Width="80px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Default parsing action" FieldName="DefaultParsingAction"
                    VisibleIndex="1" Width="80px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn ShowInCustomizationForm="True" Caption="Domain" VisibleIndex="0">
                    <DataItemTemplate>
                        <asp:HyperLink ID="hlPerson" runat="server" NavigateUrl='<%# "~\Account\Admin\EditDomain.aspx?DomainID=" & Eval("ID") %>'
                            Text='<%# Eval("SiteURL") %>' />
                        <br />
                        <span style="color: Gray">
                            <asp:Literal ID="Literal1" runat="server" Text='<%#  Eval("Title")  %>' /> (<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "~\Account\Admin\EditDomain.aspx?DomainID=" & Eval("ID") %>'
                            Text='<%# Eval("ID") %>' />)

                        </span>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataCheckColumn Caption="Is approved" FieldName="IsApproved" VisibleIndex="3"
                    Width="80px">
                    <HeaderStyle Wrap="True" />
                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataTextColumn FieldName="ViewsRecordered" ShowInCustomizationForm="True"
                    Caption="Views recordered" Width="100px" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataProgressBarColumn FieldName="PubmedShare" ReadOnly="True" Caption="Pubmed share"
                    Width="100px" ShowInCustomizationForm="True" VisibleIndex="7">
                    <PropertiesProgressBar DisplayFormatString="" Height="" Width="">
                    </PropertiesProgressBar>
                </dx:GridViewDataProgressBarColumn>
            </Columns>
            <SettingsPager PageSize="50">
            </SettingsPager>
            <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="400" />
            <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
                </LoadingPanelOnStatusBar>
                <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                </LoadingPanel>
            </Images>
            <ImagesFilterControl>
                <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
                </LoadingPanel>
            </ImagesFilterControl>
            <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" Wrap="True">
                </Header>
                <LoadingPanel ImageSpacing="5px">
                </LoadingPanel>
            </Styles>
            <StylesEditors ButtonEditCellSpacing="0">
                <ProgressBar Height="21px">
                </ProgressBar>
            </StylesEditors>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:bio_conntection_string %>"
            SelectCommand="SELECT * FROM [v_MonitoredSites] ORDER BY [PubmedShare] DESC, SiteURL">
        </asp:SqlDataSource>
            <h5><span>
   Domains in application memory
    </span></h5>
      <table width="100%">
    <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Domains in memory:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litDomainsMem" runat="server"></asp:Literal>
            </td>
        </tr>
            <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Parsing rules in memory:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litRulesMem" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
              
            </td>
            <td valign="top" style="" class="td_field">
                      <dx:ASPxButton ID="btnReload" runat="server" 
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" 
                    Text="Reload domains and parsing rules" Width="250">
                </dx:ASPxButton>
            </td>
        </tr>
        </table>



        <h5><span>
    Add new domain
    </span></h5>
  <table width="100%">
    <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Title:
            </td>
            <td valign="top" style="" class="td_field">
                <dx:ASPxTextBox ID="txtTitle" runat="server"  Width="100%" NullText="Enter title here"
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" >
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                URL:
            </td>
            <td valign="top" style="" class="td_field">
                <dx:ASPxTextBox ID="txtURL" runat="server"  Width="100%" NullText="Enter URL here without 'http://'"
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                </dx:ASPxTextBox>
            </td>
        </tr>
     
      
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Default parsing action:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:ObjectDataSource ID="odsParsingActions" runat="server" 
                    DeleteMethod="Delete" InsertMethod="Insert" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                    TypeName="BioWS.MonitoredSitesDataSetTableAdapters.ParsingActionsTableAdapter" 
                    UpdateMethod="Update">
                    <DeleteParameters>
                        <asp:Parameter Name="Original_ID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Title" Type="String" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Title" Type="String" />
                        <asp:Parameter Name="Original_ID" Type="Int32" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:DropDownList ID="cmbDefaultPA" runat="server" 
                    DataSourceID="odsParsingActions" DataTextField="Title" DataValueField="ID">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                &nbsp;</td>
            <td valign="top" style="" class="td_field">
                <dx:ASPxButton ID="btnAdd" runat="server" 
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" 
                    Text="Add new domain">
                </dx:ASPxButton>


                <span class="invalid_msg">
                <asp:Literal ID="litValidation" runat="server" />
                </span>

            </td>
        </tr>
            </table>

    </div>
</asp:Content>

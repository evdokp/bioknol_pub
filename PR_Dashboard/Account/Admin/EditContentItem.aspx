﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EditContentItem.aspx.vb"
    Inherits="BioWS.EditContentItem" %>

<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSplitter" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js"></script>
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true"
        EnablePartialRendering="true" EnableScriptGlobalization="true" EnableScriptLocalization="true">
    </ajaxToolkit:ToolkitScriptManager>
    <div>
        <dx:ASPxSplitter ID="ASPxSplitter1" runat="server" Orientation="Vertical" ClientInstanceName="splitter"
            FullscreenMode="True" CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css"
            CssPostfix="Office2010Silver" Height="100%" Width="100%">
            <Panes>
                <dx:SplitterPane MaxSize="50" AllowResize="False" PaneStyle-BackColor="#0052A4" ShowCollapseBackwardButton="False"
                    ShowCollapseForwardButton="False" ShowSeparatorImage="False" Size="50px">
                    <PaneStyle BackColor="#0052A4">
                    </PaneStyle>
                    <ContentCollection>
                        <dx:SplitterContentControl runat="server" SupportsDisabledAttribute="True">
                            <div style="font-size: 24px; font-family: Cambria; font-weight: bold; color: white;">
                                Personal reference dashboard online documentation editor
                            </div>
                        </dx:SplitterContentControl>
                    </ContentCollection>
                </dx:SplitterPane>
                <dx:SplitterPane ShowCollapseBackwardButton="False" ShowCollapseForwardButton="False"
                    ShowSeparatorImage="False">
                    <Panes>
                        <dx:SplitterPane Size="300px" ShowCollapseBackwardButton="True" ShowCollapseForwardButton="True">
                            <ContentCollection>
                                <dx:SplitterContentControl runat="server" SupportsDisabledAttribute="True">
                                    <dx:ASPxMenu ID="ASPxMenu1" runat="server" AutoSeparators="RootOnly" Width="100%"
                                        ItemAutoWidth="true" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx"
                                        ShowPopOutImages="True" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                        <Items>
                                            <dx:MenuItem Name="addroot" Text="add root">
                                                <Image Url="~/Images/element_new.png">
                                                </Image>
                                            </dx:MenuItem>
                                            <dx:MenuItem Name="addchild" Text="add child">
                                                <Image Url="~/Images/element_into.png">
                                                </Image>
                                            </dx:MenuItem>
                                            <dx:MenuItem Name="delete" Text="delete">
                                                <Image Url="~/Images/element_delete.png">
                                                </Image>
                                            </dx:MenuItem>
                                        </Items>
                                        <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
                                        </LoadingPanelImage>
                                        <ItemSubMenuOffset FirstItemX="2" LastItemX="2" X="2" />
                                        <ItemStyle DropDownButtonSpacing="10px" PopOutImageSpacing="10px" />
                                        <LoadingPanelStyle ImageSpacing="5px">
                                        </LoadingPanelStyle>
                                        <SubMenuStyle GutterImageSpacing="9px" GutterWidth="3px" />
                                    </dx:ASPxMenu>
                                    <dx:ASPxTreeView ID="tv1" runat="server" AllowSelectNode="true" AutoPostBack="true">
                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Silver/{0}/sprite.css">
                                            <NodeLoadingPanel Url="~/App_Themes/Office2003Silver/Web/tvNodeLoading.gif">
                                            </NodeLoadingPanel>
                                            <LoadingPanel Url="~/App_Themes/Office2003Silver/Web/Loading.gif">
                                            </LoadingPanel>
                                        </Images>
                                        <Styles CssFilePath="~/App_Themes/Office2003Silver/{0}/styles.css" CssPostfix="Office2003Silver">
                                        </Styles>
                                    </dx:ASPxTreeView>
                                </dx:SplitterContentControl>
                            </ContentCollection>
                        </dx:SplitterPane>
                        <dx:SplitterPane ShowCollapseBackwardButton="True" ShowCollapseForwardButton="True"
                            Name="ContentUrlPane" ScrollBars="Auto" ContentUrlIFrameName="contentUrlPane">
                            <ContentCollection>
                                <dx:SplitterContentControl runat="server" SupportsDisabledAttribute="True">
                      
                                   <table>
                                   <tr>
                                   <td><dx:ASPxButton ID="btnSave" runat="server" 
                                            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                                            SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Save">
                                        </dx:ASPxButton></td>
                                   <td>
                                                 <div class="invalid_msg" style="padding: 5px;">
                                   <asp:Literal ID="Literal1" runat="server"></asp:Literal>      
                                        
                                   </div>
                                   
                                   
                                   </td>
                                   <td>
                                   
                                           <div class="valid_msg" style="padding: 5px;">
                                   <asp:Literal ID="Literal2" runat="server"></asp:Literal>
                                   </div>

                                    </td>
                                   </tr>
                                   </table>



                                        <dx:ASPxLabel ID="lblTitle" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                                            CssPostfix="DevEx" Text="Title">
                                        </dx:ASPxLabel>
                                        <dx:ASPxTextBox ID="txtTitle" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                                            CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Width="95%">
                                        </dx:ASPxTextBox>
                                        <dx:ASPxLabel ID="lblBody" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                                            CssPostfix="DevEx" Text="Content">
                                        </dx:ASPxLabel>
                                        <dx:ASPxHtmlEditor ID="txtBody" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                                            CssPostfix="DevEx" Width="95%" Height="650px">
                                            <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
                                                <ViewArea>
                                                    <Border BorderColor="#9DA0AA" />
                                                </ViewArea>
                                            </Styles>
                                            <StylesEditors ButtonEditCellSpacing="0">
                                            </StylesEditors>
                                            <StylesStatusBar>
                                                <StatusBar TabSpacing="0px">
                                                    <Paddings Padding="0px" />
                                                </StatusBar>
                                            </StylesStatusBar>
                                            <SettingsImageUpload UploadImageFolder="">
                                            
                                            </SettingsImageUpload>
                                            <SettingsImageSelector>
                                                <CommonSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png" />
                                            </SettingsImageSelector>
                                            <SettingsDocumentSelector>
                                                <CommonSettings AllowedFileExtensions=".rtf, .pdf, .doc, .docx, .odt, .txt, .xls, .xlsx, .ods, .ppt, .pptx, .odp" />
                                            </SettingsDocumentSelector>
                                            <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                <LoadingPanel Url="~/App_Themes/DevEx/HtmlEditor/Loading.gif">
                                                </LoadingPanel>
                                            </Images>
                                            <ImagesFileManager>
                                                <FolderContainerNodeLoadingPanel Url="~/App_Themes/DevEx/Web/tvNodeLoading.gif">
                                                </FolderContainerNodeLoadingPanel>
                                                <LoadingPanel Url="~/App_Themes/DevEx/Web/Loading.gif">
                                                </LoadingPanel>
                                            </ImagesFileManager>
                                        </dx:ASPxHtmlEditor>
                          
                                </dx:SplitterContentControl>
                            </ContentCollection>
                        </dx:SplitterPane>
                    </Panes>
                    <ContentCollection>
                        <dx:SplitterContentControl runat="server" SupportsDisabledAttribute="True">
                        </dx:SplitterContentControl>
                    </ContentCollection>
                </dx:SplitterPane>
            </Panes>
            <Styles CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css" CssPostfix="Office2010Silver">
            </Styles>
            <Images SpriteCssFilePath="~/App_Themes/Office2010Silver/{0}/sprite.css">
            </Images>
        </dx:ASPxSplitter>
    </div>
    </form>
</body>
</html>

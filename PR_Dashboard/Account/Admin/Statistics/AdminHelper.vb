﻿Imports DevExpress.Web.ASPxEditors

Namespace BLL
    Public Class AdminHelper
        Public Shared Sub BindStatisticsDropdownList(ByVal ddl As ASPxComboBox)
            Dim iOverallStatistics As New ListEditItem("Overview", "OverallStatistics.aspx")
            Dim iDomainsStatistics As New ListEditItem("Domains statistics", "DomainsStatistics.aspx")
            ddl.Items.Add(iOverallStatistics)
            ddl.Items.Add(iDomainsStatistics)
        End Sub
    End Class
End Namespace


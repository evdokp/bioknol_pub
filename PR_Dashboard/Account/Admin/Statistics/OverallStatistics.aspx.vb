﻿Imports System.Web.UI.DataVisualization.Charting
Imports DevExpress.Web
Imports System.Drawing
Imports BioWS.BLL.Extensions


Public Class OverallStatistics
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.NotPostBack() Then
            CType(Master.Master.FindControl("lit_pagetitle"), Literal).Text = "Personal reference dashboard management"
            CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "Overall statistics"
            Dim pr As New ProfileCommon
            pr = pr.GetProfile(HttpContext.Current.User.Identity.Name)

         
            'BindStatisticsPage(Today.AddDays(-pr.DefaultPeriodDays), Today)


            DataBindCharts(Today.AddDays(-pr.DefaultPeriodDays), Today)

            BLL.AdminHelper.BindStatisticsDropdownList(cmbStat)
            cmbStat.SelectedIndex = 0
        End If
    End Sub

    Private Shared Sub PrepareChartArea(ByVal cha As ChartArea)
        Dim grayColor As Color = Drawing.Color.LightGray
        cha.BorderColor = grayColor
        cha.BorderWidth = 1
        cha.BorderDashStyle = ChartDashStyle.Solid
        cha.AxisY.MajorGrid.LineColor = grayColor
        cha.AxisX.MajorGrid.LineColor = grayColor
        cha.AxisY.MajorTickMark.LineColor = grayColor
        cha.AxisX.MajorTickMark.LineColor = grayColor
        cha.AxisY.LineColor = grayColor
        cha.AxisX.LineColor = grayColor
        cha.AxisX.IntervalType = DateTimeIntervalType.Days
        cha.AxisX.IsMarginVisible = False
    End Sub


    Private Sub DataBindTimeSeriesChart(StartDate As Date, EndDate As Date)
        Dim cha As ChartArea = Me.chViewsTimeSeries.ChartAreas("ChartArea1")
        PrepareChartArea(cha)

        Dim seriesPMDistinct As Series = Me.chViewsTimeSeries.Series("SeriesPMIDs")
        seriesPMDistinct.ChartType = SeriesChartType.Area
        seriesPMDistinct.XValueType = ChartValueType.Date

        Dim seriesPMViews As Series = Me.chViewsTimeSeries.Series("SeriesViewsPM")
        seriesPMViews.ChartType = SeriesChartType.Area
        seriesPMViews.XValueType = ChartValueType.Date

        Dim seriesTotalViews As Series = Me.chViewsTimeSeries.Series("SeriesViews")
        seriesTotalViews.ChartType = SeriesChartType.Area
        seriesTotalViews.XValueType = ChartValueType.Date



        Dim l As New Legend() With {.Docking = Docking.Bottom, .Alignment = Drawing.StringAlignment.Far, .IsDockedInsideChartArea = False}
        chViewsTimeSeries.Legends.Add(l)
        chViewsTimeSeries.ChartAreas("ChartArea1").AxisX.LabelStyle.Format = "dd MMM yyyy"

        Dim query As String = String.Format("SELECT AllDates.CalendarDate, Count(ViewsLog.TimeStamp) as TotalViews, count(ViewsLog.PubMed_ID) as PMViews, Count(distinct ViewsLog.PubMed_ID) as PMDistinct " & _
                " FROM AllDates LEFT OUTER JOIN ViewsLog ON AllDates.CalendarDate = ViewsLog.ViewDate where AllDates.CalendarDate between '{0}' and '{1}'" & _
                " group by AllDates.CalendarDate order by AllDates.CalendarDate ", StartDate.ToString, EndDate.ToString)
        Dim dt1 As DataTable = BLL.SQLHelper.GetDT_AllStrings(query)



        seriesPMDistinct.Points.Clear()
        seriesPMViews.Points.Clear()
        seriesTotalViews.Points.Clear()

        Dim cPMIDs, cViewsPM, cViews As Integer

        For i = 0 To dt1.Rows.Count - 1
            Dim row As DataRow = dt1.Rows(i)

            Dim dpTotal As New DataPoint()
            dpTotal.SetValueXY(CDate(row(0)), row(1))
            seriesTotalViews.Points.Add(dpTotal)

            Dim dpPMViews As New DataPoint()
            dpPMViews.SetValueXY(CDate(row(0)), row(2))
            seriesPMViews.Points.Add(dpPMViews)

            Dim dpPMDistinct As New DataPoint()
            dpPMDistinct.SetValueXY(CDate(row(0)), row(3))
            seriesPMDistinct.Points.Add(dpPMDistinct)

            cPMIDs = cPMIDs + row(3)
            cViewsPM = cViewsPM + row(2)
            cViews = cViews + row(1)

        Next




        seriesPMDistinct.LegendText = String.Format("Distinct PMIDs views")
        seriesPMViews.LegendText = String.Format("PMIDs views ({0})", cViewsPM)
        seriesTotalViews.LegendText = String.Format("Total views ({0})", cViews)


    End Sub
    Private Sub DataBindNewUsersChart(StartDate As Date, EndDate As Date)
        Dim cha As ChartArea = Me.chNewUsers.ChartAreas("ChartArea1")
        cha.AxisY2.MajorGrid.LineColor = Drawing.Color.LightGray
        cha.AxisY2.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha.AxisY2.LineColor = Drawing.Color.LightGray
        
        PrepareChartArea(cha)
        cha.AxisY2.Enabled = AxisEnabled.True

        Dim sNew As Series = Me.chNewUsers.Series("sNew")
        sNew.ChartType = SeriesChartType.Column
        sNew.XValueType = ChartValueType.Date

        Dim sNewCum As Series = Me.chNewUsers.Series("sNewCum")
        sNewCum.ChartType = SeriesChartType.Area
        sNewCum.XValueType = ChartValueType.Date



        sNew.YAxisType = AxisType.Primary
        sNewCum.YAxisType = AxisType.Secondary


        Dim l As New Legend() With {.Docking = Docking.Bottom, .Alignment = Drawing.StringAlignment.Far, .IsDockedInsideChartArea = False}
        chNewUsers.Legends.Add(l)

        chNewUsers.ChartAreas("ChartArea1").AxisX.LabelStyle.Format = "dd MMM yyyy"

        Dim query As String = String.Format("SELECT     AllDates.CalendarDate, COUNT(Persons.Person_ID) AS vRegsCount FROM AllDates LEFT OUTER JOIN Persons ON AllDates.CalendarDate = CONVERT([varchar](10), Persons.RegisterDate, 111)" & _
                                            "WHERE     (AllDates.CalendarDate BETWEEN '{0}' AND '{1}') GROUP BY AllDates.CalendarDate order by AllDates.CalendarDate ", StartDate.ToString, EndDate.ToString)

        Dim dt1 As DataTable = BLL.SQLHelper.GetDT_AllStrings(query)

        sNew.Points.Clear()
        sNewCum.Points.Clear()

        Dim personsTA As New PersonsDataSetTableAdapters.PersonsTableAdapter
        Dim cumRegCount As Integer = personsTA.GetCountRegisteredBefore(StartDate)

        For i = 0 To dt1.Rows.Count - 1
            Dim row As DataRow = dt1.Rows(i)

            Dim dpReg As New DataPoint()
            dpReg.SetValueXY(CDate(row(0)), row(1))
            sNew.Points.Add(dpReg)

            cumRegCount = cumRegCount + CInt(row(1))

            Dim dpCumReg As New DataPoint()
            dpCumReg.SetValueXY(CDate(row(0)), cumRegCount)
            sNewCum.Points.Add(dpCumReg)

        Next


        sNew.LegendText = "Registrations count"
        sNewCum.LegendText = String.Format("Accumulated users count ({0})", cumRegCount)

    End Sub

    Private Sub DataBindActivePluginsChart(StartDate As Date, EndDate As Date)

        Dim cha As ChartArea = Me.chActivePlugins.ChartAreas("ChartArea1")
        PrepareChartArea(cha)


        Dim l As New Legend() With {.Docking = Docking.Bottom, .Alignment = Drawing.StringAlignment.Far, .IsDockedInsideChartArea = False}
        chActivePlugins.Legends.Add(l)
        chActivePlugins.ChartAreas("ChartArea1").AxisX.LabelStyle.Format = "dd MMM yyyy"

        Dim browsers As List(Of String) = New List(Of String)(New String() {"IE", "FF", "CHR", "OP", "SF"})

        '! bind browser series
        For Each browser As String In browsers
            Dim browserSeries As Series = chActivePlugins.Series(browser)
            browserSeries.ChartType = SeriesChartType.StackedArea
            browserSeries.XValueType = ChartValueType.Date
            browserSeries.Points.Clear()

            Dim query As String = String.Format("SELECT     AllDates.CalendarDate,IsNull( t.vCount,0) as vCount " & _
                " FROM         (SELECT     CONVERT(datetime, CONVERT([varchar](10), LogDateTime, 111)) AS vDate, COUNT(DISTINCT PluginID) AS vCount " & _
                " FROM          PluginsLog " & _
                " WHERE      (CONVERT(datetime, CONVERT([varchar](10), LogDateTime, 111)) BETWEEN '{0}' AND '{1}') AND (PluginBrowser = '{2}') " & _
                " GROUP BY CONVERT(datetime, CONVERT([varchar](10), LogDateTime, 111))) AS t RIGHT OUTER JOIN " & _
                " AllDates ON t.vDate = AllDates.CalendarDate " & _
                " where (AllDates.CalendarDate BETWEEN '{0}' AND '{1}') " & _
                " order by AllDates.CalendarDate", StartDate.ToString, EndDate.ToString, browser)

            Dim dt1 As DataTable = BLL.SQLHelper.GetDT_AllStrings(query)

            For i = 0 To dt1.Rows.Count - 1
                Dim row As DataRow = dt1.Rows(i)

                Dim dpDay As New DataPoint()
                dpDay.SetValueXY(CDate(row(0)), row(1))
                browserSeries.Points.Add(dpDay)
            Next

        Next


        '! bind logged out serie
        Dim loggedOutSeries As Series = chActivePlugins.Series("LoggedOut")
        loggedOutSeries.ChartType = SeriesChartType.Line
        loggedOutSeries.BorderColor = Color.Red
        loggedOutSeries.Color = Color.Red
        loggedOutSeries.BorderWidth = 2
        loggedOutSeries.XValueType = ChartValueType.Date
        loggedOutSeries.Points.Clear()

        Dim query2 As String = String.Format("SELECT     AllDates.CalendarDate,IsNull( t.vCount,0) as vCount " & _
            " FROM         (SELECT     CONVERT(datetime, CONVERT([varchar](10), LogDateTime, 111)) AS vDate, COUNT(DISTINCT PluginID) AS vCount " & _
            " FROM          PluginsLog " & _
            " WHERE      (CONVERT(datetime, CONVERT([varchar](10), LogDateTime, 111)) BETWEEN '{0}' AND '{1}') AND (UserID = 0) " & _
            " GROUP BY CONVERT(datetime, CONVERT([varchar](10), LogDateTime, 111))) AS t RIGHT OUTER JOIN " & _
            " AllDates ON t.vDate = AllDates.CalendarDate " & _
            " where (AllDates.CalendarDate BETWEEN '{0}' AND '{1}') " & _
            " order by AllDates.CalendarDate", StartDate.ToString, EndDate.ToString)

        Dim dt2 As DataTable = BLL.SQLHelper.GetDT_AllStrings(query2)

        For i = 0 To dt2.Rows.Count - 1
            Dim row As DataRow = dt2.Rows(i)

            Dim dpDay As New DataPoint()
            dpDay.SetValueXY(CDate(row(0)), row(1))
            loggedOutSeries.Points.Add(dpDay)
        Next


    End Sub

    Private Sub DataBindDetectedSharePies(StartDate As Date, EndDate As Date)
        MakeChartPubmed(StartDate, EndDate)
        MakeChartDOI(StartDate, EndDate)
    End Sub
    Private Sub MakeChartPubmed(StartDate As Date, EndDate As Date)
        Dim ta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
        Dim vAll, vPubmed, vUnPubmed As Integer
        vAll = ta.GetCountAll_ByTwoDates(StartDate, EndDate)
        vPubmed = ta.GetCountWithPubMed_ByTwoDates(StartDate, EndDate)
        vUnPubmed = vAll - vPubmed

        Dim dp As New DataPoint
        dp.SetValueXY("ID was not detected", vUnPubmed)

        Dim dp1 As New DataPoint
        dp1.SetValueXY("ID was detected", vPubmed)
        dp1.CustomProperties += "Exploded=true"

        Me.pie_pubmedid.Series("Default").Points.Add(dp)
        Me.pie_pubmedid.Series("Default").Points.Add(dp1)


    End Sub
    Private Sub MakeChartDOI(StartDate As Date, EndDate As Date)
        Dim ta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
        Dim vAll, vDOI, vUnDOI As Integer
        vAll = ta.GetCountAll_ByTwoDates(StartDate, EndDate)
        vDOI = ta.GetCountWithDOI_ByTwoDates(StartDate, EndDate)
        vUnDOI = vAll - vDOI

        Dim dp As New DataPoint
        dp.SetValueXY("ID was not detected", vUnDOI)

        Dim dp1 As New DataPoint
        dp1.SetValueXY("ID was detected", vDOI)
        dp1.CustomProperties += "Exploded=true"

        Me.pie_doi.Series("Default").Points.Add(dp)
        Me.pie_doi.Series("Default").Points.Add(dp1)


    End Sub
    Private Sub MakeRequests()
        Dim s1 As New Series
        s1.Name = "Serie1"
        s1.ChartType = SeriesChartType.Column
        s1.XValueType = ChartValueType.Int32
        s1.YValueType = ChartValueType.Int32
        s1.XAxisType = AxisType.Primary
        s1.CustomProperties = "DrawingStyle=LightToDark"

        Dim ta As New ChartDataSetTableAdapters.DataTable2TableAdapter
        Dim t = ta.GetCurrentPendingRequestByDuration()
        Dim i As Integer

        For i = 0 To t.Count - 1
            Dim dp As New DataPoint
            dp.SetValueXY(CType(t.Item(i).vTitle, Integer), t.Item(i).vCount)
            s1.Points.Add(dp)
        Next

        Me.bar_requests.ChartAreas("ChartArea1").AxisX.Interval = 0
        Me.bar_requests.ChartAreas("ChartArea1").AxisX.LabelStyle.IsEndLabelVisible = False
        Me.bar_requests.ChartAreas("ChartArea1").AxisX.IsLabelAutoFit = True
        Me.bar_requests.ChartAreas("ChartArea1").AxisX.LabelAutoFitStyle = LabelAutoFitStyles.DecreaseFont
        Me.bar_requests.ChartAreas("ChartArea1").AxisX.Title = "Days pending"
        Me.bar_requests.ChartAreas("ChartArea1").AxisY.Title = "Requests count"
        Me.bar_requests.ChartAreas("ChartArea1").AxisY.Interval = 1
        Me.bar_requests.ChartAreas("ChartArea1").AxisY.LabelStyle.IsEndLabelVisible = False
        Me.bar_requests.ChartAreas("ChartArea1").AxisY.IsLabelAutoFit = True
        Me.bar_requests.ChartAreas("ChartArea1").AxisY.LabelAutoFitStyle = LabelAutoFitStyles.DecreaseFont

        Me.bar_requests.Series.Add(s1)
    End Sub
    Private Shared Sub AddPoint(ByVal sMessages As Series, ByVal row As DataRow)
        Dim dpReg As New DataPoint()
        dpReg.SetValueXY(CDate(row(0)), row(1))
        sMessages.Points.Add(dpReg)
    End Sub
    Private Sub DataBindMessagesChart(StartDate As Date, EndDate As Date)
        Dim cha As ChartArea = chMessages.ChartAreas("ChartArea1")


        PrepareChartArea(cha)


        Dim sMessages As Series = Me.chMessages.Series("messages")
        sMessages.ChartType = SeriesChartType.Column
        sMessages.XValueType = ChartValueType.Date



        Dim l As New Legend() With {.Docking = Docking.Bottom, .Alignment = Drawing.StringAlignment.Far, .IsDockedInsideChartArea = False}
        chMessages.Legends.Add(l)

        chMessages.ChartAreas("ChartArea1").AxisX.LabelStyle.Format = "dd MMM yyyy"

        Dim query As String = String.Format("SELECT     AllDates.CalendarDate, COUNT(Messages.ID) AS vMessagesCount FROM AllDates LEFT OUTER JOIN Messages ON AllDates.CalendarDate = CONVERT([varchar](10), Messages.MessageDateTime, 111)" & _
                                            "WHERE     (AllDates.CalendarDate BETWEEN '{0}' AND '{1}') GROUP BY AllDates.CalendarDate order by AllDates.CalendarDate  ", StartDate.ToString, EndDate.ToString)

        Dim dt1 As DataTable = BLL.SQLHelper.GetDT_AllStrings(query)

        sMessages.Points.Clear()

        Dim personsTA As New PersonsDataSetTableAdapters.PersonsTableAdapter
        Dim cumRegCount As Integer = personsTA.GetCountRegisteredBefore(StartDate)

        dt1.ProcessTable(Of DataRow)(Sub(a) AddPoint(sMessages, a))


    End Sub
    Private Sub DataBindArticlesChart(StartDate As Date, EndDate As Date)
        Dim cha As ChartArea = chArticles.ChartAreas("ChartArea1")
        PrepareChartArea(cha)

        Dim sOffers As Series = Me.chArticles.Series("Offers")
        sOffers.ChartType = SeriesChartType.Line
        sOffers.XValueType = ChartValueType.Date
        sOffers.MarkerStyle = MarkerStyle.Square

        Dim sRequests As Series = Me.chArticles.Series("Requests")
        sRequests.ChartType = SeriesChartType.Line
        sRequests.XValueType = ChartValueType.Date
        sRequests.MarkerStyle = MarkerStyle.Diamond

        Dim sRecommendations As Series = Me.chArticles.Series("Recommendations")
        sRecommendations.ChartType = SeriesChartType.Line
        sRecommendations.XValueType = ChartValueType.Date
        sRecommendations.MarkerStyle = MarkerStyle.Circle


        Dim l As New Legend() With {.Docking = Docking.Bottom, .Alignment = Drawing.StringAlignment.Far, .IsDockedInsideChartArea = False}
        chArticles.Legends.Add(l)
        chArticles.ChartAreas("ChartArea1").AxisX.LabelStyle.Format = "dd MMM yyyy"

        Dim query As String = String.Format(" SELECT     AllDates.CalendarDate, IsNull( v_ArticleOffersCounts.vCount,0) AS OffersCounts, IsNull(v_ArticleRequestsCounts.vCount,0) AS RequestsCounts,  " & _
                " IsNull(v_ArticleRecommendationsCounts.vCount,0) AS RecommendationsCounts " & _
                " FROM         AllDates LEFT OUTER JOIN v_ArticleRecommendationsCounts ON AllDates.CalendarDate = v_ArticleRecommendationsCounts.vDate LEFT OUTER JOIN " & _
                " v_ArticleRequestsCounts ON AllDates.CalendarDate = v_ArticleRequestsCounts.vDate LEFT OUTER JOIN " & _
                " v_ArticleOffersCounts ON AllDates.CalendarDate = v_ArticleOffersCounts.vDate " & _
                " WHERE     (AllDates.CalendarDate BETWEEN '{0}' AND '{1}') order by AllDates.CalendarDate  ", StartDate.ToString, EndDate.ToString)
        Dim dt1 As DataTable = BLL.SQLHelper.GetDT_AllStrings(query)



        sOffers.Points.Clear()
        sRequests.Points.Clear()
        sRecommendations.Points.Clear()

        Dim cOffers, cRequests, cRecommendations As Integer

        For i = 0 To dt1.Rows.Count - 1
            Dim row As DataRow = dt1.Rows(i)

            Dim dpOffers As New DataPoint()
            dpOffers.SetValueXY(CDate(row(0)), row(1))
            sOffers.Points.Add(dpOffers)

            Dim dpRequests As New DataPoint()
            dpRequests.SetValueXY(CDate(row(0)), row(2))
            sRequests.Points.Add(dpRequests)

            Dim dpRecommendations As New DataPoint()
            dpRecommendations.SetValueXY(CDate(row(0)), row(3))
            sRecommendations.Points.Add(dpRecommendations)

            cOffers = cOffers + row(1)
            cRequests = cRequests + row(2)
            cRecommendations = cRecommendations + row(3)

        Next




        sOffers.LegendText = String.Format("Uploads ({0})", cOffers)
        sRequests.LegendText = String.Format("Request ({0})", cRequests)
        sRecommendations.LegendText = String.Format("Recommendations ({0})", cRecommendations)


    End Sub


    Private Sub DataBindCharts(StartDate As Date, EndDate As Date)
        '! Views Timeseries
        DataBindTimeSeriesChart(StartDate, EndDate)
        '! New users timeseries
        DataBindNewUsersChart(StartDate, EndDate)
        '! active plugins
        DataBindActivePluginsChart(StartDate, EndDate)
        '! detected share
        DataBindDetectedSharePies(StartDate, EndDate)
        '! pending request
        MakeRequests()
        '! messages
        DataBindMessagesChart(StartDate, EndDate)
        '! articles
        DataBindArticlesChart(StartDate, EndDate)
    End Sub



    Private Sub SetPeriod1_PeriodChanged(StartDate As Date, EndDate As Date) Handles SetPeriod1.PeriodChanged
        DataBindCharts(StartDate, EndDate)
    End Sub
    Private Sub cmbStat_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cmbStat.SelectedIndexChanged
        Response.Redirect("~\Account\Admin\Statistics\" & cmbStat.SelectedItem.Value)
    End Sub
End Class
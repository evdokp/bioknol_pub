﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Masters/Admin.Master"  
    CodeBehind="DomainsStatistics.aspx.vb" Inherits="BioWS.DomainsStatistics" %>

<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp1" %>



<%@ Register assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register src="~/UserControls/SetPeriod.ascx" tagname="SetPeriod" tagprefix="uc1" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    


         
    <table width="100%">
    <tr>
    <td style="width:100px;">
    <uc1:SetPeriod ID="SetPeriod1" runat="server" />
    </td>
    <td style="padding-left:10px;">
    
        <dx:ASPxComboBox ID="cmbStat" runat="server"  Width="100%" AutoPostBack="true" 
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Spacing="0" 
            SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" 
            ValueType="System.String" >
            <LoadingPanelImage Url="~/App_Themes/DevEx/Editors/Loading.gif">
            </LoadingPanelImage>
            <LoadingPanelStyle ImageSpacing="5px">
            </LoadingPanelStyle>
            <ButtonStyle Width="13px">
            </ButtonStyle>
        </dx:ASPxComboBox>
    </td>
    </tr>
    </table>
    

 

    <h5><span>
    Overview
    </span></h5>

    
    <table width="100%" id="tblCrit">
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
             Number of domains:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litTotalCount" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Ever recordered:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litRecCount" runat="server" />
            </td>
        </tr>
    </table>
    




    <h5><span>
    Top domains (bar chart)
    </span></h5>
    <table>
    <tr>
    <td>
        <dx:ASPxComboBox ID="cmbTop" runat="server"   AutoPostBack = "true" 
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Spacing="0" 
            SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" ValueType="System.String">

            <Items>
                <dx:ListEditItem Text="Top 10" Value="10"  Selected="true" />
                <dx:ListEditItem Text="Top 50 " Value="50" />
                <dx:ListEditItem Text="Top 100" Value="100" />
            </Items>

            <LoadingPanelImage Url="~/App_Themes/DevEx/Editors/Loading.gif">
            </LoadingPanelImage>
            <LoadingPanelStyle ImageSpacing="5px">
            </LoadingPanelStyle>
            <ButtonStyle Width="13px">
            </ButtonStyle>
        </dx:ASPxComboBox>
        </td>
    <td><dx:ASPxComboBox ID="cmbType" runat="server" AutoPostBack = "true" 
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Spacing="0" 
            SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" ValueType="System.String">
        <Items>
            <dx:ListEditItem Text="by total views" Value="Views"  Selected="true" />
            <dx:ListEditItem Text="by Pubmed views" Value="Pubmed" />
            <dx:ListEditItem Text="by distinct Pubmed IDs" Value="PubmedDistinct" />
        </Items>
        <LoadingPanelImage Url="~/App_Themes/DevEx/Editors/Loading.gif">
        </LoadingPanelImage>
        <LoadingPanelStyle ImageSpacing="5px">
        </LoadingPanelStyle>
        <ButtonStyle Width="13px">
        </ButtonStyle>
        </dx:ASPxComboBox>
        </td>
    </tr>
    </table>



       <asp:Chart ID="barDomains" runat="server" Width="645px">
        <Series>
            <asp:Series Name="Series1" ChartType="Bar" CustomProperties="DrawingStyle=LightToDark"
                Font="Microsoft Sans Serif, 6.75pt" IsValueShownAsLabel="True" Label="#VAL (#PERCENT)">
            </asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>

</asp:Content>

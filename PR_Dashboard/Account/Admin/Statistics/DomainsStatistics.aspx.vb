﻿Imports System.Web.UI.DataVisualization.Charting
Imports DevExpress.Web
Public Class DomainsStatistics
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            CType(Master.Master.FindControl("lit_pagetitle"), Literal).Text = "Personal reference dashboard management"
            CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "Domains statistics"
            Dim pr As New ProfileCommon
            pr = pr.GetProfile(HttpContext.Current.User.Identity.Name)

         
            'BindStatisticsPage(Today.AddDays(-pr.DefaultPeriodDays), Today)


            '! fill table
            Dim msTA As New MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter
            litTotalCount.Text = msTA.GetCountTotal()
            litRecCount.Text = msTA.GetCountEverRecordered()

            DataBindDomains(Today.AddDays(-pr.DefaultPeriodDays), Today)

            BLL.AdminHelper.BindStatisticsDropdownList(cmbStat)
            cmbStat.SelectedIndex = 1
        End If
    End Sub

    Public Sub DataBindDomains(StartDate As Date, EndDate As Date)
        Dim typeQuery As String = String.Empty
        Select Case cmbType.SelectedItem.Value
            Case "Views"
                typeQuery = " COUNT(ViewsLog.TimeStamp) "
            Case "Pubmed"
                typeQuery = " COUNT(ViewsLog.Pubmed_ID) "
            Case "PubmedDistinct"
                typeQuery = " COUNT(distinct ViewsLog.Pubmed_ID) "
        End Select

        Dim query As String = String.Format(" SELECT top({0})    MonitoredSites.SiteURL, {1} AS vCount FROM MonitoredSites INNER JOIN ViewsLog ON MonitoredSites.ID = ViewsLog.MonitoredSite_ID " & _
        "where  Convert(datetime,CONVERT([varchar](10), ViewsLog.TimeStamp, 111)) between '{2}' and '{3}' GROUP BY MonitoredSites.SiteURL having {1} > 0 order by vCount desc ",
                                  cmbTop.SelectedItem.Value,
                                  typeQuery,
                                  StartDate.ToString,
                                  EndDate.ToString)
        Dim dt1 As DataTable = BLL.SQLHelper.GetDT_AllStrings(query)

        

        For i = dt1.Rows.Count - 1 To 0 Step -1
            Dim dRow As DataRow = dt1.Rows(i)
            Dim dp As New DataPoint
            dp.SetValueXY(dRow(0), dRow(1))
            barDomains.Series("Series1").Points.Add(dp)
        Next
        barDomains.Height = 80 + 14 * dt1.Rows.Count
        barDomains.ChartAreas("ChartArea1").AxisX.MinorGrid.Enabled = False
        barDomains.ChartAreas("ChartArea1").AxisX.MajorGrid.Enabled = False
        barDomains.ChartAreas("ChartArea1").AxisX.Interval = 1
        barDomains.ChartAreas("ChartArea1").AxisY.MajorGrid.LineColor = Drawing.Color.LightGray

    End Sub

    Private Sub SetPeriod1_PeriodChanged(StartDate As Date, EndDate As Date) Handles SetPeriod1.PeriodChanged
        DataBindDomains(StartDate, EndDate)
    End Sub


    Private Sub cmbStat_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cmbStat.SelectedIndexChanged
        Response.Redirect("~\Account\Admin\Statistics\" & cmbStat.SelectedItem.Value)
    End Sub

    Private Sub cmbTop_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cmbTop.SelectedIndexChanged
        DataBindDomains(SetPeriod1.StartDate, SetPeriod1.EndDate)
    End Sub

    Private Sub cmbType_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cmbType.SelectedIndexChanged
        DataBindDomains(SetPeriod1.StartDate, SetPeriod1.EndDate)
    End Sub
End Class
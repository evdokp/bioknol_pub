﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Masters/Admin.Master"
    CodeBehind="OverallStatistics.aspx.vb" Inherits="BioWS.OverallStatistics" %>

<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp1" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Src="~/UserControls/SetPeriod.ascx" TagName="SetPeriod" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table width="100%">
        <tr>
            <td style="width: 100px;">
                <uc1:SetPeriod ID="SetPeriod1" runat="server" />
            </td>
            <td style="padding-left: 10px;">
                <dx:ASPxComboBox ID="cmbStat" runat="server" Width="100%" AutoPostBack="true" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                    CssPostfix="DevEx" Spacing="0" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"
                    ValueType="System.String">
                    <LoadingPanelImage Url="~/App_Themes/DevEx/Editors/Loading.gif">
                    </LoadingPanelImage>
                    <LoadingPanelStyle ImageSpacing="5px">
                    </LoadingPanelStyle>
                    <ButtonStyle Width="13px">
                    </ButtonStyle>
                </dx:ASPxComboBox>
            </td>
        </tr>
    </table>
    <h5>
        <span>Number of new users </span>
    </h5>
       <asp:Chart ID="chNewUsers" runat="server" Width="675px">
                <series>
                
                <asp:Series Name="sNewCum">
                </asp:Series>
                <asp:Series name="sNew"  >
                </asp:Series>
            </series>
                <chartareas>
                <asp:ChartArea Name="ChartArea1"/>
            </chartareas>
            </asp:Chart>


    <h5>
        <span>Number of active plug-ins (by plug-in check-in functionality) </span>
    </h5>

          <asp:Chart ID="chActivePlugins" runat="server" Width="675px">
                <series>
                
                <asp:Series Name="IE" LegendText="Internet explorer" >
                </asp:Series>
                <asp:Series name="FF"    LegendText="Firefox">
                </asp:Series>
                <asp:Series name="CHR"   LegendText="Chrome">
                </asp:Series>
                <asp:Series name="OP"   LegendText="Opera">
                </asp:Series>
                <asp:Series name="SF"   LegendText="Safari">
                </asp:Series>
                <asp:Series name="LoggedOut" LegendText="Logged out" >
                </asp:Series>
            </series>
                <chartareas>
                <asp:ChartArea Name="ChartArea1"/>
            </chartareas>
            </asp:Chart>



    <h5>
        <span>Views and distinct Pubmed IDs timeline </span>
    </h5>
            <asp:Chart ID="chViewsTimeSeries" runat="server" Width="675px">
                <series>
                <asp:Series name="SeriesViews"  >
                </asp:Series>
                <asp:Series Name="SeriesViewsPM">
                </asp:Series>
                <asp:Series Name="SeriesPMIDs">
                </asp:Series>
        
        
            </series>
                <chartareas>
                <asp:ChartArea Name="ChartArea1">
                    <axisx>
                        <LabelStyle Format="d" />
                    </axisx>
                </asp:ChartArea>
            </chartareas>
            </asp:Chart>
    <h5>
        <span>Share of views with detected identifiers </span>
    </h5>
        <table width ="100%">
    <tr>
    <td style ="width :50%;">
    <asp:Chart ID="pie_pubmedid" runat="server" Palette="BrightPastel" Width="335px">
        <Series>
            <asp:Series Name="Default" ChartType="Pie" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240"
                CustomProperties="LabelsHorizontalLineSize=2, PieLabelStyle=Outside, PieDrawingStyle=Concave, CollectedThreshold=5, PieLineColor=SlateGray, CollectedLegendText=Другие\n#VAL (#PERCENT), CollectedLabel=Другие"
                Label="#VALX\n#VAL (#PERCENT)" LegendText="#VALX" XValueType="String" YValueType="Int32">
                <SmartLabelStyle AllowOutsidePlotArea="Yes" IsOverlappedHidden="False" MaxMovingDistance="40" />
            </asp:Series>
        </Series>
        <Titles>
            <asp:Title Name="Title1" Text="Share of PubMedID-detected records" >
            </asp:Title>
        </Titles>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent"
                BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                <Area3DStyle Rotation="0" />
                <AxisY LineColor="64, 64, 64, 64">
                    <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                    <MajorGrid LineColor="64, 64, 64, 64" />
                </AxisY>
                <AxisX LineColor="64, 64, 64, 64">
                    <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                    <MajorGrid LineColor="64, 64, 64, 64" />
                </AxisX>
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
    
    </td>
    <td style ="width :50%;">
    <asp:Chart ID="pie_doi" runat="server" Palette="BrightPastel" Width="335px">
        <Series>
            <asp:Series Name="Default" ChartType="Pie" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240"
                CustomProperties="LabelsHorizontalLineSize=2, PieLabelStyle=Outside, PieDrawingStyle=Concave, CollectedThreshold=5, PieLineColor=SlateGray, CollectedLegendText=Другие\n#VAL (#PERCENT), CollectedLabel=Другие"
                Label="#VALX\n#VAL (#PERCENT)" LegendText="#VALX" XValueType="String" YValueType="Int32">
                <SmartLabelStyle AllowOutsidePlotArea="Yes" IsOverlappedHidden="False" MaxMovingDistance="40" />
            </asp:Series>
        </Series>
        <Titles>
            <asp:Title Name="Title1" Text="Share of DOI-detected records" >
            </asp:Title>
        </Titles>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent"
                BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                <Area3DStyle Rotation="0" />
                <AxisY LineColor="64, 64, 64, 64">
                    <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                    <MajorGrid LineColor="64, 64, 64, 64" />
                </AxisY>
                <AxisX LineColor="64, 64, 64, 64">
                    <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                    <MajorGrid LineColor="64, 64, 64, 64" />
                </AxisX>
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
    
    </td>
    
    </tr>
    
    </table>

    <h5>
        <span>Unprocessed article requests </span>
    </h5>
      <asp:Chart ID="bar_requests" runat="server"  Width="675px">
    
        
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
                <AxisY>
                    <MajorGrid LineColor="LightGray" />
                </AxisY>
                <AxisX>
                    <MajorGrid LineColor="LightGray" />
                </AxisX>
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>

    <h5>
        <span>Number of messages between users </span>
    </h5>

      <asp:Chart ID="chMessages" runat="server" Width="675px">
                <series>
                <asp:Series Name="messages" LegendText="Messages count" >
                </asp:Series>
            </series>
                <chartareas>
                <asp:ChartArea Name="ChartArea1"/>
            </chartareas>
      </asp:Chart>


    <h5>
        <span>Article activity (requests, uploads, recommendations) </span>
    </h5>


            <asp:Chart ID="chArticles" runat="server" Width="675px">
                <series>
                <asp:Series name="Offers"  >
                </asp:Series>
                <asp:Series Name="Requests">
                </asp:Series>
                <asp:Series Name="Recommendations">
                </asp:Series>
        
        
            </series>
                <chartareas>
                <asp:ChartArea Name="ChartArea1">
                    <axisx>
                        <LabelStyle Format="d" />
                    </axisx>
                </asp:ChartArea>
            </chartareas>
            </asp:Chart>
</asp:Content>

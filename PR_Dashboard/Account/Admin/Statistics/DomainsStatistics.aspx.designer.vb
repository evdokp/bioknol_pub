﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class DomainsStatistics

    '''<summary>
    '''SetPeriod1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SetPeriod1 As Global.BioWS.SetPeriod

    '''<summary>
    '''cmbStat control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmbStat As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''litTotalCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litTotalCount As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''litRecCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litRecCount As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''cmbTop control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmbTop As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''cmbType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmbType As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''barDomains control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents barDomains As Global.System.Web.UI.DataVisualization.Charting.Chart
End Class

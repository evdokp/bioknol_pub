﻿Imports System.Web.UI.DataVisualization.Charting
Imports DevExpress.Web
Public Class ContentSummary
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            CType(Master.Master.FindControl("lit_pagetitle"), Literal).Text = "Personal reference dashboard management"
            CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "Documentation summary"



            Dim ciTA As New DataSetContentItemsTableAdapters.ContentItemsTableAdapter
            Dim ciT = ciTA.GetTopLevelItems()

            Dim ciSRC = From item In ciT
                        Select item.ID, item.Title, ItemsCount = ciTA.GetAllChildrenCountInclSelf(item.ID)

            ASPxGridView1.DataSource = ciSRC.ToList()
            ASPxGridView1.DataBind()








        End If
    End Sub

End Class
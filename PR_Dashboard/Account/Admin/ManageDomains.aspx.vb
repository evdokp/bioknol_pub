﻿Imports System.Web.UI.DataVisualization.Charting
Imports DevExpress.Web
Imports System.Xml
Imports BioWS.BLL.Extensions

Public Class ManageDomains
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            CType(Master.Master.FindControl("lit_pagetitle"), Literal).Text = "Personal reference dashboard management"
            CType(Master.Master.FindControl("lit_curobj"), Literal).Text = "Manage domains"
            litDomainsMem.Text = BLL.ParsingHelper.DomainsCount
            litRulesMem.Text = BLL.ParsingHelper.ParsingRulesCount
        End If
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click


        Dim domainOriginal = txtURL.Text.Trim
        Dim domainAlias As String
        If domainOriginal.StartsWith("www.") Then
            domainAlias = domainOriginal.Substring(4, domainOriginal.Length - 4)
        Else
            domainAlias = "www." & domainOriginal
        End If

        Dim domainsTA As New MonitoredSitesDataSetTableAdapters.MonitoredSitesTableAdapter
        Dim domainsT = domainsTA.GetDataByLikeURLall(domainOriginal & "%", domainAlias & "%")

        If domainsT.Count > 0 Then
            Dim s As String
            s = "<br/>Cannot add duplicate site:<br/><ul>"
            domainsT.ProcessTable(Of MonitoredSitesDataSet.MonitoredSitesRow)(Sub(a) s = String.Format("{0}<li>{1}</li>", s, a.SiteURL))
            s = s & "</ul>"
            litValidation.Text = s
        Else
            Dim domain As New BLL.DomainMeta(txtURL.Text, txtTitle.Text, cmbDefaultPA.SelectedValue)
            Response.Redirect(domain.EditURL)
        End If

    End Sub

    Protected Sub btnReload_Click(sender As Object, e As EventArgs) Handles btnReload.Click
        BLL.ParsingHelper.Reload()
        litDomainsMem.Text = BLL.ParsingHelper.DomainsCount
        litRulesMem.Text = BLL.ParsingHelper.ParsingRulesCount
    End Sub
End Class
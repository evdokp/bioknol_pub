﻿Imports System.Web.UI.DataVisualization.Charting
Imports DevExpress.Web
Imports BioWS.BLL.Extensions

Public Class DomainStatistics
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.NotPostBack() Then
            Me.SetTitlesToGrandMaster("Personal reference dashboard management", "Domain statistics")

            Dim domain As New BLL.DomainMeta(Me.QSInt32("DomainID"))
            litTitle.Text = domain.PageHeader
            litTotal.Text = domain.RecorderedTotalViews
            litPubmedViews.Text = domain.RecorderedPubmedViews
            litDistinctPM.Text = domain.RecorderedDistinctPubmedIDs
            litFirstView.Text = domain.RecorderedFirstViewDateTime.ToString
            litLastView.Text = domain.RecorderedLastViewDateTime.ToString
            litDurationDays.Text = domain.RecordingDurationDays
            litAvgViews.Text = domain.AvgViewsPerDayRecordered.ToString("f2")

            Dim pr As New ProfileCommon
            pr = pr.GetProfile(HttpContext.Current.User.Identity.Name)

          
            DataBindCharts(Today.AddDays(-pr.DefaultPeriodDays), Today)
        End If
    End Sub

    Private Sub SetPeriod1_PeriodChanged(ByVal StartDate As Date, ByVal EndDate As Date) Handles SetPeriod1.PeriodChanged
        DataBindCharts(StartDate, EndDate)
    End Sub

    Private Sub DataBindCharts(ByVal StartDate As Date, ByVal EndDate As Date)
        DataBindPieCharts(StartDate, EndDate)
        DataBindTimeSeries(StartDate, EndDate)
    End Sub


    Private Sub DataBindTimeSeries(ByVal StartDate As Date, ByVal EndDate As Date)
        Dim cha As ChartArea = Me.chViewsTimeSeries.ChartAreas("ChartArea1")
        cha.BorderColor = Drawing.Color.LightGray
        cha.BorderWidth = 1
        cha.BorderDashStyle = ChartDashStyle.Solid
        cha.AxisY.MajorGrid.LineColor = Drawing.Color.LightGray
        cha.AxisX.MajorGrid.LineColor = Drawing.Color.LightGray
        cha.AxisY.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha.AxisX.MajorTickMark.LineColor = Drawing.Color.LightGray
        cha.AxisY.LineColor = Drawing.Color.LightGray
        cha.AxisX.LineColor = Drawing.Color.LightGray
        cha.AxisX.IntervalType = DateTimeIntervalType.Days
        cha.AxisX.IsMarginVisible = False

        Dim sNoPM As Series = Me.chViewsTimeSeries.Series("sNoPM")
        sNoPM.ChartType = ddlChartType.SelectedValue
        sNoPM.XValueType = ChartValueType.Date

        Dim sPM As Series = Me.chViewsTimeSeries.Series("sPM")
        sPM.ChartType = ddlChartType.SelectedValue
        sPM.XValueType = ChartValueType.Date




        Dim l As New Legend() With {.Docking = Docking.Bottom, .Alignment = Drawing.StringAlignment.Far, .IsDockedInsideChartArea = False}
        chViewsTimeSeries.Legends.Add(l)
        chViewsTimeSeries.ChartAreas("ChartArea1").AxisX.LabelStyle.Format = "dd MMM yyyy"

        Dim query As String = String.Format("SELECT     AllDates.CalendarDate, SUM(CASE WHEN [PubMed_ID] IS NULL THEN 1 ELSE 0 END) AS NoPM, SUM(CASE WHEN [PubMed_ID] IS NULL THEN 0 ELSE 1 END) AS PM " & _
            " FROM         ViewsLog INNER JOIN " & _
            "                      AllDates ON ViewsLog.ViewDate = AllDates.CalendarDate " & _
            " WHERE     (ViewsLog.MonitoredSite_ID = {0}) AND (AllDates.CalendarDate BETWEEN '{1}' AND '{2}') " & _
            " GROUP BY AllDates.CalendarDate " & _
            " ORDER BY AllDates.CalendarDate", Request.QueryString("DomainID"), StartDate.ToString, EndDate.ToString)
        Dim dt1 As DataTable = BLL.SQLHelper.GetDT_AllStrings(query)



        sNoPM.Points.Clear()
        sPM.Points.Clear()


        Dim cNoPM, cPM As Integer

        For i = 0 To dt1.Rows.Count - 1
            Dim row As DataRow = dt1.Rows(i)

            Dim dpPM As New DataPoint()
            dpPM.SetValueXY(CDate(row(0)), row(2))
            sPM.Points.Add(dpPM)

            Dim dpNoPM As New DataPoint()
            dpNoPM.SetValueXY(CDate(row(0)), row(1))
            sNoPM.Points.Add(dpNoPM)

            cNoPM = cNoPM + row(1)
            cPM = cPM + row(2)

        Next




        sNoPM.LegendText = String.Format("Views without PMID ({0})", cNoPM)
        sPM.LegendText = String.Format("Views with PMID ({0})", cPM)




        SetGroupping()


    End Sub
    Private Sub SetGroupping()

        Dim it As IntervalType
        Dim int As Integer

        Select Case ddlGrInt.SelectedValue
            Case "Day"
                it = IntervalType.Days
                int = 1
            Case "Week"
                it = IntervalType.Weeks
                int = 1
            Case "2 Weeks"
                it = IntervalType.Weeks
                int = 2
            Case "Month"
                it = IntervalType.Months
                int = 1
        End Select
        Me.chViewsTimeSeries.ChartAreas("ChartArea1").AxisX.IntervalType = it
        Me.chViewsTimeSeries.ChartAreas("ChartArea1").AxisX.LabelStyle.IntervalType = it
        Me.chViewsTimeSeries.DataManipulator.Group("SUM, X:LAST", int, it, Me.chViewsTimeSeries.Series("sNoPM"))
        Me.chViewsTimeSeries.DataManipulator.Group("SUM, X:LAST", int, it, Me.chViewsTimeSeries.Series("sPM"))
    End Sub
    Private Sub DataBindPieCharts(ByVal StartDate As Date, ByVal EndDate As Date)
        Dim ta As New ViewsLogDataSetTableAdapters.ViewsLogTableAdapter
        Dim vAll, vPubmed, vUnPubmed As Integer
        vAll = ta.GetCountAll_ByTwoDateDomainID(StartDate, EndDate, Request.QueryString("DomainID"))
        vPubmed = ta.GetCountWithPubMed_ByTwoDatesDomainID(StartDate, EndDate, Request.QueryString("DomainID"))
        vUnPubmed = vAll - vPubmed

        Dim dp As New DataPoint
        dp.SetValueXY("ID was not detected", vUnPubmed)

        Dim dp1 As New DataPoint
        dp1.SetValueXY("ID was detected", vPubmed)
        dp1.CustomProperties += "Exploded=true"

        Me.pie_pubmedid.Series("Default").Points.Add(dp)
        Me.pie_pubmedid.Series("Default").Points.Add(dp1)




        Dim v_doi_DOI, v_doi_UnDOI As Integer

        v_doi_DOI = ta.GetCountWithDOI_ByTwoDatesDomainID(StartDate, EndDate, Request.QueryString("DomainID"))
        v_doi_UnDOI = vAll - v_doi_DOI

        Dim dp_doi As New DataPoint
        dp_doi.SetValueXY("ID was not detected", v_doi_UnDOI)

        Dim dp1_nondoi As New DataPoint
        dp1_nondoi.SetValueXY("ID was detected", v_doi_DOI)
        dp1_nondoi.CustomProperties += "Exploded=true"

        Me.pie_doi.Series("Default").Points.Add(dp_doi)
        Me.pie_doi.Series("Default").Points.Add(dp1_nondoi)
    End Sub
    
    Private Sub ddlChartType_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlChartType.SelectedIndexChanged
        DataBindCharts(SetPeriod1.StartDate, SetPeriod1.EndDate)
    End Sub

    Private Sub ddlGrInt_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlGrInt.SelectedIndexChanged
        DataBindCharts(SetPeriod1.StartDate, SetPeriod1.EndDate)
    End Sub
End Class
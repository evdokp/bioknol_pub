﻿Imports System.Web.UI.DataVisualization.Charting
Imports DevExpress.Web
Imports BioWS.BLL.Extensions

Public Class DeleteDomain
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.NotPostBack() Then
            Me.SetTitlesToGrandMaster("Personal reference dashboard management", "Delete domain")

            Dim domain As New BLL.DomainMeta(Me.QSInt32("DomainID"))
            litTitle.Text = domain.PageHeader
            litRecCount.Text = String.Format("{0}/{1}", domain.RecorderedTotalViews, domain.RecorderedPubmedViews)
            litPeriod.Text = domain.RecorderedFirstViewDateTime.ToString & " - " & domain.RecorderedLastViewDateTime.ToString

        End If
    End Sub

    


    Private Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.Click
        Dim domain As New BLL.DomainMeta(Me.QSInt32("DomainID"))
        domain.Delete()
        Response.Redirect("~\Account\Admin\ManageDomains.aspx")
    End Sub
End Class
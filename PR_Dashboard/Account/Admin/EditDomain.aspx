﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Masters/Admin.Master"  
    CodeBehind="EditDomain.aspx.vb" Inherits="BioWS.EditDomain" %>
    <%@ Register assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <h1 style="margin-top:1px; margin-bottom:12px;">
            <asp:Literal ID="litTitle" runat="server"></asp:Literal>
</h1>

<h5><span>Basic domain information</span></h5>

  <table width="100%">
    <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Title:
            </td>
            <td valign="top" style="" class="td_field">
                <dx:ASPxTextBox ID="txtTitle" runat="server"  Width="100%" NullText="Enter title here"
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" >
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                URL:
            </td>
            <td valign="top" style="" class="td_field">
                <dx:ASPxTextBox ID="txtURL" runat="server"  Width="100%" NullText="Enter URL here without 'http://'"
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Alias:
            </td>
            <td valign="top" style="" class="td_field">
              <span class="EmptyDataText">
                <asp:Literal ID="litAlias" runat="server">Alias will be displayed here (with or without 'www.')</asp:Literal>
              </span>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Is approved:
            </td>
            <td valign="top" style="" class="td_field">
                <dx:ASPxCheckBox ID="chkApproved" runat="server">
                </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Default parsing action:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:ObjectDataSource ID="odsParsingActions" runat="server" 
                    DeleteMethod="Delete" InsertMethod="Insert" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                    TypeName="BioWS.MonitoredSitesDataSetTableAdapters.ParsingActionsTableAdapter" 
                    UpdateMethod="Update">
                    <DeleteParameters>
                        <asp:Parameter Name="Original_ID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Title" Type="String" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Title" Type="String" />
                        <asp:Parameter Name="Original_ID" Type="Int32" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:DropDownList ID="cmbDefaultPA" runat="server" 
                    DataSourceID="odsParsingActions" DataTextField="Title" DataValueField="ID">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                &nbsp;</td>
            <td valign="top" style="" class="td_field">
            <table cellpadding="0" cellspacing="0" border="0">
            <tr>
            <td>
                <dx:ASPxButton ID="btnSave" runat="server" 
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Save">
                </dx:ASPxButton>
            </td>
            <td style="padding-left:5px;">
                <dx:ASPxButton ID="btnDelete" runat="server" 
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Delete domain" >
                </dx:ASPxButton>
            </td>
            </tr>
            </table>
            
            </td>
        </tr>
            </table>


<h5><span>Parsing rules
    </span></h5>
<asp:ObjectDataSource ID="odsRules" runat="server" DeleteMethod="Delete" 
        InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" 
        SelectMethod="GetDataByDomainID" 
        TypeName="BioWS.MonitoredSitesDataSetTableAdapters.ParsingRulesTableAdapter" 
        UpdateMethod="Update">
    <DeleteParameters>
        <asp:Parameter Name="Original_ID" Type="Int32" />
    </DeleteParameters>
    <InsertParameters>
        <asp:Parameter Name="URLValidatingRegex" Type="String" />
        <asp:Parameter Name="ParsingActionID" Type="Int32" />
        <asp:Parameter Name="SpecificPubmedRegex" Type="String" />
        <asp:Parameter Name="SpecificDOIRegex" Type="String" />
        <asp:Parameter Name="MonitoredSiteID" Type="Int32" />
    </InsertParameters>
    <SelectParameters>
        <asp:QueryStringParameter Name="DomainID" QueryStringField="DomainID" 
            Type="Int32" />
    </SelectParameters>
    <UpdateParameters>
        <asp:Parameter Name="URLValidatingRegex" Type="String" />
        <asp:Parameter Name="ParsingActionID" Type="Int32" />
        <asp:Parameter Name="SpecificPubmedRegex" Type="String" />
        <asp:Parameter Name="SpecificDOIRegex" Type="String" />
        <asp:Parameter Name="MonitoredSiteID" Type="Int32" />
        <asp:Parameter Name="Original_ID" Type="Int32" />
    </UpdateParameters>
    </asp:ObjectDataSource>







    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" 
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx"  
        DataSourceID="odsRules" KeyFieldName="ID" Width="100%" 
        style="margin-right: 0px">
        <Columns>
            <dx:GridViewDataTextColumn FieldName="SpecificDOIRegex" 
                ShowInCustomizationForm="True" Visible="False" VisibleIndex="5">
                <EditFormSettings ColumnSpan="2" Visible="True" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="SpecificPubmedRegex" 
                ShowInCustomizationForm="True" Visible="False" VisibleIndex="4">
                <EditFormSettings ColumnSpan="2" Visible="True" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn FieldName="ParsingActionID" VisibleIndex="3" 
                Caption="Parsing action" Visible="False" >
                <PropertiesComboBox DataSourceID="odsParsingActions" 
                    Spacing="0" ValueType="System.String" TextField="Title" ValueField="ID">
                </PropertiesComboBox>
                <EditFormSettings ColumnSpan="2" Visible="True" />
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataTextColumn FieldName="MonitoredSiteID" 
                ShowInCustomizationForm="True" Visible="False" VisibleIndex="6">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewCommandColumn VisibleIndex="0" Width="50">
                <EditButton Visible="True">
                </EditButton>
                <NewButton Visible="True">
                </NewButton>
                <DeleteButton Visible="True">
                </DeleteButton>
                <ClearFilterButton Visible="True">
                </ClearFilterButton>
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle Wrap="True">
                    <Paddings Padding="1px" />
                </CellStyle>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="ID" ReadOnly="True" VisibleIndex="1"  Width="40px"
                ShowInCustomizationForm="True" >
                <EditFormSettings Visible="False" />
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center">
                </CellStyle>
            </dx:GridViewDataTextColumn> 
               <dx:GridViewDataTextColumn VisibleIndex="2" Caption="Rule" 
                FieldName="URLValidatingRegex">
                   <EditFormSettings Caption="URL validating regex" ColumnSpan="2" />
               <DataItemTemplate>
                 

                <table width="100%"  >
            

        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                URL validating regex:
            </td>
            <td valign="top" style="" class="td_field">
                    <%# HttpUtility.HtmlEncode(Eval("URLValidatingRegex"))%>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Parsing action:
            </td>
            <td valign="top" style="" class="td_field">
                    <%# GetParsingAction(Eval("ParsingActionID"))%>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Specific Pubmed regex:
            </td>
            <td valign="top" style="" class="td_field">
                    <%# HttpUtility.HtmlEncode(Eval("SpecificPubmedRegex")) %>
            </td>
        </tr><tr>
            <td valign="top" style="text-align: right;" class="td_label">
                Specific DOI regex:
            </td>
            <td valign="top" style="" class="td_field">
                    <%# HttpUtility.HtmlEncode(Eval("SpecificDOIRegex")) %>
            </td>
        </tr>
        </table>
            
               
               
               </DataItemTemplate>
            </dx:GridViewDataTextColumn>
        </Columns>
   
        <SettingsEditing Mode="EditForm" />
        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
            <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
            </LoadingPanelOnStatusBar>
            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
            </LoadingPanel>
        </Images>
        <ImagesFilterControl>
            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
            </LoadingPanel>
        </ImagesFilterControl>
        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
            <Header ImageSpacing="5px" SortingImageSpacing="5px">
            </Header>
            <LoadingPanel ImageSpacing="5px">
            </LoadingPanel>
        </Styles>
        <StylesEditors ButtonEditCellSpacing="0">
            <ProgressBar Height="21px">
            </ProgressBar>
        </StylesEditors>
    </dx:ASPxGridView>


    <h5><span>Regular expressions tools and links</span></h5>
    <ul>
    <li>
    <a href="javascript:void(0)" onclick="window.open('RegexEditor.aspx', 'RegexTester', 'width=1000,height=600,resizable,scrollbars=yes,status=0,location=0')">Open PRD regular expressions tester</a>
    </li>
    <li>
    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://fishcodelib.com/Convert.htm" Target="_search"  >Go to Covert.NET regular expressions tool website</asp:HyperLink>
    </li>
    <li>
    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="http://www.regular-expressions.info/" Target="_search"  >Go to regular-expressions.info website</asp:HyperLink>
    </li>
    <li>
    <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="http://www.pluralsight-training.net/microsoft/Courses/TableOfContents?courseName=net-regular-expressions" Target="_search"  >
    Go to Pluralsight .NET Regex training course</asp:HyperLink>
    </li>
    
    </ul>
    


</asp:Content>

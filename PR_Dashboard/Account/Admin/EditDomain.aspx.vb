﻿Imports System.Web.UI.DataVisualization.Charting
Imports DevExpress.Web
Imports BioWS.BLL.Extensions

Public Class EditDomain
    Inherits System.Web.UI.Page


    Private _Domain As BLL.DomainMeta
    Private ReadOnly Property Domain As BLL.DomainMeta
        Get
            If _Domain Is Nothing Then
                _Domain = New BLL.DomainMeta(Me.QSInt32("DomainID"))
            End If
            Return _Domain
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.NotPostBack() Then
            Me.SetTitlesToGrandMaster("Personal reference dashboard management", "Edit domain")
            litTitle.Text = Domain.PageHeader
            txtTitle.Text = Domain.Title
            txtURL.Text = Domain.SiteURL
            chkApproved.Checked = Domain.IsApproved
            cmbDefaultPA.SelectedValue = Domain.DefaultParsingActionType
            litAlias.Text = Domain.URLAlias
        End If
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Domain.Title = txtTitle.Text
        Domain.SiteURL = txtURL.Text
        Domain.IsApproved = chkApproved.Checked
        Domain.DefaultParsingActionType = cmbDefaultPA.SelectedValue

        Domain.Update()

        litTitle.Text = Domain.PageHeader
        litAlias.Text = Domain.URLAlias
    End Sub


    Public Function GetParsingAction(ByVal parsingActionID As Integer) As String
        Dim paTA As New MonitoredSitesDataSetTableAdapters.ParsingActionsTableAdapter
        Return paTA.GetDataByID(parsingActionID)(0).Title
    End Function


    Protected Sub odsRules_Inserting(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceMethodEventArgs) Handles odsRules.Inserting
        e.InputParameters("MonitoredSiteID") = Me.QS("DomainID")
    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Response.Redirect("~\Account\Admin\DeleteDomain.aspx?DomainID=" & Me.QS("DomainID"))
    End Sub
End Class
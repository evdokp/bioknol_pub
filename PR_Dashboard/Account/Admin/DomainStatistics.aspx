﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Masters/Admin.Master"  
    CodeBehind="DomainStatistics.aspx.vb" Inherits="BioWS.DomainStatistics" %>
    <%@ Register assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<%@ Register Src="~/UserControls/SetPeriod.ascx" TagName="SetPeriod" TagPrefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <h1 style="margin-top:1px; margin-bottom:12px;">
            <asp:Literal ID="litTitle" runat="server"></asp:Literal>
</h1>



     <uc1:SetPeriod ID="SetPeriod1" runat="server" />


<h5>
        <span>Overview (for all time)</span>
    </h5>

    
  <table width="100%">
    <tr>
            <td valign="top" style="text-align: right; width:360px;" class="td_label">
                Total number of recordered views:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litTotal" runat="server"></asp:Literal>
            </td>
        </tr>

        <tr>
            <td valign="top" style="text-align: right;width:360px;" class="td_label">
                Number of recordered views with detected Pubmed ID:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litPubmedViews" runat="server"></asp:Literal>
            </td>
        </tr>

        <tr>
            <td valign="top" style="text-align: right;width:360px;" class="td_label">
                Number of distinct Pubmed IDs:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litDistinctPM" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;width:360px;" class="td_label">
                First view recordered on:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litFirstView" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;width:360px;" class="td_label">
                Last view recordered on:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litLastView" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;width:360px;" class="td_label">
                Recording duration:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litDurationDays" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: right;width:360px;" class="td_label">
                Averages views per day recordered:
            </td>
            <td valign="top" style="" class="td_field">
                <asp:Literal ID="litAvgViews" runat="server"></asp:Literal>
            </td>
        </tr>
        </table>

    

 <h5>
        <span>Share of views with detected identifiers (for selected period) </span>
    </h5>
        <table width ="100%">
    <tr>
    <td style ="width :50%;">
    <asp:Chart ID="pie_pubmedid" runat="server" Palette="BrightPastel" Width="335px">
        <Series>
            <asp:Series Name="Default" ChartType="Pie" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240"
                CustomProperties="LabelsHorizontalLineSize=2, PieLabelStyle=Outside, PieDrawingStyle=Concave, CollectedThreshold=5, PieLineColor=SlateGray, CollectedLegendText=Другие\n#VAL (#PERCENT), CollectedLabel=Другие"
                Label="#VALX\n#VAL (#PERCENT)" LegendText="#VALX" XValueType="String" YValueType="Int32">
                <SmartLabelStyle AllowOutsidePlotArea="Yes" IsOverlappedHidden="False" MaxMovingDistance="40" />
            </asp:Series>
        </Series>
        <Titles>
            <asp:Title Name="Title1" Text="Share of PubMedID-detected records" >
            </asp:Title>
        </Titles>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent"
                BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                <Area3DStyle Rotation="0" />
                <AxisY LineColor="64, 64, 64, 64">
                    <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                    <MajorGrid LineColor="64, 64, 64, 64" />
                </AxisY>
                <AxisX LineColor="64, 64, 64, 64">
                    <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                    <MajorGrid LineColor="64, 64, 64, 64" />
                </AxisX>
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
    
    </td>
    <td style ="width :50%;">
    <asp:Chart ID="pie_doi" runat="server" Palette="BrightPastel" Width="335px">
        <Series>
            <asp:Series Name="Default" ChartType="Pie" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240"
                CustomProperties="LabelsHorizontalLineSize=2, PieLabelStyle=Outside, PieDrawingStyle=Concave, CollectedThreshold=5, PieLineColor=SlateGray, CollectedLegendText=Другие\n#VAL (#PERCENT), CollectedLabel=Другие"
                Label="#VALX\n#VAL (#PERCENT)" LegendText="#VALX" XValueType="String" YValueType="Int32">
                <SmartLabelStyle AllowOutsidePlotArea="Yes" IsOverlappedHidden="False" MaxMovingDistance="40" />
            </asp:Series>
        </Series>
        <Titles>
            <asp:Title Name="Title1" Text="Share of DOI-detected records" >
            </asp:Title>
        </Titles>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent"
                BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                <Area3DStyle Rotation="0" />
                <AxisY LineColor="64, 64, 64, 64">
                    <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                    <MajorGrid LineColor="64, 64, 64, 64" />
                </AxisY>
                <AxisX LineColor="64, 64, 64, 64">
                    <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                    <MajorGrid LineColor="64, 64, 64, 64" />
                </AxisX>
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
    
    </td>
    
    </tr>
    
    </table>
     <h5>
        <span>Views count and Pubmed-detected share timeline (for selected period)</span>
    </h5>
            <asp:Chart ID="chViewsTimeSeries" runat="server" Width="675px">
                <series>
                <asp:Series Name="sPM">
                </asp:Series>
                <asp:Series name="sNoPM"  >
                </asp:Series>
                
        
        
            </series>
                <chartareas>
                <asp:ChartArea Name="ChartArea1">
                    <axisx>
                        <LabelStyle Format="d" />
                    </axisx>
                </asp:ChartArea>
            </chartareas>
            </asp:Chart>
            <table cellpadding="2" cellspacing="2">
        <tr>
            <td align="right">
              Grouping interval:
            </td>
            <td>
                <asp:DropDownList ID="ddlGrInt" runat="server" AutoPostBack="true">
                    <asp:ListItem Text="Day" Value="Day" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Week" Value="Week"></asp:ListItem>
                    <asp:ListItem Text="2 Weeks" Value="2 Weeks"></asp:ListItem>
                    <asp:ListItem Text="Month" Value="Month"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
             Chart type:
            </td>
            <td>
                <asp:DropDownList ID="ddlChartType" runat="server" AutoPostBack="true">
                    <asp:ListItem Text="Stacked area chart" Value="15" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Stacked area chart 100%" Value="16"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <h5><span>Top URLs with unidentified Pubmed IDs (for selected period)</span></h5>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:bio_dataConnectionString %>" 
        SelectCommand="select top 100 URLwoQS,count(*) as ViewsCount from (SELECT case when charindex('?',URL)>0 then substring(URL,1,charindex('?',URL))  + '... query string'     else URL end as URLwoQS  
FROM [dbo].[ViewsLog]  where [MonitoredSite_ID] = @DomainID and Pubmed_ID is null  and ViewDate between @StartDate and @EndDate) as t   group by URLwoQS  order by ViewsCount desc">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlChartType" Name="ActionDate" 
                PropertyName="SelectedValue" Type="String" />
                <asp:QueryStringParameter Name="DomainID" QueryStringField="DomainID" 
                Type="Int32" />
                <asp:ControlParameter ControlID="SetPeriod1" Name="StartDate" 
                PropertyName="StartDate" Type="String" />
                <asp:ControlParameter ControlID="SetPeriod1" Name="EndDate" 
                PropertyName="EndDate" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>






    <dx:ASPxGridView ID="ASPxGridView1" runat="server" 
        DataSourceID="SqlDataSource1" 
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
            <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
            </LoadingPanelOnStatusBar>
            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
            </LoadingPanel>
        </Images>
        <ImagesFilterControl>
            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
            </LoadingPanel>
        </ImagesFilterControl>
        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
            <Header ImageSpacing="5px" SortingImageSpacing="5px">
            </Header>
            <LoadingPanel ImageSpacing="5px">
            </LoadingPanel>
        </Styles>
        <StylesEditors ButtonEditCellSpacing="0">
            <ProgressBar Height="21px">
            </ProgressBar>
        </StylesEditors>
    </dx:ASPxGridView>




</asp:Content>

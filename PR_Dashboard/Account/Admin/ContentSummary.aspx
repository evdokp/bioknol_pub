﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Masters/Admin.Master"  
    CodeBehind="ContentSummary.aspx.vb" Inherits="BioWS.ContentSummary" %>
    

<%@ Register assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
    

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


<div style="margin-top:15px;">

    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False"  Width="100%"
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
        <Columns>
              <dx:GridViewDataTextColumn    Caption ="Documentations section" VisibleIndex="0">
                <DataItemTemplate>
                   <asp:Literal ID="lit1" runat="server" Text='<%# Eval("Title")%>'></asp:Literal> 
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn    Caption ="Items count" VisibleIndex="1" Width="150px">
                <DataItemTemplate>
                <asp:Literal ID="lit2" runat="server" Text='<%# Eval("ItemsCount")%>'></asp:Literal> 
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn    Caption ="" VisibleIndex="2" Width="70px">
                <DataItemTemplate>
                <asp:HyperLink ID="h1" runat="server" NavigateUrl='<%# "~\Help\ViewItem.aspx#" & Eval("ID")%>'>View</asp:HyperLink>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn    Caption ="" VisibleIndex="2" Width="70px">
                <DataItemTemplate>
                <asp:HyperLink ID="h1" runat="server" NavigateUrl='<%# "~\Account\Admin\EditContentItem.aspx?path=" & Eval("ID")%>'>Edit</asp:HyperLink>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
        </Columns>
        <Images SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
            <LoadingPanelOnStatusBar Url="~/App_Themes/DevEx/GridView/StatusBarLoading.gif">
            </LoadingPanelOnStatusBar>
            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
            </LoadingPanel>
        </Images>
        <ImagesFilterControl>
            <LoadingPanel Url="~/App_Themes/DevEx/GridView/Loading.gif">
            </LoadingPanel>
        </ImagesFilterControl>
        <Styles CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx">
            <Header ImageSpacing="5px" SortingImageSpacing="5px">
            </Header>
            <LoadingPanel ImageSpacing="5px">
            </LoadingPanel>
        </Styles>
        <StylesEditors ButtonEditCellSpacing="0">
            <ProgressBar Height="21px">
            </ProgressBar>
        </StylesEditors>
    </dx:ASPxGridView>

    </div>


</asp:Content>

﻿Imports System.Drawing
Imports System.Linq
Imports System.Threading
Imports System.Xml

Public Class EditContentItem
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim pathIntList As New List(Of Integer)
            If String.IsNullOrEmpty(Request.QueryString("path")) = False Then
                Dim initialpath As String = Request.QueryString("path")
                initialpath = initialpath.Replace("#", "")
                Dim pathArray As String() = initialpath.Split("/")
                For Each s In pathArray
                    Dim id As Integer
                    If Integer.TryParse(s, id) Then
                        pathIntList.Add(id)
                    End If
                Next

                Dim curCIid As Integer
                If Integer.TryParse(pathArray(pathArray.Length - 1), curCIid) Then
                

                    Dim _profile As New ProfileCommon
                    _profile = _profile.GetProfile(User.Identity.Name)
                    Dim pers As New BLL.Person(_profile.Person_ID)


                End If
            Else
                btnSave.Visible = False
                txtBody.Enabled = False
                txtTitle.Enabled = False
                Literal1.Text = "Select content item to edit"
            End If
            PopulateTV(pathIntList)
        End If
    End Sub


    Private Sub PopulateTV(ByVal path As List(Of Integer))
        Dim ciTa As New DataSetContentItemsTableAdapters.v_ContentItems1TableAdapter
        Dim ctTopT = ciTa.GetTopLevel()
        For i = 0 To ctTopT.Count - 1
            Dim r = ctTopT(i)
            Dim node As New DevExpress.Web.ASPxTreeView.TreeViewNode
            node.Name = r.ID
            node.NavigateUrl = "~\Account\Admin\EditContentItem.aspx?path=" & node.Name
            node.Text = r.Title
            tv1.Nodes.Add(node)
            If path.Contains(r.ID) Then
                If path(path.Count - 1) = r.ID Then
                    tv1.SelectedNode = node
                    node.Expanded = False

                    Dim ciBodyTA As New DataSetContentItemsTableAdapters.v_ContentItemsTableAdapter
                    Dim ciR = ciBodyTA.GetDataByID(r.ID)(0)
                    txtTitle.Text = ciR.Title
                    txtBody.Html = ciR.Body

                Else
                    node.Expanded = True
                End If
            End If
            If r.ChildrenCount > 0 Then
                PopulateNode(r.ID, node, path)
            End If
        Next
    End Sub


    Private Sub PopulateNode(ByVal parentid As Integer, ByVal parentnode As DevExpress.Web.ASPxTreeView.TreeViewNode, ByVal path As List(Of Integer))
        Dim ciTa As New DataSetContentItemsTableAdapters.v_ContentItems1TableAdapter
        Dim ctT = ciTa.GetDataByParentID(parentid)
        For i = 0 To ctT.Count - 1
            Dim r = ctT(i)
            Dim node As New DevExpress.Web.ASPxTreeView.TreeViewNode
            node.Name = String.Format("{0}/{1}", parentnode.Name, r.ID)
            node.NavigateUrl = "~\Account\Admin\EditContentItem.aspx?path=" & node.Name
            node.Text = r.Title
            parentnode.Nodes.Add(node)
            If path.Contains(r.ID) Then
                If path(path.Count - 1) = r.ID Then
                    tv1.SelectedNode = node
                    node.Expanded = False

                    Dim ciBodyTA As New DataSetContentItemsTableAdapters.v_ContentItemsTableAdapter
                    Dim ciR = ciBodyTA.GetDataByID(r.ID)(0)
                    txtTitle.Text = ciR.Title
                    txtBody.Html = ciR.Body
                Else
                    node.Expanded = True
                End If
            End If
            If r.ChildrenCount > 0 Then
                PopulateNode(r.ID, node, path)
            End If
        Next
    End Sub

    


    <System.Web.Services.WebMethod()> _
    Public Shared Function GetBody(ByVal nodename As String) As String
        Dim id As Integer
        Dim sa As String() = nodename.Split("/")
        id = sa(sa.Length - 1)

        Dim ciTA As New DataSetContentItemsTableAdapters.v_ContentItemsTableAdapter
        Dim ciR = ciTA.GetDataByID(id)(0)
        Return ciR.Body
    End Function

    Protected Sub ASPxMenu1_ItemClick(source As Object, e As DevExpress.Web.ASPxMenu.MenuItemEventArgs) Handles ASPxMenu1.ItemClick

        Select Case e.Item.Name
            Case "addroot"
                Dim cita As New DataSetContentItemsTableAdapters.ContentItemsTableAdapter
                cita.Insert(Nothing, "new root item", "")
                Response.Redirect("~\Account\Admin\EditContentItem.aspx")
            Case "addchild"
                If tv1.SelectedNode Is Nothing Then
                    Literal1.Text = "select parent node to add child to"
                Else
                    Dim newid As Integer
                    Dim cita As New DataSetContentItemsTableAdapters.ContentItemsTableAdapter
                    Dim id As Integer
                    Dim sa As String() = tv1.SelectedNode.Name.Split("/")
                    id = sa(sa.Length - 1)
                    cita.InsertContentItem(id, "new item", "", newid)
                    Response.Redirect(String.Format("~\Account\Admin\EditContentItem.aspx?path={0}/{1}", tv1.SelectedNode.Name, newid))
                End If
            Case "delete"
                If tv1.SelectedNode Is Nothing Then
                    Literal1.Text = "select node to delete"
                Else
                    Dim id As Integer
                    Dim sa As String() = tv1.SelectedNode.Name.Split("/")
                    id = sa(sa.Length - 1)
                    Dim viewcita As New DataSetContentItemsTableAdapters.v_ContentItemsTableAdapter
                    Dim cir = viewcita.GetDataByID(id)(0)
                    If cir.ChildrenCount > 0 Then
                        Literal1.Text = "cannot delete node with child nodes"
                    Else
                        Dim cita As New DataSetContentItemsTableAdapters.ContentItemsTableAdapter
                        cita.Delete(cir.ID)
                        Response.Redirect("~\Account\Admin\EditContentItem.aspx")
                    End If
                End If
        End Select
        
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim initialpath As String = Request.QueryString("path")
        initialpath = initialpath.Replace("#", "")
        Dim pathArray As String() = initialpath.Split("/")
        Dim curCIid As Integer
        If Integer.TryParse(pathArray(pathArray.Length - 1), curCIid) Then
            Dim cita As New DataSetContentItemsTableAdapters.ContentItemsTableAdapter
            Dim cir = cita.GetDataByID(curCIid)(0)
            cir.BeginEdit()
            cir.Title = txtTitle.Text
            cir.Body = txtBody.Html
            cir.EndEdit()
            cita.Update(cir)
            Literal2.Text = "Saved"
        End If
    End Sub
End Class
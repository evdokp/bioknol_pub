﻿Public Class uc_GroupItem
    Inherits System.Web.UI.UserControl

    Private _GroupID As Integer
    Public Property GroupID() As Integer
        Get
            Return _GroupID
        End Get
        Set(ByVal value As Integer)
            _GroupID = value

            Dim gr As New BLL.Group(GroupID)
            If gr.ParentGroup_ID < 2 Then
                Me.hl_grouptitle.Font.Bold = True
            End If
            Me.hl_grouptitle.Text = gr.Title
            Me.hl_grouptitle.NavigateUrl = gr.URL

            Dim s As String

            If gr.ParentGroup_ID > 1 Then
                Dim par_gr As New BLL.Group(gr.ParentGroup_ID)
                s = s & WriteRow("Parent group:", par_gr.Anchor)
            End If

            If gr.DirectChildGroupsCount > 0 Then
                s = s & WriteRow("Child groups count:", gr.DirectChildGroupsCount)
            End If

            If gr.AllParticipantsCountInclChildren > 0 Then
                s = s & WriteRow("Total (direct) participants count:", gr.AllParticipantsCountInclChildren & " (" & gr.DirectParticipantsCount & ")")
            End If
            Me.Literal1.Text = s



        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
   
    End Sub
    Private Function WriteRow(ByVal FieldName As String, ByVal FieldValue As String) As String
        Dim s As String
        s = "<tr>"
        s = s & "<td valign=""top"" class=""field_name2"" style=""width:190px; text-align:right;"">"
        s = s & FieldName
        s = s & "</td>"
        s = s & "<td valign=""top"" class=""field_value2"">"
        s = s & FieldValue
        s = s & "</td>"
        s = s & "</tr>"
        Return s

    End Function

End Class
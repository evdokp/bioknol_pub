﻿Imports System.Xml
Imports System.Drawing
Imports BioWS.BLL.Extensions



Public Class SetPeriod
    Inherits System.Web.UI.UserControl

    Public Property StartDate() As DateTime
    Public Property EndDate() As DateTime
    Public Event PeriodChanged(ByVal StartDate As DateTime, ByVal EndDate As DateTime)


    Protected Overrides Function SaveControlState() As Object
        Dim controlstate(3) As Object
        controlstate(1) = MyBase.SaveControlState()
        controlstate(2) = StartDate
        controlstate(3) = EndDate
        Return controlstate
    End Function
    Protected Overrides Sub LoadControlState(ByVal savedState As Object)
        Dim controlstate() As Object = CType(savedState, Object)
        MyBase.LoadControlState(controlstate(1))
        StartDate = CDate(controlstate(2))
        EndDate = CDate(controlstate(3))
    End Sub
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            txtYear.Text = Today.Year
            txtQuarter.Text = CInt((Now.Month + 2) / 3)
            txtQuarterYear.Text = Today.Year
            txtMonth.DateTime = New Date(Today.Year, Today.Month, 1)
            txtDay.Date = Today
            txtDayFrom.Date = Today
            txtDayTo.Date = Today
            ASPxDateEdit1.Date = StartDate
            ASPxDateEdit2.Date = EndDate
        End If
    End Sub

    
    Private Sub ASPxDateEdit1_DateChanged(sender As Object, e As System.EventArgs) Handles ASPxDateEdit1.DateChanged
        StartDate = ASPxDateEdit1.Date
        RaiseEvent PeriodChanged(ASPxDateEdit1.Date, ASPxDateEdit2.Date)
    End Sub

    Private Sub ASPxDateEdit2_DateChanged(sender As Object, e As System.EventArgs) Handles ASPxDateEdit2.DateChanged
        EndDate = ASPxDateEdit2.Date
        RaiseEvent PeriodChanged(ASPxDateEdit1.Date, ASPxDateEdit2.Date)
    End Sub




    Protected Sub btnChange_Click(sender As Object, e As EventArgs) Handles btnChange.Click
        Dim type As String = CType(hfPeriodType.Value, String)

        Dim startdt As DateTime
        Dim enddt As DateTime

        Select Case type
            Case "year"
                startdt = New Date(txtYear.Text, 1, 1)
                enddt = New Date(txtYear.Text, 12, 31)
            Case "quarter"
                Dim yearstart As DateTime = New Date(txtQuarterYear.Text, 1, 1)
                enddt = yearstart.AddMonths(3 * txtQuarter.Text).AddDays(-1)
                If chkQuarter.Checked = True Then
                    startdt = yearstart
                Else
                    startdt = yearstart.AddMonths(3 * txtQuarter.Text - 3)
                End If
            Case "month"
                enddt = txtMonth.DateTime.AddMonths(1).AddDays(-1)
                If chkMonth.Checked = True Then
                    startdt = New Date(txtMonth.DateTime.Year, 1, 1)
                Else
                    startdt = txtMonth.DateTime
                End If
            Case "day"
                enddt = txtDay.Date
                If chkDay.Checked = True Then
                    startdt = New Date(txtDay.Date.Year, 1, 1)
                Else
                    enddt = txtDay.Date
                End If
            Case "arbitrary"
                startdt = txtDayFrom.Date
                enddt = txtDayTo.Date
        End Select

        Dim days As Integer = DateDiff(DateInterval.Day, startdt, enddt)
        If HttpContext.Current.User.Identity.IsAuthenticated And chkSaveLength.Checked Then
            Dim _profile As New ProfileCommon
            _profile = _profile.GetProfile(HttpContext.Current.User.Identity.Name)
            _profile.DefaultPeriodDays = days
            _profile.Save()
        End If


        StartDate = startdt
        EndDate = enddt
        ASPxDateEdit1.Date = startdt
        ASPxDateEdit2.Date = enddt
        RaiseEvent PeriodChanged(ASPxDateEdit1.Date, ASPxDateEdit2.Date)


    End Sub


End Class
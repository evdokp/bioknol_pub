﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Widget.ascx.vb" Inherits="BioWS.UserControls_Widget" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSplitter" TagPrefix="dx" %>
<asp:Panel ID="PanelTitle" runat="server" CssClass="darklueHeader">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="middle" style="width: 20px;">
                <asp:Image ID="Image2" runat="server" EnableViewState="false" />
            </td>
            <td valign="middle">
                <asp:Literal ID="lit_w_title" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="PanelContent" runat="server">
    <div class="darklueContainer">
        <asp:MultiView ID="mv_content" runat="server">
            <asp:View ID="v_Internal" runat="server">
            
            </asp:View>
            <asp:View ID="v_External" runat="server">
                <asp:Literal ID="lit_ext_resp" runat="server"></asp:Literal>
            </asp:View>
        </asp:MultiView>
    </div>
</asp:Panel>
<cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server" CollapseControlID="PanelTitle"
    Collapsed="true" ExpandControlID="PanelTitle" ExpandedImage="~/Images/nav_down_blue.png"
    CollapsedImage="~/Images/nav_right_grey.png" ImageControlID="Image2" TargetControlID="PanelContent"
    Enabled="True">
</cc1:CollapsiblePanelExtender>

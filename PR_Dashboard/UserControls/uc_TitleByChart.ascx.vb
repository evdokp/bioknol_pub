﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.Web.UI.DataVisualization.Charting.Utilities
Public Class uc_TitleByChart
    Inherits System.Web.UI.UserControl

    Private _Title As String
    Public Property Title() As String
        Get
            Return _Title
        End Get
        Set(ByVal value As String)
            _Title = value

            Me.ChartTitle.Width = 675
            Me.ChartTitle.Height = 28
            Dim title1 As New Title
            title1.Alignment = System.Drawing.ContentAlignment.TopLeft
            title1.Font = New System.Drawing.Font("Trebuchet MS", 13.25F, System.Drawing.FontStyle.Bold)
            title1.ForeColor = System.Drawing.Color.FromArgb(26, 59, 105)
            title1.ShadowOffset = 3
            title1.Name = "Title1"
            title1.ShadowColor = System.Drawing.Color.FromArgb(32, 0, 0, 0)
            title1.Text = Title
            Me.ChartTitle.Titles.Clear()
            Me.ChartTitle.Titles.Add(title1)

        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
    End Sub

End Class
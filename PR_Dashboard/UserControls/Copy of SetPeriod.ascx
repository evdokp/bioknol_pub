﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SetPeriod.ascx.vb"
    Inherits="BioWS.SetPeriod" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.5.1.min.js" type="text/javascript"></script>
<script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js"></script>
<script type="text/javascript">


    $(document).ready(function () {
        $("[name=R1]").filter("[value=year]").attr("checked", "checked");
        redrawtable();
        reenablecontrols();
        $("[name=R1]").change(function () {
            redrawtable();
            reenablecontrols();
        });
    });

    function redrawtable() {
        $("#tblSetPeriod .pertr").removeClass('rowdisabled');
        $("#tblSetPeriod .pertr").addClass('rowdisabled');
        $('input[name=R1]:checked').parents('.pertr').removeClass('rowdisabled');
    }

    function reenablecontrols() {
        $('#hfPeriodType').val($('input[name=R1]:checked').val());
        switch ($('input[name=R1]:checked').val()) {
            case "year":
                txtYear.SetEnabled(true);
                txtQuarter.SetEnabled(false);
                txtQuarterYear.SetEnabled(false);
                chkQuarter.SetVisible(false);
                txtMonth.SetEnabled(false);
                chkMonth.SetVisible(false);
                txtDay.SetEnabled(false);
                chkDay.SetVisible(false);
                txtDayFrom.SetEnabled(false);
                txtDayTo.SetEnabled(false);
                break;
            case "quarter":
                txtYear.SetEnabled(false);
                txtQuarter.SetEnabled(true);
                txtQuarterYear.SetEnabled(true);
                chkQuarter.SetVisible(true);
                txtMonth.SetEnabled(false);
                chkMonth.SetVisible(false);
                txtDay.SetEnabled(false);
                chkDay.SetVisible(false);
                txtDayFrom.SetEnabled(false);
                txtDayTo.SetEnabled(false);
                break;
            case "month":
                txtYear.SetEnabled(false);
                txtQuarter.SetEnabled(false);
                txtQuarterYear.SetEnabled(false);
                chkQuarter.SetVisible(false);
                txtMonth.SetEnabled(true);
                chkMonth.SetVisible(true);
                txtDay.SetEnabled(false);
                chkDay.SetVisible(false);
                txtDayFrom.SetEnabled(false);
                txtDayTo.SetEnabled(false);
                break;
            case "day":
                txtYear.SetEnabled(false);
                txtQuarter.SetEnabled(false);
                txtQuarterYear.SetEnabled(false);
                chkQuarter.SetVisible(false);
                txtMonth.SetEnabled(false);
                chkMonth.SetVisible(false);
                txtDay.SetEnabled(true);
                chkDay.SetVisible(true);
                txtDayFrom.SetEnabled(false);
                txtDayTo.SetEnabled(false);
                break;
            case "arbitrary":
                txtYear.SetEnabled(false);
                txtQuarter.SetEnabled(false);
                txtQuarterYear.SetEnabled(false);
                chkQuarter.SetVisible(false);
                txtMonth.SetEnabled(false);
                chkMonth.SetVisible(false);
                txtDay.SetEnabled(false);
                chkDay.SetVisible(false);
                txtDayFrom.SetEnabled(true);
                txtDayTo.SetEnabled(true);
                break;
        }
    }

</script>
<style type="text/css">
    .style1
    {
        width: 125px;
    }
</style>

<div style="margin:5px;">


<table>
<tr>
<td>from</td><td>
    <dx:ASPxDateEdit ID="ASPxDateEdit1" runat="server" 
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Spacing="0" 
        SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" 
        DisplayFormatString="dd.MM.yyyy" EditFormat="Custom"  AutoPostBack="true" 
        EditFormatString="dd.MM.yyyy" UseMaskBehavior="True" 
        AllowUserInput="False" Width="100px">
        <CalendarProperties>
            <HeaderStyle Spacing="1px" />
        </CalendarProperties>
        <ButtonStyle Width="13px">
        </ButtonStyle>
    </dx:ASPxDateEdit>
</td>
<td>to</td><td>
    <dx:ASPxDateEdit ID="ASPxDateEdit2" runat="server" 
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" Spacing="0" AutoPostBack="true" 
        SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"    DisplayFormatString="dd.MM.yyyy" EditFormat="Custom" 
        EditFormatString="dd.MM.yyyy" UseMaskBehavior="True" AllowUserInput="False" Width="100px">
        <CalendarProperties>
            <HeaderStyle Spacing="1px" />
        </CalendarProperties>
        <ButtonStyle Width="13px">
        </ButtonStyle>
    </dx:ASPxDateEdit>

</td>
<td>

<div style="cursor:pointer;">
    <dx:ASPxImage ID="btnEditPeriod" runat="server" ImageUrl="~/Images/calendar2.png" >
    </dx:ASPxImage>
</div>

</td>
</tr>
</table>






<asp:HiddenField ID="hfPeriodType" runat="server" ClientIDMode="Static" Value="year" />


<dx:ASPxPopupControl ID="ASPxPopupControl1" runat="server" ClientInstanceName="pcPeriod"
    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" EnableHotTrack="False"
    Height="186px" Width="340px" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"
    CloseAction="CloseButton" Modal="True" PopupElementID="btnEditPeriod" PopupHorizontalAlign="WindowCenter"
    PopupVerticalAlign="WindowCenter" RenderIFrameForPopupElements="False" HeaderText="Select new period">
    <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
    </LoadingPanelImage>
    <LoadingPanelStyle ImageSpacing="5px">
    </LoadingPanelStyle>
    <ContentCollection>
        <dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
            <table id="tblSetPeriod" border="0" cellpadding="2" cellspacing="0" style="margin-bottom: 10px;">
                <tr class="pertr">
                    <td style="width: 16px;" valign="top">
                        <input id="rYear" name="R1" type="radio" value="year" />
                    </td>
                    <td style="text-align: right;" valign="top">
                        Year:
                    </td>
                    <td class="style1" valign="top">
                        <dx:ASPxSpinEdit ID="txtYear" runat="server" ClientIDMode="Static" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                            CssPostfix="DevEx" Height="21px" HorizontalAlign="Center" Number="0" NumberType="Integer"
                            Spacing="0" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Width="60px">
                            <SpinButtons HorizontalSpacing="0">
                            </SpinButtons>
                        </dx:ASPxSpinEdit>
                    </td>
                    <td valign="top" style="width: 110px;">
                        &nbsp;
                    </td>
                </tr>
                <tr class="pertr">
                    <td style="width: 16px;" valign="top">
                        <input id="rQuarter" name="R1" type="radio" value="quarter" />
                    </td>
                    <td style="text-align: right;" valign="top">
                        Quarter:
                    </td>
                    <td class="style1" valign="top">
                        <div style="float: left; margin-right: 5px;">
                            <dx:ASPxSpinEdit ID="txtQuarter" runat="server" ClientIDMode="Static" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                                CssPostfix="DevEx" Height="21px" HorizontalAlign="Center" MaxValue="4" MinValue="1"
                                Number="0" NumberType="Integer" Spacing="0" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"
                                Width="40px">
                                <SpinButtons HorizontalSpacing="0">
                                </SpinButtons>
                            </dx:ASPxSpinEdit>
                        </div>
                        <div style="float: left;">
                            <dx:ASPxSpinEdit ID="txtQuarterYear" runat="server" ClientIDMode="Static" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                                CssPostfix="DevEx" Height="21px" HorizontalAlign="Center" Number="0" NumberType="Integer"
                                Spacing="0" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Width="60px">
                                <SpinButtons HorizontalSpacing="0">
                                </SpinButtons>
                            </dx:ASPxSpinEdit>
                        </div>
                        <div class="clear">
                        </div>
                    </td>
                    <td valign="top">
                        <dx:ASPxCheckBox ID="chkQuarter" runat="server" CheckState="Unchecked" ClientIDMode="Static"
                            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"
                            Text="from year start">
                        </dx:ASPxCheckBox>
                    </td>
                </tr>
                <tr class="pertr">
                    <td style="width: 16px;" valign="top">
                        <input id="rMonth" name="R1" type="radio" value="month" />
                    </td>
                    <td style="text-align: right;" valign="top">
                        Month:
                    </td>
                    <td class="style1" valign="top">
                        <dx:ASPxTimeEdit ID="txtMonth" runat="server" ClientIDMode="Static" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                            CssPostfix="DevEx" DisplayFormatString="MMMM yyyy" EditFormat="Date" EditFormatString="MM.yyyy"
                            Spacing="0" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Width="120px">
                        </dx:ASPxTimeEdit>
                    </td>
                    <td valign="top" valign="top">
                        <dx:ASPxCheckBox ID="chkMonth" runat="server" CheckState="Unchecked" ClientIDMode="Static"
                            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"
                            Text="from year start">
                        </dx:ASPxCheckBox>
                    </td>
                </tr>
                <tr class="pertr">
                    <td style="width: 16px;" valign="top">
                        <input id="rDay" name="R1" type="radio" value="day" />
                    </td>
                    <td style="text-align: right;" valign="top">
                        Day:
                    </td>
                    <td class="style1" valign="top">
                        <dx:ASPxDateEdit ID="txtDay" runat="server" ClientIDMode="Static" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                            CssPostfix="DevEx" DisplayFormatString="dd.MM.yyyy" EditFormat="Custom" EditFormatString="dd.MM.yyyy"
                            Spacing="0" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Width="120px">
                            <CalendarProperties>
                                <HeaderStyle Spacing="1px" />
                            </CalendarProperties>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                        </dx:ASPxDateEdit>
                    </td>
                    <td valign="top">
                        <dx:ASPxCheckBox ID="chkDay" runat="server" CheckState="Unchecked" ClientIDMode="Static"
                            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"
                            Text="from year start">
                        </dx:ASPxCheckBox>
                    </td>
                </tr>
                <tr class="pertr">
                    <td style="width: 16px;" valign="top">
                        <input id="rArbitrary" name="R1" type="radio" value="arbitrary" />
                    </td>
                    <td style="text-align: right;" valign="top">
                        Arbitrary<br />
                        interval:
                    </td>
                    <td class="style1" valign="top">
                        <div style="margin-bottom: 5px;">
                            <dx:ASPxDateEdit ID="txtDayFrom" runat="server" ClientIDMode="Static" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                                CssPostfix="DevEx" DisplayFormatString="dd.MM.yyyy" EditFormat="Custom" EditFormatString="dd.MM.yyyy"
                                Spacing="0" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Width="120px">
                                <CalendarProperties>
                                    <HeaderStyle Spacing="1px" />
                                </CalendarProperties>
                                <ButtonStyle Width="13px">
                                </ButtonStyle>
                            </dx:ASPxDateEdit>
                        </div>
                        <dx:ASPxDateEdit ID="txtDayTo" runat="server" ClientIDMode="Static" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                            CssPostfix="DevEx" DisplayFormatString="dd.MM.yyyy" EditFormat="Custom" EditFormatString="dd.MM.yyyy"
                            Spacing="0" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Width="120px">
                            <CalendarProperties>
                                <HeaderStyle Spacing="1px" />
                            </CalendarProperties>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                        </dx:ASPxDateEdit>
                    </td>
                    <td valign="top" valign="top">
                    </td>
                </tr>
                <tr>
                    <td colspan="4" valign="top">
                        <dx:ASPxCheckBox ID="chkSaveLength" runat="server" CheckState="Unchecked" 
                            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                            SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" 
                            Text="Save period length as default to be used in widgets and statistics" 
                            Font-Italic="True">
                            <CheckBoxStyle Font-Italic="True" />
                        </dx:ASPxCheckBox>
                    </td>
                </tr>
            </table>
            <dx:ASPxButton ID="btnChange" runat="server" Width="100%" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css" Text="Set period">
                <ClientSideEvents Click="function(s, e) {
	pcPeriod.Hide();
}" />
            </dx:ASPxButton>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>
</div>

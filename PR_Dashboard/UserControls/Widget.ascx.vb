﻿
Public Class UserControls_Widget
    Inherits System.Web.UI.UserControl
    Private Dim _StrInput As String
    Protected _Widget_ID As Integer

    Public Property StrInput() As String
        Get
            Return _StrInput
        End Get
        Set(ByVal value As String)
            _StrInput = Value
        End Set
    End Property
    Public Property Widget_ID() As Integer
        Get
            Widget_ID = _Widget_ID
        End Get
        Set(ByVal value As Integer)
            _Widget_ID = value
        End Set
    End Property




    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim w As New BLL.Widgets.Widget(Widget_ID)
        Me.lit_w_title.Text = w.Title

        If w.IsInternal Then
            Me.mv_content.ActiveViewIndex = 0
         
        Else
            Me.mv_content.ActiveViewIndex = 1
            Me.lit_ext_resp.Text = w.ExternalResponse(StrInput, Nothing)
        End If

    End Sub

   
End Class
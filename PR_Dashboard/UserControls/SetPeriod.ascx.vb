﻿Imports System.Xml
Imports System.Drawing
Imports BioWS.BLL.Extensions



Public Class SetPeriod
    Inherits System.Web.UI.UserControl

    Public Property StartDate() As DateTime
    Public Property EndDate() As DateTime
    Public Event PeriodChanged(ByVal StartDate As DateTime, ByVal EndDate As DateTime)


    Protected Overrides Function SaveControlState() As Object
        Dim controlstate(3) As Object
        controlstate(1) = MyBase.SaveControlState()
        controlstate(2) = StartDate
        controlstate(3) = EndDate
        Return controlstate
    End Function
    Protected Overrides Sub LoadControlState(ByVal savedState As Object)
        Dim controlstate() As Object = CType(savedState, Object)
        MyBase.LoadControlState(controlstate(1))
        StartDate = CDate(controlstate(2))
        EndDate = CDate(controlstate(3))
    End Sub
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)


        If Not Page.IsPostBack Then
            Dim pr As New ProfileCommon
            pr = pr.GetProfile(HttpContext.Current.User.Identity.Name)

            ResetSelected()
            If pr.DefaultPeriod.Length > 0 Then
                If pr.DefaultPeriod = "custom" Then
                    hlCurPeriod.CssClass = "btn active"
                    StartDate = Today.AddDays(-pr.DefaultPeriodDays)
                    EndDate = Today
                Else
                    Dim lnkb As LinkButton = CType(Me.FindControl("lb" & pr.DefaultPeriod), LinkButton)
                    lnkb.CssClass = "btn  active"
                    SetDateByLinkbName("lb" & pr.DefaultPeriod)
                End If
            Else
                lb7d.CssClass = "btn active"
                SetDateByLinkbName("lb7d")
            End If

            litPeriod.Text = String.Format("{0:dd MMM yyyy} &mdash; {1:dd MMM yyyy}", StartDate, EndDate)


            ASPxDateEdit1.Date = StartDate
            ASPxDateEdit2.Date = EndDate
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub



    'Protected Sub btnChange_Click(sender As Object, e As EventArgs) Handles btnChange.Click
    '    Dim type As String = CType(hfPeriodType.Value, String)

    '    Dim startdt As DateTime
    '    Dim enddt As DateTime

    '    Select Case type
    '        Case "year"
    '            startdt = New Date(txtYear.Text, 1, 1)
    '            enddt = New Date(txtYear.Text, 12, 31)
    '        Case "quarter"
    '            Dim yearstart As DateTime = New Date(txtQuarterYear.Text, 1, 1)
    '            enddt = yearstart.AddMonths(3 * txtQuarter.Text).AddDays(-1)
    '            If chkQuarter.Checked = True Then
    '                startdt = yearstart
    '            Else
    '                startdt = yearstart.AddMonths(3 * txtQuarter.Text - 3)
    '            End If
    '        Case "month"
    '            enddt = txtMonth.DateTime.AddMonths(1).AddDays(-1)
    '            If chkMonth.Checked = True Then
    '                startdt = New Date(txtMonth.DateTime.Year, 1, 1)
    '            Else
    '                startdt = txtMonth.DateTime
    '            End If
    '        Case "day"
    '            enddt = txtDay.Date
    '            If chkDay.Checked = True Then
    '                startdt = New Date(txtDay.Date.Year, 1, 1)
    '            Else
    '                enddt = txtDay.Date
    '            End If
    '        Case "arbitrary"
    '            startdt = txtDayFrom.Date
    '            enddt = txtDayTo.Date
    '    End Select

    '    Dim days As Integer = DateDiff(DateInterval.Day, startdt, enddt)
    '    If HttpContext.Current.User.Identity.IsAuthenticated And chkSaveLength.Checked Then
    '        Dim _profile As New ProfileCommon
    '        _profile = _profile.GetProfile(HttpContext.Current.User.Identity.Name)
    '        _profile.DefaultPeriodDays = days
    '        _profile.Save()
    '    End If


    '    StartDate = startdt
    '    EndDate = enddt
    '    ASPxDateEdit1.Date = startdt
    '    ASPxDateEdit2.Date = enddt
    '    RaiseEvent PeriodChanged(ASPxDateEdit1.Date, ASPxDateEdit2.Date)


    'End Sub

    Protected Sub RaisePeriodChanged(ByVal periodType As String)

        Dim days As Integer = DateDiff(DateInterval.Day, StartDate, EndDate)
        If HttpContext.Current.User.Identity.IsAuthenticated Then
            Dim _profile As New ProfileCommon
            _profile = _profile.GetProfile(HttpContext.Current.User.Identity.Name)
            _profile.DefaultPeriodDays = days
            _profile.DefaultPeriod = periodType
            _profile.Save()
        End If
        litPeriod.Text = String.Format("{0:dd MMM yyyy} &mdash; {1:dd MMM yyyy}", StartDate, EndDate)

        RaiseEvent PeriodChanged(StartDate, EndDate)
    End Sub

    Protected Sub ResetSelected()
        lb7d.CssClass = ""
        lb1m.CssClass = ""
        lb3m.CssClass = ""
        lb6m.CssClass = ""
        lb1y.CssClass = ""
        lball.CssClass = ""
        hlCurPeriod.CssClass = ""
    End Sub

    Protected Sub SetDateByLinkbName(ByVal linkbuttonname As String)
        EndDate = Today
        Select Case linkbuttonname
            Case "lb7d"
                StartDate = Today.AddDays(-6)
            Case "lb1m"
                StartDate = Today.AddMonths(-1)
            Case "lb3m"
                StartDate = Today.AddMonths(-3)
            Case "lb6m"
                StartDate = Today.AddMonths(-6)
            Case "lb1y"
                StartDate = Today.AddYears(-1)
            Case "lball"
                StartDate = New Date(2009, 1, 1)
        End Select



    End Sub
    Protected Sub period_Click(sender As Object, e As EventArgs)
        Dim lnkb As LinkButton = CType(sender, LinkButton)
        SetDateByLinkbName(lnkb.ID)
        ResetSelected()
        lnkb.CssClass = "btn active"
        RaisePeriodChanged(lnkb.ID.Substring(2, lnkb.ID.Length - 2))
    End Sub


    Protected Sub btnSet_Click(sender As Object, e As EventArgs) Handles btnSet.Click
        StartDate = ASPxDateEdit1.Date
        EndDate = ASPxDateEdit2.Date
        ResetSelected()
        hlCurPeriod.CssClass = "btn active"
        RaisePeriodChanged("custom")
    End Sub
End Class
﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ViewWidget.ascx.vb" Inherits="BioWS.ViewWidget4" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp1" %>



<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSplitter" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>


<dx:ASPxSplitter ID="ASPxSplitter1" runat="server" ClientIDMode="AutoID" CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css"
    CssPostfix="Office2010Silver" Height="550px">
    <Panes>
        <dx:SplitterPane ContentUrlIFrameName="f1">
            <ContentCollection>
                <dx:SplitterContentControl ID="SplitterContentControl2" runat="server" SupportsDisabledAttribute="True">
                    <asp:Literal ID="lit_w" runat="server" Text="Select widget in left navigation panel"></asp:Literal>
                </dx:SplitterContentControl>
            </ContentCollection>
        </dx:SplitterPane>
    </Panes>
    <Styles CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css" CssPostfix="Office2010Silver">
    </Styles>
    <Images SpriteCssFilePath="~/App_Themes/Office2010Silver/{0}/sprite.css">
    </Images>
</dx:ASPxSplitter>




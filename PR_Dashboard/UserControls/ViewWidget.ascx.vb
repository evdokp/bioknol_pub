﻿Public Class ViewWidget4
    Inherits System.Web.UI.UserControl

    Public Property PubmedID As Integer
    Public Property WidgetID As Integer
    Public Property PersonID As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.ASPxSplitter1.Panes(0).ContentUrl = String.Format("../Widgets/ViewWidget.aspx?PersonID={0}&WidgetID={1}&Pubmed_ID={2}",
                                              PersonID,
                                              WidgetID,
                                              PubmedID)
    End Sub

End Class
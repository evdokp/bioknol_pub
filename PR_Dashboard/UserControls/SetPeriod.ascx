﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SetPeriod.ascx.vb"
    Inherits="BioWS.SetPeriod" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>

<div style="margin: 5px; width:370px;" class="setperiod">

  <div class="btn-group" data-toggle="buttons-radio">
    <asp:LinkButton ID="lb7d" runat="server"  class="btn" OnClick="period_Click">7d</asp:LinkButton>
    <asp:LinkButton ID="lb1m" runat="server"  class="btn" OnClick="period_Click">1m</asp:LinkButton>
    <asp:LinkButton ID="lb3m" runat="server"  class="btn" OnClick="period_Click">3m</asp:LinkButton>
    <asp:LinkButton ID="lb6m" runat="server"  class="btn" OnClick="period_Click">6m</asp:LinkButton>
    <asp:LinkButton ID="lb1y" runat="server"  class="btn" OnClick="period_Click">1y</asp:LinkButton>
    <asp:LinkButton ID="lball" runat="server"  class="btn" OnClick="period_Click">All</asp:LinkButton>
    <asp:HyperLink ID="hlCurPeriod"  class="btn" runat="server">Custom</asp:HyperLink>
</div>
    
    <br />
   <span style="font-size:11px;"> <span style="color:#b0b0b0;">Current period:&nbsp;</span><asp:Literal ID="litPeriod" runat="server"></asp:Literal></span>
    <dx:ASPxPopupControl ID="puCustom" runat="server" PopupAction="MouseOver" PopupElementID="hlCurPeriod"
        ClientInstanceName="puCustom" ShowHeader="false">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
                <table>
                    <tr>
                        <td>
                            from
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="ASPxDateEdit1" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                                CssPostfix="DevEx" Spacing="0" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"
                                DisplayFormatString="dd.MM.yyyy" EditFormat="Custom" AutoPostBack="false" EditFormatString="dd.MM.yyyy"
                                UseMaskBehavior="True" AllowUserInput="False" Width="130px">
                                <CalendarProperties>
                                    <HeaderStyle Spacing="1px" />
                                </CalendarProperties>
                                <ButtonStyle Width="13px">
                                </ButtonStyle>
                            </dx:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            to
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="ASPxDateEdit2" runat="server" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                                CssPostfix="DevEx" Spacing="0" AutoPostBack="false" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css"
                                DisplayFormatString="dd.MM.yyyy" EditFormat="Custom" EditFormatString="dd.MM.yyyy"
                                UseMaskBehavior="True" AllowUserInput="False" Width="130px">
                                <CalendarProperties>
                                    <HeaderStyle Spacing="1px" />
                                </CalendarProperties>
                                <ButtonStyle Width="13px">
                                </ButtonStyle>
                            </dx:ASPxDateEdit>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnSet" runat="server" Text="Set" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                                CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                <ClientSideEvents Click="function(s,e){ puCustom.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnClose" runat="server" Text="Cancel" AutoPostBack="false" CssFilePath="~/App_Themes/DevEx/{0}/styles.css"
                                CssPostfix="DevEx" SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                <ClientSideEvents Click="function(s,e){ puCustom.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</div>

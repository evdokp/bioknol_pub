﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AzureStorageLibrary
{
    public static class StringExtensions
    {
        public static string ToUrlSlug(this string input)
        {
            return Regex.Replace(input, @"[^a-z0-9]+", "-", RegexOptions.IgnoreCase).Trim(new char[] {'-'}).ToLower();
        }
    }
}

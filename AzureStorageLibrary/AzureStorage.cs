﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace AzureStorageLibrary
{
    public class AzureStorage
    {
        private CloudBlobContainer Container { get; set; }
        private CloudBlobClient BlobClient { get; }
        private const string AccountName = "bioknolstorage";
        private const string KeyValue = "w9LXIVEraYZU54WhmU+zAyjgFqzOjXRf0l+QLOazj25QdZWsiByMvD0OrDotWHDbRt1MveaOHqSSo+ucdqJcqg==";

        public AzureStorage()
        {
            StorageCredentials credentials = new StorageCredentials(AccountName, KeyValue);
            CloudStorageAccount storageAccount = new CloudStorageAccount(credentials, true);
            BlobClient = storageAccount.CreateCloudBlobClient();
        }

        public void UploadArticle(byte[] data, string fullfilename)
        {
            UploadFile(data, fullfilename, "articles");
        }
        public void UploadImage(byte[] data, string fullfilename)
        {
            UploadFile(data, fullfilename, "userimages");
            Container.SetPermissions(new BlobContainerPermissions
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            });
        }
        public void UploadHtml(string html, string fullfilename)
        {
            UploadFile(Encoding.Default.GetBytes(html), fullfilename, "savedhtml");
        }
        private void UploadFile(byte[] data, string fullFileName, string containerName)
        {
            Container = BlobClient.GetContainerReference(containerName);
            Container.CreateIfNotExists();
            CloudBlockBlob fileBlob = Container.GetBlockBlobReference(fullFileName);
            using (var stream = new MemoryStream(data, writable: false))
            {
                fileBlob.UploadFromStream(stream);
            }
        }
        public void DownloadFileToStream(string fullfilename, ref Stream stream)
        {
            CloudBlobContainer articles = BlobClient.GetContainerReference("articles");
            // get reference to blob
            CloudBlockBlob articleBlob = articles.GetBlockBlobReference(fullfilename);
            using (stream)
            {
                articleBlob.DownloadToStream(stream);
            }
        }
        public string DownloadHtml(string fullfilename)
        {
            CloudBlobContainer savedhtml = BlobClient.GetContainerReference("savedhtml");
            savedhtml.CreateIfNotExists();
            CloudBlockBlob htmlBlob = savedhtml.GetBlockBlobReference(fullfilename);
            using (var memoryStream = new MemoryStream())
            {
                htmlBlob.DownloadToStream(memoryStream);
                return Encoding.UTF8.GetString(memoryStream.ToArray());
            }
        }

        public void DeleteImage(string fullFileName)
        {
            Container = BlobClient.GetContainerReference("userimages");
            Container.CreateIfNotExists();
            CloudBlockBlob fileBlob = Container.GetBlockBlobReference(fullFileName);
            if (fileBlob.Exists())
                fileBlob.Delete();
        }

        public IEnumerable<IListBlobItem> GetBlobsByContainer(string containerName)
        {
            CloudBlobContainer container = BlobClient.GetContainerReference(containerName);
            return container.ListBlobs();
        }
    }
}
